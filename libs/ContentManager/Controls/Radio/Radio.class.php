<?php
/**
 * Элемент управления типа Radio.
 * Переключатель. Можно выбирать одно из значений 
 * связанного с полем перечисления (enum).
 * Или выбирать значения из связанной таблицы, 
 * для чего необходимо задать настройку referentTable и listingKey.
 * 
 * Настройки: 
 * - enum : bool
 * - referentTable : string (имя связанной таблицы со значениями)
 * - listingKey : string (имя поля с человеко-понятным значением)
 * - CSSClass : string
 * 
 * Возвращает контрол в готовом XHTML-виде controlHTML
 * 
 * @version 3.1
 *
 */
class Radio extends Control 
{    
    /**
     * Имя таблицы со значениями
     *
     * @var string
     */
    private $referentTableName = null;
    
    /**
     * Имя поля с человеко-понятным значением
     *
     * @var string
     */
    private $listingKey = null;
    
    /**
     * Использовать связанное с полем перечисление
     *
     * @var bool
     */
    private $isEnum = false;

    public function InitControl($value = null, $rowID = null)
    {
        $optionTemplate = '<input type="radio" name="{name}" value="{value}" {class} {selected} id="radio_{name}_{i}" /><label for="radio_{name}_{i}" class="subLabel">{text}</label>';
        $SelectedWord = 'checked="checked"';
                        
        $this -> LoadSettings();

        $options = '';
        if ($this -> isEnum)
        {
            $query = "SELECT `column_type` 
                      FROM `information_schema`.`columns` 
                      WHERE `table_name` = '". $this -> controlsRegistry -> activeTableStructure -> TableName ."' 
                        AND `column_name` = '". $this -> name . "'
                        AND `table_schema` = '".DB::$DBName."'";
            $result = DB::FetchAssoc(DB::Query($query));
            $items = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$result['column_type']));

            $cnt = count($items);

            $curOption = '';
            $options = '<div class="radioGroup">';
            for ($i = 0; $i < $cnt; $i++)
            {
                $curOption = str_replace('{text}', $items[$i], $optionTemplate);
                $curOption = str_replace('{value}', $items[$i], $curOption);
                $curOption = str_replace('{class}', $this -> CSSClass, $curOption);                    
                $curOption = str_replace('{name}', $this -> name, $curOption);                    
                if ($value == $items[$i])
                    $curOption = str_replace('{selected}', $SelectedWord, $curOption);
                else 
                    $curOption = str_replace('{selected}', '', $curOption);
                $curOption = str_replace('{i}', $i, $curOption);
                $options .= $curOption;    
            }
            $options .= '</div>';
        }
        else 
        {
            // Вытаскиваем значения ReferentTable
            $referentTableManager = new DBTableManager($this -> referentTableName);
            $entries = $referentTableManager -> Select();
            
            $cnt = count($entries);
            
            if ($this -> required && $cnt == 0)
            {
                throw new Exception('No entries in reference table! '.$this -> name);
            }
            
            $curOption = '';
            for ($i = 0; $i < $cnt; $i++)
            {
                $curOption = str_replace('{text}', $entries[$i][$this -> listingKey], $optionTemplate);
                $curOption = str_replace('{value}', $entries[$i][$referentTableManager -> TableStructure -> PrimaryKey -> Name], $curOption);
                $curOption = str_replace('{class}', $this -> CSSClass, $curOption);                    
                $curOption = str_replace('{name}', $this -> name, $curOption);                    
                if ($value == $entries[$i][$referentTableManager -> TableStructure -> PrimaryKey -> Name])
                    $curOption = str_replace('{selected}', $SelectedWord, $curOption);
                else 
                    $curOption = str_replace('{selected}', '', $curOption);
                $options .= $curOption;    
            }
        }
        
        $this -> controlHTML = $options;
        $this -> type = __CLASS__;
    }

    private function LoadSettings()
    {
        if ($this -> controlSettings['enum'])
           $this -> isEnum = true;
        else
        {
            if (!empty($this -> controlSettings['referentTable']))
               $this -> referentTableName = $this -> controlSettings['referentTable'];
            else 
               throw new Exception('Setting `referentTable` for '.$this -> name.' is undefined.');

            if (!empty($this -> controlSettings['listingKey']))
               $this -> listingKey = $this -> controlSettings['listingKey'];
            else 
               throw new Exception('Setting `listingKey` for '.$this -> name.' is undefined.');     
        }
    }

    public function PostBackHandler(&$data, $rowID = null){}
}
?>
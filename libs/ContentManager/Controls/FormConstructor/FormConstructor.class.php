<?php

require_once(IncPaths::$LIBS_PATH .'ControlsManager/ControlsManager.class.php');

/**
 * Контрол построения форм
 * 
 * @version 1.0
 *
 * Settings:
 *
 */
class FormConstructor extends Control implements IFlushingControl
{
	public function InitControl($value = null, $rowID = null)
	{
		$this -> LoadSettings();
		
		$controlsManager = new ControlsManager();
		
		$controls = $controlsManager -> GetControls(array('isPublic' => 1));
	    
	    $controlsCnt = count($controls);
	    for ($i = 0; $i < $controlsCnt; $i++)
	    {
	        $controls[$i]['settings'] = $controlsManager -> GetSettings($controls[$i]['controlID'], array('isPublic' => 1));
	    }
		
		$this -> controlTplPath = parent::$CONTROLS_PATH . __CLASS__ .'/control.tpl';

		$this -> controlTplVars = array();
		$this -> controlTplVars['CONTROLS'] = $controls;
		$this -> controlTplVars['NAME'] = $this -> name;
		$this -> controlTplVars['TIP']  = $this -> tip;

		// Используемые JS-библиотеки
        $this -> controlJSLibs = array();

        // Инициализирующая функция
        $this -> controlJSFiles = array();
        $this -> controlJSFiles[] = parent::$CONTROLS_ALIAS.__CLASS__.'/init.js';

        // Переменные, необходимые JS
        $this -> controlJSVars = array();
        $this -> controlJSVars['controls'] = $controls;

		$this -> type = __CLASS__;
	}

	private function LoadSettings()
	{
		
	}

	public function PostBackHandler(&$data, $rowID = null)
	{
		$this -> LoadSettings();
	}
	
	public function Flush(&$data, $rowID)
	{
	    
	}
}
?>
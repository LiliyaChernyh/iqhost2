<?php

/**
 * Класс для работы с модулями
 *
 * @version 1.2
 *
 */
class Modules
{
	/**
	 * Менеджер работы с таблицей модулей
	 *
	 * @var DBTableManager
	 */
	var $ModulesTable;

	/**
	 * Менеджер работы с таблицей настроек модулей
	 *
	 * @var DBTableManager
	 */
	var $ModulesSettingsTable;

	/**
	 * Менеджер работы с таблицей типов страниц
	 *
	 * @var DBTableManager
	 */
	var $PageTypesTable;

        /**
	 * Менеджер работы с таблицей страниц
	 *
	 * @var DBTableManager
	 */
	var $PageStructure;

        /**
	 * Менеджер работы с таблицей роутов
	 *
	 * @var DBTableManager
	 */
	var $Routes;

	/**
	 * Директория, в которой находятся модули
	 *
	 * @var string
	 */
	var $ModulesDir;

	/**
	 * Имена доступных модулей
	 *
	 * @var array
	 */
	var $AvailableModulesNames;

	public function __construct()
	{
		$this -> ModulesTable 			= new DBTableManager(TablesNames::$MODULES_TABLE_NAME);
		$this -> ModulesSettingsTable 		= new DBTableManager(TablesNames::$MODULES_SETTINGS_TABLE_NAME);
                $this -> PageTypesTable                 = new DBTableManager(TablesNames::$PAGES_TYPE_TABLE_NAME);
                $this -> PageStructure                  = new DBTableManager(TablesNames::$PAGE_STRUCTURE_TABLE_NAME);
                $this -> Routes                         = new DBTableManager(TablesNames::$ROUTING_TABLE_NAME);

		$this -> AvailableModulesNames		= array();
		$this -> ModulesSettingsTable -> SortOrder = "`order`";
	}

	/**
	 * Ищет доступные модули
	 * Заполняет массив $this -> AvailableModulesNames
	 *
	 */
	public function ModulesSearch()
	{
		$this -> ModulesDir = IncPaths::$MODULES_PATH;

		if (!is_dir($this -> ModulesDir))
			throw new FileNotFoundException("Директория с модулями не найдена");

		if (!($dir = opendir($this -> ModulesDir)))
			throw new Exception("Не удается прочитать директорию с модулями");

		while (($item = readdir($dir)) !== false)
		{
			if(is_dir($this -> ModulesDir.$item) && $item != "." && $item != "..")
			{
				$this -> AvailableModulesNames[] = $item;
			}
		}

		closedir($dir);
	}

	/**
	 * Возвращает имя модуля по алиасу
	 *
	 * @param unknown_type $moduleAlias
	 * @return unknown
	 */
	public function GetModuleClassNameByAlias($moduleAlias)
	{
	    $installedModules = $this -> GetInstalledModules();
	    $moduleAlias = strtolower($moduleAlias);

	    for ($i = 0; $i < count($installedModules); $i++)
	    {
	        if (strtolower($installedModules[$i]['name']) == $moduleAlias)
	           return $installedModules[$i]['name'];
	    }
	}

	/**
	 * Проверяет, установлен ли модуль
	 *
	 * @param string $moduleName
	 * @return bool
	 */
	public function isModuleInstalled($moduleName)
	{
		if (empty($moduleName) || !is_string($moduleName))
			throw new ArgumentException("Название модуля должно быть непустой строкой");

		$num = $this -> ModulesTable -> Count(array('name' => $moduleName));

		if ($num == 0)
			return false;
		else
			return true;
	}

	/**
	 * Возвращает массив атрибутов установленных модулей
	 *
	 * @return array
	 */
	public function GetInstalledModules()
	{
		return $rows = $this -> ModulesTable -> Select();
	}

	/**
	 * Возвращает массив имен доступных, но не установленных модулей
	 *
	 * @return array
	 */
	public function GetUninstalledModules()
	{
		if (empty($this -> AvailableModulesNames))
			$this -> ModulesSearch();

		$uninstalledModules = array();

		foreach ($this -> AvailableModulesNames as $module)
		{
			if (!$this -> isModuleInstalled($module))
				$uninstalledModules[] = $module;
		}

		return $uninstalledModules;
	}

	/**
	 * Возвращает массив имен доступных модулей
	 *
	 * @return array
	 */
	public function GetAvailableModules()
	{
		if (empty($this -> AvailableModulesNames))
			$this -> ModulesSearch();

		return $this -> AvailableModulesNames;
	}

	/**
	 * Установка модуля
	 *
	 * @param string $moduleName
	 */
	public function InstallModule($moduleName)
	{
		if (empty($moduleName) || !is_string($moduleName))
			throw new ArgumentException("Название модуля должно быть непустой строкой");
		
		if (empty($this -> AvailableModulesNames))
			$this -> ModulesSearch();

                $exists = false;
                $moduleName = strtolower($moduleName);
                for ($i = 0; $i < count($this -> AvailableModulesNames) && !$exists; $i++)
                {
                   if (strtolower($this -> AvailableModulesNames[$i]) == $moduleName)
                   {
                       $exists = true;
                       $moduleName = $this -> AvailableModulesNames[$i];
                   }
                }

                if (!$exists)
                        throw new Exception('Такого модуля не существует');

                if ($this -> isModuleInstalled($moduleName))
                        throw new Exception('Указанный модуль уже установлен');

                // Устанавливаем модуль из XML файла
                $this -> InstallFromXML($moduleName);
	}

	/**
	 * Читает XML файл установки модуля и возвращает всю информацию о модуле
	 *
	 * @param string $xmlPath
	 * @return array
	 */
	public function ReadXML($xmlPath)
	{
		// Проверяем входные данные
		if (!file_exists($xmlPath))
			throw new FileNotFoundException("Can not find $xmlPath");

		if (!$xml = simplexml_load_file($xmlPath))
			throw new Exception("Can not parse xml");



		$moduleInfo = array();

                // Читаем настройки модуля (числа: 1 - означает обязательность поля, 0 - нет)
                $moduleProperty = array('name' => 1,
                                        'description' => 1,
                                        'rootAlias' => 0,
                                        'version' => 0);
                foreach ($moduleProperty as $key => $item){
                    $property = $xml -> xpath('/module/'.$key);
                    if (isset($property[0]))
                        $moduleInfo[$key] = XML::value($property[0]);
                    else{
                        if ($item) throw new Exception("Wrong xml format. Can not find '$key' node");
                    }
                }


//		// Читаем необходимые для модуля типы страниц
//		$pageTypes = $xml -> xpath('/module/pagetypes/pagetype');
//
//		$moduleInfo['pageTypes'] = array();
//
//		if (is_array($pageTypes))
//		{
//			for ($i=0; $i < count($pageTypes); $i++)
//			{
//				$type = array();
//				$type['name'] = XML::value($pageTypes[$i] -> name);
//				$type['description'] = XML::value($pageTypes[$i] -> description);
//				$type['handlerType'] = XML::value($pageTypes[$i] -> handlerType);
//				$type['constantName'] = XML::value($pageTypes[$i] -> constantName);
//                $type['noCreatePage'] = XML::value($pageTypes[$i] -> noCreatePage);
//
//				$moduleInfo['pageTypes'][] = $type;
//			}
//		}

		// Читаем настройки модуля
		$settings = $xml->xpath('/module/settings/setting');

		$settingsCnt = count($settings);

		if ($settingsCnt > 0)
		{
			$moduleInfo['settings'] = array();

			for ($i=0; $i<$settingsCnt; $i++)
			{
				$settingName = $settings[$i] -> name."";
				$moduleInfo['settings'][$settingName] = array(
                                        'value'         => XML::value($settings[$i] -> value),
										'description'	=> XML::value($settings[$i] -> description),
										'order'		=> XML::value($settings[$i] -> order),
                                        'isVisible'	=> XML::value($settings[$i] -> isVisible)
				);
			}
		}

		// Читаем запросы на создание необходимых таблиц
		$dbtables = $xml->xpath('/module/dbtables/dbtable');
		$moduleInfo['dbtables'] = array();
		for ($i=0; $i<count($dbtables); $i++)
		{
			$moduleInfo['dbtables'][] = XML::value($dbtables[$i]);
		}

                // SQL-запросы при установке модуля
		$dbtables = $xml->xpath('/module/sqlinstall/query');
		$moduleInfo['sqlinstall'] = array();
		for ($i=0; $i<count($dbtables); $i++)
		{
			$moduleInfo['sqlinstall'][] = XML::value($dbtables[$i]);
		}

                // SQL-запросы при удалении модуля
		$dbtables = $xml->xpath('/module/sqluninstall/query');
		$moduleInfo['sqluninstall'] = array();
		for ($i=0; $i<count($dbtables); $i++)
		{
			$moduleInfo['sqluninstall'][] = XML::value($dbtables[$i]);
		}

		// Запрсы на добавление данных
		$dbdump = $xml->xpath('/module/dbdata');

		$moduleInfo['dbdump'] = $dbdump;

		return $moduleInfo;
	}

        /**
	 * Формируем запросы на вставку строк
	 *
	 * @param array
	 */
	private function DBDump($dbdump)
        {
                if (is_array($dbdump))
		{
                        $res = array();
			for ($i = 0; $i < count($dbdump); $i++)
			{
				foreach($dbdump[$i] as $key1 => $value1)
				{
					$Q = "INSERT INTO `$key1` (";
					$first = true;
					$vals = " VALUES (";
					foreach ($value1 as $key2 => $value2)
					{
						if ($first)
						{
							$Q .= "`$key2`";
							$vals .= "'$value2'";
							$first = false;
						}
						else
						{
							$Q .= ", `$key2`";
							$vals .= ", '$value2'";
						}
					}
					$vals .= ")";
					$Q .= ")" . $vals;
					$Q = $Q;
					$res[] = $Q;
				}
			}

                        return $res;
                }else{
                        return false;
                }
        }

        /**
	 * Проверяем на уникальность страницы и роута с таким алиасом
	 *
	 * @param bool
	 */
	private function savePageAndRoute($rootAlias, $nameModule, $descModule)
        {
            //проверяем на наличие страницы
            $res = $this -> PageStructure -> SelectFirst(array('parentID' => 0, 'alias' => $rootAlias));
            if ($res) throw new Exception("Страница с таким алиасом уже существует, измените rootAlias в install.xml");

            //проверяем на наличие роута
            $res = $this -> Routes -> SelectFirst(array('parentID' => 1, 'pattern' => $rootAlias));
            if ($res) throw new Exception("Роут с таким алиасом уже существует, измените rootAlias в install.xml");

            //создаем страницу для front
            $this -> PageStructure -> Insert(array('caption' => $descModule, 'alias' => $rootAlias));

            //создаем роут для front
            $this -> Routes -> Insert(array('parentID' => '1', 'pattern' => $rootAlias, 'controller' => $nameModule.'ControllerFront', 'action' => 'Index'));
        }

        /**
	 * Удаляем страницу и роуты модуля
	 *
	 * @param bool
	 */
	private function delPageAndRoute($nameModule, $moduleID)
        {
            //проверяем на наличие страницы
            $res = $this -> ModulesSettingsTable -> SelectFirst(array('moduleID' => $moduleID));
            if (isset($res['value'])){
                //удаляем страницу
                $this -> PageStructure -> Delete(array('parentID' => 0, 'alias' => $res['value']));

                //удаляем роут фронта
                $this -> Routes -> Delete(array('parentID' => 1, 'pattern' => $res['value']));
            }

            //удаляем роут админки
            $this -> Routes -> Delete(array('parentID' => 2, 'pattern' => $nameModule));
        }

         /**
	 * Обновляем страницу и роут
	 *
	 * @param bool
	 */
	private function updatePageAndRoute($old_value, $new_value)
        {
            //проверяем на наличие страницы
            $res = $this -> PageStructure -> SelectFirst(array('parentID' => 0, 'alias' => $old_value));
            if (!$res) throw new Exception("Страница с таким алиасом уже существует, попробуйте ввести другой rootAlias+");
            $pageID = $res['pageID'];

            //проверяем на наличие роута
            $res = NUll;
            $res = $this -> Routes -> SelectFirst(array('parentID' => 1, 'pattern' => $old_value));
            if (!$res) throw new Exception("Роут с таким алиасом уже существует, измените rootAlias в install.xml");
            $routeID = $res['routeID'];

            //обновляем алиас страницы
            $this -> PageStructure -> Update(array('alias' => $new_value), $pageID);

            //обновляем паттерн роута
            $this -> Routes -> Update(array('pattern' => $new_value), $routeID);
        }

	/**
	 * Устанавливает модуль по его XML файлу
	 *
	 * @param string $xmlPath
	 */
	private function InstallFromXML($moduleName)
	{
		$moduleInfo = $this -> ReadXML($this -> ModulesDir.$moduleName."/install.xml");
                //устанавливаем rootAlias
                if (isset($moduleInfo['rootAlias'])){
                    $rootAlias = $moduleInfo['rootAlias'];

                    //проверяем наличие страницы и роута с таким же алиасом
                    $this -> savePageAndRoute($rootAlias, $moduleInfo['name'], $moduleInfo['description']);
                }else
                    $rootAlias = NULL;

                //устанавливаем версию модуля
                if (isset($moduleInfo['version']))
                    $version = $moduleInfo['version'];
                else
                    $version = NULL;

		// Устанавливаем модуль и его настройки
		$moduleID = $this -> ModulesTable -> Insert( array('name'        => $moduleInfo['name'],
                                                                   'description' => $moduleInfo['description'],
                                                                   'rootAlias'   => $rootAlias,
                                                                   'version'     => $version )
                                                            );

                //записываем настройку rootAlias для модуля
                if ($rootAlias != 'NULL'){
                    $modulesSettings = new DBTableManager(TablesNames::$MODULES_SETTINGS_TABLE_NAME);
                    $modulesSettings -> Insert(array('moduleID' => $moduleID, 'name' => 'rootAlias', 'value' => $rootAlias, 'description' => 'Алиас во фрон-енде'));
                }

                // Записываем роутер модуля
                $routes = new DBTableManager(TablesNames::$ROUTING_TABLE_NAME);
                $routes -> Insert(array('parentID' => '2', 'pattern' => $moduleInfo['name'], 'controller' => $moduleInfo['name'].'ControllerAdmin', 'action' => 'Index'));

                //очищаем кеш роутера, чтобы принять изменения
                ObjectsCache::KillCache(RoutingTableDBProvider::cacheFileName);

		if (!empty($moduleInfo['settings']))
		{
			$settingsToAdd = array();

			foreach ($moduleInfo['settings'] as $settingName => $settingInfo)
			{
				$settingsToAdd[] = array(
                                        'moduleID' 		=> $moduleID,
                                        'name'			=> addslashes($settingName),
                                        'value'			=> addslashes($settingInfo['value']),
                                        'description'           => addslashes($settingInfo['description']),
                                        'order'			=> $settingInfo['order'],
                                        'isVisible'             => $settingInfo['isVisible']
                                );
			}

			$this -> ModulesSettingsTable -> InsertRows($settingsToAdd);
		}

		$errors = array();

		// Создаем необходимые для модуля типы страниц
//		if (count($moduleInfo['pageTypes']) > 0)
//		{
//			$pageTypesNumber = count($moduleInfo['pageTypes']);
//
//			$pageTypesToAdd = array();
//
//			for ($i=0; $i < $pageTypesNumber; $i++)
//			{
//				$pageTypesToAdd[] = array(
//                                        'name'		=> addslashes($moduleInfo['pageTypes'][$i]['name']),
//                                        'description'	=> addslashes($moduleInfo['pageTypes'][$i]['description']),
//                                        'handlerType'	=> addslashes($moduleInfo['pageTypes'][$i]['handlerType']),
//                                        'constantName'	=> addslashes($moduleInfo['pageTypes'][$i]['constantName'])
//                                );
//			}
//
//			$this -> PageTypesTable -> InsertRows($pageTypesToAdd);
//		}

		// Создаем необходимые для модуля таблицы
		for ($i=0; $i<count($moduleInfo['dbtables']); $i++)
		{
			try
			{
				DB::Query($moduleInfo['dbtables'][$i]);
			}
			catch (SQLException $e)
			{
				$errors[] = "Ошибка во время выполнения запроса: <br />".$moduleInfo['dbtables'][$i]."<br />".$e -> getMessage();
			}
		}

                // Выполняем необходимые запросы к базе при установке
		for ($i=0; $i<count($moduleInfo['sqlinstall']); $i++)
		{
			try
			{
				DB::Query($moduleInfo['sqlinstall'][$i]);
			}
			catch (SQLException $e)
			{
				$errors[] = "Ошибка во время выполнения запроса: <br />".$moduleInfo['sqlinstall'][$i]."<br />".$e -> getMessage();
			}
		}

		// Пробуем добавить данные
                $dbdump_query = $this->DBDump($moduleInfo['dbdump']);
		for ($i=0; $i<count($dbdump_query); $i++)
		{
			try
			{
				DB::Query($dbdump_query[$i]);
			}
			catch (SQLException $e)
			{
				$errors[] = "Ошибка во время выполнения запроса: <br />".$dbdump_query[$i]."<br />".$e -> getMessage();
			}
		}

		if (count($errors) == 0) return true;
		else return implode("<br /><br />",$errors);
	}

	public function UninstallModule($moduleName)
	{
		if (empty($moduleName) || !is_string($moduleName))
			throw new ArgumentException("Название модуля должно быть непустой строкой");

		if (empty($this -> AvailableModulesNames))
			$this -> ModulesSearch();

		$moduleName =$this -> GetModuleClassNameByAlias($moduleName);
		if (empty($moduleName) && !in_array($moduleName, $this -> AvailableModulesNames))
			throw new Exception("Такого модуля не существует");

		if (!$this -> isModuleInstalled($moduleName))
			throw new Exception("Указанный модуль не установлен");

		// Получаем ID удаляемого модуля
		$module = $this -> ModulesTable -> Select(array('name' => $moduleName));
		if (count($module) != 1)
			throw new Exception("Указанный модуль не установлен");

		$moduleID = $module[0]['moduleID'];

		$moduleInfo = $this -> ReadXML($this -> ModulesDir.$moduleName."/install.xml");

		// Удаляем модуль из таблицы модулей
		$this -> ModulesTable -> Delete(array('moduleID' => $moduleID));

                // Удаляем страницу и роуты модуля
                $this -> delPageAndRoute($moduleName, $moduleID);

                //очищаем кеш роутера, чтобы принять изменения
                ObjectsCache::KillCache(RoutingTableDBProvider::cacheFileName);

		// Удаляем настройки модуля из таблицы настроек
		$this -> ModulesSettingsTable -> Delete(array('moduleID' => $moduleID), true);

//		// Удаляем страницы и типы страниц (созданные модулем)
//		for ($i=0; $i < count($moduleInfo['pageTypes']); $i++){
//                        //получаем pageTypeID по constantName
//                        $ar = $this -> PageTypesTable -> SelectFirst(array('constantName' => $moduleInfo['pageTypes'][$i]['constantName']), array('pageTypeID'));
//
//                        $this -> PageStructure -> Delete(array('pageTypeID' => $ar['pageTypeID']), true);
//			$this -> PageTypesTable -> Delete(array('constantName' => addslashes($moduleInfo['pageTypes'][$i]['constantName'])));
//                }

                // Удаляем строки из таблиц, созданные при установки модуля
                if (isset($moduleInfo['dbdump']) && is_array($moduleInfo['dbdump']))
		{
                    for ($i = 0; $i < count($moduleInfo['dbdump']); $i++)
                    {
			foreach ($moduleInfo['dbdump'][$i] as $key => $item)
			{
				try
				{
                                    // Формируем запрос на удаление
                                    $deleteQuery = "DELETE FROM ".$key;

                                    // Формируем массив полей с данными
                                    $ar_pageTypeID = array();
                                    $condition = array();
                                    foreach ($item as $k => $item_attr){
                                        $condition[] = "`".trim($k)."` = '".trim($item_attr)."'";

                                        // Запоминаем pageTypeID, если это тип страницы
                                        if ($key == 'greeny_pageTypes' && $k == 'constantName')
                                            $ar_pageTypeID = $this -> PageTypesTable -> SelectFirst(array('constantName' => $item_attr), array('pageTypeID'));
                                    }

                                    //удаляем страницы с типами созданными при установки модуля
                                    if (!empty($ar_pageTypeID))
                                        $this -> PageStructure -> Delete(array('pageTypeID' => $ar_pageTypeID['pageTypeID']), true);

                                    $deleteQuery .= " WHERE ".implode(" AND ", $condition);
                                    DB::Query($deleteQuery);
                                }
				catch (Exception $e)
				{

				}
			}
                    }
		}

                // Выполняем необходимые запросы к базе при удалении
		for ($i=0; $i<count($moduleInfo['sqluninstall']); $i++)
		{
			try
			{
				DB::Query($moduleInfo['sqluninstall'][$i]);
			}
			catch (SQLException $e)
			{
				$errors[] = "Ошибка во время выполнения запроса: <br />".$moduleInfo['sqluninstall'][$i]."<br />".$e -> getMessage();
			}
		}

		// Удаляем созданные модулем таблицы в обратном порядке (т.к. связанные таблицы могут вызвать ошибку)
                $moduleInfo['dbtables'] = array_reverse($moduleInfo['dbtables']);
		foreach ($moduleInfo['dbtables'] as $tableQuery)
		{
			$tableNameBracketPos = strpos($tableQuery, "(");
			$tableName = substr($tableQuery, 0, $tableNameBracketPos);

			$tableName = str_replace("CREATE TABLE IF NOT EXISTS", "", $tableName);
			$tableName = str_replace("CREATE TABLE", "", $tableName);
			$tableName = trim($tableName);
			DB::Query("DROP TABLE ".$tableName);
		}
	}

	public function GetModuleSettings($moduleName)
	{
		if (!$this -> isModuleInstalled($moduleName))
			throw new Exception("Указанный модуль не существует");

		$module = $this -> ModulesTable -> Select(array('name' => $moduleName));

		$moduleSettings = $this -> ModulesSettingsTable -> Select(array('moduleID' => $module[0]['moduleID']));

		$settings = array();

		foreach ($moduleSettings as $setting)
		{
			$settings[$setting['name']] = array('value' => $setting['value'],
							    'description' => $setting['description'],
                                                            'isVisible' => $setting['isVisible']
                                                           );
		}

		return $settings;
	}

	public function UpdateSetting($moduleName, $settingName, $settingValue)
	{
		$module = $this -> ModulesTable -> Select(array('name' => $moduleName));

		if (count($module) != 1)
			throw new Exception("Модуля ".$moduleName." не существует");

		$setting = $this -> ModulesSettingsTable -> Select(array('moduleID' => $module[0]['moduleID'], 'name' => $settingName));

		if (count($setting) != 1)
			throw new Exception("Настройки ".$settingName." в модуле ".$moduleName." не существует");

                //обновляем алиас для фронт-енда в странице и роуте
                if ($settingName == 'rootAlias'){
                    $this -> updatePageAndRoute($setting[0]['value'], $settingValue);

                    //очищаем кеш роутера, чтобы принять изменения
                    ObjectsCache::KillCache(RoutingTableDBProvider::cacheFileName);
                }

		$this -> ModulesSettingsTable -> Update(    array(	'name'	=> $settingName,
									'value'	=> $settingValue
								 ),
                                                            $setting[0]['settingID']
		);
	}

       /**
        * Получаем установленные модули
        *
        */
       public static function getActiveModules()
       {
           $ModulesTable = new DBTableManager(TablesNames::$MODULES_TABLE_NAME);
           return $ModulesTable -> Select(array('isVisible' => 1));
       }
}

?>
<?php
/**
 * Контрол для управления связями многие ко многим.
 * Имеет представление таблицы с заданными полями,
 * одно из которых имеет вид селекта.
 *
 * @version 3.4
 *
 */
class DependendTable extends Control implements IFlushingControl
{

    /**
     * Имя таблицы со значениями
     *
     * @var string
     */
    private $referentTableName = null;

    /**
     * Имя поля с человеко-понятным значением
     *
     * @var string
     */
    private $listingKey = null;

	/**
	 * Имя поля с изображением
	 */
	private $imageKey = null;

	/**
	 * Имя таблицы, хранящей множественные
	 *
	 * @var string
	 */
    private $linkingTableName = null;
	private $textFields = null;
    /**
	 * Порядок сортировки строк в таблице-справочнике,
	 * откуда будут браться опшены селекта.
	 *
	 * Подставляется в DBTableManager -> SortOrder
	 *
	 * @var string
	 */
	protected $orderBy			 = 'priority';

	public function InitControl($value = null, $rowID = null)
	{
        $this -> LoadSettings();
        $this->controlTplPath = parent::$CONTROLS_PATH . __CLASS__ . '/control_table.tpl';


		$this -> controlTplVars = array();
        $this -> controlTplVars['NAME'] = $this -> name;
        $this -> controlTplVars['NAMETABLE'] = $this->linkingTableName;
        $this -> controlTplVars['TIP'] = $this -> tip;

//        $this -> controlTplVars['tableName'] = $this->tableName;
		$this -> controlTplVars['formIndex'] = (isset($this->formIndex))?$this->formIndex:0;
//        $this->controlTplVars['listenName'] = $this->listenName;

		if ($this -> required) $this -> controlTplVars['REQUIRED'] = 1;
        if ($this -> disabled) $this -> controlTplVars['READONLY'] = 1;

		// Добавляем текстовые поля
		if (!empty($this->textFields))
		{
			$this->controlTplVars['textFields'] = $this->textFields;
		}

		$fields = array();
		$linkingTableStructure = new DBTableStructure($this->linkingTableName);
		// default values
		$key					 = null;
		FB::log($this -> textFields, "TF");
		foreach ($this->textFields as $fieldName => $field)
		{
			$f = array();
			$f['type'] = $field['containerType'];
			$f['name'] = $field['label'];
			if ($f['name'] == $linkingTableStructure->PrimaryKey->Name)
			{
				$key = $linkingTableStructure->PrimaryKey->Name;
			}
			if ($f['type'] == 'select')
			{
				// Вытаскиваем значения ReferentTable - это та таблица, в которой хранятся option'ы селекта
				$referentTableManager			 = new DBTableManager($field['referentTable']);
				$referentTableRows				 = $referentTableManager->Select();

				$cnt = sizeof($referentTableRows);
				if ($cnt == 0)
				{
					$this->controlHTML = 'no rows';
					$this->name = $this->name;
					$this->type = __CLASS__;
					$this->description = $this->description;
					return;
				}

				foreach ($referentTableRows as $items)
				{
					$d = array();
					$d['TEXT'] = $items[$field['listingKey']];
					$d['VALUE'] = $items[$fieldName];
					$f['data'][] = $d;
				}
			}
			$fields[$fieldName] = $f;
			FB::log($fields);
		}
		if (!$key)
		{
			$fields[$linkingTableStructure->PrimaryKey->Name] = array('name' => '', 'type' => 'hidden');
		}
		FB::log($fields,'fieldsUpd');
		// Связывающая таблица
        $selectedRows = array();
		$key = null;
		if ($rowID != null)
        {
			FB::log($this->orderBy, "order");
			$linkingTableManager = new DBTableManager($this->linkingTableName);
			$linkingTableManager->SortOrder	 = empty($this->orderBy) ? '"' . $this->listingKey . '"' : $this->orderBy;
			$linkingTableSelectedRows = $linkingTableManager->Select(array($this->controlsRegistry->activeTableStructure->PrimaryKey->Name => $rowID));
			if (sizeof($linkingTableSelectedRows) > 0)
			{
				foreach ($linkingTableSelectedRows as $key => $value)
				{
					foreach ($fields as $fieldName => $field)
					{
						$selectedRows[$key][$fieldName] = $value[$fieldName];
					}
				}
			}
			FB::log($selectedRows, "SR");
		}
		$this->controlTplVars['FIELDS'] = $fields;
		$this->controlTplVars['OPTIONS'] = $selectedRows;
		// Инициализирующая функция
		$this->controlJSFiles = array();
		//$this -> controlJSFiles[] = AppSettings::$LIBS_ALIAS.'jQuery/'.__CLASS__.'/jquery.damnUploader.min.js';
		$this->controlJSFiles[] = parent::$CONTROLS_ALIAS . __CLASS__ . '/init.js';
	    $this->controlJSVars['name'] = $this->name;
		$this->controlJSVars['libsPath'] = AppSettings::$LIBS_ALIAS;
		$this->controlJSVars['fields'] = $fields;

		$this->type = __CLASS__;

	}

    /**
     * Сюда приходит массив ID полей в ReferentTable
     *
     * @param array $data
     * @param int $rowID
     */
    public function Flush(&$data, $rowID)
    {
        $this -> LoadSettings();
        $linkingTableManager = new DBTableManager($this->linkingTableName);
		$linkingTableStructure	 = new DBTableStructure($this->linkingTableName);
//		FB::log($linkingTableStructure->Fields);
		$dataToInsert = array();
		$fields = array();
		if (!is_array($data))
		{
			FB::log("empty data");
			return;
		}
		$this->refreshFormAfterFlush = true;
//		FB::log($data, "data");
		foreach ($data as $field => $fdata)
		{
			if (!is_array($fdata))
			{
				continue;
			}
//			FB::log($fdata, $field);
			foreach ($fdata as $key => $val)
			{
				$fields[$key][$field] = $val;
			}
		}
//		FB::log($fields, "fields");
		$KeysToUpdate = array();
		foreach ($fields as $key => $field)
		{
			if(!isset($field['priority']) || empty($field['priority']))
			{
				$field['priority'] = $key + 1;
			}
//			FB::log($field);
			if (!empty($field[$linkingTableStructure->PrimaryKey->Name]))
			{
				$linkingTableManager->Update($field, $field[$linkingTableStructure->PrimaryKey->Name]);
				$KeysToUpdate[] = $field[$linkingTableStructure->PrimaryKey->Name];
			}
			else
			{
				$error = FALSE;
				foreach ($field as $f => $vall)
				{
					foreach($linkingTableStructure->Fields as $ffield)
					{
						if(empty($vall) && $f != $linkingTableStructure->PrimaryKey->Name && $ffield->Name == $f && $ffield->IsNotNull == 1)
						{
							//						$error = true;
							throw new ArgumentException("состав: Не все поля заполнены. $f");
						}
					}
				}
				if ($error)
				{
					continue;
				}
				$field[$this->controlsRegistry->activeTableStructure->PrimaryKey->Name] = $rowID;
				$KeysToUpdate[] = $linkingTableManager->Insert($field);
			}
		}
		if(!empty($KeysToUpdate))
		{
			$query = "DELETE FROM {$this->linkingTableName} WHERE {$this->controlsRegistry->activeTableStructure->PrimaryKey->Name} = {$rowID} AND {$linkingTableStructure->PrimaryKey->Name} NOT IN (" . implode(",", $KeysToUpdate) . ")";
//			FB::log($query, "delete");
			DB::Query($query);
		}
		else
		{
			$query = "DELETE FROM {$this->linkingTableName} WHERE {$this->controlsRegistry->activeTableStructure->PrimaryKey->Name} = {$rowID} ";
//			FB::log($query, "delete");
			DB::Query($query);
		}

//        $referentTableStructure = new DBTableStructure($this -> referentTableName);
//		$linkingTableSelectedRows = $linkingTableManager->Select(array($this->controlsRegistry->activeTableStructure->PrimaryKey->Name => $rowID));
//		FB::log($linkingTableSelectedRows);
		// Удалим все текущие записи.
        // Это довольно опасная операция... Надо все тщательно проверить :)
//        if ($rowID == null || !is_numeric($rowID) || $rowID < 1)
//            throw new ArgumentException("Неверно задан rowID -> $rowID");
//
//        $linkingTableManager -> Delete(
//            array(
//                $this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID
//            ), true
//        );
//
//        // Теперь добавим новые записи
//        foreach ($data as $referentID)
//        {
//            if (is_numeric($referentID) && $referentID > 0)
//                $linkingTableManager -> Insert(
//                    array(
//                        $this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID,
//                        $referentTableStructure -> PrimaryKey -> Name => $referentID
//                    )
//                );
//        }
	}

    public function PostBackHandler(&$data, $rowID = null) {}

    protected function LoadSettings()
    {
		$errmsg = $this -> name. ' is undefined. It should be defined in FieldsRegistry';
//        if (!empty($this -> controlSettings['referentTable']))
//            $this -> referentTableName = $this -> controlSettings['referentTable'];
//        else
//            throw new Exception('Referent Table for ' . $errmsg);
//        if (!empty($this -> controlSettings['listingKey']))
//            $this -> listingKey = $this -> controlSettings['listingKey'];
//        else
//            throw new Exception('listing Key for ' . $errmsg);

		if (!empty($this -> controlSettings['linkingTable']))
            $this -> linkingTableName = $this -> controlSettings['linkingTable'];
        else
            throw new Exception('linking Table for ' . $errmsg);

        if (!empty($this -> controlSettings['controlType']))
            $this->controlType = $this->controlSettings['controlType'];

		if (isset($this->controlSettings['textFields']))
		{
			$this->textFields = $this->controlSettings['textFields'];

			foreach ($this->textFields as $key => $value)
			{
				if (empty($this->textFields[$key]['label']))
					$this->textFields[$key]['label'] = 'Label ' . $key;

				if (!isset($this->textFields[$key]['showLabel']))
					$this->textFields[$key]['showLabel'] = true;
			}
		}

		if (isset($this->controlSettings['listenName']))
			$this->listenName = $this->controlSettings['listenName'];
        if(!empty($this->controlSettings['orderBy']))
			$this->orderBy	 = $this->controlSettings['orderBy'];
	}

}
?>
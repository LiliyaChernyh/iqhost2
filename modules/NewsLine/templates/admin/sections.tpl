<h1>Категории</h1>

<form action="{$MODULE_ALIAS}sections/" method="post">
    <ul>
        <li style="background-image:none;">
            <div class="formElement">
                <label class="formElementLabel" for="sectionID">
                   Список категорий
                </label>
                <select id="sections" multiple name="sections[]">
                    {foreach from=$SECTIONS item=SECTION}
                       <option value="{$SECTION.sectionID}">{$SECTION.caption}</option>
                    {/foreach}
                </select>

                <br />

                <input class="btn" type="submit" name="doDel" value="Удалить" />
            </div>
        </li>
        <li style="background-image:none;">
            <div class="formElement">
                <label class="formElementLabel" for="sectionID">
                   Категория
                   <span style="color: red;">*</span>
                </label>
                <input type="text" name="section" id="section" />
                <input class="btn" type="submit" name="doAdd" value="Добавить" alt="" />
                <input class="btn" type="submit" name="doSave" value="Сохранить" alt="" disabled="disabled"/>
            </div>
        </li>
    </ul>
    <input type="hidden" name="messageID" value="" alt="">
</form>
{include file='top.tpl'}
{include file='invisible-stadart-blocks.tpl'}

<div id="container">

	<div class="w1">
		{include file='header.tpl'}

    <br class="clear" />

		<div id="contentwrapper">
			<div id="content">
				{if isset($CONTENT_TPL)}{include file="$CONTENT_TPL"}
				{else}
				{if isset($CONTENT)}{$CONTENT}{/if}
				{/if}

			</div>
    </div>

				<div id="sidebar">
					{include file='navigation.tpl'}
					{*				{if isset($MENU_TPL)}{include file="$MENU_TPL"}{/if}*}
					{*		{if isset($MENU_TPL)}{include file="menu.tpl"}{/if}*}

		</div>
	</div>
</div>

		{include file='footer.tpl'}
		{if !empty($_MODAL_TPL)}
			{foreach from=$_MODAL_TPL item=modaltpl}
				{include file="modals/$modaltpl.tpl"}
			{/foreach}
		{/if}
</body>
</html>
<?php
/**
 * Результат работы контроллера в виде редиректа на другой адрес
 * 
 * @version 1.0
 *
 */
class RedirectActionResult extends ActionResult 
{
    /**
     * Путь редиректа
     *
     * @var string
     */
    private $redirectTo;

    public function __construct($redirectTo)
    {
       $this -> redirectTo = $redirectTo;
    }

    public function ExecuteResult()
    {
        if (!empty($this -> redirectTo))
        {
            if (!headers_sent())
                header('Location: '.$this -> redirectTo);
        }
        else 
            throw new Exception('Не задан путь редиректа');
    }
}
?>
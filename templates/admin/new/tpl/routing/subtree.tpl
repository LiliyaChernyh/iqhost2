<ul class="routing-tree">
{foreach from=$SUBTREE item=ITEM}
	<li>{*{$CONTROLLER_ALIAS}edit/*}
		{*section name=sect loop=$ITEM.level}-{/section*}
		<span>
			<a href="{$CONTROLLER_ALIAS}edit/{$ITEM.routeID}/" class="branch">{if empty($ITEM.pattern)}(Пустая строка){else}{$ITEM.pattern}{/if}</a>

		</span>
			<a href="{$CONTROLLER_ALIAS}add/?after={$ITEM.routeID}" class="add_"><img src="{$ADMIN_IMAGES}add.png" title="Добавить сюда" /></a>
			<a href="{$CONTROLLER_ALIAS}delete/{$ITEM.routeID}/" class="delete"><img src="{$ADMIN_IMAGES}delete.png" title="Удалить" /></a>
			{if !empty($ITEM.children)}
		{include file='routing/subtree.tpl' SUBTREE=$ITEM.children}
	{/if}
	</li>

{/foreach}
</ul>
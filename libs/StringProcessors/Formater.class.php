<?php

/**
 * Этот класс умеет по-всякому форматировать строки
 *
 */
class Formater
{
	/**
	 * Переформатирует размер файла (в байтах)
	 *
	 * @param int $size
	 * @param int $precision
	 * @return array
	 */
	public static function FileSize($size, $precision = null)
	{
		if ($precision == null) $precision = 2;
		
		$sizeRange = ceil(strlen($size) / 3) - 1;
		
		switch ($sizeRange)
		{
			case 0:
				$measure = "байт";
				break;
				
			case 1:
				$measure = "Кб";
				break;
				
			case 2:
				$measure = "Мб";
				break;
				
			case 3:
				$measure = "Гб";
				break;
		}
		
		return array
		(
			'size'		=> round($size/pow(1024, $sizeRange), $precision),
			'measure'	=> $measure
		);
	}
}

?>
<?php
/**

 * Управление структурой в виде контроллера
 *
 * @author George
 * @version 1.6
 *
 * @invokes StructureController.WelcomePageInit
 * @invokes StructureController.DirectAccess.Begin
 * @invokes StructureController.AddPage.Begin
 * @invokes StructureController.AddPage.DataFlushed
 * @invokes StructureController.AddPage.End
 * @invokes StructureController.PageEdit.Begin
 * @invokes StructureController.PageEdit.DataFlushed
 * @invokes StructureController.PageEdit.End
 * @invokes StructureController.PageRecycling
 * @invokes StructureController.PageRestored
 * @invokes StructureController.PageWiping
 * @invokes StructureController.PageClonning
 *
 */
class StructureController extends AdminController
{
	/**
     * Событие начала добавления страницы. При получении POST запроса
     * @var string
     */
    const EVENT_ADDPAGE_BEGIN        = 'StructureController.AddPage.Begin';

    /**
     * Событие после записи связанных со страницей данных.
     * @var string
     */
    const EVENT_ADDPAGE_DATAFLUSHED  = 'StructureController.AddPage.DataFlushed';

    /**
     * Событие окончания добавления страницы. Все данные записаны
     * @var string
     */
    const EVENT_ADDPAGE_END          = 'StructureController.AddPage.End';

    /**
     * Событие начала редактирования страницы. При получении POST запроса
     * @var string
     */
    const EVENT_PAGEEDIT_BEGIN       = 'StructureController.PageEdit.Begin';

    /**
     * Событие после записи связанных со страницей данных.
     * @var string
     */
    const EVENT_PAGEEDIT_DATAFLUSHED = 'StructureController.PageEdit.DataFlushed';

    /**
     * Событие окончания редактирования страницы. Все данные записаны
     * @var string
     */
    const EVENT_PAGEEDIT_END         = 'StructureController.PageEdit.End';

    /**
     * Событие начала удаления страницы
     * @var string
     */
    const EVENT_PAGE_RECYCLING       = 'StructureController.PageRecycling';

    /**
     * Событие окончания восстановления страницы из корзины
     * @var string
     */
    const EVENT_PAGE_RESTORED        = 'StructureController.PageRestored';

    /**
     * Событие удаления страницы навсегда.
     * @var string
     */
    const EVENT_PAGE_WIPING          = 'StructureController.PageWiping';

	/**
     * Событие вывода страницы с приветсвенным контентом.
     * @var string
     */
    const WELCOME_PAGE_INIT          = 'StructureController.WelcomePageInit';

	/**
     * Событие вызова DirectAccess.
     * @var string
     */
    const DIRECT_ACCESS_BEGIN        = 'StructureController.DirectAccess.Begin';

	/**
     * Событие клонирования страницы (до).
     * @var string
     */
    const CLONNING_BEGIN        = 'StructureController.PageClonning';

    /**
     * Структура страниц
     *
     * @var PagesStructure
     */
    private $struct = null;

    /**
     * Информация об обрабатываемой странице
     *
     * @var array
     */
    private $currentPage;

    /**
     * Флаг, включающий управление главным меню сайта
     *
     * @var bool
     */
    public static $enableMainMenuControl = true;

    /**
     * Создаем объект структуры, задаем путь к шаблонами,
     * алиас секции и вызываем родительский конструктор
     *
     */
    public function __construct()
    {
        // Вызываем конструктор родительского класса
        parent::__construct();
    }

    private function InitView()
    {
    	// Создаем объект структуры страниц
        $this -> struct = PagesStructure::getInstance();

        // Создаем представление
        $this -> view = new StructureView();

        // Передаем текущий action в представление
        $this -> view -> SetAction(Application::GetApplication() -> GetRoute() -> GetActionName());

         // Переменные, нужные во всех action
        ViewData::Assign('DELETED_PAGES_NUMBER', $this -> struct -> Count(array('isDeleted' => 1)));
    }

    /**
     * ACTION
     */
	public function Index($args = null)
	{
		$this -> InitView();
		$dispatcher = EventDispatcher::getInstance();
		$dispatcher -> Invoke(self::WELCOME_PAGE_INIT);

		$pagesTree = $this -> struct -> GetPages(array('parentID' => 0, 'isDeleted' => 0));
		//$pagesTree = $this -> struct -> GetPagesTree();
		//$pagesTree = $this -> struct -> GetPages(array('parentID' => 0));
		ViewData::Assign('PAGES_TREE', $pagesTree);
		ViewData::Assign("OLD_JQUERY", 1);

		return $this -> view;
	}

	/**
	 * AJAX метод для подгрузки дочерних элементов
	 *
	 * @param array $args
	 */
	public function GetSubPagesList($args = null)
	{
	    try
        {
            if (empty(Request::$POST['pageID']))
                throw new Exception('Не задан ID страницы');

            $pageID = Request::$POST['pageID'];

            $this -> struct = PagesStructure::getInstance();
            $children = $this -> struct -> GetPages(array('parentID' => $pageID, 'isDeleted' => 0));

            $response = array(
                'status'    => 'success',
                'pages'     => $children,
                'pageID'    => $pageID
            );

    		return new JsonActionResult($response);
        }
        catch (Exception $e)
        {
            return new JsonActionResult(array('error' => $e -> getMessage(), 'errno' => $e -> getCode()));
        }
	}

	public function ReorderPages($args = null)
	{
	    try
        {
            if (empty(Request::$POST['pageID']))
                throw new Exception('Не задан ID страницы');

            $pageID = Request::$POST['pageID'];

            if (!isset(Request::$POST['after']))
                throw new Exception('Не задан ID предыдущей страницы');

            $afterPageID = Request::$POST['after'];

            $this -> struct = PagesStructure::getInstance();

            // Получим редактируемую страницу
            $editingPage = new Page($pageID);

            $levelPages = $this -> struct -> GetPages(array('parentID' => $editingPage -> parentID));

            $levelPagesReverse = array_reverse($levelPages);
            $levelPagesReverseCnt = count($levelPagesReverse);

            // Если надо поставить страницу первой,
            // устанавливаем ей порядок на 20 больше максимального
            if ($afterPageID == 0)
            {
                $priority = $levelPages[0]['priority'] + 20;
                $editingPage -> priority = $priority;
                $editingPage -> Save();
            }
            else
            {
                $levelPagesCnt = count($levelPages);
                $afterPageKey = null;
                for ($i = 0; $i < $levelPagesCnt; $i++)
                {
                    if ($levelPages[$i]['pageID'] == $afterPageID)
                    {
                        $afterPageKey = $i;
                        break;
                    }
                }
                $beforePageKey = $afterPageKey + 1;

                if (!isset($levelPages[$beforePageKey]))
                {
                    if ($levelPages[$afterPageKey]['priority'] > 1)
                    {
                        $editingPage -> priority = floor($levelPages[$afterPageKey]['priority'] / 2);
                        $editingPage -> Save();
                    }
                    else
                    {
                        $this -> struct -> SetNewPriorityInterval($levelPages[$afterPageKey]['pageID']);

                        $afterPage = new Page($afterPageID);
                        $editingPage -> priority = floor($afterPage -> priority / 2);
                        $editingPage -> Save();
                    }
                }
                else
                {
                    if ($levelPages[$afterPageKey]['priority'] - $levelPages[$beforePageKey]['priority'] > 1)
                    {
                        $editingPage -> priority = floor(($levelPages[$afterPageKey]['priority'] + $levelPages[$beforePageKey]['priority']) / 2);
                        $editingPage -> Save();
                    }
                    else
                    {
                        $this -> struct -> SetNewPriorityInterval($levelPages[$beforePageKey]['pageID']);

                        $afterPage = new Page($afterPageID);
                        $beforePage = new Page($levelPages[$beforePageKey]['pageID']);
                        $editingPage -> priority = floor(($afterPage -> priority + $beforePage -> priority) / 2);
                        $editingPage -> Save();
                    }
                }
            }

            $response = array(
                'status'    => 'success',
                'msg'       => 'Порядок изменен'
            );

    		return new JsonActionResult($response);
        }
        catch (Exception $e)
        {
            return new JsonActionResult(array('error' => $e -> getMessage(), 'errno' => $e -> getCode()));
        }
	}

	/**
	 * ACTION
	 * @return ActionResult
	 */
	public function DirectAccess()
    {
		$dispatcher = EventDispatcher::getInstance();
		$dispatcher -> Invoke(self::DIRECT_ACCESS_BEGIN);

        $path = Request::$POST['directAccess'];
        $path = str_replace($_SERVER['HTTP_HOST'].'/', '', $path);
        $path = str_replace('http://', '', $path);
		$questMark = strpos($path, '?');
		if ($questMark != false)
			$path = substr($path, 0, $questMark);


        // Создаем объект структуры страниц
	    $this -> struct = PagesStructure::getInstance();
        $page = $this -> struct -> GetPageByFullAlias($path);
        if (empty($page))

            return new AdminErrorView('Не получилось открыть, указанную вами страницу, для редактирования.', AppSettings::$ADMIN_ALIAS);
        else
        {
            return new RedirectActionResult($this -> controllerAlias . 'edit/'.$page -> pageID.'/');
        }
    }

    /**
     * ACTION

     * Добавление новой cтраницы
     *
     */
    public function Add()
    {
        if (isset(Request::$POST['doAdd']))
        {
			$dispatcher = EventDispatcher::getInstance();

        	// Создаем объект структуры страниц
	        $this -> struct = PagesStructure::getInstance();
			$dispatcher -> Invoke(self::EVENT_ADDPAGE_BEGIN);

            // Раздеяем атрибуты страницы и атрибуты контента
            $pageAttrs = $this -> PreparePageAttrs();

        	if (empty($pageAttrs['alias']) || empty($pageAttrs['caption']))
            {
                if (Request::IsAsync())
                    return new JsonActionResult(array('error' => 'Не заданы необходимые параметры'));
                return $this -> Error('Не заданы необходимые параметры', 'back');
            }

            // Проверка уникальности алиаса
            $pageWithTheSameAlias = $this -> struct -> GetPage(array('parentID' => $pageAttrs['parentID'], 'alias' => $pageAttrs['alias']));
            if (!empty($pageWithTheSameAlias))
            {
                $err = 'Страница с таким псевдо-URL '. $pageAttrs['alias'] .' уже существует';
                if (Request::IsAsync())
                    return new JsonActionResult(array('error' => $err ));
                return $this -> Error($err , 'back');
            }

            $inMainMenu = false;
            if (isset(Request::$POST['showInMainMenu']))
            {
                if (Request::$POST['showInMainMenu'])
                    $inMainMenu = true;
                unset(Request::$POST['showInMainMenu']);
            }

            $contentManager = new ContentManager();
            $pageAttrs['contentID'] = 0;
            $pageTypeRow = null;
            $pageID = 0;
            try
            {
                if ($pageAttrs['pageTypeID'] > 0)
                {

                    // Получим информацию о типе содержимого
                    $typesTable = new DBTableManager(TablesNames::$PAGES_TYPE_TABLE_NAME, null, false);
                    if (is_numeric($pageAttrs['pageTypeID']))
                    {
                        $pageTypeRow = $typesTable -> SelectFirst(array('pageTypeID' => $pageAttrs['pageTypeID']));
                        if (empty($pageTypeRow))

                            throw new Exception('Не существует указанного типа. ID не существует.');
                    }

                    if ($pageTypeRow['handlerType'] == 'table')
                    {
                        $pageAttrs['contentID'] = $contentManager -> FlushData($pageTypeRow['name'], Request::$POST);
                        $dispatcher -> Invoke(self::EVENT_ADDPAGE_DATAFLUSHED);
                    }
                }
            }
            catch (Exception $e)
            {
				DBLog::Error('Error creating page: ' . $e -> getMessage());
                if (Request::IsAsync())
                	return new JsonActionResult(array('error' => 'Возникла ошибка во время создания заготовки контента '. $e -> getMessage()));
                return $this -> Error('Возникла ошибка во время создания заготовки контента.<br />' . $e -> getMessage(), $_SERVER['HTTP_REFERER']);
            }
            try
            {
                $pageID = $this -> struct -> AddPage($pageAttrs);
            }
            catch (Exception $e)
            {
				DBLog::Error('Error creating new page: ' . $e -> getMessage());
                if (Request::IsAsync())
                	return new JsonActionResult(array('error' => 'Возникла ошибка во время создания страницы '.$e -> getMessage()));
                return $this -> Error('Возникла ошибка во время создания страницы.<br />' . $e -> getMessage(), $_SERVER['HTTP_REFERER']);
            }

            $dispatcher -> Invoke(self::EVENT_ADDPAGE_END, $pageID);
            if (self::$enableMainMenuControl)
            {
                $mainMenuMngr = new MainMenuManager();
                if ($inMainMenu)
                {
                    $mainMenuMngr -> AddPageToMainMenu($pageID);
                }
                else if ($mainMenuMngr -> IsPageInMainMenu($pageID))
                {
                    $mainMenuMngr -> RemovePageFormMainMenu($pageID);
                }
            }


            // Если не задан тип или если задан
            if ($pageTypeRow['handlerType'] == 'table' || $pageTypeRow == null)
            {
				DBLog::Info('New page created. pageID = %d', $pageID);
                if (Request::IsAsync())
                	return new JsonActionResult(array(
                									'msg' => 'ok',
                									'pageID' => $pageID,
                									'refresh' => ($contentManager -> formRefreshRequired ? 'true':'false')
                								));
                return $this -> Success('Страница создана.', $this -> controllerAlias . 'edit/'.$pageID.'/');
            }
            elseif ($pageTypeRow['handlerType'] == 'module')
            {
                $moduleNameArr = explode('.', $pageTypeRow['name']);
                $moduleName = $moduleNameArr[0];
                if (Request::IsAsync())
                	return new JsonActionResult(array(
                									'msg' => 'ok',
                									'pageID' => $pageID
                								));
                return $this -> Success('Страница создана.', AppSettings::$ADMIN_ALIAS."$moduleName/guide/".$attr['pageID'].'/');
            }
            else
            {
				DBLog::Error('Error creating new page: Unspecified handlerType');
                if (Request::IsAsync())
                	return new JsonActionResult(array('error' => 'Неопределённый handlerType'));
                return $this -> Error('Неопределённый handlerType', $_SERVER['HTTP_REFERER']);
            }
        }

        $this -> InitView();

        if (self::$enableMainMenuControl)
            ViewData::Assign('SHOW_MAINMENU_CHECKBOX', 1);

        ViewData::Assign('PARENT_OPTION', $this -> PossibleParentOption());
        ViewData::Assign('PAGE_TYPES_OPTION', $this -> GetPageTypesOption(true));

        return $this -> view;
    }

	/**
     * ACTION

     * Редактирование cтраницы
     *
     */
    public function Edit($args = null)
    {
        if (isset(Request::$POST['doEdit']))
        {
            if (empty(Request::$POST['pageID']))
                throw new ArgumentException('Не задан идентификатор страницы');

            $dispatcher = EventDispatcher::getInstance();

            // Получаем идентификатор страницы
            $pageID = Request::$POST['pageID'];

            $dispatcher -> Invoke(self::EVENT_PAGEEDIT_BEGIN, $pageID);

            // Создаем объект структуры страниц
            $this -> struct = PagesStructure::getInstance();

            // Раздеяем атрибуты страницы и атрибуты контента
            $pageAttrs = $this -> PreparePageAttrs();

            if (empty($pageAttrs['alias']) || empty($pageAttrs['caption']))
            {
                if (Request::IsAsync())
                    return new JsonActionResult(array('error' => 'Не заданы необходимые параметры'));
                return $this -> Error('Не заданы необходимые параметры', 'back');
            }

            // Проверка уникальности алиаса
            $pagesWithTheSameAlias = $this -> struct -> GetPages(array('parentID' => $pageAttrs['parentID'], 'alias' => $pageAttrs['alias']));
            $pagesCnt = count($pagesWithTheSameAlias);
            if ($pagesCnt > 0 && $pagesWithTheSameAlias[0]['pageID'] != $pageID)
            {
                $err = 'Страница с таким псевдо-URL '. $pageAttrs['alias'] .'уже существует';
				DBLog::Error('Error editing page (pageID=%d)' . $err, $pageID);
                if (Request::IsAsync())
                    return new JsonActionResult(array('error' => $err ));
                return $this -> Error($err , 'back');
            }

       		if (self::$enableMainMenuControl)
            {
                $mainMenuMngr = new MainMenuManager();
                if (isset(Request::$POST['showInMainMenu']) && Request::$POST['showInMainMenu'])
                {
                    unset(Request::$POST['showInMainMenu']);
                    $mainMenuMngr -> AddPageToMainMenu($pageID);
                }
                else if ($mainMenuMngr -> IsPageInMainMenu($pageID))
                {
                    $mainMenuMngr -> RemovePageFormMainMenu($pageID);
                }
            }

        	$pageTypeRow = null;

        	// Смотрим не изменили ли PageTypeID
            $page = $this -> struct -> GetPageByID($pageID);
            $page = $page -> ToArray();
            $contentTypeHasNotBeenChosenYet = ($page['pageTypeID'] == 0) && ($pageAttrs['pageTypeID'] != 0);
            $contentManager = new ContentManager();

            $pageTypeID = 0;

            // Если не был задан contentTypeID и если его вдруг задали
            if ($contentTypeHasNotBeenChosenYet)
            {
                $pageTypeID = $pageAttrs['pageTypeID'];
            }
            elseif ($page['pageTypeID'] != 0)
            {
                $pageTypeID = $page['pageTypeID'];
            }

            try
            {
                if ($pageTypeID > 0)
                {

                    // Получим информацию о типе содержимого
                    $typesTable = new DBTableManager(TablesNames::$PAGES_TYPE_TABLE_NAME, null, false);
                    if (is_numeric($pageTypeID))
                    {
                        $pageTypeRow = $typesTable -> SelectFirst(array('pageTypeID' => $pageTypeID));
                        if (empty($pageTypeRow))

                            throw new Exception('Не существует указанного типа. ID не существует.');
                    }

                    if ($pageTypeRow['handlerType'] == 'table')
                    {
                        if ($contentTypeHasNotBeenChosenYet)
                            $pageAttrs['contentID'] = $contentManager -> FlushData($pageTypeRow['name'], Request::$POST);
                        else
                            $contentManager -> FlushData($pageTypeRow['name'], Request::$POST);
                        if (!$contentManager -> isSuccess)
                            throw new Exception($contentManager -> error);
                        $dispatcher -> Invoke(self::EVENT_PAGEEDIT_DATAFLUSHED, $pageID);
                    }
                }
            }
            catch (Exception $e)
            {
                DBLog::Error('Error editing page (pageID=%d): ' . $e->getMessage(), $pageID);
                if (Request::IsAsync())
                	return new JsonActionResult(array('error' => 'Возникла ошибка во время изменения содержимого '. $e -> getMessage(), 'errno' => $e->getCode()));
                $this -> Error('Возникла ошибка во время изменения содержимого.<br />' . $e -> getMessage(), $_SERVER['HTTP_REFERER']);
            }

            try
            {
                $this -> struct -> EditPage($pageID, $pageAttrs);
                $dispatcher -> Invoke(self::EVENT_PAGEEDIT_END, $pageID);
                if ($contentTypeHasNotBeenChosenYet && $pageAttrs['pageTypeID'] > 0 && $pageTypeRow != null && $pageTypeRow['handlerType'] == 'module')
                {
                    $moduleNameArr = explode('.', $pageTypeRow['name']);
                    $moduleName = $moduleNameArr[0];
                    $redirect = AppSettings::$ADMIN_ALIAS . $moduleName .'/guide/'. $pageTypeRow['name']. '/' .$pageID .'/';
                }
                else
                {
                    $redirect = $this -> controllerAlias . 'view/' .$pageID .'/';
                }

				DBLog::Info('Page edit success (pageID=%d)', $pageID);
                if (Request::IsAsync())
				{
					return new JsonActionResult(array(
													'msg' => 'ok',
													'refresh' => $contentManager -> formRefreshRequired
												));
				}


                return $this -> Success('Редактирование прошло успешно!', $redirect);
            }
            catch (Exception $e)
            {
				DBLog::Error('Error editing page (pageID=%d): ' . $e -> getMessage(), $pageID);
                if (Request::IsAsync())
                	return new JsonActionResult(array('error' => 'Возникла ошибка во время редактирования страницы '.$e -> getMessage()));
                return $this -> Error('Возникла ошибка во время редактирования страницы.<br />' . $e -> getMessage(), $_SERVER['HTTP_REFERER']);
            }
        }

        $this -> InitView();

        // Получаем иденификатор страницы
        if (empty($pageID) && !empty($args))
            $pageID = $args[0];

        if (empty($pageID))
            return new AdminErrorView('Не задан идентификатор страницы');

        // Получаем параметры запрашиваемой страницы
        $fullAlias = null;

        $requestedPage = $this -> struct -> GetPage(array('pageID' => $pageID)) -> ToArray();
        $fullAlias = $this -> struct -> GetFullAlias($pageID);
        if (empty($requestedPage))
            return $this -> Error('Страница не найдена', $this -> controllerAlias);

        // Заполняем шаблон
        ViewData::Assign('PAGE', $requestedPage);

        // Если до сих пор не указан тип, то нет и содержимого, значит не показываем 'Редактировать содержимое' и даём возможность выбрать тип
        if ($requestedPage['pageTypeID'] != 0)
        {
            //Получим название таблицы
            $tableName = null;
            $typesTable = new DBTableManager(TablesNames::$PAGES_TYPE_TABLE_NAME, null, false);
            $pageTypeRow = $typesTable -> SelectFirst(array('pageTypeID' => $requestedPage['pageTypeID']));
            if (empty($pageTypeRow))

                return $this -> Error('Не найден тип содержимого. Обратитесь к разработчикам и опишите подробно как возникла эта ошибка.', $this -> redirectURL);

            switch ($pageTypeRow['handlerType'])
            {
                case 'table':
                    // Сгенерим необходимые элементы управления
                    $formGenerator = $this -> view -> AssignFormToTpl($pageTypeRow['name'], $requestedPage['contentID']);

                    // Флаг обновления формы
                    ViewData::Append('JS_VARS', array('REFRESH_AFTER_FLUSH' => ($formGenerator -> formRefreshRequired) ? 1 : 0));
                    if ($requestedPage['contentID'] > 0)
                        ViewData::Assign('DISABLE_PAGE_TYPES_OPTION', 1);
                    break;

                case 'module':
                    $moduleNameArr = explode('.', $pageTypeRow['name']);
                    $moduleName = $moduleNameArr[0];
                    $moduleLink = AppSettings::$ADMIN_ALIAS . $moduleName .'/guide/'. $requestedPage['pageID'] .'/';
                    ViewData::Assign('MODULE_LINK', $moduleLink);
                    ViewData::Assign('HIDE_CONTENT_FORM', 1);
                    ViewData::Assign('DISABLE_PAGE_TYPES_OPTION', 1);
                    break;
            }
        }
        else
        {
            // нужно убрать пустой блок с содержимым, так как тип пока не задан
            ViewData::Assign('HIDE_CONTENT_FORM', 1);
        }

   		if (self::$enableMainMenuControl)
        {
            $mainMenuMngr = new MainMenuManager();
            ViewData::Assign('SHOW_PAGE_IN_MAIN_MENU', $mainMenuMngr -> IsPageInMainMenu($pageID) ? 1 : 0);
            ViewData::Assign('SHOW_MAINMENU_CHECKBOX', 1);
        }

        ViewData::Assign('HEADER', "Редактирование параметров страницы<br /><small>[ <a href=\"$fullAlias\" class=\"openInNewWin\" style=\"text-decoration:underline\">$requestedPage[caption]</a> ]</small>");
        ViewData::Assign('DELETE_SCRIPT', $this -> controllerAlias .'delete/' .$pageID .'/');
        ViewData::Assign('PARENT_OPTION', $this -> PossibleParentOption($pageID));
        ViewData::Assign('PAGE_TYPES_OPTION', $this -> GetPageTypesOption(empty($requestedPage['pageTypeID']) ? true : false));

        return $this -> view;
    }

	private function PreparePageAttrs()
	{
		$pageAttrs = array();
		foreach (Request::$POST as $key => $value)
		{
			if (strpos($key, 'page_') !== false)
			{
				// Вырезаем page_
				$pageAttrs[substr($key, 5)] = $value;
				unset(Request::$POST[$key]);
			}
		}

		$pageAttrs['alias'] = UTF8::trim($pageAttrs['alias']);
		$pageAttrs['caption'] = UTF8::trim($pageAttrs['caption']);

		// Если не задан алиас, то возьмём его из названия
		if (empty($pageAttrs['alias']) && !empty($pageAttrs['caption']))
			$pageAttrs['alias'] = $pageAttrs['caption'];

		$pageAttrs['alias'] = Transliter::TranslitToStrictName(UTF8::strtolower($pageAttrs['alias']));
		$pageAttrs['caption'] = htmlspecialchars($pageAttrs['caption']);
		$pageAttrs['title'] = UTF8::trim(htmlspecialchars($pageAttrs['title']));
		$pageAttrs['description'] = UTF8::trim(htmlspecialchars($pageAttrs['description']));
		$pageAttrs['keywords'] = UTF8::trim(htmlspecialchars($pageAttrs['keywords']));

		if (isset($pageAttrs['caption_en']))
			$pageAttrs['caption_en'] = UTF8::trim(htmlspecialchars($pageAttrs['caption_en']));
		if (isset($pageAttrs['title_en']))
			$pageAttrs['title_en'] = UTF8::trim(htmlspecialchars($pageAttrs['title_en']));
		if (isset($pageAttrs['description_en']))
			$pageAttrs['description_en'] = UTF8::trim(htmlspecialchars($pageAttrs['description_en']));
		if (isset($pageAttrs['keywords_en']))
			$pageAttrs['keywords_en'] = UTF8::trim(htmlspecialchars($pageAttrs['keywords_en']));

		return $pageAttrs;
	}

    /**
     *  Action
     *  Клонирование
     */
    public function ClonePage($args = null)
    {
        if (empty($args) || empty($args[0]) || !is_numeric($args[0]))
            return $this -> Error('Не задан идентификатор страницы');
        $pageID = $args[0];
        try
        {
            $dispatcher = EventDispatcher::getInstance();
            $dispatcher -> Invoke(self::CLONNING_BEGIN, $pageID);

            $this -> struct = PagesStructure::getInstance();
            $newPageID = $this -> struct -> ClonePage($pageID, true);
            return $this -> Success('Страница успешно склонирована. Нажмите ok, чтобы переидти к редактированию копии', $this -> controllerAlias . 'edit/'.$newPageID.'/');
        }
        catch (Exception $e)
        {
            return $this -> Error('Возникла ошибка во время клонирования страницы.<br />' . $e -> getMessage(), $_SERVER['HTTP_REFERER']);
        }
    }

	/**
	 * AJAX Action

     * Получение формы редактирования
     *
     */
    public function GetEditContentForm()
    {
        try
        {
            if (!isset(Request::$POST['pageTypeID']) || Request::$POST['pageTypeID'] == 0)
               throw new Exception('Не задан тип страницы');

            $pageTypeID = Request::$POST['pageTypeID'];

            $typesTable = new DBTableManager(TablesNames::$PAGES_TYPE_TABLE_NAME, null, false);
            $Qresult = $typesTable -> Select(array('pageTypeID' => $pageTypeID));

            if (count($Qresult) == 0)
                throw new Exception('Не найден тип содержимого. Обратитесь к разработчикам и опишите подробно как возникла эта ошибка.');
            if ($Qresult[0]['handlerType'] != 'table')
                return new JsonActionResult(array('contentType' => 'module'));

            $response = array();
            $response['contentType'] = 'page';
            $tableName = $Qresult[0]['name'];

            // Сгенерим необходимые элементы управления
            $formGenerator = new FormGenerator();
            $controls = $formGenerator -> generateForm($tableName);

            // Если нет controlHTML то надо отпарсить шаблон
            $template = new Template('');
            for ($i = 0; $i < count($controls); $i++)
            {
                for ($j = 0; $j < count($controls[$i]['controls']); $j++)
                {
                    if (empty($controls[$i]['controls'][$j]['controlHTML']) &&
                      !empty($controls[$i]['controls'][$j]['controlTplPath']))
                      {
                          $template -> tplPath = $controls[$i]['controls'][$j]['controlTplPath'];
                          $template -> assign('localVars', $controls[$i]['controls'][$j]['controlTplVars']);
                          $controls[$i]['controls'][$j]['controlHTML'] = $template -> Parse();
                      }
                }
            }
            $response['formBlocks'] = $controls;
            $response['includingJSFiles'] = $formGenerator -> includingJSFiles;
            $response['includingJSLibs'] = $formGenerator -> includingJSLibs;

            return new JsonActionResult($response);
        }
        catch (Exception $e)
        {
            return new JsonActionResult(array('error' => $e -> getMessage(), 'errno' => $e -> getCode()));
        }
    }

    /**
     * ACTION
     * Возвращает дерево страниц
     *
     * @param string $hrefPrefix
     */
    public function GetStructure()
    {
    	$hrefPrefix = $this -> controllerAlias . 'edit/';

    	// Создаем объект структуры страниц
        $this -> struct = PagesStructure::getInstance();

        $root = isset(Request::$GET['root']) ? Request::$GET['root'] : 'root';
        $alias = null;
        if ($root == 'source')
            $root = 0;
        else
        {
            $root = substr($root, 5);
            try
            {
               $alias = $this -> struct -> GetFullAlias($root);
            }
            catch (Exception $e) {}
        }

        $parents = array();
        $selectedPageID = null;
        if (isset(Request::$GET['current']))
        {
            $current = Request::$GET['current'];
            $parts = explode('/', $current);
            $parents = $this -> struct -> GetParentIDs($parts[count($parts) - 2]);
            $selectedPageID = $parts[count($parts) - 2];
        }

        $res = array();
        $res = $this -> GetStructureRecurs($res, $hrefPrefix, $parents, $root, $alias, $selectedPageID);

        return new JsonActionResult($res);
    }

    protected function GetStructureRecurs($currentNode, $hrefPrefix, $parents, $parentID = 0, $alias = '/', $selectedPageID = null)
    {
        $pages = $this -> struct -> GetPages( array('parentID' => $parentID, 'isDeleted' => 0) );

        $res = array();
        if (count($pages) > 0)
        {
            $i = 0;

            foreach ($pages as $page)
            {
                $pageIsActive = ($page['isActive'] == 0) ? 'Нет':'Да';
                $res[$i] = array();
                if ($page['isActive'] == 0)
                	$res[$i]['classes'] = 'inactive';

            	if ($selectedPageID == $page['pageID'])
                    $res[$i]['classes'] = empty($res[$i]['classes']) ? 'selected' : $res[$i]['classes'] .' selected';

                $res[$i]['id'] = 'node_'.$page['pageID'];
                $res[$i]['text'] = '<a href="'.$hrefPrefix.$page['pageID'].

                                   '/" title="Редактировать страницу &laquo;'.
                                   $page['caption'].'&raquo; &lt;br/&gt; Алиас: '.
                                   $alias.$page['alias'].
                                   '/ &lt;br/&gt; Активна: '.$pageIsActive.' &lt;br/&gt; Приоритет: '.
                                   $page['priority'].'"'.
                                   '>'.$page['caption'].'</a>';

                if ($page['isSection'] == 1 && ($this -> struct -> Count(array('parentID' => $page['pageID'], 'isDeleted' => 0)) > 0))
                	$res[$i]['hasChildren'] = true;

                if (!empty($parents) && in_array($page['pageID'], $parents))
                    $res[$i] = $this -> GetStructureRecurs($res[$i], $hrefPrefix, $parents, $page['pageID'], $alias.$page['alias'].'/', $selectedPageID);
                $i++;
            }
        }

    	if (!empty($currentNode))
        {
           $currentNode['children'] = $res;
           $currentNode['expanded'] = true;
           $res = $currentNode;
        }

        return $res;
    }

    /**

     * Получает список всех страниц, которые могут стать родителем данной
     *
     * @param int $currentpageID
     * @param int $currentPageParentID
     * @param int $parentId
     * @param int $iter
     * @return string
     */
    private function PossibleParentOption($currentpageID = 0, $currentPageParentID = 0, $parentId = 0, $iter = 0)
    {
        $sortOrder = $this -> struct -> pagesTableManager -> SortOrder;
        $this -> struct -> pagesTableManager -> SortOrder = '`parentID` ASC, `priority` DESC';
        $pages = $this -> struct -> GetPages(array('isDeleted' => 0, 'isSection' => 1));
        $this -> struct -> pagesTableManager -> SortOrder = $sortOrder;

        $options = array();
        $tree = array();

        // Страницы, разбитые по уровням
        $levels = array();
        $cnt = count($pages);

        $level = 0;
        $bufferedParentID = 0;
        $levels[0] = array();
        for ($i = 0; $i < $cnt; $i++)
        {
			if ($pages[$i]['pageID'] == $currentpageID)
				continue;
            if ($bufferedParentID != $pages[$i]['parentID'])
            {
                $bufferedParentID = $pages[$i]['parentID'];
                if ($level == 0)
                {
                    $level++;
                    $levels[1] = array();
                }
                else
                {
                    if (isset($levels[$level][$pages[$i]['parentID']]))
                    {
                        $level++;
                        $levels[$level] = array();
                    }
                }
            }

			$pages[$i]['children'] = array();
            $levels[$level][$pages[$i]['pageID']] = &$pages[$i];

            if ($level == 0)
                $tree[] = &$pages[$i];
            else
            {
                $prevLevel = $level - 1;
                while ($prevLevel >= 0)
                {
                    if (isset($levels[$prevLevel][$pages[$i]['parentID']]))
                    {
                        $levels[$prevLevel][$pages[$i]['parentID']]['children'][] = &$pages[$i];
                        break;
                    }
                    $prevLevel--;
                }
            }
        }
        return $this -> LinearizeTree($tree);
    }

    /**

     * Рекурсивный метод линеаризации дерева для селекта возможных родителей
     *
     * @param unknown_type $tree
     * @param unknown_type $level
     * @return unknown
     */
    private function LinearizeTree($tree, $level = 0)
    {
        $options = array();
        $space = str_repeat('&nbsp;&nbsp;&nbsp;', $level);
        $cnt = count($tree);
        for ($i = 0; $i < $cnt; $i++)
        {
            $options[] = array(
                'value' => $tree[$i]['pageID'],
                'text'  => $space . $tree[$i]['caption']
            );

            if (isset($tree[$i]['children']))
            	$options = array_merge($options, $this -> LinearizeTree($tree[$i]['children'], $level + 1));
        }

        return $options;
    }

    /**

     * Возвращаяет HTML код формы, содержащий перечисление типов страниц.
     *
     * @param int $pageID
     * @return string
     */
    private function GetPageTypesOption($forNew = false)
    {
        $TypesTableMngr = new DBTableManager(TablesNames::$PAGES_TYPE_TABLE_NAME, null, false);
        return $TypesTableMngr -> Select(($forNew) ? array('noCreatePage' => 0) : null);
    }

    public function Delete($args = null)
    {
        $dispatcher = EventDispatcher::getInstance();
        $this -> InitView();
    	if (empty($args) || !is_numeric($args[0]))

            return $this -> Error('Возникла ошибка во время удаления: не указан id или его формат неверен', $_SERVER['HTTP_REFERER']);
        else
            $pageID = $args[0];
        try
        {
            $dispatcher -> Invoke(self::EVENT_PAGE_RECYCLING, $pageID);
            $this -> struct -> RecyclePage($pageID);
			DBLog::Info('Page (pageID=%d) deleted', $pageID);
            return $this -> Success('Страница перемещена в корзину', $this -> controllerAlias, array('pageID' => $pageID));
        }
        catch (Exception $e)
        {
			DBLog::Error('Error deleting page (pageID=%d): ' . $e -> getMessage(), $pageID);
            return $this -> Error('Возникла ошибка во время удаления.', $_SERVER['HTTP_REFERER']);
        }
    }

    /**
     * ACTION

     * Корзина
     *
     */
    public function Recycled()
    {
        $dispatcher = EventDispatcher::getInstance();
        if (isset(Request::$POST['doRestore']))
        {
            $toEdit = (isset(Request::$POST['toEdit'])) ? Request::$POST['toEdit'] : array();

            if (empty($this -> struct))
                $this -> struct = PagesStructure::getInstance();

            foreach ($toEdit as $pageID)
            {
                try
                {
                    $this -> struct -> Restore($pageID);
                    $dispatcher -> Invoke(self::EVENT_PAGE_RESTORED, $pageID);
					DBLog::Info('Page (pageID=%d) restored', $pageID);
                }
                catch (Exception $e)
                {
					DBLog::Error('Error restoring page (pageID=%d)' . $e -> getMessage(), $pageID);
                    return new JsonActionResult(array('error' => 'Возникла ошибка при восстановлении страницы: '.$e -> getMessage()));
                }
            }
            return new JsonActionResult(array('status' => 'ok', 'action' => 'restored', 'msg' => 'Отмеченные страницы были восстановлены'));
        }

        if (isset(Request::$POST['doWipe']))
        {
            $toEdit = (isset(Request::$POST['toEdit'])) ? Request::$POST['toEdit'] : array();

            if (empty($this -> struct))
                $this -> struct = PagesStructure::getInstance();

            foreach ($toEdit as $pageID)
            {
               try
               {
                   $dispatcher -> Invoke(self::EVENT_PAGE_WIPING, $pageID);
                    $this -> WipePage($pageID);
               }
               catch (Exception $e)
               {
				   DBLog::Error('Error wiping page (pageID=%d)' . $e -> getMessage(), $pageID);
                   return new JsonActionResult(array('error' => 'Возникла ошибка при удалении страницы: '.$e -> getMessage()));
               }
            }
            return new JsonActionResult(array('status' => 'ok', 'action' => 'deleted', 'msg' => 'Отмеченные страницы были удалены'));
        }

        $this -> InitView();
        $deletedPages = $this -> struct -> GetPages(array('isDeleted' => 1));

        ViewData::Assign('DELETED_PAGES', $deletedPages);
        return $this -> view;
    }

    private function WipePage($pageID)
    {
        $page = $this -> struct -> GetPageById($pageID) -> ToArray();

		// Если не задан тип контента и его ID, то и нефик его удалять
		if (($page['pageTypeID'] > 0) && ($page['pageID'] > 0))
		{

			// Получим информацию о типе содержимого
            $typesTable = new DBTableManager(TablesNames::$PAGES_TYPE_TABLE_NAME);
            if (is_numeric($page['pageTypeID']))
            {
                $pageTypeRow = $typesTable -> SelectFirst(array('pageTypeID' => $page['pageTypeID']));
                if (empty($pageTypeRow))

                    throw new Exception('Не существует указанного типа. ID не существует.');
            }

            if (!empty($pageTypeRow['handlerType']))
            {
                switch ($pageTypeRow['handlerType'])
                {
                    case 'module':

                        // пытаемся создать объект модуля
            			$moduleNameArr = explode('.', $pageTypeRow['name']);
            			$moduleName = $moduleNameArr[0];
            			$modules = new Modules();
            			if (!$modules -> isModuleInstalled($moduleName))
                            throw new FileNotFoundException("Модуль $moduleName не найден.");

        				require_once(AppSettings::$MODULES_PATH.$moduleName."/".$moduleName.".class.php");
        				$code = '$module = new ' . $moduleName . '();';
        				eval($code);
        				$module -> DeleteObject($Qresult[0]['name'], $_contentID);
                        break;

                    case 'table':
                        $contentManager = new ContentManager();
                        $tableMngr = new DBTableManager($pageTypeRow['name']);
                        $contentManager -> DeleteContent($page['contentID'], $tableMngr);
                        break;
                }
            }
		}

        $this -> struct -> WipePage($pageID);
		DBLog::Info('Page wiped (pageID=%d)', $pageID);
    }
}

?>
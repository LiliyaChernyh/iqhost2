<?php

class MaintenanceController extends AdminController
{
    public function Index($args = null)
    {
        $this->view = new MaintenanceView();
        $settingsMgr = CachingDBSettingsManager::getInstance();

        $setting = $settingsMgr->GetSetting('admin.maintenanceEnabled');
        $enabled = $setting['value'];
        $setting = $settingsMgr->GetSetting('admin.maintenancePageText');
        $text = $setting['value'];

        ViewData::Assign('maintenanceEnabled', $enabled);
        ViewData::Assign('maintenancePageText', $text);

        if (isset(Request::$POST['maintenanceEnabled']) && isset(Request::$POST['maintenancePageText']))
        {
            try {
                $settingsMgr->EditSetting('admin.maintenanceEnabled', null, null, Request::$POST['maintenanceEnabled']);
                $settingsMgr->EditSetting('admin.maintenancePageText', null, null, Request::$POST['maintenancePageText']);
            }
            catch (Exception $e) {
                return $this->Error($e->getMessage());
            }

            return $this->Success('Настройки сохранены');
        }
        
        return $this->view;
    }
}

FCKConfig.ToolbarSets["GreenySet"] = [
	['Source','DocProps','-','Templates'],
	['Cut','Copy','Paste','PasteText','PasteWord'],
	['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
	['FitWindow','ShowBlocks','-','About'],
	'/',
	['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
	['OrderedList','UnorderedList','-','Outdent','Indent','Blockquote'],
	['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
	['Link','Unlink','Anchor'],
	['Image','Flash','Table','Rule','SpecialChar','PageBreak'],
	'/',
	['TextColor','BGColor'] 	// No comma for the last row.
];
FCKConfig.ToolbarStartExpanded	= false ;
FCKConfig["AutoDetectLanguage"] = false ;
FCKConfig["DefaultLanguage"]    = "ru" ;
<h1>Управление товарами</h1>
<form action="" method="post" enctype="multipart/form-data" id="addProduct" class="storeForm">
    <div class="tabs-area">
        <ul class="tabset">
            <li><a href="#tab-1" class="tab active">Данные</a></li>
            <li><a href="#tab-2" class="tab">Атрибуты</a></li>
            <li><a href="#tab-3" class="tab">Опции</a></li>
        </ul>
        <div class="tab-content" id="tab-1">
            {include file='formBlocks.tpl'}
        </div>
        <div class="tab-content" id="tab-2">
            <div class="formBlockHeader">Атрибуты</div>
            <div class="formBlock extendedAttributes">
                {include file='store/products/add_attributes.tpl'}
            </div>
        </div>
        <div class="tab-content" id="tab-3">
            <div class="formBlockHeader">Опции</div>
            <div class="formBlock extendedOptions">
                {include file='store/products/add_options.tpl'}
            </div>
        </div>
    </div>
    <div class="formBlock1">
		<div class="formElement " style="margin:0 0 0 4px;">
			<input type="submit" value="Сохранить" name="doSave" class="handyButton" />
		</div>
    </div>
</form>

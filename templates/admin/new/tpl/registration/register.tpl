<h1>Регистрация</h1>

{if $ERROR}
    <div style="color: red;">{$ERROR}</div>
{/if}

{if $SUCCESS}
    <div style="font-size: 120%">
        Спасибо за регистрацию. Вы получите уведомление на указанный почтовый адрес после того, как модератор подтвердит вашу учетную запись.
    </div>
{else}
<form action="" method="post" id="contentForm" enctype="multipart/form-data">
	<div class="formBlock">
	    <div class="formElement">
			<label for="login" class="formElementLabel">Имя пользователя:</label>
			<input type="text" name="login" id="login" value="{$DATA.login}" size="45" />
		</div>
		
	    <div class="formElement">
			<label for="password" class="formElementLabel">Пароль:</label>
			<input type="text" name="password" id="password" value="" size="45" />
		</div>
		
	    <div class="formElement">
			<label for="passwordConf" class="formElementLabel">Еще раз пароль:</label>
			<input type="text" name="passwordConf" id="passwordConf" value="" size="45" />
		</div>
		
	    <div class="formElement">
			<label for="FIO" class="formElementLabel">ФИО:</label>
			<input type="text" name="FIO" id="email" value="{$DATA.FIO}" size="45" />
		</div>
		
	    <div class="formElement">
			<label for="email" class="formElementLabel">E-mail:</label>
			<input type="text" name="email" id="email" value="{$DATA.email}" size="45" />
		</div>
	</div>
	<div style="clear:both"></div>
	
	<div style="margin-left:350px">
		<input type="submit" value="Зарегистрироваться" name="doRegister" class="handyButton" />
	</div>
</form>
{/if}
<?php

require_once('ControlFactory.class.php');
require_once('DBControlsRegistryProvider.class.php');

/**
 * Класс позволяет автоматически генерировать массив элементов
 * управления для редактирования полей таблицы
 *
 *  @version 4.5
 */
class FormGenerator
{
    /**
     * Провайдер настроек контролов
     *
     * @var DBControlsRegistryProvider
     */
    public $controlsRegistryProvider;

    /**
     * Редактируемая запись
     *
     * @var array
     */
    public $currentEntry = null;

    /**
     * Список подключаемых JS-библиотек
     * Содержит константные имена библиотек,
     * определённые в контролах.
     * Заполняется при генерации формы
     *
     * @var array
     */
    public $includingJSLibs = null;

    /**
     * Список подключаемых JS-файлов
     * Содержит адреса файлов,
     * определённых в контролах.
     * Заполняется при генерации формы
     *
     * @var array
     */
    public $includingJSFiles = null;

    /**
     * Флаг необходимости обновления формы после отправки данных.
     * Обновляется при генерации формы.
     *
     * @var bool
     */
    public $formRefreshRequired = false;

    /**
     * Конструктор генератора форм
     *
     * @param sting $fieldsRegisterTableName
     * @return FormGenerator
     */
    function __construct()
    {

    }

    /**
     * Генерирует массив элементов управления для указанной таблицы
     * Можно указать ID записи в таблице которой необходимо инициализировать
     * значения элементов управления
     *
     * @param string $tableName
     * @param int $rowID
     * @return array
     */
    public function GenerateForm($tableName, $rowID = null, $data = null, $formIndex = 0)
    {
	$row = null;
	$this->controlsRegistryProvider = new DBControlsRegistryProvider();

	if (($rowID != null) && ($rowID != 0))
	{
	    // Ключ может быть составным
	    // мы работаем только с простым ключём
	    $tableManager = new DBTableManager($tableName);
	    $row = $tableManager->SelectFirst(array($tableManager->TableStructure->PrimaryKey->Name => $rowID));
	    if (empty($row))
		throw new Exception('Невозможно получить форму редактирования записи. Запись не найдена, ID не существует.', 571);

	    $this->currentEntry = &$row;
	}

	// Пробуем прочитать данные из сессии
	@session_start();
	if (isset($_SESSION['savedData']) && isset($_SESSION['savedDataTime']))
	{
	    if ($_SESSION['savedDataTime'] > time() - 30)
	    {
		$data = unserialize($_SESSION['savedData']);
	    }
            unset($_SESSION['savedData']);
            unset($_SESSION['savedDataTime']);
        }

        if ($data != null && is_array($data))
	{
	    if ($row == null)
		$row = array();

	    foreach ($data as $field => $value)
	    {
		if (strpos($field, "__") === false)
		{
		    $row[$field] = $value;
		}
		else
		{
		    $dataNameParts = explode("__", $field);

		    // Если название разделилось не на 2 части, значит оно задано неверно.
		    if (count($dataNameParts) != 2)
			continue;

		    $row[$dataNameParts[0]][$dataNameParts[1]] = $value;
		}
	    }
	}
	Control::$CONTROLS_ALIAS = AppSettings::$LIBS_ALIAS . Control::$CONTROL_ALIAS;
	Control::$CONTROLS_PATH = IncPaths::$LIBS_PATH . Control::$CONTROL_PATH;
	// Теперь получим параметры отображения полей из fieldsRegistry
	$fieldsParamsRaw = $this->controlsRegistryProvider->GetControlsParams($tableName);
	$cnt = sizeof($fieldsParamsRaw);
	if ($cnt == 0)
	    throw new Exception('Ошибка при получении параметров формы редактирования записи выбранного типа. Нет ни одной записи');

	// Теперь, собственно, генерируем форму.
	$form = array();
//	if (isset($formIndex))
//	    $form['formIndex'] = $formIndex;
	$this->includingJSFiles = array();
	$this->includingJSLibs = array();

	$cnt = sizeof($fieldsParamsRaw);
	$contentRow = array();
	for ($i = 0; $i < $cnt; $i++)
	{
	    try
	    {
		$control = ControlFactory::CreateControl($fieldsParamsRaw[$i], $this->controlsRegistryProvider, $tableName, $formIndex, $data);

				// -------------- MultiLanguage --------------- //
		if ($fieldsParamsRaw[$i]['isMultiLanguage'] && !is_null($rowID))
		{
		    if (!isset($contentRow) || sizeof($contentRow) == 0)
		    {
			if (isset($_SESSION['lang']))
			    $currentLang = $_SESSION['lang'];
			else
			{
			    $systemSettings = new SystemSettings();
			    $currentLang = $systemSettings->GetValue('DEFAULT_LANG');
			}
			$relTableManager = new DBTableManager($fieldsParamsRaw[$i]['tableName'] . ML_RELTABLE_POSTFIX);
			$relRows = $relTableManager->Select(
				array(
				    $tableStructure->PrimaryKey->Name => $rowID,
				    'lang' => $currentLang
				)
			);

			/*
			  if (sizeof($relRows) == 0)
			  throw new MultiLanguageException('У поля «'. $fieldsParamsRaw[$i]['fieldName'] .'» нет связи для языка «'. $currentLang .'»');

			  $relRow = $relRows[0];

			  $contentTableManager = new DBTableManager($fieldsParamsRaw[$i]['tableName'] . ML_CONTENTTABLE_POSTFIX);
			  $contentRows = $contentTableManager -> Select(
			  array(
			  'contentID'	=> $relRow['contentID']
			  )
			  );

			  if (sizeof($contentRows) == 0)
			  throw new MultiLanguageException('У поля «'. $fieldsParamsRaw[$i]['fieldName'] .'» нет версии для языка «'. $currentLang .'»');

			  $contentRow = $contentRows[0];
			 */

			if (sizeof($relRows) != 0)
			{
			    $relRow = $relRows[0];

			    $contentTableManager = new DBTableManager($fieldsParamsRaw[$i]['tableName'] . ML_CONTENTTABLE_POSTFIX);
			    $contentRows = $contentTableManager->Select(
				    array(
					'contentID' => $relRow['contentID']
				    )
			    );

			    if (sizeof($contentRows) != 0)
			    {
				$contentRow = $contentRows[0];
			    }
			}
		    }

		    $control->InitControl($contentRow[$fieldsParamsRaw[$i]['fieldName']], $rowID);
		}
		else
		{
		    $control->InitControl(isset($row[$fieldsParamsRaw[$i]['fieldName']]) ? $row[$fieldsParamsRaw[$i]['fieldName']] : null, $rowID);
		}
		// -------------- MultiLanguage --------------- //

		$serialControl = $control->ToArray();
		$serialControl['blockOrder'] = intval($serialControl['blockOrder']);
		$form[$serialControl['blockOrder']]['controls'][] = $serialControl;
		if (isset($serialControl['blockName']))
		{
		    $serialControl['blockName'] = trim($serialControl['blockName']);
		    if (!empty($serialControl['blockName']))
		    {
			$form[$serialControl['blockOrder']]['blockName'] = $serialControl['blockName'];
		    }
		}

		// Инициализируем списки подключаемых JS файлов и библиотек
		for ($l = 0; $l < count($control->controlJSLibs); $l++)
		{
		    if (array_search($control->controlJSLibs[$l], $this->includingJSLibs) === false)
			$this->includingJSLibs[] = $control->controlJSLibs[$l];
		}

		for ($l = 0; $l < count($control->controlJSFiles); $l++)
		{
		    if (array_search($control->controlJSFiles[$l], $this->includingJSFiles) === false)
			$this->includingJSFiles[] = $control->controlJSFiles[$l];
		}

		if ($control->refreshFormAfterFlush)
		    $this->formRefreshRequired = true;
	    }
	    catch (Exception $e)
	    {
		if ($e->getCode() == 3233)
		{
		    $control = array();
		    $control['description'] = "<b>Неизвестный тип элемента управления! {$fieldsParamsRaw[$i]['controlType']}</b>";
		    $control['name'] = $fieldsParamsRaw[$i]['fieldName'];
		    $control['type'] = "undefined";
		    $form[0]['controls'][] = $control;
		}
		else
		    throw $e;
	    }
	}

	// Отсортируем элементы управления должным образом
	// Сортируем по блокам
	ksort($form);

	// Теперь в пределах блока
	foreach ($form as $blockNum => $block)
	    $this->SortBlockByControlOrder($form[$blockNum]);

	return $form;
    }

    /**
     * Сортирует элементы управления в блоке по elemOrder
     * На вход подается блок в след. виде:
     * 	{
     * 		'controls'	=> {числовой массив элементов управления},
     * 		'blockName'	=> название_блока
     * 	}
     *
     * @param array $block
     */
    private function SortBlockByControlOrder(&$block)
    {
	$controls = &$block['controls'];
//        trace($controls);
	$controlsNumber = sizeof($controls);
	for ($i = 0; $i < $controlsNumber; $i++)
	{
	    for ($j = 1; $j < $controlsNumber; $j++)
	    {
		if (isset($controls[$j]['controlOrder']) && isset($controls[$j - 1]['controlOrder']) && $controls[$j - 1]['controlOrder'] > $controls[$j]['controlOrder'])
		{
		    $buf = $controls[$j - 1];
		    $controls[$j - 1] = $controls[$j];
		    $controls[$j] = $buf;
		}
	    }
	}
    }

    /**
     * Трейс блока
     *
     * @param array $block
     */
    private function PrintBlock(&$block)
    {
	$controls = &$block['controls'];

	$out = '';
	$nl = '<br />';
	$controlsNumber = sizeof($controls);
	for ($i = 0; $i < $controlsNumber; $i++)
	{
	    $out .= $controls[$i]['controlOrder'] . ' - ' . $controls[$i]['name'];
	    $out .= $nl;
	}

	echo $out;
    }

}

?>
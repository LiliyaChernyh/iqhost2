<?php

/**
 * Интерфейс кеширующего класса
 *
 * @version 1.0
 */
interface ICache
{
    /**
     * Сохраняет данные в кеше
     *
     * @param string $dataID
     * @param mixed $data
     * @param int $lifetime
     * @return bool
     */
    public static function Save($dataID, $data, $lifetime = null);
    
    /**
     * Берет данные из кеша, если они там есть
     *
     * @param unknown_type $dataID
     * @return mixed
     */
    public static function Load($dataID);
    
    /**
     * Удаляет данные из кеша
     *
     * @param string $dataID
     * @return bool
     */
    public static function Delete($dataID);
}

?>
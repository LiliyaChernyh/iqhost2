<?php
/**
 *
 * Класс управления модулями в Админке
 *
 * @version 2.2
 *
 */
class ModulesController extends AdminController
{
    /**
     * Объект класса модулей
     *
     * @var Modules
     */
    var $modules;

    /**
     * Путь к модулям
     *
     */
    protected $sectionAlias;
	protected $actionAlias;

	public function __construct()
	{
        parent::__construct();

        $this -> view = new ModulesView();
        $this -> modules = new Modules();
		$aliases		 = Request::GetAliases();
//        $this->controllerAlias = AppSettings::$ADMIN_ALIAS.AppSettings::$MODULES_ALIAS;
		ViewData::Assign('SECTION_ALIAS', $this->controllerAlias);
		ViewData::Assign('ACTION_ALIAS', end($aliases));
		ViewData::Assign('MODULES_MENU', $this -> modules -> GetInstalledModules());
    }

    public function Index($args = null)
    {
        $this -> view -> Index();
        return $this -> view;
    }

    public function Edit($args = null)
    {
        if (!isset($args[0]))
			return $this->Error('Ошибка при редактировании настроек: укажите имя модуля', $this->controllerAlias);

		$moduleName = $args[0];

        if (isset($_POST['doEdit']))
        {
            $settings = array();
            if (isset($_POST['settings']))
                $settings = $_POST['settings'];

            foreach ($settings as $settingName => $settingValue)
            {
                try
                {
                    $this -> modules -> UpdateSetting($moduleName, $settingName, $settingValue);
                }
                catch (Exception $e)
                {
                    return $this->Error('Ошибка при редактировании настроек: ' . $e->getMessage(), $this->controllerAlias . 'edit/' . $moduleName . '/');
				}
            }

            return $this->Success('Редактирование настроек прошло успешно', $this->controllerAlias . 'edit/' . $moduleName . '/');
		}

        $moduleSettings = $this -> modules -> GetModuleSettings($moduleName);

        //проверяем, что у модуля нет страниц связанных страниц, если есть, выводим предупреждение
        if (true)
            ViewData::Assign('AYS_MESSAGE', "При удалении модуля также будут удалены все данные модуля!");

        ViewData::Assign('MODULE_SETTINGS', $moduleSettings);
        ViewData::Assign('MODULE_NAME', $moduleName);

        $this -> view -> Edit();
        return $this -> view;
    }

    public function Install($args = null)
    {
        if (isset($_POST['doInstall']))
        {
            if (!isset($args[0]))
                return $this->Error('Возникла ошибка при установке модуля: не указано название модуля', $this->controllerAlias . 'install/');

			$moduleName = $args[0];
			FB::log($moduleName, "MN");
			try
            {
                $this -> modules -> InstallModule($moduleName);
                return $this->Success('Модуль успешно установлен!', $this->controllerAlias);
			}
            catch (Exception $e)
            {
                return $this->Error('Возникла ошибка при установке модуля: ' . $e->getMessage(), $this->controllerAlias . 'install/');
			}
        }

        $uninstalledModules = $this -> modules -> GetUninstalledModules();
        $uninstalledModulesNumber = count($uninstalledModules);

        if ($uninstalledModulesNumber == 0)
        {
            ViewData::Assign('NO_UNINSTALLED_MODULES', 1);
        }
        else
        {
            ViewData::Assign('HAS_UNINSTALLED_MODULES', 1);
            ViewData::Assign('UNINSTALLED_MODULES', $uninstalledModules);
        }

        $this -> view -> Install();
        return $this -> view;
    }

    public function Uninstall($args = null)
    {
        if (!isset($args[0]))
			return $this->Error('Ошибка при удалении модуля: укажите имя модуля', $this->controllerAlias);

		if (!isset($args[0]))
            $this->Error('Возникла ошибка при удалении модуля: не указано название модуля', $this->controllerAlias);

		$moduleName = $args[0];

        try
        {
            $this -> modules -> UninstallModule($moduleName);
            return $this->Success('Модуль успешно удален!', $this->controllerAlias);
		}
        catch (Exception $e)
        {
            return $this->Error('Возникла ошибка при удалении модуля: ' . $e->getMessage(), $this->controllerAlias);
		}
    }
}

?>
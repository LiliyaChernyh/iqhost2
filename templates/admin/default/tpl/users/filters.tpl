{literal}
<script type="text/javascript">
    $(document).ready(function(){
        $('#doCancel').click(function(){
            $('#filterForm').submit();
        });
    });
</script>
{/literal}

<form id="filterForm" action="" method="post" style="display: none; margin-bottom: 10px;">
<table id="doctorFilters">
    <tr>
        <td class="title">Логин:</td>
        <td class="title">E-mail:</td>
        <td class="title">Имя:</td>
        <td class="title">Тип пользователя:</td>
    </tr>
    <tr>
        <td class="control"><input type="text" name="login" value="{if isset($FILTERS.login)}{$FILTERS.login}{/if}" /></td>
        <td class="control"><input type="text" name="email" value="{if isset($FILTERS.email)}{$FILTERS.email}{/if}" /></td>
        <td class="control"><input type="text" name="name" value="{if isset($FILTERS.name)}{$FILTERS.name}{/if}" /></td>
        <td class="control">
            <select name="role">
                <option value="all">Все</option>
                {if isset($FILTERS.roles)}
                {foreach from=$FILTERS.roles item=ROLE}
                <option value="{$ROLE.role}"{if isset($ROLE.selected) && ($ROLE.selected == 1)}selected="selected"{/if}>{$ROLE.name}</option>
                {/foreach}
                {/if}
            </select>
        </td>
    </tr>
    
    <tr>
        <td class="title">Активность:</td>
        <td class="title">&nbsp;</td>
        <td class="title">&nbsp;</td>
        <td class="title">&nbsp;</td>
    </tr>
    <tr>
        <td class="control">
            <select name="activity">
                <option value="all">Все</option>
                <option value="active"{if isset($FILTERS.activity) && $FILTERS.activity == 'active'} selected="selected"{/if}>Активные</option>
                <option value="inactive"{if isset($FILTERS.activity) && $FILTERS.activity == 'inactive'} selected="selected"{/if}>Не активные</option>
                <option value="notmoderated"{if isset($FILTERS.activity) && $FILTERS.activity == 'notmoderated'} selected="selected"{/if}>Ожидающие модерации</option>
            </select>
        </td>
        <td class="control">&nbsp;</td>
        <td class="control">&nbsp;</td>
        <td class="control">&nbsp;</td>
    </tr>
</table>
<div align="right">
    <input type="reset" name="doCancel1" id="doCancel" value="Сбросить фильтры" style="width: 130px;" />
    <input type="submit" name="doFilter" value="Отфильтровать"  style="width: 120px;" />
    <input type="hidden" name="doCancel" value="1" />
</div>
</form>
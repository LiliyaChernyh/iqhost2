<?php

class AppSettings
{
    /**
	 * алиас по умолчанию в админке
	 * @var string
	 */
	public static $ADMIN_DEFAULT_ALIAS = 'main';

	/**
	 * Алиас по умолчанию во фронте
	 * @var string
	 */
	public static $DEFAULT_ALIAS = 'main';

	/**
	 * алиас профиля пользователя (ЛК)
	 * @var string
	 */
	public static $PROFILE_ALIAS = 'profile/';

	/**
	 * Алиас менеджера пользоователей
	 * @var string
	 */
	public static $USERS_ALIAS = 'users/';

	/**
	 * Алиас менеджера настроек
	 * @var string
	 */
	public static $SETTINGS_ALIAS = 'settings/';

	/**
	 * Путь к темплейтам
	 * @var string
	 */
	public static $TEMPLATES_PATH		 = '';

	/**
	 * Путь к темплейтам админки
	 * @var string
	 */
	public static $TEMPLATES_ADMIN_PATH	 = '';

	/**
	 * Путь к темплейтам фронта
	 * @var string
	 */
	public static $TEMPLATES_FRONT_PATH	 = '';
	/**
	 * Алиас к темплейтам админки
	 * @var string
	 */
	public static $TEMPLATES_ADMIN_ALIAS = '';

	/**
	 * Алиас к темплейтам фронта
	 * @var string
	 */
	public static $TEMPLATES_FRONT_ALIAS = '';

	/**
	 * Папка с темплейтами
	 * @var string
	 */
	public static $TEMPLATES_FOLDER		 = 'templates/';

	/**
	 * Папка с темплейтами админки
	 * @var string
	 */
	public static $ADMIN_FOLDER			 = 'admin/';

	/**
	 * Папка с темплейтами фронта
	 * @var string
	 */
	public static $FRONT_FOLDER	 = 'front/';

	/**
	 * название темплейта для админки
	 * @var string
	 */
	public static $ADMIN_TEMPLATE = "default";
	public static $ADMIN_TEMPLATE_COL	 = 1;

	/**
	 * название темплейта для фронта
	 * @var string
	 */
	public static $FRONT_TEMPLATE = "default";

	/**
	 * Папка с шаблонами темплейта
	 * @var string
	 */
	public static $TPLS_FOLDER = "tpl/";

	/**
	 * Папка со скриптами темплейта
	 * @var string
	 */
	public static $SCRIPTS_FOLDER = "js/";

	/**
	 * Папка с изображениями темплейта
	 * @var string
	 */
	public static $IMAGES_FOLDER = "images/";

	/**
	 * Папка со стилями темплейта
	 * @var string
	 */
	public static $STYLES_FOLDER = "css/";

	/**
	 * Относительный путь корня
	 *
	 * @var string
	 */
    public static $ROOT_ALIAS = '/';

    /**
     * Относительный путь панели администрирования
     *
     * @var string
     */
    public static $ADMIN_ALIAS = 'admin/';

    /**
     * Физический путь панели администрирования
     *
     * @var string
     */
    public static $ADMIN_PATH = 'admin/';

    /**
     * Относительный путь папки с библиотеками
     *
     * @var string
     */
    public static $LIBS_ALIAS = 'publicLibs/';

    public static $STYLES_ALIAS = '';
    public static $ADMIN_STYLES = '';

    public static $SCRIPTS_ALIAS = '';
    public static $ADMIN_SCRIPTS = '';

    public static $IMAGES_ALIAS = '';
    public static $ADMIN_IMAGES = '';

	public static $SECTION_ID = '';

	/**
	 * Относительный путь папки с модулями
	 *
	 * @var string
	 */
    public static $MODULES_ALIAS      = 'modules/';
    public static $OBJECTS_CACHE_PATH = 'compile/';
    public static $CONTROLLERS_PATH   = 'controllers/';

    /**
     *  Имя хоста, используется для запуска CLI
     */
    public static $HTTP_HOST          = '';

    /**
     * Инициализания переменных среды
     */
    public static function Init()
    {
		self::$ROOT_ALIAS			 = str_replace($_SERVER['DOCUMENT_ROOT'], '', IncPaths::$ROOT_PATH);
		self::$ADMIN_PATH			 = IncPaths::$ROOT_PATH . self::$ADMIN_PATH;
		self::$OBJECTS_CACHE_PATH	 = IncPaths::$ROOT_PATH . self::$OBJECTS_CACHE_PATH;
		self::$CONTROLLERS_PATH		 = IncPaths::$APPLICATION_PATH . self::$CONTROLLERS_PATH;
		self::$LIBS_ALIAS			 = self::$ROOT_ALIAS . self::$LIBS_ALIAS;
		self::$MODULES_ALIAS		 = self::$ROOT_ALIAS . self::$MODULES_ALIAS;
		self::$ADMIN_ALIAS			 = self::$ROOT_ALIAS . self::$ADMIN_ALIAS;
        self::InitTemplate();
    }

    public static function InitTemplate()
    {
        $taa                         = self::$TEMPLATES_FOLDER . self::$ADMIN_FOLDER . self::$ADMIN_TEMPLATE . "/";
        $tfa                         = self::$TEMPLATES_FOLDER . self::$FRONT_FOLDER . self::$FRONT_TEMPLATE . "/";
        self::$TEMPLATES_ADMIN_PATH  = IncPaths::$ROOT_PATH . $taa . self::$TPLS_FOLDER;
        self::$TEMPLATES_FRONT_PATH  = IncPaths::$ROOT_PATH . $tfa . self::$TPLS_FOLDER;
        self::$TEMPLATES_ADMIN_ALIAS = self::$ROOT_ALIAS . $taa;
        self::$TEMPLATES_FRONT_ALIAS = self::$ROOT_ALIAS . $tfa;
        self::$ADMIN_SCRIPTS         = self::$TEMPLATES_ADMIN_ALIAS . self::$SCRIPTS_FOLDER;
        self::$SCRIPTS_ALIAS         = self::$TEMPLATES_FRONT_ALIAS . self::$SCRIPTS_FOLDER;
        self::$ADMIN_STYLES          = self::$TEMPLATES_ADMIN_ALIAS . self::$STYLES_FOLDER;
        self::$STYLES_ALIAS          = self::$TEMPLATES_FRONT_ALIAS . self::$STYLES_FOLDER;
        self::$ADMIN_IMAGES          = self::$TEMPLATES_ADMIN_ALIAS . self::$IMAGES_FOLDER;
        self::$IMAGES_ALIAS          = self::$TEMPLATES_FRONT_ALIAS . self::$IMAGES_FOLDER;
    }

}

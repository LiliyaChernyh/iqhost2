<?php
/**
 * Конроллер для формы запроса
 * 
 * @version 1.0
 *
 */
class FeedbackControllerFront extends FrontController
{
    public function __construct()
    {
        parent::__construct();
        $this -> view = new FeedbackViewFront();
    }
    
    public function Index($args = null)
    {
        if (isset(Request::$POST['fio']))
        {
            try
            {
                if (!Captcha::CheckCaptcha(Request::$POST['captcha']))
                    throw new Exception('Неверно введены цифры с картинки', 555);

                $msg = array();

                $msg['text'] = '';
                if (isset(Request::$POST['message']))
                    $msg['text'] = strip_tags(Request::$POST['message']);
                
                if (isset(Request::$POST['fio']))
                    $msg['userName'] = strip_tags(Request::$POST['fio']);
                
                if (isset(Request::$POST['phone']))
                    $msg['userPhone'] = strip_tags(Request::$POST['phone']);
                
                if (isset(Request::$POST['email']))
                    $msg['userEmail'] = strip_tags(Request::$POST['email']);
                    
                $msg['title'] = 'Сообщение с формы контактов';
                
                $fb = new Feedback();
                $fb -> AddMessage($msg);
                
                $mailer = new BitPlatformMailer();
                $generalSettings = $this -> settingsManager -> GetSettingsAssoc('general');

                $mailer -> Subject = '['.$generalSettings['general']['siteName'].'] Сообщение с сайта';
                $mailer -> Body = $msg['text'].'
             
     E-mail:  '.$msg['userEmail'].'
     Телефон: '.$msg['userPhone'].'
     Имя:     '.$msg['userName'];
                
                $emails = $fb -> settings['Email'];
                $emails = explode(';', $emails);
                
                foreach ($emails as $email)
                {
                    $email = trim($email);
                    if (!empty($email))
                    {
                        $mailer -> ClearAddresses();
                        $mailer -> AddAddress($email);
                    }
                }
                $mailer -> Send();
            }
            catch(Exception $e)
            {
                return new JsonActionResult(array('error' => $e->getMessage(), 'errno' => $e->getCode()));
            }
            return new JsonActionResult(array('msg' => 'ok'));
        }
        
        // Используем статический метод для получения параметра rootAlias
        $settings = CommonModule::LoadSettings('Feedback');
        $rootAlias = $settings['rootAlias'];
        
        // Получаем страницу из базы
        $this -> page = new Page(array($rootAlias));

        // Получаем контент
        $pageContent = $this -> page -> Content();
        ViewData::Assign('CONTENT1', $pageContent['pageContent']);
        
        $this -> view -> Index();
        return $this -> view;
    }
}
?>
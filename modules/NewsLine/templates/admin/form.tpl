<h1>
    {if isset($DELETE.url)}
        <form onsubmit="return AreYouSure()" method="post" action="{$DELETE.url}">
            <input type="hidden" value="{$DELETE.value}" name="{$DELETE.name}"/>
		{/if}
		{$TITLE}
		{if isset($DELETE.url)}
			[ <input type="image" src="{$ADMIN_IMAGES}delete_icon.png" name="doDelete" title="Удалить" /> ]
        </form>
    {/if}
</h1>

<form action="" method="post" id="contentForm" enctype="multipart/form-data">
	{include file='formBlocks.tpl'}

	<div style="margin-left:350px">
		<input type="submit" value="Сохранить" name="doSave" class="handyButton" />
	</div>
</form>
<h1>Редактирование пользователя</h1>

<form action="" method="post" id="contentForm" enctype="multipart/form-data">

    {include file='formBlocks.tpl'}

    <a href="{$SECTION_ALIAS}passchange/{$EDIT_USER.userID}/">Сменить пароль</a>
    <a href="{$SECTION_ALIAS}restore_password/{$EDIT_USER.userID}/" title="На указанный почтовый адрес будет выслан новый пароль" onclick="if (!confirm('Вы действительно хотите сменить пароль?')) return false;">Восстановить пароль</a>

    <div style="margin:10px 0 0">
            <input type="submit" value="Сохранить" name="doSave" class="handyButton" />
    </div>
</form>
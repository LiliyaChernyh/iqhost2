<?php
class Template extends Smarty
{
    public $tplPath		 = null;
	public $contentTpl	 = null;
	public $menuTpl		 = null;

	public function __construct($tplPath)
	{
        $this -> Smarty();
		$this->compile_id = AppSettings::$SECTION_ID;
        if (AppSettings::$SECTION_ID == 'admin')
		{
			AppSettings::$TEMPLATES_PATH = AppSettings::$TEMPLATES_ADMIN_PATH;
            $this->compile_id          = AppSettings::$SECTION_ID . "_" . AppSettings::$ADMIN_TEMPLATE;
        }
		else
		{
			AppSettings::$TEMPLATES_PATH = AppSettings::$TEMPLATES_FRONT_PATH;
            $this->compile_id          = AppSettings::$SECTION_ID . "_" . AppSettings::$FRONT_TEMPLATE;
        }
		$this -> error_reporting = true;
		$this -> debugging = true;

        $this->template_dir  = AppSettings::$TEMPLATES_PATH;
        $this->compile_dir   = AppSettings::$OBJECTS_CACHE_PATH . 'templates_c/';
        $this->config_dir    = IncPaths::$CONFIG_PATH . 'template_configs/';
        $this->cache_dir     = AppSettings::$OBJECTS_CACHE_PATH . 'templates_cache/';
        $this->plugins_dir[] = IncPaths::$LIBS_PATH . 'Smarty/extPlugins/';
        $this->caching       = 0;
//		$this -> cache_modified_check	 = true;
        $this->tplPath       = $tplPath;
    }

    public function Parse()
    {
        if (!is_null($this -> contentTpl))
            $this -> assign('CONTENT_TPL', $this -> contentTpl);
        if (!is_null($this -> menuTpl))
            $this -> assign('MENU_TPL', $this -> menuTpl);
        return $this -> fetch($this -> tplPath);
    }
}

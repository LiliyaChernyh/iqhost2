<?php
/**
 * DependentSingleSelect
 * 
 * Одинарный селект, варианты которого зависят от 
 * значения другого контрола
 * При изменении значения контрола, от которого зависит 
 * данный вызывается скрипт изменения значений
 * 
 * версия 2.0
 *
 */
class DependentSingleSelect extends Control 
{
    /**
	 * Имя таблицы, участвующей в связи многие ко многим
	 *
	 * @var string
	 */
	private $referentTableName;
	
	/**
	 * Ключ для отображения в человеко-понятном виде
	 * 
	 * @var string
	 */
	private $listingKey;

	/**
	 * Контрол, от которого зависит этот контрол
	 *
	 * @var string
	 */
	private $dependsOn = null;
	
	/**
	 * Зависимое поле. Нужно для определения зависимости в  базе.
	 *
	 * @var string
	 */
	private $dependentField = null;
	
	/**
	*  Путь к серверному скрипту, возвращающему блоки видео
	*
	*  @var string
	*/
	private $serverScript = null;

	public function InitControl($value = null, $rowID = null)
	{
	    $this -> LoadSettings();
	    $this -> controlTplPath = parent::$CONTROLS_PATH.__CLASS__.'/control.tpl';

		// Инициализирующая функция
        $this -> controlJSFiles = array();
        $this -> controlJSFiles[] = parent::$CONTROLS_ALIAS.__CLASS__.'/init.js';
		
		$this -> controlJSVars['controlName'] = $this -> name;
        $this -> controlJSVars['fieldsRegistryEntryID'] = $this -> fieldRegistryID;
        $this -> controlJSVars['dependsOn'] = $this -> dependsOn;
		$this -> controlJSVars['serverScript'] = $this -> serverScript;
        $this -> controlJSVars['value'] = $value;
		
	    $this -> controlTplVars = array();
	    $this -> controlTplVars['NAME'] = $this -> name;
		$this -> controlTplVars['FIELDS_REGISTRY_ENTRY_ID'] = $this -> fieldRegistryID;
		$this -> controlTplVars['ADMIN_ALIAS'] = AppSettings::$ADMIN_ALIAS;
		$this -> controlTplVars['DEPENDS_ON'] = $this -> dependsOn;
		$this -> controlTplVars['TIP'] =  $this -> tip;
		$this -> controlTplVars['CLASS'] =  $this -> CSSClass;
		$this -> controlTplVars['VALUE'] =  $value;

		// Вытаскиваем значения ReferentTable - это та таблица, в которой хранятся option'ы селекта
		if (!empty($value))
		{
			$referentTableManager = new DBTableManager($this -> referentTableName);
			$primaryKey = $referentTableManager -> TableStructure -> PrimaryKey -> Name;
			$selectedValue = $referentTableManager -> Select(array($primaryKey => $value));
			if (!empty($selectedValue))
			{
				/* инициализируем значение контрола (селекта),
				 * от которого зависит этот через JS
				 * Лучше сделать интерфейс IInitialDataChangingControl
				 * который бы указывал на то, что данный контрол может вносить
				 * изменения в инициализирующий набор данных через специальный метод
				 * имеет также смысл сделать метод, возвращающий порядок таких контролов,
				 * 
				 */
				$this -> controlJSVars['dependsOnValue'] = $selectedValue[0][$this -> dependentField];
				$optionsRaw = $referentTableManager -> Select(array($this -> dependentField => $selectedValue[0][$this -> dependentField]));
				$cnt = count($optionsRaw);
				$options = array();
				for ($i = 0; $i < $cnt; $i++)
				    $options[] = array('value' => $optionsRaw[$i][$primaryKey], 
				                       'text' => $optionsRaw[$i][$this -> listingKey],
				                       'selected' => ($optionsRaw[$i][$primaryKey] == $value ? 1:0));
				$this -> controlTplVars['OPTIONS'] = $options;				      
			}
		}
		$this -> type = __CLASS__;
	}
	
	/**
	 * Вытаскивает различные настройки элемента управления (из JSON)
	 *
	 */
	private function LoadSettings()
	{
		if (!empty($this -> controlSettings['referentTable']))
		   $this -> referentTableName = $this -> controlSettings['referentTable'];
		else 
		   throw new Exception('Setting `referentTable` for '.$this -> name.' is undefined.');

		if (!empty($this -> controlSettings['listingKey']))
		   $this -> listingKey = $this -> controlSettings['listingKey'];
		else 
		   throw new Exception('Setting `listingKey` for '.$this -> name.' is undefined.');  

		if (isset($this -> controlSettings['dependentField']))
			$this -> dependentField = $this -> controlSettings['dependentField'];
		else 
			throw new Exception('Dependent field is not set'); 
		
		if (isset($this -> controlSettings['dependsOn']))
			$this -> dependsOn = $this -> controlSettings['dependsOn'];
		else
			throw new Exception('Form filed on which this control is dependent is not defined'); 

		if (!empty($this -> controlSettings['serverScript']))
            $this -> serverScript = $this -> controlSettings['serverScript'];
        else 
            throw new Exception('Server Script (serverScript) for ' . $errmsg);

        if (!empty($this -> controlSettings['emptyValue']))
            $this -> emptyValue = $this -> controlSettings['emptyValue'];
            
        if (!empty($this -> controlSettings['emptyText']))
            $this -> emptyText = $this -> controlSettings['emptyText'];
        
        if (!empty($this -> controlSettings['orderBy']))
            $this -> orderBy = $this -> controlSettings['orderBy'];
	}
	
	public function Flush(&$data, $rowID) {}
	public function PostBackHandler(&$data, $rowID = null) {}
}
?>
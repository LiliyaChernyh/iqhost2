{if !empty($CUSTOMMENU)}
    {foreach from=$CUSTOMMENU key=key item=item}
        {if $key=='TOP_MENU'}
            <ul class="add-nav">
                {foreach from=$item key=key item=itemMenu}
                    <li><a href="{$itemMenu.alias}">{$itemMenu.name}</a></li>
                    {/foreach}
            </ul>
        {/if}
    {/foreach}
{/if}
{*<input type="hidden" name="{$localVars.NAME}[]" value="0" />*}
<div style="overflow: auto; min-width: 630px" data-name="{$localVars.NAME}">
    <div style="float: left; margin-top: 14px;">
        <legend>Доступные:</legend>
        <select style="width: 280px" size="{$localVars.SIZE}" multiple="multiple" class="tags-avail {$localVars.CLASS}"{if !empty($localVars.READONLY)} redaonly="readonly"{/if}>
            {foreach from=$localVars.OPTIONS_AVAILABLE item=OPTION}
                <option value="{$OPTION.VALUE}" >{$OPTION.TEXT}</option>
            {/foreach}
        </select>
    </div>
    <div style="width: 50px; float: left; text-align: center; margin: 30px 5px 0 5px;">
        <button style="width:50px; height: 30px;" type="button" class="button-add-tag" title="Добавить">-&gt;</button>
        <button style="width:50px; height: 30px;" type="button" class="button-remove-tag" title="Убрать">&lt;-</button>
    </div>
    <div style="float: left; margin-top: 14px;">
        <legend>Выбранные:</legend>
        <select style="width: 280px" size="{$localVars.SIZE}" multiple="multiple" {*name="{$localVars.NAME}[]"*} class="tags-selected {$localVars.CLASS}"{if !empty($localVars.READONLY)} redaonly="readonly"{/if}>
            {foreach from=$localVars.OPTIONS_SELECTED item=OPTION}
                <option value="{$OPTION.VALUE}" >{$OPTION.TEXT}</option>
            {/foreach}
        </select>
    </div>
    
    <span style="display: none" class="hidden-holder">
        {foreach from=$localVars.OPTIONS_SELECTED item=OPTION}
            <input type="hidden" name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}]{else}{$localVars.NAME}{/if}[]" value="{$OPTION.VALUE}" />
        {/foreach}
    </span>
</div>
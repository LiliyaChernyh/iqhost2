$(document).ready(function() {
    
    $(".addCategory").on("click",function(){
        var $btn = $(this);
		$.get( "/admin/store/categories/addinner/", function( data ) {
            var $addCategoryForm = $( "#addCategoryForm" );
            $addCategoryForm.html( data );
            var $form = $('form#addCategory');
            var $catInp = $form.find('input.categoryImportName');
            
            var $controlForm = $("#importControl");
            
            $catInp.val($btn.attr('data-import-category'));
            
            $form.on('submit', function() {
                $.ajax({
                    url: $form.attr('action'),
                    type: 'post',
                    data: $form.serialize(),
                    dataType: 'json',
                    success: function (data) {
                        var catName = $catInp.val();
                        $addCategoryForm.empty();
                        var categories = data.categories;
                        
                        var selOpts = '';
                        $.each(categories, function(idx, elm) {
                            selOpts += '<option value="' + elm.categoryID + '">' + (new Array(3 * elm.level + 1).join('&nbsp;') + elm.categoryName) + '</option>';
                        });
                        
                        $controlForm.find('select').each(function(idx, elm) {
                            var $select = $(this);
                            var selectID = $select.val();
                            $select.empty().html(selOpts);
                            if ($select.attr('name') == 'importCategory[' + catName + ']')
                                $select.val(data.info.categoryID);
                            else
                                $select.val(selectID);
                        });
                    }
                });
                
                return false;
            });
		});
//		$("#addCategory").html($.ajax("/admin/store/categories/add/?type=inner"));
		return false;
	});
	
//	$(".saveCategory").on("click",function(){
//			alert( "Load was performed." );
//		$.ajax({
//			url:"/admin/store/categories/addinner/",
//			type: "post",
//			dataType: 'json',
//			success: function (data)
//			{
//				console.log(data)
//			}
//		});
//		return false;
//	});
});

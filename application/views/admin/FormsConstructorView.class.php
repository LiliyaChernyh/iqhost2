<?php
/**
 * Представление для раздела конструирования форм
 *
 * @version 1.0
 *
 */
class FormsConstructorView extends AdminView
{
    public function __construct()
    {
		if (AppSettings::$ADMIN_TEMPLATE_COL == 1)
		{
			$mainTpl = 'single_column.tpl';
		}
		else
		{
			$mainTpl = 'main.tpl';
		}

		parent::__construct($mainTpl);

		// Адрес к шаблонам раздела
        $this -> tplsPath = $this -> tplsPath .'formsconstruct/';

        $this -> tpl -> contentTpl = $this -> tplsPath . 'main.tpl';

        $this -> tpl -> append('SCRIPT_FILE', AppSettings::$ADMIN_SCRIPTS.'formsconstructor.js');
        $this -> tpl -> assign('INCLUDE_JQUERY_UI_SORTABLE', 1);
    }
}
?>
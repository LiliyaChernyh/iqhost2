<h1>{if !empty($HEADER)}{$HEADER}{/if}</h1>

{if isset($ALLOW_ADDITION) && $ALLOW_ADDITION == 1}
<div style="text-align: right; margin: -30px 0 5px 0;">
    <input type="button" id="goAddItem" class="goToBtn" value="Добавить {$ITEM_NAME}" />
    <input type="hidden" id="goAddItem_link" value="{$ADD_ENTRY_ALIAS}" />
</div>
{/if}

<table id="itemsList"></table>
<div id="pager"></div>

{literal}
<script type="text/javascript">
$(document).ready(function(){
    $('#itemsList')
	.jqGrid({
        url : '{/literal}{$CONTROLLER_ALIAS}items/?table={$TABLE_NAME}{literal}',
        datatype: 'json',
        mtype: 'GET',
        colNames:[{/literal}{foreach from=$TABLE_HEADERS item=HEADER key=I}'{$HEADER.caption}'{if $I<$TABLE_HEADERS|@count-1},{/if}{/foreach}{literal}],
        colModel:[ 
            {/literal}{foreach from=$TABLE_HEADERS item=HEADER key=I}
            {ldelim}
                name:'{$HEADER.name}', 
                index:'{$HEADER.name}'
                {if !isset($HEADER.searchable) || $HEADER.searchable == true}
                , search:true
                , searchoptions:{ldelim}sopt:['cn','eq']{rdelim}
                {else}
                , search:false
                {/if}
                {if !isset($HEADER.sortable) || $HEADER.sortable == true}
                , sortable:true
                {else}
                , sortable:false
                {/if}
                {if !empty($HEADER.width)}, width: {$HEADER.width}{/if}
                {if !empty($HEADER.align)}, align: '{$HEADER.align}'{/if}
            {rdelim}{if $I<$TABLE_HEADERS|@count-1},{/if}
            {/foreach}{literal}
        ],
		ondblClickRow: function(id)
		{
			window.location.href = editURL + id;
        },
        {/literal}{if !empty($ITEMS_ON_PAGE)}rowNum: {$ITEMS_ON_PAGE}, {/if}{literal}
        pager: $('#pager'),
		toppager: true,
        height: "100%",
        //width: $(document).width() - 100,
        autowidth: true,
        viewrecords: true
    })
	.navGrid('#pager',
		{view:false, del:false, add:false, edit:false, searchtext:"Поиск", refresh:false, refreshtext:"Обновить"}, 
		{}, //  default settings for edit
		{}, //  default settings for add
		{},  // delete instead that del:false we need this
		{closeOnEscape:true, multipleSearch:true, closeAfterSearch:true}, // search options
		{} /* view parameters*/
	)
	.navButtonAdd
	(
		'#pager',
		{
			caption:"Редактировать", 
			onClickButton: AEGoToEditRow, 
			position:"last"
		}
	)
	.navButtonAdd
	(
		'#pager',
		{
			caption:"Добавить", 
			onClickButton: AEGoToAddRow, 
			position:"last"
		}
	)
	.navButtonAdd
	(
		'#pager',
		{
			caption:"Удалить", 
			onClickButton: AEDeleteRow, 
			position:"last"
		}
	);
    //$('#itemsList').navGrid('#pager', {search:true});
});

var editURL = '{/literal}{$CONTROLLER_ALIAS}edit/?table={$TABLE_NAME}&rowID={literal}';
function AEGoToEditRow()
{
    var rowID = $('tr[aria-selected=true]').attr('id');
    if (typeof(rowID) != "undefined")
    {
        window.location.href = editURL + rowID;
    }
    else
    {
        $.jGrowl('Необходимо выбрать запись для редактирования');
    }
}

var addURL = '{/literal}{$CONTROLLER_ALIAS}add/?table={$TABLE_NAME}{literal}';
function AEGoToAddRow()
{
    window.location.href = addURL;
}

var removeURL = '{/literal}{$CONTROLLER_ALIAS}delete/?table={$TABLE_NAME}&rowID={literal}';
function AEDeleteRow()
{
    var rowID = $('tr[aria-selected=true]').attr('id');
    if (typeof(rowID) != "undefined")
    {
        $.post(
            removeURL + rowID,
            { async: true },
            AEDeleteRowSuccess,
            'json'
        );
    }
    else
    {
        $.jGrowl('Необходимо выбрать запись для удаления');
    }
    
}

function AEDeleteRowSuccess(data, status)
{
    if (!standartAJAXResponseHandler(data, status)) return;
	if (data.msg == "ok")
	{
	    $('tr#' + data.rowID).remove();
		$.jGrowl("Запись удалена");
		//if ($("#entriesList tbody").length == 0) list.replaceWith( "Таблица пуста" );
	}
	else if(data.dependentEntries)
	{
		$('#dialog').jqm({modal: false});
		var dependentItemsContainer = $("#dialog div").eq(1).empty();
		var link = 0;
		for (var i = 0; i < data.dependentEntries.length; i++)
		{
			dependentItemsContainer.append($("<h2>"+data.dependentEntries[i].tableName+"</h2>"));
			for (var l = 0; l < data.dependentEntries[i].itemsLinks.length; l++)
			{
				link = data.dependentEntries[i].itemsLinks[l];
				dependentItemsContainer.append($("<div>- <a href='"+link+"'>"+link+"</a></div>"));
			}
		}
		$('#dialog').jqmShow();
	}
}
</script>
{/literal}


<div id="dialog" class="jqmWindow">
    <div style="width:100%;text-align:right"><a href="#" class="jqmClose">х</a></div>
    Данная запись не может быть удалена, поскольку имеются связанные записи в других таблицах:
    <div></div>
</div>
<div style="width:100%; float:left;" id="{$localVars.NAME}_controlContainer">

    <div style="float: left; width: 100%">
	{if !empty($localVars.FILES)}
    {foreach from=$localVars.FILES item=FILE key=I}
        <div style="float: left; width: 250px; margin: 15px 0;"{if isset($localVars.blockClass)} class="{$localVars.blockClass}"{/if}>
			{if preg_match('/\.jpg$|\.jpeg$|\.gif$|\.png$/i',$FILE.src)!=false}
				{if !empty($FILE.srcSmall)}
				<a href="{$FILE.src}" class="fancy" rel="{$localVars.NAME}"><img src="{$FILE.srcSmall}" alt="" /></a>

					{if !empty($localVars.includeThumbCropDialog)}
					<a href="#" class="thumbCropDialogActivator jsLink" title="Открыть диалог редактирования уменьшенной копии">Обрезать вручную</a>
					{/if}

				{else}
				<img src="{$FILE.src}" alt="" />
				{/if}
			{else}
				{if preg_match('/\.flv$/i',$FILE.src)!=false}
				<object id="flvPlayer_{$localVars.NAME}_{$I}" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="600" height="330">
					<param name="movie" value="/publicLibs/VideoPlayerStandart.swf" />
					<param name="flashvars" value="flv={$FILE.src}" />
					<param name="wmode" value="opaque" />
					<!--[if !IE]>-->
					<object type="application/x-shockwave-flash" data="/publicLibs/VideoPlayerStandart.swf" width="600" height="330">
						<param name="flashvars" value="flv={$FILE.src}" />
						<param name="wmode" value="opaque" />
					<!--<![endif]-->
					  <p>Установите Flash-плеер</p>
					<!--[if !IE]>-->
					</object>
					<!--<![endif]-->
				</object>
				{else}
					{if preg_match('/\.mp3$/i',$FILE.src)!=false}
					<object id="mp3Player_{$localVars.NAME}_{$I}" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="300" height="20">
						<param name="movie" value="/publicLibs/MP3PlayerStandart.swf" />
						<param name="flashvars" value="mp3={$FILE.src}" />
						<param name="wmode" value="opaque" />
						<!--[if !IE]>-->
						<object type="application/x-shockwave-flash" data="/publicLibs/MP3PlayerStandart.swf" width="300" height="20">
							<param name="flashvars" value="mp3={$FILE.src}" />
							<param name="wmode" value="opaque" />
						<!--<![endif]-->
						  <p>Установите Flash-плеер</p>
						<!--[if !IE]>-->
						</object>
						<!--<![endif]-->
					</object>
					{else}
					<a href="{$FILE.src}" class="openInNewWin">{$FILE.src}</a>
					{/if}
				{/if}
			{/if}
            {if !empty($localVars.includeTitle)}
            <br />
			<input type="text" name="{$localVars.NAME}__title[{$FILE[$localVars.FILE_ID_KEY]}]" id="{$localVars.NAME}__title[{$FILE[$localVars.FILE_ID_KEY]}]" value="{$FILE.title}" title="Название изображения" style="width: 200px" />
			{/if}
			{if !empty($localVars.textFields)}
			{foreach from=$localVars.textFields item=fieldInfo key=fieldName}
			<div {if !empty($fieldInfo.containerClass)}class="{$fieldInfo.containerClass}"{/if}>
    			{if $fieldInfo.showLabel}<span>{$fieldInfo.label} </span>{/if}
    			<input type="text" name="{$localVars.NAME}__{$fieldName}[{$FILE[$localVars.FILE_ID_KEY]}]" id="{$localVars.NAME}__{$fieldName}[{$FILE[$localVars.FILE_ID_KEY]}]" value="{$FILE[$fieldName]}" title="{$fieldInfo.label}"{if !empty($fieldInfo.styles)} style="{$fieldInfo.styles}"{/if} />
			</div>
			{/foreach}
			{/if}
			{if !empty($localVars.usePriority)}
            <br />
			<label for="{$localVars.NAME}__priority[{$FILE[$localVars.FILE_ID_KEY]}]">Проиритет:</label> <input type="text" name="{$localVars.NAME}__priority[{$FILE[$localVars.FILE_ID_KEY]}]" id="{$localVars.NAME}__priority[{$FILE[$localVars.FILE_ID_KEY]}]" value="{$FILE.priority}" style="width: 50px" />
			{/if}
            <br />
            <label for="{$localVars.NAME}_{$I}__delete">Удалить <input type="checkbox" id="{$localVars.NAME}_{$I}__delete" name="{$localVars.tableName}[{$localVars.NAME}__delete][]" value="{$FILE[$localVars.FILE_ID_KEY]}" /></label>
        </div>
    {/foreach}
	{/if}
    </div>

    <div style="clear:left;"></div>

    <div class="SWFUploadContainer">
    	<div class="fieldset flash" id="fsUploadProgress_{$localVars.NAME}">
        	<span class="legend">Загружаемые файлы</span>
    	</div>
        <div id="divStatus_{$localVars.NAME}"></div>
    	<div>
    		<span id="spanButtonPlaceHolder_{$localVars.NAME}"></span>
    		<input id="btnCancel_{$localVars.NAME}" type="button" value="Отменить загрузку" onclick="swfu.cancelQueue();" disabled="disabled" style="margin-left: 2px; font-size: 8pt; height: 29px; display: none;" />
    	</div>
    	<span style="display: none;" id="uploadedFilesInputsPlaceHolder_{$localVars.NAME}"></span>
    </div>

</div>
<div style="clear:left;"></div>
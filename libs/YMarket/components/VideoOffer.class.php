<?php
/**
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License (version 2 or (at your option) any later version) as published by the Free Software Foundation.
 *
 * @author web-junior <admin@web-junior.net>
 * @version 0.1
 * @package
 * @link http://web-junior.net
 */



class VideoOffer extends Offer{


	public function __construct($id, $name, $price){
		parent::__construct($id, $name, $price);

		$this->privateProperties = array(
			'artist'=>'', 'title'=>'', 'year'=>'', 'media'=>'',
			'starring'=>'', 'director'=>'', 'originalName'=>'',
			'country'=>''
		);

		$this->generalAttributes['type'] = 'artist.title';
	}
}

?>
 {if isset($ADVANTAGES) && !empty($ADVANTAGES)}
            <div class="post-area container">
                <h2>Наши преимущества</h2>
                <div class="columns">
                    {foreach from=$ADVANTAGES item=advantage}
                        <div class="column">
                            <h3>{$advantage.title}</h3>
                            <p>{$advantage.text}</p>
                        </div>
                    {/foreach}                
                </div>
            </div>
        {/if}
        {if isset($ACTIONS) && !empty($ACTIONS)}
            <div class="action-holder">
                <div class="container">
                    <h2>Акции</h2>
                    <div class="threecolumns">
                        {foreach from=$ACTIONS item=action}
                        <div class="col">
                            <div class="inner">
                                <div class="img-holder">
                                    <a href="/actions/{$action.alias}/"><img src="/getimage/{$action.imageID}/"  width="540" height="405"></a>
                                </div>
                                <div class="textholder">
                                    <p><a href="/actions/{$action.alias}/">{$action.caption}</a></p>
                                </div>
                            </div>
                        </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        {/if}
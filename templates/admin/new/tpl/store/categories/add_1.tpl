<form action="/admin/store/categories/addinner/" method="post" enctype="multipart/form-data" id="addCategory" class="">
	<div class="formBlock">
        <input type="hidden" class="categoryImportName" />
		<div class="formElement HiddenInput">
			<label for="categoryID_0" class="formElementLabel"></label>
		</div>
		<div class="formElement   SingleSelect">
			<label for="categoryParentID_0" class="formElementLabel">Родительская категория<span style="color: red;">*</span></label>
			<select name="Store_Categories[0][categoryParentID]" id="categoryParentID_0" class=" ">
				<option value="1">-</option>
				{foreach from=$CATEGORIES item=cat}
					<option value="{$cat.categoryID}">{'&nbsp;'|str_repeat:$cat.level*3}{$cat.categoryName}</option>
				{/foreach}
			</select>
		</div>
		<div class="formElement   LineInput">
			<label for="categoryName_0" class="formElementLabel">Название категории<span style="color: red;">*</span></label>
			<input name="Store_Categories[0][categoryName]" id="categoryName_0" value="" size="60" maxlength="255" title="" type="text">
		</div>
		<div class="formElement   TextArea">
			<label for="categoryDescription_0" class="formElementLabel">Описание категории</label>
			<textarea cols="60" rows="5" name="Store_Categories[0][categoryDescription]" id="categoryDescription" class=" "></textarea>
		</div>
		<div class="formElement   LineInput">
			<label for="categoryAlias_0" class="formElementLabel">Алиас</label>
			<input name="Store_Categories[0][categoryAlias]" id="categoryAlias_0" value=""  size="60" maxlength="64" title="" type="text">
		</div>
	</div>
	<div class="formBlock">
		<div class="formElement " style="margin:0 0 0 4px;">
			<button name="doSave" class="saveCategory">Сохранить</button>
		</div>
	</div>
</form>
<?php
/**
 * Представление для обычных страниц
 * 
 * @version 1.0
 *
 */
class SearchView extends FrontView
{
    public function __construct()
    {
        parent::__construct();

        // Шаблон с меню
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
        $this -> tpl -> contentTpl = $this -> tplsPath .'search.tpl';
    }
}
?>
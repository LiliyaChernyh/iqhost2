<h1>Пользователи</h1>

<form id="foto" method="POST" action="/admin/Fotobank/">
    {if isset($FCHAR_USERS)}
    <div id="block-collabse" style="width:300px; float:left;">
        <div id="block-list-users" class="block-list">
            {if !empty($FCHAR_USERS)}
                {foreach from=$FCHAR_USERS key=KEY item=CHAR}
                <h3>{$KEY}<span class="badge">{$FCHAR_USERS[$KEY]|@count}</span></h3>
                <div>
                  <ul class="list-group">
                        {foreach from=$CHAR key=I item=USER}
                        <li class="list-group-item list-user">
                          {$CHAR[$I]}
                          <input type="hidden" value="{$I}">
                        </li>
                        {/foreach}
                  </ul>
                </div>
                {/foreach}
            {else}
                <h2>Нет пользователей<h2>
            {/if}
        </div>
    </div>
    {/if}

    {if isset($TYPES_AND_TAGS)}
    <div id="block-collabse" style="width:700px;float:left;margin-left:1px;">
        <div id="block-list-tags" class="block-list">
            {if !empty($TYPES_AND_TAGS)}
            {foreach from=$TYPES_AND_TAGS key=KEY item=TYPES}
            <h3>
                {$TYPES.caption}<span class="badge">{if (isset($TYPES.children))}{$TYPES.children|@count}{else}0{/if}</span>
                <input type="hidden" value="{$TYPES.typeTagID}">
            </h3>
            <div class="block-list block-list-fotos">
                {if (isset($TYPES.children))}
                {foreach from=$TYPES.children key=I item=TAG}
                <h3 id="head-foto_{$TAG.tagID}">
                    <input type="hidden" value="{$TAG.tagID}" class="head-foto-input">
                    <!-- <input class="check-fotos" type="checkbox"> -->
                    <div class="checkbox"><input type="checkbox" value="0"/></div>
                    {$TAG.caption}&nbsp;<span class="badge">{$TAG.count_foto}</span>
                    <div class="link-edit" style="display: inline;" onclick="event.stopPropagation(); location.href = '{$MODULE_ALIAS}uploads/{$TAG.tagID}/';">[ред.]</div>
                </h3>
                <div id="block-fotos_{$TAG.tagID}" data-tag-id="{$TAG.tagID}"></div>
                {/foreach}
                {/if}
            </div>
            {/foreach}
            {else}
                <h2>Нет тегов<h2>
            {/if}
        </div>
    </div>
    {/if}

    <script>
        InitCollabse();
    </script>


    <div style="clear:both;">
        <input id="save" name="save" type="submit" value="Сохранить">
    </div>
</form>




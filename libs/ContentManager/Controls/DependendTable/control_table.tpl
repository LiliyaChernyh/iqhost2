<input type="hidden" name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}]{else}{$localVars.NAME}{/if}[]" value="0"/>
<div class="feedback_messageItem {$localVars.NAME}">
	<table class="highlightable " style="width: 1020px;">
		<thead>
        <tr>
            {foreach from=$localVars.FIELDS key=name item=field}
                <th>
                    {$field.name}
                </th>
            {/foreach}
            <th width="20%" style="text-align: center;">Добавить поле <a href="#" id="{$localVars.NAME}_add">+</a></th>
        </tr>
        </thead>
        <tbody>
		<tr style="display:none" class="default">
			{foreach from=$localVars.FIELDS key=name item=field}
				<td>
					{if $field.type == "select"}
						<select name="{$localVars.NAME}__{$name}[]">
							<option value=""> --- </option>
							{foreach from=$field.data item=OPTION}
								<option value="{$OPTION.VALUE}">{$OPTION.TEXT}</option>
							{/foreach}
						</select>
					{elseif $field.type == "hidden"}
						<input type="hidden" name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}__{$name}]{else}{$localVars.NAME}__{$name}{/if}[]" value=""  />
					{else}
						<input type="text" name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}__{$name}]{else}{$localVars.NAME}__{$name}{/if}[]" value=""  />
					{/if}
				</td>
			{/foreach}
			<td style="text-align: center;">
				<input class="btn" value="-" type="button" alt="" onclick="{literal}$(this).parents('tr').remove();{/literal}"/>
				<input class="btn" value="&uparrow;" type="button" alt=""/>
				<input class="btn" value="&downarrow;" type="button" alt=""/>
			</td>
		</tr>
		{foreach from=$localVars.OPTIONS key=ind item=item}
			<tr id="itms_{$ind}" title="ololol">
				{foreach from=$item key=field item=val}
					<td>
						{if $localVars.FIELDS[$field].type == "select"}
							<select name="{$localVars.NAME}__{$field}[]">
								<option value=""> --- </option>
								{foreach from=$localVars.FIELDS[$field].data item=OPTION}
									<option value="{$OPTION.VALUE}" {if $val == $OPTION.VALUE} selected="selected"{/if}>{$OPTION.TEXT}</option>
								{/foreach}
							</select>
						{elseif $localVars.FIELDS[$field].type == "hidden"}
							<input type="hidden" name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}__{$field}]{else}{$localVars.NAME}__{$field}{/if}[]" value="{$val}"  />
						{else}
							<input type="text" name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}__{$field}]{else}{$localVars.NAME}__{$field}{/if}[]" {*id="{$localVars.NAME}__{$field}"*} value="{$val}"  />
						{/if}
					</td>
				{/foreach}
				<td style="text-align: center;">
					<input class="btn" value="-" type="button" alt="" onclick="{literal}$(this).parents('tr').remove();{/literal}"/>
                    <input class="btn up" value="&uparrow;" type="button" alt=""/>
                    <input class="btn down" value="&downarrow;" type="button" alt=""/>
				</td>
			</tr>
		{/foreach}
        </tbody>
	</table>
</div>

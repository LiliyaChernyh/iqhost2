<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-language" content="ru" />
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />			

<title>Система управления заказами McCoy</title>
<style>
body {
	font-family: Verdana, Tahoma, Arial;
	font-size: 8pt;
	margin: 0;
	padding: 0;
}
h1 {font-size: 1.3em;}
h2 {font-size: 1.2em;}
h3 {font-size: 1.1em;}
h1, h2, h3 {margin-bottom: 0.2em;}

table#customerInfo, table#orderInfo {
	border: 1px solid #eee;
	border-collapse: collapse;
}
table#customerInfo td, table#orderInfo td {
	border-bottom: 1px dotted #AAA;
}
table#customerInfo td#tdTitle {
	width: 100px;
	font-weight: bold;
	font-size: 0.8em;
}
table#orderInfo td#tdTitle {
	width: 250px;
	font-weight: bold;
	font-size: 0.8em;
}
table#cart1, table#cart2 {
	border: 1px solid #AAA;
	float: left;
	width: 49%;
	border-collapse: collapse;
}
table#cart1 th, table#cart2 th {
	border-bottom: 1px solid black;
	font-size: 0.8em;
}
table#cart1 td, table#cart2 td {
	border-bottom: 1px dotted black;
	padding: 3px;
}

table#additionalServicesTable {
	border-collapse: collapse;
	border: 2px solid #eee;
}
table#additionalServicesTable td {
	border-bottom: 1px dotted black;
	
}

</style>
</head>

<body>
<h1 style="position:absolute;top:0px;right:40px"><small>тел.</small> 608-20-07</h1>
<h2>Заказ №{#var: ORDERID}</h2>

<div style="float:left; width: 40%; margin-right:1%;">
	<h3>Информация о госте</h3>
	<table id="customerInfo">
		<tr>
			<td id="tdTitle">
				ФИО: 
			</td>
			<td>
				{#var: CUSTOMER_FIO}
			</td>
		</tr>
		<tr>
			<td id="tdTitle">
				Почта: 
			</td>
			<td>
				{#var: CUSTOMER_EMAIL}
			</td>
		</tr>
		<tr>
			<td id="tdTitle">
				Телефон: 
			</td>
			<td>
				{#var: CUSTOMER_PHONE}
			</td>
		</tr>
		{#ifset: CUSTOMER_ADDRESS}
		<tr>
			<td id="tdTitle">
				Адрес: 
			</td>
			<td>
				{#var: CUSTOMER_ADDRESS}
			</td>
		</tr>	
		{##ifset}
	</table>
</div>
<div style="float:left; width: 58%;">	
	<h3>Информация о заказе</h3>
	<table id="orderInfo">
		<tr>
			<td id="tdTitle">
				Дата проведения банкета:
			</td>
			<td>
				{#var: BANQUETDATE}
			</td>
		</tr>
		<tr>
			<td id="tdTitle">
				Число гостей:
			</td>
			<td>
				{#var: GUESTSNUMBER}
			</td>
		</tr>
		<tr>
			<td id="tdTitle">
				Время создания:
			</td>
			<td>
				<span class="selected">{#var: CREATIONDATE}</span>
			</td>
		</tr>
		<tr>
			<td id="tdTitle">
				Номера столов:
			</td>
			<td>
				{#var: TABLESNUMBERS}
			</td>
		</tr>
		<tr>
			<td id="tdTitle">
				Задаток:
			</td>
			<td>
				{#var: PREPAYMENT}
			</td>
		</tr>
		<tr>
			<td id="tdTitle">
				Ответственный менеджер:
			</td>
			<td>
				{#var: RESPONSIBLE_MANAGER_NAME}
			</td>
		</tr>
	</table>
</div>

<div style="clear:left; margin:0;">&nbsp;</div>

	<table id="cart1">
		<tr>
			<th width="15%">Наименование</th>
			<th width="5%">Цена</th>
			<th width="3%">Ко&shy;ли&shy;чес&shy;тво</th>
			<th width="1%">Пор&shy;ции</th>
			<th width="5%">Сум&shy;ма</th>
		</tr>
		{#block: dishes_part1}
		{#var: DISHES1_CATEGORY}
		<tr class="{#var: ODD_EVEN}">
			<td>{#var: DISHES1_NAME}</td>
			<td>{#var: DISHES1_PRICE} р.</td>
			<td>{#var: DISHES1_AMOUNT}</td>
			<td>{#var: DISHES1_PORTIONS}</td>
			<td>{#var: DISHES1_SUMM} р.</td>
		</tr>
		{##block}
	</table>
	<table id="cart2">
		<tr>
			<th width="15%">Наименование</th>
			<th width="5%">Цена</th>
			<th width="3%">Ко&shy;ли&shy;чес&shy;тво</th>
			<th width="1%">Пор&shy;ции</th>
			<th width="5%">Сум&shy;ма</th>
		</tr>
		{#block: dishes_part2}
		{#var: DISHES2_CATEGORY}
		<tr class="{#var: ODD_EVEN}">
			<td>{#var: DISHES2_NAME}</td>
			<td>{#var: DISHES2_PRICE} р.</td>
			<td>{#var: DISHES2_AMOUNT}</td>
			<td>{#var: DISHES2_PORTIONS}</td>
			<td>{#var: DISHES2_SUMM} р.</td>
		</tr>
		{##block}
	</table>
	<div style="clear:both">&nbsp;</div>

	<table id="additionalServicesTable">
		<thead>
			<tr>
				<th>Дополнительные услуги</th>
				<th width="13%">Цена / %</th>
			</tr>
		</thead>
		<tbody>
			{#block: additional_services}
			<tr style="border: 1px solid #EEE">
				<td>
					{#var: ADDITIONAL_SERVICE_SERVICETITLE}
				</td>
				<td>
					{#var: ADDITIONAL_SERVICE_COST}
				</td>
			</tr>
			{##block}
		</tbody>
	</table>
<div style="font-weight:bold; margin:0 20px 10px 0;clear:both">
	<div style="width:200px;float:left;margin:5px 0">Кухня: {#var: KITCHEN_TOTAL} р.</div>{#ifset: KITCHEN_TOTAL_DISCOUNT}<div style="float:left;margin:5px 0"> Со скидкой: {#var: KITCHEN_TOTAL_DISCOUNT} p.</div>{##ifset}
	<div style="width:200px;float:left;clear:left;margin:5px 0">Бар: {#var: BAR_TOTAL} р.</div>{#ifset: BAR_TOTAL_DISCOUNT}<div style="float:left;margin:5px 0"> Со скидкой: {#var: BAR_TOTAL_DISCOUNT} p.</div>{##ifset}
	<div style="width:200px;float:left;clear:left;margin:5px 0">Кухня и бар: {#var: DISHES_TOTAL} р.</div>{#ifset: DISHES_TOTAL_DISCOUNT}<div style="float:left;margin:5px 0"> Со скидкой: {#var: DISHES_TOTAL_DISCOUNT} p.</div>{##ifset}
	<div style="clear:both">&nbsp;</div>
	<div style="margin:10px 0">
		Стоимость всего заказа: {#var: TOTAL_ORDER_COST} р. <br/>
		{#ifset: TOTAL_ORDER_COST_DISCOUNT}Со скидкой на бар и кухню: {#var: TOTAL_ORDER_COST_DISCOUNT} р.{##ifset}
	</div>
</div>

</body>
</html>
<h1>Управление справочником опций</h1>
<form action="" method="post" enctype="multipart/form-data" id="addOption" class="storeForm">
    <div class="formBlockHeader">Основные данные</div>
    {include file='formBlocks.tpl'}
    <div class="formBlockHeader">Значения поля</div>
    <div class="formBlock">
	<div class="formElement AddButton" style="float: right;">
	    <div>
		<input type="button" value="Добавить значение" name="addValue" id="addValue"/>
	    </div>
	</div>
	<div class="clear"></div>
	<div class="optionValues">
	    {if !empty($VALUES)}{$VALUES}{/if}
	</div>
	<div class="formElement AddButton" style="float: right;">
	    <div>
		<input type="button" value="Добавить значение" name="addValue" id="addValue"/>
	    </div>
	</div>
    </div>
    <div class="formBlock">
		<div class="formElement " style="margin:0 0 0 14px;">
			<input type="submit" value="Сохранить" name="doSave" class="handyButton" />
		</div>
    </div>
</form>
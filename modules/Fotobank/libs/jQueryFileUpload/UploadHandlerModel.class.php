<?php
/*
 * jQuery File Upload Plugin PHP Class 6.4.4
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

class UploadHandlerModel
{  
    public function insert_file($image_versions, $upload_url, $file_name){
        $src = '"'.$upload_url.$file_name.'"';
        $srcThumbnail = null;
        $srcSmall = null;
        
        foreach($image_versions as $version => $options){
            if (!empty($version)){
                if ($version == 'medium') $srcSmall = '"'.$upload_url.'medium/'.$file_name.'"';
                elseif ($version == 'thumbnail') $srcThumbnail = '"'.$upload_url.'thumbnail/'.$file_name.'"';
            }
        }
        
        $query = 'INSERT INTO greeny_images (`imageID`, `src`, `srcSmall`, `srcThumbnail`) 
                  VALUE (null, '.$src.', '.$srcSmall.', '.$srcThumbnail.')';
        
        if (DB::Query($query)){
            $query = 'SELECT LAST_INSERT_ID()';
            $imageID = DB::QueryOneValue($query);
            if ($imageID) return $imageID;
            else return false;
        }else return false;
    }
    
    public function delete_file($file_path){
        $query = 'SELECT `imageID` FROM greeny_images 
                  WHERE `src` = "'.$file_path.'"';
        
        $imageID = DB::QueryOneValue($query);
        
        $query = 'DELETE FROM greeny_images
                  WHERE `imageID` = "'.$imageID.'"';
        
        $res = DB::Query($query);

        if ($res) return $imageID;
        else return false;
    }
    
    public function val_autoinc_greene_images(){
        $query = 'SHOW TABLE STATUS LIKE "'.TablesNames::$IMAGES_TABLE_NAME.'"';
        return ($res = DB::QueryToArray($query)) ? $res[0]['Auto_increment'] : false;
    }
    
    public function get_array_tag_images_ID($tagID, $ok_file_name = false){
        $fields = ' ti.`imageID` ';
        $from = ' FROM `fb_TagsImages` ti ';
        $join = null;
        $cond = null;
        
        if ($tagID === null) 
            return false;
        elseif ($tagID == 0){
            $fields = ' img.`imageID`, img.`src`, img.`caption`, img.`description` ';
            $from = ' FROM `'.TablesNames::$IMAGES_TABLE_NAME.'` img ';
            $cond = ' WHERE img.`srcThumbnail` IS NOT NULL 
                            AND NOT img.`imageID` IN (SELECT ti.`imageID` FROM `fb_TagsImages` ti)';
        }else 
            $cond = ' WHERE ti.`tagID` = "'.$tagID.'" ';
        
        if (($tagID != 0) && $ok_file_name){ 
            $fields .= ', img.`src`, img.`caption`, img.`description` ';
            $join = ' INNER JOIN `'.TablesNames::$IMAGES_TABLE_NAME.'` img ON  ti.`imageID` = img.`imageID` ';
        }
        
        $query = 'SELECT
                  '.$fields
                   .$from
                   .$join
                   .$cond;

        if ($res = DB::QueryToArray($query)){
            $ar = array();
            if ($ok_file_name){
                //получаем список тегов (tagID) в которых указано фото
                foreach ($res as &$item){
                    $query = 'SELECT `tagID` FROM `fb_TagsImages` WHERE `imageID` = "'.$item['imageID'].'"';
                    $tagsID = DB::QueryToArray($query);
                    //приводим к виду
                    foreach ($tagsID as &$it)
                        $it = $it['tagID'];
                    $item['tagsID'] = $tagsID;
                }
                
                return $res;
            }else{
                foreach ($res as $item)
                    $ar[] = $item['imageID'];
            }
                
            return $ar;
        }else
            return false;    
    }
    
}


?>
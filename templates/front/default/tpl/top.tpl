<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    {if (isset($PAGE.isNoindex) and $PAGE.isNoindex)}<meta name="robots" content="noindex,nofollow">{/if}
    <title>{if isset($PAGE.title)}{$PAGE.title}{/if}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width" />
    {if isset($PAGE.description)}<meta name="description" content="{$PAGE.description}" />{/if}
    {if isset($PAGE.keywords)}<meta name="keywords" content="{$PAGE.keywords}" />{/if}
    <link rel="shortcut icon" href="/favicon.ico" />
</head>
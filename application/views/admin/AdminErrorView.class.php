<?php
/**
 * Представление ошибки в админке
 *
 * @version 1.1
 *
 */
class AdminErrorView extends AdminView
{
    public function __construct($msg, $link = null, $params = null)
    {
        parent::__construct();
		if (AppSettings::$ADMIN_TEMPLATE_COL == 1)
		{
			$this -> tpl -> tplPath = $this -> tplsPath . 'single_column.tpl';
		}
		else
		{
			$this -> tpl -> tplPath = $this -> tplsPath . 'main.tpl';
		}
		$this -> tpl -> contentTpl = $this -> tplsPath . 'error.tpl';
		ViewData::Assign('ERROR_MSG', $msg);
		ViewData::Assign('REDIRECT_URL', $link);
		ViewData::Assign('PAGE_TITLE', 'Ошибка!');
    }
}
?>
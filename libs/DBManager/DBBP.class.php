<?php
require_once('DB.class.php'); // опираемся на этот функционал

/**
 * 
 * DBBP  Data Base of BitPlatform
 * 
 * Готовые решения для извлечения данных связанных со стандартными сущностимя (таблицами) битплатформы
 * 
 * Это переходный класс (не стоит воспринимать его как архитектурный шедевр),
 * но тем не менее он предоставляет относительно быстрый доступ к связанным данным.
 * 
 * Тут предполагается собирать SQL "общего употребления" для работы с данными в BitPlatform
 */
class DBBP
{
    
    /**
     * @var string главная таблица о страницах
     */
    public $pageTableName = 'greeny_pageStructure';
    /**
     * @var string название поля внешнего ключа (ссылка на доп. поля)
     */
    public $pageTableForeignKeyFieldName = 'contentID';
    /**
     * @var string название поля ссылки на родительскую страницу (и поля и род. страница находятся в $pageTableName)
     */
    public $pageTableParentIdFieldName = 'parentID';
    
    /**
     * Получит все страницы - непосредственные потомки данной 
     * со связанными данными из отдельной таблицы
     * 
     * @param int    $parentPageId                  номер родительской страницы
     * @param string $dataTableName                 имя связанной таблицы
     * @param string $dataTableMainIdFieldName      имя поля главного ключа связанной таблицы
     * @param string $orderByDataField              поле по которому будем сортировать (тут предполагается, что это поле из связанной таблицы) 
     * @return array
     */
    public function getAllPagesByParentIdWithData(
            $parentPageId, 
            $dataTableName, 
            $dataTableMainIdFieldName, 
            $orderByDataField)
    {          
        $query = "SELECT *
                  FROM   `$this->pageTableName`  tb LEFT JOIN  `$dataTableName` tbd  ON
                      tb.`$this->pageTableForeignKeyFieldName` = tbd.`$dataTableMainIdFieldName`
                  WHERE tb.`$this->pageTableParentIdFieldName` = $parentPageId ORDER BY tbd.`$orderByDataField` DESC";

        $result = DB::QueryToArray($query);
        return  $result;
    }
    
    /**
     * Вытащит одну страницу по псевдониму + данные из связанной таблицы
     * 
     * @param string $pageAlias
     * @param string $dataTableName
     * @param string $dataTableMainIdFieldName
     * @return string
     */
    public function getOnePageWithData(
            $pageAlias, 
            $dataTableName, 
            $dataTableMainIdFieldName
            )
    {          
        $query = "SELECT *
                  FROM   `$this->pageTableName`  tb LEFT JOIN  `$dataTableName` tbd  ON
                      tb.`$this->pageTableForeignKeyFieldName` = tbd.`$dataTableMainIdFieldName`
                  WHERE tb.`alias` = '$pageAlias' LIMIT 1;";

        $result = DB::QueryOneRecordToArray($query);
        
        return  $result;
    }
}
?>
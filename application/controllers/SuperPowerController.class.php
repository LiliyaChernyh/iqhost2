<?php
/**
 * Общий контроллер для админки и  фронтенда
 *
 * @version 1.1
 */
class SuperPowerController extends Controller 
{
    /**
	 * Менеджер настроек
	 *
	 * @var DBSettingsManager
	 */
	protected $settingsManager = null;
	
	/**
	 * View
	 *
	 * @var ViewResult
	 */
	protected $view;
        
        /**
	 * Model
	 *
	 */
	protected $model = null;
    
        /**
         * Алиас текущего контроллера
         *
         * @var string
         */
        protected $controllerAlias = null;
	
	public function __construct()
	{
	    // Инициализируем менеджер настроек
            $this -> settingsManager = CachingDBSettingsManager::getInstance();
            
            // Получим алиас текущего контроллера
            $app = Application::GetApplication();
            $this -> controllerAlias = $app -> GetRouter() -> GetURL($app -> GetRoute() -> GetControllerName());
	}
        
        protected function setPaginator($itemsOnPage, $itemsNumber, $currentPage /*, $prefixUrl*/)
	{
	    if ($itemsNumber <= $itemsOnPage) return;
	    $paginator = array();
	    $pagesTotal = ceil($itemsNumber / $itemsOnPage);
	    if ($pagesTotal == 1) return;
	    $paginator['itemsOnPage'] = $itemsOnPage;
	    $paginator['pageCurrent'] = $currentPage;
	    $paginator['pageCount'] = $pagesTotal;
	    ViewData::Assign('PAGINATOR', $paginator);
	}
}
?>
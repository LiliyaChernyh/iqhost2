<?php

/**
 * Контрол для управления связями многие ко многим.
 * Имеет два представления: в виде группы чекбоксов и
 * селекта с возможностью множественного выбора.
 *
 * @version 3.5
 *
 */
class MultipleSelect extends Control implements IFlushingControl {

    /**
     * Максимальный размер множественного выбора.
     * Число вариантов выбора, умещающихся в окошко контрола без прокрутки.
     *
     * @var int
     */
    public static $MaxSelectSize = 10;

    /**
     * Имя таблицы со значениями
     *
     * @var string
     */
    private $referentTableName = null;

    /**
     * Имя поля с человеко-понятным значением
     *
     * @var string
     */
    private $listingKey = null;

    /**
     * Имя таблицы, хранящей множественные
     *
     * @var string
     */
    private $linkingTableName = null;

    /**
     * Тип отображения элемента управления.
     * Возможные значения: 'select' (default), 'checkbox'
     *
     * @var string
     */
    private $controlType = 'select';

    /**
     * Порядок сортировки строк в таблице-справочнике,
     * откуда будут браться опшены селекта.
     *
     * Подставляется в DBTableManager -> SortOrder
     *
     * @var string
     */
    private $orderBy = null;
    private $condition = null;
    private $parentKey = null;
    private $counter = null;

    public function InitControl($value = null, $rowID = null)
    {
        $this->LoadSettings();
        $this->controlTplPath = ($this->controlType == 'select') ?
                parent::$CONTROLS_PATH . __CLASS__ . '/control_select.tpl' :
                parent::$CONTROLS_PATH . __CLASS__ . '/control_checkboxes.tpl';

        $this->controlTplVars = array();
        $this->controlTplVars['NAME'] = $this->name;
        $this->controlTplVars['TIP'] = $this->tip;
        $this->controlTplVars['SIZE'] = $this->controlSettings['size'];
        $this->controlTplVars['CLASS'] = $this->controlSettings['className'];

        if ($this->required)
            $this->controlTplVars['REQUIRED'] = 1;
        if ($this->disabled)
            $this->controlTplVars['READONLY'] = 1;

        if (isset($this->tableName))
            $this->controlTplVars['tableName'] = $this->tableName;
        $this->controlTplVars['formIndex'] = (isset($this->formIndex)) ? $this->formIndex : 0;

        // Вытаскиваем значения ReferentTable - это та таблица, в которой хранятся option'ы селекта
        $referentTableManager = new DBTableManager($this->referentTableName);
        $referentTableManager->SortOrder = empty($this->orderBy) ? '"' . $this->listingKey . '"' : $this->orderBy . "," . $this->listingKey;
        $referentTableRows = $referentTableManager->Select($this->condition);

        // Связывающая таблица
        $selectedRows = array();
        if ($rowID != null) {
            $linkingTableManager = new DBTableManager($this->linkingTableName);
            $linkingTableSelectedRows = $linkingTableManager->Select(array($this->controlsRegistry->activeTableStructure->PrimaryKey->Name => $rowID));
            $cnt = count($linkingTableSelectedRows);
            for ($i = 0; $i < $cnt; $i++)
                $selectedRows[] = $linkingTableSelectedRows[$i][$referentTableManager->TableStructure->PrimaryKey->Name];
        }

        $cnt = count($referentTableRows);
        $options = '';
        if ($cnt == 0) {
            $this->controlHTML = 'no rows';
            $this->name = $this->name;
            $this->type = __CLASS__;
            $this->description = $this->description;
            return;
        }

        if ($this->parentKey) {
            //FB::log($referentTableRows,"FB");
//            $optData = new Tree("--&nbsp;");
            $optData = new Tree();
            $referentTableRows = $optData->SortTree($referentTableRows, $referentTableManager->TableStructure->PrimaryKey->Name, $this->listingKey, $this->parentKey, 1);
            $cnt = sizeof($referentTableRows);
        }

        $this->controlTplVars['OPTIONS'] = array();
        for ($i = 0; $i < $cnt; $i++) {
            $option = array();
            $option['TEXT'] = $referentTableRows[$i][$this->listingKey] . ((!empty($this->counter) && isset($referentTableRows[$i][$this->counter])) ? " (" . $referentTableRows[$i][$this->counter] . ")" : '');
            $option['VALUE'] = $referentTableRows[$i][$referentTableManager->TableStructure->PrimaryKey->Name];
            $option['SELECTED'] = ($rowID != null && in_array($referentTableRows[$i][$referentTableManager->TableStructure->PrimaryKey->Name], $selectedRows));
            $option['level'] = (isset($referentTableRows[$i]['level']) ? $referentTableRows[$i]['level'] : 0);

            if ($this->controlType != 'select')
                $option['ID'] = $this->name . 'Label' . $i;
            $this->controlTplVars['OPTIONS'][] = $option;
        }
        $this->type = __CLASS__;
    }

    /**
     * Сюда приходит массив ID полей в ReferentTable
     *
     * @param array $data
     * @param int $rowID
     */
    public function Flush(&$data, $rowID)
    {
        $this->LoadSettings();
        $this->refreshFormAfterFlush = true;

        $linkingTableManager = new DBTableManager($this->linkingTableName);
        $referentTableStructure = new DBTableStructure($this->referentTableName);
        $linkingTableStructure = new DBTableStructure($this->linkingTableName);

        $oldData = DB::QueryToArray("SELECT " . $referentTableStructure->PrimaryKey->Name . " FROM " . $this->linkingTableName . " WHERE " . $this->controlsRegistry->activeTableStructure->PrimaryKey->Name . " = " . $rowID, null, $referentTableStructure->PrimaryKey->Name);

//        FB::log($oldData, "OLD Data");
//        FB::log($data, "DATA");
        // Все пусто, тут больше нечего делать
        if (empty($data) && empty($oldData)) {
//            FB::log("Nothing for change");
            return;
        }
        // пусто во входящих, удаляем все
        elseif (empty($data)) {
//            FB::log("Remove all");
            $linkingTableManager->Delete(array($this->controlsRegistry->activeTableStructure->PrimaryKey->Name => $rowID), true);
        }
        elseif (empty($oldData)) {
//            FB::log("Insert all");
            // нет старых записей, делаем инсерт всего
            foreach ($data as $referentID) {
                if (is_numeric($referentID) && $referentID > 0) {
                    $linkingTableManager->Insert(
                            array(
                                $this->controlsRegistry->activeTableStructure->PrimaryKey->Name => $rowID,
                                $referentTableStructure->PrimaryKey->Name => $referentID
                            )
                    );
                }
            }
        }
        else {
            $dataToInsert = $data;
            $dataToDelete = $oldData;

            foreach ($oldData as $odkey => $odval) {
                if (in_array($odval, $data)) {
//                    FB::log("Skip " . $odval);
                    unset($dataToDelete[$odkey]);
                    unset($dataToInsert[array_search($odval, $dataToInsert)]);
                }
            }
            // Удаляем неподтвержденные старые данные
            if (!empty($dataToDelete)) {
//                FB::log($dataToDelete, "Remove data");
                foreach ($dataToDelete as $referentID) {
                    $linkingTableManager->Delete(array(
                        $this->controlsRegistry->activeTableStructure->PrimaryKey->Name => $rowID,
                        $referentTableStructure->PrimaryKey->Name => $referentID));
                }
            }
            //Вставляем новые записи
            if (!empty($dataToInsert)) {
//                FB::log($dataToInsert, "Add data");
                $fields = array();
                foreach ($dataToInsert as $referentID) {
                    $fields[] = array(
                        $this->controlsRegistry->activeTableStructure->PrimaryKey->Name => $rowID,
                        $referentTableStructure->PrimaryKey->Name => $referentID
                    );
                }
                $linkingTableManager->InsertRows($fields);
            }
        }
    }

    public function PostBackHandler(&$data, $rowID = null)
    {
        
    }

    protected function LoadSettings()
    {
        $errmsg = $this->name . ' is undefined. It should be defined in FieldsRegistry';
        if (!empty($this->controlSettings['referentTable']))
            $this->referentTableName = $this->controlSettings['referentTable'];
        else
            throw new Exception('Referent Table for ' . $errmsg);

        if (!empty($this->controlSettings['listingKey']))
            $this->listingKey = $this->controlSettings['listingKey'];
        else
            throw new Exception('listing Key for ' . $errmsg);

        if (!empty($this->controlSettings['linkingTable']))
            $this->linkingTableName = $this->controlSettings['linkingTable'];
        else
            throw new Exception('linking Table for ' . $errmsg);

        if (!empty($this->controlSettings['controlType']))
            $this->controlType = $this->controlSettings['controlType'];

        if (!empty($this->controlSettings['orderBy']))
            $this->orderBy = $this->controlSettings['orderBy'];

        if (!empty($this->controlSettings['condition']))
            $this->condition = $this->controlSettings['condition'];

        if (!empty($this->controlSettings['parentKey']))
            $this->parentKey = $this->controlSettings['parentKey'];

        if (!empty($this->controlSettings['counter']))
            $this->counter = $this->controlSettings['counter'];
    }

}

<h1>Список пользователей</h1>

{literal}
<script type="text/javascript">
    $(document).ready(function(){
        $('#showFiltersBtn').click(function(){
            $('#filterForm').toggle();
        });
    });
</script>
{/literal}

<div style="text-align: right; margin: -30px 0 5px 0;">
<form action="" method="post">
    <input type="button" id="showFiltersBtn" value="Фильтры" />
    <input type="button" id="goAddUser" class="goToBtn" value="Добавить пользователя" />
    <input type="hidden" id="goAddUser_link" value="{$SECTION_ALIAS}add/" />
</form>
</div>
{include file='users/filters.tpl'}

{if $USERS}
{include file='paginator.tpl'}
<table id="entriesList" class="highlightable users">
    <thead>
        <tr>
            <th style="width: 200px;">Логин</th>
            <th>E-mail</th>
            <th>ФИО</th>
            <th>Тип пользователя</th>
            <th>Дата регистрации</th>
            <th>Активность</th>
            <th style="width: 15px;">Инфо</th>
            <th style="width: 15px;">Удал</th>
        </tr>
    </thead>
    <tbody>
    {foreach from=$USERS item=USER}
        <tr>
            <td style="display: none;"><a href="{$SECTION_ALIAS}edit/{$USER.userID}/"></a></td>
            <td>
                {$USER.login}
            </td>
            <td>
                {$USER.email}
            </td>
            <td>
                {$USER.FIO}
            </td>
            <td>
                {$USER.roleName}
            </td>
            <td>
                {$USER.registrationDate|date_format:"%d.%m.%Y %H:%M:%S"}
            </td>
            <td style="text-align: center;">
                {if $USER.isActive == 1}
                    <span style="color: green;">Активен</span>
                {else}
                    <span style="color: red;">Не активен</span>
                {/if}
            </td>
            <td style="text-align: center;">
                <a href="{$SECTION_ALIAS}info/{$USER.userID}/"><img src="{$ADMIN_IMAGES}icons/page_white_edit.png"></a>
            </td>
            <td style="text-align: center;">
                <a href="{$SECTION_ALIAS}delete/{$USER.userID}/" onclick="if (!confirm('Вы действительно хотите безвозвратно удалить пользователя?')) return false;"><img src="{$ADMIN_IMAGES}icons/delete_icon.png"></a>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>
{else}
    Список пользователей пуст.
{/if}
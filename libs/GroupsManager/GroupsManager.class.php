<?php

/**
 * Класс работы с группами.
 *
 * текущая версия: 1.0.0
 */
class GroupsManager
{
	function __construct()
	{
		$this -> InitGroupsTable();
	}
	
	/**
	 * Менеджер таблицы групп пользователей
	 *
	 * @var DBTableManager
	 */
	private $groupsTable = null;
	
	/**
	 * Инициализирует менеджер таблицы групп пользователей
	 *
	 */
	private function InitGroupsTable()
	{
		if ($this -> groupsTable == null)
			$this -> groupsTable = new DBTableManager(GROUPS_TABLE_NAME);
	}

	/**
	 * Добавляет новую группу.
	 * Перед добавлением проверяется корректность всех параметров.
	 * Возвращает ID новой записи.
	 *
	 * @param array $attributes
	 * @return int
	 */
	public function AddGroup($attributes)
	{
		/**
		 * Проверяем корректность переданных данных
		 */
		if ((!isset($attributes['name'])) or (!is_string($attributes['name'])) or (empty($attributes['name'])))
			throw new ArgumentException("Название группы должно быть непустой строкой");
			
		return $this -> groupsTable -> Insert( $attributes );
	}

	/**
	 * Редактирует данные группы
	 *
	 * @param array $attributes
	 * @param int $groupID
	 */
	public function EditGroup($attributes, $groupID)
	{
		/**
		 * Проверяем корректность переданных данных
		 */
		if ((!isset($attributes['name'])) or (!is_string($attributes['name'])) or (empty($attributes['name'])))
			throw new ArgumentException("Название группы должно быть непустой строкой");
		
		if (empty($groupID) || !is_numeric($groupID))
			throw new ArgumentException("Идентификатор группы должен быть числом");
		
		$this -> groupsTable -> Update($attributes, $groupID);
	}

	public function GetGroup($attributes)
	{
		$group = array();
		$groups = $this -> groupsTable -> Select($attributes);
		if (isset($groups[0]))
			$group = $groups[0];

		return $group;
	}
	
	public function GetGroups()
	{
		return $this -> groupsTable -> Select();
	}
	
	public function DeleteGroup($groupID)
	{
		$this -> groupsTable -> Delete( array('groupID' => $groupID) );
	}
	
	public function GetGroupsNumber($attributes = null)
	{
		return $this -> groupsTable -> Count( $attributes );
	}
	
	public function AddUserToGroup($groupID, $userID)
	{
		if (!is_numeric($groupID))
			throw new Exception("Неверно задан ID группы [$groupID]");
			
		if (!is_numeric($userID))
			throw new Exception("Неверно задан ID пользователя [$userID]");
			
		$usersManager = new UsersManager();
		$userNumber = $usersManager -> GetUsersNumber( array('userID' => $userID) );
		if ($userNumber != 1)
			throw new Exception("Пользователя с ID [$userID] не существует");
			
		$groupNumber = $this -> GetGroupsNumber( array('groupID' => $groupID) );
		if ($groupNumber != 1)
			throw new Exception("Группы с ID [$groupID] не существует");
			
		// Проверям, может этот пользователь уже состоит в этой группе
		$query = "SELECT count(userID) as cnt
					FROM `".USERSGROUPS_TABLE_NAME."`
					WHERE 	`userID` 	= $userID
						AND	`groupID`	= $groupID";
		
		$result = DB::Query($query);
		$res = DB::FetchAssoc($result);
		if ($res['cnt'] != 0)
			throw new Exception("Пользователь уже состоит в данной группе");
			
		$query = "INSERT INTO `".USERSGROUPS_TABLE_NAME."` (`userID`, `groupID`) VALUES ($userID, $groupID)";
		
		DB::Query($query);
	}
	
	public function RemoveUserFromGroup($groupID, $userID)
	{
		if (!is_numeric($groupID))
			throw new Exception("Неверно задан ID группы [$groupID]");
			
		if (!is_numeric($userID))
			throw new Exception("Неверно задан ID пользователя [$userID]");
			
		$usersManager = new UsersManager();
		$userNumber = $usersManager -> GetUsersNumber( array('userID' => $userID) );
		if ($userNumber != 1)
			throw new Exception("Пользователя с ID [$userID] не существует");
			
		$groupNumber = $this -> GetGroupsNumber( array('groupID' => $groupID) );
		if ($groupNumber != 1)
			throw new Exception("Группы с ID [$groupID] не существует");
			
		$query = "DELETE FROM `".USERSGROUPS_TABLE_NAME."` 
					WHERE 	`userID` = '$userID'
						AND `groupID` = '$groupID'";
		
		DB::Query($query);
	}
}
?>
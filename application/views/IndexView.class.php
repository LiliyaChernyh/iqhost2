<?php
/**
 * Представление для главной страницы сайта
 * 
 * @version 1.0
 *
 */
class IndexView extends FrontView
{
    public function __construct()
    {
        parent::__construct('index.tpl');
    }
}
?>
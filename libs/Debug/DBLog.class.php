<?php
/**
 * Логирование событий в БД
 *
 * @author grom
 */
class DBLog
{
	const TYPE_INFO = 'info';
	const TYPE_WARNING = 'warning';
	const TYPE_ERROR = 'error';

	/**
	 * @var bool Флаг включения лога
	 */
	static $LOG_ENABLED = false;

	/**
	 * Добавляет сообщение в лог
	 *
	 * @param string $msg Сообщение (может содержать подстановки типа %s, см sprintf())
	 * @param array|mixed $args Массив с параметрами
	 * @param string $type Тип сообщение (по умолчанию 'info'). Используйте константы класса
	 * @param int $userID Идентификатор пользователя
	 * @param int $ip IP-адрес пользователя
	 */
	public static function Log($msg, $args = null, $type = self::TYPE_INFO, $userID = null, $ip = null)
	{
		if (!self::$LOG_ENABLED)
			return;
		if (!empty($args))
		{
			if (!is_array($args))
				$args = array($args);
			$msg = vsprintf($msg, $args);
		}

		if (is_null($userID) and !empty($_SESSION['userID']))
			$userID = $_SESSION['userID'];

		if (is_null($ip))
			$ip = ip2long($_SERVER['REMOTE_ADDR']);
		$query = '
			INSERT DELAYED INTO `' . TablesNames::$LOG_TABLE_NAME . '`
				(`userID`, `ip`, `type`, `message`)
			VALUES
			(
				' . ((is_null($userID)) ? 'NULL' : (int)$userID ) . ',
				' . (int)$ip . ',
				' . DB::Quote($type) . ',
				' . DB::Quote($msg) . '
			)
		';
		DB::Query($query);
	}

	/**
	 * Добавляет информацию в лог
	 *
	 * @param string $msg Сообщение (может содержать подстановки типа %s, см sprintf())
	 * @param array|mixed $args Массив с параметрами
	 * @param int $userID Идентификатор пользователя
	 * @param int $ip IP-адрес пользователя
	 */
	public static function Info($msg, $args = null, $userID = null, $ip = null)
	{
		self::Log($msg, $args, self::TYPE_INFO, $userID, $ip);
	}

	/**
	 * Добавляет предупреждение в лог
	 *
	 * @param string $msg Сообщение (может содержать подстановки типа %s, см sprintf())
	 * @param array|mixed $args Массив с параметрами
	 * @param int $userID Идентификатор пользователя
	 * @param int $ip IP-адрес пользователя
	 */
	public static function Warning($msg, $args = null, $userID = null, $ip = null)
	{
		self::Log($msg, $args, self::TYPE_WARNING, $userID, $ip);
	}

	/**
	 * Добавляет ошибку в лог
	 *
	 * @param string $msg Сообщение (может содержать подстановки типа %s, см sprintf())
	 * @param array|mixed $args Массив с параметрами
	 * @param int $userID Идентификатор пользователя
	 * @param int $ip IP-адрес пользователя
	 */
	public static function Error($msg, $args = null, $userID = null, $ip = null)
	{
		self::Log($msg, $args, self::TYPE_ERROR, $userID, $ip);
	}

	/**
	 * Очищает таблицу логов. Обычный truncate table на ней не работает,
	 * так что используем workaround
	 *
	 */
	public static function ClearLog()
	{
		DB::Query('ALTER TABLE `' . TablesNames::$LOG_TABLE_NAME . '` ENGINE=BLACKHOLE');
		DB::Query('ALTER TABLE `' . TablesNames::$LOG_TABLE_NAME . '` ENGINE=ARCHIVE');
	}
}
<?php

if (strpos($_SERVER['REQUEST_URI'], 'index.php') !== false)
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: /');
    exit();
}

function trace($toTrace) {echo '<pre>'; print_r($toTrace); echo '</pre>';}

require_once('IncPaths.class.php');
IncPaths::Init();

/* Autoload speedup here      */   
require_once('includes.php');

require_once(IncPaths::$CORE_PATH . 'Autoload.class.php');
Autoload::Register();

Request::Init();
if (!empty(Request::$GET['page_alias']) && strpos(Request::$GET['page_alias'], AppSettings::$ADMIN_ALIAS) !== 0 && Request::$GET['page_alias']{strlen(Request::$GET['page_alias'])-1} != '/')
{
    $params = str_replace('page_alias='. Request::$GET['page_alias'], '', $_SERVER['QUERY_STRING']);
    if (!empty($params))
        $params = '?'. substr($params, 1);

    $url = Request::$GET['page_alias'] .'/'. $params;
    if ($url{0} != '/')
        $url = '/'. $url;
        
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: '. $url);
    exit();
}

CMSApplication::getInstance() -> Run();
?>
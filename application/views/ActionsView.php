<?php

class ActionsView extends FrontView {

    public function __construct() {
        parent::__construct();
        $this->tplsPath .= 'actions/';
    }

    public function Index() {
        $this->tpl->contentTpl = $this->tplsPath . 'index.tpl';
        return $this;
    }

    public function Item() {
        $this->tpl->contentTpl = $this->tplsPath . 'item.tpl';
        return $this;
    }

}

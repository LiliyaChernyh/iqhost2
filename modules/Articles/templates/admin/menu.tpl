<ul>
    <li{if $MENU_CURRENT == 'sections'} class="active"{/if}>
        <a href="{$MODULE_ALIAS}sections/" title="Редактор категорий">Категории</a>
    </li>
    {if isset($SECTIONS)}
        <ul class="sections">
            {foreach from=$SECTIONS item=SECTION}
                <li>
                    <a href="{$MODULE_ALIAS}list/{$SECTION.asectionID}" title="Показать статьи из категории «{$SECTION.caption}»">
                        <span style="{if (isset($CUR_SECTION) && ($CUR_SECTION == $SECTION.asectionID))}font-weight: bold;{/if}">
                            {$SECTION.caption}
                        </span>
                    </a>
                </li>
            {/foreach}
        </ul>
    {/if}
    <li>
        <a href="{$MODULE_ALIAS}" title="Показать новости из всех категорий">Все статьи (<span class="total">{$COUNT_ARTICLES.total}</span>)</a>
    </li>
    <li{if $MENU_CURRENT == 'published'} class="active"{/if}>
        <a href="{$MODULE_ALIAS}published/" title="Показать опубликованные новости">Опубликованные статьи (<span class="published">{if empty($COUNT_ARTICLES.published) AND $COUNT_ARTICLES.published !== 0}0{else}{$COUNT_ARTICLES.published}{/if}</span>)</a>
    </li>
    <li{if $MENU_CURRENT == 'unpublished'} class="active"{/if}>
        <a href="{$MODULE_ALIAS}unpublished/" title="Показать неопубликованные новости">Неопубликованные статьи (<span class="unpublished">{if empty($COUNT_ARTICLES.unpublished) AND $COUNT_ARTICLES.unpublished !== 0}0{else}{$COUNT_ARTICLES.unpublished}{/if}</span>)</a>
    </li>
     <li{if $MENU_CURRENT == 'recycledsection'} class="active"{/if}>
        <a href="{$MODULE_ALIAS}recycledsection/" title="Список удаленных категорий">Корзина удаленных категорий (<span class="recycled">{if empty($COUNT_SECTIONS.total) AND $COUNT_SECTIONS.total !== 0}0{else}{$COUNT_SECTIONS.total}{/if}</span>)</a>
    </li>
    <li{if $MENU_CURRENT == 'recycled'} class="active"{/if}>
        <a href="{$MODULE_ALIAS}recycled/" title="Список удаленных статей">Корзина удаленных статей (<span class="recycled">{if empty($COUNT_ARTICLES.recycled) AND $COUNT_ARTICLES.recycled !== 0}0{else}{$COUNT_ARTICLES.recycled}{/if}</span>)</a>
    </li>
</ul>
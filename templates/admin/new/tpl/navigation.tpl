<div class="menuBlock">
	{if isset($USER)}
	<ul>
		<li {if $CURRENT_SECTION_ALIAS == "structure"} class="active"{/if}>
			<a href="{$ADMIN_ALIAS}structure/" title="Управления структурой сайта">Страницы</a>
			{if $CURRENT_SECTION_ALIAS == "structure"}
				{if isset($MENU_TPL)}
					{include file="$MENU_TPL"}
				{/if}
			{/if}
		</li>
			<li {if $CURRENT_SECTION_ALIAS == "anyeditor"}class='active'{/if}>
			<a href="{$ADMIN_ALIAS}AnyEditor/" title="">Данные</a>
			{if $CURRENT_SECTION_ALIAS == "anyeditor"}
				{if isset($MENU_TPL)}
					{include file="$MENU_TPL"}
				{/if}
			{/if}
		</li>
 <li {if $CURRENT_SECTION_ALIAS == "custommenu"}class='active'{/if}>

			<a href="{$ADMIN_ALIAS}custommenu/" title="Управление меню">Меню</a>
		</li>
		{if $USER.role == 'admin'}
			{*			<li {if $CURRENT_SECTION_ALIAS == "custommenu"}class='active'{/if}>
			<a href="{$ADMIN_ALIAS}custommenu/" title="Управление меню">Меню</a>
		</li>
		<li {if $CURRENT_SECTION_ALIAS == "store"}class='active'{/if}>
			<a href="{$ADMIN_ALIAS}store/" title="Управление меню">Магазин</a>
			{if $CURRENT_SECTION_ALIAS == "store"}
				{if isset($MENU_TPL)}
					{include file="$MENU_TPL"}
				{/if}
			{/if}
		</li>
		*}
		<li {if $CURRENT_SECTION_ALIAS == "profile"}class='active'{/if}>
			<a href="{$ADMIN_ALIAS}profile/" title="Управление учетной записью">Профиль</a>
			{if $CURRENT_SECTION_ALIAS == "profile"}
				{if isset($MENU_TPL)}
					{include file="$MENU_TPL"}
				{/if}
			{/if}
		</li>
		{if $USER.role == 'admin'}
			<li {if $CURRENT_SECTION_ALIAS == "users"}class='active'{/if}>
				<a href="{$ADMIN_ALIAS}users/" title="Управление учётными записями">Пользователи</a>
				{if $CURRENT_SECTION_ALIAS == "users"}
					{if isset($MENU_TPL)}
						{include file="$MENU_TPL"}
					{/if}
				{/if}
			</li>
			<li {if $CURRENT_SECTION_ALIAS == "settings"}class='active'{/if}>
				<a href="{$ADMIN_ALIAS}settings/" title="Настройки сайта">Настройки</a>
				{if $CURRENT_SECTION_ALIAS == "settings"}
					{if isset($MENU_TPL)}
						{include file="$MENU_TPL"}
					{/if}
				{/if}
			</li>
            <li {if $CURRENT_SECTION_ALIAS == "routing"}class='active'{/if}><a href="{$ADMIN_ALIAS}routing/" title="Роутинг">Роутинг</a></li>

            <li {if $CURRENT_SECTION_ALIAS == "fieldsregistry"}class='active'{/if}><a href="{$ADMIN_ALIAS}fieldsregistry/" title="Регистратор полей">Fieldregistry</a></li>
            <li {if $CURRENT_SECTION_ALIAS == "maintenance"}class='active'{/if}><a href="{$ADMIN_ALIAS}maintenance/" title="Обслуживание сайта">Обслуживание</a></li>
            <li {if $CURRENT_SECTION_ALIAS == "modules"}class='active'{/if}><a href="{$ADMIN_ALIAS}modules/" title="Управление учётными записями">Модули</a>
				{if $CURRENT_SECTION_ALIAS == "modules"}
					{if isset($MENU_TPL)}
						{include file="$MENU_TPL"}
					{/if}
				{/if}
			</li>
			<li {if $CURRENT_SECTION_ALIAS == "editor"}class='active'{/if}><a href="{$ADMIN_ALIAS}editor/" title="Управление учётными записями">Редактор</a>
				{if $CURRENT_SECTION_ALIAS == "editor"}
					{if isset($MENU_TPL)}
						{include file="$MENU_TPL"}
					{/if}
				{/if}
			</li>

            {if isset($MODULES)}
                {foreach from=$MODULES item=MODULE}
                    <li {if $CURRENT_SECTION_ALIAS == $MODULE.rootAlias}class='active'{/if}><a href="{$ADMIN_ALIAS}{$MODULE.rootAlias}/" title="{$MODULE.description}">{$MODULE.description}</a>
                        {if $CURRENT_SECTION_ALIAS == $MODULE.rootAlias}
                            {if isset($MENU_TPL)}
                                {include file="$MENU_TPL"}
                            {/if}
                        {/if}
                    </li>
                {/foreach}
            {/if}

		{/if}
		{/if}
	</ul>
	{/if}
</div>
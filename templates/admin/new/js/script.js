$(document).ready(function()
{
	$("select.select2").select2({});
	pageInit.init();
	jQuery('form').customForm({
		select: {
			elements: 'select.customSelect',
			structure: '<div class="selectArea"><a tabindex="-1" href="#" class="selectButton"><span class="center"></span><span class="right"><span>&nbsp;</span><span>&nbsp;</span></span></a><div class="disabled"></div></div>',
			optStructure: '<div class="selectOptions"><div class="custom-scroll"><ul></ul></div></div>',
			maxHeight: 350
		}
	});
	$('.custom-scroll').customScrollV({step:80});
	 multy()
    $('input[type=submit], input[type=button], input[type=image], .helper').addClass('handyButton');
    $('a, img, input, textarea, .helper').tooltip({showURL: false});

    $(".paginator").each(assignPaginator);
    $(".ddm").dropDownMenu();

    $('.goToBtn').click(LinkBtnClick);
	$(".openInNewWin").click(function (event) {
		event.preventDefault();
		event.stopPropagation();
		window.open($(this).attr("href"));
	})

    $(".jsLink").click(function (e) {
		e.preventDefault();
	});
$('.richEditor').nicEditor();
});

function multy(){
	var multi = $('.multi').each(function(){
		var hold = $(this);
		var button = hold.find('.miuli-button');
		var drop = hold.find('.drop-multi');
		var boxH = hold.innerHeight();
		var boxTop = hold.offset();
		var link = drop.find('a');
		var inp = hold.find('input');
		var text = '';

		$('body').append(drop);

		button.click(function(){
			if(!$(this).hasClass('open')){
				multi.not(hold).trigger('closeAll');
				$(this).addClass('open');
				drop.css({left:boxTop.left, top:boxTop.top+boxH});
			}
			else {
				$(this).removeClass('open');
				drop.css({left:-99999, top:-99999});
			}

			return false;
		});

		hold.bind('closeAll', function(){
			button.removeClass('open');
			drop.css({left:-99999, top:-99999});
		});

		drop.hover(function(){
			drop.addClass('hovering');
		}, function(){
			drop.removeClass('hovering');
		});

		jQuery('body').bind('click', function(){
			if(!drop.hasClass('hovering')){
				button.removeClass('open');
				drop.css({left:-99999, top:-99999});
			}
		});

		function goVal(){
			text = '';
			link.each(function(){
				if($(this).parent().hasClass('active')){
					text += $(this).text() + ', ';
				};
			});
			inp.val(text.slice(0, -2));
		}
		goVal();
		link.click(function(){
			if(!$(this).parent().hasClass('active')){
				$(this).parent().addClass('active');
				console.log('1');
				goVal();
			}
			else {
				$(this).parent().removeClass('active');
				goVal();
			}

			return false;
		});
	})
}

var pageInit = {
	init: function(){
		this.selectAll();
	},
	selectAll: function(){
		$('#sortProducts').each(function(){
			var hold = $(this);
			var selAll = hold.find('.select-all');
			var desAll = hold.find('.deselect-all');

			selAll.css({left:0});
			desAll.css({left:-9999});

			selAll.mousedown(function() {
				$(this).css({left:-9999});
				desAll.css({left:0});
			});
			desAll.mousedown(function() {
				$(this).css({left:-9999});
				selAll.css({left:0});
			});

		})
	}
}

/**
 * jQuery Vertical Custom Scroll v1.1.1
 * Copyright (c) 2013 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

jQuery.uaMatch=function(ua){ua=ua.toLowerCase();var match=/(chrome)[ \/]([\w.]+)/.exec(ua)||/(webkit)[ \/]([\w.]+)/.exec(ua)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua)||/(msie) ([\w.]+)/.exec(ua)||ua.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua)||[];return{browser:match[1]||"",version:match[2]||"0"};};if(!jQuery.browser){matched=jQuery.uaMatch(navigator.userAgent);browser={};if(matched.browser){browser[matched.browser]=true;browser.version=matched.version;}if(browser.chrome){browser.webkit=true;}else if(browser.webkit){browser.safari=true;}jQuery.browser=browser;};
var types=['DOMMouseScroll','mousewheel'];if($.event.fixHooks){for(var i=types.length;i;){$.event.fixHooks[types[--i]]=$.event.mouseHooks;}}$.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var i=types.length;i;){this.addEventListener(types[--i],handler,false);}}else{this.onmousewheel=handler;}},teardown:function(){if(this.removeEventListener){for(var i=types.length;i;){this.removeEventListener(types[--i],handler,false);}}else{this.onmousewheel=null;}}};$.fn.extend({mousewheel:function(fn){return fn?this.bind("mousewheel",fn):this.trigger("mousewheel");},unmousewheel:function(fn){return this.unbind("mousewheel",fn);}});
function handler(event){var orgEvent=event||window.event,args=[].slice.call(arguments,1),delta=0,returnValue=true,deltaX=0,deltaY=0;event=$.event.fix(orgEvent);event.type="mousewheel";if(orgEvent.wheelDelta){delta=orgEvent.wheelDelta/120;}if(orgEvent.detail){delta=-orgEvent.detail/3;}deltaY=delta;if(orgEvent.axis!==undefined&&orgEvent.axis===orgEvent.HORIZONTAL_AXIS){deltaY=0;deltaX=-1*delta;}if(orgEvent.wheelDeltaY!==undefined){deltaY=orgEvent.wheelDeltaY/120;}if(orgEvent.wheelDeltaX!==undefined){deltaX=-1*orgEvent.wheelDeltaX/120;}args.unshift(event,delta,deltaX,deltaY);return($.event.dispatch||$.event.handle).apply(this,args);}jQuery.easing['jswing']=jQuery.easing['swing'];jQuery.extend(jQuery.easing,{def:'easeOutQuad',swing:function(x,t,b,c,d){return jQuery.easing[jQuery.easing.def](x,t,b,c,d);},easeOutQuad:function(x,t,b,c,d){return-c*(t/=d)*(t-2)+b;},easeOutCirc:function(x,t,b,c,d){return c*Math.sqrt(1-(t=t/d-1)*t)+b;}});jQuery.fn.customScrollV=function(_options){var _options=jQuery.extend({lineWidth:16,step:100},_options);return this.each(function(){var _box=jQuery(this);if(_box.is(':visible')){if(_box.children('.scroll-content').length==0){var line_w=_options.lineWidth;var scrollBar=jQuery('<div class="vscroll-bar">'+'	<div class="scroll-up"></div>'+'	<div class="scroll-line">'+'		<div class="scroll-slider">'+'			<div class="scroll-slider-c"></div>'+'		</div>'+'	</div>'+'	<div class="scroll-down"></div>'+'</div>');_box.wrapInner('<div class="scroll-content"><div class="scroll-hold"></div></div>').append(scrollBar);var scrollContent=_box.children('.scroll-content');var scrollSlider=scrollBar.find('.scroll-slider');var scrollSliderH=scrollSlider.parent();var scrollUp=scrollBar.find('.scroll-up');var scrollDown=scrollBar.find('.scroll-down');var box_h=_box.height();var slider_h=0;var slider_f=0;var cont_h=scrollContent.height();var _f=false;var _f1=false;var _f2=true;var _t1,_t2,_s1,_s2;var span=$('<span style="position: absolute; width: 100%; height: 100%; z-index: 1000; display: none;" />');_box.append(span);var kkk=0,start=0,_time,flag=true;_box.css({position:'relative',overflow:'hidden',height:box_h});scrollContent.css({position:'absolute',top:0,left:0,zIndex:1,height:'auto'});scrollBar.css({position:'absolute',top:0,right:0,zIndex:2,width:line_w,height:box_h,overflow:'hidden'});scrollUp.css({width:line_w,height:line_w,overflow:'hidden',cursor:'pointer'});scrollDown.css({width:line_w,height:line_w,overflow:'hidden',cursor:'pointer'});slider_h=scrollBar.height();if(scrollUp.is(':visible'))slider_h-=scrollUp.height();if(scrollDown.is(':visible'))slider_h-=scrollDown.height();scrollSliderH.css({position:'relative',width:line_w,height:slider_h,overflow:'hidden'});slider_h=0;scrollSlider.css({position:'absolute',top:0,left:0,width:line_w,height:slider_h,overflow:'hidden',cursor:'pointer'});box_h=_box.height();cont_h=scrollContent.height();if(box_h<cont_h){_f=true;slider_h=Math.round(box_h/cont_h*scrollSliderH.height());if(slider_h<5)slider_h=5;scrollSlider.height(slider_h);slider_h=scrollSlider.outerHeight();slider_f=(cont_h-box_h)/(scrollSliderH.height()-slider_h);_s1=_options.step;_s2=(scrollSliderH.height()-slider_h)/3;scrollContent.children('.scroll-hold').css('padding-right',scrollSliderH.width());}else{_f=false;scrollBar.hide();scrollContent.css({width:_box.width(),top:0,left:0});scrollContent.children('.scroll-hold').css('padding-right',0);};var _top=0;scrollUp.bind('mousedown',function(){_top-=_s1;scrollCont();_t1=setTimeout(function(){_t2=setInterval(function(){_top-=4*slider_f;scrollCont();},20);},500);return false;}).mouseup(function(){if(_t1)clearTimeout(_t1);if(_t2)clearInterval(_t2);}).mouseleave(function(){if(_t1)clearTimeout(_t1);if(_t2)clearInterval(_t2);});scrollDown.bind('mousedown',function(){_top+=_s1;scrollCont();_t1=setTimeout(function(){_t2=setInterval(function(){_top+=4*slider_f;scrollCont();},20);},500);return false;}).mouseup(function(){if(_t1)clearTimeout(_t1);if(_t2)clearInterval(_t2);}).mouseleave(function(){if(_t1)clearTimeout(_t1);if(_t2)clearInterval(_t2);});scrollSliderH.click(function(e){if(_f2){_top=(e.pageY-scrollSliderH.offset().top-scrollSlider.outerHeight()/2)*slider_f;scrollCont();}else{_f2=true;};});var t_y=0;var tttt_f=(jQuery.browser.msie)?(true):(false);scrollSlider.mousedown(function(e){t_y=e.pageY-jQuery(this).position().top;_f1=true;return false;}).mouseup(function(){_f1=false;});jQuery('body').bind('mousemove',function(e){if(_f1){_f2=false;_top=(e.pageY-t_y)*slider_f;if(tttt_f)document.selection.empty();scrollCont();}}).mouseup(function(){_f1=false;});scrollSlider.bind('touchstart',function(e){if(_time)clearTimeout(_time);scrollSlider.stop();scrollContent.stop();kkk=e.originalEvent.touches?e.originalEvent.touches[0].pageY:e.originalEvent.pageY;e.preventDefault();e.stopPropagation();return false;}).bind('touchmove',function(e){if(_f){_f=false;var _tt=e.originalEvent.touches?e.originalEvent.touches[0].pageY:e.originalEvent.pageY;if(kkk>_tt)_top-=1*Math.abs(_tt-kkk);else _top-=-1*Math.abs(_tt-kkk);scrollCont();kkk=_tt;_f=true;if((_top>0)&&(_top+slider_h<scrollSliderH.height()))return false;}e.preventDefault();e.stopPropagation();return false;}).bind('touchend',function(e){e.preventDefault();e.stopPropagation();return false;});_box.bind('touchstart',function(e){if(_time)clearTimeout(_time);scrollSlider.stop();scrollContent.stop();kkk=e.originalEvent.touches?e.originalEvent.touches[0].pageY:e.originalEvent.pageY;start=kkk;flag=true;}).bind('touchmove',function(e){if(_f){var _tt=e.originalEvent.touches?e.originalEvent.touches[0].pageY:e.originalEvent.pageY;_f=false;if(kkk>_tt)_top-=-1*Math.abs(_tt-kkk)/(cont_h/box_h);else _top-=1*Math.abs(_tt-kkk)/(cont_h/box_h);if(Math.abs(kkk-_tt)>20)span.css({display:'block'});scrollCont();kkk=_tt;_f=true;_time=setTimeout(function(){flag=false;},200);if((_top>0)&&(_top+slider_h<scrollSliderH.height()))return false;}}).bind('touchend',function(e){if(flag&&Math.abs(start-kkk)>80){_top+=(start-kkk)/3;if(_top<0)_top=0;else if(_top+slider_h>scrollSliderH.height())_top=scrollSliderH.height()-slider_h;scrollSlider.animate({top:_top},{queue:false,easing:'easeOutCirc',duration:300*Math.abs(start-kkk)/40});scrollContent.animate({top:-_top*slider_f},{queue:false,easing:'easeOutCirc',duration:300*Math.abs(start-kkk)/40});}span.css({display:'none'});});scrollUp.bind('touchstart',function(){_top-=_s1;scrollCont();e.preventDefault();e.stopPropagation();return false;}).bind('touchend',function(e){e.preventDefault();e.stopPropagation();return false;}).bind('touchmove',function(e){e.preventDefault();e.stopPropagation();return false;});scrollDown.bind('touchstart',function(){_top+=_s1;scrollCont();e.preventDefault();e.stopPropagation();return false;}).bind('touchend',function(e){e.preventDefault();e.stopPropagation();return false;}).bind('touchmove',function(e){e.preventDefault();e.stopPropagation();return false;});document.body.onselectstart=function(){if(_f1)return false;};if(!_box.hasClass('not-scroll')){_box.bind('mousewheel',function(event,delta){if(_f){_top-=delta*_s1;scrollCont();if((_top>0)&&(_top/slider_f+slider_h<scrollSliderH.height()))return false;}});};function scrollCont(){if(_top<0)_top=0;else if(_top/slider_f+slider_h>scrollSliderH.height())_top=(scrollSliderH.height()-slider_h)*slider_f;scrollSlider.css('top',_top/slider_f);scrollContent.css('top',-_top);};this.scrollResize=function(){box_h=_box.height();cont_h=scrollContent.height();if(box_h<cont_h){_f=true;scrollBar.show();scrollBar.height(box_h);slider_h=scrollBar.height();if(scrollUp.is(':visible'))slider_h-=scrollUp.height();if(scrollDown.is(':visible'))slider_h-=scrollDown.height();scrollSliderH.height(slider_h);slider_h=Math.round(box_h/cont_h*scrollSliderH.height());if(slider_h<5)slider_h=5;scrollSlider.height(slider_h);slider_h=scrollSlider.outerHeight();slider_f=(cont_h-box_h)/(scrollSliderH.height()-slider_h);if(cont_h+scrollContent.position().top<box_h)scrollContent.css('top',-(cont_h-box_h));_top=-scrollContent.position().top/slider_f;scrollSlider.css('top',_top);_s1=_options.step;_s2=(scrollSliderH.height()-slider_h)/3;scrollContent.children('.scroll-hold').css('padding-right',scrollSliderH.width());}else{_f=false;scrollBar.hide();scrollContent.css({top:0,left:0});scrollContent.children('.scroll-hold').css('padding-right',0);};};setInterval(function(){if(_box.is(':visible')&&cont_h!=scrollContent.height())_box.get(0).scrollResize();},200);}else{this.scrollResize();};};})};
/**
 * jQuery Custom Form min v1.2.4
 * Copyright (c) 2012 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

jQuery.fn.customForm = jQuery.customForm = function(_options) {
	var _this = this;
	var methods = {
		destroy: function(){
			var elements;
			if(typeof this === 'function') {
				elements = $('select, input:radio, input:checkbox');
			}
			else{
				elements = this.add(this.find('select, input:radio, input:checkbox'));
			}
			elements.each(function(){
				var data = $(this).data('customForm');
				if(data){
					$(this).removeClass('outtaHere');
					if(data['events']) data['events'].unbind('.customForm');
					if(data['create']) data['create'].remove();
					if(data['resizeElement']) data.resizeElement = false;
					$(this).unbind('.customForm');
				}
			});
		},
		refresh: function(){
			if(typeof this === 'function') $('select, input:radio, input:checkbox').trigger('refresh');
			else this.trigger('refresh');
		}
	};

	if ( typeof _options === 'object' || ! _options ){
		if(typeof _this == 'function') _this = $(document);
		var options = jQuery.extend(true, {
			select: {
				elements: 'select.customSelect',
				structure: '<div class="selectArea"><a tabindex="-1" href="#" class="selectButton"><span class="center"></span><span class="right">&nbsp;</span></a><div class="disabled"></div></div>',
				text: '.center',
				btn: '.selectButton',
				optStructure: '<div class="selectOptions"><ul></ul></div>',
				maxHeight: false,
				topClass: 'position-top',
				optList: 'ul'
			},
			radio: {
				elements: 'input.customRadio',
				structure: '<div></div>',
				defaultArea: 'radioArea',
				checked: 'radioAreaChecked'
			},
			checkbox: {
				elements: 'input.customCheckbox',
				structure: '<div></div>',
				defaultArea: 'checkboxArea',
				checked: 'checkboxAreaChecked'
			},
			disabled: 'disabled',
			hoverClass: 'hover'
		}, _options);

		return _this.each(function() {
			var hold = jQuery(this);
			var reset = jQuery();
			if(this !== document) reset = hold.find('input:reset, button[type=reset]');

			initSelect(hold.find(options.select.elements), hold, reset);
			initRadio(hold.find(options.radio.elements), hold, reset);
			initCheckbox(hold.find(options.checkbox.elements), hold, reset);
		});
	}
	else{
		if (methods[_options]) {
			methods[_options].apply(this);
		}
	}

	function initSelect(elements, form, reset){
		elements.not('.outtaHere').each(function(){
			var select = $(this);
			var replaced = jQuery(options.select.structure);
			var selectText = replaced.find(options.select.text);
			var selectBtn = replaced.find(options.select.btn);
			var selectDisabled = replaced.find('.'+options.disabled).hide();
			var optHolder = jQuery(options.select.optStructure);
			var optList = optHolder.find(options.select.optList);
			var html = '';
			var optTimer;

			if(select.prop('disabled')) selectDisabled.show();

			function createStructure(){
				html = '';
				select.find('option').each(function() {
					var selOpt = jQuery(this);
					if(selOpt.prop('selected')) selectText.html(selOpt.html());

					if(selOpt.attr('class')){
						html += '<li data-value="' + selOpt.val() + '" ' +
							(selOpt.prop('selected') ? 'class="selected"' : '') + '>' +
							(selOpt.prop('disabled') ? '<span>' : '<a href="#" class="'+ selOpt.attr('class') +'">') + selOpt.html() +
							(selOpt.prop('disabled') ? '</span>' : '</a>') + '</li>';
					}
					else {
						html += '<li data-value="' + selOpt.val() + '" ' +
							(selOpt.prop('selected') ? 'class="selected"' : '') + '>' +
							(selOpt.prop('disabled') ? '<span>' : '<a href="#">') + selOpt.html() +
							(selOpt.prop('disabled') ? '</span>' : '</a>') + '</li>';
					}

				});
				if (select.data('placeholder') !== undefined) {
					selectText.html(select.data('placeholder'));
					replaced.addClass('placeholder');
				}
				optList.append(html).find('a').click(function() {
					replaced.removeClass('placeholder');
					optList.find('li').removeClass('selected');
					jQuery(this).parent().addClass('selected');
					select.val(jQuery(this).parent().data('value').toString());
					selectText.html(jQuery(this).html());
					select.change();
					replaced.removeClass(options.hoverClass);
					optHolder.css({
						left:-9999,
						top:-9999
					});
					return false;
				});
			}
			createStructure();
			replaced.width(select.outerWidth());
			replaced.insertBefore(select);
			replaced.addClass(select.attr('class'));
			optHolder.css({
				width: select.outerWidth(),
				position: 'absolute',
				left:-9999,
				top:-9999
			});
			optHolder.addClass(select.attr('class'));
			jQuery(document.body).append(optHolder);

			select.bind('refresh', function(){
				optList.empty();
				createStructure();
			});

			replaced.hover(function() {
				if(optTimer) clearTimeout(optTimer);
			}, function() {
				optTimer = setTimeout(function() {
					replaced.removeClass(options.hoverClass);
					optHolder.css({
						left:-9999,
						top:-9999
					});
				}, 200);
			});
			optHolder.hover(function(){
				if(optTimer) clearTimeout(optTimer);
			}, function() {
				optTimer = setTimeout(function() {
					replaced.removeClass(options.hoverClass);
					optHolder.css({
						left:-9999,
						top:-9999
					});
				}, 200);
			});
			if (options.select.maxHeight && optHolder.children().height() > options.select.maxHeight) {
				optHolder.children().css({
					height:options.select.maxHeight,
					overflow:'auto'
				});
			}
			selectBtn.click(function() {
				if(optHolder.offset().left > 0) {
					replaced.removeClass(options.hoverClass);
					optHolder.css({
						left:-9999,
						top:-9999
					});
				}
				else{
					replaced.addClass(options.hoverClass);
					select.removeClass('outtaHere');
					optHolder.css({
						width: select.outerWidth(),
						top: -9999
					});
					select.addClass('outtaHere');
					if (options.select.maxHeight && optHolder.children().height() > options.select.maxHeight) {
						optHolder.children().css({
							height:options.select.maxHeight,
							overflow:'auto'
						});
					}

					if($(document).height() > optHolder.outerHeight(true) + replaced.offset().top + replaced.outerHeight()){
						optHolder.removeClass(options.select.topClass).css({
							top: replaced.offset().top + replaced.outerHeight(),
							left: replaced.offset().left
						});
						replaced.removeClass(options.select.topClass);
					}
					else{
						optHolder.addClass(options.select.topClass).css({
							top: replaced.offset().top - optHolder.outerHeight(true),
							left: replaced.offset().left
						});
						replaced.addClass(options.select.topClass);
					}

					replaced.focus();

				}
				return false;
			});
			reset.click(function(){
				setTimeout(function(){
					select.find('option').each(function(i) {
						var selOpt = jQuery(this);
						if(selOpt.val() == select.val()) {
							selectText.html(selOpt.html());
							optList.find('li').removeClass('selected');
							optList.find('li').eq(i).addClass('selected');
						}
					});
				}, 10);
			});
			select.bind('change.customForm', function(){
				if(optHolder.is(':hidden')){
					select.find('option').each(function(i) {
						var selOpt = jQuery(this);
						if(selOpt.val() == select.val()) {
							selectText.html(selOpt.html());
							optList.find('li').removeClass('selected');
							optList.find('li').eq(i).addClass('selected');
						}
					});
				}
			});
			select.bind('focus.customForm', function(){
				replaced.addClass('focus');
			}).bind('blur.customForm', function(){
				replaced.removeClass('focus');
			});
			select.data('customForm', {
				'resizeElement': function(){
					select.removeClass('outtaHere');
					replaced.width(Math.floor(select.outerWidth()));
					select.addClass('outtaHere');
				},
				'create': replaced.add(optHolder)
			});
			$(window).bind('resize.customForm', function(){
				if(select.data('customForm')['resizeElement']) select.data('customForm').resizeElement();
			});
		}).addClass('outtaHere');
	}

	function initRadio(elements, form, reset){
		elements.each(function(){
			var radio = $(this);
			if(!radio.hasClass('outtaHere') && radio.is(':radio')){
				radio.data('customRadio', {
					radio: radio,
					name: radio.attr('name'),
					label: $('label[for='+radio.attr('id')+']').length ? $('label[for='+radio.attr('id')+']') : radio.parents('label'),
					replaced: jQuery(options.radio.structure, { 'class': radio.attr('class') })
				});
				var data = radio.data('customRadio');

				if(radio.is(':disabled')) {
					data.replaced.addClass(options.disabled);
					if(radio.is(':checked')) data.replaced.addClass('disabledChecked');
				}
				else if (radio.is(':checked')) {
					data.replaced.addClass(options.radio.checked);
					data.label.addClass('checked');
				}
				else {
					data.replaced.addClass(options.radio.defaultArea);
					data.label.removeClass('checked');
				}
				data.replaced.click(function(){
					if(jQuery(this).hasClass(options.radio.defaultArea)){
						radio.change();
						radio.prop('checked', true);
						changeRadio(data);
					}
				});
				reset.click(function(){
					setTimeout(function(){
						if(radio.is(':checked')) data.replaced.removeClass(options.radio.defaultArea+' '+options.radio.checked).addClass(options.radio.checked);
						else data.replaced.removeClass(options.radio.defaultArea+' '+options.radio.checked).addClass(options.radio.defaultArea);
					}, 10);
				});

				radio.bind('refresh', function(){
					if (radio.is(':checked')) {
						data.replaced.removeClass(options.radio.defaultArea + ' ' + options.radio.checked).addClass(options.radio.checked);
						data.label.addClass('checked');
					}
					else {
						data.replaced.removeClass(options.radio.defaultArea + ' ' + options.radio.checked).addClass(options.radio.defaultArea);
						data.label.removeClass('checked');
					}
				});

				radio.bind('click.customForm', function(){
					changeRadio(data);
				});
				radio.bind('focus.customForm', function(){
					data.replaced.addClass('focus');
				}).bind('blur.customForm', function(){
					data.replaced.removeClass('focus');
				});
				data.replaced.insertBefore(radio);
				radio.addClass('outtaHere');
				radio.data('customForm', {
					'create': data.replaced
				});
			}
		});

	}

	function changeRadio(data){
		jQuery('input:radio[name="'+data.name+'"]').not(data.radio).each(function(){
			var _data = $(this).data('customRadio');
			if (_data.replaced && !jQuery(this).is(':disabled')) {
				_data.replaced.removeClass(options.radio.defaultArea+' '+options.radio.checked).addClass(options.radio.defaultArea);
				_data.label.removeClass('checked');
			}
		});
		data.replaced.removeClass(options.radio.defaultArea+' '+options.radio.checked).addClass(options.radio.checked);
		data.label.addClass('checked');
		data.radio.trigger('change');
	}

	function initCheckbox(elements, form, reset){
		elements.each(function(){
			var checkbox = $(this);
			if(!checkbox.hasClass('outtaHere') && checkbox.is(':checkbox')){
				checkbox.data('customCheckbox', {
					checkbox: checkbox,
					label: $('label[for='+checkbox.attr('id')+']').length ? $('label[for='+checkbox.attr('id')+']') : checkbox.parents('label'),
					replaced: jQuery(options.checkbox.structure, { 'class': checkbox.attr('class') })
				});
				var data = checkbox.data('customCheckbox');

				if(checkbox.is(':disabled')) {
					data.replaced.addClass(options.disabled);
					if(checkbox.is(':checked')) data.replaced.addClass('disabledChecked');
				}
				else if(checkbox.is(':checked')) {
					data.replaced.addClass(options.checkbox.checked);
					data.label.addClass('checked');
				}
				else {
					data.replaced.addClass(options.checkbox.defaultArea);
					data.label.removeClass('checked');
				}

				data.replaced.click(function(){
					if(!data.replaced.hasClass('disabled') && !data.replaced.parents('label').length){
						if(checkbox.is(':checked')) checkbox.prop('checked', false);
						else checkbox.prop('checked', true);
						changeCheckbox(data);
					}
				});
				reset.click(function(){
					setTimeout(function(){
						changeCheckbox(data);
					}, 10);
				});
				checkbox.bind('refresh', function(){
					if (checkbox.is(':checked')) {
						data.replaced.removeClass(options.checkbox.defaultArea + ' ' + options.checkbox.defaultArea).addClass(options.checkbox.checked);
						data.label.addClass('checked');
					}
					else {
						data.replaced.removeClass(options.checkbox.defaultArea+' '+options.checkbox.checked).addClass(options.checkbox.defaultArea);
						data.label.removeClass('checked');
					}
				});
				checkbox.bind('click.customForm', function(){
					changeCheckbox(data);
				});
				checkbox.bind('focus.customForm', function(){
					data.replaced.addClass('focus');
				}).bind('blur.customForm', function(){
					data.replaced.removeClass('focus');
				});
				data.replaced.insertBefore(checkbox);
				checkbox.addClass('outtaHere');
				data.replaced.parents('label').bind('click.customForm', function(){
					if(!data.replaced.hasClass('disabled')){
						if(checkbox.is(':checked')) checkbox.prop('checked', false);
						else checkbox.prop('checked', true);
						changeCheckbox(data);
					}
					return false;
				});
				checkbox.data('customForm', {
					'create': data.replaced,
					'events': data.replaced.parents('label')
				});
			}
		});
	}

	function changeCheckbox(data){
		if (data.checkbox.is(':checked')) {
			data.replaced.removeClass(options.checkbox.defaultArea + ' ' + options.checkbox.defaultArea).addClass(options.checkbox.checked);
			data.label.addClass('checked');
		}
		else {
			data.replaced.removeClass(options.checkbox.defaultArea+' '+options.checkbox.checked).addClass(options.checkbox.defaultArea);
			data.label.removeClass('checked');
		}
		data.checkbox.trigger('change');
	}
}

function standartAJAXResponseHandler(data, status)
{
	if (status == 'success')
	{
		if (data != null && data.error)
		{
			$.jGrowl(data.error);
			return false;
		}
	}
	else
	{
		$.jGrowl("Ошибка запроса");
		return false;
	}
	return true;
}

function standartAJAXBeforeSubmit(form, jObject, options)
{
	form.push({name:"async",value:"1"});
}

function LinkBtnClick()
{
    var btnID = $(this).attr('id');
    var link = $('#' + btnID + '_link').val();
    if (typeof link == 'undefined')
        return;

    window.location = link;
}

function assignPaginator()
{
	if (typeof $.fn.pagination == "undefined") return;
	var that = $(this);
	var links = that.children("a");
	var firstLink = links.eq(0).attr("href");
	var secondLink = links.eq(1).attr("href");
	if (!firstLink || !secondLink || firstLink.length != secondLink.length)
	{
		that.append("Должны быть заданы первые две ссылки. Их длины должны совпадать.");
		return;
	}
	var linkPrefix = "";
	var linkPostfix = "";
	var pref = true;
	for (var i = 0; i < firstLink.length; i++)
	{
		if (firstLink.charAt(i) == secondLink.charAt(i) && pref)
			linkPrefix += firstLink.charAt(i);
		else if (!pref)
			linkPostfix += firstLink.charAt(i);
		else
			pref = false;
	}
	var spans = that.children("span");
	var pagesTotal = spans.eq(2).text();
	var itemsOnPage = spans.eq(0).text();
	var current = spans.eq(1).text();
	that.pagination(pagesTotal*itemsOnPage,
	{
		items_per_page:itemsOnPage,
		num_display_entries: 30,
		current_page:current,
		link_to:linkPrefix+"__id__"+linkPostfix,
		next_text:"След.",
		prev_text:"Пред.",
		callback:paginatorLinkClick
	});
}

function paginatorLinkClick(pageID, container){}

function AreYouSure(Message)
{
     if (typeof Message == 'undefined') Message = "";
     return confirm("Вы уверены? " + Message);
}

function ChooseLen(event)
{
	var sender = event.target == undefined? event.srcElement:event.target;
	if (sender.maxLength < 0) return;
	var M = sender.value.length;
	var span = document.getElementById(sender.id+"_count");
	if (M == undefined || span == undefined) return;
	span.innerHTML = M + " / " + sender.maxLength;
	var parent = span.parentElement == undefined ? span.parentNode : span.parentElement;

	parent.style.display = "inline";
}
function HideLen(event)
{
	var sender = event.target == undefined? event.srcElement:event.target;
	var span = document.getElementById(sender.id+"_count");
	var parent = span.parentElement == undefined ? span.parentNode : span.parentElement;
	parent.style.display = "none";
}

function checkAll(formName, cbName)
{
    alert('checkAll');
	var form = document.forms[formName];

	for (var i=0; i < form[cbName].length; i++)
		form[cbName][i].checked = true;
}

function uncheckAll(formName, cbName)
{
    alert('uncheckAll');
	var form = document.forms[formName];

	for (var i=0; i < form[cbName].length; i++)
		form[cbName][i].checked = false;
}

function clearTextField(fieldId)
{
    alert('Clear Text Field');
	var textField = document.getElementById(fieldId);

	textField.value = "";
}
function isEmpty(str)
{
	if (str == "") return true;
    for (var i = 0; i < str.length; i++)
		if (str.charAt(i) != " ")
            return false;
    return true;
}

function checkEmail(email)
{
		var arr;
		arr=email.split("@");
		if (arr.length<2)
		{
			alert("Некорректный e-mail.");
			return false;
		}
		arr=email.split(".");
		if (arr.length<2)
		{
			alert("Некорректный e-mail.");
			return false;
		}
		return true;
}
function checkform(event) {

	f = event.target == undefined? event.srcElement:event.target
	// <!--это для того, чтобы отображать имена полей -->
	//<!--var errMSG = "";-->
	var isErr = false;
	msg = "Все поля, помеченые звёздочкой, необходимо заполнить.";
    for (var i = 0; i<f.elements.length; i++)
	{

    	if (f.elements[i].getAttribute("required") != null)
		{
    		if (isEmpty(f.elements[i].value))
			{
				    //<!-- Это массив с именами незаполненныйх полей заполняем -->
                    //<!--errMSG += "  " + f.elements[i].name + "\\n"; -->
					isErr= true;
			}
			else if ((f.elements[i].name == "email") && (!checkEmail(f.elements[i].value)))
			{
				isErr= true;
				msg = "Неверный email";
			}
		}
	}
           // <!--if ("" != errMSG) { -->
	if (isErr)
	{
               //<!--alert("Не заполнены обязательные поля:\\n" + errMSG); -->

		//alert(msg);
        return false;
    }
    else
        return true;
}

(function ($) {
    $.fn.nicEditor = function() {
        if (typeof nicEditor == 'undefined')
            return;
        this.each(function (key, elem) {
            var tarea = $(this);
            if (!tarea.attr('id'))
                tarea.attr('id', 'nicEditor' + Math.floor((Math.random() * 10000)));
            var areaId = tarea.attr('id');
            var short = tarea.hasClass('short');
            var toggleLink = $('<a class="nicEditorToggler" title="Переключить редактор" href="#">Переключить редактор</a><br clear="all">').on('click', function(e) {
                var link = $(this);
                if (link.data.nicEditor)
                {
                    link.data.nicEditor.removeInstance(areaId);
                    link.data.nicEditor = null;
                }
                else
                    link.data.nicEditor = new nicEditor({fullPanel : !short}).panelInstance(areaId);

                return false;
            });
            tarea.before(toggleLink);
        });
    };
}(jQuery));
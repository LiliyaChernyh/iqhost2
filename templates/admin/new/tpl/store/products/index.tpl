<h1>Управление товарами</h1>

<div style="text-align: right; margin: -30px 0 5px 0;">
    <input type="button" id="goAddTheme" class="goToBtn" value="Добавить товар" />
    <input type="hidden" id="goAddTheme_link" value="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/add/" />
</div>

<form action="/admin/store/products/search/" method="GET" class="form bottom20">
	<label>Cписок товаров по категориям</label><br>
	<div class="hold-left">
		<select name="category"  title="список товаров по категориям"  class="select2" style="width: 400px;">
			<option value="">-</option>
			{foreach from=$CATEGORIES item=THEME key=I}
				{*				<option value="{$THEME.categoryFullAlias}" {if !empty($CATEGORY) AND $CATEGORY.categoryID == $THEME.categoryID}selected="selected"{/if}>{"&nbsp;&nbsp;&nbsp;&nbsp;"|str_repeat:$THEME.level-1}{$THEME.categoryName}</option>*}
				<option value="{$THEME.categoryID}" {if $THEME.level== 2} class="level1"{/if} {if !empty($CATEGORY) AND $CATEGORY.categoryID == $THEME.categoryID}selected="selected"{/if}>{'&nbsp'|str_repeat:$THEME.level*4}{$THEME.categoryName}{if isset($THEME.productsCount)} ({$THEME.productsCount}){/if}</option>
			{/foreach}
		</select>
		<button class="btn-search" type="submit">Показать</button>
	</div>
</form>
<form action="/admin/store/products/search/" method="GET" class="form bottom20">
	<div class="wrap">
		<label>Поиск товаров</label>
	</div>
    <input type="text" class="text-input left" style=" width:245px;" name="q" value="" placeholder="поиск"  title="поиск по названию / описанию / артикулу / id товара  " />
    <button class="btn-search left" type="submit" style="width: 74px;">Поиск</button>
</form>
{if !empty($CATEGORY)}

	<div class="bottom20">
		<span class="result">Категория: </span><strong class="title">{$CATEGORY.categoryName}</strong>
	</div>
	<div class="bottom20">
		<span class="result">Товаров: </span><strong class="title">{$PRODUCTS_TOTAL}</strong>
	</div>
{/if}
{if !empty($searchWord)}
	<div class="bottom20">
		<span class="result">Товаров: </span><strong class="title">{$PRODUCTS_TOTAL}</strong>
	</div>
{/if}

{include file='paginator_new.tpl'}
<form action="" method="POST">
	<div id="sortProducts">
	<table class="highlightable" id="faqTable_">
		<thead>
			<tr>
				<th colspan="2">
		<div class="sel-link">
			<a href="#" class="select-all">Выбрать все</a>
			<a href="#" class="deselect-all">Снять выбор</a>
		</div>
	</th>
	<th>ID</th>
	<th>Изображение</th>
	<th>Название</th>
	<th>Артикул</th>
	<th>Бренд</th>
	<th>Цена</th>
	<th>Описание</th>
	<th>Имп</th>
	<th>Категория</th>
	<th>DK</th>
	<th colspan="3">&nbsp;</th>
	</tr>
	</thead>
	<tbody>
		{if !empty($PRODUCTS)}
				{foreach from=$PRODUCTS item=THEME key=I}
					<tr id="tr-{$THEME.productID}">
						<td class="hov">1</td>
						<td><input type='checkbox' class="select-product" name="productID[]" value="{$THEME.productID}"/></td>
						<td class="productID">{$THEME.productID}</td>
						<td>
							{if !empty($THEME.srcSmall)}
								<img src='{$THEME.srcSmall}' data-big='{$THEME.src}' width="100" class='product-image' />
							{else}
								&nbsp;
							{/if}
						</td>
						<td class="productName">
							{$THEME.productName}
						</td>
						<td>
							{$THEME.productArticul}
						</td>
						<td>
							{$THEME.brandName}
						</td>
						<td>
							{$THEME.productPrice}
						</td>
						<td>
							{$THEME.productDescription}
						</td>
						<td>
						{if !empty($THEME.importName)}{$THEME.importName}{else}N/A{/if}
					</td>
					<td class="category">
						{$THEME.categoryName}
						</td>
						<td class="subcategory">
							{if empty($THEME.categories)}
								&nbsp;
							{else}
								{foreach from=$THEME.categories item=prod}
									<div class="cat cat_{$THEME.productID}_{$prod.categoryID}">{$prod.categoryName} <a class="delsubcat" href="#" data-link="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/delsubcategory/{$THEME.productID}/{$prod.categoryID}/" title="Удалить"><img src="{$IMAGES_ALIAS}icons/cross_bg.png" data-alt="удалить" style="border: 0;" /></a></div>
								{/foreach}
							{/if}
						</td>
						<td>
							<a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/edit/{$THEME.productID}/" title="Редактировать" target="_blank"><img src="{$IMAGES_ALIAS}icons/pencil.png" alt="Редактировать" style="border: 0;" /></a>
					</td>
					{if $THEME.status == 3}
						<td colspan="2">
							<a class="actproduct" href="#restoreproduct" data-link="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/restore/{$THEME.productID}/" title="Восстановить"><img src="{$IMAGES_ALIAS}icons/arrow_undo.png" data-alt="восстановить" style="border: 0;" /></a>
						</td>
					{else}
						{if $THEME.status == 2}
							<td>
								<a class="actproduct" href="#unblockproduct" data-link="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/unblock/{$THEME.productID}/" title="Включить"><img src="{$IMAGES_ALIAS}icons/active1.png" data-alt="включить" style="border: 0;" /></a>
							</td>
						{else}
							<td>
								<a class="actproduct" href="#blockproduct" data-link="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/block/{$THEME.productID}/" title="Отключить"><img src="{$IMAGES_ALIAS}icons/delete.png" data-alt="отключить" style="border: 0;" /></a>
							</td>
						{/if}
						<td>
							<a class="actproduct" href="#delproduct" data-link="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/del/{$THEME.productID}/" title="Удалить"><img src="{$IMAGES_ALIAS}icons/delete_icon.png" data-alt="удалить" style="border: 0;" /></a>
						</td>
					{/if}
				</tr>
			{/foreach}
	</tbody>
	<tfoot>
		<tr class="last">
			<td colspan="2">
				<div class="sel-link">
					<a href="#" class="select-all">Выбрать все</a>
					<a href="#" class="deselect-all">Снять выбор</a>
				</div>
			</td>
			<td colspan="13">
				<strong class="title">Применить к выбранным:</strong>
				<span><a class="funcproduct" href="#unblockproduct" data-link="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/unblock/" title="Включить"><img src="{$IMAGES_ALIAS}icons/active1.png" data-alt="включить" style="border: 0;" /></a></span>
				<span><a class="funcproduct" href="#blockproduct" data-link="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/block/" title="Отключить"><img src="{$IMAGES_ALIAS}icons/delete.png" data-alt="отключить" style="border: 0;" /></a></span>
				<span><a class="funcproduct" href="#delproduct" data-link="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/del/" title="Удалить"><img src="{$IMAGES_ALIAS}icons/delete_icon.png" data-alt="удалить" style="border: 0;" /></a></span>
				<span><a class="funcproduct" href="#restoreproduct" data-link="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/restore/" title="Восстановить"><img src="{$IMAGES_ALIAS}icons/arrow_undo.png" data-alt="восстановить" style="border: 0;" /></a></span>
				<span >Применить категорию</span>
				<select class='apply-selected select2' name="categoryID[]" style="width:300px">
					{foreach from=$CATEGORIES item=CATEGORY}
						<option value="{$CATEGORY.categoryID}" {if $CATEGORY.level== 2} class="level1"{/if}>
							{'&nbsp'|str_repeat:$CATEGORY.level*3}{$CATEGORY.categoryName}{if isset($CATEGORY.productsCount)} ({$CATEGORY.productsCount}){/if}
						</option>
					{/foreach}
				</select>
				<input type="radio" name="changeCategory" value="1" id="moveToCat" class="changeCategory" checked="checked"><label for="moveToCat">Перенести в категорию</label>
				<input type="radio" name="changeCategory" value="2" id="copyToCat" class="changeCategory"><label for="copyToCat">Скопировать в категорию</label>
				<div style="float:right; display: inline"><button class="blue-button actcategory" type="button">Сохранить</button></div>
			</td>
		</tr>
	</tfoot>

	</table>
</div>
				<div id="dialog-func" style="display:none;" title="">
					<p></p>
				</div>

{else}
</tbody>
</table>
</div>
<div class="formBlock">
	<div class="error">Записей не найдено</div>
</div>
{/if}
</form>
{include file='paginator_new.tpl'}

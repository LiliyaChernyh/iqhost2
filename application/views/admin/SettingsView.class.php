<?php
/**
 * 
 * Представление для настроек в Админке
 * 
 * @version 1.2
 * 
 */
class SettingsView extends AdminView
{
    public function __construct($tplName = 'main.tpl')
    {
        parent::__construct($tplName);

        $this -> tplsPath = $this -> tplsPath.'settings/';
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
    }

    public function View()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'settings.tpl';
    }
    
    public function AddSetting()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'addsetting.tpl';
    }
}

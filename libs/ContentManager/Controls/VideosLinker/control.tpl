<div id="linkedVideosHiddenInputs">
	{* Отобразим уже сохранённые ролики в виде скрытых инпутов*}
	{if !empty($localVars.LINKED_VIDEOS)}
		{foreach from=$localVars.LINKED_VIDEOS item=VIDEO}
			<input type="hidden" name="{$localVars.NAME}__saved[]" value="{$VIDEO.videoID}" />
		{/foreach}
	{else}
	<input type="hidden" name="{$localVars.NAME}__saved" value="0" />
	{/if}
	<input type="hidden" name="{$localVars.NAME}__added" value="0" />
	<input type="hidden" name="{$localVars.NAME}__delete" value="0" />
</div>
<div id="videosLinker_{$localVars.NAME}" class="linkedVideosContainer">

	<h2>Связанные видео</h2>
	<div id="linkedVideos">
	{if !empty($localVars.LINKED_VIDEOS)}{foreach from=$localVars.LINKED_VIDEOS item=VIDEO}
		<div id="video_{$VIDEO.videoID}" class="videoInList linkedVideo">
			
			<div class="videoThumbnail">
				<a href="{$ADMIN_ALIAS}anyeditor/videos/edit/{$VIDEO.videoID}/" class="openInNewWin">
				{if !empty($VIDEO.srcSmall)}
					<img src="{$VIDEO.srcSmall}" alt=""/>
				{else}
					<img src="/images/admin/no-image.png" alt="no image"/>
				{/if}
				</a>
			</div>
			
			<div class="videoTitle">{$VIDEO.title}</div>
			<div class="videoLinkageControls"><a href="#" class="removeFromLinked jsLink">Убрать из связанных</a></div>
		</div>
	{/foreach}
	{/if}
	</div>

	<h2>Остальные видео</h2>
	<div id="otherVideos">
	{foreach from=$localVars.VIDEOS item=VIDEO}
		<div id="video_{$VIDEO.videoID}" class="videoInList">
			<div class="videoThumbnail">
				<a href="{$ADMIN_ALIAS}anyeditor/videos/edit/{$VIDEO.videoID}/" class="openInNewWin">
				{if !empty($VIDEO.srcSmall)}
					<img src="{$VIDEO.srcSmall}" alt=""/>
				{else}
					<img src="/images/admin/no-image.png" alt="no image"/>
				{/if}
				</a>
			</div>
			<div class="videoTitle">{$VIDEO.title}</div>
			<div class="videoLinkageControls"><a href="#" class="addToLinked jsLink">Добавить в связанные</a></div>
		</div>
	{/foreach}
	</div>
	{if count($localVars.VIDEOS) >= 50 }
	<div id="loadMoreVideosContainer">
		<a id="loadMoreVideos" href="#">Ещё</a>
	</div>
	{/if}
</div>
<?php
/**
 * Класс позволяет генерировать RSS 2.0 документы
 * Спецификация RSS: http://cyber.law.harvard.edu/rss/rss.html
 *
 */
class RSSGenerator
{
	/**
	 * XML-документ
	 *
	 * @var string
	 */
	private $xml;
	
	/**
	 * The name of the channel. It's how people refer to your service. If you have an HTML website that contains the same information as your RSS file, the title of your channel should be the same as the title of your website.
	 * 
	 * @var string
	 */
	public $channelName;
	/**
	 * The URL to the HTML website corresponding to the channel.
	 * 
	 * @var string
	 */
	public $channelLink;
	/**
	 * Phrase or sentence describing the channel.
	 * 
	 * @var string
	 */
	public $channelDescription;
	/**
	 * The language the channel is written in. This allows aggregators to group all Italian language sites, for example, on a single page. A list of allowable values for this element, as provided by Netscape, is here http://cyber.law.harvard.edu/rss/languages.html. You may also use values defined by the W3C (http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes).
	 * 
	 * @var string
	 */
	public $channelLanguage = null;
	/**
	 * Copyright notice for content in the channel.
	 * 
	 * @var string
	 */
	public $channelCopyright = null;
	/**
	 * Email address for person responsible for editorial content.
	 * 
	 * @var string
	 */
	public $channelManagingEditor = null;
	/**
	 * Email address for person responsible for technical issues relating to channel.
	 * 
	 * @var string
	 */
	public $channelWebMaster = null;
	/**
	 * The publication date for the content in the channel. For example, the New York Times publishes on a daily basis, the publication date flips once every 24 hours. That's when the pubDate of the channel changes. All date-times in RSS conform to the Date and Time Specification of RFC 822, with the exception that the year may be expressed with two characters or four characters (four preferred).
	 * 
	 * @var string
	 */
	public $channelPubDate = null;
	/**
	 * The last time the content of the channel changed.
	 * 
	 * @var string
	 */
	public $channelLastBuildDate = null;
	/**
	 * Specify one or more categories that the channel belongs to. Follows the same rules as the <item>-level category element. More info. http://cyber.law.harvard.edu/rss/rss.html#syndic8
	 * 
	 * @var string
	 */
	public $channelCategory = null;
	/**
	 * A string indicating the program used to generate the channel.
	 * 
	 * @var string
	 */
	public $channelGenerator = null;
	/**
	 * A URL that points to the documentation for the format used in the RSS file. It's probably a pointer to this page. It's for people who might stumble across an RSS file on a Web server 25 years from now and wonder what it is.
	 * 
	 * @var string
	 */
	public $channelDocs = 'http://blogs.law.harvard.edu/tech/rss';
	/**
	 * Specifies a GIF, JPEG or PNG image that can be displayed with the channel. More info here.
	 * 
	 * @var string
	 */
	public $channelImage = null;
	/**
	 * A hint for aggregators telling them which hours they can skip. More info here.
	 * 
	 * @var string
	 */
	public $channelSkipHours = null;
	/**
	 * A hint for aggregators telling them which hours they can skip. More info here.
	 * 
	 * @var string
	 */
	public $channelSkipDays = null;
	
	
	public function RSSGenerator(
	/**
	 * The name of the channel. It's how people refer to your service. If you have an HTML website that contains the same information as your RSS file, the title of your channel should be the same as the title of your website.
	 */
	$channelName, 
	/**
	 * The URL to the HTML website corresponding to the channel.
	 */
	$channelLink, 
	/**
	 * Phrase or sentence describing the channel.
	 */
	$channelDescription)
	{
		$this -> xml = '<?xml version="1.0" encoding="UTF-8"?>
	<rss version="2.0">
		<channel>
				 <title>'. $this -> CyrToUnicode($channelName) . '</title>
     			 <link>' . $this -> CyrToUnicode($channelLink) .'</link>
      			 <description>'. $this -> CyrToUnicode($channelDescription) .'</description>';
		header("Content-Type: text/xml; charset=UTF-8 \r\n");
	}
	
	public function CyrToUnicode($str) {
    $conv=array();
    for($x=128;$x<=143;$x++) $conv[$x+112]=chr(209).chr($x);
    for($x=144;$x<=191;$x++) $conv[$x+48]=chr(208).chr($x);
    $conv[184]=chr(209).chr(145); #ё
    $conv[168]=chr(208).chr(129); #Ё
    $conv[179]=chr(209).chr(150); #і
    $conv[178]=chr(208).chr(134); #І
    $conv[191]=chr(209).chr(151); #ї
    $conv[175]=chr(208).chr(135); #ї
    $conv[186]=chr(209).chr(148); #є
    $conv[170]=chr(208).chr(132); #Є
    $conv[180]=chr(210).chr(145); #ґ
    $conv[165]=chr(210).chr(144); #Ґ
    $conv[184]=chr(209).chr(145); #Ґ
    $ar=str_split($str);
    $nstr = "";
    foreach($ar as $b) if(isset($conv[ord($b)])) $nstr.=$conv[ord($b)]; else $nstr.=$b;
    return $nstr;
}
	public function AddItem($title, $link, $description, $pubDate, $author = null, $guid = null)
	{
		$item = "
		<item>
			<title>". $this->CyrToUnicode($title) ."</title>
			<link>". $this->CyrToUnicode($link) ."</link>
			<description>". $this->CyrToUnicode($description) ."</description>
			<pubDate>". $this->CyrToUnicode($pubDate) ."</pubDate>".
			(($author == null) ? "" : "<author>".$this->CyrToUnicode($author) . "</author>") .
			(($guid == null) ? "" : "<guid>".$this->CyrToUnicode($guid) . "</guid>") .
		 "
		 </item>
		 ";
		 
		$this -> xml .= $item;
	}
	public function GetRSS()
	{
		return $this -> xml . "</channel></rss>";
	}
}
?>
/**
* Plugin for switching between two children in a "a" container
*
*/
$.fn.switching = function(options)
{
	var defaults = 
		{
			indexOfVisible : 0
		};
	var opts = $.extend(defaults, options);
	return this.each(function() 
	{
		$(this)
			.children(":eq(" + opts.indexOfVisible + ")").show()
			.siblings().hide()
			.end()
			.end()
			.data("indexOfVisible", opts.indexOfVisible)
			.click(function(e)
			{
				e.preventDefault();
				var that = $(this);
				that
					.children(":eq(" + that.data("indexOfVisible") + ")").hide()
					.siblings().show();
				that.data("indexOfVisible", (that.data("indexOfVisible") == 0 ? 1 : 0));
			});
	});
};
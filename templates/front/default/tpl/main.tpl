
{include file="top.tpl"}
<body>
    {*s
    {if isset($CONTENT_TPL)}
    {include file="$CONTENT_TPL"}
    {/if}*}
    <div id="wrapper">
        <div class="w1">
        {include file="header.tpl"}
        {if isset($CONTENT_TPL)}
         {include file="$CONTENT_TPL"}
        {elseif isset($CONTENT)}
            <div class="content-block">
                <div class="container">
                    <h2>{$PAGE.caption}</h2>
                {$CONTENT}
                </div>
            </div>
        {/if}
   			{include file="scripts.tpl"} 
        </div>
	</div>
{include file="footer.tpl"}
</body>
</html>
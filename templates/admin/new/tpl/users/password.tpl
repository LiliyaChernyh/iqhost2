<h1>Изменение пароля</h1>    

<div>
	<form action="{$PASSWORDFORM_ACTION}" method="post" id="password_form">
    <div><input type="hidden" name="userID" value="{$CURRENTUSER_userID}" /></div>
	<div class="formBlockHeader">Пароль</div>
    <div class="formBlock">
		<div class="formElement">
			<label for="currentPassword">Текущий пароль 
			<span class="smallLightGrey" style="display:none; margin-left:10px;">(<span id="currentPassword_count"></span>)</span>
			<span class="helper" title="Введите ваш текущий пароль">?</span>
			</label><br />
			<input type="password" name="currentPassword" id="currentPassword" size="80" class="limitedLen" title="Пароль должен содержать не менее 6 символов" maxlength="63" />
		</div>
		
		<div class="formElement">
			<label for="newPassword">Новый пароль 
			<span class="smallLightGrey" style="display:none; margin-left:10px;">(<span id="newPassword_count"></span>)</span>
			<span class="helper" title="Введите новый пароль">?</span>
			</label><br />
			<input type="password" name="newPassword" id="newPassword" size="80" class="limitedLen" title="Пароль должен содержать не менее 6 символов" maxlength="63" />
		</div>
		
		<div class="formElement">
			<label for="newPasswordConf">Подтвердите пароль 
			<span class="smallLightGrey" style="display:none; margin-left:10px;">(<span id="newPasswordConf_count"></span>)</span>
			<span class="helper" title="Введите новый пароль еще раз">?</span>
			</label><br />
			<input type="password" name="newPasswordConf" id="newPasswordConf" size="80" class="limitedLen" title="Подтврежденный пароль не совпадает с ранее введенным" maxlength="63" />
		</div>
	</div>

	<div style="margin-left:350px">
		<input type="submit" value="{$PASSWORDFORM_SUBMIT_VALUE}" name="{$PASSWORDFORM_SUBMIT_NAME}" />
	</div>
	
	</form>
		{literal}
	<script type="text/javascript">
	$(document).ready(function()
		{
			$("#password_form").validate({
			rules:
			{
				newPassword: {required : true},
				newPasswordConf: {required : true,equalTo: "#newPassword"},
				currentPassword: {required : true}
			}
			});
		});
		$(".limitedLen").focus(ChooseLen).change(ChooseLen).keyup(ChooseLen).blur(HideLen);
	</script>
	{/literal}
</div>
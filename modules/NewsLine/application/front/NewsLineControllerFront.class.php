<?php

/**
 * Контроллер для работы с модулем NewsLine во фронт-енде
 *
 * @version 2.0
 */
class NewsLineControllerFront extends FrontController {

    protected $model;
    protected $newsline;
    protected $rootAlias;

    public function __construct()
    {
        parent::__construct();

        // Используем статический метод для получения параметра rootAlias
        $settings = CommonModule::LoadSettings('NewsLine');
        $this->rootAlias = !empty($settings['ParentAlias']) ? $settings['ParentAlias'] . "/" . $settings['rootAlias'] : $settings['rootAlias'];
        ViewData::Assign("ROOT_ALIAS", $this->rootAlias);
        // Получаем страницу из базы
        $this->page = new Page($this->rootAlias);
        $this->view = new NewsLineViewFront();
        $this->model = new NewsLineModelFront($this->rootAlias);
    }

    public function Index($args = null) {
        $this->newsLine = new NewsLine();
        $count          = count($args);
        if ($count > 2)
        {
            throw new PageNotFoundException('Error');
        }

        $sectionID = isset($args[0]) ? ($args[0] == 'unsort' ? $args[0] : (!is_numeric($args[0]) ? $this->model->GetSectionID($args[0]) : null)) : null;
        $section = isset($args[0]) && !is_numeric($args[0]) ? $args[0] : null;

        if ($count > 0 && is_numeric($args[$count - 1]))
        {
            return $this->Item($args[$count - 1], $sectionID);
        } elseif (!empty($args) && is_null($sectionID)) {
            throw new PageNotFoundException('Error');
        }

        $page = 1;
        if (isset(Request::$GET['page']) && is_numeric(Request::$GET['page'])) {
            $page = Request::$GET['page'];
        }
        $limit = $this->newsLine->settings['MessagesOnPageFront'];
        $start = ($page - 1) * $limit;

        $sections = $this->newsLine->GetSections(1);
        ViewData::Assign("SUBMENU", $sections);
        ViewData::Assign("SECTION", $section);

        $news = $this->model->GetNews($sectionID, $start, $limit);
        //пагинация
        $countRows = $this->model->GetFoundRows();
        $this->setPaginator($limit, $countRows, $page);

        ViewData::Assign('news', $news);

        $curSect           = $sections[(!empty($section) ? $section : '')];
        $id                = 1;
        $this->page->title = $this->page->title . ($curSect['alias'] != '' ? " ".$curSect['caption'] : '');
        $this->view->Index();
        return $this->view;
    }

    public function Item($newsID, $sectionID = null) {
        if (!is_numeric($newsID) || (!is_null($sectionID) && !is_numeric($sectionID)))
            throw new PageNotFoundException('Error');

        $news = $this->model->GetNewsItem($newsID, $sectionID);

        if (empty($news))
            throw new PageNotFoundException('Error');
        ViewData::Assign('news', $news);

        $this->page->title       = $news['title'];
        $this->page->caption     = $this->page->title;
        $this->page->description = strip_tags($news['body']);
        $this->page->pageUrl     = "http://" . Request::GetHost() . "/newsline/" . (!empty($news['alias']) ? $news['alias'] . "/" : '') . $news['newsID'] . "/";
        $this->page->pageImage   = "http://" . Request::GetHost() . $news['src'];

        $this->view->Item();
        return $this->view;
    }

}

?>
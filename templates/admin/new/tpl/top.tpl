<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-language" content="ru" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
{if !empty($AUTOREFRESH)}
	<meta http-equiv="refresh" content="{$AUTOREFRESH}" >
	{/if}
	<title>{if isset($PAGE_TITLE)}{$PAGE_TITLE} &mdash; {/if} {$SETTINGS.general.siteName} &mdash; Bitplatform</title>

<link rel="stylesheet" href="{$ADMIN_CSS}mainStyles.css" type="text/css" media="screen,projection" />
<link rel="stylesheet" href="{$ADMIN_CSS}commonlist.css" type="text/css" media="screen,projection" />
<!--[if IE]><link rel="stylesheet" type="text/css" href= "{$ADMIN_CSS}ie.css" /><![endif]-->

<link rel="stylesheet" href="{$ADMIN_CSS}tables.css" type="text/css" media="screen,projection" />
<link rel="stylesheet" href="{$ADMIN_CSS}forms.css" type="text/css" media="screen,projection" />
<link rel="stylesheet" href="{$ADMIN_CSS}menu.css" type="text/css" media="screen,projection" />
<!--<link rel="stylesheet" href="{$ADMIN_CSS}linkedVideos.css" type="text/css" media="screen,projection" />-->
<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/select2/select2.css" type="text/css" media="screen,projection" />


<link rel="stylesheet" href="{$ADMIN_CSS}profile.css" type="text/css" media="screen,projection" />

<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/jgrowl/jquery.jgrowl.css" type="text/css" media="screen,projection" />
<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/impromptu/impromptu.css" type="text/css" media="screen,projection" />
{if isset($STYLE_FILE)}
{foreach from=$STYLE_FILE item=FILE}
<link rel="stylesheet" href="{$FILE}" type="text/css" />
{/foreach}
{/if}

<link rel="shortcut icon" href="/adminfavicon.ico" />

{if isset($JS_VARS)}
<script type="text/javascript">
{foreach from=$JS_VARS item=JS_VAR}{foreach from=$JS_VAR item=JS_VAR_VALUE key=JS_VAR_NAME}
var {$JS_VAR_NAME} = {if is_numeric($JS_VAR_VALUE)}{$JS_VAR_VALUE}{else}"{$JS_VAR_VALUE}"{/if};
{/foreach}{/foreach}
</script>
{/if}

{if !empty($OLD_JQUERY)}
	<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/old/jquery.js"></script>
	<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/old/form.js"></script><!--  AJAX forms-->
{else}
		<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jquery.js"></script>
		{*	<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jquery-1.11.0.min.js"></script>*}
	<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jquery.form.js"></script><!--  AJAX forms-->
{/if}
{if !empty($NEW_UI)}
	<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jquery-ui/jquery-ui.min.js"></script>
{/if}
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/select2/select2.js"></script>
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/validator/validate.js"></script><!-- Валидатор форм -->
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/validator/validate.ext.js"></script><!-- Локализатор валидатора форм -->
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/tooltip/jquery.tooltip.js"></script><!--  Tooltip-->
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/dropDownMenu/ddm.js"></script><!--  DropDownMenu-->
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/impromptu/jquery.impromptu.js"></script><!--  Impromptu -->

{if (isset($INCLUDE_JQUERY_UI)
 || isset($INCLUDE_JQUERY_UI_EFFECTS)
 || isset($INCLUDE_JQUERY_UI_DATEPICKER)
 || isset($INCLUDE_JQUERY_UI_SLIDER)
 || isset($INCLUDE_JQUERY_UI_SORTABLE)
 || isset($INCLUDE_JQUERY_UI_RESIZABLE)
 || isset($INCLUDE_JQUERY_UI_DRAGGABLE))}
<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/UI/jquery-ui-1.7.2.custom.css" type="text/css" media="screen,projection" />
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/UI/ui.core.js"></script><!--  UI core-->
{/if}
{if isset($INCLUDE_JQUERY_UI_EFFECTS)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/UI/effects.core.js"></script><!--  UI effects core-->{/if}
{if isset($INCLUDE_JQUERY_UI_DATEPICKER)}
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/UI/ui.datepicker.js"></script><!--  Datepicker-->
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/UI/ui.datepicker-ru.js"></script><!--  Datepicker localisation-->
{/if}
{if isset($INCLUDE_JQUERY_UI_SLIDER)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/UI/ui.slider.js"></script><!--  Slider-->{/if}
{if isset($INCLUDE_JQUERY_UI_SORTABLE)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/UI/ui.sortable.js"></script>{/if}
{if isset($INCLUDE_JQUERY_UI_RESIZABLE)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/UI/ui.draggable.js"></script><!--  Slider-->{/if}
{*{if isset($INCLUDE_JQUERY_UI_DRAGGABLE)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/UI/ui.resizable.js"></script><!--  Slider-->{/if}*}
{if isset($INCLUDE_JQUERY_UI_DIALOG)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/UI/ui.dialog.js"></script><!--  Dialog-->{/if}
{if isset($INCLUDE_JQUERY_UI_TABS)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/UI/ui.tabs.js"></script><!--  Tabs-->{/if}

{if isset($INCLUDE_JQUERY_METADATA)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jquery.metadata.js"></script><!--  Metadata-->{/if}

{if isset($INCLUDE_FILE_UPLOADER)}<!-- File Uploader -->
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jqUploader/jquery.flash.js"></script>
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jqUploader/jquery.jqUploader.js"></script><!-- File Uploader -->
{/if}
{if isset($INCLUDE_JQUERY_MULTISELECT)} <!-- Needs dimensions --><script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jqueryMultiSelect/jqueryMultiSelect.js"></script><!--  Multiselect-->{/if}
{if isset($INCLUDE_SWFOBJECT)}<script type="text/javascript" src="{$LIBS_ALIAS}swfobject/swfobject.js"></script><!--  swf object -->{/if}
{if isset($INCLUDE_BLOCKUI)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jquery.blockUI.js"></script><!--  block UI -->{/if}
{if isset($INCLUDE_AUTOCOMPLETE)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/autocomplete/jquery.autocomplete.js"></script><!-- Autocomplete -->{/if}
{if isset($INCLUDE_PAGINATOR)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/pagination/jquery.pagination.js"></script>{/if}
{if isset($INCLUDE_JQMODAL)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jqModal/jqModal.js"></script><!-- jqModal -->{/if}
{if isset($INCLUDE_FCK)}
    <script type="text/javascript" src="{$LIBS_ALIAS}fckeditor/fckeditor.js"></script>
    <script type="text/javascript" src="{$LIBS_ALIAS}fckeditor/jquery.FCKEditor.js"></script>
{/if}
{if isset($INCLUDE_JWYSIWYG)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jwysiwyg/jquery.wysiwyg.js"></script>{/if}
{if isset($INCLUDE_JTRUNCATE)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jquery.jtruncate.js"></script>{/if}
{if isset($INCLUDE_JMASK)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/maskedInput/jquery.maskedinput.min.js"></script>{/if}
{if isset($INCLUDE_FANCYBOX)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/fancybox/fancybox.js"></script><!-- fancyBox -->{/if}
{if isset($INCLUDE_PNGFIX) || isset($INCLUDE_FANCYBOX)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/pngFix.js"></script>{/if}
{if isset($INCLUDE_TREEVIEW)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/treeview/treeview.min.js"></script>
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/treeview/treeview.async.js"></script>
{/if}
{if isset($INCLUDE_INLINE_MULTI_SELECT)}<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/inlinemultiselect/inlinemultiselect1.1.js"></script>{/if}
{if isset($INCLUDE_JCROP)}
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jcrop/jquery.Jcrop.min.js"></script>
<script type="text/javascript" src="{$LIBS_ALIAS}dialogs/CropThumbnail.js"></script>
{/if}
{if isset($INCLUDE_SWFUPLOAD)}
<script type="text/javascript" src="{$LIBS_ALIAS}SWFUpload/swfupload.js"></script><!--  swf upload -->
<script type="text/javascript" src="{$LIBS_ALIAS}SWFUpload/plugins/swfupload.queue.js"></script><!--  swf upload -->
<script type="text/javascript" src="{$LIBS_ALIAS}SWFUpload/plugins/swfupload.cookies.js"></script><!--  swf upload -->
<script type="text/javascript" src="{$LIBS_ALIAS}SWFUpload/plugins/swfupload.swfobject.js"></script><!--  swf upload -->
<script type="text/javascript" src="{$LIBS_ALIAS}SWFUpload/fileprogress.js"></script><!--  swf upload -->
<script type="text/javascript" src="{$LIBS_ALIAS}SWFUpload/handlers.js"></script><!--  swf upload -->
<link rel="stylesheet" href="{$LIBS_ALIAS}SWFUpload/swfupload.css" type="text/css" media="screen,projection" />
{/if}

{if isset($INCLUDE_DAMNUPLOAD)}
<link rel="stylesheet" href="{$LIBS_ALIAS}DamnUpload/damnupload.css" type="text/css" media="screen,projection" />
<script type="text/javascript" src="{$LIBS_ALIAS}DamnUpload/jquery.damnUploader.js"></script>
{/if}


<script type="text/javascript" src="{$LIBS_ALIAS}prettyprint/prettyprint.js"></script>

<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jgrowl/jquery.jgrowl.js"></script><!-- jGrowl -->
<script type="text/javascript" src="{$ADMIN_SCRIPTS}script.js"></script>
<script type="text/javascript" src="{$ADMIN_SCRIPTS}tables.js"></script>
<script type="text/javascript" src="{$ADMIN_SCRIPTS}iesucks.js"></script>
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/nicEdit/nicEdit.js"></script>
{if isset($SCRIPT_FILE)}
{foreach from=$SCRIPT_FILE item=FILE}
<script type="text/javascript" src="{$FILE}"></script>
{/foreach}
{/if}


<!-- Tooltip -->
<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/tooltip/jquery.tooltip.css" type="text/css" media="screen,projection" />

<!--  DropDownMenu-->
<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/dropDownMenu/ddm.css" type="text/css" media="screen,projection" />
{if isset($INCLUDE_JQUERY_MULTISELECT)}<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/jqueryMultiSelect/jqueryMultiSelect.css" type="text/css" media="screen,projection" /><!--  Multiselect-->{/if}
{if isset($INCLUDE_AUTOCOMPLETE)}<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/autocomplete/jquery.autocomplete.css" type="text/css" media="screen,projection" /><!--  Autocomplete-->{/if}
{if isset($INCLUDE_PAGINATOR)}<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/pagination/pagination.css" type="text/css" media="screen,projection" />{/if}
{if isset($INCLUDE_JQMODAL)}<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/jqModal/jqModal.css" type="text/css" media="screen,projection" />{/if}
{if isset($INCLUDE_JWYSIWYG)}<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/jwysiwyg/jquery.wysiwyg.css" type="text/css" media="screen,projection" />{/if}
{if isset($INCLUDE_FANCYBOX)}<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/fancybox/fancybox.css" type="text/css" media="screen,projection" />{/if}
{if isset($INCLUDE_TREEVIEW)}<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/treeview/treeview.css" type="text/css" media="screen,projection" />{/if}
{if isset($INCLUDE_INLINE_MULTI_SELECT)}<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/inlinemultiselect/inlinemultiselect.css" type="text/css" media="screen,projection" />{/if}
{if isset($INCLUDE_JCROP)}<link rel="stylesheet" href="{$LIBS_ALIAS}jQuery/jcrop/jquery.Jcrop.css" type="text/css" media="screen,projection" />{/if}
{if isset($INCLUDE_JQGRID)}
	<link rel="stylesheet" type="text/css" media="screen" href="{$LIBS_ALIAS}jQuery/jqGrid/jquery-ui-1.7.1.custom.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="{$LIBS_ALIAS}jQuery/jqGrid/ui.jqgrid.css" />
	<script src="{$LIBS_ALIAS}jQuery/jqGrid/grid.locale-ru.js" type="text/javascript"></script>
	<script src="{$LIBS_ALIAS}jQuery/jqGrid/jquery.jqGrid.min.js" type="text/javascript"></script>
	<script src="{$LIBS_ALIAS}jQuery/jqGrid/jqDnR.js" type="text/javascript"></script>
{/if}

{if isset($INCLUDE_FANCYBOX)}
{literal}
<script type="text/javascript">
    $(document).ready(function(){
        $('a.fancy').fancybox();
    });
</script>
{/literal}
{/if}

</head>
<body>
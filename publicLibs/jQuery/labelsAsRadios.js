jQuery.fn.labelsAsRadios = function(){
	$(this).each(function(i)
	{	
		if($(this).is('[type=checkbox],[type=radio]'))
		{
			var input = $(this);

			input.hide();
			// get the associated label using the input's id
			var label = $('label[for='+input.attr('id')+']').addClass("labelAsRadio");

			//get type, for classname suffix 
			var inputType = input.attr("type");
			
			// find all inputs in this set using the shared name attribute
			var allInputs = $('input[name='+input.attr('name')+']');
			
			// necessary for browsers that don't support the :hover pseudo class on labels
			label.hover(
				function()
				{
					$(this).addClass('hover'); 
					if(inputType == 'checkbox' && input.is(':checked')){
						$(this).addClass('checkedHover'); 
					} 
				},
				function(){ $(this).removeClass('hover checkedHover'); }
			).
			click(function() 
			{
				$(this).siblings("input").click().trigger('updateState');
			});
			
			//bind custom event, trigger it, bind click,focus,blur events					
			input.bind('updateState', function()
			{	
				if (input.is(':checked')) 
				{
					if (input.is(':radio')) 
					{				
						allInputs.each(function(){
							$('label[for='+$(this).attr('id')+']').removeClass('checked');
						});		
					};
					label.addClass('checked');
				}
				else { label.removeClass('checked checkedHover checkedFocus'); }
										
			})
			.trigger('updateState')
			.click(function(){ 
				$(this).trigger('updateState'); 
			})
			.focus(function(){ 
				label.addClass('focus'); 
				if(inputType == 'checkbox' && input.is(':checked')){ 
					$(this).addClass('checkedFocus'); 
				} 
			})
			.blur(function(){ label.removeClass('focus checkedFocus'); });
		}
	});
};
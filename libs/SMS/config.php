<?php
// необходим акаунт на http://sms-sending.ru/
// Должно быть установлено:
// - curl
// - iconv
// - json_decode
// реализация в silentspace.ru -> OrderController.class.php
// todo: оформить в виде модуля и подвязать на смс рассылки, пока что только либа, которую можно использовать в любом направлении
return array(
	"HTTPS_LOGIN"				 => "", //Ваш логин для HTTPS-протокола
	"HTTPS_PASSWORD"			 => "", //Ваш пароль для HTTPS-протокола
	"HTTPS_ADDRESS"				 => "http://lcab.sms-sending.ru/", //HTTPS-Адрес, к которому будут обращаться скрипты. Со слэшем на конце.
	"HTTP_ADDRESS"				 => "http://lcab.sms-sending.ru/", //HTTP-Адрес, к которому будут обращаться скрипты. Со слэшем на конце.
	"HTTPS_METHOD"				 => "curl", //метод, которым отправляется запрос (curl или file_get_contents)
	"USE_HTTPS"					 => 0, //1 - использовать HTTPS-адрес, 0 - HTTP
	//Класс попытается автоматически определить кодировку ваших скриптов.
	//Если вы хотите задать ее сами в параметре HTTPS_CHARSET, то укажите HTTPS_CHARSET_AUTO_DETECT значение FALSE
	"HTTPS_CHARSET_AUTO_DETECT"	 => false,
	"HTTPS_CHARSET"				 => "utf-8" //кодировка ваших скриптов. cp1251 - для Windows-1251, либо же utf-8 для, сообственно - utf-8 :) 
);

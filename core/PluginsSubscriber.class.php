<?php
/**
 * Статический класс для подписки плагинов на события
 * @version 1.0
 * @author George
 * 
 * TODO Редактирование файла со списком подписчиков через этот класс
 *
 */
class PluginsSubscriber
{
    /**
     * Функция смотрит в файлик со списком подписчиков и 
     * подписывает их через диспетчер
     * @return void
     */
    public static function SubscribeAll($list)
    {
        $dispatcher = EventDispatcher::getInstance();
        
        $cnt = count($list);
        for ($i = 0; $i < $cnt; $i++)
        {
            if (!isset($list[$i]['event']))
                throw new Exception('Event name must be defined in listeners list.');
            if (!isset($list[$i]['listener']))
                throw new Exception('Listener must be defined in listeners list.');
            if (!isset($list[$i]['handler']))
                throw new Exception('Handler must be defined in listeners list.');
            $dispatcher -> RegisterListener($list[$i]['event'], $list[$i]['listener'], $list[$i]['handler']);
        }
    }
}
?>
<?php
/**
 * Родительский абстрактный класс для всех элементов управления (контролов)
 *
 * @version 3.5
 *
 */
abstract class Control
{

	/**
     * Клиентский путь к контролам
     * Инициализируется во время выполения
     *
     * @var string
     */
    public static $CONTROL_ALIAS = 'ContentManager/Controls/';
    public static $CONTROLS_ALIAS = 'ContentManager/Controls/';

    /**
     * Физический путь к контролам
     * Инициализируется во время выполения
     *
     * @var string
     */
    public static $CONTROL_PATH = 'ContentManager/Controls/';
    public static $CONTROLS_PATH = 'ContentManager/Controls/';

    /**
	 * Провайдер реестра полей
	 *
	 * @var IControlsRegistryProvider
	 */
	protected $controlsRegistry;

	/**
	 * Параметры элемента управления
	 * Специфические параметры для элемента управления, хранящиеся в реестре полей
	 *
	 * @var array
	 */
	protected $controlSettings = null;

	/**
	 * Подпись к элементу управления, отображается в связанном теге label
	 * Задаётся в реестре полей.
	 *
	 * @var string
	 */
	public $description = null;

	/**
	 * Подсказка к элементу управления.
	 * Задаётся в реестре полей.
	 *
	 * @var string
	 */
	public $tip = null;

	/**
	 * Имя элемента управления. Имя поля таблицы, полученного из реестра полей.
	 * В общем случае подставляется в атрибут name
	 * контрола. В случае, если контрол состоит из группы тегов input, то
	 * name используется как префикс ко всем связанным полям.
	 *
	 * @var string
	 */
	public $name = null;

	/**
	 * Путь к шаблону элемента управления.
	 *
	 * @var string
	 */
	public $controlTplPath = null;

	/**
	 * Массив переменных, которые необходимо передать шаблонизатору.
	 *
	 * @var array
	 */
	public $controlTplVars = null;

	/**
	 * HTML-код элемента управления. В простейших контролах,
	 * не требуется парсинг связанных с контролом шаблонов,
	 * генерится сразу html-код.
	 *
	 * @deprecated
	 *
	 * @var string
	 */
	public $controlHTML = null;

	/**
	 * Массив переменных для JS части контрола
	 *
	 * @var array
	 */
	public $controlJSVars = null;

	/**
	 * Необходимые стандартные JS библиотеки, используемые в шаблоне
	 * в виде массива их константных названий,
	 * например AUTOCOMPLETE, к которым подставляется INCLUDE_
	 * и передаётся в шаблон.
	 * В случае с AJAX-загрузкой формы. Эти библиотеки должны грузится
	 * JS-движком.
	 *
	 * @var array
	 */
	public $controlJSLibs = null;

	/**
	 * JS-файлы контрола с инициализирующими функциями.
	 *
	 * @var array
	 */
	public $controlJSFiles = null;

	/**
	 * Имя блока, в котором отображается контрол.
	 * Связанные по смыслу контролы могут группироваться в блоки.
	 * Имя блока обычно отображается на форме. Достаточно задать
	 * имя блока в одном контроле.
	 *
	 * @var name
	 */
	public $blockName = null;

	/**
	 * Номер блока.
	 * Номер блока, к которому относится контрол.
	 *
	 * @var int
	 */
	public $blockOrder = 0;

	/**
	 * CSS-класс контрола.
	 * Достаточно прописать в одном контроле блока.
	 *
	 * @var string
	 */
	public $CSSClass = null;

	/**
	 * Порядок контрола в блоке контролов.
	 * Крнтролы сортируются по возрастанию порядка.
	 *
	 * @var int
	 */
    public $controlOrder = 0;

    /**
     * Флаг, передающееся в шаблон,сообщающий о том,
     * что после ассинхронного сохранения формы,
     * её необходимо перезагрузить, поскольку шаблон
     * контрола не поддерживает самообновление.
     * Нужно в сложных ajax-контролах.
     *
     * @var  bool
     */
	public $refreshFormAfterFlush = false;

	/**
	 * Флаг видимости контрола
	 *
	 * @var bool
	 */
	public $visible;

	/**
	 * Флаг обязательности заполнения
	 *
	 * @var bool
	 */
	public $required;

	/**
	 * Флаг редактируемости
	 *
	 * @var bool
	 */
	public $disabled;

	/**
	* Идентификатор поля в реестре полей
	* @var int
	*/
	public $fieldRegistryID;

	/**
	 * Название таблицы (привязка контрола к названию таблицы)
	 * @var string
	 */
	public $tableName;

	/**
	 * Индекс формы, для случаев дублирования формы на странице
	 * @var int
	 */
	public $formIndex;
	public $data = array();

	/**
	 * Конструктор
	 *
	 * @param string $name
	 * @param string $description
	 * @param string $tip
	 * @param bool $required
	 * @param bool $visible
	 * @param bool $disabled
	 * @param string $controlOrder
	 * @param string $blockOrder
	 * @param string $blockName
	 * @param string $CSSClass
	 * @param array $controlSettings
	 * @param IControlsRegistryProvider $controlsRegistry
	 * @param DBTableStructure $activeTableStructure
	 * @param string $tableName
	 * @param int $formIndex
	 * @param array $data
	 */
	public function __construct($name, $description, $tip, $required, $visible, $disabled, $controlOrder, $blockOrder, $blockName = null, $CSSClass = null, $controlSettings = null, &$controlsRegistry = null, $tableName = null, $formIndex = null, $data = array())
	{
	    $this->name				 = $name;
		$this->description		 = $description;
		$this->tip				 = $tip;
		$this->required			 = $required;
		$this->visible			 = $visible;
		$this->disabled			 = $disabled;
		$this->controlOrder		 = $controlOrder;
		$this->blockOrder		 = $blockOrder;
		$this->blockName		 = $blockName;
		$this->CSSClass			 = $CSSClass;
		$this->controlsRegistry	 = &$controlsRegistry;
		$this->controlSettings	 = &$controlSettings;
		$this->tableName		 = $tableName;
		$this->formIndex		 = $formIndex;
		$this->data				 = $data;
	}

	/**
	 * Преобразование в массив для сериализации.
	 * Используется при сереализации для передачи
	 * ajax-загрузчику формы.
	 *
	 * @return array
	 */
	public function ToArray()
	{
		$result = array();

		$result['controlTplPath']		 = $this->controlTplPath;
		$result['controlTplVars']		 = $this->controlTplVars;
		$result['controlHTML']			 = $this->controlHTML;
		$result['controlJSVars']		 = $this->controlJSVars;
		$result['controlJSVarsJSON']	 = JSON::Encode($this->controlJSVars);
		$result['controlJSLibs']		 = $this->controlJSLibs;
		$result['controlJSFiles']		 = $this->controlJSFiles;
		$result['type']					 = $this->type;
		$result['description']			 = $this->description;
		$result['tip']					 = $this->tip;
		$result['name']					 = $this->name;
		$result['blockName']			 = $this->blockName;
		$result['blockOrder']			 = $this->blockOrder;
		$result['CSSClass']				 = $this->CSSClass;
		$result['controlOrder']			 = $this->controlOrder;
		$result['refreshFormAfterFlush'] = $this->refreshFormAfterFlush;
		$result['tableName']			 = $this->tableName;
		$result['formIndex']			 = $this->formIndex;

		return $result;
	}

	/**
	 * Функция, инициализирующая элемент управления
	 *
	 * @param mixed $value
	 * @param int	$rowID		Нужен для отношения many-to-many
	 */
	abstract public function InitControl($value = null, $rowID = null);

	/**
	 * Вызывается непосредственно при получении POST-запроса
	 * с данными из формы.
	 * Используется для скрытого преобразования данных или валидации
	 * в простых контролах, не использующих Flush.
	 *
	 * @param array $data
	 * @param int $rowID
	 */
	abstract public function PostBackHandler(&$data, $rowID = null);
}
?>
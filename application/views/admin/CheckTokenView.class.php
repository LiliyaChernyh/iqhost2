<?php
/**
 * Представление для формы восстановления пароля по токену админки
 * 
 * @version 1.0
 *
 */
class CheckTokenView extends AdminView  
{
    public function __construct()
    {
        parent::__construct();

        $this -> tpl -> contentTpl = $this -> tplsPath . 'pwdrecovery/newpassword.tpl';
	$this -> tpl -> assign('SCRIPT_FILE', AppSettings::$ADMIN_SCRIPTS.'pwdrecovery.js');	
    }
}
?>
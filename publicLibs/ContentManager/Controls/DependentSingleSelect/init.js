﻿function DependentSingleSelectInit(initObject)
{
	var dependsOn = initObject.dependsOn;
	var controlName = initObject.controlName;
	$("#dependentSingleSelect_"+controlName).data("val", initObject.value);

	var dependsOnControl = $("#"+dependsOn);
	
	if (dependsOnControl.length == 0) dependsOnControl = $(":input[name="+dependsOn+"]");
	if (dependsOnControl.length == 0)
	{
		$.jGrowl("No controls with name or ID " + dependsOn + " were found. Dependent control could not be initialized.");
		return false;
	}
	
	if (dependsOnControl.attr("multiple"))
	{
		dependsOnControl.children().click(getNewOptions);
	}
	else
	{
		if (typeof initObject.dependsOnValue != "undefined" && dependsOnControl.val() == 0)
		{
			dependsOnControl.val(initObject.dependsOnValue);
			//dependsOnControl.find("option[value="+initObject.dependsOnValue+"]").attr("selected", true);
		}
		dependsOnControl.change(getNewOptions);
	}
	
	if (dependsOnControl.data("dependantControlsRegstryIDs") == null)
		dependsOnControl.data("dependantControlsRegstryIDs", new Array());
	
	dependsOnControl.data("dependantControlsRegstryIDs").push(initObject.fieldsRegistryEntryID);
	dependsOnControl.data("serverScript", initObject.serverScript);
	getNewOptions(null, dependsOn, controlName, true);
}

function getNewOptions(event, dependsOn, controlName, loadOptions4Last)
{
	var val = null;
	var dependsOnControl = null;

	if (event != null)
	{
		var that = $(this);
		if (that.is("option"))
			dependsOnControl = that.parent();
		else
			dependsOnControl = that;
	}
	else if (dependsOn)
	{
		dependsOnControl = $("#"+dependsOn);
		if (dependsOnControl.length == 0) dependsOnControl = $(":input[name="+dependsOn+"]");
	}
	else
	{
		//console.error("No event or dependsOn params were defined.");
		return false;
	}

	// В зависимости от тип контрола выбираем его текцщее значение. Если это не multiple select, то всё просто.
	// если это multiple select, то сложнее. Группа чекбоксов не обрабатывается.
	var multiple = false;
	if (dependsOnControl.attr("multiple"))
	{
		val = new Array();
		dependsOnControl.children(":selected").each(function() {
			val.push($(this).val());
		});
		multiple = true;
	}
	else
	{
		val = dependsOnControl.val();
	}
	
	var data = new Object();
	if (multiple && val.length > 0)
		data["dependsOnValue[]"] = val;
	else if (multiple)
		data["dependsOnValue"] = "";
	else
		data["dependsOnValue"] = val;
	
	var dependentFieldIDs = dependsOnControl.data("dependantControlsRegstryIDs");

	for (var i = (loadOptions4Last ?  dependentFieldIDs.length - 1 : 0); i < dependentFieldIDs.length; i++)
	{
		data["fieldsRegistryEntryID"] = dependentFieldIDs[i];
		$.post(dependsOnControl.data("serverScript"), 
			   data,
			   updateSingleSelectOptions,
			   "json");
	}
}

function updateSingleSelectOptions(data, status)
{
	if (!standartAJAXResponseHandler(data, status)) return;
	if (data.options && data.controlName)
	{
		var controlContainer = $("#dependentSingleSelect_"+data.controlName);
		controlContainer.empty();
		controlContainer.append($("<option value=''>-</option>"));
		//if (data.options.length == 0) controlContainer.text("Нет опций");
		for (var i = 0; i < data.options.length; i++)
		{
			var inp = $("<option value=\""+data.options[i].value+"\" id=\""+ data.controlName + "_" + data.options[i].value+"\">"+data.options[i].text+"</option>");
			if (data.options[i].value == controlContainer.data("val"))
				inp.attr("selected", true);
			controlContainer.append(inp);
		}
	}
}
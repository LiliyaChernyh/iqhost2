{if !empty($CATEGORIES)}
	<ul class="listContainer parent_{$CATEGORIES[0].categoryParentID} level_{$LEVEL}" {if $LEVEL != 0} style="display:none;"{/if}>
		{foreach from=$CATEGORIES item=PAGE_ITEM}
			<li class="row pageID_{$PAGE_ITEM.categoryID}">
				<div class="leftControlsContainer">
					<span class="item checkbox"><input type="checkbox" name="checked[]" value="{$PAGE_ITEM.categoryID}" /></span>
					<span class="item arrows"><img src="{$ADMIN_IMAGES}icons/arrows_updown.png" alt="Переместить" /></span>
				</div>
				<div class="captionContainer">
					<span class="item caption" style="margin-left: {math equation="lev * step" lev=$LEVEL step=15}px;"><span class="toggleArrow {if !empty($PAGE_ITEM.children)}closed{/if}">{if !empty($PAGE_ITEM.children)}<img src="{$ADMIN_IMAGES}icons/arrow_right.png" align="absmiddle" />{/if}</span>{$PAGE_ITEM.categoryName}</span>
				</div>
				<div class="rightControlsContainer">
					<span class="item delete"><img src="{$ADMIN_IMAGES}icons/cross_bg.png" title="Удалить" alt="Удалить" /></span>
					<span class="item clone"><img src="{$ADMIN_IMAGES}icons/page_copy_bg.png" title="Клонировать" alt="Клонировать" /></span>
					<span class="item edit"><img src="{$ADMIN_IMAGES}icons/pencil_bg.png" title="Редактировать" alt="Редактировать" /></span>
					<span>{if $PAGE_ITEM.isActive == 1}<img src="/images/icons/active1.png">{else}<img src="/images/icons/active0.png">{/if}</span>
				</div>
			</li>
			{if !empty($PAGE_ITEM.children)}
				{include file='store/categories/list.tpl' CATEGORIES=$PAGE_ITEM.children LEVEL=$LEVEL+1}
			{/if}
		{/foreach}
	</ul>
{/if}
{if !empty($TAG_CAPTION)}<h1>Тег: {$TAG_CAPTION}</h1>{/if}

<!-- The file upload form used as target for the file upload widget -->
<form id="fileupload" method="POST" enctype="multipart/form-data">
    <div class="row fileupload-buttonbar">
        <div class="span7">
            <!-- The fileinput-button span is used to style the file input field as button -->
            <span class="btn fileinput-button">
                <i class="icon-plus icon-white"></i>
                <span>Добавить файл...</span>
                <input type="file" name="files[]" multiple>
            </span>
            <button type="submit" class="btn start">
                <span>Загрузить все</span>
            </button>
            <button type="reset" class="btn cancel">
                <span>Отменить все</span>
            </button>
            <button type="button" class="btn btn-danger delete">
                <span>Удалить</span>
            </button>
            <input type="checkbox" class="toggle">
            <!-- The loading indicator is shown during file processing -->
            <span class="fileupload-loading"></span>
        </div>
        <!-- The global progress information -->
        <div class="span5 fileupload-progress fade">
            <!-- The global progress bar -->
            <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                <div class="bar" style="width:0%;"></div>
            </div>
            <!-- The extended global progress information -->
            <div class="progress-extended">&nbsp;</div>
        </div>
    </div>
    <!-- The table listing the files available for upload/download -->
    <table role="presentation" class="table table-striped" style="width:650px;">
        <tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody>
    </table>
</form>
	
	
<!-- modal-gallery is the modal dialog used for the image gallery -->
<div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd" tabindex="-1">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h3 class="modal-title"></h3>
    </div>
    <div class="modal-body"><div class="modal-image"></div></div>
    <div class="modal-footer">
        <a class="btn btn-success modal-download" target="_blank">
            <span>Загрузить</span>
        </a>
        <a class="btn modal-play modal-slideshow" data-slideshow="5000">
            <span>Слайд-шоу</span>
        </a>
    </div>
</div>

{literal}
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td style="width:300px;">
            <span class="preview"></span>
        </td>
        <td>
            <span class="name">Имя: {%=file.name%}</span>
            {% if (file.error) { %}
                <div><span class="label label-important">Error</span> {%=file.error%}</div>
            {% } %}
            <br/>

            <span class="size">Размер: {%=o.formatFileSize(file.size)%}</span>
            {% if (!o.files.error) { %}
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
            {% } %}
        </td>
        <td>
            {% if (!o.files.error && !i && !o.options.autoUpload) { %}
                <button class="btn start">
                    <span>Загрузить</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn cancel">
                    <span>Отменить</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade" style="overflow: hidden;">
        <td>
            <div style="width:150px;">
                <span class="preview">
                    {% if (file.thumbnail_url) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
                    {% } %}
                </span>
            </div>
        </td>
        <td class="block_imagesID">
            <input class="imagesID" type="hidden" form="foto" value="{%=file.imageID%}">
            <div style="width:150px;">
                <span class="name">
                    Имя: <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
                </span>
                {% if (file.error) { %}
                    <div><span class="label label-important">Error</span> {%=file.error%}</div>
                {% } %}
                <br/>

                <span class="size">Размер: {%=o.formatFileSize(file.size)%}</span>
            </div>
        </td>
        <td>
            <div class="formElement round">
                <select multiple class="tagID" class="form-control" form="foto" data-image-id="{%=file.imageID%}" style="width:300px;height:150px;margin-bottom:0;" name="update">
                    <option value="0" selected style="display:none">-</option>
                    {/literal}
                    {foreach from=$TREE_TAGS item=TYPE_TAG}
                    <option disabled>{$TYPE_TAG.caption}</option>
                        {if isset($TYPE_TAG.tags)}
                        {foreach from=$TYPE_TAG.tags item=TAG}
                        <option value="{$TAG.tagID}" {literal}{% if (file.tagsID !== undefined){ for (var j=0; j<file.tagsID.length; j++) { if (file.tagsID[j] == "{/literal}{$TAG.tagID}{literal}") %}{%='selected'%}{% }}else{ if("{/literal}{$TAG.tagID}{literal}" == "{/literal}{$TAG_ID}{literal}"){ %}{%='selected'%}{% }} %}{/literal}>&nbsp;&nbsp;{$TAG.caption}</option>
                        {/foreach}
                        {/if}
                    {/foreach}
                    {literal}
                <select>
            </div>
        </td>
        <td>
            <div class="formElement round">
                <input class="caption" type="text" form="foto" value="{%=file.caption%}" data-image-id="{%=file.imageID%}">
            </div>
            
            <div class="formElement round">
                <textarea class="description" form="foto" style="height:100px;margin-bottom:0;" data-image-id="{%=file.imageID%}">{%=file.description%}</textarea>
            </div>
        </td>
        <td>
            <div style="width:100px;">
                <button class="btn btn-danger delete" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <span>Удалить</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            </div>
        </td>
    </tr>
{% } %}
</script>
{/literal}
    
<form id="foto" method="POST" action="/admin/Fotobank/uploads/{$TAG_ID}">
    <input id="save" name="save" type="submit" value="Сохранить">
</form>
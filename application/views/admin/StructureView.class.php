<?php
/**
 * Представление для раздела управления страницами
 * 
 * @version 1.2
 *
 */
class StructureView extends AdminView 
{
	private $action = null;
	
    public function __construct($tplName = 'main.tpl')
    {
        parent::__construct($tplName);
        
        // Адрес к шаблонам раздела
        $this -> tplsPath = $this -> tplsPath .'structure/';
        
        // Шаблон с меню
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
    }
    
    /**
     * Выполняет настройку представления в зависимости от текущего действия
     *
     * @param string $action
     */
    public function SetAction($action)
    {
    	ViewData::Append('SCRIPT_FILE', AppSettings::$ADMIN_SCRIPTS. 'structure.js');
        $this -> action = strtolower($action);
        
        switch ($this -> action)
        {
            case 'add':
                $this -> tpl -> contentTpl = $this -> tplsPath .'page_form.tpl';
                
		        // Подключаем необходимые JS-библиотеки
		        ViewData::Assign('INCLUDE_JQUERY_UI', 1);
				ViewData::Assign('INCLUDE_JQUERY_UI_DATEPICKER', 1);
				ViewData::Assign('INCLUDE_AUTOCOMPLETE', 1);
				ViewData::Assign('INCLUDE_TREEVIEW', 1);
				ViewData::Assign('INCLUDE_FCK', 1);
				ViewData::Assign('INCLUDE_SWFUPLOAD', 1);
				ViewData::Assign('INCLUDE_DAMNUPLOAD', 1);
				ViewData::Assign('INCLUDE_JCROP', 1);
				ViewData::Assign('INCLUDE_JQUERY_UI_RESIZABLE', 1);
				ViewData::Assign('INCLUDE_JQUERY_UI_DRAGGABLE', 1);
				ViewData::Assign('INCLUDE_JQUERY_UI_DIALOG', 1);
	
		        ViewData::Assign('HEADER', 'Создание страницы');
		        ViewData::Assign('PAGE', Page::EmptyArray());
		        ViewData::Assign('HIDE_CONTENT_FORM', 1);
		        
//		        $this -> tpl -> assign('SCRIPT', $this -> sectionAlias.'add/');
  	            ViewData::Assign('SUBMIT_NAME', 'doAdd');
  	            ViewData::Append('JS_VARS', array('editMode' => 0));
  
                break;
                
            case 'edit':
            	$this -> tpl -> contentTpl = $this -> tplsPath .'page_form.tpl';
                
		        // Подключаем необходимые JS-библиотеки
		        ViewData::Assign('INCLUDE_JQUERY_UI', 1);
				ViewData::Assign('INCLUDE_JQUERY_UI_DATEPICKER', 1);
				ViewData::Assign('INCLUDE_AUTOCOMPLETE', 1);
				ViewData::Assign('INCLUDE_TREEVIEW', 1);
				ViewData::Assign('INCLUDE_FCK', 1);
				ViewData::Assign('INCLUDE_SWFUPLOAD', 1);
				ViewData::Assign('INCLUDE_DAMNUPLOAD', 1);
				ViewData::Assign('INCLUDE_JCROP', 1);
				ViewData::Assign('INCLUDE_JQUERY_UI_RESIZABLE', 1);
				ViewData::Assign('INCLUDE_JQUERY_UI_DRAGGABLE', 1);
				ViewData::Assign('INCLUDE_JQUERY_UI_DIALOG', 1);
		        
//		        $this -> tpl -> assign('SCRIPT', $this -> sectionAlias.'add/');
				ViewData::Assign('RIGHTBLOCK_ENABLED', 1);
				ViewData::Assign('DELETE_ENABLED', 1);
				ViewData::Assign('CLONE_ENABLED', 1);
  	            ViewData::Assign('SUBMIT_NAME', 'doEdit');
  	            ViewData::Append('JS_VARS', array('editMode' => 1));
            	
                break;
                
            case 'index':
                $this -> tpl -> contentTpl = $this -> tplsPath.'list.tpl';
            	ViewData::Append('SCRIPT_FILE', AppSettings::$ADMIN_SCRIPTS .'pagesList.js');
            	ViewData::Append('STYLE_FILE', AppSettings::$ADMIN_STYLES .'pagesList.css');
            	ViewData::Append('JS_VARS', array('IMAGES_ALIAS' => AppSettings::$IMAGES_ALIAS));
            	
		        ViewData::Assign('INCLUDE_TREEVIEW', 1);
		        ViewData::Assign('HTTP_HOST', $_SERVER['HTTP_HOST']);
		        ViewData::Assign('SERVER_ADDR', $_SERVER['SERVER_ADDR']);
		        
				ViewData::Assign('INCLUDE_JQUERY_UI_SORTABLE', 1);
                break;
             
            case 'index1':
            	$this -> tpl -> contentTpl = $this -> tplsPath.'welcome_content.tpl';
            	
            	ViewData::Assign('INCLUDE_TREEVIEW', 1);
		        ViewData::Assign('HTTP_HOST', $_SERVER['HTTP_HOST']);
		        ViewData::Assign('SERVER_ADDR', $_SERVER['SERVER_ADDR']);
            	break; 
            	
            case 'recycled':
            	$this -> tpl -> contentTpl = $this -> tplsPath.'recycled_content.tpl';
            	ViewData::Append('SCRIPT_FILE', AppSettings::$ADMIN_SCRIPTS . 'recycledPagesForm.js');
		        ViewData::Assign('INCLUDE_TREEVIEW', 1);
            	
			break;			        		
        }
    }
   /*    
    * Пример костылей, запрещающий удалять или изменять алиас у определённых страниц
    
    public function ExecuteResult()
    {
        if ($this -> action == 'add')
        {
            // Нельзя добавлять новостную страницу страницы
            $options = ViewData::Get('PAGE_TYPES_OPTION');
            $newOptions = array(); 
            for ($i = 0; $i < count($options); $i++)
                if ($options[$i]['constantName'] != 'NEWS')
                    $newOptions[] = $options[$i];
            ViewData::Delete('PAGE_TYPES_OPTION');
            ViewData::Assign('PAGE_TYPES_OPTION', $newOptions);
        }
        else if ($this -> action == 'edit')
        {
            $page = ViewData::Get('PAGE');
            if (in_array($page['pageID'], array(AppSettings::$PAGE_ID_MAIN,
                                                AppSettings::$PAGE_ID_NEWS
                                                )))
                ViewData::Delete('RIGHTBLOCK_ENABLED');
                
            if (in_array($page['pageID'], array(AppSettings::$PAGE_ID_CONTACTS
                                                )))
                ViewData::Assign('ALIAS_READONLY', 1);
        }

        parent::ExecuteResult();
    }*/
}
?>
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ManualMenuController
 *
 * @author
 */
class CustomMenuAdminController extends AdminController
{
	protected $model;
        protected static $aliases = array();
	public function __construct()
	{
		parent::__construct();
		$this->model = new CustomMenuModel();
		$this -> view = new CustomMenuView();
                self::$aliases = Request::GetAliases(null, 1);
	}

	public function Index($args = null)
	{
            if (!empty($args))
            {
                switch ($args[0])
                {
                    case 'edit':
                        return $this->EditMenu($args[1]);
                        break;
                    case 'add':
                        return $this->AddMenu($args[0]);
                        break;
                    case 'del':
                        return $this->RemoveMenu($args[1]);
                        break;
                    case 'showinner':
                        return $this->ShowInnerMenu(self::$aliases);
                        break;
                }

            }
            ViewData::Assign("LIST_MENU", $this->model->GetListMenu());
            $this->view->Index();
            return $this->view;
	}

        private function ShowInnerMenu($args = null)
        {
            if (!empty($args[4]))
            {
                switch ($args[4])
                {
                    case 'edit':
                        return $this->EditInnerMenu($args);
                        break;
                    case 'addinner':
                            return $this->AddInnerMenu($args);
                            break;
                    case 'del':
                            return $this->DeleteInnerMenu($args);
                            break;
                }
            }
           ViewData::Assign("PARENT_NAME", $this->model->GetParentName($args[3]));



           $innerMenu = $this->model->GetInnerMenu($args[3]);
           ViewData::Assign("INNER_MENU", $this->model->GetInnerMenu($args[3]));
            $this->view->Inner();
            return $this->view;
        }

        private function AddMenu($args)
           {
            if (Request::IsAsync() and isset(Request::$POST['doSave']) && isset(Request::$POST['MenuList']) && !empty(Request::$POST['MenuList']))
            {
                $data = Request::$POST['MenuList'][0];
//                if (isset($data[$this->model->MenuListTN]))
//                {
                    try
                    {
                        $attr = array(
                        'menuListName'      =>  $data['menuListName'],
                        'menuListAlias'      => $data['menuListAlias'],
                        'menuListComment'      => $data['menuListComment']
                        );
                        if (isset($attr))
                        {
                            $this->model->AddMenu($attr);
                            return new JsonActionResult(array('msg' => 'Элемент "'.$data['menuListName'].'" успешно добавлен', 'info' => "Элемент успешно добавлен", 'refresh' => 1));
                        }
                    }
                    catch (Exception $e)
                    {
                        return new JsonActionResult(array('error' => "Ошибка сохранения данных", "msg" => $e->getMessage()));
                    }
                return new JsonActionResult(array('info' => Request::$POST));
//                }
            }
               $this->view->AssignFormToTpl($this->model->MenuListTN);
               $this->view->Menu();
               return $this->view;
           }

        private function EditMenu($itemID)
           {
            if (Request::IsAsync() and isset(Request::$POST['doSave']) && isset(Request::$POST['MenuList']) && !empty(Request::$POST['MenuList']))
            {
                 $data = Request::$POST['MenuList'][0];

                    try
                    {
                        $attr = array(
                        'menuListName'      =>  $data['menuListName'],
                        'menuListAlias'      => $data['menuListAlias'],
                        'menuListComment'      => $data['menuListComment']
                        );
                        if (isset($attr))
                        {
                            $this->model->UpdateMenu($attr, $itemID);
                            return new JsonActionResult(array('msg' => 'Элемент "'.$data['menuListName'].'" успешно изменен', 'info' => "Элемент успешно добавлен", 'refresh' => 1));
                        }
                    }
                    catch (Exception $e)
                    {
                        return new JsonActionResult(array('error' => "Ошибка сохранения данных", "msg" => $e->getMessage()));
                    }
                return new JsonActionResult(array('info' => Request::$POST));

            }
               $this->view->AssignFormToTpl($this->model->MenuListTN,$itemID);
               $this->view->EditMenu();
               return $this->view;
           }

        private function RemoveMenu($itemID)
        {
            if (Request::IsAsync() and !empty($itemID)) {
                try
                {
                    $this->model->DeleteMenu($itemID);
                    return new JsonActionResult(array('msg' => "ok", 'rowID' => $itemID, 'refresh' => 1));
                }
                catch (Exception $e)
                {
                    return $this->Error("Невозможно удалить меню", AppSettings::$ADMIN_ALIAS . 'custommenu/');
                }
            }
            return $this->Error("Невозможно удалить меню", AppSettings::$ADMIN_ALIAS . 'custommenu/');
        }

        private function AddInnerMenu($args)
           {
            if (Request::IsAsync() and isset(Request::$POST['doSave']))
            {
                $data = Request::$POST;
                $data['menuListID'] = $data['Menu'][0]['menuListID'];
                FB::log($data);
                    try
                    {

                            $cManager = new ContentManager();
                            $categoryID = $cManager->FlushData($this->model->MenuTN, $data, false, false);
                            return new JsonActionResult(array('msg' => 'Элемент "'.$data['Menu'][0]['name'].'" успешно добавлен', 'info' => "Элемент успешно добавлен", 'refresh' => 1,'listMenuID' => $args[3]));

                    }
                    catch (Exception $e)
                    {
                        return new JsonActionResult(array('error' => "Ошибка сохранения данных", "msg" => $e->getMessage()));
                    }
                return new JsonActionResult(array('info' => Request::$POST));
            }
               ViewData::Assign("PARENT_NAME", $this->model->GetParentName($args[3]));
               $this->view->AssignFormToTpl($this->model->MenuTN);
               $this->view->InnerMenu();
               return $this->view;
           }

           private function EditInnerMenu($args)
           {
            if (Request::IsAsync() and isset(Request::$POST['doSave']))
            {
                $data = Request::$POST;
                    try
                    {
                            $cManager = new ContentManager();
                            $categoryID = $cManager->FlushData($this->model->MenuTN, $data, false, false);
                            return new JsonActionResult(array('msg' => 'Элемент "'.$data['Menu'][0]['name'].'" успешно обновлен', 'info' => "Элемент успешно добавлен", 'refresh' => 1,'listMenuID' => $args[3]));

                    }
                    catch (Exception $e)
                    {
                        return new JsonActionResult(array('error' => "Ошибка сохранения данных", "msg" => $e->getMessage()));
                    }
                return new JsonActionResult(array('info' => Request::$POST));
            }
            ViewData::Assign("PARENT_NAME", $this->model->GetParentName($args[3]));
               $this->view->AssignFormToTpl($this->model->MenuTN,$args[5]);
               $this->view->EditInnerMenu();
               return $this->view;
           }

        private function DeleteInnerMenu($args)
        {
            if (Request::IsAsync() and !empty($args[5])) {
                try
                {
                    $arr = $this->model->GetParent($args[5],$args[3]);
                    if (empty($arr))
                    {
                        $flag = true;
                    }
                    else
                    {
                        $flag = false;
                    }
                    $this -> model -> DeleteInnerMenu($args[5],$flag);
                    return new JsonActionResult(array('msg' => "ok", 'rowID' => $args[5], 'listMenuID' => $args[3], 'refresh' => 1));
                }
                catch (Exception $e)
                {
                    return $this->Error("Невозможно удалить меню", AppSettings::$ADMIN_ALIAS . 'custommenu/');
                }
            }
            return $this->Error("Невозможно удалить меню", AppSettings::$ADMIN_ALIAS . 'custommenu/');
        }


}

?>

<?php
/**
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License (version 2 or (at your option) any later version) as published by the Free Software Foundation.
 *
 * @author web-junior <admin@web-junior.net>
 * @version 0.1
 * @package
 * @link http://web-junior.net
 */

include_once 'Component.class.php';
include_once 'components/Offer.class.php';
include_once 'components/Category.class.php';
include_once 'components/Currency.class.php';


class YMarket {
	protected
		$shop = array(),
		$currencies = array(),
		$categories = array(),
		$offers = array(),
		$doctype = "<?xml version='1.0' encoding='utf-8'?>
		<!DOCTYPE yml_catalog SYSTEM 'shops.dtd'>\r\n<!-- This YML generated with YMarket 1.0(http://www.web-junior.net)-->\r\n",
		$rootElement = '',
		$shopElement = '',
		$name='',
		$company='',
		$url = '',
		$date = null,
		$deliveryCost=0;

	private static $specialChars = array('&'=>'&amp;','"'=>'&quot;','>'=>'&gt;',
		'<'=>'&lt;','`'=>'&apos;');



		public function __construct($name='', $company='', $url=''){
			$this->name = $name;
			$this->company = $company;
			$this->url = $url;
			$this->date = time();
			$this->rootElement = "<yml_catalog date='%04d-%02d-%02d %02d:%02d'>\r\n%s\r\n</yml_catalog>";
			$this->shopElement =
			"<shop>\r\n<name>%s</name>\r\n<company>%s</company>\r\n<url>%s</url>\r\n%s\r\n</shop>";

		}
		
		
		public function setDeliveryCost($cost = 0){
			$cost = intval($cost);
			if($cost>0){
				$this->deliveryCost = $cost;
			}
			
		}



		public function generate($output = false, $gz = false){

			$currencies = "<currencies>\r\n";
			foreach($this->currencies as $currency){
				$currencies.=$currency->generate();
			}
			$currencies .= "</currencies>\r\n";


			$categories = "<categories>\r\n";
			foreach($this->categories as $category){
				$categories.=$category->generate();
			}
			$categories.="</categories>\r\n";
			
			if($this->deliveryCost>0){
				$categories.="<local_delivery_cost>{$this->deliveryCost}</local_delivery_cost>";
			}

			$offers = "<offers>\r\n";
			foreach($this->offers as $offer){
				$offers.=$offer->generate();
			}
			$offers.="</offers>\r\n";

			$this->shopElement = sprintf($this->shopElement,
			self::specialChars($this->name), self::specialChars($this->company),
			self::specialChars($this->url),
			"$currencies \r\n$categories \r\n$offers\r\n");

			$this->rootElement = sprintf($this->rootElement,
			date('Y', $this->date), date("m", $this->date),
			date('d', $this->date), date('H', $this->date),
			date('i', $this->date), $this->shopElement);

			$data = $this->doctype.$this->rootElement;
			
			
		
			if($gz===true&&function_exists('gzencode')){
				$data = gzencode($data, 9);
			}

			if($output === true){
				if($gz===true&&function_exists('gzencode'))
				$header = array("Content-type:application/x-gzip","Content-Disposition: attachment; filename=ymarket.gz", "Content-length:".strlen($data));
				else
				$header = 'Content-type:application/xml';
				if(is_array($header))
				{
					foreach($header as $value){
						header($value);
					}
				}else{
				header($header);
				}
				echo $data;
			}else{
			return $data;
			}

		}

		public function issetComponent(Component $comp){
			switch(get_class($comp)){
				case 'Currency':
					return isset($this->currencies[$comp->getId()]);
				case 'Category':
					return isset($this->categories[$comp->getId()]);
				case 'Offer':
					return isset($this->offers[$comp->getId()]);
				default:
					return false;
			}
		}


		public function add(Component $comp, $replace = false){
			if(!$this->issetComponent($comp) || $replace === true){
			switch(get_class($comp)){
				case 'Currency':
					$this->currencies[$comp->getId()] = $comp;
					break;
				case 'Category':
					$this->categories[$comp->getId()] = $comp;
					break;
				case 'Offer':
				case 'BookOffer':
				case 'AudioBookOffer':
				case 'MusicOffer':
				case 'VideoOffer':
				case 'TourOffer':
				case 'TicketOffer':
					$this->offers[$comp->getId()] = $comp;
					break;
				default:
					break;
			}

			return true;

			}else{
				return false;
			}
		}
		


		public function delete(Component $comp){
			if($this->issetComponent($comp)){
				switch(get_class($comp)){
					case 'Currency':
						unset($this->currencies[$comp->getId()]);
						break;
					case 'Category':
						unset($this->categories[$comp->getId()]);
						break;
					case 'Offer':
						unset($this->offers[$comp->getId()]);
						break;
					default:
						break;
				}

				return true;
			}else{
				return false;
			}
		}


		public function replace(Component $comp){
			if($this->issetComponent($comp)){
				return $this->add($comp, true);
			}else{
				return false;
			}
		}



		public static function specialChars($str){
			foreach(self::$specialChars as $k=>$v){
				$symbols [] =$k;
				$chars[] = $v;
			}
			return str_replace($symbols, $chars, $str);
		}
}
?>
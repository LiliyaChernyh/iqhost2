<input type="hidden" name="{$localVars.tableName}[{$localVars.formIndex}][{$localVars.NAME}][]" value="0"/>
<select size="{$localVars.SIZE}" multiple="multiple" name="{$localVars.tableName}[{$localVars.formIndex}][{$localVars.NAME}][]" class="select2 {if $localVars.CLASS}{$localVars.CLASS}{/if}" {if isset($localVars.READONLY)} redaonly="readonly"{/if} style="width:400px;">
	{foreach from=$localVars.OPTIONS item=OPTION}
		<option value="{$OPTION.VALUE}" {if $OPTION.SELECTED} selected="selected"{/if} {if isset($OPTION.level) AND $OPTION.level== 2} class="level1"{/if}>{'&nbsp'|str_repeat:$OPTION.level*4}{$OPTION.TEXT}</option>
	{/foreach}
</select>
<?php

class Searcher
{
    public $keyWords;
    
    public $keyWordsForHighlight = null;
    
    public $searchQuery = null;
    
    /**
	 * Подготавливает строку, переданную пользователем к поиску
	 *
	 * @param string $searchquery
	 */
	protected function PrepareKeyWords($searchQuery)
	{
	    // уберём кавычки
		$searchQuery = str_replace('\'','',$searchQuery);
		// Разбиваем ключевые слова и выкидываем короткие
		$keyWords = preg_split("/[\s,.:\";!?']+/",$searchQuery);

		$russianWords = array();
		$englishWords = array();

		for ($i = 0; $i< count($keyWords); $i++)
		{
		    if (preg_match('/[a-zA-Z]+/', $keyWords[$i]))
		    {
		        if (UTF8::strlen($keyWords[$i]) >= 3)
                    $englishWords[] = UTF8::strtoupper($keyWords[$i]);
		    }
		    else 
		    {
    			if (UTF8::strlen($keyWords[$i]) < 4) 
                    continue;
    			$russianWords[] = UTF8::strtoupper($keyWords[$i]);
		    }
		}

	    // first we include phpmorphy library
        require_once(IncPaths::$LIBS_PATH .'PhpMorphy/src/common.php');
        
        // set some options
        $opts = array(
        	// storage type, follow types supported
        	// PHPMORPHY_STORAGE_FILE - use file operations(fread, fseek) for dictionary access, this is very slow...
        	// PHPMORPHY_STORAGE_SHM - load dictionary in shared memory(using shmop php extension), this is preferred mode
        	// PHPMORPHY_STORAGE_MEM - load dict to memory each time when phpMorphy intialized, this useful when shmop ext. not activated. Speed same as for PHPMORPHY_STORAGE_SHM type
        	'storage' => PHPMORPHY_STORAGE_FILE,
        	// Enable prediction by suffix
        	'predict_by_suffix' => true, 
        	// Enable prediction by prefix
        	'predict_by_db' => true,
        	// TODO: comment this
        	'graminfo_as_text' => true,
        );

        // Path to directory where dictionaries located
        $dir = IncPaths::$LIBS_PATH . 'PhpMorphy/dicts/utf-8';

        // Create phpMorphy instance
        try 
        {
            if (!empty($russianWords))
                $morphyRu = new phpMorphy($dir, 'ru_RU', $opts);
        	if (!empty($englishWords))
                $morphyEn = new phpMorphy($dir, 'en_EN', $opts);
        }
        catch(phpMorphy_Exception $e) 
        {
        	//Log::Error('Error occured while creating phpMorphy instance: ' . $e->getMessage());
        	return array_merge($russianWords, $englishWords);
        }

        $allFormsRu = array();
        $allFormsEn = array();
        
        if (isset($morphyRu))
            $allFormsRu = $morphyRu -> getAllForms($russianWords);
        if (isset($morphyEn))
            $allFormsEn = $morphyEn -> getAllForms($englishWords);

        $allForms = array_merge($allFormsEn, $allFormsRu);

        $result = array();
        foreach ($allForms as $key => $value)
        {
            $result[$key] = 1;
            $cnt = count($allForms[$key]);
            for ($i = 0; $i < $cnt; $i++)
            {
                $result[$allForms[$key][$i]] = 1;
            }
        }

        return array_keys($result);
	}
	
	protected function GetSearchQuery($query)
	{
	    if (!empty($this -> searchQuery))
            return $this -> searchQuery;
            
        if (empty($this -> keyWords))
            $this -> keyWords = $this -> PrepareKeyWords($query);
            
        foreach ($this -> keyWords as $key => $pattern)
            $this -> keyWordsForHighlight[$key] = '#('. $pattern .')#ui';
	    
        $this -> searchQuery = '';
	    if (is_array($this -> keyWords))
            $this -> searchQuery = implode(' ', $this -> keyWords);
	    elseif (is_string($this -> keyWords))
            $this -> searchQuery = $this -> keyWords;
	    elseif (empty($this -> keyWords))
            return null;
            
	    // Закавычим все слова с дефисом + экран
		$this -> searchQuery = preg_replace('/([a-zA-Zа-яА-Я]+)-([a-zA-Zа-яА-Я]+)/u', '\\\'$1-$2\\\'', $this -> searchQuery);
		
		return $this -> searchQuery;
	}
}

?>
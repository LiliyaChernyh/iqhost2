/**
    Document   : store.js
    Created on : 02.06.2013, 10:16:58
    Author     : Ivkov Eugene
    Version    : 1.01a
    Description:
        Набор скриптов для контрольной панели магазина.
*/

$(document).ready(function(){
    $("#addMenuList").validate({
      
	rules: {
	    
	},
	messages: {
	},
	submitHandler: function(form) {
		$(form).ajaxSubmit({success:saveMenuResponse, beforeSubmit:showRequest, dataType:"json"});
		return false;
	}
    });
    $("#addInnerMenu").validate({
      
	rules: {
	    
	},
	messages: {
	},
	submitHandler: function(form) {
		$(form).ajaxSubmit({success:saveInnerMenuResponse, beforeSubmit:showRequest, dataType:"json"});
		return false;
	}
    });
    $(".deleteEntry, .deleteEntry a").unbind("click").click(deleteItemClick);
    
        $(".ul-dropfree").find("li:has(ul)").prepend('<div class="drop"></div>');
	$(".ul-dropfree div.drop").click(function() {
		if ($(this).nextAll("ul").css('display')=='none') {
			$(this).nextAll("ul").slideDown(400);
			$(this).css({'background-position':"-11px 0"});
		} else {
			$(this).nextAll("ul").slideUp(400);
			$(this).css({'background-position':"0 0"});
		}
	});
	$(".ul-dropfree").find("ul").slideUp(400).parents("li").children("div.drop").css({'background-position':"0 0"});

});

function showRequest(form)
{
    $(".storeForm input:submit").attr("disabled",true);
}
    
function saveMenuResponse(data, status)
{
	$(".menuForm input:submit").attr("disabled",false);
	    $.jGrowl(data.msg);
	    setTimeout("window.location = '/admin/custommenu/'", 300);	
}

function saveInnerMenuResponse(data, status)
{
	$(".menuForm input:submit").attr("disabled",false);
	    $.jGrowl(data.msg);
	    setTimeout("window.location = '/admin/custommenu/showinner/"+data.listMenuID+"'", 300);	
}

function deleteItemClick(event)
{
	event.preventDefault();

	var that = $(this);
	var link = (that.is("a")) ? that.attr("href") : that.children('a').attr('href');       
        $.data.ShopDelMenu = that;
	//window.deleteDialog.data.link = link;
	$.post(link, {}, itemDeleteResponse, "json");
	return false;
}

function itemDeleteResponse(data, status)
{
	if (!standartAJAXResponseHandler(data, status)) return;
	if (data.msg == "ok")
	{
            var $that = $(this);          
            $(".deleteEntry a[href*=\"/"+data.rowID+"/\"]").parent().parent().remove();
            ($.data.ShopDelMenu).parent().remove();
            var list = $("#entriesList");
            $.jGrowl("Запись удалена");
            if ($("#entriesList tbody").length == 0) list.replaceWith( "Таблица пуста" );
	}
	/*else if(data.dependentEntries)
	{
                if (data.option){
                    $("#field-name").text(data.option.optionName);
                }
                if (data.attribute){
                    $("#field-name").text(data.attribute.attributeName);
                }

		$("#products-number").text(data.dependentEntries);

		window.deleteDialog.data.confirmToken = data.confirmToken;
		window.deleteDialog.dialog("open");
	}*/
}

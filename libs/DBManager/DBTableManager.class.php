<?php

require_once('DBTableStructure.class.php');
require_once('DBFieldStructure.class.php');

/**
 * Менеджер таблиц базы данных
 * на PDO
 *
 * @version 3.3
 *
 */
class DBTableManager
{
    /**
     * Название таблицы
     *
     * @var string
     */
    private $TableName;

    /**
     * Структура таблицы
     * Необходима для контроля правильности именования полей при передаче
     * параметров. При неправильном имени эти поля игнорируются.
     *
     * @var DBTableStructure
     */
    public $TableStructure;

    /**
     * Флаг необходимости использования структуры таблицы
     * задаётся при создании объекта
     *
     * @var unknown_type
     */
    private $useStructure = true;

    /**
     * Часть запроса, в которой определяется поле(я),
     * по которому(ым) сортировать результат выборки из таблицы
     *
     * @var string
     */
    public $SortOrder;

    /**
     * Созданные объекты класса
     *
     * @var array
     */
    public static $instances;


    /**
     * Конструктор
     *
     * @param string $tableName
     * @param DBTableStructure $tableStructure
     * @return DBTableManager
     */
    public function __construct($tableName, $tableStructure = null, $useStructure = true)
    {
        DB::Connect();
        $this -> TableName = $tableName;
        $this -> useStructure = $useStructure;

        if ($this -> useStructure)
        {
	        if ($tableStructure == null)
	            $this -> TableStructure = DBTableStructure::getInstance($this -> TableName);
	        else
	            $this -> TableStructure = $tableStructure;
        }

        $this -> SortOrder = '';
    }

    /**
     * Возвращает менеджер таблицы
     *
     * @param string $tableName
     * @return DBTableManager
     */
    public static function getInstance($tableName, $tableStructure = null, $useStructure = true)
    {
        if (!isset(self::$instances[$tableName]))
        {
            self::$instances[$tableName] = new DBTableManager($tableName, $tableStructure, $useStructure);
        }

        return self::$instances[$tableName];
    }

    /**
     * Начать использовать структуру.
     * Произойдёт загрузка структуры, если этого не было сделано
     *
     */
    public function BeginToUseStructure()
    {
    	if (empty($this -> TableStructure))
    	{
    		$this -> TableStructure = DBTableStructure::getInstance($this -> TableName);
    		$this -> useStructure = true;
    	}
    }

    /**
     * Возвращает массив записей, удовлетворяющих заданным в массиве $attributes условиям
     *
     * @param array $attributes
     * @param int $limit
     * @param int $start
     * @param array $fields
     * @return array
     */
    public function Select($attributes = null, $limit = null, $start = null, $fields = null)
    {
        $selectFields = '*';

        $fieldsNames = array();
        if ($this -> useStructure && (!empty($fields) || !empty($attributes)))
            $fieldsNames = $this -> TableStructure -> GetFieldsNames();

        if (!empty($fields))
        {
            $selectFieldsArr = array();
            for ($i = 0; $i < count($fields); $i++)
            {
                if ($this -> useStructure && in_array($fields[$i], $fieldsNames) || !$this -> useStructure)
                {
                	$selectFieldsArr[] = '`'. $fields[$i] .'`';
                }
            }
            if (!empty($selectFieldsArr))
                $selectFields = implode(', ', $selectFieldsArr);
        }

        $conditions = "";
        if ($attributes != null)
        {
            if (!is_array($attributes))
                throw new ArgumentException('Атрибуты должны быть массивом');

            $condArray = array();
            foreach ($attributes as $field => $value)
            {
                // Проверяем, есть ли поле $field в таблице
                if ($this -> useStructure && in_array($field, $fieldsNames) || !$this -> useStructure)
                {
                    if (is_array($value))
						$condArray[] = "`$field` in (".implode(", ", array_map('DB::Quote', $value)).")";
					elseif ($value === 'NULL')
						$condArray[] = "`$field` IS NULL";
					else
						$condArray[] = "`$field` = ". DB::Quote($value);
                }
            }

            if (!empty($condArray))
                $conditions = ' WHERE '.implode(' AND ', $condArray);
        }

        $lim = (!empty($limit) && is_numeric($limit)) ? (' LIMIT ' .
                    ((!is_numeric($start)) ? 0 : $start) .
                    ', ' . $limit) : '';

        $order = (empty($this -> SortOrder)) ? '' : ' ORDER BY '.$this -> SortOrder;

        $query = 'SELECT '.$selectFields.' FROM `' . $this -> TableName . '`' . $conditions . $order . $lim;

        return DB::QueryToArray($query);
    }

    /**
     * Возвращает первую найденную запись метода Select по аргументам
     *
     * @param array $attributes
     * @return array
     */
    public function SelectFirst($attributes = null, $fields = null)
    {
        $res = $this -> Select($attributes, 1, null, $fields);
        if (isset($res[0]))
           return $res[0];
        else
           return null;
    }

    /**
     * Обновляет запись в таблице
	 * keyFieldName надо передавать, если не используется структура
     *
     * @param array $attributes
     * @param int $id
     * @param string $keyFieldName
     */
    public function Update($attributes, $id, $keyFieldName = null)
    {
        // Проверяем правильность введенных аргументов
        if (!is_array($attributes))
            throw new ArgumentException('Атрибуты должны быть массивом');
        if (!is_numeric($id))
            throw new ArgumentException('ID должен быть числом');

		if ($this -> useStructure)
		{
			$keyFieldName = $this -> TableStructure -> PrimaryKey -> Name;
			$fieldsNames = $this -> TableStructure -> GetFieldsNames();
		}

        $forUpdateArray = array();
        foreach ($attributes as $field => $value)
        {
            // Проверяем, есть ли поле $field в таблице
            if (!$this -> useStructure || ($this -> useStructure && in_array($field, $fieldsNames)))
            {
                if ($value == NULL)
                    $forUpdateArray[] = "`$field` = NULL";
                else
                    $forUpdateArray[] = "`$field` = " . DB::Quote($value);
            }
        }

        $forUpdate = implode(', ', $forUpdateArray);

		if (empty($keyFieldName))
			throw new Exception('Не известно название поля первичного ключа');
        // Составялем запрос на обновление в базе
        $query = 'UPDATE `'.$this -> TableName."` SET $forUpdate
                  WHERE `".$keyFieldName.'` = '.$id;

        return DB::AffectedRows(DB::Query($query));
    }

    /**
     * Обновляет записи в таблице
     *
     * @param array $attributes
     * @param array $condition
     */
    public function UpdateRows($attributes,
    /**
     * [['key'],['value']]
     */
    $condition)
    {
        // Проверяем правильность введенных аргументов
        if (!is_array($attributes))
            throw new ArgumentException('Атрибуты должны быть массивом');
        if (!is_array($condition))
            throw new ArgumentException('Conditions должны быть массивом');

        if ($this -> useStructure)
        	$fieldsNames = $this -> TableStructure -> GetFieldsNames();

        $forUpdateArray = array();
        foreach ($attributes as $field => $value)
        {
            // Проверяем, есть ли поле $field в таблице
            if ($this -> useStructure && in_array($field, $fieldsNames) || !$this -> useStructure)
                $forUpdateArray[] = "`$field` = " . DB::Quote($value);
        }

        $forUpdate = implode(', ', $forUpdateArray);

        // Составялем запрос на обновление в базе
        $query = 'UPDATE `'.$this -> TableName."` SET $forUpdate
                  WHERE `$condition[key]` = " . DB::Quote($condition['value']);

        return DB::AffectedRows(DB::Query($query));
    }

    /**
     * Добавляет запись в таблицу
     *
     * @param array $attributes
     * @return int
     */
    public function Insert($attributes)
    {
        // Проверяем правильность введенных аргументов
        if (!is_array($attributes))
            throw new ArgumentException('Атрибуты должны быть массивом');

        // Получаем список всех полей таблицы
        if ($this -> useStructure)
        	$fieldsNames = $this -> TableStructure -> GetFieldsNames();

        $addFieldsArray = array();
        $addValuesArray = array();
        foreach ($attributes as $field => $value)
        {
            if (empty($value) && !is_numeric($value)) // 0 тоже расценивается как empty
                continue;
            // Проверяем, есть ли поле $field в таблице
            if ($this -> useStructure && in_array($field, $fieldsNames) || !$this -> useStructure)
            {
                $addFieldsArray[] = "`$field`";
                $addValuesArray[] = DB::Quote($value);
            }
        }

        $addFields = implode(', ', $addFieldsArray);
        $addValues = implode(', ', $addValuesArray);

        // Составляем запрос
        $query = 'INSERT INTO `'.$this -> TableName."` ( $addFields ) VALUES ( $addValues )";

        $statement = DB::Query($query);

        // Возвращаем id добавленной записи
        $res = DB::$PDO -> lastInsertId();
        if ($res == 0) $res = DB::AffectedRows($statement);
        return $res;
    }

    /**
     * Добавляет несколько строк в таблицу
     *
     * @param array $rows
     */
    public function InsertRows($rows)
    {
        // Проверяем правильность введенных аргументов
        if (!is_array($rows))
            throw new ArgumentException('Строки должны быть массивом');

        // Получаем список всех полей таблицы
        if ($this -> useStructure)
        	$fieldsNames = $this -> TableStructure -> GetFieldsNames();

        $first = true;

        $addFieldsArray = array();
        $addMultiValuesArray = array();
        foreach ($rows as $attributes)
        {
            $addValuesArray = array();
            foreach ($attributes as $field => $value)
            {
                // Проверяем, есть ли поле $field в таблице
                if ($this -> useStructure && in_array($field, $fieldsNames) || !$this -> useStructure)
                {
                    if ($first)
                        $addFieldsArray[] = "`$field`";
                    $addValuesArray[] = DB::Quote($value);
                }
            }
            $first = false;
            $addMultiValuesArray[] = '( '.implode(', ', $addValuesArray).' )';
        }

        $addFields = implode(', ', $addFieldsArray);
        $addValues = implode(', ', $addMultiValuesArray);

        // Составляем запрос
        $query = 'INSERT INTO `'.$this -> TableName."` ( $addFields ) VALUES $addValues";

        DB::Query($query);
    }

    /**
     * Функция удаления записи из таблицы.
     * Возвращает количество удалённых записей
     *
     * @param array $attributes
     */
    public function Delete($attributes, $multi = false)
    {
        // Проверяем правильность введенных аргументов
        if (!is_array($attributes))
            throw new ArgumentException('Атрибуты должны быть массивом');

        if (empty($attributes))
            throw new ArgumentException('Массив атрибутов не должен быть пустым');

        // Получаем список всех полей таблицы
        if ($this -> useStructure)
        	$fieldsNames = $this -> TableStructure -> GetFieldsNames();

        $condArray = array();
        foreach ($attributes as $field => $value)
        {
            // Проверяем, есть ли поле $field в таблице
            if ($this -> useStructure && in_array($field, $fieldsNames) || !$this -> useStructure)
            {
                $condArray[] = "`$field` = " . DB::Quote($value);
            }
        }

        if (!empty($condArray))
            $conditions = ' WHERE '.implode(' AND ', $condArray);
        else
            throw new ArgumentException('Массив атрибутов не содержит ни одного поля, существующего в таблице');

        $lim = $multi ? '' : ' LIMIT 1';

        // Составляем запрос на удаление
        $query = 'DELETE FROM `' . $this -> TableName . '` '.$conditions.$lim;

        return DB::AffectedRows(DB::Query($query));
    }

    /**
     * Подсчитывает количество записей в таблице,
     * удовлетворяющих заданным в массиве $attributes условиям
     *
     * @param array $attributes
     * @return int
     */
    public function Count($attributes = null)
    {
        if (empty($attributes))
        {
            $conditions = '';
        }
        else
        {
            if (!is_array($attributes))
                throw new ArgumentException('Атрибуты должны быть массивом');

            // Получаем список всех полей таблицы

            if ($this -> useStructure)
            	$fieldsNames = $this -> TableStructure -> GetFieldsNames();

            $condArray = array();
            foreach ($attributes as $field => $value)
            {
                // Проверяем, есть ли поле $field в таблице
                if ($this -> useStructure && in_array($field, $fieldsNames) || !$this -> useStructure)
                {
                    $condArray[] = "`$field` = " . DB::Quote($value);
                }
            }

            if (!empty($condArray))
                $conditions = ' WHERE '.implode(' AND ', $condArray);
        }

        if ($this -> useStructure)
        	$keyFieldName = $this -> TableStructure -> PrimaryKey -> Name;
        else
        	$keyFieldName = '*';

        $query = "SELECT count($keyFieldName) as cnt FROM ". $this -> TableName . $conditions;

        return DB::QueryOneValue($query);
    }

    /**
     * Получение имени таблицы
     *
     * @return string
     */
    public function GetTableName()
    {
        return $this -> TableName;
    }
}
?>
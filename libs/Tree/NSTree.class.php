<?php

/*
CREATE TABLE `tree` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`left_key` INT(10) NOT NULL,
	`right_key` INT(10) NOT NULL,
	`level` INT(10) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `key` (`left_key`, `right_key`, `level`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
*/

class NSTree
{
	protected $table = NULL;
    protected $idField = null;

    public function __construct($tablename,$idFieldName)
	{
		$this->table = $tablename;
        $this->idField = $idFieldName;
	}

	// Модифицирует ключи
	protected function modifyNodes($key, $delta)
	{
		$query =
			'UPDATE `'.$this->table.'`
			SET right_key = right_key + '.(int)$delta.'
			WHERE right_key >= '.(int)$key;
//		FB::log($query);
		DB::Query($query);

		$query =
			'UPDATE `'.$this->table.'`
			SET left_key = left_key + '.(int)$delta.'
			WHERE left_key >= '.(int)$key;
//		FB::log($query);
		DB::Query($query);
	}

	public function getNode($id)
	{

//            FB::error($this->idField);
            $query = "SELECT * FROM `".$this->table."` WHERE ".$this->idField."=".$id;
            //FB::log($query);
            $result = DB::QueryToArray($query);
            if (count($result) == 0)
                    throw new Exception('Node id='.$id.' does not exist.');
            $result = $result[0];
            return $result;
	}

//	public function clear($extrafields=array())
//	{
//		if (!is_array($extrafields))
//			throw new Exception('$extrafields must be array.');
//
//		DB::query(NULL, 'TRUNCATE '.Database::instance()->quote_table($this->table))->execute();
//
//		DB::delete($this->table)->execute();
//
//		$data = array(
//			'id' => 1,
//			'title' => 'главная',
//			'alias' => 'index',
//			'left_key' => 1,
//			'right_key' => 2,
//			'level' => 0
//		);
//		$data = $data + $extrafields;
//		list($insert_id, $tmp) = DB::insert($this->table, array_keys($data))->values(array_values($data))->execute();
//
//		return $insert_id;
//	}
        public function getTree($id=NULL, $fields = null, $rootId, $pidFld, $childrenFld = 'children')
        {
            $lineTree = $this->getLineTree($id,$fields);
            //FB::log($lineTree,"line");
            $tree = new Tree();
            return $tree->Table2Tree($lineTree, $rootId, $this->idField, $pidFld, $childrenFld);
        }

        /**
         * Возвращает массив дерева от заданного узла включительно
         * @param int $id
         * @return array
         */
	public function getLineTree($id=NULL, $fields = null)
	{
            $fields = ($fields && is_array($fields))?implode(",", $fields):$fields?$fields:"*";
		$id = (int) $id;
		$QTblName = "`".$this->table."`";

		if ($id != 0)
		{
			$node = $this->getNode($id);

			$query =
				'SELECT '.$fields.'
				FROM '.$QTblName.'
				WHERE left_key >= '.(int)$node['left_key'].' AND right_key <= '.(int)$node['right_key'].'
				ORDER BY left_key';
		}
		else
		{
			$query =
				'SELECT '.$fields.'
				FROM '.$QTblName.'
				ORDER BY left_key';
		}
//                trace($query);
		$result = DB::QueryToArray($query);
                if ($fields && !is_array($fields) && $fields != "*")
                {
                    $res = array();
                    foreach ($result as $key => $val)
                        $res[] = $val[$fields];
                    $result = $res;
                }
		return $result;
	}

        /**
         * Возвращает массив всей ветки дерева, в которой находится узел
         * @param int $id
         * @return array
         */
	public function getLinePath($id)
	{
		$node = $this->getNode($id);

		$query =
			'SELECT *
			FROM `'.$this->table.'`
			WHERE right_key > '.(int)$node['left_key'].' AND left_key < '.(int)$node['right_key'].'
			ORDER BY left_key';

		$result = DB::QueryToArray($query);

		return $result;
	}

        /**
         * Возвращает массив всей ветки дерева от корня до заданного узла
         * @param int $id
         * @return array
         */
        public function getLineRootPath($id)
	{
		$node = $this->getNode($id);

		$query =
			'SELECT *
			FROM `'.$this->table.'`
			WHERE right_key >= '.(int)$node['right_key'].' AND left_key <= '.(int)$node['left_key'].'
			ORDER BY left_key';

		$result = DB::QueryToArray($query);

		return $result;
	}

	public function insert($id, $extrafields=array())
	{
//                FB::error($extrafields,"EXTRA");
		if (!is_array($extrafields))
                {
                    throw new Exception('$extrafields must be array.');

                }
		// Находим родителя
		$parent = $this->getNode($id);
                //FB::log($parent,"parent");
		DB::StartTransaction();

		// Обновляем ключи существующего дерева, узлы стоящие за родительским узлом
		$this->modifyNodes((int)$parent['right_key'], 2);

		// Добавляем новый узел
		$node = array(
//			$this->idField => NULL,
			'left_key' => $parent['right_key'],
			'right_key' => $parent['right_key'] + 1,
			'level' => $parent['level'] + 1
		);

		$node += $extrafields;
//                FB::error($extrafields,"NODE");


		$addFieldsArray = array();
                $addValuesArray = array();
                foreach ($node as $field => $value)
                {
                    if (empty($value) && !is_numeric($value)) // 0 тоже расценивается как empty
                        continue;

                    $addFieldsArray[] = "`$field`";
                    $addValuesArray[] = DB::Quote($value);
                }
                //FB::log($addFieldsArray,"FA");
                //FB::log($addValuesArray,"VA");

                $addFields = implode(', ', $addFieldsArray);
                $addValues = implode(', ', $addValuesArray);

                //FB::log($addFields,"F");
                //FB::log($addValues,"V");

//                exit;

                $query = "INSERT INTO `".$this->table."` (".$addFields.") VALUES (".$addValues.")";
		//                FB::error($query);
		$statement = DB::Query($query);

                // Возвращаем id добавленной записи
                $res = DB::LastInsertID();
                if ($res == 0)
                {
                    $res = DB::AffectedRows($statement);
                    DB::RollBackTransaction();
                }

		DB::CommitTransaction();

                return $res;
	}

	public function delete($id)
	{
		$node = $this->getNode($id);

		DB::StartTransaction();

		// Удаляем узел (вместе с веткой)
		$query =
			'DELETE FROM `'.$this->table.'`
			WHERE left_key >= '.(int)$node['left_key'].' AND right_key <= '.(int)$node['right_key'];
//		FB::log($query);
		DB::Query($query);

		$width = $node['right_key'] - $node['left_key'] + 1;
		// Обновление последующих узлов
		$this->modifyNodes((int)$node['right_key'], -$width);

		DB::CommitTransaction();
	}

	// Метод перемещает ноду $id в родительскую ноду $parentId
	public function move($id, $parentId)
	{
        /* Неоптимизированный алгоритм перемещения ветки по дереву
         * Работает при переносе как вверх, так и вниз по дереву
         * Вся обработка - 3 основных запроса на изменение:
         * 1. Уменьшаются ключи всех нод правее и выше текущей (ветка "отрезается")
         *     Для правильного переноса вниз по дереву ключи родительской ноды беруться после этого шага
         * 2. Увеличиваются ключи всех нод правее и выше нового родителя (исключая ноды нашей перемещаемой ветки) (подготавливаем место)
         * 3. Соответствующе меняются ключи нод перемещаемой ветки
         */

		// получаем ноду перемещаемого узла
		$node = $this->getNode($id);
        if (($node['categoryParentID'] == $parentId) || ($node[$this->idField]) == $parentId)
            return; // родитель не поменялся или попытка перемещения в себя
		$level = $node['level'];
		$lk = $node['left_key'];
		$rk = $node['right_key'];

		// Определяем смещение ключей дерева
		$key_mod = $rk - $lk + 1;

		//Выбираем все узлы перемещаемой ветки
		$id_edit = implode(', ', DB::QueryToArray("SELECT {$this->idField} FROM {$this->table} WHERE left_key >= {$lk} AND right_key <= {$rk}",null, $this->idField));
		try{
			DB::StartTransaction();
            // Обновляем ключи у нод правее нашей
            $query = "
                UPDATE `{$this->table}`
                SET
                    `right_key` = `right_key` - {$key_mod},
                    `left_key` = IF(`left_key` > {$lk}, `left_key` - {$key_mod}, `left_key`)
                WHERE `right_key` > $rk
            ";
            DB::Query($query);

            // получаем ноду нового родителя
            $parent_node = $this->getNode($parentId);
            // Определяем смещение уровня изменяемого узла;
            $plevel = $parent_node['level'];
            $level_mod = $plevel - $level + 1;
            $prk = $parent_node['right_key'];
            $plk = $parent_node['left_key'];
            $tree_mod = $prk - $lk;

            // Обновляем ключи у нод правее родительской
            $query = "
                UPDATE `{$this->table}`
                SET
                    `right_key` = `right_key` + {$key_mod},
                    `left_key` = IF(`left_key` > $plk, `left_key` + {$key_mod}, `left_key`)
                WHERE `right_key` >= {$prk} AND `{$this->idField}` NOT IN ({$id_edit});
            ";
            DB::Query($query);

            // обновляем ключи у нашей ветки
            $query = "
                UPDATE Store_Categories
                SET
                    `left_key` = `left_key` + {$tree_mod},
                    `right_key` = `right_key` + {$tree_mod},
                    `level` = `level` + {$level_mod}
                WHERE `{$this->idField}` IN ({$id_edit})
            ";
            DB::Query($query);
		}
		catch (Exception $e){
			DB::RollBackTransaction();
		}
        DB::CommitTransaction();
	}

	/**
	 * Change items position.
	 *
	 * @param integer $nodeId1 first item id
	 * @param integer $nodeId2 second item id
	 * @return bool true if successful, false otherwise.
	 */
	public function ChangePosition($nodeId1, $nodeId2)
	{
		$node_info = $this->getNode($nodeId1);

		$left_id1	 = $node_info['left_key'];
		$right_id1	 = $node_info['right_key'];
		$level1		 = $node_info['level'];

		$node_info = $this->getNode($nodeId2);

		$left_id2	 = $node_info['left_key'];
		$right_id2	 = $node_info['right_key'];
		$level2		 = $node_info['level'];

		$sql1 = 'UPDATE ' . $this->table . ' SET ';
		$sql1 .= 'left_key = ' . $left_id2 . ', right_key = ' . $right_id2 . ', level = ' . $level2 . ' ';
		$sql1 .= 'WHERE ' . $this->idField . ' = ' . $nodeId1;

		$sql2 = 'UPDATE ' . $this->table . ' SET ';
		$sql2 .= 'left_key = ' . $left_id1 . ', right_key = ' . $right_id1 . ', level = ' . $level1 . ' ';
		$sql2 .= 'WHERE ' . $this->idField . ' = ' . $nodeId2;
		try
		{
			DB::StartTransaction();
			DB::Query($sql1);
			DB::Query($sql2);
		}
		catch(Exception $e)
		{
			DB::RollBackTransaction();
		}
		DB::CommitTransaction();
	}

	/**
	 * Swapping nodes within the same level and limits of one parent with all
	 * its children: $nodeId1 placed "before" or "after" $nodeId2.
	 *
	 * @param int $nodeId1 first item id
	 * @param int $nodeId2 second item id
	 * @param string $position 'before' or 'after' (default) $nodeId2
	 * @param array $condition array key - condition (AND, OR, etc), value - condition string
	 * @return bool true if successful, false otherwise.
	 */
	public function ChangePositionAll($nodeId1, $nodeId2)
	{
		$node_info = $this->getNode($nodeId1);

		$left_id1	 = $node_info['left_key'];
		$right_id1	 = $node_info['right_key'];
		$level1		 = $node_info['level'];
		$parent_id1	 = $node_info['categoryParentID'];

		$node_info2	 = $this->getNode($nodeId2);
		$left_id2	 = $node_info2['left_key'];
		$right_id2	 = $node_info2['right_key'];
		$level2		 = $node_info2['level'];
		$parent_id2	 = $node_info2['categoryParentID'];

		if($level1 != $level2)
		{
			throw new Exception("Invalid level");
		}

		if($parent_id1 != $parent_id2)
		{
			throw new Exception("Can't move this...");
		}

		if($left_id1 > $left_id2)
		{
			$position = 'before';
		}
		else
		{
			$position = 'after';
		}

		if(($left_id1 == ($left_id2 + 2) && $right_id1 == ($right_id2 + 2)) || ($left_id1 == ($left_id2 - 2) && $right_id1 == ($right_id2 - 2)))
		{
			return $this->ChangePosition($nodeId1, $nodeId2);
		}


		$sql = 'UPDATE ' . $this->table . ' SET ';
		if('before' == $position)
		{
			if($left_id1 > $left_id2)
			{
				$sql .= 'right_key = CASE WHEN left_key  BETWEEN ' . $left_id1 . ' AND ' . $right_id1 . ' THEN right_key - ' . ($left_id1 - $left_id2) . ' ';
				$sql .= 'WHEN left_key BETWEEN ' . $left_id2 . ' AND ' . ($left_id1 - 1) . ' THEN right_key +  ' . ($right_id1 - $left_id1 + 1) . ' ELSE right_key END, ';
				$sql .= 'left_key = CASE WHEN left_key BETWEEN ' . $left_id1 . ' AND ' . $right_id1 . ' THEN left_key - ' . ($left_id1 - $left_id2) . ' ';
				$sql .= 'WHEN left_key BETWEEN ' . $left_id2 . ' AND ' . ($left_id1 - 1) . ' THEN left_key + ' . ($right_id1 - $left_id1 + 1) . ' ELSE left_key END ';
				$sql .= 'WHERE left_key BETWEEN ' . $left_id2 . ' AND ' . $right_id1;
			}
			else
			{
				$sql .= 'right_key = CASE WHEN left_key BETWEEN ' . $left_id1 . ' AND ' . $right_id1 . ' THEN right_key + ' . (($left_id2 - $left_id1) - ($right_id1 - $left_id1 + 1)) . ' ';
				$sql .= 'WHEN left_key BETWEEN ' . ($right_id1 + 1) . ' AND ' . ($left_id2 - 1) . ' THEN right_key - ' . (($right_id1 - $left_id1 + 1)) . ' ELSE right_key END, ';
				$sql .= 'left_key' . ' = CASE WHEN ' . 'left_key' . ' BETWEEN ' . $left_id1 . ' AND ' . $right_id1 . ' THEN ' . 'left_key' . ' + ' . (($left_id2 - $left_id1) - ($right_id1 - $left_id1 + 1)) . ' ';
				$sql .= 'WHEN left_key BETWEEN ' . ($right_id1 + 1) . ' AND ' . ($left_id2 - 1) . ' THEN left_key - ' . ($right_id1 - $left_id1 + 1) . ' ELSE left_key END ';
				$sql .= 'WHERE left_key BETWEEN ' . $left_id1 . ' AND ' . ($left_id2 - 1);
			}
		}

		if('after' == $position)
		{
			if($left_id1 > $left_id2)
			{
				$sql .= 'right_key = CASE WHEN left_key BETWEEN ' . $left_id1 . ' AND ' . $right_id1 . ' THEN right_key - ' . ($left_id1 - $left_id2 - ($right_id2 - $left_id2 + 1)) . ' ';
				$sql .= 'WHEN left_key BETWEEN ' . ($right_id2 + 1) . ' AND ' . ($left_id1 - 1) . ' THEN right_key +  ' . ($right_id1 - $left_id1 + 1) . ' ELSE right_key END, ';
				$sql .= 'left_key = CASE WHEN left_key BETWEEN ' . $left_id1 . ' AND ' . $right_id1 . ' THEN left_key - ' . ($left_id1 - $left_id2 - ($right_id2 - $left_id2 + 1)) . ' ';
				$sql .= 'WHEN left_key BETWEEN ' . ($right_id2 + 1) . ' AND ' . ($left_id1 - 1) . ' THEN left_key + ' . ($right_id1 - $left_id1 + 1) . ' ELSE left_key END ';
				$sql .= 'WHERE left_key BETWEEN ' . ($right_id2 + 1) . ' AND ' . $right_id1;
			}
			else
			{
				$sql .= 'right_key = CASE WHEN left_key BETWEEN ' . $left_id1 . ' AND ' . $right_id1 . ' THEN right_key + ' . ($right_id2 - $right_id1) . ' ';
				$sql .= 'WHEN left_key BETWEEN ' . ($right_id1 + 1) . ' AND ' . $right_id2 . ' THEN right_key - ' . (($right_id1 - $left_id1 + 1)) . ' ELSE right_key END, ';
				$sql .= 'left_key = CASE WHEN left_key BETWEEN ' . $left_id1 . ' AND ' . $right_id1 . ' THEN left_key + ' . ($right_id2 - $right_id1) . ' ';
				$sql .= 'WHEN left_key BETWEEN ' . ($right_id1 + 1) . ' AND ' . $right_id2 . ' THEN left_key - ' . ($right_id1 - $left_id1 + 1) . ' ELSE left_key END ';
				$sql .= 'WHERE left_key BETWEEN ' . $left_id1 . ' AND ' . $right_id2;
			}
		}
		try
		{
			DB::StartTransaction();

			DB::Query($sql);
		}
		catch(Exception $e)
		{
			DB::RollBackTransaction();
		}
		DB::CommitTransaction();

	}

	public function check($thorough=FALSE)
	{
		$QTblName = "`".$this->table."`";

		// Тест 1
		$query =
			'SELECT `' . $this->idField . '`
			FROM '.$QTblName.'
			WHERE MOD(right_key - left_key, 2) = 0';
		$result = DB::QueryToArray($query);

		if (count($result) != 0)
			throw new Exception('Test 1 integrity check failed.');

		// Тест 2
		$query =
			'SELECT `' . $this->idField . '`
			FROM '.$QTblName.'
			WHERE MOD(left_key - level + 2, 2) = 1';
		$result = DB::QueryToArray($query);

		if (count($result) != 0)
			throw new Exception('Test 2 integrity check failed.');

		// Тест 3
		if ($thorough)
		{
			$query =
				'SELECT t1.`' . $this->idField . '`, COUNT(t1.`' . $this->idField . '`) AS rep, MAX(t3.right_key) AS max_right
				FROM
					'.$QTblName.' AS t1,
					'.$QTblName.' AS t2,
					'.$QTblName.' AS t3
				WHERE
					t1.left_key <> t2.left_key
				  AND
					t1.left_key <> t2.right_key
				  AND
					t1.right_key <> t2.left_key
				  AND
					t1.right_key <> t2.right_key
				GROUP BY
					t1.`' . $this->idField . '`
				HAVING
					max_right <> SQRT( 4 * rep + 1 ) + 1';

			$result = DB::QueryToArray($query);

			if (count($result) != 0)
				throw new Exception('Test 3 integrity check failed.');
		}

		// Тест 4, проверка level
//		$query =
//			'SELECT node.`' . $this->idField . '` as id, node.level as level
//			FROM
//				'.$QTblName.' AS node,
//				'.$QTblName.' AS parent
//			WHERE node.left_key BETWEEN parent.left_key AND parent.right_key
//			GROUP BY node.`' . $this->idField . '`
//			HAVING COUNT(parent.`' . $this->idField . '`) - 1 != level
//			ORDER BY node.left_key';
//		$result = DB::QueryToArray($query);
//
//		if (count($result) != 0)
//			throw new Exception('Test 4 integrity check failed.');
	}
}

<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/dictionary.php';

/**
 * 
 * 
 * Интернационализация, мультязычность, словарь -- поддержка многих языков
 */
class i18n
{
    /**
     * Язык, на версии фраз для которого нужно получать
     * Данный параметр можно установить (поменять), например в  class BaseFrontController
     * 
     * @var string 
     */
    public static $language = 'ru'; // или, например, 'en' (можно изменить снаружи)
    
    /**
     *  Сообщение об отсутствии сущности в файле перевода
     * 
     * @var string 
     */
    public static $additionalInfoIfNotFound = ' -- not in translation file';
    
    /**
     * Содержимое файла словаря (массив)
     * 
     * ВНИМАНИЕ: файл словаря должен подлючаться снаружи вызовом типа:
     *    i18n::$dict = array(
            'industry' => array(
                'ru' => 'Отрасли',
                'en' => 'Industry',
            ),
            'active_is' => array(
                'ru' => 'Active это',
                'en' => 'About Active',
            ),
        );
     *  
     *  (например см. /dictionary)
     * 
     * 
     * @var string 
     */
    public static $dict = array();

    
    /**
     * @param array $params           возможные (необязательные) параметры
     * @param array $params['i'] - international -- текст подлежащий переводу по словарю
     * @param array $params['t'] -  text         -- просто текст для вывода (полезно использовать в паре с ['d'])
     * @param array $params['d'] -  default      -- значение выводимое если параметр ['t'], а значение ['d'] присутствует
     * @return string
     */
    public static function t($params)
    {
        $defaultText = 'SoMe-TeXt-HeRe';
        $result = '';
        
        

        if(!empty($params['i'])) { // Cначала работаем с переводом
            if (!empty(self::$dict[$params['i']])) { // если такая штука есть в файле перевода
                $result = self::$dict[$params['i']][self::$language];
            } else { // если такой сущности нет
                $result = $params['i'] . self::$additionalInfoIfNotFound; 
            }
        } else if (!empty($params['t'])) { // если просто текст
            $result = $params['t'];   
        }
        
        // если значение по прежнему пусто
        if (empty($result) && !empty($params['d'])) {  // посмотрим -- есть ли значение по-умолчанию
            $result = $params['d'];
        } else if (empty($result)) {
            $result = $defaultText;
        }

        return $result;
    }
    
    /**
     * Регистрируем новую функцию для смарти (поддержка мультиязычности)
     * 
     * @param SmartyObject $smarty
     */
    public static function registerSmartyMultyLangFuncion(&$smarty)
    {
        $smarty->register_function("t", "i18n::t"); // фуккция вывода мультиязычного текста
    }
    
}
    
<?php

require_once('IControlsRegistryProvider.class.php');

/**
 * Провайдер реестра контролов (реестра полей)
 * хранящегося в базе данных.
 * Обычно в таблице greeny_fieldsRegistry
 * 
 * @version 1.4
 *
 */
class DBControlsRegistryProvider implements IControlsRegistryProvider 
{
    /**
     * Менеджер таблицы реестра полей (уже контролов)
     *
     * @var DBTableManager
     */
    private $fieldsRegistryTableManager;
 
    /**
     * Структура редактируемой таблицы
     *
     * @var DBTableStructure
     */
    public $activeTableStructure;

    public function __construct($fieldsRegistryTableName = null)
    {
    	if ($fieldsRegistryTableName == null)
		{
			$fieldsRegistryTableName = !isset(TablesNames::$FIELDS_REGISRTY_TABLE_NAME)
                                        ? 'greeny_fieldsRegistry'
                                        : TablesNames::$FIELDS_REGISRTY_TABLE_NAME;
		}
        $this -> fieldsRegistryTableManager = new DBTableManager($fieldsRegistryTableName);
		$this -> fieldsRegistryTableManager -> SortOrder = ' "entryID"';
    }

    /**
     * Получение объекта параметров всех контролов указанной формы
     *
     * @param string $formName
     */
    public function GetControlsParams($formName)
    {
    	return $this -> GetParams($formName);
    }
    
    /**
     * Получение объекта параметров заданного контрола указанной формы
     *
     * @param string $formName
     * @param string $controlName
     */
    public function GetControlParams($formName, $controlName)
    {
        $res = $this -> GetParams($formName, $controlName);
        return empty($res) ? $res : $res[0];
    }

    private function GetParams($formName, $controlName = null)
    {
        $args = array('tableName' => $formName);
        if (!empty($controlName))
            $args['fieldName'] = $controlName;

        $controls = $this -> fieldsRegistryTableManager -> Select($args);

        $controlsCnt = count($controls);
        if ($controlsCnt == 0) return null;

        $this -> activeTableStructure = new DBTableStructure($formName);

		$fields = &$this -> activeTableStructure -> Fields;

		if (defined('ML_CONTENTTABLE_POSTFIX'))
		{
    		$query = 'SHOW TABLES LIKE "'. $this -> activeTableStructure -> TableName . ML_CONTENTTABLE_POSTFIX .'"';
    		$qRes = DB::QueryToArray($query);
    		if (sizeof($qRes) == 1)
    		{
    			$contentTableStructure = new DBTableStructure($this -> activeTableStructure -> TableName . ML_CONTENTTABLE_POSTFIX);
    			$fields = array_merge($fields, $contentTableStructure -> Fields);
    		}
		}

		$fieldsAsoc = array();
		$cnt = sizeof($fields);
		for ($i = 0; $i < $cnt; $i++)
			$fieldsAsoc[$fields[$i] -> Name] = &$fields[$i];

        for ($i = 0; $i < $controlsCnt; $i++)
        {
            $controls[$i]['name'] = $controls[$i]['fieldName'];

            // Если поле не виртуальное и есть такое в редактируемой таблице
            if (isset($fieldsAsoc[$controls[$i]['name']]) && !empty($controls[$i]['description']))
            {
        		if ($fieldsAsoc[$controls[$i]['name']] -> IsNotNull)
        		{
        		    $controls[$i]['required'] = true;
        		    
        		    $star = '<span style="color: red;">*</span>';
        			if ($controls[$i]['description']{strlen($controls[$i]['description'])-1} == ':')
        				$controls[$i]['description'] = str_replace(':', $star.':', $controls[$i]['description']);
        			else 
        				$controls[$i]['description'] .= $star;
        		}
            }

            if (isset($controls[$i]['controlSettings']) && $controls[$i]['controlSettings'] != '')
		    {
    			$controls[$i]['controlSettings'] = JSON::Decode($controls[$i]['controlSettings'], true);

    			if (isset($controls[$i]['controlSettings']['className']))
    				$controls[$i]['CSSClass'] = $controls[$i]['controlSettings']['className'];

                if (isset($controls[$i]['controlSettings']['blName'])) 	  
                    $controls[$i]['blockName'] = $controls[$i]['controlSettings']['blName'];

        		if (isset($controls[$i]['controlSettings']['blOrder']))
                    $controls[$i]['blockOrder'] = $controls[$i]['controlSettings']['blOrder'];

                if (isset($controls[$i]['controlSettings']['elemOrder']))
                    $controls[$i]['controlOrder'] = $controls[$i]['controlSettings']['elemOrder'];
                if (isset($controls[$i]['controlSettings']['controlOrder']))
                    $controls[$i]['controlOrder'] = $controls[$i]['controlSettings']['controlOrder'];
		    }
		    
		    if (isset($fieldsAsoc[$controls[$i]['name']]) && !empty($fieldsAsoc[$controls[$i]['name']] -> Length))
		       $controls[$i]['controlSettings']['length'] = $fieldsAsoc[$controls[$i]['name']] -> Length;

    		if (!$controls[$i]['visible'])
    		{
    		    if (empty($controls[$i]['CSSClass']))
                    $controls[$i]['CSSClass'] = 'invisible';
                else 
                    $controls[$i]['CSSClass'] .= ' invisible';
    		}
			
			if ($controls[$i]['canChange'] == 0)
    		{
                $controls[$i]['disabled'] = true;
    		}
    		
    		if (!empty($controls[$i]['tip']) && $controls[$i]['tip'] == 'NULL')
    		  $controls[$i]['tip'] = '';
        }

        return $controls;
    }
	
	/**
	* Получение объекта параметров по уникальному ID контрола
	*
	* @param int $ID
	*/
	public function GetControlParamsByID($ID)
	{
		$control = $this 
						-> fieldsRegistryTableManager 
						-> SelectFirst
						(
							array($this 
									-> fieldsRegistryTableManager 
									-> TableStructure 
									-> PrimaryKey 
									-> Name => $ID)
						);
		
		if (empty($control)) return null;

        //$this -> activeTableStructure = new DBTableStructure($formName);

		// То что идёт ниже скорее всего не понадобится, поэтому мы это пока закомментим
		//$fields = &$this -> activeTableStructure -> Fields;

		//if (defined('ML_CONTENTTABLE_POSTFIX'))
		////{
    	//	$query = 'SHOW TABLES LIKE "'. $this -> activeTableStructure -> TableName . ML_CONTENTTABLE_POSTFIX .'"';
    	//	$qRes = DB::QueryToArray($query);
    	//	if (sizeof($qRes) == 1)
    	//	{
    	//		$contentTableStructure = new DBTableStructure($this -> activeTableStructure -> TableName . ML_CONTENTTABLE_POSTFIX);
    	//		$fields = array_merge($fields, $contentTableStructure -> Fields);
    	//	}
		//}

		//$fieldsAsoc = array();
		//$cnt = sizeof($fields);
		//for ($i = 0; $i < $cnt; $i++)
		//	$fieldsAsoc[$fields[$i] -> Name] = &$fields[$i];

		$control['name'] = $control['fieldName'];

            // Если поле не виртуальное и есть такое в редактируемой таблице
            //if (isset($fieldsAsoc[$controls[$i]['name']]) && !empty($controls[$i]['description']))
           // {
        	//	if ($fieldsAsoc[$controls[$i]['name']] -> IsNotNull)
        	//	{
        	//	    $controls[$i]['required'] = true;
        	//	    
        	////	    $star = '<span style="color: red;">*</span>';
        	//		if ($controls[$i]['description']{strlen($controls[$i]['description'])-1} == ':')
        	//			$controls[$i]['description'] = str_replace(':', $star.':', $controls[$i]['description']);
        	//		else 
        	//			$controls[$i]['description'] .= $star;
        	//	}
            //}

            if (isset($control['controlSettings']) && $control['controlSettings'] != '')
		    {
    			$control['controlSettings'] = JSON::Decode($control['controlSettings'], true);

    			if (isset($control['controlSettings']['className']))
    				$control['CSSClass'] = $control['controlSettings']['className'];

                if (isset($control['controlSettings']['blName'])) 	  
                    $control['blockName'] = $control['controlSettings']['blName'];

        		if (isset($control['controlSettings']['blOrder']))
                    $control['blockOrder'] = $control['controlSettings']['blOrder'];

                if (isset($control['controlSettings']['elemOrder']))
                    $control['controlOrder'] = $control['controlSettings']['elemOrder'];
                if (isset($control['controlSettings']['controlOrder']))
                    $control['controlOrder'] = $control['controlSettings']['controlOrder'];
		    }
		    
		    //if (isset($fieldsAsoc[$control['name']]) && !empty($fieldsAsoc[$control['name']] -> Length))
		    //   $control['controlSettings']['length'] = $fieldsAsoc[$control['name']] -> Length;

    		if (!$control['visible'])
    		{
    		    if (empty($control['CSSClass']))
                    $control['CSSClass'] = 'invisible';
                else 
                    $control['CSSClass'] .= ' invisible';
    		}
    		
    		if (!empty($control['tip']) && $control['tip'] == 'NULL')
    		  $control['tip'] = '';

        return $control;
	}
}
?>
<div class="top-section">
    <div class="container">
        <div class="twocolumns">
            <div class="text-holder">
                <h1>{$PAGE.title}</h1>
                {$CONTENT.pageContent}
            </div>
            <div class="section-right">
                <form id="domainFrom" method="post" class="survey">
                    <h2>Проверить доменное имя:</h2>
                    <div class="row">
                        <div class="area">
                            <input type="text"  name="domain" placeholder="Имя домена">
                        </div>
                        <div class="select-block">
                        </div>
                        <input type="submit" value="Проверить" >
                    </div>
                </form>
                {if !empty($DOMAINS)}
                <p>Стоимость доменов в зонах:</p>
                <div class="threecolumns">
                    
                        {assign var=ind1 value=0}
                        <div class="col">
                        <ul class="price-list">
                            {foreach from=$DOMAINS item=domain}
                                {if $ind1%6 == 0 && $ind1 != 0}
                                      </ul></div> <div class="col"><ul class="price-list">
                                {/if}
                                <li>
                                    <span class="name">.{$domain.zone}</span>
                                    <span class="price"><strong>{$domain.price}</strong> / год</span>
                                </li>
                            {assign var=ind1 value=$ind1+1}
                            {/foreach}                            
                        </ul></div>                                   
                </div>
                {/if}
            </div>
        </div>
    </div>
</div>
{include file="include-area.tpl"}
<?php
$path = $_SERVER['PHP_SELF'];
	$m = explode("/", $path);
	
	$num = count($m) - 2;
	
	if ($num == 0)
	{
		$AppSettings::$APPLICATION_PATH = "./";
	}
	else 
	{
		$AppSettings::$APPLICATION_PATH = str_repeat("../", $num);
	}
setlocale(LC_ALL, 'ru_RU.CP1251');
require_once($AppSettings::$APPLICATION_PATH."includes.php");

if (!isset($_REQUEST['q']) || !isset($_REQUEST['controlsRegistryEntryID'])) die('{"error":"Не заданы необходимые параметры", "errno":4}');
if (!is_numeric($_REQUEST['controlsRegistryEntryID'])) die('{"error":"Wrong params","errno":5}');

$letters = $_REQUEST['q'];
// Узнаем что за поле
$fieldsRegestryTableMngr = new DBTableManager(TablesNames::$FIELDS_REGISRTY_TABLE_NAME);
$entries = $fieldsRegestryTableMngr -> Select(array($fieldsRegestryTableMngr -> TableStructure -> PrimaryKey -> Name => $_REQUEST['controlsRegistryEntryID']));
if (count($entries) == 0) die('{"error":"Field does not registered","errno":6}');
$controlParams = $entries[0];

// Посмотрим заданы ли JSON настройки
if (isset($controlParams['controlSettings']) && $controlParams['controlSettings'] != '')
{
	$Json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
	$Jresult = $Json -> decode($controlParams['controlSettings']);
	if (isset($Jresult['referentTable']))
		$referentTableName = $Jresult['referentTable'];
	else 
		die('{"error":"Referent Table  is undefined. It should be defined in FieldsRegistry","errno":7}');
	
	// Хотя нам не нужна она сейчас
	if (isset($Jresult['linkingTable']))
		$linkingTableName = $Jresult['linkingTable'];
	else 
		die('{"error":"linking Table for  is undefined. It should be defined in FieldsRegistry","errno":8}');
}
else 
{
	die('{"error":"Settings for are undefined. They should be defined in FieldsRegistry. \"referentTable\" setting needed.","errno":8}');
}

// Теперь узнаем какой листинг у ссылочной таблицы и выберем значения
$entries = $fieldsRegestryTableMngr -> Select(array('tableName' => $referentTableName, 'isListingKey' => 1));
if (count($entries) == 0) die('{"error":"ListingKey for Referent table does not registered","errno":8}');

$referentTablStructure = new DBTableStructure($referentTableName);
if (DB::$DBMS == 'PostgreSQL')
	$query = "SELECT \"{$referentTablStructure->PrimaryKey->Name}\" as \"key\",
					 \"{$entries[0]['fieldName']}\" as \"value\" 
					 FROM \"$referentTableName\" WHERE LOWER(\"{$entries[0]['fieldName']}\") LIKE '%$letters%'";
else
	$query = "SELECT `{$referentTablStructure->PrimaryKey->Name}` as `key`,
					 `{$entries[0]['fieldName']}` as `value` 
					 FROM `$referentTableName` WHERE LOWER(`{$entries[0]['fieldName']}`) LIKE '%$letters%'";

$qRes = DB::Query($query);

while ($row = DB::FetchAssoc($qRes))
{
	echo $row["value"]."|$row[key]\n";
}
?>
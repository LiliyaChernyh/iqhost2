<h1>Управление справочником коллекций</h1>

<div style="text-align: right; margin: -30px 0 5px 0;">
    <input type="button" id="goAddTheme" class="goToBtn" value="Добавить коллекцию" />
    <input type="hidden" id="goAddTheme_link" value="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/add/" />
</div>

{include file='paginator.tpl'}
<div class="holder-table">
	<table class="highlightable" id="faqTable">
	    <thead>
	        <tr>
	            <th colspan="2">№</th>
	            <th>Название</th>
				<th style="width: 10px;"><img src="{$ADMIN_IMAGES}icons/delete_icon.png" /></th>
			</tr>
	    </thead>
	    <tbody>
	{if !empty($CATEGORIES)}
	    {foreach from=$CATEGORIES item=THEME key=I}
	        <tr>
	        	<td style="display: none;"><a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/edit/{$THEME.collectionID}/" /></td>
	        	<td class="hov">&nbsp;</td>
	            <td style="width: 20px;">{$I+1}</td>
	            <td>
	                {$THEME.collectionName}
	            </td>
	            <td class="deleteEntry">
	                <a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/del/{$THEME.collectionID}/" title="Удалить"><img src="{$ADMIN_IMAGES}delete_icon.png" alt="Удалить" style="border: 0;" /></a>
	            </td>
	        </tr>
	    {/foreach}
	    </tbody>
	</table>
</div>
{else}
    </tbody>
</table>
</div>
    <div class="formBlock">
    	<div class="error">Записей не найдено</div>
    </div>
{/if}

<?php

/**
 * Класс для работы с обратной связью
 *
 * @version  1.0
 */
class Feedback extends CommonModule
{
	public function __construct()
	{
		parent::__construct(__CLASS__);
                
		$this -> Inits();
	}
	
	/**
	 * Менеджер таблицы сообщений
	 *
	 * @var DBTableManager
	 */
	private $messagesTable = null;
	
	public static $messagesTableName = 'greeny_fb_messages';
	
	private function Inits()
	{		
		if ($this -> messagesTable == null)
		{
			$this -> messagesTable = new DBTableManager(self::$messagesTableName);
			$this -> messagesTable -> SortOrder = '`sendDate` DESC';
		}
	}
	
	/**
	 * Добавляет новое сообщение в таблицу.
	 * Перед добавлением проверяется корректность всех параметров.
	 * Возвращает ID новой записи.
	 *
	 * @param array $attributes
	 * @return int
	 */
	public function AddMessage($attributes)
	{
		/**
		 * Проверяем корректность переданных данных
		 */	
		//if (!isset($attributes['title']) || !is_string($attributes['title']) || empty($attributes['title']))
			//throw new ArgumentException("Некорректно введен заголовок сообщения");	
			
		if (!isset($attributes['text']) || !is_string($attributes['text']) || empty($attributes['text']))
			throw new ArgumentException("Некорректно введен текст сообщения");
			
		if (!isset($attributes['userEmail']) || !Validator::EmailValidator($attributes['userEmail']))
			throw new ArgumentException("Некорректно введен Email");
	
		if (!isset($attributes['userName']) || !is_string($attributes['userName']) || empty($attributes['userName']))
			throw new ArgumentException("Некорректно введено имя");
		
		if (!isset($attributes['sendDate']) || !Validator::DateValidator($attributes['registrationDate']))
			$attributes['sendDate'] = date("Y-m-d H:i:s");
			
		return $this -> messagesTable -> Insert( $attributes );
	}
	
	/**
	 * Возвращает сообщение, удовлетворяющего заданным атрибутам
	 *
	 * @param 		array 		$attributes
	 * @exception 	Exception	Если найдено 0 или больше 1 сообщения
	 * @return 		array
	 */
	public function GetMessage($attributes)
	{
		$messages = $this -> messagesTable -> Select( $attributes );
		
		if (isset($messages[0])) return $messages[0];
                else return false;
	}
	
	/**
	 * Возвращает массив сообщений, удовлетворяющих заданным параметрам
	 *
	 * @param 	array 	$attributes
	 * @param 	int 	$limit
	 * @param 	int 	$start
	 * @return 	array
	 */
	public function GetMessages($attributes, $limit = null, $start = null)
	{
                $this -> messagesTable -> SortOrder = '`messageID` DESC';
		return $this -> messagesTable -> Select( $attributes, $limit, $start );
	}
	
	public function CountMessages($attributes = null)
	{
		return $this -> messagesTable -> Count( $attributes );
	}
        
        public function updateIsModeration($v_messageID, $v_isModeration)
	{
                return $this -> messagesTable -> Update(array('isModeration' => $v_isModeration), $v_messageID);
	}
}

?>
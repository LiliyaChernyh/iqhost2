<?php
/**
 * Контрол для ввода даты с помощью календарика.
 *
 * @version 3.3
 *
 * Settings:
 *  - dateFormat: Формат вывода даты. Для функции date()
 *  - mask: Маска, по которой будет выполняться ввод даты
 *  - size: Размер поля для ввода
 *  - insertCurrentDateIfEmpty: Вставлять текущую дату если пустое значение. (was ifNull)
 *  - ignoreUserInput: Игнорировать введённые данные и подставлять текущую дату (was ifNotNull)
 *
 */
class DateInput extends Control
{
	/**
     * атрибут size тега intut
     *
     * @var int
     */
	private $size = 60;

	/**
	 * Вставка текущей даты, если поле не заполнено
	 *
	 * @var bool
	 */
	private $insertCurrentDateIfEmpty = false;

	/**
	 * Игнорировать введённые пользователем данные
	 *
	 * @var bool
	 */
	private $ignoreUserInput = false;

	/**
	 * Диапазон годов
	 *
	 * @var string
	 */
	private $yearRange = null;

	/**
	 * Формат даты для вывода в поле
	 *
	 * @var string
	 */
	private $dateFormat = 'd.m.Y';

	/**
	* Можно ли вводить дату самому без календаря
	*
	* @var bool
	*/
	private $AllowEdit = false;

	public function InitControl($value = null, $rowID = null)
	{
		$this -> LoadSettings();

		if (empty($value) && $this -> insertCurrentDateIfEmpty)
			$value = date($this -> dateFormat);
		else if (!empty($value))
			$value = date($this -> dateFormat, strtotime($value));

		$this -> controlTplPath = parent::$CONTROLS_PATH.__CLASS__.'/control.tpl';

		$this -> controlTplVars = array();
		$this -> controlTplVars['NAME'] = $this -> name;
		$this -> controlTplVars['SIZE'] = $this -> size;
		$this -> controlTplVars['TIP']  = $this -> tip;
		$this -> controlTplVars['VALUE'] = $value;
		if ($this -> AllowEdit) $this -> controlTplVars['ALLOW_EDIT'] = 1;
		$this -> controlTplVars['YEAR_RANGE'] = $this -> yearRange;

		if ($this -> disabled)
		    $this -> controlTplVars['READONLY'] = 1;

		$this -> controlTplVars['REQUIRED'] = ($this -> required ? 1 : 0);

		// Используемые JS-библиотеки
		$this -> controlJSLibs = array();
		$this -> controlJSLibs[] = 'JQUERY_UI_DATEPICKER';

		// Инициализирующая функция
		$this -> controlJSFiles = array();
		$this -> controlJSFiles[] = parent::$CONTROLS_ALIAS.__CLASS__.'/init.js';

		// Переменные, необходимые JS
		$this -> controlJSVars = array();
		$this -> controlJSVars['yearRange'] = $this -> yearRange;
		$this -> controlJSVars['name'] = $this -> name;

		$this -> type = __CLASS__;

		if (isset($this->tableName))
            $this->controlTplVars['tableName'] = $this->tableName;
        if (isset($this->formIndex))
            $this->controlTplVars['formIndex']   = $this->formIndex;
    }

    private function LoadSettings()
	{
		if (isset($this -> controlSettings['size']) && is_numeric($this -> controlSettings['size']))
			$this -> size = $this -> controlSettings['size'];

		if (!empty($this -> controlSettings['insertCurrentDateIfEmpty']))
			$this -> insertCurrentDateIfEmpty = $this -> controlSettings['insertCurrentDateIfEmpty'];

		if (!empty($this -> controlSettings['ignoreUserInput']))
			$this -> ignoreUserInput = $this -> controlSettings['ignoreUserInput'];

        if (!empty($this -> controlSettings['dateFormat']))
			$this -> dateFormat = $this -> controlSettings['dateFormat'];

        if (!empty($this -> controlSettings['yearRange']))
			$this -> yearRange = $this -> controlSettings['yearRange'];
	}

	public function PostBackHandler(&$data, $rowID = null)
	{
		$this -> LoadSettings();

        $writeFormat = strpos($this -> dateFormat, 'H') === false ? 'Y-m-d' : 'Y-m-d H:i:s';

		if (isset($data) && !empty($data))
		{
			if ($this -> ignoreUserInput)
				$data = date($writeFormat);
			else
				$data = date($writeFormat, strtotime($data));
		}
		else if ($this -> insertCurrentDateIfEmpty)
			$data = date($writeFormat);
	}
}

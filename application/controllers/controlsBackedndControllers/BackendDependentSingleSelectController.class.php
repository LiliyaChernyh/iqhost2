<?php
class BackendDependentSingleSelectController extends FrontController
{
    public function  GetDependentSelectOptions($args = null)
	{
	    
	    if (empty(Request::$POST['fieldsRegistryEntryID']) || !is_numeric(Request::$POST['fieldsRegistryEntryID']))
	       return new JsonActionResult(array('status' => 'error', 'msg' => 'Не задан fieldsRegistryID', 'errno' => 2222));

	    if (!isset(Request::$POST['dependsOnValue']))
	       return new JsonActionResult(array('status' => 'error', 'msg' => 'Не задан dependsOnValue', 'errno' => 2223));
	    
	    $dependsOnValue = Request::$POST['dependsOnValue'];
	    $fieldsRegistryEntryID = Request::$POST['fieldsRegistryEntryID'];
	    
	    // Узнаем что за поле
        $fieldsRegestryTableMngr = new DBTableManager(TablesNames::$FIELDS_REGISRTY_TABLE_NAME);
        $entries = $fieldsRegestryTableMngr 
                            -> Select(array
                                        ($fieldsRegestryTableMngr 
                                                -> TableStructure 
                                                -> PrimaryKey 
                                                -> Name     => $fieldsRegistryEntryID));

        if (count($entries) == 0)
            return new JsonActionResult(array('status' => 'error', 'msg' => 'Field does not registered', 'errno' => 6));

        $fieldParams = $entries[0];
        
        // Посмотрим заданы ли JSON настройки
        if (isset($fieldParams['controlSettings']) && $fieldParams['controlSettings'] != '')
        {
        	$Jresult = JSON::Decode($fieldParams['controlSettings'], true);
        	if (isset($Jresult['referentTable']))
        		$referentTableName = $Jresult['referentTable'];
        	else
        	   return new JsonActionResult(array('status' => 'error', 'msg' => 'Referent Table  is undefined. It should be defined in FieldsRegistry', 'errno' => 7));
        	   
            if (isset($Jresult['listingKey']))
        		$listingKey = $Jresult['listingKey'];
        	else
        	   return new JsonActionResult(array('status' => 'error', 'msg' => 'Listing key is undefined. It should be defined in FieldsRegistry', 'errno' => 13));
        	
            if (isset($Jresult['dependentField']))
                $dependentField = $Jresult['dependentField'];
            else 
                return new JsonActionResult(array('status' => 'error', 'msg' => 'Dependent field is not set', 'errno' => 10));
                
            if (isset($Jresult['sortOrder']))
                $sortOrder = $Jresult['sortOrder'];
        }
        else 
        {
            return new JsonActionResult(array('status' => 'error', 'msg' => 'Settings for are undefined. They should be defined in FieldsRegistry. "referentTable" setting needed.', 'errno' => 8));
        }
        
        $referentTablStructure = new DBTableStructure($referentTableName);
        $esc = '`';
        if (DB::$DBMS == 'PostgreSQL') $esc = '"';
        
        $returnEmptyResult = false;
        if (is_array($dependsOnValue))
        {
            $cnt = sizeof($dependsOnValue);
            if ($cnt == 0)
                $returnEmptyResult = true;
            $where = '';
            for ($i = 0; $i < $cnt; $i++)
            {
                if ($i > 0) $where .= ' OR ';
                $where .= "{$esc}{$dependentField}{$esc} = '{$dependsOnValue[$i]}'";
            }
        }
        else 
        {
            if (strlen(trim($dependsOnValue)) == 0)
                $returnEmptyResult = true;
            $where = "{$esc}{$dependentField}{$esc} = '$_POST[dependsOnValue]'";
        }
        if (!$returnEmptyResult)
        {
            $query = "SELECT {$esc}{$referentTablStructure->PrimaryKey->Name}{$esc} as {$esc}value{$esc},
                             {$esc}{$listingKey}{$esc} as {$esc}text{$esc}
            				 FROM {$esc}$referentTableName{$esc} 
            				 WHERE $where ";
            if (!empty($sortOrder)) $query .= ' ORDER BY '. $sortOrder;
            
            $qRes = DB::Query($query);
        }
        $result = array();
        $result['controlName'] = $fieldParams['fieldName'];
        $result['options'] = array();
        
        if (!$returnEmptyResult)
        {
            while ($row = DB::FetchAssoc($qRes))
            {
                $result['options'][] = $row;
            }
        }
        
        return new JsonActionResult($result);
	}
}
?>
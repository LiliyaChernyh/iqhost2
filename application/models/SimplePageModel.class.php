<?php

/**
 * Небольшое дополнение для работы с контентом страницы
 *
 * @version 1.0
 */
class SimplePageModel
{
    /**
     * Объект страницы
     *
     * @var Page
     */
    public $page = null;
    
    /**
     * Контруктор
     *
     * @param Page $page
     */
    public function __construct($page)
    {
        $this -> page = $page;
    }
    
    /**
     * Возвращает контент страницы
     *
     * @return array
     */
    public function Content()
    {
        return $this -> page -> Content();
    }
}

?>
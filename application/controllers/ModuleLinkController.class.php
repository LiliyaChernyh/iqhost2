<?php
/**
 * Контроллер для подключения подключаемых модулей
 * 
 * @version 1.0
 *
 */
class ModuleLinkController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function Index($args = null)
    {
        // Первый раздел. Он является названием модуля
        $firstAlias = isset($args[0]) ? $args[0] : null;
        if (is_null($firstAlias)) throw new PageNotFoundException('');
        
        $modules = new Modules();
        $moduleClassName = $modules -> GetModuleClassNameByAlias($firstAlias);
        
        //проверяем путь к админ файлу (классу) в новой версии модулей
        if (!empty($moduleClassName) && is_file(IncPaths::$MODULES_PATH.$moduleClassName.'/application/admin/'.$moduleClassName.'ControllerAdmin.class.php'))
        {                  
            $className = $moduleClassName.'ControllerAdmin';
            $sectionManager = new $className();
            return $sectionManager -> Index();
        }
        else
        {
            throw new PageNotFoundException('');
        }
    }
}
?>
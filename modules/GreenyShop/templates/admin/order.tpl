			<h1>Заказ №{$ORDER.orderID}</h1>
			
			<h3>Информация о заказчике</h3>
			
			{assign var=customer_form_tpl_subpath value='admin/customer_form.tpl'}
			{include file="$MODULE_TPLS_PATH$customer_form_tpl_subpath"}
			
			<h3>Информация о заказе</h3>
			
			<form action="{$MODULE_ALIAS}edit/" method="post" id="orderForm">
			<div style="display:none"><input type="hidden" name="orderID" value="{$ORDER.orderID}" /></div>

			
			<ul style="margin-left: 0;clear:both;">
				<li>Дата создания: <span style="margin-left: 40px">{$ORDER.creationTime}</span></li>
				<li>Дата доставки: <input type="text" name="deliveryDate" id="deliveryDate" style="margin-left: 40px" value="{$ORDER.deliveryDate}" class="date" /></li>
				<li>Статус заказа: <select name="statusID" style="margin-left: 44px" >
									{foreach from=$STATUSES item=STATUS}
										<option value="{$STATUS.statusID}"{if $STATUS.statusID == $ORDER.statusID} selected="selected"{/if}>{$STATUS.description}</option>
									{/foreach}
									</select>
				</li>
				<li style="float:left;"><div style="float:left;margin:0 60px 0 0">Коментарий:</div>
					<textarea name="tip" rows="30" cols="5" style="display:block;float:left;width:700px;height:50px;">{$ORDER.tip}</textarea>
				</li>	
			</ul>
			<div style="clear:both">&nbsp;</div>
			<h3>Корзина заказа</h3>
			
				{assign var=cart_form_tpl_subpath value='admin/cart_form.tpl'}
				{include file="$MODULE_TPLS_PATH$cart_form_tpl_subpath"}
			
				
				<div style="margin-top: 10px; width:98%; text-align:right;">
					Стоимость всего заказа: <strong id="totalCost">{$GOODS_TOTAL_COST} р.</strong> 
				</div>

			<div style="clear:both; margin: -10px auto; width:200px;"><input type="submit" value="Сохранить изменения" name="doChangeOrder"  id="changeOrderBtn" class="handyButton" /></div>
	        </form>
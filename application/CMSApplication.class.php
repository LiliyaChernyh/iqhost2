<?php
class CMSApplication extends Application
{
    public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }

    public static function GetApplication()
    {
        if (!is_null(self::$derivedObject))
            return self::$derivedObject -> GetApplication();
        else
            return self::getInstance();
    }

    public function __construct()
    {
        parent::$derivedObject = &$this;
    }

    public function Init()
    {
        include_once IncPaths::$CONFIG_PATH . 'config.php';
        if (is_file(IncPaths::$CONFIG_PATH . 'config-local.php'))
        {
			include_once IncPaths::$CONFIG_PATH . 'config-local.php';
        }

        parent::Init();
    }

    public function Run()
    {
        try
        {
		parent::Run();
    	}
        catch (PageNotFoundException $e)
    	{
    	    $errorController = new ErrorController();
    	    $result = $errorController -> PageNotFound();
    	    $errorController -> Complete();
    	    $result -> ExecuteResult();
    	}
        catch (Exception $e)
        {
            if (Request::IsAsync())
            {
                $result = new ContentActionResult($e -> getMessage());
                $result -> ExecuteResult();
                exit;
            }
            else
            {
				$result = new ContentActionResult($e -> getMessage() . '<br/>' . $e -> getTraceAsString());
				$result -> ExecuteResult();
				throw $e;
            }
        }
    }
}
?>
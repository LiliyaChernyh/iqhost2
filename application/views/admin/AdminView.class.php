<?php
/**
 * Представление для админки
 *
 * @version 1.0
 *
 */
class AdminView extends ViewResult
{
    public function __construct($tplName = 'main.tpl')
    {
		AppSettings::$SECTION_ID = 'admin';
		$this -> tplsPath			 = '';

		parent::__construct($this -> tplsPath . $tplName);

        //записываем пути к шаблонам и подключаемым файлам
        if (!is_null($this -> moduleName)){
            $this -> publicFilesPath = AppSettings::$MODULES_ALIAS.$this->moduleName.'/';
            $this -> tplsPath = IncPaths::$MODULES_PATH.$this->moduleName.'/templates/admin/';
            ViewData::Assign('IMAGES_MODULE_ALIAS', $this -> publicFilesPath.'images/');
        }

        ViewData::Assign('ADMIN_CSS', AppSettings::$ADMIN_STYLES);
        ViewData::Assign('ADMIN_SCRIPTS', AppSettings::$ADMIN_SCRIPTS);
        ViewData::Assign('ADMIN_IMAGES', AppSettings::$ADMIN_IMAGES);
        ViewData::Assign('ADMIN_ALIAS', AppSettings::$ADMIN_ALIAS);
        ViewData::Append('JS_VARS', array('ADMIN_ALIAS' => AppSettings::$ADMIN_ALIAS, 'ADMIN_IMAGES' => AppSettings::$ADMIN_IMAGES));

        ViewData::Assign('STYLES_ALIAS', AppSettings::$STYLES_ALIAS);
        ViewData::Assign('LIBS_ALIAS', AppSettings::$LIBS_ALIAS);
        ViewData::Assign('SCRIPTS_ALIAS', AppSettings::$SCRIPTS_ALIAS);
        ViewData::Assign('IMAGES_ALIAS', AppSettings::$IMAGES_ALIAS);
        ViewData::Assign('HOST_NAME', 'http://'.$_SERVER['HTTP_HOST']);
		ViewData::Assign("CURRENT_SECTION_ALIAS", Request::GetAliases(1));
        ViewData::Append('SCRIPT_FILE', AppSettings::$SCRIPTS_ALIAS . 'common/common.js'); // стандартный js для админки

		$settingsManager = DBSettingsManager::getInstance();

        $settings = array_merge(
            $settingsManager -> GetSettingsAssoc('general'),
            $settingsManager -> GetSettingsAssoc('front')
        );

        ViewData::Assign('SETTINGS', $settings);
    }

    public function ExecuteResult()
    {
        // Передадим в шаблон алиас текущего маршрута
        $app = Application::GetApplication();
        $currentRoute = $app -> GetRoute();
        $controllerURL = rtrim(AppSettings::$ROOT_ALIAS, '/') . $app -> GetRouter() -> GetURL($currentRoute -> GetControllerName());
        ViewData::Assign('CONTROLLER_ALIAS', $controllerURL);
        ViewData::Append('JS_VARS', array('CONTROLLER_ALIAS' => $controllerURL));

        parent::ExecuteResult();
    }

	/**
     * Задаёт JS-пагинатор в админке
     *
     * @param int $itemsOnPage
     * @param int $itemsNumber
     * @param int $currentPage
     * @param string $prefixUrl
     */
    public function SetJQPaginator($itemsOnPage, $itemsNumber, $currentPage, $prefixUrl)
	{
	    if ($itemsNumber <= $itemsOnPage) return;
	    $paginator = array();
	    $pagesTotal = ceil($itemsNumber / $itemsOnPage);
	    if ($pagesTotal == 1) return;
	    $paginator['itemsOnPage'] = $itemsOnPage;
	    $paginator['current'] = $currentPage;
	    $paginator['total'] = $pagesTotal;
	    $paginator['firstLink'] = $prefixUrl . 1;
	    $paginator['secondLink'] = $prefixUrl . 2;
	    if ($currentPage < $pagesTotal) $paginator['nextLink'] = $prefixUrl . ($currentPage+1);
	    ViewData::Assign('PAGINATOR', $paginator);
	    ViewData::Assign('INCLUDE_PAGINATOR', 1);
	}

	/**
	 * Заливает форму в шаблон
	 *
	 * @param string $tableName
	 * @param int $rowID
	 * @param array $data
	 * @return FormGenerator
	 */
	public function AssignFormToTpl($tableName, $rowID = null, $data = null)
	{
	    $formGenerator = new FormGenerator();
	    $form = $formGenerator -> generateForm($tableName, $rowID, $data);

	    for ($i = count($formGenerator -> includingJSFiles) - 1; $i >= 0; $i--)
	    {
	        ViewData::Append('SCRIPT_FILE', $formGenerator -> includingJSFiles[$i]);
	    }

	    for ($i = count($formGenerator -> includingJSLibs) - 1; $i >= 0; $i--)
	    {
	         ViewData::Assign('INCLUDE_'.$formGenerator -> includingJSLibs[$i], 1);
	    }

	    foreach ($form as $key => $value)
	    {
	        if (empty($value['controls'])) unset($form[$key]);
	    }

		ViewData::Assign('FORM_BLOCKS', $form);
		return $formGenerator;
	}
}
?>
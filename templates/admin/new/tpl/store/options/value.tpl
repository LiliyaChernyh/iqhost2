<tr id="tr-{$val.productOptionID}">
	<td class="hov">&nbsp;</td>
	{*	<td style="width: 20px;">{$fieldIndex+1}</td>*}
	<td>{$val.identifier}</td>
	<td>{if !empty($val.srcSmall)}<img src="{$val.srcSmall}" width="50">{/if}&nbsp;</td>
	<td>{$val.optionValue}</td>
	<td>{$val.size}</td>
	<td>{$val.quantity}</td>
	<td class="enableValue" align="center">
		<div class="hold-checkbox">
			<input type="checkbox" class="ccCheckBox" name="options[{$val.productOptionID}]" value="1" id="checkbox_{$val.productOptionID}" {if isset($val.productOptionID) and $val.isActive == 1}checked="checked"{/if}>
		</div>
	</td>
	<td class="editEntry">
		<a href="#addoption" data-link="/admin/store/options/addproductoption/{$val.optionID}/{$productID}/{$val.productOptionID}/" class="addoption">
			<img style="border: 0;" alt="" src="{$ADMIN_IMAGES}edit.png">
		</a>
	</td>
	<td class="delEntry">
		<a href="#remove" data-link="/admin/store/options/delproductoption/{$val.productOptionID}/" class="deloption">
			<img style="border: 0;" alt="" src="{$ADMIN_IMAGES}delete_icon.png">
		</a>
	</td>
</tr>

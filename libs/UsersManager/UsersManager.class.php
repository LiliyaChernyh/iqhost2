<?php

/**
 * Класс работы с пользователями.
 *
 * @version  2.10
 * 
 */
class UsersManager extends Singleton
{
	/**
	 * Менеджер таблицы пользователей
	 *
	 * @var DBTableManager
	 */
	public $usersTableManager = null;
		
	/**
	 * Enter description here...
	 *
	 * @return UsersManager
	 */
	public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }
    
	function __construct()
	{
		$this -> usersTableManager = new DBTableManager(TablesNames::$USERS_TABLE_NAME);
		$this -> usersTableManager -> SortOrder = "`login`";
	}

	/**
	 * Добавляет нового пользователя в таблицу.
	 * Перед добавлением проверяется корректность всех параметров.
	 * Возвращает ID новой записи.
	 *
	 * @param array $attributes
	 * @return int
	 */
	public function AddUser($FIO, $login, $email, $password, $passwordConf, $role = null, $isActive = null)
	{
	    $attributes = array();
	    
	    $this -> CheckFIO($FIO);
	    $attributes['FIO'] = $FIO;
	    
	    $this -> CheckLogin($login);
	    $attributes['login'] = $login;
	    
            $this -> CheckEmail($email);
	    $attributes['email'] = $email;
	    
	    $this -> CheckPassword($password);
	    if ($password != $passwordConf)
		throw new Exception('Подтвержденный пароль не совпадает с основным');
            
	    $attributes['password'] = Password::HashPassword($password);
	    
	    if (!is_null($role))
	    {
    	    $this -> CheckRole($role);
    	    $attributes['role'] = $role;
	    }
	    
	    if (!is_null($isActive))
	    {
    	    $this -> CheckActivation($isActive);
    	    $attributes['isActive'] = $isActive;
	    }
		
		// Проверяем, есть ли уже пользователь с таким логином в базе
		if ($this -> Exists($login))
			throw new ArgumentException('Пользователь с таким именем уже существует в системе');
		$attributes['registrationDate'] = date('Y-m-d H:i:s');
		
		return $this -> usersTableManager -> Insert($attributes);
	}
	
	/**
	 * Проверяет существование пользователя по его логину или ФИО
	 *
	 * @param string $login
	 * @param string $FIO
	 * @return bool
	 */
	public function Exists($login = null, $FIO = null)
	{
	    if (is_null($login) && is_null($FIO))
            throw new ArgumentException('Необходимо задать либо логин, либо ФИО');
            
        $attrs = array();
        if (!is_null($login))
            $attrs['login'] = $login;
            
        if (!is_null($FIO))
            $attrs['FIO'] = $FIO;
            
        $user = $this -> GetUser($attrs);
        
        return count($user) > 0;
	}
	
	/**
	 * Изменяет ФИО пользователя
	 *
	 * @param string $newFIO
	 * @param int $userID
	 */
	public function ChangeFIO($newFIO, $userID)
	{
	    $this -> CheckUserID($userID);
	    $this -> CheckFIO($newFIO);
            
        $this -> usersTableManager -> Update(array('FIO' => $newFIO), $userID);
	}
	
	/**
	 * Изменяет Логин пользователя
	 *
	 * @param string $newLogin
	 * @param int $userID
	 * @param bool $unique
	 */
	public function ChangeLogin($newLogin, $userID, $unique = true)
	{
	    $this -> CheckUserID($userID);
	    $this -> CheckLogin($newLogin);
	    
            if ($unique && $this -> Exists($newLogin))
                throw new ArgumentException('Пользователь с указанным Логином уже существует');

            $this -> usersTableManager -> Update(array('login' => $newLogin), $userID);
	}
	
	/**
	 * Изменяет пароль пользователя
	 *
	 * @param string $newPassword
	 * @param string $newPasswordConf
	 * @param string $currentPassword
	 * @param int $userID
	 * @param bool $currentPasswordRequired
	 */
	public function ChangePassword($newPassword, $newPasswordConf, $currentPassword, $userID, $currentPasswordRequired = true)
	{
		$this -> CheckUserID($userID);
		
		if ($currentPasswordRequired){
                        if (is_null($currentPassword) || empty($currentPassword))
                            throw new ArgumentException('Вы не ввели текущий пароль');
                            
			$this -> CheckPassword($currentPassword);
                }
                
		if ($newPassword != $newPasswordConf)
			throw new Exception('Новый пароль не совпадает с подтверждающим');
		
		if ($currentPasswordRequired)
		{
			// Получим данные пользователя
			$user = $this -> GetUser(array('userID' => $userID));
			
			// Проверяем, чтобы был введен правильно текущий пароль
                        $t_hasher = new PasswordHash();
                        $check = $t_hasher->CheckPassword($currentPassword, Password::$SALT, $user['password']);
                        
			if (!$check)
                            throw new Exception('Введенный пароль не совпадает с текущим');
		}
		
		return $this -> usersTableManager -> Update(array('password' => Password::HashPassword($newPassword)), $userID);
	}
	
	/**
	 * Изменяет роль пользователя
	 *
	 * @param string $newRole
	 * @param int $userID
	 */
	public function ChangeRole($newRole, $userID)
	{
	    $this -> CheckUserID($userID);
	    $this -> CheckRole($newRole);

	    $this -> usersTableManager -> Update(array('role' => $newRole), $userID);
	}
	
	/**
	 * Изменяет активность пользователя
	 *
	 * @param string $newActivation
	 * @param int $userID
	 */
	public function ChangeActivation($newActivation, $userID)
	{
	    $this -> CheckUserID($userID);
	    $this -> CheckActivation($newActivation);
            
        $this -> usersTableManager -> Update(array('isActive' => $newActivation), $userID);
	}

	/**
	 * Редактирует данные пользователя
	 * По умолчанию, данные проверяются на целостность (чтобы были заданы все необходимые поля) 
	 * и корректность (чтобы поля соответствовали своим типам), но эту проверку можно отключить, 
	 * если передать третим параметром значение false
	 *
	 * @param array $attributes
	 * @param int $userID
	 * @param bool $checkAttributes
	 */
	public function EditUser1($attributes, $userID, $checkAttributes = true)
	{
		// Проверяем корректность переданных данных
		if ($checkAttributes)
		{
			if ((!isset($attributes['FIO'])) or (!is_string($attributes['FIO'])) or (empty($attributes['FIO'])))
				throw new ArgumentException('ФИО пользователя должно быть непустой строкой');
			
			if ((!empty($attributes['email'])) and (!Validator::EmailValidator($attributes['email'])))
				throw new ArgumentException('Некорректно введен Email пользователя ($attributes[email])');
		}
			
		/**
		 * Если задан пароль, то проверяем, чтобы он был равен подтвержденному, и хешируем
		 * Если подтвержденный не совпадает с основным, то генериться исключение.
		 */
		if (isset($attributes['password']))
		{
			if (!empty($attributes['password']))
			{
				if ($attributes['password'] != $attributes['passwordconf'])
					throw new Exception("Подтвержденный пароль не совпадает с основным");
					
				$attributes['password'] = Password::HashPassword($attributes['password']);
			}
			else 
			{
				unset($attributes['password']);
				unset($attributes['passwordconf']);
			}
		}
		
		$this -> usersTableManager -> Update($attributes, $userID);
	}

	/**
	 * Возвращает все данные пользователя, удовлетворяющего заданным параметрам
	 *
	 * @param array $attributes
	 * @return array
	 */
	public function GetUser($attributes)
	{
	    return $this -> usersTableManager -> SelectFirst($attributes);
	}
	
	/**
	 * Возвращает массив пользователей, удовлетворяющих заданным параметрам
	 *
	 * @param array $attributes
	 * @return array
	 */
	public function GetUsers($attributes)
	{
		return $this -> usersTableManager -> Select($attributes);
	}
	
	/**
	 * Выборка пользователей по SQL фильтру
	 *
	 * @param string $filter
	 */
	public function SelectUsers($filter)
	{
		if (!empty($filter)) $filter = ' WHERE '.$filter;
		$query = 'SELECT * FROM `'. TablesNames::$USERS_TABLE_NAME ."` $filter ORDER BY `role`";
		return DB::QueryToArray($query);
	}
	
	/**
	 * Удаляет пользователя из системы
	 *
	 * @param int $userID
	 */
	public function DeleteUser($userID)
	{
	    $this -> CheckUserID($userID);
	    
		$this -> usersTableManager -> Delete(array('userID' => $userID));
	}
	
	/**
	 * Подсчитывает число пользователей, удовлетворяющих заданным параметрам
	 *
	 * @param array $attributes
	 * @return array
	 */
	public function GetUsersNumber($attributes = null)
	{
		return $this -> usersTableManager -> Count($attributes);
	}
	
	/**
	 * Поиск пользователя по переданной строке
	 * Это может быть ID, email, login or FIO
	 * @return 
	 * @param string $searchQuery
	 */
	public function FindUser($searchQuery)
	{
		if (empty($searchQuery)) return null;
		if (is_numeric($searchQuery))
		{
			return array_pop($this -> GetUser(array('userID' => $searchQuery)));
		}
		else 
		{
			$parts = explode(' ', $searchQuery);
		    $filter = '';
            for ($i = 0; $i < count($parts); $i++)
            {
                if (empty($parts[$i])) continue;
                $filter .= ($i == 0 ? ' (' : ' OR '). ' u.FIO LIKE ' . DB::Quote('%'.$parts[$i].'%') ;
				$filter .=  ' OR  u.email LIKE ' . DB::Quote('%'.$parts[$i].'%');
				$filter .=  ' OR  u.login LIKE ' . DB::Quote('%'.$parts[$i].'%');
            }
            $filter .= ')';
					
			$query = 'SELECT * FROM '. $this -> usersTableManager -> TableStructure -> TableName .
				     ' u WHERE '.$filter;
			return DB::QueryToArray($query);
		}
	}

	/**
	 * Проверяет идентификатор пользователя на корректность
	 *
	 * @exception ArgumentException
	 * @param int $userID
	 */
	private function CheckUserID($userID)
	{
	    if (empty($userID) || !is_numeric($userID) || $userID <= 0)
                throw new ArgumentException('Неверно задан идентификатор пользователя ['. $userID .']');
	}

	/**
	 * Проверяет логин пользователя на корректность
	 *
	 * @exception ArgumentException
	 * @param string $login
	 */
	private function CheckLogin($login)
	{
	    if (empty($login) || !is_string($login))
            throw new ArgumentException('Неверно задан логин пользователя');
	}

	/**
	 * Проверяет email пользователя на корректность
	 *
	 * @exception ArgumentException
	 * @param string $email
	 */
	private function CheckEmail($email)
	{
	    if (empty($email) || !Validator::EmailValidator($email))
                    throw new ArgumentException("Некорректно введен Email пользователя ($email)");
            
            if (count($this -> usersTableManager -> Select(array('email' => $email))) > 0)
                    throw new ArgumentException("Пользователь с таким почтовым ящиком уже зарегистрирован ($email)", 11001);
	}

	/**
	 * Проверяет ФИО пользователя на корректность
	 *
	 * @exception ArgumentException
	 * @param string $FIO
	 */
	private function CheckFIO($FIO)
	{
	    if (empty($FIO) || !is_string($FIO))
            throw new ArgumentException('Неверно задано ФИО пользователя');
	}

	/**
	 * Проверяет пароль пользователя на корректность
	 *
	 * @exception ArgumentException
	 * @param string $password
	 */
	private function CheckPassword($password)
	{
	    if (empty($password) || !is_string($password))
            throw new ArgumentException('Неверно задан пароль пользователя');
	}

	/**
	 * Проверяет активность на корректность
	 *
	 * @exception ArgumentException
	 * @param bool $isActive
	 */
	private function CheckActivation($isActive)
	{
	    if (!is_numeric($isActive) || !in_array($isActive, array(0, 1)))
            throw new ArgumentException('Неверно задана активность пользователя');
	}

	/**
	 * Проверяет роль на корректность
	 *
	 * @exception ArgumentException
	 * @param string $role
	 */
	private function CheckRole($role)
	{
	    if (empty($role) || !is_string($role))
            throw new ArgumentException('Неверно задана роль пользователя');
	}
}
?>
/**
*  Инициализатор контрола DateMasked
*/
function DateMaskedInputInit(initObject)
{
    $("#" + initObject.name).mask(initObject.mask);
}
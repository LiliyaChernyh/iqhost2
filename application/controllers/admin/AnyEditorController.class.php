<?php

/**
 * Контроллер для ВсеРедактора
 *
 * @version 1.1
 */
class AnyEditorController extends AdminController
{
    /**
     * Объект класса
     *
     * @var AnyEditor
     */
    public $anyEditor = null;

    /**
     * Название текущей таблицы
     *
     * @var string
     */
    public $tableName = null;

    /**
     * Алиас контроллера
     *
     * @var string
     */
    public $anyEditorAlias = null;

    public function __construct()
    {
        parent::__construct();

        $aliasParts = split('/', $this -> controllerAlias);
        $this -> anyEditorAlias = $aliasParts[count($aliasParts) - 2];

        $this -> anyEditor = AnyEditor::getInstance();
    }

    /**
     * Инициализирует представление
     *
     */
    public function InitView()
    {
        $this -> view = new AnyEditorView();
    }

    /**
     * Список редактируемых таблиц (объектов)
     *
     * @param array $args
     * @return ActionView
     */
    public function Index($args = null)
    {
        // Получаем список таблиц и отправим в шаблон
        $editableTables = AnyEditor::GetEditingObjects($this -> anyEditorAlias);
        ViewData::Assign('TABLES', $editableTables);

        $this -> InitView();
        $this -> view -> Index();
        return $this -> view;
    }

    public function Items($args = null)
    {
        // Получим название таблицы
        $tableName = $this -> TableName();
        if (empty($tableName))
            return $this -> Error('Не задано название таблицы', $this -> controllerAlias);

        $pageNumber = $this -> PageNumber();

        // Получим сортировки
        $sortField = !empty(Request::$GET['sidx']) ? Request::$GET['sidx'] : null;
        $sortOrder = !empty(Request::$GET['sord']) ? Request::$GET['sord'] : null;

        // Фильтры
        $filters = !empty(Request::$GET['filters']) ? Request::$GET['filters'] : null;

        if (!empty($filters))
        {
            $filters = JSON::Decode($filters);
        }

        // Получим список элементов
        $items = $this -> anyEditor -> GetTableItems($tableName, $pageNumber, $sortField, $sortOrder, $filters);

        if (Request::IsAsync())
        {
            $itemsCnt = count($items);
            $entries = array();
            foreach ($items as $id => $row)
            {
                $cell = array();
                foreach ($row as $name => $value)
                    $cell[] = $value;

                $entries[] = array('id' => $id, 'cell' => $cell);
            }

            $response = array();

            $response['page'] = $pageNumber;
            $response['total'] = $this -> anyEditor -> totalPagesNumber;
            $response['records'] = $this -> anyEditor -> entireItemsNumber;
            $response['rows'] = $entries;

            return new JsonActionResult($response);
        }
        else
        {
            // Получим заголовки
            $row = null;
            if (!empty($items))
            {
                $keys = array_keys($items);
        		$row = $items[$keys[0]];
            }
            $headers = $this -> anyEditor -> GetTableHeaders($tableName, $row);

    		ViewData::Assign('TABLE_NAME', $tableName);
    		ViewData::Assign('INCLUDE_JQGRID', 1);
    		ViewData::Assign('TABLE_HEADERS', $headers);
    		ViewData::Assign('ITEMS_ON_PAGE', count($items));

    		if (!empty($this -> anyEditor -> itemsOnPage))
                ViewData::Assign('ITEMS_ON_PAGE', $this -> anyEditor -> itemsOnPage);
            else
                ViewData::Assign('ITEMS_ON_PAGE', count($items));

            $this -> InitView();
            $this -> view -> Items();
            return $this -> view;
        }
    }

    public function Add($args = null)
    {
        // Получим название страницы
        $tableName = $this -> TableName();

        if (empty($tableName))
            return $this -> Error('Не задано имя таблицы', $this -> controllerAlias);

        // Можно ли добавлять
		$tableSettings = AnyEditorSettings::$EditableTables[strtolower($tableName)];
		$allowAddition = isset($tableSettings['allowAddition']) ? $tableSettings['allowAddition'] : 1;
		if ($allowAddition == 0)
            return $this -> Error('Добавлять записи запрещено', $this -> controllerAlias . 'items/?table='. UTF8::strtolower($tableName));

	    if (isset($_POST['doAdd']))
	    {
	        try
	        {
				$this -> SubscribeEventListeners();
				// Event EntryAddPreFlush
				$dispatcher = EventDispatcher::getInstance();
				$args = array();
				$args['tableName'] = $tableName;
				$dispatcher -> Invoke('AnyEditorEntryAddPreFlush', $args);

	            $cm = new ContentManager();
                $res = $cm -> FlushData($tableName, Request::$POST);

                if (empty($res))
				    throw new Exception('Запись не может быть добавлена.');

                return new JsonActionResult(array(
                    'id'        => $res,
                    'refresh'   => ($cm -> formRefreshRequired ? true : true)
                ));
	        }
	        catch (Exception $e)
	        {
	            $response = array(
                    'status'    => 'error',
                    'msg'       => $e -> getMessage(),
                    'error'     => $e -> getMessage()
                );

                return new JsonActionResult($response);
	        }
	    }

        $this -> InitView();
        $this -> view -> Add($tableName);
        return $this -> view;
    }

    public function Edit($args = null)
    {
        // Получим название таблицы
        $tableName = $this -> TableName();

        if (empty($tableName))
            return $this -> Error('Не задано имя таблицы', $this -> controllerAlias);

        if (empty(Request::$GET['rowID']))
            return $this -> Error('Не задан ID элемента', $this -> controllerAlias .'items/?table='. $tableName);

        $rowID = Request::$GET['rowID'];

	    if (isset($_POST['doSave']))
	    {
	        try
	        {
				$this -> SubscribeEventListeners();
				// Event EntryEditPreFlush
				$dispatcher = EventDispatcher::getInstance();
				$args = array();
				$args['tableName'] = $tableName;
				$dispatcher -> Invoke('AnyEditorEntryEditPreFlush', $args);

				$cm = new ContentManager();
                $res = $cm -> FlushData($tableName, Request::$POST);

                if (empty($res))
				    throw new Exception('Запись не может быть отредактирована.');

                return new JsonActionResult(array(
                    'id'        => $res,
                    'refresh'   => ($cm -> formRefreshRequired ? true : false)
                ));
	        }
	        catch (Exception $e)
	        {
	            $response = array(
                    'status'    => 'error',
                    'msg'       => $e -> getMessage(),
                    'error'     => $e -> getMessage()
                );

                return new JsonActionResult($response);
	        }
	    }

        $this -> InitView();
        $this -> view -> Edit($tableName, $rowID);
        return $this -> view;
    }

    public function Delete($args = null)
    {
        if (Request::IsAsync())
        {
            try
    	    {
				$this -> SubscribeEventListeners();
    	        // Получим название таблицы
                $tableName = $this -> TableName();

                if (empty($tableName))
                       return $this -> Error('Не задано имя таблицы', $this -> controllerAlias);

                if (empty(Request::$GET['rowID']))
                    return $this -> Error('Не задан ID элемента', $this -> controllerAlias .'items/?table='. $tableName);

                $rowID = Request::$GET['rowID'];

    	        // Можно ли удалять
        		$allowRemove = isset(AnyEditorSettings::$EditableTables[strtolower($tableName)]['allowRemove']) ? AnyEditorSettings::$EditableTables[strtolower($tableName)]['allowRemove'] : 1;
        		if ($allowRemove == 0)
                    throw new Exception('Нельзя удалять записи');

    	        $dependetTables = isset(AnyEditorSettings::$EditableTables[strtolower($tableName)]['dependentTables']) ? AnyEditorSettings::$EditableTables[strtolower($tableName)]['dependentTables'] : array();
        	    $result = null;
    	        // необходимо проверить зависимые таблицы
    	        for ($i = 0; $i < count($dependetTables); $i++)
        	    {
        	        $tm = DBTableManager::getInstance($dependetTables[$i]['tableName']);
        	        $items = $tm -> Select(array($dependetTables[$i]['fieldName'] => $rowID));
        	        if (is_null($result)) $result = array();
        	        $ccnt = count($items);
        	        if ($ccnt > 0)
        	        {
        	            $dependentItems = array('tableName' => $dependetTables[$i]['tableAlias'], 'itemsLinks' => array());
        	            $dependetTables[$i]['itemEditPath'] = str_replace('{ADMIN_ALIAS}', AppSettings::$ADMIN_ALIAS, $dependetTables[$i]['itemEditPath']);
        	            $dependetTables[$i]['itemEditPath'] = str_replace('{MODULE_ALIAS}', $this -> controllerAlias, $dependetTables[$i]['itemEditPath']);

            	        for ($j = 0; $j < $ccnt; $j++)
            	        {
            	            $dependentItems['itemsLinks'][] = $dependetTables[$i]['itemEditPath'] . $items[$j][$tm -> TableStructure -> PrimaryKey -> Name] .'/';
            	        }

            	        $result[] = $dependentItems;
        	        }
        	    }

        	    $response = array();
        	    if (count($result) > 0)
        	        $response = array('dependentEntries' => $result);
        	    else
        	    {
					$this -> SubscribeEventListeners();
					// Event EntryEditPreFlush
					$dispatcher = EventDispatcher::getInstance();
					$args = array();
					$args['tableName'] = $tableName;
					$dispatcher -> Invoke('AnyEditorEntryDelete', $args);

        	        $tm = DBTableManager::getInstance($tableName);
        	        $tm -> Delete(array($tm -> TableStructure -> PrimaryKey -> Name => $rowID));
        	        $response = array('msg' => 'ok', 'rowID' => $rowID);
        	    }

        	    return new JsonActionResult($response);
    	    }
    	    catch (Exception $e)
    	    {
    	        $response = array(
                    'status'    => 'error',
                    'msg'       => $e -> getMessage(),
                    'error'     => $e -> getMessage()
                );

                return new JsonActionResult($response);
    	    }
        }
        else
        {
            return $this -> Error('Попытка несанкционированного доступа!', $this -> controllerAlias);
        }
    }


    /**
     * Возвращает имя таблица, с которой идет работа
     *
     * @return string
     */
	private function TableName()
	{
	    if (is_null($this -> tableName))
	    {
	        if (empty(Request::$GET['table']))
                return null;
	        $tableName = Request::$GET['table'];

	        $tableName = strtolower($tableName);
    	    // Определяем имя таблицы независимо от регистра
    	    if (!isset(AnyEditorSettings::$EditableTables[$tableName]))
    	    {
    	        $this -> Error('Не могу определить имя таблицы', $this -> controllerAlias);
    	    }
    	    $this -> tableName = AnyEditorSettings::$EditableTables[$tableName]['tableName'];
	    }
	    return $this -> tableName;
	}

	/**
	 * Возвращает номер страницы
	 *
	 * @return int
	 */
	private function PageNumber()
	{
	    return empty(Request::$GET['page']) || !is_numeric(Request::$GET['page']) || Request::$GET['page'] < 0 ? 1 : Request::$GET['page'];
	}

	private function SubscribeEventListeners()
	{
		// Зарегистрируем слушателей событий
		$tableSettings = AnyEditorSettings::$EditableTables[strtolower($this -> TableName())];
		if (!empty($tableSettings['eventListeners']))
		{
			$dispatcher = EventDispatcher::getInstance();
			foreach ($tableSettings['eventListeners'] as $event => $listeners)
			{
				for ($i = 0; $i < count($listeners); $i++)
					$dispatcher -> RegisterListener($event, $listeners[$i]['class'], $listeners[$i]['method']);
			}
		}
	}
}

?>
{if isset($NEWS) && !empty($NEWS)}
    <div class="action-holder">
        <div class="container">
            <h2>Новости</h2>
            <div class="threecolumns">
                {foreach from=$NEWS item=item}
                    <div class="col">
                        <div class="inner">
                            <div class="img-holder">
                                <a href="/news/{$item.alias}/"><img src="/getimage/{$item.imageID}/"  width="540" height="405"></a>
                            </div>
                            <div class="textholder">
                                <p><a href="/news/{$item.alias}/">{$item.caption}</a></p>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
{/if}
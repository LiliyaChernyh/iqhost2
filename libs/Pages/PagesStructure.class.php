<?php

require_once('Page.class.php');

/**
 * Управление древовидной структурой страниц сайта.
 * 
 * @version 1.3
 * 
 */
class PagesStructure extends Singleton 
{	
	/**
	 * Конструктор
	 *
	 * @return PagesStructure
	 */
	function __construct()
	{
	    // Создаем менеджер таблицы
	    $this -> pagesTableManager = DBTableManager::getInstance(TablesNames::$PAGE_STRUCTURE_TABLE_NAME);
	    
	    // Устанавливаем порядок сортировки при выборке
		$this -> pagesTableManager -> SortOrder = "`priority` DESC";
	}

	/**
	 * Менеджер таблицы в БД
	 *
	 * @var DBTableManager
	 */
	public $pagesTableManager = null;

	/**
	 * Возвращает экземпляр данного класса.
	 * Если он был уже создан, заново не создается.
	 *
	 * @return PagesStructure
	 */
    public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }
	
	/**
	 * Функция производит поиск страницы по ее полному алиасу
	 *
	 * @param string $fullPageAlias
	 * @param bool $onlyActive
	 * @return Page
	 */
	public function GetPageByFullAlias($fullPageAlias, $onlyActive = null)
	{
	    try
	    {
	        // Пытаемся получить страниц по полному алиасу
	        $page = new Page($fullPageAlias);
	    }
	    catch (Exception $e)
	    {
	        // Страница не найдена. Возвращаем пустое значение
	        return null;
	    }
	    
	    $onlyActive = is_null($onlyActive) ? false : true;
	    if ($onlyActive && !$page -> isActive)
			return null;
			
		return $page;
	}
	
	/**
     * Формирует полный алиас страницы по ID
     *
     * @param int $pageID
     * @return string
     */
	function GetFullAlias($pageID)
	{
	    // Создаем объект страницы
	    $page = new Page($pageID);
	    
	    // Вычисляем полный алиас
		return $page -> FullAlias();
	}
	
	/**
	 * Добавляет страницу
	 *
	 * @param array $attributes
	 * @return int
	 */
	function AddPage($attributes)
	{
	    // Создаем объект страницы.
	    $page = new Page($attributes, false);
	    
	    // Сохраняем в базе
	    return $page -> Save();
	}
	
	/**
	 * Обновляет атрибуты страницы
	 *
	 * @param int $pageID
	 * @param array $attributes
	 */
	function EditPage($pageID, $attributes)
	{	
	    // Создаем объект страницы.
	    $page = new Page($pageID);
	    
	    // Обновляем данные и сохраняем в базе
	    return $page -> Merge($attributes) -> Save();
	}
	
	/**
	 * Перемещение в корзину
	 *
	 * @param int $pageID
	 */
	public function RecyclePage($pageID)
	{
	    // Создаем объект страницы.
	    $page = new Page($pageID);
	    
	    // Изменяем флаг удаленности
	    $page -> isDeleted = 1;
	    
	    // Сохраняем
	    $page -> Save();

	    // Удаляем дочерние страницы
	    $this -> RecycleChildren($pageID);
	}
	
	/**
	 * Восстанавливает страницу из корзины
	 *
	 * @param int $pageID
	 */
	public function Restore($pageID)
	{
		$query = "
            SELECT  t1.`isDeleted`, t1.`pageID` 
            FROM    ". $this -> pagesTableManager -> GetTableName() ." t1 
            JOIN    ". $this -> pagesTableManager -> GetTableName() ." t2 
                ON  t1.`pageID` = t2.`parentID` 
            WHERE   t2.`pageID` = ". $pageID;
		
		$queryResult = DB::Query($query);
		$hasNoPapa = (DB::ReturnedRows($queryResult) == 0);
		if (!$hasNoPapa) 
		{
			$row = DB::FetchAssoc($queryResult);
			$hasNoPapa = $row['isDeleted'] == 1;
		}
		
		if ($hasNoPapa)
			$this -> EditPage($pageID, array('isDeleted' => 0, 'parentID' => 0));
		else 
			$this -> EditPage($pageID, array('isDeleted' => 0));
	}
	
	/**
	 * Удаление страницы из базы
	 *
	 * @param int $pageID
	 */
	public function WipePage($pageID)
	{
	    // Перемещаем в корзину всех потомков
	    $this -> RecycleChildren($pageID);
	    
		// Удаляем страницу из базы
		$this -> pagesTableManager -> Delete(array('pageID' => $pageID));
	}
	
	/**
	 * Возвращает массив потомков страницы
	 *
	 * @param int $pageID
	 * @param array $attributes
	 * @return array
	 */
	public function GetChildren($pageID, $attributes = null)
	{
	    // Формируем массив атрибутов для получения потомков
	    if (!is_null($attributes)) 
            $attributes['parentID'] = $pageID;
		else 
            $attributes = array('parentID' => $pageID);
        
        // Возвращаем массив страниц
		return $this -> pagesTableManager -> Select($attributes);
	}
	
	/**
	 * Возвращяет страницу по ее идентификатору
	 *
	 * @param int $pageID
	 * @return Page
	 */
	public function GetPageById($pageID)
	{
		return new Page($pageID);
	}
	
	/**
	 * Возвращает страницу, соответствующую заданным атрибутам
	 *
	 * @param array $attributes
	 * @return Page
	 */
	public function GetPage($attributes)
	{
	    $page = new Page($attributes, true, false);
	    return $page -> IsInitialized() ? $page : null;
	}
	
	/**
	 * Подсчитывает количество страниц с заданными атрибутами
	 *
	 * @param array $attributes
	 */
	public function Count($attributes = null)
	{
		return $this -> pagesTableManager -> Count($attributes);
	}
	
	/**
	 * Стоит дерево (наверх), частью которого является страница (До ParentID = 0) и без детей
	 * return [page{parent_id = 2, id = 3}, page{parent_id=1, id=2}, page{parent_id = 0, id = 1}]
	 * 
	 * @param int $pageID
	 * @return array
	 */
	public function GetParentsForPage($pageID)
	{
		try 
		{
		    $page = new Page($pageID);
		}
		catch (PageNotFoundException $e)
		{
			return null;
		}
		
		$res = array();
		$res[] = $page -> pageAttrs;
		return ($page['parentID'] == 0 ? $res : array_merge($res, $this -> GetParentsForPage($page -> parentID)));
	}
	
	/**
	 * Удаляет дочерние страницы
	 *
	 * @param int $pageID
	 */
	private function RecycleChildren($pageID)
	{
	    // Обвноляем записи в БД для соответствующих страниц
		$this -> pagesTableManager -> UpdateRows(array('isDeleted' => 1), array('key' => 'parentID', 'value' => $pageID));
		
		// Получаем массив потомков
		$children = $this -> GetChildren($pageID);
		
		// Удаляем потомков потомков
		for ($i = count($children) - 1; $i >= 0; $i--)
			$this -> RecycleChildren($children[$i]['pageID']);
	}
	
	/**
	 * Возвращет массив страниц, соответствующих заданным атрибутам
	 *
	 * @param array $attributes
	 */
	public function GetPages($attributes, $limit = null, $start = null)
	{
		return $this -> pagesTableManager -> Select($attributes, $limit, $start);
	}
	
	/**
	 * Функция генерирует поддерево структуры для заданной страницы
	 * Поддерево содержит саму страницу, её детей (если есть),
	 * а также всех братьев
	 *
	 * @param int $pageID
	 * @return array
	 */
	public function GetSubTreeForPage($pageID)
	{
		$parents = $this -> GetParentsForPage($pageID);
		if ($parents == null) return null;
		
		$subTree = array();
		$currentLevel = &$subTree;
		for ($i = count($parents)-1; $i>-1; $i--)
		{
			// Получим братьев
			$brothers = $this -> GetPages(array('parentID' => $parents[$i]['parentID'], 'isActive' =>1, 'isDeleted' => 0));
			$nextLevel = null;
			for ($j = 0; $j < count($brothers); $j++)
			{
				$currentItem = array('value' => &$brothers[$j]);
				// Проверяем является ли этот узел отцом или праотцом первоначального
				if ($brothers[$j]['pageID'] == $parents[$i]['pageID'])
				{
					$currentItem['children'] = array();
					$nextLevel = &$currentItem['children'];
				}
				else
				{
					$currentItem['children'] = null;
				}
				$currentLevel[] = $currentItem;
			}
			if (!is_array($nextLevel)) break;
			$currentLevel = &$nextLevel;
		}
		// Получим братьев
		$brothers = $this -> GetPages(array('parentID' => $parents[0]['pageID'], 'isActive' =>1, 'isDeleted' => 0));
			
		for ($j = 0; $j < count($brothers); $j++)
			$currentLevel[] = array('value' => &$brothers[$j], 'children' => null);

		return $subTree;
	}
	
	/**
	 * Возвращает дерево страниц, начиная с $sectionPageID
	 *
	 * @param int $sectionPageID
	 * @return array
	 */
	public function GetTreeForSection($sectionPageID, $attributes = null)
	{
		$tree = array();
		$attr = ($attributes == null) ? array() : $attributes;
		
		$attr['pageID'] = $sectionPageID;
		$page = $this -> GetPage( $attr );
		
		$tree = array(	'value'		=>	$page,
						'children'	=>	array()
					);

		$attr = ($attributes == null) ? array() : $attributes;	
		$attr['parentID'] = $page['pageID'];
		$pages = $this -> GetPages( $attr );
			
		foreach ($pages as $child)
		{
			$tree['children'][] = $this -> GetTreeForSection($child['pageID'], $attributes);
		}

		return $tree;
	}

	/**
	 * Возвращает список ID родительских страниц
	 * 
	 * @param int $pageID
	 * @return array
	 */
	public function GetParentIDs($pageID)
	{
		if ($pageID == 0)
			return array();

		$currentPage = $this -> GetPage( array('pageID' => $pageID) );

		$parent_id = $currentPage -> parentID;

		$parents = array();
		$parents[] = $pageID;
		while ($parent_id != 0)
		{
			$parents[] = $parent_id;
			$page = $this -> GetPages( array('pageID' => $parent_id) );
			$parent_id = $page[0]['parentID'];		
		}

		return $parents;
	}

    /**
     * Получение из алиаса id страницы, модуля - обработчика и параметров для модуля.
     * Возвращаемый массив page + [fullAlias] + [params => {param1,param3}]
     *
     * @param array $pageAlias
     * @return array
     */
    public function GetExistingPage($pageAlias)
    {
        throw new Exception('Метод GetExistingPage(). ХЗ что за фигня');
        
        $aliasNo = count($pageAlias);
        $testAlias = '/';
        $fullAliasLength = -1;
        for ($i = 0; $i < $aliasNo; $i++)
        {
            $testAlias .= $pageAlias[$i].'/';
            $testPage = $this -> GetPageByFullAlias($testAlias);
            if($testPage == null)
            {
                $testPage = $prevPage;
                $fullAliasLength = $i;
                break;
            }
            else 
            {
                $prevPage = $testPage;
            }
        }
        if($fullAliasLength == -1)
        {
            $fullAliasLength = $aliasNo;
        }
        $testPage['fullAlias'] = array_slice($pageAlias,0,$fullAliasLength);
        $testPage['params'] = array_slice($pageAlias,$fullAliasLength - 1);
        return $testPage;
    }

	// Возвращает всех потомков
	// детей вглубь
	public function GetOffsprings($pageID, &$attrs)
	{
	    throw new Exception('Метод GetOffsprings(). ХЗ что за фигня');
	    
		$children = $this -> GetChildPages($pageID, $attrs);
		$cnt = count($children);
		$result = array();
		$result = $children;
		for($i = 0; $i < $cnt; $i++)
		{
			$result = array_merge($result, $this -> GetOffsprings($children[$i]['pageID'], $attrs));
		}
		return $result;
	}
	
	/**
	 * Подсчитывает количество страниц с заданными атрибутами
	 *
	 * @param array $attributes
	 */
	public function PagesNumber($attributes)
	{
		throw new Exception('Метод PageNumber() устарел. Используй Count()');
	}
	
	/**
	 * Выдает массив с номерами дочерних страниц для страницы $parentID
	 *
	 * @param int $pageID
	 * @return array
	 */
	public function GetChildPages($pageID, $attributes = null)
	{
		throw new Exception('Метод GetChildPages был удален. Используйте GetChildren');
	}
	
	/**
	 * Клонирует страницу
	 * Если задан флаг $cloneContent, копирует контент, но поверхностно (без копирования связей)
	 * Можно задавать новые значения "названия страницы" и "алиаса".
	 * 
	 * @param $pageID
	 * @param $cloneContent
	 * @param $newCaption
	 * @param $newAlias
	 * @return int
	 */
	public function ClonePage($pageID, $cloneContent = true, $newCaption = null, $newAlias = null)
	{
	    if (!is_numeric($pageID) || empty($pageID))
	       throw new ArgumentException('PageID must be numeric');
	       
	    $page = $this -> GetPageById($pageID);

        // Менеджер таблицы с типами страниц
        $pagesTypesTableManager = DBTableManager::getInstance(TablesNames::$PAGES_TYPE_TABLE_NAME);
        
        // Получаем данные о текущем типе
        $pageType = $pagesTypesTableManager -> SelectFirst(array('pageTypeID' => $page -> pageTypeID));
        
        $contentID = 0;
        // Клонируем контент
        if ($cloneContent && !empty($pageType) && $pageType['handlerType'] == 'table')
        {
            // Вытаскиваем название таблицы
            $tableName = $pageType['name'];
            
            // Создаем менеджер данной таблицы
            $contentTableManager = DBTableManager::getInstance($tableName);
           
            // Получаем контент
            $content = $contentTableManager -> SelectFirst(array($contentTableManager -> TableStructure -> PrimaryKey -> Name => $page -> contentID));
            
            if (!empty($content))
            {
                unset($content[$contentTableManager -> TableStructure -> PrimaryKey -> Name]);
                $fieldsCnt = count($contentTableManager -> TableStructure -> Fields);
                for ($i = 0; $i < $fieldsCnt; $i++)
                {
                    if ($contentTableManager -> TableStructure -> Fields[$i] -> IsUnique)
                        unset($content[$contentTableManager -> TableStructure -> Fields[$i] -> Name]);
                }
                $contentID = $contentTableManager -> Insert($content);
            }
        }
        
        try
        {
            // Клонируем страницу
            $pageAttrs = $page -> pageAttrs;
            unset($pageAttrs['pageID']);
            
            if (!empty($newCaption))
                $pageAttrs['caption'] = $newCaption;
            else 
                $pageAttrs['caption'] .= ' (Копия) ';
                
            $cloneAlias = empty($newAlias) ? $cloneAlias = $pageAttrs['alias'] .'_'. rand(0, 1000) : $newAlias;
            $uniqueAlias = false;
            while (true) 
            {
                $pageWithSameAliasCnt = $this -> Count(array('parentID' => $pageAttrs['parentID'], 'alias' => $cloneAlias));
                if ($pageWithSameAliasCnt > 0)
                    $cloneAlias = empty($newAlias) ? $cloneAlias = $pageAttrs['alias'] .'_'. rand(0, 1000) : $newAlias .'_'. rand(0, 1000);
                else 
                    break;
            }
            
            $pageAttrs['alias'] = $cloneAlias;
            $pageAttrs['contentID'] = $contentID;
    	    
            $pageID = $this -> AddPage($pageAttrs);
        }
        catch (Exception $e)
        {
            // Подобие транзакции
            if (!empty($contentID) && !empty($contentTableManager))
                $contentTableManager -> Delete(array($contentTableManager -> TableStructure -> PrimaryKey -> Name => $contentID));
            throw $e;
        }
        return $pageID;
	}
	
	/**
	 * Возвращает дерево страниц, начиная с указанной, 
	 * которые удовлетворяют заданным параметрам
	 *
	 * @param int $parentID
	 * @param array $attributes
	 * @return array
	 */
	public function GetPagesTree($parentID = null, $attributes = null)
	{
	    // Добавляем в массив параметров условия на parentID и isDeleted
	    $attrs = !empty($attributes) ? array_merge($attributes, array('parentID' => $parentID, 'isDeleted' => 0)) : array('parentID' => $parentID, 'isDeleted' => 0);
	    
	    // Получем список страниц
	    $pages = $this -> GetPages($attrs);
	    
	    // Для каждой страницы в цикле получаем его поддерево
	    $pagesCnt = count($pages);
	    for ($i = 0; $i < $pagesCnt; $i++)
	    {
	        $pageChildren = $this -> GetPagesTree($pages[$i]['pageID'], $attributes);
	        if (!empty($pageChildren))
                $pages[$i]['children'] = $pageChildren;
	    }
	    
	    return $pages;
	}
	
	/**
	 * Задает интервал приоритетов между страницами, которые находятся на том же уровне,
	 * что и указанная страница $startFromPageID и у которых приоритет больше, чем приоритет
	 * это страницы
	 *
	 * @param int $startFromPageID
	 * @param int $interval
	 */
	public function SetNewPriorityInterval($startFromPageID, $interval = 20)
	{
	    $query = '
	       SET @cur_priority = (SELECT `priority` FROM `'. TablesNames::$PAGE_STRUCTURE_TABLE_NAME .'` WHERE `pageID` = '. $startFromPageID .');
	       SET @start_priority = (SELECT `priority` FROM `'. TablesNames::$PAGE_STRUCTURE_TABLE_NAME .'` WHERE `pageID` = '. $startFromPageID .');
	       SET @parent_id = (SELECT `parentID` FROM `'. TablesNames::$PAGE_STRUCTURE_TABLE_NAME .'` WHERE `pageID` = '. $startFromPageID .');
	       UPDATE  `'. TablesNames::$PAGE_STRUCTURE_TABLE_NAME .'`
	       SET     `priority` = (
	           SELECT  @start_priority:=@start_priority+'. $interval .'
	       )
	       WHERE `priority` >= @start_priority
	           AND `isDeleted` = 0
	           AND `parentID` = @parent_id
	       ORDER BY `priority` ASC
	    ';
	    
	    DB::Query($query);
	}
}
?>
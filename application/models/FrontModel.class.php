<?php

/**
 * Небольшое дополнение для работы с контентом страницы
 *
 * @version 1.0
 */
class FrontModel
{
     public $menuListTN = 'MenuList';
     public $menuTN = 'Menu';
    public function checkLoginUser($login)
    {
       $query = 'SELECT 
                    COUNT(*) 
                 FROM `'.TablesNames::$USERS_TABLE_NAME.'` usr
                 WHERE 
                    usr.`login` = "'.$login.'"';
       
       return DB::QueryOneValue($query);
    }
    
    public function checkEmailUser($email)
    {
       $query = 'SELECT 
                    COUNT(*) 
                 FROM `'.TablesNames::$USERS_TABLE_NAME.'` usr
                 WHERE 
                    usr.`email` = "'.$email.'"';
       
       return DB::QueryOneValue($query);
    }
    
    public function saveUser($user)
    {
       $query = 'INSERT INTO `'.TablesNames::$USERS_TABLE_NAME.'` (`login`,`password`,`email`,`FIO`,`role`,`isActive`,`registrationDate`)
                 VALUES ("'.$user['login'].'","'.$user['password'].'","'.$user['email'].'","'.$user['FIO'].'","user",0,NOW())';
       
       return DB::Query($query);
    }
    
    public function getLoginPageAliases()
    {
       $query = 'SELECT
                    ps.`pageID`
                 FROM `'.TablesNames::$PAGE_STRUCTURE_TABLE_NAME.'` ps
                 WHERE 
                    ps.`isLogin` = 1';

       $res = DB::QueryToArray($query);
       if (is_array($res)){
           $ar = array();
           foreach($res as $item){
               $ar[] = $item['pageID'];
           }
           
           return $ar;
       }else
           return false;
    }
 public function GetMenuName()
    {
        $query = 'SELECT ml.`menuListAlias` FROM `'.$this->menuListTN.'`  ml';
        return DB::QueryToArray($query);
}

public function GetMenuByAlias($alias)
	{
		$query	 = 'SELECT ml.`menuListID`
                            FROM  ' . $this->menuListTN . ' ml
                            WHERE ml.`menuListAlias` = "' . $alias . '"';
		$id		 = DB::QueryOneValue($query);
		if (!empty($id))
		{
			$query		 = '
                            SELECT m.itemID,
                            m.parentID,
                            m.menuListID,
                            m.name as name,
                            m.alias,
                            m.imageID
                            FROM  ' . $this->menuTN . ' m
                            WHERE m.`menuListID` = ' . $id . ' AND m.isActive = 1 
                            ORDER BY m.`priority` ASC';
			$menuArr	 = DB::QueryToArray($query, 'itemID');
			$query1		 = '
                            SELECT m.`itemID`
                            FROM  ' . $this->menuTN . ' m
                            WHERE m.`name` = "-" AND m.`menuListID` = ' . $id . '';
			$rootElem	 = DB::QueryOneValue($query1);
			$catTree	 = Tree::Table2Tree($menuArr, $rootElem, 'itemID', 'parentID');
			return $catTree;
		}
		else
			return $catTree = array();
	}
        
    public function GetHeaderImage($imageID)
    {
        if(!empty($imageID)){
        $query = '
            SELECT im.`src` 
            FROM  `greeny_images`  im
            WHERE im.`imageID` = '.$imageID.'
            ';
        return DB::QueryOneValue($query);
        } else return array();
    }
}

?>
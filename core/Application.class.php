<?php
/**
 * Стандартный класс приложения
 *
 * @version 1.0
 */
class Application extends Singleton implements IApplication
{
    /**
     * Ссылка на объект текущего приложения
     *
     * @var Application
     */
    protected static $derivedObject = null;

    /**
     * Ссылка на маршрутизатор приложения
     *
     * @var IRouter
     */
    protected $router = null;

    /**
     * Текущий маршрут
     *
     * @var Route
     */
    protected $route = null;

    /**
     * Получение объекта класса
     *
     * @return Application
     */
    public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }

    /**
     * Получение объекта наследника, если есть
     *
     * @return Application
     */
    public static function GetDerived()
    {
        return self::derivedObject;
    }

    /**
     * Получение объекта приложения: наследника или
     *
     * @return Application
     */
    public static function GetApplication()
    {
        if (!is_null(self::$derivedObject))
            return self::$derivedObject -> GetApplication();
        else
            return self::getInstance();
    }

    /**
     * Получение текущего маршрута
     *
     * @return Route
     */
    public function GetRoute()
    {
        if (!is_null($this -> route))
            return $this -> route;

        return null;
    }

    /**
     * Получение роутера
     *
     * @return Router
     */
    public function GetRouter()
    {
        return $this -> router;
    }

    /**
     * Запуск приложения
     *
     */
    public function Run()
    {
        $this -> Init();
        $this -> Execute();
        $this -> Complete();
    }

    /**
     * Инициализация приложения
     *
     */
    protected function Init()
    {
        Request::Init();
        AppSettings::Init();

        if (empty($this -> router))
        {
//            $routingTable = new RoutingTableProvider();
            $routingTable = new RoutingTableDBProvider();
            $this -> router = new Router($routingTable);
        }

        register_shutdown_function('Application::Shutdown');
        setlocale(LC_ALL, 'ru_RU.UTF-8', 'ru_RU.utf8', 'ru_RU');
    }

    /**
     * Выполнение приложения
     *
     * @param Route $route
     */
    protected function Execute()
    {
        // Получение маршрута
        $this -> route = $this -> router -> GetRoute();

        $controllerName = $this -> route -> GetControllerName();
        $actionName = $this -> route -> GetActionName();

        // создание контроллера
        if (!class_exists($controllerName))
            throw new PageNotFoundException('Не найден класс контроллера '.$controllerName);

        $controller = new $controllerName();

        // Инициализация контроллера
        $actionResult = $controller -> Init();
        if (empty($actionResult))
            throw new Exception('Вызываемый метод контроллера должен возвращать объект класса ActionResult');

        $actionResult -> ExecuteResult();
        if ($actionResult -> IsExecutionStopped())
            return;

        // Выполнение нужного действия
        if (!method_exists($controller, $actionName))
            throw new PageNotFoundException('Не найден метод контроллера '.$actionName);

        $actionResult = $controller -> $actionName($this -> route -> GetArgs());
        if (empty($actionResult))
            throw new Exception('Вызываемый метод контроллера должен возвращать объект класса ActionResult');

        // Вызываем "пост-обработчик" контроллера
        $controller -> Complete();

        $actionResult -> ExecuteResult();
    }

    /**
     * Завершение приложения
     *
     */
    protected function Complete()
    {

    }

    public static function Shutdown()
    {
        Autoload::SavePaths();
    }
}
?>
<?php

class Encoder
{
	/**
	 * Перекодировщик из UTF-8 в WIN-CP1251
	 *
	 * @param string $str
	 * @return string
	 */
	public static function UTF8_CP1251($str)
	{
		return self::UTF8andCP1251($str, 'w');
	}
	
	/**
	 * Перекодировщик из WIN-CP1251 в UTF-8
	 *
	 * @param string $str
	 * @return string
	 */
	public static function CP1251_UTF8($str)
	{
		return self::UTF8andCP1251($str, 'u');
	}
	
	/**
	 * Перекодирует строку из кодировки UTF-8 в WIN-CP1251 (параметр type = 'w')
	 * и наоборот (параметр type = 'u')
	 *
	 * @param string $str
	 * @param char $type
	 * @return string
	 */
	private static function UTF8andCP1251($str, $type)
	{
	    $conv='';
	    if (!is_array ( $conv ))
	    {
	        $conv=array ();
	        for ( $x=128; $x <=143; $x++ )
	        {
	        	$conv['utf'][]=chr(209).chr($x);
	          	$conv['win'][]=chr($x+112);
	        }
	
	        for ( $x=144; $x <=191; $x++ )
	        {
				$conv['utf'][]=chr(208).chr($x);
	            $conv['win'][]=chr($x+48);
	        }
	 
	        $conv['utf'][]=chr(208).chr(129);
	        $conv['win'][]=chr(168);
	        $conv['utf'][]=chr(209).chr(145);
	        $conv['win'][]=chr(184);
	     }
	     if ( $type=='w' )
	     	return str_replace ( $conv['utf'], $conv['win'], $str );
	     elseif ( $type=='u' )
	        return str_replace ( $conv['win'], $conv['utf'], $str );
	     else
	        return $str;
  	}
}

?>
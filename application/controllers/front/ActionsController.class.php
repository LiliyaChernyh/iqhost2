<?php

class ActionsController extends FrontController {

   
    public function __construct() {
        parent::__construct();
        $this->view = new ActionsView();
        $this->model = new ActionsModel();
    }


    public function Index($args = null)
    {
        $alias = Request::GetAliases();
        $this->page = new Page($alias);
        $actions = $this->model->GetActions($this->page->pageID);   
        $content = $this->page->Content();
//      trace($actions);
        ViewData::Assign('ACTIONS', $actions);
        return $this->view->Index();
    }
    
     public function Item($args = null)
    {
        $this->page = new Page(Request::GetAliases());
        $content = $this->page->Content();             
//      trace($content);
        ViewData::Assign('ACTION', $content);
        return $this->view->Item();
    }

     
    

}

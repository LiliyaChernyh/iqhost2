<h1>Фотобанк</h1>

<div>
    {if isset($TYPES_AND_TAGS) and !empty($TYPES_AND_TAGS)}
    <ul>
    {foreach from=$TYPES_AND_TAGS item=TYPE}
        <li>{$TYPE.caption}
            <ul>
            {foreach from=$TYPE.children item=TAG}
                <li><a href="/{$ROOT_ALIAS}/{$TAG.tagID}/">{$TAG.caption}</a></li>
            {/foreach} 
            </ul>
        </li>
    {/foreach}  
    </ul>
    {else}
        <h2>У пользователя нет доступных тегов</h2>
    {/if}
    
    {if isset($USER_IMAGES)}
    <ul>
    {foreach from=$USER_IMAGES item=IMAGE}
        <li><img src="{$IMAGE.srcThumbnail}"><a href="{$IMAGE.src}" download="{$IMAGE.src}">Скачать</a></li>
    {/foreach}  
    </ul>   
    {/if}   
</div>
<?php
/**
 * Абстрактный класс результата выполнения контроллера    
 * 
 * @version 1.0
 */
abstract class ActionResult
{
    /**
     * Флаг остановки дальнейшего процесса обработки
     *
     * @var bool
     */
    private $stopExecution = false;

    /**
     * Остановка процесса обработки.
     * При инициализации контроллера, дальнейшие
     * этапы работы контроллера не будут вызваны
     *
     */
    public function StopExecution()
    {
        $this -> stopExecution = true;
    }

    /**
     * Проверка остановки выполения
     *
     * @return 
     */
    public function IsExecutionStopped()
    {
        return $this -> stopExecution;
    }
    
    public abstract function ExecuteResult();
}
?>
/**
*  Инициализатор контрола TagsInput
*/
function TagsInputInit(initObject)
{
    var controlName = initObject.name;
    var newTagsNumber = 0;
    
    var SuggesterServerScript = '/admin/controls/TagsInput/';
    
    $("#" + controlName + "_text").autocomplete(
        SuggesterServerScript, 
        {
            minChars:2,
            max:100, 
            extraParams: 
            {
                objectType : initObject.objectType
            }
        }
    ).result(function(event, data, formatted)
    {
        if (data)
            $("#" + controlName).val(data[1]);
    });
    
    $("." + controlName + "_tagRemove").data("controlName", controlName).click(TagsInputRemoveTag);
    
    $("#" + controlName + "_addTag").click(function()
    {
        var newTag = $("#" + controlName + "_text").val();
        if (newTag.length != 0)
        {
            newTagsNumber += 1;
            $("#" + controlName + "_tags").append('<span>' + newTag + '<img src="/images/icons/cross_small.png" id="' + controlName + '_tag_new' + newTagsNumber + '" class="' + controlName + '_tagRemove tagRemove" align="top" title="Удалить тег «' + newTag + '»" /></span>');
            $("#" + controlName + "_IDs").append('<input type="hidden" id="' + controlName + '_tag_new' + newTagsNumber + '" name="' + controlName + '__newTags[]" value="' + newTag + '" />');
            
            $("." + controlName + "_tagRemove").data("controlName", controlName).click(TagsInputRemoveTag);
            $("#" + controlName + "_text").val("");
        }
    });
}

function TagsInputRemoveTag()
{
    var controlName = $(this).data("controlName");
    var objectID = $(this).attr("id").split("_");
    tagID = objectID[2];
    
    $("#" + controlName + "_IDs").children("input#" + controlName + "_tag_" + tagID).remove();
    $(this).parent().remove();
}
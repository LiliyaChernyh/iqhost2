/**
*  Инициализатор контрола MultipleRichText
*/
function MultipleRichTextInit(initObject)
{
    $('textarea.MRT_fck').each(function() {
        var that = $(this);
        InitRichText(that);
    });
    
    $('#' + initObject.name + '_add').click(function(event){
        event.preventDefault();
        
        AddNewFCKRow(initObject);
    });
}

function InitRichText(that)
{
    var parts = that.attr("class").split(" ");
    var height = that.css("height");
    height = height.replace('px', '');
    
    var params = {
        BasePath: '/libs/fckeditor/', 
        toolbar: parts[1]
    };
    
    if (typeof height != 'undefined')
        params['height'] = height;
        
    that.fck(params);
}

function AddNewFCKRow(initObject)
{
    $('#' + initObject.name + '_table tbody').append('<tr><td></td><td><textarea style="width:'+ initObject.width +'; height:'+ initObject.height +'" class="MRT_fck_new ' + initObject.toolbar + '" name="'+ initObject.name +'__add[]"></textarea></td><td></td></tr>');
    var newFck = $('.MRT_fck_new');
    InitRichText(newFck);
}
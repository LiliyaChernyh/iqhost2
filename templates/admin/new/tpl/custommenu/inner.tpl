
<h1>Управление меню: {$PARENT_NAME.menuListName}</h1>

<div style="text-align: right; margin: -30px 0 5px 0;">
    <input type="button" id="goAddTheme" class="goToBtn" value="Добавить пункт" />
    <input type="hidden" id="goAddTheme_link" value="{$CONTROLLER_ALIAS}showinner/{$PARENT_NAME.menuListID}/addinner/" />
</div>
{if !empty($INNER_MENU)}
    <ul class="ul-treefree ul-dropfree">
    {foreach from=$INNER_MENU item=THEME key=I}
        <li>{$THEME.name}
            &emsp;
            <a href="/admin/custommenu/showinner/{$PARENT_NAME.menuListID}/edit/{$THEME.itemID}" title="Редактировать">
				<img src="{$ADMIN_IMAGES}icons/pencil.png" />
            </a>
            &emsp;
            <a class="deleteEntry" href="{$CONTROLLER_ALIAS}showinner/{$PARENT_NAME.menuListID}/del/{$THEME.itemID}/" title="Удалить">
				<img src="{$ADMIN_IMAGES}icons/delete_icon.png" />
            </a>
        {if !empty($THEME.children)}
            <ul  class="ul-treefree ">
                {foreach from=$THEME.children item=child}
                    <li >{$child.name}
                        &emsp;
                        <a href="/admin/custommenu/showinner/{$PARENT_NAME.menuListID}/edit/{$child.itemID}" title="Редактировать">
							<img src="{$ADMIN_IMAGES}icons/pencil.png" />
                        </a>
                        &emsp;
                        <a class="deleteEntry" href="{$CONTROLLER_ALIAS}showinner/{$PARENT_NAME.menuListID}/del/{$child.itemID}/" title="Удалить">
							<img src="{$ADMIN_IMAGES}icons/delete_icon.png" />
						</a>
                    </li>
                {/foreach}
            </ul>
        {/if}
        </li>
      {/foreach}
    </ul>
{/if}




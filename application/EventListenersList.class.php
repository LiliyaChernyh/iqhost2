<?php
/**
 * Список слушателей событий приложения
 * @author George
 *
 */
class EventListenersList
{
    public static $ListenersList = array(
        /*
		Можно создать список подписчиков на стандартные события и где-нибудь их подписать,
		например в AdminController, так:
		
		PluginsSubscriber::SubscribeAll(EventListenersList::$ListenersList);
		
		array(
            'event'     => 'StructureController.AddPage.End',
            'listener'  => 'CacheCleaner',
            'handler'   => 'CleanCache'
        )
		*/
    );
} 
?>
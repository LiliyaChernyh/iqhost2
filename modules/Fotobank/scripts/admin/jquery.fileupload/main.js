/*
 * jQuery File Upload Plugin JS Example 8.0
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, unparam: true, regexp: true */
/*global $, window, document */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: '/admin/Fotobank/file-upload/' + tagID
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    if (window.location.hostname === 'blueimp.github.com' ||
            window.location.hostname === 'blueimp.github.io') {
        // Demo settings:
        $('#fileupload').fileupload('option', {
            url: '//jquery-file-upload.appspot.com/',
            disableImageResize: false,
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: '//jquery-file-upload.appspot.com/',
                type: 'HEAD'
            }).fail(function () {
                $('<span class="alert alert-error"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {
        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function (result) {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, null, {result: result});
        });
    }
    
    setTimeout(function(){
        $('.caption,.description,.tagID').live('change',function(){ 
            $(this).attr('name', 'update');
        });
        
        $('.tagID').each(function(){ 
            $(this).removeAttr('name');
        });
    }, 400);
    
    //переименовываем поля
    $('#save').click(function(){
        var ok_send = true;
        
        $('.caption').each(function(i){
            if ($(this).attr('name') == 'update')
                $(this).attr('name', 'caption[' + $(this).attr('data-image-id') + ']');
            if ($(this).val() == ''){ 
                ok_send = false;
                $(this).closest('div').removeClass('success').addClass('error');
            }else{
                $(this).closest('div').removeClass('error').addClass('success');
            }
        });

        $('.tagID').each(function(i){
            if ($(this).attr('name') == 'update')
                $(this).attr('name', 'tagsID[' + $(this).attr('data-image-id') + '][]');
        });

        $('.description').each(function(i){
            if ($(this).attr('name') == 'update')
                $(this).attr('name', 'description[' + $(this).attr('data-image-id') + ']');
            
            if ($(this).val() == ''){ 
                ok_send = false;
                $(this).closest('div').removeClass('success').addClass('error');
            }else{
                $(this).closest('div').removeClass('error').addClass('success');
            }
        });
        
        return ok_send;
    });

});

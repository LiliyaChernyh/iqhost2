{*<div class="blog_c ideas-hover">
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
            {foreach from=$articles item=item key=key}
                <div class="blog_blk red5 clearfix animated" data-animation="rollIn">
                    {if !empty($item.imageID)}
                        <a {if !$item.isBody}href="{$item.fullAlias}"{/if}>
                            <div class="box ">
                                <figure class="effect-chico">
                                    <img alt="{$item.caption}"  src="/getimage/{$item.imageID}/861x300xS/"  />
                                    <figcaption></figcaption>
                                </figure>
                            </div>
                        </a>
                    {/if}
                    <div class="blog_desc">
                        <h5><a {if !$item.isBody}href="{$item.fullAlias}"{/if}>{$item.caption}</a></h5>
                        <p>{$item.summary}</p>
                    </div>
                        <div class="tag_c clearfix">
                            <ul>
                                <li><span class="fa fa-calendar"></span><span>{$item.publicationDate|date_format:"%e %m %Y":"":"rus"}</span></li>
                            </ul>
                        </div>
                </div>
            {/foreach}
            {include file="paginator.tpl"}
        </div>
    </div>
</div>*}

<div class="top-section">
    <div class="container">
        <div class="twocolumns">
            <div class="text-holder">
                <h1>Инструкции</h1>
                {if !empty($SECTIONS)}
                <ul class="sidebar-nav">
                    {foreach from=$SECTIONS item=pageItem}
                        <li{if $PAGE.alias == $pageItem.alias} class="active"{/if}><a href="/articles/{$pageItem.alias}/">{$pageItem.caption}</a></li>
                    {/foreach}
                </ul>
                {/if}
            </div>
            <div class="section-right">
                <div class="twocolumns">
                    <div class="content-holder">
                        {if !empty($articles) && isset($articles)}
                         {foreach from=$articles item=item key=key}
                            {*}{if !empty($item.imageID)}
                                <a href="{$item.fullAlias}">
                                    <div class="img-holder">
                                        <img alt="{$item.caption}"  src="/getimage/{$item.imageID}/861x300xS/"  />
                                    </div>
                                </a>
                            {/if}*}
                            <a href="{$item.fullAlias}">{$item.caption}</a>
                            <p></p>
                           
                            {*<p>{$item.summary}</p>*}
                         {/foreach}
                        <!-- {$ITEM.summary} -->
                        {else}
                        {$CONTENT1}
                        {/if}
                    </div>
                </div>
            </div>
            {include file="paginator.tpl"}
        </div>
    </div>
</div>

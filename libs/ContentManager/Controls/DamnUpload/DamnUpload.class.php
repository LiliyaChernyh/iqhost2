<?php
/**
 * DamnUPload
 *
 * @version 1.2
 *
 */
class DamnUpload extends Control implements IFlushingControl
{
	const DAMN_UPLOAD_FILE_TYPE_IMAGE = 'image';
	const DAMN_UPLOAD_FILE_TYPE_FILE  = 'file';

	/**
	* Максимальный размер загружаемого файла
	* @var int
	*/
	private $maxFileSize = 5048576; // 5 Mb

	/**
	* Параметры загружаемой картинки, если загружается картинка
	* @var array
	*/
	private $imageParams = null;

	/**
	 * Максимально количество загружаемых файлов
	 * @var int
	 */
	private $maxFilesNumber = null;

	/**
	 * Имя файловой таблицы
	 * @var string
	 */
	private $filesTable = null;

	/**
	 * Ключ файловой таблицы
	 * @var string
	 */
	private $filesTableKey = null;

	/**
	 * Использовать связующую таблицу или писать ID файла прямо в
	 * текущее поле.
	 * @var boolean
	 */
	private $useLinkingTable = false;

	/**
	 * Имя связующей таблицы
	 * @var string
	 */
	private $linkingTable = null;

	/**
	 * Тип загружаемых фалов
	 * Одно из значений констант SWF_UPLOAD_FILE_TYPE
	 * image | file
	 * @var string
	 */
	private $filesType = null;

	/**
	 * Разрешённые расширения
	 * @var string
	 */
	private $allowedExtensions = null;

	/**
	 * Папка назначения
	 * @var string
	 */
	private $destinationDirectory = '/uploaded/';

	/**
	* Скрипт загрузки
	* @var string
	*/
	private $uploadScript = '/upload/';

	/**
	 * Подключить диалог обрезки иконок
	 * @var bool
	 */
	private $includeThumbCropDialog = false;

	/**
	 * Добавляет текстовое поле для названия
	 *
	 * @var bool
	 */
	private $includeTitle = false;

	/**
	 * Название поля для названия в таблице LinkingTable
	 *
	 * @var unknown_type
	 */
	private $titleFieldName = 'title';

	/**
	 * Путь к скрипту обрезки иконок
	 * @var string
	 */
	private $thumbCropScript = '/admin/cropthumbnail/';

	/**
	 * Добавляет возможность менять порядок отображения картинок
	 *
	 * @var bool
	 */
	private $usePriority = false;

	/**
	 *  Добавляет css класс к блоку загружаемого объекта
	 */
	private $blockClass = null;

	/**
	 * Названия поля для приоритета в таблице LinkingTable
	 *
	 * @var unknown_type
	 */
	private $priorityFieldName = 'priority';

	/**
	 * Объект, представляющий текстовые поля изображения
	 * Формат объекта в настройках:
	 * "textFields" : {
            "_fieldName_" : {
                "label" : string,
                "showLabel" : bool,
                "styles" : string,
                "containerType": string(text,line:default),
                "containerClass": string
            }
        }
	 *
	 * @var array
	 */
	private $textFields = null;

	public function InitControl($value = null, $rowID = null)
	{
	    $this -> LoadSettings();

	    $this -> controlTplPath = parent::$CONTROLS_PATH .__CLASS__.'/control.tpl';

	    $this -> controlTplVars['NAME'] = $this -> name."_".$this->formIndex;
	    $this -> controlTplVars['FIELD_NAME'] = $this -> name;
	    $this -> controlTplVars['VALUE'] = $value;
	    $this -> controlTplVars['TIP'] = $this -> tip;
	    $this -> controlTplVars['FILE_TYPE'] = $this -> filesType;
	    $this -> controlTplVars['MAX_FILE_SIZE'] = $this -> maxFileSize;
	    $this -> controlTplVars['MAX_FILES_NUMBER'] = $this -> maxFilesNumber;
	    $this -> controlTplVars['FILE_ID_KEY'] = $this -> filesTableKey;
	    //$this -> controlTplVars['IMAGES_EXTENSIONS'] = array('.gif', '.jpeg', '.jpg', '.png');
	    $this -> controlTplVars['includeThumbCropDialog'] = $this -> includeThumbCropDialog;
	    $this -> controlTplVars['includeTitle'] = $this -> includeTitle;
	    $this -> controlTplVars['usePriority'] = $this -> usePriority;
	    if (isset($this->tableName)) $this -> controlTplVars['tableName'] = $this->tableName;
	    $this -> controlTplVars['formIndex'] = (isset($this->formIndex))? $this->formIndex: 0;

	    // Инициализирующая функция
	    $this -> controlJSFiles = array();
		    //$this -> controlJSFiles[] = AppSettings::$LIBS_ALIAS.'jQuery/'.__CLASS__.'/jquery.damnUploader.min.js';
	    $this -> controlJSFiles[] = parent::$CONTROLS_ALIAS.__CLASS__.'/init.js';

	    // Используемые JS-библиотеки
	    $this -> controlJSLibs = array();
	    $this -> controlJSLibs[] = 'DAMNUPLOAD';
	    $this -> controlJSLibs[] = 'FANCYBOX';
	    if ($this -> includeThumbCropDialog)
	    {
		$this -> controlJSLibs[] = 'JQUERY_UI_DRAGGABLE';
		$this -> controlJSLibs[] = 'JQUERY_UI_RESIZABLE';
		$this -> controlJSLibs[] = 'JQUERY_UI_DIALOG';
		$this -> controlJSLibs[] = 'JCROP';
		$this -> controlJSLibs[] = 'CROP_THUMB_DIALOG';
	    }

	    // Получим fieldRegistryID
	    if (empty($this -> fieldRegistryID))
		    throw new Exception('Field registry ID prop is undefined in Controll class. It should be assigned manually in ControlFactory after control creation');

	    $this -> controlJSVars['name'] = $this -> name;
	    $this -> controlJSVars['libsPath'] = AppSettings::$LIBS_ALIAS;
	    $this -> controlJSVars['maxFilesNumber'] = $this -> maxFilesNumber;
	    $this -> controlJSVars['allowedExtensions'] = $this -> allowedExtensions;
	    $this -> controlJSVars['uploadScript'] = $this -> uploadScript . '?frid='.$this -> fieldRegistryID;
	    $this -> controlJSVars['filesType'] = $this -> filesType;
	    $this -> controlJSVars['includeThumbCropDialog'] = $this -> includeThumbCropDialog;
	    if (isset($this->tableName)) $this -> controlJSVars['tableName'] = $this->tableName;
	    $this -> controlJSVars['formIndex'] = (isset($this->formIndex))? $this->formIndex: 0;

	    if ($this -> includeThumbCropDialog)
	    {
		$this -> controlJSVars['thumbWidth'] = $this -> imageParams['thumb']['resize']['width'];
		$this -> controlJSVars['thumbHeight'] = $this -> imageParams['thumb']['resize']['height'];
		$this -> controlJSVars['thumbCropScript'] = $this -> thumbCropScript;
	    }
	    $this -> controlJSVars['filesNumber'] = 0;
	    if (!is_null($rowID))
	    {
		if ($this -> useLinkingTable)
		{
		    $selectFields = '';
		    $orderBy = '';
		    if ($this -> includeTitle)
			$selectFields = ', lt.`'. $this -> titleFieldName .'` as title';

		    if ($this -> usePriority)
		    {
			$selectFields .= ', lt.`'. $this -> priorityFieldName .'` as priority';
			$orderBy = 'ORDER BY lt.`'. $this -> priorityFieldName .'` DESC ';
		    }

		    if (!empty($this -> textFields))
		    {
			foreach ($this -> textFields as $fieldName => $fieldInfo)
			    $selectFields .= ', lt.`'. $fieldName .'`';
		    }

		    $query = '
			SELECT  rt.* '. $selectFields .'
			FROM    `'. $this -> filesTable .'` rt
			JOIN    `'. $this -> linkingTable .'` lt
			    ON  rt.`'.$this -> filesTableKey.'` = lt.`'.$this -> filesTableKey.'`
			WHERE   lt.`'. $this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name .'` = '. $rowID .'
			'. $orderBy;

		    $savedLinks = DB::QueryToArray($query);
		    $this -> controlTplVars['FILES'] = $savedLinks;
		    $this -> controlJSVars['filesNumber'] = sizeof($savedLinks);
		}
		else
		{
		    $activeTableManager = new DBTableManager($this -> controlsRegistry -> activeTableStructure -> TableName, $this -> controlsRegistry -> activeTableStructure);
		    $current = $activeTableManager -> SelectFirst(
				array( $this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID),
				array($this -> name)
			);

		    if (!empty($current) && !empty($current[$this -> name])  && is_numeric($current[$this -> name]))
		    {
			$filesTable = new DBTableManager($this -> filesTable, null, false);
			if (empty($this -> filesTableKey))
			    throw new Exception('Files table key or fieldName is empty');
			$this -> controlTplVars['FILES'] = $filesTable -> Select(array($this -> filesTableKey => $current[$this -> name]));
			$this -> controlJSVars['filesNumber'] = $filesTable -> Count(array($this -> filesTableKey => $current[$this -> name]));
		    }
		}
	    }

	    // Добавляем текстовые поля
	    if (!empty($this -> textFields))
	    {
		$this -> controlTplVars['textFields'] = $this -> textFields;
	    }

	    if (!empty($this -> blockClass))
	    {
		    $this -> controlTplVars['blockClass'] = $this -> blockClass;
	    }
	    $this -> type = __CLASS__;
	}

	private function LoadSettings()
	{
		if (!empty($this -> controlSettings['blockClass']))
			$this -> blockClass = $this -> controlSettings['blockClass'];

		if (!empty($this -> controlSettings['maxFileSize']))
			$this -> maxFileSize = $this -> controlSettings['maxFileSize'];

		if (!empty($this -> controlSettings['maxFilesNumber']))
			$this -> maxFilesNumber = $this -> controlSettings['maxFilesNumber'];

		if ($this -> maxFilesNumber > 1)
		{
			if (!empty($this -> controlSettings['linkingTable']))
				$this -> linkingTable = $this -> controlSettings['linkingTable'];
			else
				throw new Exception('linkingTable must be assigned in fields registry if maxFileNumber > 1');

			$this -> useLinkingTable = true;
		}
		else if (!empty($this -> controlSettings['linkingTable']))
		{
			$this -> linkingTable = $this -> controlSettings['linkingTable'];
			$this -> useLinkingTable = true;
		}

		if (!empty($this -> controlSettings['filesType']))
		{
			switch ($this -> controlSettings['filesType'])
			{
				case self::DAMN_UPLOAD_FILE_TYPE_IMAGE:
					$this -> filesType = self::DAMN_UPLOAD_FILE_TYPE_IMAGE;
					break;

				case self::DAMN_UPLOAD_FILE_TYPE_FILE:
				default:
					$this -> filesType = self::DAMN_UPLOAD_FILE_TYPE_FILE;
					break;
			}
		}

		if (!empty($this -> controlSettings['allowedExtensions']))
			$this -> allowedExtensions = $this -> controlSettings['allowedExtensions'];
		elseif ($this -> filesType == self::DAMN_UPLOAD_FILE_TYPE_IMAGE)
			$this -> allowedExtensions = '*.jpg; *.jpeg; *.gif; *.png';
		else
			throw new Exception('Allowed files extensions must be assigned in fields registry');

		if (!empty($this -> controlSettings['filesTable']))
			$this -> filesTable = $this -> controlSettings['filesTable'];
		else
		    throw new Exception('Files Table must be assigned in fields registry. Files table store files paths');

		if (!empty($this -> controlSettings['filesTableKey']))
			$this -> filesTableKey = $this -> controlSettings['filesTableKey'];
		else
			throw new Exception('Files Table Key must be assigned in fields registry. Files table store files paths');

		if (!empty($this -> controlSettings['image']))
			$this -> imageParams = $this -> controlSettings['image'];

		if (!empty($this -> controlSettings['destinationDirectory']))
		{
			$this -> destinationDirectory = $this -> controlSettings['destinationDirectory'];
//			if (!is_writable(IncPaths::$ROOT_PATH . $this -> destinationDirectory))
//				throw new Exception('Directory does not exist or I have no permissions to write there '.IncPaths::$ROOT_PATH . $this -> destinationDirectory);
		}

		if (!empty($this -> controlSettings['uploadScript']))
			$this -> uploadScript = $this -> controlSettings['uploadScript'];

		if (isset($this -> controlSettings['includeThumbCropDialog']))
            $this -> includeThumbCropDialog = $this -> controlSettings['includeThumbCropDialog'];

        if (isset($this -> controlSettings['thumbCropScript']))
            $this -> thumbCropScript = $this -> controlSettings['thumbCropScript'];

		if (isset($this -> controlSettings['includeTitle']))
            $this -> includeTitle = $this -> controlSettings['includeTitle'];

        if (isset($this -> controlSettings['titleField']))
            $this -> titleFieldName = $this -> controlSettings['titleField'];

		if (isset($this -> controlSettings['usePriority']))
            $this -> usePriority = $this -> controlSettings['usePriority'];

        if (isset($this -> controlSettings['priorityField']))
            $this -> priorityFieldName = $this -> controlSettings['priorityField'];

        if (isset($this -> controlSettings['textFields']))
        {
            $this -> textFields = $this -> controlSettings['textFields'];

            foreach ($this -> textFields as $key => $value)
            {
                if (empty($this -> textFields[$key]['label']))
                    $this -> textFields[$key]['label'] = 'Label '. $key;

                if (!isset($this -> textFields[$key]['showLabel']))
                    $this -> textFields[$key]['showLabel'] = true;
            }
        }
	}

	public function Flush(&$data, $rowID)
	{
	    $this -> LoadSettings();
        $fileNames = isset($data['files']) ? $data['files'] : null;

        $activeTableManager = new DBTableManager($this -> controlsRegistry -> activeTableStructure -> TableName, $this -> controlsRegistry -> activeTableStructure);
		$filesTableManager = new DBTableManager($this -> filesTable, null, false);
		if ($this -> useLinkingTable)
			$linksTableManager = new DBTableManager($this -> linkingTable, null, true);
		$fieldName = $this -> name;

		if (!empty($fileNames))
        {
			// Форму необходимо перезагрузить
			$this -> refreshFormAfterFlush = true;
            foreach ($fileNames as $fileName)
            {
				// перемещаем файл
				// перенос картинок в тысячные папки
				//$destDir	 = str_replace('[ID]', ceil($rowID / 1000) . '/' . $rowID, $this->destinationDirectory);
				$destDir	 = str_replace('[ID]', $rowID, $this->destinationDirectory);
				$fileName	 = FileManager::MoveFile($fileName, $destDir);
				// todo сделать настройку для тысячных папок
				// Если нужно, ресайзим оригинальное изображение
                if(isset($this -> imageParams['resize']) &&
        			isset($this -> imageParams['resize']['width']) &&
        			isset($this -> imageParams['resize']['height']) &&
        			is_numeric($this -> imageParams['resize']['width']) &&
        			is_numeric($this -> imageParams['resize']['height']))
        		{
        			// Если StrongResize и если он задан
        			if (isset($this -> imageParams['resize']['strong']) && $this -> imageParams['resize']['strong'])
        			{
        				ImageManager::StrongImageResize($fileName, $this -> imageParams['resize']['width'], $this -> imageParams['resize']['height']);
//                                        Watermark::AddWatermark($fileName);
        			}
        			else // Если не задан или false, то Простой ресайз
        			{
        				ImageManager::ImageResize($fileName, $this -> imageParams['resize']['width'], $this -> imageParams['resize']['height']);
//                                        Watermark::AddWatermark($fileName);
        			}
        		}

				$newFileArray = array();

				/**
				 * @ticket svn rev #199
				 * @todo Добавить удаление файлов превьюшек при обновлении/удалении основного изображения
				 * если нужно, создаем превьюшки. Проходим по списку параметра image,
				 * отсекаем реайз и создаем по всем остальным превьюшки и сразу
				 * добавляем в массив полей для сохранения в БД.
				 */
				if (!empty($this->imageParams)){
					foreach ($this->imageParams as $key => $val){
						if ($key == 'resize') {
							$newFileArray['src'] = $fileName;
							continue;
						}
						elseif(empty($val)){
							continue;
						}
						$$key = $this->CreateThumbs($key,$fileName);

						if (!empty($val['fieldName'])){
							$newFileArray[$val['fieldName']] = $$key;
						}
						elseif ($key == 'thumb'){
							$newFileArray['srcSmall'] = $$key;
						}
					}
				}
                else
                {
                    $newFileArray['src'] = $fileName;
                }

        		// Добавляем новое изображение
        		$newFileID = $filesTableManager -> Insert($newFileArray);
        		if ($this -> useLinkingTable && ($this->linkingTable != $this -> controlsRegistry -> activeTableStructure -> TableName))
        		{
        			$link = array();
        			$link[$this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name] = $rowID;
					// Важно, чтобы в связующей таблице foreignKey файловой таблицы назывался также, как primaryKey в файловой таблице
        			$link[$this -> filesTableKey] = $newFileID;
					$linksTableManager -> Insert($link);
        		}
        		else
        		{
        			// Удалим старый файл, поскольку мы его переписываем.
        			// Сначала получим её ID
        			$current = $activeTableManager -> SelectFirst(
        				array(
			            	$this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID
						),
						array($this -> name)
					);

					// Заполним массив удаления
					if (!empty($current) && is_numeric($current[$this -> name]) && $current[$this -> name] != 0)
					{
						if (!isset($data['delete']) || empty($data['delete']))
							$data['delete'] = array();
						$data['delete'][] = $current[$this -> name];
					}

        			$activeTableManager -> Update(array($this -> name => $newFileID), $rowID);
        		}
            }
        }

        // Обновляем текстовые поля
        if (!empty($this -> textFields) && $this -> useLinkingTable)
        {
            $fileIDs = array();

            foreach ($this -> textFields as $fieldName => $fieldInfo)
            {
                if (!empty($data[$fieldName]))
                {
                    foreach ($data[$fieldName] as $fileID => $value)
                    {
                        $fileIDs[$fileID][$fieldName] = $value;
                    }
                }
            }

            foreach ($fileIDs as $fileID => $attributes)
            {
                $row = $linksTableManager -> SelectFirst(array(
					$this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID,
					$this -> filesTableKey  => $fileID
				));

                $linksTableManager -> Update(
                    $attributes,
                    $row[$linksTableManager -> TableStructure -> PrimaryKey -> Name]
                );
            }
        }

        // Обновляем title
        if ($this -> includeTitle && !empty($data['title']))
        {
            if ($this -> useLinkingTable)
			{
			    //$linksTableManager -> TableStructure = new DBTableStructure($this -> linkingTable);

			    foreach ($data['title'] as $fileID => $title)
                {
                    $row = $linksTableManager -> SelectFirst(array(
    					$this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID,
    					$this -> filesTableKey  => $fileID
    				));

                    $linksTableManager -> Update(
                        array($this -> titleFieldName => $title),
                        $row[$linksTableManager -> TableStructure -> PrimaryKey -> Name]
                    );
                }
			}
        }

        // Обновляем priority
        if ($this -> usePriority && !empty($data['priority']))
        {
            if ($this -> useLinkingTable)
			{
			    //$linksTableManager -> TableStructure = new DBTableStructure($this -> linkingTable);

			    foreach ($data['priority'] as $fileID => $priority)
                {
                    $row = $linksTableManager -> SelectFirst(array(
    					$this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID,
    					$this -> filesTableKey  => $fileID
    				));

                    $linksTableManager -> Update(
                        array($this -> priorityFieldName => $priority),
                        $row[$linksTableManager -> TableStructure -> PrimaryKey -> Name]
                    );
                }
			}
        }

	    // Удаляем старые ссылки
        if (!empty($data['delete']))
        {
            // Форму необходимо перезагрузить
			$this -> refreshFormAfterFlush = true;

            foreach ($data['delete'] as $fileID)
            {
				if ($this -> useLinkingTable && ($this->linkingTable != $this -> controlsRegistry -> activeTableStructure -> TableName))
				{
					$linksTableManager -> Delete(array(
						$this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID,
						$this -> filesTableKey  => $fileID
					));
				}

                $fileAttrs = array($this -> filesTableKey => $fileID);
                $file = $filesTableManager -> SelectFirst($fileAttrs);
                if (!empty($file))
    		    {
    		        try
    		        {
						foreach ($file as $key => $fi)
						{
							if (!preg_match("/^src/", $key)){
								continue;
							}
							FileManager::DeleteFile($file[$key]);
						}
                    }
    		        catch (Exception $e)
    		        {
    		        }
    		    }
                $filesTableManager -> Delete($fileAttrs);
            }
        }
	}

    public function PostBackHandler(&$data, $rowID = null){}

	/**
	 * Метод создания превьюшек. Имена берутся из fields_registry.
	 * medium - имя превьюшки.
	 * fieldName - имя поля в greeny_images, строится по схеме: src<Имя_превьюшки_с_большой_буквы>.
	 * пример:  имя превьюшки: medium, fieldName: srcMedium
	 *
	 * Внимание!!! fieldName ДОЛЖНО НАЧИНАТЬСЯ ТОЛЬКО с src
	 *
	 * В настройки так же можно добавлять параметры для дополнительной обработки
	 * изображений, например, grayscale или watermark
	 *
	 * 		"medium" : {
	 *			"resize" : {
	 *				"strong" : false,
	 *				"width" : 352,
	 *				"height" : 432
	 *			},
	 *			"postfix" : "_medium",
	 *			"fieldName": "srcMedium",
	 *			"grayscale" : true,
	 *			"watermark" : true
	 *		}
	 *
	 * @param string $name имя превьюшки
	 * @param string $fileName Путь и имя файла основного изображения
	 * @return string имя файла полученной превьюшки
	 */
	private function CreateThumbs($name,$fileName)
	{
		// Если нужно, создаем превьюшку
		$thumb = null;
		if (isset($this -> imageParams[$name]) &&
			isset($this -> imageParams[$name]['resize']) &&
			isset($this -> imageParams[$name]['resize']['width']) &&
			isset($this -> imageParams[$name]['resize']['height']) &&
			is_numeric($this -> imageParams[$name]['resize']['width']) &&
			is_numeric($this -> imageParams[$name]['resize']['height']))
		{
			if (isset($this -> imageParams[$name]['prefix']) && $this -> imageParams[$name]['prefix'] != null && $this -> imageParams[$name]['prefix'] != '')
			{
				$type = 'prefix';
				$arg = $this -> imageParams[$name]['prefix'];
			}
			else if (isset($this -> imageParams[$name]['postfix']) && $this -> imageParams[$name]['postfix'] != null && $this -> imageParams[$name]['postfix'] != '')
			{
				$type = 'postfix';
				$arg = $this -> imageParams[$name]['postfix'];
			}
			$thumb = ImageManager::ImageCopy($fileName, $type, $arg);

			// Если StrongResize и если он задан
			if (isset($this -> imageParams[$name]['resize']['strong']) && $this -> imageParams[$name]['resize']['strong'])
			{
				ImageManager::StrongImageResize($thumb, $this -> imageParams[$name]['resize']['width'], $this -> imageParams[$name]['resize']['height']);
			}
			else // Если не задан или false, то Простой ресайз
			{
				ImageManager::ImageResize($thumb, $this -> imageParams[$name]['resize']['width'], $this -> imageParams[$name]['resize']['height']);
			}
			// обработка параметра watermark
			if (!empty($this->imageParams[$name]['watermark'])){
				Watermark::AddWatermark($thumb);
			}
		}
		return $thumb;
	}

}

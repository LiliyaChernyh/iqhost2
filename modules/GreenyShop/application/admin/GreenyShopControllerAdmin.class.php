<?php

/**
* Администраторская часть модуля интернет-магазина
* 
* @version 2.2
*/
class GreenyShopControllerAdmin extends AdminController 
{
	/**
	 * Объект класса GreenyShop
	 *
	 * @var GreenyShop
	 */
	private $shop;
	
	/**
	 * Телега
	 *
	 * @var array
	 */
	private $cart;
	
	/**
	 * Путь к шаблонам модуля
	 *
	 * @var string
	 */
	var $moduleTplsPath;
	
	/**
	 * Шаблон с содержимым
	 *
	 * @var Template
	 */
	var $contentTpl;
	
	/**
	 * Шаблон для меню
	 *
	 * @var Template
	 */
	var $menuTpl;
        
        private $fullAliases = null;

	private $moduleAlias = null;

	private $modulePath = null;
	
	private $pulicFilesPath = null;
        
        public function __construct()
        {
            parent::__construct();
            $this -> fullAliases = Request::GetAliases();
            
            //Указываем относительный путь к админским шаблонам модуля
            $this -> moduleTplsPath = IncPaths::$MODULES_PATH.'GreenyShop/templates/';
            $this -> moduleAlias = AppSettings::$ADMIN_ALIAS.'GreenyShop/';
            $this -> modulePath = IncPaths::$MODULES_PATH.'GreenyShop/';
            $this -> publicFilesPath = AppSettings::$MODULES_ALIAS.'GreenyShop/';

            $this -> view = new GreenyShopViewAdmin();
            $this -> model = new GreenyShopModelAdmin();
            
            //экземпляр класса GreenyShop
            $this -> shop = new GreenyShop();
            
            ViewData::Assign('MODULE_TPLS_PATH', $this -> moduleTplsPath);
            ViewData::Assign('MODULE_ALIAS', $this -> moduleAlias);
            ViewData::Assign('JS_VARS', array('MODULE_ALIAS' => $this -> moduleAlias));
        }
	
	public function Index($args = null)
	{
		if (!isset($this -> fullAliases[2]))
                    header('Location: '.$this -> moduleAlias .'orders/');
		else 
                    switch ($this -> fullAliases[2])
                    {
                            case 'orders' :
                                    return $this -> ShowOrders($this -> fullAliases);
                                    break;
                            case 'groupoperation' :
                                    $this -> GroupOperation();
                                    break;
                            case 'order' :
                                    return $this -> ShowOrder($this -> fullAliases);
                                    break;
                            case 'changecustomer':
                                    $this -> ChangeCustomer();
                                    break;
                            case 'customercomplete':
                                    $this -> CustomerComplete();
                                    break;
                            default :
                                    header('Location: '.$this -> moduleAlias);
                    }
                        
                $this -> view -> Index();
                return $this -> view;
	}

	private function ShowOrders($aliases = null)
	{
		// Ищем номер страницы;
                if (isset(Request::$GET['page']) && is_numeric(Request::$GET['page'])) $pageNum = Request::$GET['page'];
                else $pageNum = 1;

		// Filters
		$filters = array();
		if (!empty($_COOKIE['SHOP_ORDERS_FILTERS']))
		    $filters = unserialize(stripcslashes($_COOKIE['SHOP_ORDERS_FILTERS']));

		if (isset($_POST['doFilter']))
		{
		    unset($filters);
                    
                    if (!empty($_POST['filter']['FIO']))
                    $filters['FIO'] = $_POST['filter']['FIO'];

                    if (!empty($_POST['filter']['statuses']))
                    $filters['statuses'] = $_POST['filter']['statuses'];

                    if (isset($_POST['filter']['creationDateLeft']))
                    {
                        $stamp = strtotime($_POST['filter']['creationDateLeft']);
                        $filters['creationDateLeft'] = $stamp > 0 ? date('Y-m-d', $stamp) : null;
                    }

                    if (isset($_POST['filter']['creationDateRight']))
                    {
                        $stamp = strtotime($_POST['filter']['creationDateRight']);
                        $filters['creationDateRight'] = $stamp > 0 ? date('Y-m-d', $stamp) : null;
                    }

		    setcookie('SHOP_ORDERS_FILTERS', serialize($filters), time() + (empty($filters) ? -3600 : 30758400), '/');
		}
                
		// Ищем по сколько показывать
		$showLim = 50;//$this -> GetValueFromAlias('show');
		if (empty($showLim)) $showLim = 50;

		$orders = $this -> shop -> GetOrders($filters, $showLim, ($pageNum-1)*$showLim);

		$statuses = $this -> shop -> GetStatuses();
	   	if (isset($filters['statuses']))
		{
			foreach ($statuses as $key => $value)
			{
				if (array_search($key, $filters['statuses']) !== false)
					$statuses[$key]['selected'] = 1;
			}
		}
		

		$orderStatus = array();
		$orderStatusDesc = array();
		$orderCreationTime = array();
		$orderLink = array();
		$orderID = array();
		$orderTip = array();

		$ordersCnt = count($orders);

		for ($i = 0; $i < $ordersCnt; $i++)
		{
			$orders[$i]['status'] = $statuses[$orders[$i]['statusID']]['name'];
			$orders[$i]['statusDesc'] = $statuses[$orders[$i]['statusID']]['description'];
			$orders[$i]['creationTime'] = date('d.m.Y H:i:s', strtotime($orders[$i]['creationTime']));
			$stamp = empty($orders[$i]['deliveryDate']) ? -1 : strtotime($orders[$i]['deliveryDate']);
			$orders[$i]['deliveryDate'] = $stamp > 0 ? date('d.m.Y', $stamp) : '';
			$orders[$i]['LINK'] = $this -> moduleAlias.'order/'.$orders[$i]['orderID'];
                        $orders[$i]['tip'] = '';
		}
                
		// Считаем заказы, делаем пагинатор
		$ordersTotal = $this -> shop -> ordersCnt;
		if ($ordersTotal > $showLim)
                    $this -> setPaginator($showLim, $ordersTotal, $pageNum);
                
                ViewData::Assign('STATUSES', $statuses);
                ViewData::Assign('FILTERS', $filters);
                ViewData::Assign('ORDERS', $orders);
                ViewData::Assign('INCLUDE_INLINE_MULTI_SELECT', 1);
                ViewData::Assign('INCLUDE_JMASK', 1);
                
                $this -> view -> Orders();
                return $this -> view;
	}
	
	private function ShowOrder($aliases = null)
	{
		//$orderID = $this -> GetValueFromAlias('order');
                if (isset($aliases[3]) and is_numeric($aliases[3]))
                    $orderID = $aliases[3];
                
		if (!empty($_POST['orderID'])) $orderID = $_POST['orderID'];
                
		if (isset($_POST['doChangeOrder']))
		{
			if (!empty($_POST['deliveryDate']))
			{
				$stamp = strtotime($_POST['deliveryDate']);
				if ($stamp > 0) 
					$_POST['deliveryDate'] =  date('d-m-Y', $stamp);
				else unset($_POST['deliveryDate']);
			}
			
			try
			{
				$this -> shop -> UpdateOrder($_POST, $orderID);

				// Обновляем корзину
				$cart = $this -> shop -> GetCart($orderID);

				//cartIDs
				for ($i = 0; $i < count($_POST['cartIDs']); $i++)
				{
					$index = $this -> FindInCart($_POST['cartIDs'][$i], $cart);
					if ($index !== false 
						&& isset($_POST['amount'.$_POST['cartIDs'][$i]]))
					{
						if ($_POST['amount'.$_POST['cartIDs'][$i]] < 1)
						{
							$this -> shop -> RemoveFromCart($_POST['cartIDs'][$i]);
						}
						elseif ($cart[$index]['amount'] != $_POST['amount'.$_POST['cartIDs'][$i]] || $cart[$index]['portions'] != $_POST['portions'.$_POST['cartIDs'][$i]])
						{
							$this -> shop -> ChangeAmount($_POST['cartIDs'][$i], $_POST['amount'.$_POST['cartIDs'][$i]]);
						}
					}
				}
			}
			catch (SQLException $e)
			{
				if (isset($_POST['async']))
				die('{"error":"'.str_replace('"', '`', $e -> getMessage()).'", "errno" : "'. ($e -> getCode) .'"}');
				$this -> Error('Возникла ошибка в запросе' , $_SERVER['HTTP_REFERER']);
			}
			if (isset($_POST['async']))
				die('{"msg":"Заказ был успешно изменён"}');
			$this -> Success('Заказ был успешно изменён', $redirect);
		}

		if (isset($_POST['doDeleteGoods']))
		{
			if (!isset($_POST['orderID']) || !isset($_POST['goodsToDelete']))
			{
				if (isset($_POST['async']))
					die('{"msg":"Ничего не выбрано"}');
				$this -> Error('Ничего не выделено', $redirect);
			}
			try 
			{
				for ($i = 0; $i < count($_POST['goodsToDelete']); $i++)
				{
					$this -> shop -> RemoveFromCart($_POST['goodsToDelete'][$i]);
				}
			}
			catch (SQLException $e) 
			{
				if (isset($_POST['async']))
					die('{"error":"Возникла ошибка во время удаления."}');
				$this -> Error('Возникла ошибка во время удаления.', $redirect);
			}
			if (isset($_POST['async']))
				die('{"msg":"Выбранные товары были удалёны."}');
			$this -> Success('Выбранные товары были удалёны.', $redirect);
		}

		$order = $this -> shop -> GetOrder($orderID);
		if (empty($order)) $this -> Error('Заказ не найден или у вас нет права на его просмотр и редактирование.', $_SERVER['HTTP_REFERER']);

		$order['creationTime'] = date('d.m.Y H:i', strtotime($order['creationTime']));
		if (!empty($order['deliveryDate'])) $order['deliveryDate'] = date('d.m.Y', strtotime($order['deliveryDate']));

		// Статусы
                $statuses = $this -> shop -> GetStatuses();
		ViewData::Assign('STATUSES', $statuses);

		// Задаём инфу о пользователе
		$usersManager = new UsersManager();	
                ViewData::Assign('CUSTOMER', $usersManager -> GetUser(array('userID' => $order['customerID'])));
                
		// Задаём корзину
		$total = $this -> FillTemplateWithCart($orderID);
		ViewData::Assign('GOODS_TOTAL_COST', $total);
                
                ViewData::Assign('ORDER', $order);
                ViewData::Assign('PUBLIC_FILES_PATH', $this -> publicFilesPath);
		ViewData::Assign('INCLUDE_AUTOCOMPLETE', 1);
		ViewData::Assign('INCLUDE_JMASK', 1);
                
                $this -> view -> Order();
                return $this -> view;
	}
        
        private function GroupOperation()
	{
		if (isset($_POST['doGroupOperation']) && !empty($_POST['groupOperation']))
		{
			switch ($_POST['groupAction']) 
			{
				case 'delete':
					$this -> shop -> DeleteOrders($_POST['groupOperation']);
					break;
				case 'changeStatus':
					if (!empty($_POST['newStatus']))
					$this -> shop -> ChangeStatus($_POST['newStatus'], $_POST['groupOperation']);
					break;
			}
		}
		header('Location: '.$this -> moduleAlias);
	}
	
	/**
	 * Смена пользователя в заказе
	 * @return 
	 */
	private function ChangeCustomer()
	{
		try 
		{
			if (!isset($_POST['orderID']) || 
			!isset($_POST['doChangeCustomer']))
				throw new Exception('Не задан ID заказа', 10241);

			if (!empty($_POST['userID']) && is_numeric($_POST['userID']))
			{
				$userID = $_POST['userID'];
			}
			else if (!empty($_POST['userFIO']) && is_numeric($_POST['userFIO']))
			{
				$usrMngr = new UsersManager();
				$usr = array_pop($usrMngr -> GetUser(array('userID' => $_POST['userFIO'])));
				if (!empty($usr))
					$userID = $usr['userID'];
				else
					throw new Exception('Пользователь с таким ID не найден', 10243);
			}
			else
				throw new Exception('Пользователь не найден', 10242);
			
				$this -> shop -> UpdateOrder(array('customerID' => $userID), $_POST['orderID']);
		}
		catch (Exception $e)
		{
			if (isset($_POST['async']))
				die('{"error":"'.str_replace('"', '`', $e -> getMessage()).'", "errno" : "'. ($e -> getCode) .'"}');
			$this -> Error($e -> getMessage(), $redirect);
		}
		if (isset($_POST['async']))
			die('{"msg":"Клиент успешно изменён."}');
		$this -> Success('Клиент изменён', $redirect);
		break;
	}
	
	/**
	 * AJAX handler for autocomplete
	 * @return 
	 */
	private function CustomerComplete()
	{
		$q = UTF8::strtolower($_POST['q']);

	    if (!$q) die();
	    if (is_numeric($q)) die();

		$usrMngr = new UsersManager();
		$users = $usrMngr -> FindUser($q);

		$cnt = count($users);
		for ($i = 0; $i < $cnt; $i++)
            echo $users[$i]['FIO'].' '.$users[$i]['login']. ' '.$users[$i]['email'].' (#'.$users[$i]['userID'].')|'.$users[$i]['userID']."\n";
		die();
	}
	
	/**
	 * Заполняет шаблон информацией о корзине заказа
	 * Возвращает суммарную сумму всех блюд
	 * @param int $orderID
	 * @param Template $this -> tpl
	 * @return int
	 */
	private function FillTemplateWithCart($orderID)
	{
		$this -> cart = $this -> shop -> GetFullCart($orderID);
		$CartCnt = count($this -> cart);

		$total = 0;

		if ($CartCnt > 0)
		{
			ViewData::Assign('DELETE_FROM_CART_ACTION', $this -> moduleAlias . 'order/remove/');

			$currentCategory = null;
			for ($i = 0; $i < $CartCnt; $i++)
			{ 
				$row = $this -> cart[$i];

				$this -> cart[$i]['summ'] = $this -> cart[$i]['amount'] * $this -> cart[$i]['price'];
				$total += $this -> cart[$i]['summ'];

				$categoryColspan = 5;
				
				// узнаём категорию
				if ($this -> cart[$i]['caption'] != $currentCategory)
				{
					$currentCategory = $this -> cart[$i]['caption'];
					$this -> cart[$i]['CATEGORY'] = $currentCategory;
				}
				else 
				{
					$this -> cart[$i]['CATEGORY'] = '';
				}
			}

			ViewData::Assign('cart', $this -> cart);
			ViewData::Assign('GOODS_TOTAL', $total);				
		}
		return $total;
	}
	
	/**
	 * Ищет есть ли в корзине товар с указанным ID
	 *
	 * @param int $itemID
	 * @param array $cart
	 * @return bool
	 */
	private function FindInCart($itemID, &$cart)
	{
		$cartCnt = count($cart);
		$found = false;
		for($i = 0; $i < $cartCnt; $i++)
		{
			if ($cart[$i]['cartID'] == $itemID)
			{
				$found = $i;
				break;
			}
		}
		return $found;
	}
}
?>
<?php

/**
 * Модель для работы с картой сайта (взято с 1с-netnord)
 *
 * @version 1.0
 */
class SitemapModel
{

    /**
     * Контруктор
     *
     * @param Page $page
     */
    public function __construct()
    {
        return 0;
    }

    public function GetPagesTree($parentID = 0, $startPriority = 0.9, $level = 0)
    {
        $parentCond = ' AND `parentID` = ' . $parentID;

        // and (pageTypeID <> 4 or pageTypeID is null)
        $query = '
            SELECT *
            FROM ' . TablesNames::$PAGE_STRUCTURE_TABLE_NAME . '
            WHERE isActive = 1 AND isDeleted = 0 ' . $parentCond . '
            ORDER BY priority DESC
        ';
        //AND (pageTypeID <> 3  or pageTypeID is null)
//		trace($query);
        $pages = DB::QueryToArray($query);

        if (empty($pages)) {
            return NULL;
        }
        else {
            $tree = array();
            $pageCount = count($pages);

            $structure = PagesStructure::getInstance();

            for ($i = 0; $i < $pageCount; $i++) {
                $priority = $startPriority;

                // Получаем полный путь
                $alias = $structure->GetFullAlias($pages[$i]['pageID']);
                if (strcasecmp($alias, '/search/') == 0 OR strcasecmp($alias, '/sitemap/') == 0)
                    continue;

                // Если страница главная, назначаем приоритет 1.0
                if (strcasecmp($alias, '/main/') == 0) {
                    $alias = '/';
                    $priority = 1.0;
                }
                
//		  $alias = '/'.$alias;
                $tree[$i]['location'] = $alias;
                $tree[$i]['priority'] = $priority; //sprintf('%.2f',$priority);
                $tree[$i]['name'] = $pages[$i]['caption'];
                $tree[$i]['description'] = $pages[$i]['description'];

                // Проверяем, содержит ли раздел подразделы
                $subPages = $this->GetPagesTree($pages[$i]['pageID'], $priority - 0.1, $level + 1);

                if (!empty($subPages)) {
                    $tree[$i]['children'] = $subPages;
                }
            }
        }

        return $tree;
    }
    
    public function GetProductList($str_in = null)
    {
        $condition = null;
        if ($str_in) $condition = 'AND p.parentID in '.$str_in;
        
        $query = '
            SELECT *
            FROM ' . TablesNames::$PAGE_STRUCTURE_TABLE_NAME . ' p
            LEFT JOIN ' . TablesNames::$PAGES_TYPE_TABLE_NAME . ' pt ON pt.`pageTypeID` = p.`pageTypeID`
            WHERE 
                p.isActive = 1 AND 
                p.isDeleted = 0 AND 
                p.pageTypeID IN (2,3,4)
                '.$condition.' 
            ORDER BY p.pageTypeID DESC
        ';
        $products = DB::QueryToArray($query);

        $structure = PagesStructure::getInstance();
        foreach ($products as $key => $prod) {
            $products[$key]['fullAlias'] = $structure->GetFullAlias($prod['pageID']);
            $products[$key]['data'] = $this->GetProductData($prod['name'], $prod['contentID']);
        }

        return $products;
    }

    private function GetProductData($tableName, $contentID)
    {
        $query = '
            SELECT
                co.name,
                co.descriptionShort,
                co.price,
                im.srcSmall,
                im.src
            FROM ' . $tableName . ' co
            LEFT OUTER JOIN greeny_images im ON co.imgFront_ID = im.imageID
            WHERE co.nns_ID = ' . $contentID . '
        ';
        $data = DB::QueryToArray($query);
        if (sizeof($data) > 0) {
            $data = $data[0];
        }
        return $data;
    }

    public function GetCategoriesList($parentID = 0,$level = 0)
    {
        $levelCond = ($level == 0)?" AND `pageID` IN (2,3,16)":' AND p.isActive = 1 AND pageTypeID NOT IN (2,3,4)';
        $parentCond = ' AND `parentID` = ' . $parentID;
        $query = '
            SELECT *
            FROM ' . TablesNames::$PAGE_STRUCTURE_TABLE_NAME . ' p
            WHERE p.isDeleted = 0 ' . $parentCond . $levelCond .'
            ORDER BY p.pageTypeID, p.parentID DESC
        ';
        $pages = DB::QueryToArray($query);
        if (empty($pages)) {
            return NULL;
        }

        $tree = array();
        $pageCount = count($pages);
        for ($i = 0; $i < $pageCount; $i++) {
            $tree[$i]['pageID'] = $pages[$i]['pageID'];
            $tree[$i]['parentID'] = $pages[$i]['parentID'];
            $tree[$i]['name'] = $pages[$i]['caption'];

            // Проверяем, содержит ли раздел подразделы
            $subPages = $this->GetCategoriesList($pages[$i]['pageID'],++$level);

            if (!empty($subPages)) {
                $tree[$i]['children'] = $subPages;
            }
        }
        return $tree;
    }

    public function LinearizeTree($tree, $level = 0)
    {
        $options = array();
        $space = str_repeat('&nbsp;&nbsp;&nbsp;', $level);
        $cnt = count($tree);
        for ($i = 0; $i < $cnt; $i++)
        {
            $options[] = array(
                'pageID' => $tree[$i]['pageID'],
                'parentID' => $tree[$i]['parentID'],
                'name'  => $tree[$i]['name']
            );

            if (isset($tree[$i]['children']))
            	$options = array_merge($options, $this -> LinearizeTree($tree[$i]['children'], $level + 1));
        }

        return $options;
    }
}
<?php

class NewsController extends FrontController {

   
    public function __construct() {
        parent::__construct();
        $this->view = new NewsView();
        $this->model = new NewsModel();
    }


    public function Index($args = null)
    {
        $alias = Request::GetAliases();
        $this->page = new Page($alias);
        $news = $this->model->GetNews($this->page->pageID);   
        $content = $this->page->Content();
//      trace($actions);
        ViewData::Assign('NEWS', $news);
        return $this->view->Index();
    }
    
     public function Item($args = null)
    {
        $this->page = new Page(Request::GetAliases());
        $content = $this->page->Content();             
//      trace($content);
        ViewData::Assign('ITEM', $content);
        return $this->view->Item();
    }

     
    

}

<div class="group-info">
	{if !empty($news.src)}
		<figure class="baner-hold">
			<img src="{$news.src}" alt="image" width="480" height="320" />
		</figure>
	{/if}
	{$news.body}
</div>
{$news.publicationDate|date_format:"%e %m %Y":"":"rus"}
<br/><br/>
{if !empty($news.gallery)}
	<div class="block-hold">
		<h2>ФОТО</h2>
		<ul class="photo">
			{foreach from=$news.gallery item=photo}
				<li>
					<a data-rel="1" href="{$photo.src}" class="link-photo fancybox">
						<span class="text-center">
							<span class="text-inner">
								<span class="text-last">
									<img src="{$photo.srcSmall}" alt="image" width="90" height="59" />
								</span>
							</span>
						</span>
					</a>
				</li>
			{/foreach}
			<li class="last"></li>
		</ul>
	</div>
{/if}

<a href="/{$ROOT_ALIAS}/{if $news.alias !== null}{$news.alias}/{/if}">Вернуться в раздел</a>


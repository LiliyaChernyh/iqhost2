<?php
/**
 * Названия таблиц в базе данных
 * Статический класс
 *
 *
 */
class TablesNames
{
    // ----- Служебные таблицы, необходимые в любой CMS ----- //
	/**
	* Таблица со структурой страниц
	*/
    public static $PAGE_STRUCTURE_TABLE_NAME = 'greeny_pageStructure';

	/**
	*  Контент-страницы
	*/
    public static $SIMPLE_PAGES_TABLE_NAME = 'simplePages';

	/**
	*	Типы страниц
	*/
    public static $PAGES_TYPE_TABLE_NAME = 'greeny_pageTypes';

    /**
     * Пользователи с основной информацией
     */
    public static $USERS_TABLE_NAME = 'Users';

    /**
     * Подробная информация о пользователях
     */
    public static $USERS_INFO_TABLE_NAME = 'UsersInfo';

    /**
     * Коды для активации учетных записей
     */
    public static $USERS_IDENTITY_TABLE_NAME = 'UsersIdentity';

    /**
     * Роли пользователей
     */
    public static $ROLES_TABLE_NAME = 'Roles';

    /**
     * Модули
     */
    public static $MODULES_TABLE_NAME = 'greeny_modules';

    /**
     * Настройки модулей
     */
    public static $MODULES_SETTINGS_TABLE_NAME = 'greeny_modulesSettings';

    /**
     * Настройки системы
     */
    public static $SETTINGS_TABLE_NAME = 'greeny_settings';

    /**
     * Группы настроек
     */
    public static $SETTINGS_GROUPS_TABLE_NAME = 'greeny_settingsGroups';

    /**
     * Реестр полей для форм-генератора
     */
    public static $FIELDS_REGISRTY_TABLE_NAME = 'greeny_fieldsRegistry';

    /**
     *  Таблица с картинками
     */
    public static $IMAGES_TABLE_NAME = 'greeny_images';

	/**
	*  Таблица с файлами
	*/
    public static $FILES_TABLE_NAME = 'greeny_files';

    public static $CONTROLS_TABLE_NAME = 'Controls';

    public static $CONTROLS_SETTINGS_TABLE_NAME = 'ControlsSettings';

    /**
     * Таблица логов
     */
    public static $LOG_TABLE_NAME = 'greeny_log';

	/**
	 * Таблица маршрутов
	 */
	public static $ROUTING_TABLE_NAME = 'greeny_routes';

	// ----- Специфические таблицы проекта ----- //
}
?>
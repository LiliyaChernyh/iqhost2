<div class="top-section">
    {if !empty($HOSTING)}
        <div class="container">
            <div class="twocolumns">
                <div class="text-holder">
                    <h1>{$PAGE.caption}</h1>
                    {$CONTENT.pageContent}
                </div>
                <div class="slick-slider">
                    {assign var='foo_start' value=0}
                    {foreach from=$HOSTING item=hosting key=key}
                        <div>
                            <form  class="hold-form" action="#">
                                <fieldset class="">
                                    <div class="info-holder">
                                        <div class="heading">
                                            <h2>{$hosting.title}</h2>
                                            <dl>
                                                <dt>Рекомендованная нагрузка:</dt>
                                                <dd>
                                                    <img src="{$IMAGES_ALIAS}/ico11.png" alt="image description" width="57" height="42">
                                                    <span class="text">
                                                        <strong>{$hosting.load}</strong>
                                                        <span>посетителей / сутки</span>
                                                    </span>
                                                </dd>
                                            </dl>
                                        </div>
                                        <ul class="price-list">
                                            <li class="active">
                                                <input type="radio" class="customRadio" name="radio{$key}" id="rad{$foo_start}" data-href="{$hosting.linkM}" value="0"  />
                                                <label for="rad{$foo_start}"></label>
                                                <span class="price"><span class="num">{$hosting.priceM}</span> / месяц</span>
                                            </li>
                                            <li>
                                                <input type="radio" checked="checked" class="customRadio" name="radio{$key}" id="rad{$foo_start+1}" data-href="{$hosting.linkK}" value="1" />
                                                <label for="rad{$foo_start+1}"></label>
                                                <span class="price"><span class="num">{$hosting.priceK}</span> / квартал</span>
                                                <span class="comment">c выгодой <span>{$hosting.percentK}%</span></span>
                                            </li>
                                            <li>
                                                <input type="radio" class="customRadio" name="radio{$key}" id="rad{$foo_start+2}" data-href="{$hosting.linkP}" value="2"  />
                                                <label for="rad{$foo_start+2}"></label>
                                                <span class="price"><span class="num">{$hosting.priceP}</span> / полугодие</span>
                                                <span class="comment">c выгодой <span>{$hosting.percentP}%</span></span>
                                            </li>
                                            <li>
                                                <input type="radio" class="customRadio" name="radio{$key}" id="rad{$foo_start+3}" data-href="{$hosting.linkY}" value="3" />
                                                <label for="rad{$foo_start+3}"></label>
                                                <span class="price"><span class="num">{$hosting.priceY}</span> / год</span>
                                                <span class="comment">c выгодой <span>{$hosting.percentY}%</span></span>
                                            </li>
                                        </ul>
                                        <div class="threecolumns">
                                            <div class="column">
                                                {$hosting.textWhite}
                                            </div>
                                            {if !empty($hosting.gallery)}
                                                <div class="column">
                                                    <ul class="description-list">
                                                        {foreach from=$hosting.gallery item=option}
                                                            <li>
                                                                <span class="ico-holder"><img src="/getimage/{$option.imageID}/" alt="{$option.title}"></span>
                                                                <span class="text">
                                                                    <strong>{$option.value}</strong>
                                                                    <span>{$option.title}</span>
                                                                </span>
                                                            </li>
                                                        {/foreach}                                   
                                                    </ul>
                                                </div>
                                            {/if}
                                            <div class="column">
                                                <a href="" class="btn" id="shopCart">Заказать</a>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        {assign var=foo_start value=$foo_start+4}
                    {/foreach}
                </div>
            </div>
            <div class="slider-nav">
                {foreach from=$HOSTING item=hosting}
                    <div>
                        <div class="block">
                            <div class="heading">
                                <h3>{$hosting.title}</h3>
                                <span class="price">{$hosting.priceM}</span>
                            </div>
                            {$hosting.summary}
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    {/if}
</div>
{include file="include-area.tpl"}
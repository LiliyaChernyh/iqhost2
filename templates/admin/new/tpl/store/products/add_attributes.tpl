    {if !empty($ATTRIBUTES)}
        <table class="highlightable" id="faqTable">
            <thead>
                <tr>
                    <th colspan="2">№</th>
                    <th>Название</th>
                    <th colspan="2">Значение</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$ATTRIBUTES key=key item=field}
                <tr>
                	<td class="hov">&nbsp;</td>
                    <td style="width: 20px;">{$key+1}<input type="hidden" name="attributes[{$field.attributeID}][productAttributeID]" value="{$field.productAttributeID}"></td>
                    <td>
                        {$field.attributeName}
                    </td>
                    <td class="enableValue" align="right">
                        <input type="text" name="attributes[{$field.attributeID}][productAttributeValue]" value="{$field.productAttributeValue}">
                    </td>
                    <td align="left">
                        {$field.attributeMetric}
                    </td>
                </tr>
                {/foreach}
            </tbody>
        </table>
    {else}
	<div class="formBlock">
		<div class="error">
			Дла выбранного типа товаров атрибутов не найдено
		</div>
	</div>
    {/if}

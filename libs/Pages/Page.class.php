<?php

/**
 * Объект страницы
 *
 * @version 1.3
 */
class Page
{
	public static $ADD_ENGLISH_ATTRS = false;
    /**
     * Получает страницу из базы, соответствующую переданным параметрам.
     * Можно передать параметры страницы и false во втором параметре,
     * чтобы не брать страницу из базы
     *
     * @param int $params
     * @param bool $getPageFromDB
     * @return Page
     */
    public function __construct($params = null, $getPageFromDB = true, $throwExeptionIfNotFound = true)
    {
        // Инициализируем менеджер таблицы со страницами
        $this -> pagesTableManager = DBTableManager::getInstance(TablesNames::$PAGE_STRUCTURE_TABLE_NAME);
        $this -> pagesTableManager -> SortOrder = '`priority` DESC';

        // Если не нужно брать страницу из базы, инициалзируем ее переданными параметрами
        if (!$getPageFromDB)
            return $this -> Init($params);

        // Если заданы параметры, пытаемся найти нужную страницу
        if (!is_null($params))
        {
            // Передан массив - либо параметры страницы, либо массив алиасов
            if (is_array($params))
            {
                // Числовой массив, значит - массив алиасов
                if (isset($params[0]))
                {
                    $page = $this -> GetPageByFullAlias($params);
                }
                // Ассоциативный массив, значит - массив с параметрами страницы
                else
                {
                    $page = $this -> pagesTableManager -> SelectFirst($params, 1);
                }
            }
            // Передан ID страницы
            elseif (is_numeric($params))
            {
                $page = $this -> pagesTableManager -> SelectFirst(array('pageID' => $params), 1);
            }
            // Передан полный алиас страницы
            elseif (is_string($params))
            {
                $page = $this -> GetPageByFullAlias($params);
            }

            // Страница не найдена, генерим исключение
            if (empty($page))
            {
                if ($throwExeptionIfNotFound)
                    throw new PageNotFoundException('Страница с указанными параметрами не существует');
                else
                    return;
            }
            return $this -> Init($page);
        }
    }

    /**
     * Таблица со страницами
     *
     * @var DBTableManager
     */
    public $pagesTableManager = null;

    /**
     * Все параметры страницы
     *
     * @var array
     */
    public $pageAttrs = null;

    /**
     * Идентификатор страницы
     *
     * @var int
     */
    public $pageID = null;

    /**
     * Идентификатор родительской страницы
     *
     * @var int
     */
    public $parentID = null;

    /**
     * Название страницы
     *
     * @var string
     */
    public $caption = null;

    /**
     * Идентификатор типа страницы
     *
     * @var int
     */
    public $pageTypeID = null;

    /**
     * Идентификатор контента
     *
     * @var int
     */
    public $contentID = null;

    /**
     * Заголовок
     *
     * @var string
     */
    public $title = null;

    /**
     * Описание
     *
     * @var string
     */
    public $description = null;

    /**
     * Ключевые слова
     *
     * @var string
     */
    public $keywords = null;
    public $fullAlias    = null;

	/**
     * Название страницы en
     *
     * @var string
     */
    public $caption_en = null;

    /**
     * Заголовок
     *
     * @var string
     */
    public $title_en = null;

    /**
     * Описание
     *
     * @var string
     */
    public $description_en = null;

    /**
     * Ключевые слова
     *
     * @var string
     */
    public $keywords_en = null;

    /**
     * Приоритет
     *
     * @var int
     */
    public $priority = null;

    /**
     * Алиас
     *
     * @var string
     */
    public $alias = null;

    /**
     * Флаг активности
     *
     * @var bool
     */
    public $isActive = null;
    
    /**
     * Авторизация для страницы
     *
     * @var bool
     */
    public $isLogin = null;
    
    /**
     * Страница noindex
     *
     * @var bool
     */
    public $isNoindex = null;

    /**
     * Флаг удаленности
     *
     * @var bool
     */
    public $isDeleted = null;

    /**
     * Флаг секции
     *
     * @var bool
     */
    public $isSection = null;

    /**
     * Массив контента
     *
     * @var array
     */
    public $content = null;

    /**
    *  Флаг инициализации
    */
    private $isInitialized = false;

    /**
     * Инициализирует класс переданными параметрами
     *
     * @param array $pageAttrs
     * @return Page
     */
    public function Init($pageAttrs)
    {
        $this -> pageAttrs = array();

        // Записываем атрибуты страницы в соответствующие поля объекта
        foreach ($pageAttrs as $attribute => $value)
        {
            // Смотрим, существует ли такое поле. Если нет, игнорируем его
            if (property_exists($this, $attribute))
            {
                // Проверяем значение поля на корректность.
                if (!$this -> Validate($attribute, $value))
                    throw new ArgumentException('Неверное значение параметра: '. $attribute .' = '. $value);

                // Записываем значение в соответствующее поле
                $this -> $attribute = $value;

                // и в общий массив pageAttrs
                $this -> pageAttrs[$attribute] = $value;
            }
        }

        $this -> isInitialized = true;
        return $this;
    }

    public function IsInitialized()
    {
        return $this -> isInitialized;
    }

    /**
     * Заменяет атрибуты страницы на переданные
     *
     * @param array $pageAttrs
     * @return Page
     */
    public function Merge($pageAttrs)
    {
        // Записываем атрибуты страницы в соответствующие поля объекта
        foreach ($pageAttrs as $attribute => $value)
        {
            // Смотрим, существует ли такое поле. Если нет, игнорируем его
            if (property_exists($this, $attribute))
            {
                // Проверяем значение поля на корректность. Если не корректно, пропускаем
                if (!$this -> Validate($attribute, $value))
                    continue;

                // Записываем значение в соответствующее поле
                $this -> $attribute = $value;

                // и в общий массив pageAttrs
                $this -> pageAttrs[$attribute] = $value;
            }
        }

        return $this;
    }
    
    
    /**
     * Получит уровень вложенности данной страницы (отностельно корня сайта)
     * Подразумеваемтся что корень обладает нулемы уровнем, а адрес вроде example.com/test или example.com/test/ - 1-ым и т.д.
     * 
     * @return int
     */
    public static function getLevel()
    {
        return count(Request::GetAliases());
    }
    
    
    /**
     * Получить путь (ссылку) к родительскому разделу данной страницы (фрагмент URL)
     * 
     * @return string  вернёт строку вида /path/to/parent
     */
    public static function getParentPath()
    {
        $aliases = Request::GetAliases();
        
        unset($aliases[count($aliases) - 1]); // удаляем последний фрагмент (собственно данной страницы)
        
        $path = '/' . implode('/', $aliases); 
        
        return $path;
    }
    
    /**
     * Получить путь (ссылку) к данной странице (фрагмент URL)
     * 
     * @return string  вернёт строку вида /path/to/this
     */
    public static function getPath()
    {
        $aliases = Request::GetAliases();
        
        $path = '/' . implode('/', $aliases); 
        
        return $path;
    }   
    
    /**
     * Получит фрагмент пути (из запрашиваемого URL), состояний из $sectionsCount секций, начиная с корня
     * 
     * @param int $sectionsCount число секций
     * @return string
     */
    public static function getPathLimited($sectionsCount)
    {
        $aliases = Request::GetAliasesLimited($sectionsCount);
        
        $path = '/' . implode('/', $aliases); 
        
        return $path;
    }

    /**
     * Сохраняет параметры страницы
     * Если задан pageID, то обновятся параметры страницы с этим ID.
     * Если не задан, создастся новая страница
     *
     * @return int
     * @todo По хорошему, при создании новой страницы надо проверять алиас. Если такой уже есть, то как-то модифицировать его.
     */
    public function Save()
    {
        $this -> ToArray(true);

    	// Чтобы не возникала SQL ошибка
    	if ($this -> pageAttrs['pageTypeID'] == 0)
    		unset($this -> pageAttrs['pageTypeID']);

        if (empty($this -> pageID))
        {
            $this -> pageID = $this -> pagesTableManager -> Insert($this -> pageAttrs);
            return $this -> pageID;
        }
        else
        {
            return $this -> pagesTableManager -> Update($this -> pageAttrs, $this -> pageID);
        }
    }

    /**
     * Возвращает контент страницы
     *
     * @return array
     */
    public function Content()
    {
        if (empty($this -> content))
        {
            if (empty($this -> contentID))
                return null;

            // Возвращаем пустое значение, если не задан pageTypeID
            if (empty($this -> pageTypeID))
                return null;

            // Тут смотрим pageTypeID, находим нужную таблицу, вытаскиваем из нее данные и записываем из в переменную $this -> content

            // Менеджер таблицы с типами страниц
            $pagesTypesTableManager = DBTableManager::getInstance(TablesNames::$PAGES_TYPE_TABLE_NAME);

            // Получаем данные о текущем типе
            $pageType = $pagesTypesTableManager -> SelectFirst(array('pageTypeID' => $this -> pageTypeID));

            // Такого типа нет, возвращаем пустое значение
            if (empty($pageType))
                return null;

            // Если данные хранятся не в таблицу, возвращаем пустое значение
            if ($pageType['handlerType'] != 'table')
                return;

            // Вытаскиваем название таблицы
            $tableName = $pageType['name'];

            // Создаем менеджер данной таблицы
            $contentTableManager = DBTableManager::getInstance($tableName);

            // Получаем контент
            $this -> content = $contentTableManager -> SelectFirst(array($contentTableManager -> TableStructure -> PrimaryKey -> Name => $this -> contentID));
        }

        return $this -> content;
    }

    /**
     * Возвращает полный алиас страницы
     *
     * @return string
     */
    public function FullAlias()
    {
        // Берем идентификатор родительской страницы
		$parentID = $this -> parentID;

		// Строим полный алиас
		$fullAlias = '/'. $this -> alias .'/';

		// Идем вверх по дереву страниц по корня
		while ($parentID != 0)
		{
			$parentPage = $this -> pagesTableManager -> SelectFirst(array('pageID' => $parentID));
			$fullAlias = '/'. $parentPage['alias'] . $fullAlias;
			$parentID = $parentPage['parentID'];
		}

		return $fullAlias;
    }

    /**
     * Возвращает массив с пустыми значениями всех параметров страницы
     *
     * @return array
     */
    public static function EmptyArray()
    {
        $res = array(
            'pageID'        => 0,
            'parentID'      => 0,
            'caption'       => '',
            'pageTypeID'    => 0,
            'contentID'     => 0,
            'title'         => '',
            'description'   => '',
            'keywords'      => '',
            'priority'      => 0,
            'alias'         => '',
            'isActive'      => 1,
            'isLogin'       => 0,
            'isDeleted'     => 0,
            'isSection'     => 0,
            'isNoindex'     => 0,
'fullAlias'    => ''
        );

		if (self::$ADD_ENGLISH_ATTRS)
		{
			$res['caption_en'] = '';
			$res['title_en'] = '';
			$res['description_en'] = '';
			$res['keywords_en'] = '';
		}

		return $res;
    }

    /**
     * Ищет страницу по ее полному алиасу.
     * Полный алиас можно задавать как строкой, так и массивом алиасов.
     *
     * @param string|array $fullAlias
     * @return array
     */
    protected function GetPageByFullAlias($fullAlias)
    {
        // Нам нужен массив алиасов. Если передана строка, разбиваем на массив
	    if (is_array($fullAlias))
            $aliases = $fullAlias;
	    elseif (is_string($fullAlias))
	    {
    		// Обрезаем слешы по краям
    		$fullAliasNoSlash = trim($fullAlias, '/');

    		// Разбиваем на массив алиасов
    		$aliases = explode('/', $fullAliasNoSlash);
	    }
	    else
	       throw new ArgumentException('$fullPageAlias Должен быть строкой или массивом', 4000);

		$page = array();
		$parentID = 0;
		$cnt = count($aliases);
		for ($i = 0; $i < $cnt; $i++)
		{
		    $result = $this -> pagesTableManager -> Select(array('parentID' => $parentID, 'alias' => $aliases[$i], 'isDeleted' => 0, 'isActive' => 1));

		    if (empty($result))
                return null;

		    $page = array_shift($result);

            $parentID = $page['pageID'];
		}

		return $page;
    }

    /**
     * Проверяет значение атрибута на корректность
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    protected function Validate($attribute, &$value)
    {
        switch ($attribute)
        {
            case 'pageID':
                return is_numeric($value) && $value > 0;
                break;

            case 'parentID':
            case 'pageTypeID':
            case 'contentID':
                return (is_numeric($value) && ($value >= 0)) || $value == null;
                break;

            case 'priority':
                return is_numeric($value);
                break;

            case 'alias':
                $value = UTF8::trim($value);
                return preg_match('/[A-z0-9_]/', $value) != 0;
                break;

            case 'isActive':
            case 'isDeleted':
            case 'isSection':
                return is_bool($value) || (is_numeric($value) && ($value == 1 || $value == 0));
                break;

            default:
                return true;
                break;
        }
    }

    /**
     * Возвращает объект страницы на текущий момент,
     * отражающий все изменения
     *
     * @param bool $updatePageAttrs
     * @return array
     */
    public function ToArray($updatePageAttrs = false)
    {
    	$res = array(
    	    'pageID'        => $this -> pageID,
            'parentID'      => $this -> parentID,
            'caption'       => $this -> caption,
            'pageTypeID'    => $this -> pageTypeID,
            'contentID'     => $this -> contentID,
            'title'         => $this -> title,
            'description'   => $this -> description,
            'keywords'      => $this -> keywords,
            'priority'      => $this -> priority,
            'alias'         => $this -> alias,
            'isLogin'       => $this -> isLogin,
            'isNoindex'     => $this -> isNoindex,
            'isActive'      => $this -> isActive,
            'isDeleted'     => $this -> isDeleted,
            'isSection'     => $this -> isSection,
'fullAlias'    => $this->fullAlias
    	);

		if (self::$ADD_ENGLISH_ATTRS)
		{
			$res['caption_en'] = $this -> caption_en;
			$res['title_en'] = $this -> title_en;
			$res['description_en'] = $this -> description_en;
			$res['keywords_en'] = $this -> keywords_en;
		}

    	if ($updatePageAttrs)
    		$this -> pageAttrs = $res;
    	return $res;
    }
}

?>
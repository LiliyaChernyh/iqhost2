<h1>Управление импортом товаров</h1>
<h1>Импорт</h1>
{if isset($step)}
{else}
<p>На данной странице Вы можете импортировать товары из xml файла.</p>
<p>Для импорта выберите файл xml формата и нажмите кнопку "Импорт".</p>

<form id="excelUpload" action="/admin/store/import/" method="post" enctype="multipart/form-data">
    <select name="importType" id="importType" style="width: 179px;">
        {foreach from=$STORES_LIST item=store}
        <option value="{$store.importStore}" {if !empty($storeID) AND $store.importID == $storeID}selected="selected"{/if}>{$store.importName}</option>
        {/foreach}
    </select><br /><br />
	<select name="isPart" id="isPart" style="">
		<option value="0">Не является частью</option>
		{foreach from=$FILES item=file}
			<option value="{$file.importFileID}">{$file.fileName}</option>
		{/foreach}
	</select>
	{*<input type="file" name="importFile" size="50" />*}
    <br />
    <label for="fileUrl">URL файла (протокол указывать обязательно)</label>
    <br />
    <input type="text" name="fileUrl" id="fileUrl" size="60" />
    <br /><br />
	<input type="submit" name="importRun" value="Импорт" />
</form>
{/if}

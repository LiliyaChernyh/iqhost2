<?php

/**
 * Класс поля таблицы
 * 
 * @version 1.1
 *
 */
class DBFieldStructure
{
	/**
	 * Имя поля
	 *
	 * @var string
	 */
	public $Name;

	/**
	 * Тип поля
	 *
	 * @var string
	 */
	public $Type;

	/**
	 * Длина поля
	 *
	 * @var int
	 */
	public $Length;

	/**
	 * Является ли поле вторичным ключом
	 *
	 * @var bool
	 */
	public $IsReference;

	/**
	 * Является ли поле первичным ключом.
	 *
	 * @var bool
	 */
	public $IsPrimaryKey;
	
	/**
     * Является ли поле уникальным ключом.
     *
     * @var bool
     */
    public $IsUnique;

	/**
	 * Допустимо ли NULL значение.
	 *
	 * @var bool
	 */
	public $IsNotNull;

	/**
	 * Сслыка на связную таблицу
	 *
	 * @var DBTableExtendedStructure
	 */
	public $ReferentTable;
	
	/**
	 * Конструктор.
	 *
	 * @param string $_name
	 * @param string $_type
	 * @param int $_length
	 * @param bool $_isReference
	 * @param bool $_isPrimary
	 * @param bool $_isNotNull
	 * @param DBTableExtendedStructure $_refferentTable
	 * @return DBField
	 */
	function DBFieldStructure(
	/**
	 * Название поля
	 */
	$_name, 
	/**
	 * Тип поля
	 */
	$_type, 
	/**
	 * Длина
	 */
	$_length, 
	/**
	 * Является ли ссылкой на таблицу?
	 */
	$_isReference, 
	/**
	 * Является ли первичным ключем?
	 */
	$_isPrimary, 
	/**
	 * Может ли равняться null?
	 */
	$_isNotNull, 
	/**
	 * Ссылка на таблицу
	 */
	$_refferentTable = null)
	{
		$this -> Name = $_name;
		$this -> Type = $_type;
		$this -> Length = $_length;
		$this -> IsReference = $_isReference;
		$this -> ReferentTable = $_refferentTable;
		$this -> IsPrimaryKey = $_isPrimary;
		$this -> IsNotNull = $_isNotNull;
	}
	
	public function ToString() 
	{
		$res = "".$this -> Name ." ".$this -> Type." ".$this -> Length . ($this -> IsNotNull ? " is not NULL" : "").($this -> IsPrimaryKey ? " Primary key " : "") . ($this -> IsReference ? " Reference: ":"");
		if (($this -> IsReference)&&($this -> ReferentTable != null)) 
		{
			$res .= $this -> ReferentTable -> ToString();
		}
		return $res;
	}
}
?>
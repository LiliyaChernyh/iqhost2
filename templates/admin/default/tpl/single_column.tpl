{include file='top.tpl'}

<div id="container">
	{include file='header.tpl'}

	{include file='navigation.tpl'}

	<br class="clear" />

	<div id="contentForSingleColumnPage">
		{if $CONTENT_TPL}{include file="$CONTENT_TPL"}{/if}
	</div>
</div>

{include file='footer.tpl'}
</body>
</html>
<h1>Список заказов</h1>
<div id="filters">
	<form method="post" action="{$MODULE_ALIAS}orders" id="filtersForm">
		<div class="filtersFormControl">
			<label for="filter_FIO">ФИО клиента:</label>
			<input type="text" name="filter[FIO]" id="filter_FIO" value="{if isset($FILTERS.FIO)}{$FILTERS.FIO}{/if}" title="Часть фамилии имени или отчества.&lt;br/&gt;Введите больше 3-х символов." />
		</div>
		<div class="filtersFormControl">
			<label for="filter_creationDateLeft">Дата создания от:</label>
			<input type="text" name="filter[creationDateLeft]" id="filter_creationDateLeft" value="{if isset($FILTERS.creationDateLeft)}{$FILTERS.creationDateLeft}{/if}" title="Начальная дата диапазона" style="width:75px" />
			<label for="filter_creationDateRight">до:</label>
			<input type="text" name="filter[creationDateRight]" id="filter_creationDateRight" value="{if isset($FILTERS.creationDateRight)}{$FILTERS.creationDateRight}{/if}" title="Конечная дата диапазона"  style="width:75px" />
		</div>
		<div class="filtersFormControl">
			<label for="filter_statuses">Статусы:</label>
			<select name="filter[statuses][]" id="filter_statuses"  multiple="multiple">
				{foreach from=$STATUSES item=STATUS}
				<option value="{$STATUS.statusID}" {if isset($STATUS.selected)} selected="selected"{/if}>{$STATUS.description}</option>
				{/foreach}
			</select>
		</div>
		<div style="float:left"><input type="submit" name="doFilter" value=" &gt;&gt;&gt; " id="doFilter" title="Отфильтровать"/></div>		
	</form>	
</div>

<div id="clearFilters"><a href="#">[ Снять фильтры ]</a></div>

{if $ORDERS}
	{if isset($PAGINATOR)}
	<div class="paginator">
		<span>{$PAGINATOR.itemsOnPage}</span> 
		<a href="{$PAGINATOR.firstLink}">1</a>  
		<a href="{$PAGINATOR.secondLink}">2</a> 
		<span>{$PAGINATOR.current}</span>/<span>{$PAGINATOR.total}</span>
		{if $PAGINATOR.nextLink} <a href="{$PAGINATOR.nextLink}">След</a>{/if}
	</div>
	{/if}
	<div style="clear:left;"></div>

	<form action="{$MODULE_ALIAS}groupoperation" method="post">
	<table id="orders" class="highlightable">
	<tr>
		<th style="width:3%">Номер заказа</th>
		<th style="width:15%">Дата создания</th>
		<th style="width:5%">Дата доставки</th>
		<th style="width:25%">Статус</th>
		<th style="width:25%">Покупатель</th>
		<th style="width:25%">Метка</th>
		<th style="width:1%" title="Групповые операции"></th>
		<th style="width:1%" title="Откть в новом окне"><img src="/images/icons/bullet_go.png" alt="Открыть в новом окне"/></th>
	</tr>
	{foreach from=$ORDERS item=ORDER}
	<tr class="{$ORDER.status}">
		<td><a href="{$ORDER.LINK}">{$ORDER.orderID}</a></td>
		<td>{$ORDER.creationTime}</td>
		<td>{$ORDER.deliveryDate}</td>
		<td>{$ORDER.statusDesc}</td>
		<td>{$ORDER.FIO}</td>
		<td>{$ORDER.tip}</td>
		<td><input type="checkbox" name="groupOperation[]" value="{$ORDER.orderID}" /></td>
		<td><a href="{$ORDER.LINK}" class="openInNewWin" title="Открыть в новом окне"><img src="/images/icons/bullet_go.png" alt="Открыть в новом окне"  title="Открыть в новом окне" /></a></td>
	</tr>
	{/foreach}
	</table>

	<div style="margin-top: 20px; float:right;">
		<a href="#" id="selectAll">Выделить всё</a> |
		<a href="#" id="deselectAll">Снять выделение</a> |
		<a href="#" id="invertSelection">Инвертировать выделение</a>
	</div>
	
	
	{if isset($PAGINATOR)}
	<div class="paginator">
		<span>{$PAGINATOR.itemsOnPage}</span> 
		<a href="{$PAGINATOR.firstLink}">1</a>  
		<a href="{$PAGINATOR.secondLink}">2</a> 
		<span>{$PAGINATOR.current}</span>/<span>{$PAGINATOR.total}</span>
		{if $PAGINATOR.nextLink} <a href="{$PAGINATOR.nextLink}">След</a>{/if}
	</div>
	{/if}
	
	<div style="clear:left; margin-top:30px; float:left;">
		<label for="withSelected">Отмеченные заказы:</label>
		<select id="withSelected" name="groupAction">
			<option value=""> - </option>
			<option value="changeStatus">Изменить статус</option>
			<option value="delete">Удалить</option>
		</select>
		<select id="changeStatus" style="display:none" name="newStatus">
			{foreach from=$STATUSES item=STATUS}
			<option value="{$STATUS.statusID}">{$STATUS.description}</option>
			{/foreach}
		</select>
		<input type="submit" name="doGroupOperation" value="Вперёд" />
	</div>	
	</form>
{else}
Заказов нет.
{/if}
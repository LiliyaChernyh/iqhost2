<h1>Управление импортом товаров</h1>

<div style="text-align: right; margin: 0px 0 5px 0;">
	{*    <input type="button" id="goAddTheme" class="goToBtn" value="Импорт товаров" />*}
    <input type="hidden" id="goAddTheme_link" value="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/add/" />
</div>
{if !empty($RESULT)}
    <div style="border: 1px solid greenyellow; padding: 10px 10px 0; margin-bottom: 10px;">
        <h3>Выгрузка из файла {$RESULT.file} завершена</h3>
        <p>Всего {$RESULT.total} товаров</p>
        <p>Добавлено {$RESULT.new} товаров</p>
        <p>Обновлено {$RESULT.updated} товаров</p>
		{if !empty($RESULT.categories)}
			<div>
				<p>Неизвестные категории:</p>
				<ul>
					{foreach from=$RESULT.categories item=cats}
						<li>{$cats}</li>
					{/foreach}
				</ul>
			</div>
		{/if}
    </div>
{/if}
{if !empty($ERROR)}
    <div style="border: 1px solid red; padding: 10px 10px 0; margin-bottom: 10px;">
        <h3>Ошибка</h3>
        <p>{$ERROR}</p>
    </div>
{/if}
{include file='paginator.tpl'}
<div class="holder-table">
	<table class="highlightable" id="faqTable">
	    <thead>
	        <tr>
	            <th colspan="2">№</th>
	            <th>Название</th>
				<th width="200">Дата файла выгрузки</th>
				<th width="200">Дата обновления</th>
	            <th width="200">Статус</th>
					{*            <th style="width: 10px;"><img src="{$ADMIN_IMAGES}icons/delete_icon.png" /></th>*}
			</tr>
	    </thead>
	    <tbody>
	{if !empty($IMPORTS)}
	    {foreach from=$IMPORTS item=IMPORT key=I}
	        <tr>
	        	<td style="display: none;"><a href="#" /></td>
					{*            <td style="display: none;"><a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/edit/{$IMPORT.importID}/" /></td>*}
	        	<td class="hov">&nbsp;</td>
	            <td style="width: 20px;">{$I+1}</td>
	            <td>
	                {$IMPORT.importName}
	            </td>
	            <td>
					{$IMPORT.storeXMLUpDate}
				</td>
	            <td>
	                {$IMPORT.updateDate}
	            </td>
	            <td>
	                {$IMPORT.statusText}
	            </td>
	            {*<td class="deleteEntry">
					<a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/del/{$IMPORT.brandID}/" title="Удалить"><img src="{$ADMIN_IMAGES}delete_icon.png" alt="Удалить" style="border: 0;" /></a>
	            </td>*}
	        </tr>
	    {/foreach}
	    </tbody>
	</table>
</div>
{else}
    </tbody>
</table>
</div>
    <div class="formBlock">
    	<div class="error">Записей не найдено</div>
    </div>
{/if}

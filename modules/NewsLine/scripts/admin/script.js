$(document).ready(function(){
    $('input[name="doAdd"]').click(function(){
        if ($('#section').val() == ''){
            alert('Поле пустое, введите значение!');
            return false;
        }
    });
    
    $('#sections').change(function(){
        var $options_selected = $('#sections option:selected');
        var $doSave = $('input[name="doSave"]');
        
        if ($options_selected.length == 1) {
            $('#section').val($options_selected.text());
            $doSave.removeAttr("disabled");
        }else{
            $doSave.attr("disabled", "disabled");
        }
    });
    
    $('input[name="doDel"]').click(function(){
        if (!$('#sections option:selected').length) {
            alert('Выделите хотя бы одну категорию.');
            return false;
        }
        
        if (!confirm('Будет удалены и связанные с этой категорией новости, вы действительно хотите удалить?')) 
            return false;
    });
});
<?php
/**
 * Синглтон.
 * В дочернем классе необходимо создать след. метод:
 * 
    public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }
 * @version 1.0
 */
abstract class Singleton
{
    private static $instances = array();
   
    /**
     * @param string $classname
     * @return Singleton
     */
    protected static function getInstance()
    {
        $className = @func_get_arg(0);
        
        if (empty($className))
            throw new Exception('Необходимо задать имя класса');
        
        if (!isset(self::$instances[$className]) || !is_object(self::$instances[$className]))
        {
            self::$instances[$className] = new $className();
        }
      
        return self::$instances[$className];
   }
}


?>
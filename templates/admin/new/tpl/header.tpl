	<div id="header">
		<h1>
			<a href="{$ADMIN_ALIAS}">&nbsp;</a>
		</h1>
		<div class="holder-left-top">
			<h2 class="logo-small">
				<a href="{$HOST_NAME}">
					<img src="{$ADMIN_IMAGES}backgrounds/logo.svg" alt="IQ Host" width="80" height="65"/>
				</a>
			</h2>
			<div class="holder-right">
				<h3><span>Управление сайтом:</span> <a href="{$HOST_NAME}"><span>{$SETTINGS.general.siteName}</span></a></h3>
							{if isset($USER)}
					<ul id="nav" class="ddm">
						<li>
							<a class="profile" href="{$ADMIN_ALIAS}profile/" title="Управление профилем"><span>{$USER.login} <span></span></span></a>
						</li>
						<li>
							<a class="exit" href="{$ADMIN_ALIAS}profile/logout/" title="Выйти из системы" onclick="return AreYouSure()"></a>
						</li>
{* <li {if $CURRENT_SECTION_ALIAS == "custommenu"}class='active'{/if}>

			<a href="{$ADMIN_ALIAS}custommenu/" title="Управление меню">Меню</a>
		</li>*}
					</ul>
				{/if}
			</div>
		</div>
	</div>
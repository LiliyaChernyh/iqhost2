$(document).ready(function()
{
    $("#newPasswordForm").ajaxForm({
            beforeSubmit:  passwordChangeBeforeHandler,
            success:  passwordChangeSubmitHandler,
            dataType:	   "json"
    });
});

function passwordChangeBeforeHandler(form, jO)
{
    if ($('#password').val() == ''){
        $.jGrowl('Введите пароль');
        return false;
    }
    if ($('#password').val() != $('#passwordConf').val()){
        $.jGrowl('Пароли не совпадают');
        return false;
    }
}

function passwordChangeSubmitHandler(data, status)
{
    if (data.error){
        $("#newPasswordForm").slideToggle();
	$("#newPasswordForm").parent().append(data.error);
    }else if (data.msg == 'ok'){
	$("#newPasswordForm").slideToggle();
	$("#newPasswordForm").parent().append(data.text);
    }
}
<?php
/**
 * RichText
 * Контрол для вставки FCKEditor в форму.
 * Требуется сам fckeditor и jQuery обёртка для него
 *
 * @version 3.5
 *
 */
class RichText extends Control
{
    /**
     * Ширина контрола
     *
     * @var string
     */
    private $width = '100%';

    /**
     * Высота контрола в пикселях
     *
     * @var int
     */
    private $height = 500;

    /**
     * Название тулбара FCK
     *
     * @var string
     */
    private $toolbarSet = '';

    /**
     * Путь к файлу с шаблоном заполнения.
     * Можно задавать как абсолютный путь (/templates/.../empty.tpl), так и относительный (empty.tpl),
     * который ищется в папке контрола
     *
     * @var string
     */
    private $emptyTemplate = null;

    public function InitControl($value = null, $rowID = null)
    {
        // Загружаем настройки
        $this -> LoadSettings();

        //$tagPattern = '<textarea name="{name}" style="width:{width};height:{height}" class="fck {toolbarSet}" rows="20" cols="60" id="{name}">{value}</textarea>';
        if ($value === null)
        {
            if (!empty($this -> emptyTemplate))
            {
                $templatePath = $this -> emptyTemplate{0} == '/' ? substr($this -> emptyTemplate, 1) : parent::$CONTROLS_PATH . __CLASS__ .'/'. $this -> emptyTemplate;

                if (file_exists($templatePath))
                    $value = file_get_contents($templatePath);
                else
                    $value = '';
            }
            else
                $value = '';
        }
        else
        {
            $value = str_replace("\r\n", "", $value);
            $value = str_replace("\n", "", $value);
            //$value = str_replace("'", "\'", $value);
            $value = str_ireplace("</script>", "</scr' + 'ipt>", $value);
        }

        // Задаем путь к шаблону
        $this -> controlTplPath = parent::$CONTROLS_PATH .__CLASS__.'/control.tpl';

        // Передаем переменные в шаблон
        $this -> controlTplVars['NAME'] = $this -> name;
        $this -> controlTplVars['VALUE'] = $value;
        $this -> controlTplVars['TOOLBARSET'] = $this -> toolbarSet;
        $this -> controlTplVars['WIDTH'] = $this -> width;
		$this -> controlTplVars['HEIGHT'] = $this -> height;
		$this -> controlTplVars['TIP'] = $this -> tip;
        $this -> controlTplVars['tableName'] = $this->tableName;
        $this -> controlTplVars['formIndex'] = (isset($this->formIndex))?$this->formIndex:0;

        // Используемые JS-библиотеки
        $this -> controlJSLibs = array();
        $this -> controlJSLibs[] = 'FCK';

        // Инициализирующая функция
        $this -> controlJSFiles = array();
        $this -> controlJSFiles[] = parent::$CONTROLS_ALIAS.__CLASS__.'/init.js';

        // Пременные, которые передаются инициализирующей JS функции
        $this -> controlJSVars['name'] = $this -> name;
        $this -> controlJSVars['toolbar'] = $this -> toolbarSet;
        $this -> controlJSVars['width'] = $this -> width;
        $this -> controlJSVars['height'] = $this -> height;

        $this -> type = __CLASS__;
    }

    private function LoadSettings()
    {
        if (!empty($this -> controlSettings['width']))
            $this -> width = $this -> controlSettings['width'];

        if (!empty($this -> controlSettings['height']))
            $this -> height = $this -> controlSettings['height'];

        if (!empty($this -> controlSettings['toolbarSet']))
            $this -> toolbarSet = $this -> controlSettings['toolbarSet'];

        if (!empty($this -> controlSettings['emptyTemplate']))
            $this -> emptyTemplate = $this -> controlSettings['emptyTemplate'];
    }

    public function PostBackHandler(&$data, $rowID = null) {}
}

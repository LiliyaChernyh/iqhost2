<?php
/**
 * Контрол для управления связями видео-файлов
 * 
 * @version 1.0
 *
 */
class VideosLinker extends Control implements IFlushingControl
{
    /**
     * Максимальный размер множественного выбора.
     * Число вариантов выбора, умещающихся в окошко контрола без прокрутки.
     *
     * @var int
     */
    public static $MaxSelectSize = 10;

    /**
     * Имя таблицы со значениями
     *
     * @var string
     */
    private $referentTableName = null;

    /**
     * Имя таблицы, хранящей множественные
     *
     * @var string
     */
    private $linkingTableName = null;
	
	/**
	*  Путь к серверному скрипту, возвращающему блоки видео
	*
	*  @var string
	*/
	private $serverScript = null;

    public function InitControl($value = null, $rowID = null)
    {
        $this -> LoadSettings();
        $this -> controlTplPath = parent::$CONTROLS_PATH.__CLASS__.'/control.tpl';
		
		// Инициализирующая функция
        $this -> controlJSFiles = array();
        $this -> controlJSFiles[] = parent::$CONTROLS_ALIAS.__CLASS__.'/init.js';
		
		// Используемые JS-библиотеки
        $this -> controlJSLibs = array();
        
        $this -> controlJSVars['name'] = $this -> name;
        $this -> controlJSVars['serverScript'] = $this -> serverScript;

        $this -> controlTplVars = array();        
        $this -> controlTplVars['NAME'] = $this -> name;
        $this -> controlTplVars['TIP'] = $this -> tip;

        if ($this -> required) $this -> controlTplVars['REQUIRED'] = 1;
        if ($this -> disabled) $this -> controlTplVars['READONLY'] = 1;

        // Связывающая таблица
        $linkedVideos = array();
        if ($rowID != null)
        {
			$query = 'SELECT videoID1, videoID2 FROM '.$this -> linkingTableName.' 
						WHERE videoID1 = '.$rowID.'
						 OR   videoID2 = '.$rowID;
			$linked = DB::QueryToArray($query);
            $cnt = count($linked);
            for ($i = 0; $i < $cnt; $i++)
			{
                $linkedVideos[] = ($linked[$i]['videoID1'] == $rowID ? $linked[$i]['videoID2'] : $linked[$i]['videoID1']);
			}
        }
		
		// Вытащим связанные видео
		if (!empty($linkedVideos))
		{
			$query = 'SELECT * FROM '.$this -> referentTableName.' v 
						LEFT JOIN '.TablesNames::$IMAGES_TABLE_NAME. ' i
						ON (v.picture = i.imageID)
						WHERE v.videoID IN ('.implode(',', $linkedVideos).')';
			$this -> controlTplVars['LINKED_VIDEOS'] = DB::QueryToArray($query);
		}
		
		// Уберём само видео
		if (!empty($rowID))
			$linkedVideos[] = $rowID;

		// Вытащим остальные видео
		$query = 'SELECT * FROM '.$this -> referentTableName.' v 
					LEFT JOIN '.TablesNames::$IMAGES_TABLE_NAME. ' i
					ON (v.picture = i.imageID) ';
					if (!empty($linkedVideos))
					$query .= ' WHERE v.videoID NOT IN ('.implode(',', $linkedVideos).') ';
					$query .= ' ORDER BY v.videoID DESC LIMIT 50';
		$this -> controlTplVars['VIDEOS'] = DB::QueryToArray($query);
        $this -> type = __CLASS__;
    }

    /**
     * Сюда приходит массив ID полей в ReferentTable
     *
     * @param array $data
     * @param int $rowID
     */
    public function Flush(&$data, $rowID)
    {
        $this -> LoadSettings();
        $linkingTableManager = new DBTableManager($this -> linkingTableName);
        $referentTableStructure = new DBTableStructure($this -> referentTableName);
        //$linkingTableSelectedRows = $linkingTableManager -> Select(array($this -> activeTableStructure -> PrimaryKey -> Name => $rowID));

        // добавим новые записи
		if (!empty($data['added']))
		{
			// Форму необходимо перезагрузить
			// ,к сожалению повесить доп обработчик на ajax response событие не получилось
			$this -> refreshFormAfterFlush = true;
			
			foreach ($data['added'] as $referentID)
			{
				if (is_numeric($referentID) && $referentID > 0)
					$linkingTableManager -> Insert(
						array(
							'videoID1' => $rowID,
							'videoID2' => $referentID
						)
					);
			}
		}
		
		// удалим ненужные
		if (!empty($data['delete']))
		{
			// Форму необходимо перезагрузить
			$this -> refreshFormAfterFlush = true;
			foreach ($data['delete'] as $referentID)
			{
				if (is_numeric($referentID) && $referentID > 0)
				{
					$linkingTableManager -> Delete(
						array(
							'videoID1' => $rowID,
							'videoID2' => $referentID
						)
					);

					$linkingTableManager -> Delete(
						array(
							'videoID1' => $referentID,
							'videoID2' => $rowID
						)
					);
				}
			}
		}
    }
    
    public function PostBackHandler(&$data, $rowID = null) {}
    
    protected function LoadSettings()
    {
        $errmsg = $this -> name. ' is undefined. It should be defined in FieldsRegistry';
        if (!empty($this -> controlSettings['referentTable']))
            $this -> referentTableName = $this -> controlSettings['referentTable'];
        else 
            throw new Exception('Referent Table (referentTable) for ' . $errmsg);

        if (!empty($this -> controlSettings['linkingTable']))
            $this -> linkingTableName = $this -> controlSettings['linkingTable'];
        else 
            throw new Exception('linking Table (linkingTable) for ' . $errmsg);
			
		if (!empty($this -> controlSettings['serverScript']))
            $this -> serverScript = $this -> controlSettings['serverScript'];
        else 
            throw new Exception('Server Script (serverScript) for ' . $errmsg);
    }
}
?>
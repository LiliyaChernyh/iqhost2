$(document).ready(function(){
	$('.highlightable th').tooltip();
	$('.highlightable td').tooltip();
	$("#filter_creationDateLeft, #filter_creationDateRight").mask("99.99.9999");
	$("#clearFilters").click(clearFiltersHandler);
	$("#filter_statuses").inlinemultiselect({
		triggerPopup: {'empty':"[Выбрать]",'nonempty':"[Изменить]",'disabled':"[Отключено]"},
		showResetLink : false
	});
	$("input:checkbox").click(function(event){event.stopPropagation();}).
		parent().unbind("click").click(function() {$(this).children().click();});
	
	$("#withSelected").change(function()
	{
		switch ($(this).val())
		{
			case "changeStatus":
				$("#changeStatus").show();
				break;
			case "delete":
				if (!AreYouSure("Удалённые заказы нельзя восстановить."))
				{
					$(this).val("");
				}
				break;
		}
	});

	$("#selectAll").click(function(event){
		event.preventDefault();
		$("input:checkbox").attr("checked", true);
	});
	
	$("#deselectAll").click(function(event){
		event.preventDefault();
		$("input:checkbox").attr("checked", false);
	});
	
	$("#invertSelection").click(function(event){
		event.preventDefault();
		$("input:checkbox").each(function(){
			$(this).attr("checked", !$(this).attr("checked"));
		})
	});
});

function clearFiltersHandler(event)
{
	event.preventDefault();
	$("#filtersForm input:text").val("");
	$("#filtersForm select").val("");
	$("#filtersForm input:checkbox").attr("checked", false);
	$("#filtersForm input:submit").click();
}
<h1>Настройки модуля "{$MODULE_NAME}" </h1>
<div style="float:right;"><a href="{$SECTION_ALIAS}uninstall/{$MODULE_NAME}/" onclick="return AreYouSure('{if isset($AYS_MESSAGE)}{$AYS_MESSAGE}{/if}')">[ удалить модуль ]</a></div>

<div>
    {if $MODULE_SETTINGS}
    <form action="{$SECTION_ALIAS}edit/{$MODULE_NAME}/" method="post">
            <ol>
                    {foreach from=$MODULE_SETTINGS item=SETTING key=SETTING_NAME}
                    {if $SETTING.isVisible}
                    <li>[ <strong>{$SETTING_NAME}</strong> ] {$SETTING.description}
                            <br />
                            <input type="text" size="70" name="settings[{$SETTING_NAME}]" value="{$SETTING.value}" />
                    </li>
                    {/if}
                    {/foreach}
            </ol>
            <div style="clear:left; margin:25px 0 0 150px;"><input type="submit" name="doEdit" value="Обновить настройки" /></div>
    </form>
    {else}
    Настроек нет. Нечего тут настраивать.
    {/if}
</div>

<?php
/**
 * Словосклонятор
 * 
 * Класс, сколняющий слова
 * 
 * @version 1.0
 */
class Declensioner
{
    /**
     * Склонение существительного после числительного
     * Первая форма - после 1
     * Вторая форма - после 2, 3, 4
     * Третья - все остальные
     *
     * @param unknown_type $int
     * @param unknown_type $firstForm
     * @param unknown_type $secondForm
     * @param unknown_type $thirdForm
     * @return unknown
     */
    public static function WordAfterIntDeclension($int, $firstForm, $secondForm, $thirdForm)
	{
	    $remainder = 0;
		$res = null;
		$remainder = $int % 10;
		$twoLast = substr($int, strlen($int) -2);
		if ($remainder == 1 && $twoLast != 11) $res = $firstForm;
		else if (($remainder == 2 || $remainder == 3 || $remainder == 4) && ($twoLast < 5 || $twoLast > 15)) $res = $secondForm;
		else $res = $thirdForm;
		return $res;
	}
}
?> 
<div class="top-section">
    <div class="container">
        <div class="twocolumns">
            <div class="text-holder">
                <h1>{$PAGE.caption}</h1>
                {if !empty($PAGES)}
                    <ul class="solution-menu">
                        {foreach from=$PAGES item=pageItem}
                            <li{if $PAGE.alias == $pageItem.alias} class="active"{/if}>
                                <a href="{$ALIAS}{$pageItem.alias}/">{$pageItem.caption}</a>
                            </li>
                        {/foreach}
                    </ul>
                {/if}
                <div class="text-holder__banners">
                    <div class="text-holder__banners-img">
                        <img src="{$IMAGES_ALIAS}server.png" alt="icon-server">
                    </div>
                    <div class="text-holder__banners-text">
                        <div>Нужен сервер для задач?</div>
                        <a href="/services/servers/">Есть готовые решения!</a>
                    </div>  
                </div>
                {if !empty($MENU_SOLUTION)}
                    <ul class="solution-menu">
                        {foreach from=$MENU_SOLUTION item=m} 
                            <li>
                                <a href="{$m.link}"><span>{$m.title}</span></a>
                            </li>
                        {/foreach}
                    </ul>
                {/if}
                <div class="text-holder__banners">
                    <div class="text-holder__banners-img">
                        <img src="{$IMAGES_ALIAS}help.png" alt="icon-help">
                    </div>
                    <span>Техническая поддержка</span>
                </div>
                <a href="/services/support/" class="solution__btn">Техническая поддержка</a>
            </div>

            {if !empty($BANNER)}
                <div class="banner-center clearfix">
                    <div class="twocolumns">
                        <img src="/getimage/{$BANNER.imageID}/">
                    </div>
                </div>
                {if !empty($BANNER.text)}
                    <div class="advantages clearfix">
                        {$BANNER.text}
                    </div>
                {/if}
            {/if}

            <div class="section-right">
                <section class="emailto">
                    <div class="emailto__img">
                        <img src="{$IMAGES_ALIAS}air.jpg" alt="air">
                    </div>
                    <div class="emailto__description">
                        <h1>Возможности стандартного почтового ящика:</h1>
                        <div class="emailto__description-wrapp">
                            {if !empty($FEATURES.one)}
                                <ul class="emailto__list">
                                    {foreach from=$FEATURES.one item=feature}
                                        <li class="check">{$feature.text}</li>
                                    {/foreach}
                                </ul>
                            {/if}
                            {if !empty($FEATURES.two)}
                                <ul class="emailto__list">
                                    {foreach from=$FEATURES.two item=feature}
                                        <li class="check">{$feature.text}</li>
                                    {/foreach}
                                </ul>
                            {/if}
                        </div>
                    </div>
                </section>
                <div class="twocolumns">
                    {if !empty($CONTENT.summary)}
                        {$CONTENT.summary}
                    {/if}

                    {if !empty($MAILBOX_SERVICES)}
                        <p class="border"><strong class="titl">Стоимость услуги</strong></p>
                        <ul class="services-wrapp inner">
                            {foreach from=$MAILBOX_SERVICES item=service}
                            <li>
                                <a class="emailto__link" href="{$service.link}">
                                    <div class="services-wrapp__img">
                                        <img src="/getimage/{$service.imageID}/" alt="{$service.title}">
                                    </div>
                                    <p><strong>{$service.title}</strong></p>
                                    <p><strong>от {$service.price}</strong></p>
                                    <p class="aksis">{$service.text}</p>
                                    <button class="services-wrapp__btn">Подробнее</button>
                                </a>
                            </li>
                            {/foreach}
                        </ul>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>
{if !empty($CONTENT.text)}                
    <div class="content-block">
        <div class="container">
            <h2>{$PAGE.caption}</h2>
            {$CONTENT.text}
        </div>
    </div>
{/if}
{include file="include-area.tpl"}
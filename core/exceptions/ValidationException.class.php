<?php
/**
 * Исключение валидации
 * @author George
 * @version 1.1
 */
class ValidationException extends Exception
{
	/**
	 * Имя поля, в котором была допущена ошибка
	 * @var string
	 */
	private $fieldName = null;
	
    public function __construct($message, $code = 0, $fieldName = null) 
    {
        parent::__construct($message, $code);
    }
    
    public function getFieldName()
    {
    	return $this -> fieldName;
    }

    // custom string representation of object
    public function __toString() 
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
?>
��������� ����� � /uploaded/ ����� ����:
1)/foto/
2)/foto/medium/
3)/foto/thumbnail/

��� ������ ���� ������ ������ �������� ���� .htaccess (�� ���������� /install/) � ����������:
1)/uploaded/foto/
2)/uploaded/foto/medium/

� �������� .htaccess ����� ��������� ��������� php:
# fileupload

php_value upload_max_filesize 9G

php_value post_max_size 9G

php_value max_execution_time 200

php_value max_input_time 200

php_value memory_limit 256M

� AnyEditorSettigns.class.php �������� ��� �������������� ��������� (��� ���� ����, ��� �������� ������!):
	'fb_typetags'    => array(
            'tableName'     => 'fb_typeTags',
            'ruName'        => '���� �����',
            'ruItem'        => '���� �����',
            'listFields'    => array(
                'typeTagID' => array(
                    'width'         => 40,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'caption' => array(
                    'width'         => 500,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
            ),
            'filter'            => null,
            'orderBy'           => '`typeTagID`',
            'limitOnPage'       => 20,
            'allowAddition'     => 1,
            'allowRemove'       => 1
        ),
        
        'fb_tags'    => array(
            'tableName'     => 'fb_Tags',
            'ruName'        => '����',
            'ruItem'        => '����',
            'listFields'    => array(
                'tagID' => array(
                    'width'         => 40,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'caption' => array(
                    'width'         => 500,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'typeTagID' => array(
                    'width'         => 500,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true,
                    'linkTable'     => 'fb_typeTags',
                    'linkField'     => 'caption'
                ),
            ),
            'filter'            => null,
            'orderBy'           => '`tagID`',
            'limitOnPage'       => 20,
            'allowAddition'     => 1,
            'allowRemove'       => 1
        ),
<h1>Корзина удаленных статей</h1>

{if $ARTICLES_LIST|@count == 0}
Корзина пуста
{else}
    <form action="" method="POST">
    <div style="text-align: right; margin: -30px 0 10px 0">
        <input type="button" id="checkerBtn" value="Отметить все" />
    </div>
    <table id="entriesList" class="highlightable">
        <thead>
    		<tr>
    			<th width="5%">№</th>
    			<th width="30%">Заголовок статьи</th>
    			<th width="30%">Категория</th>
    			<th width="15%">Дата публикации</th>
    			<th width="15%">Дата изменения</th>
    			<th width="5%"></th>
    		</tr>
    	</thead>
    	<tbody>
            {foreach from=$ARTICLES_LIST item=ARTICLES_ITEM key=I}
    		<tr>
                <td style="display: none;"><a href="{$MODULE_ALIAS}edit/{$ARTICLES_ITEM.articleID}/"></a></td>
    			<td>{$I+1}</td>
    			<td>{$ARTICLES_ITEM.title|truncate:50}</td>
    			<td>{$ARTICLES_ITEM.caption|truncate:60}</td>
    			<td>{$ARTICLES_ITEM.publicationDate|@date_format:"%d.%m.%Y %H:%M"}</td>
    			<td>{$ARTICLES_ITEM.modificationDate|@date_format:"%d.%m.%Y %H:%M"}</td>
    			<td class="skip"><input type="checkbox" name="toEdit[]" class="checks" value="{$ARTICLES_ITEM.articleID}" /></td>
    		</tr>
    		{/foreach}
    	</tbody>
    </table>
    
    <div class="deleteBtn" style="margin-top: 10px; text-align: right">
    	<input type="submit" id="deleteItems" name="doWipe" value="Удалить отмеченные" disabled="disabled" />
    	<input type="submit" id="restoreItems" name="doRestore" value="Восстановить отмеченные" disabled="disabled" />
    </div>
    </form>
    
    {literal}
    <script type="text/JavaScript"> 
        $(document).ready(function()
    	{
    	    $('.checks').change(ActivateButtons);
    	    $('#checkerBtn').click(CheckUncheck);
    	});
    	
    	function CheckUncheck()
    	{
    	    if ($('#checkerBtn').val() == 'Отметить все')
    	    {
    	        $('.checks').attr('checked', true);
    	        $('#checkerBtn').val('Снять отметки');
    	    }
    	    else
    	    {
    	        $('.checks').attr('checked', false);
    	        $('#checkerBtn').val('Отметить все');
    	    }
    	    ActivateButtons();
    	}
    	
    	function ActivateButtons(event)
    	{
    	    var checked = $(".checks:checked").length;
    	    
    	    if (checked > 0)
    	    {
    	        $('#restoreItems').attr("disabled", false);
    	        $('#deleteItems').attr("disabled", false);
    	    }
    	    else
    	    {
    	        $('#restoreItems').attr("disabled", true);
    	        $('#deleteItems').attr("disabled", true);
    	    }
    	}
    </script>
    {/literal}
{/if}
<div class="block-hold">
	{if !empty($news)}
		<ul class="list-news">
			{foreach from=$news item=item key=key}
			<li>
				<a {if !$item.isBody}href="{$item.fullAlias}"{/if} class="figure">
					{if !empty($item.srcSmall)}
						<img src="{$item.srcSmall}" width="220" />
					{else}
						<img src="{$IMAGES_ALIAS}noimage.jpg" alt="image" width="220" height="165" />
					{/if}
				</a>
					<span class="date">{$item.publicationDate|date_format:"%e %m %Y":"":"rus"}</span>
					<a class="title-news" {if !$item.isBody}href="{$item.fullAlias}"{/if}>{$item.title}</a>
					<p>{$item.summary}</p>
				{if !$item.isBody}<a href="{$item.fullAlias}">Подробнее</a>{/if}
			</li>
			{/foreach}
		</ul>
		{include file="paginator.tpl"}
	{/if}
</div>



<?php

/**
 * Контроллер для работы с модулем Articles во фронт-енде
 *
 * @version 2.0
 */
class ArticlesControllerFront extends FrontController
{
    protected $model;
    protected $rootAlias;
    protected $articles;

    public function __construct()
    {
        parent::__construct();
        $settings        = CommonModule::LoadSettings('Articles');
        $this->rootAlias = !empty($settings['ParentAlias']) ? $settings['ParentAlias'] . "/" . $settings['rootAlias'] : $settings['rootAlias'];
        // Получаем страницу из базы
        $this->page      = new Page($this->rootAlias);
        $this->view      = new ArticlesViewFront();
        $this->model     = new ArticlesModelFront($this->rootAlias);
        $this->articles  = new Articles();

        ViewData::Assign("ROOT_ALIAS", $this->rootAlias);
        ViewData::Assign("SECTIONS", $this->articles->GetSections());
    }

    public function Index($args = null)
    {

        $count   = count($args);
        $section = !empty($args[0]) ? $args[0] : null;
        $page    = (isset(Request::$GET['page']) && is_numeric(Request::$GET['page'])) ? Request::$GET['page'] : 1;
        // $limit   = $this->articles->settings['MessagesOnPageFront'];
        $limit   = 200;

        $start   = ($page - 1) * $limit;

        ViewData::Assign("SECTION", $section);

        if(!empty($section))
        {
            $sectionData             = $this->model->GetSection($section);
            $this->page->caption     = UTF8::ucfirst($sectionData['caption']);
            $this->page->title       = $sectionData['title'];
            $this->page->description = $sectionData['description'];
            $this->page->keywords    = $sectionData['keywords'];
            // ViewData::Assign("CONTENT", $sectionData['body']);
            $articles = $this->model->GetArticles($section, $start, $limit);
            ViewData::Assign('articles', $articles);
                   // trace($articles);
            $countRows = $this->model->GetFoundRows();
            //пагинация
            $this->setPaginator($limit, $countRows, $page);
            ViewData::Assign("BACK_URL", "/" . $this->rootAlias . "/");
        }
        else
        {
            $content = $this->page->Content();
            // trace($content);
            ViewData::Assign("CONTENT1", $content['pageContent']);
        }

        if($count > 2) throw new PageNotFoundException("Error");


        if($count > 0 && !$this->model->isSection($args[$count - 1]))
        {
            $alias = array_pop($args);
            // trace($alias);
            return $this->Item($alias, $section);
        }
        
        $this->view->Index();
        return $this->view;
    }

    public function Item($alias, $section = null)
    {
        $article = $this->model->GetArticle($alias, $section);
        if(empty($article)) throw new PageNotFoundException('Error');

        $this->page->pageUrl = "/" . $this->rootAlias . "/" . (!empty($article['cat_alias']) ? $article['cat_alias'] . "/" : '') . (!empty($article['alias']) ? $article['alias'] : $article['newsID']) . "/";

        ViewData::Assign("BACK_URL", "/" . $this->rootAlias . "/" . (!empty($article['cat_alias']) ? $article['cat_alias'] . "/" : ''));
        ViewData::Assign('article', $article);
        // trace($article);
        ViewData::Assign("SINGLE_PAGE", true);

        $this->view->Item();
        $this->page->title       = $article['title'];
        $this->page->caption     = $article['caption'];
        $this->page->description = $article['description'];
        return $this->view;
    }

}

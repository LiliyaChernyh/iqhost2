<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImagesController
 *
 * @author Prog
 */
class ImagesController extends SuperPowerController
{
    /**
     * @var bool Позволяет выставить заголовок с датой последнего изменения файла,
     *           что в свою очередь даёт браузеру возможность не загружать картинку повторно
     */
	public $allowBrowserImagesCache = true; 
    
    /**
     *  Выставляем нужные опции для ImageManager
     * 
     * @param array $argsArr  аргументы из URL (сегменты) -- по ссылке
     */
    public static function initImageManager(&$argsArr)
    {
        if ((!empty($argsArr[1])) // если опции размеров вообще указаны
                && (self::is_in_str($argsArr[1], 'p'))) { // и есть буква 'p'
            ImageManager::$proportionalResize = true;
            
        } else {
            ImageManager::$proportionalResize = false;
        }
    }
    
    public function Index($args = null)
	{
        self::initImageManager($args);
        
        $args[0] = str_replace('_', '', $args[0]); // нужно для пустых imageID, чтобы $args[0] вообще существовал (см. документацию BP)
        
        if (!isset($args[0]) || !is_numeric($args[0])){
			// throw new PageNotFoundException("");
            $imageID = '';
        } else {
            $imageID = $args[0];
        }
        
		try
		{
			$image	 = self::GetImagePath($imageID);
			$size = isset($args[1]) ? $args[1] : false;
			if (!$size) {
				return $this->ShowImage($image);
			}

			$path = pathinfo($image);
			$newPath = $path['dirname'] . "/" . $size;
			$newImage = $newPath . "/" . $path['basename'];
			$newImage = str_replace("//", "/", $newImage);
			if (is_file($newImage)) {
				return $this->ShowImage($newImage);
			}

			if (!isset($size[0]) || !is_numeric($size[0]) || !isset($size[1]) || !is_numeric($size[1])){
				return $this->ShowImage($image);
			}
			$newImage = $this->CopyImage($image,$size);

			$size = explode("x", $size);
			$width = $size[0];
			$height = $size[1];
//			die($newImage);

			$newImage = str_replace(IncPaths::$ROOT_PATH, '', $newImage);
			if ($newImage{0} != '/')
				$newImage = '/'.$newImage;

			if (sizeof($size) == 3){
				ImageManager::StrongImageResize($newImage, $width, $height);
			}
			else{
				ImageManager::ImageResize($newImage, $width, $height);
			}
			$newImage = preg_replace('/^\//', IncPaths::$ROOT_PATH, $newImage);
			return $this->ShowImage($newImage);
		}
		catch (Exception $ex)
		{
			die($ex->getMessage());
		}
	}

	public static function GetImagePath($imageID)
	{
		$notFound = false;
        
        if (!empty($imageID)) {
            $imageFilePath = DB::QueryOneValueEx("SELECT src FROM greeny_images WHERE imageID=:imageID", array("imageID" => array($imageID, "int")));
            if (!$imageFilePath)
                throw new Exception();
            if ($imageFilePath{0} == "/")
            $imageFilePath = IncPaths::$ROOT_PATH . $imageFilePath;
            
            if (!is_file($imageFilePath)) {
                $notFound = true;
            }
        } else {
            $notFound = true;
        }
         
        if ($notFound) {
            $imageFilePath = self::randomDefaultImage();
        }
        
		return $imageFilePath;
	}
    
    /**
     * Получит случайный файл из некоторой диреткории
     * 
     * @param string $randomDir  путь к директории, откуда нужно взять случайный файл
     * @return string            путь к случайному файлу из этой директории
     */
    public static function randomDefaultImage($randomDir = '/uploaded/rybi')
    {
        $randomDir = IncPaths::$ROOT_PATH . $randomDir;  
        $files = glob($randomDir . '/*.*');
        $file = array_rand($files);   
        
        return $files[$file];
    }

	private function ShowImage($image)
	{
		$info = getImageSize($image);

//		trace($info);
		header("Content-Type: " . $info['mime']);
		header("Last-Modified: " . date(DATE_RFC822, filemtime($image)));
		header("Cache-Control: private, max-age=10800, pre-check=10800");
		header("Pragma: private");
		header("Expires: " . date(DATE_RFC822, strtotime(" 2 day")));
        
		if ($this->allowBrowserImagesCache) {
            if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && // не передаём дважды уже переданные файлы
                (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == filemtime($image)))
            {
                // send the last mod time of the file back
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($image)) . ' GMT', true, 304);
                exit;
            }
        }
        
        readfile($image);

		return new EmptyActionResult();
	}

	private function CopyImage($image,$size)
	{
		$path = pathinfo($image);
		$newPath = $path['dirname'] . "/" . $size;
		if (!is_dir($newPath)) {
			if (!mkdir($newPath, 0777, true)) {
				throw new Exception();
			}
		}
		$newImage = $newPath . "/" . $path['basename'];

		if (!copy($image, $newImage)) {
			throw new Exception();
		}
		return $newImage;
	}

	public static function CheckSize($imageID, $sizes)
	{
		try
		{
			$image	 = self::GetImagePath($imageID);

//			if($image{0} == "/")
//				$image = IncPaths::$ROOT_PATH . $image;

			if(!is_file($image) || !($imageInfo = getimagesize($image)))
			{
//				FB::log($image, "image");
//				FB::log("1", "image error");
				return false;
			}
//			FB::log($imageInfo, "imageInfo");
			$size	 = explode("x", $sizes);
			$width	 = $size[0];
			$height	 = $size[1];
			$imageWidth	 = $imageInfo[0];
			$imageHeight = $imageInfo[1];

			if($imageWidth <= $width || $imageHeight <= $height)
			{
				return false;
			}
			return true;
		}
		catch(Exception $ex)
		{
//			FB::log("0", "image error");
			return false;
		}
	}
    
    // проверим встречается ли подстрока в строке
    public static function is_in_str($str,$substr)
    {
      $result = strpos ($str, $substr);
      if ($result === FALSE) // если это действительно FALSE, а не ноль, например 
        return false;
      else
        return true;   
    }

}

?>

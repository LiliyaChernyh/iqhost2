    {if !empty($FIELDS)}
	{assign var="fieldIndex" value=0}
	{foreach from=$FIELDS key=key item=field}
	    <div class="formBlockHeader">{$field.fieldName}</div>
	    <div class="formBlock">
		<table class="highlightable" id="faqTable">
		    <thead>
			<tr>
			    <th colspan="2">№</th>
			    <th>Значение</th>
			    <th style="width: 10px;">вкл/выкл</th>
			    <th>Дополнительные данные значения</th>
			</tr>
		    </thead>
		    <tbody>
			{foreach from=$field.values key=key2 item=val}
			    <tr>
			    	<td class="hov">&nbsp;</td>
				<td style="width: 20px;">{$key2+1}</td>
				<td>
				    {$val.fieldValue}
				</td>
				<td class="enableValue" align="center">
				    <div class="hold-checkbox">
					<input type="checkbox" class="ccCheckBox" name="fields[{$val.fieldID}][{$fieldIndex++}][fieldValueID]" value="{$val.fieldValueID}" id="checkbox_{$key}_{$key}" {if isset($val.productFieldID)}checked="checked"{/if}>
				    </div>
				</td>
				<td>
				    {$val.form}
				</td>
			    </tr>
			{/foreach}
		    </tbody>
		</table>

	    </div>
	{/foreach}
    {else}
	<div class="formBlock">
		<div class="error">
			Дла выбранного типа товаров дополнительных полей не найдено
		</div>
	</div>
    {/if}

<?php

/**
 * Класс использует возможности MySQL по полнотекстовому поиску
 * библиотеку PHPMorphy 
 * 
 * @version 2.0
 *
 */
class MysqlSearchEngine
{
    /**
     * Ключевые слова
     *
     * @var array
     */
    public $keyWords;
    
    /**
     * Количество найденных результатов
     *
     * @var int
     */
    public $resultsCnt = 0;
    
    /**
	 * Конструктор инициализирует класс
	 *
	 * @param string $tableNamesMatch
	 * @return MysqlSearchEngine
	 */
	public function MysqlSearchEngine(
	/**
	 * Регулярное выражение. 
	 * Поиск будет производиться в таблицах, соответствующих регулярному выражению
	 */
	$tableNamesMatch = null)
	{
		$query = "SHOW TABLES";
		$queryResult = DB::Query($query);
		
		$this -> searchTablesNamesList = array();
		$this -> searchTablesKeysList = array();
		$this -> searchTablesIndexes = array();
		
		$curTable = null;
		while ($row = DB::FetchArray($queryResult)) 
		{
			if (($tableNamesMatch != null) && (!preg_match($tableNamesMatch, $row[0])))
			continue;

				$curTable = $row[0];
				$FTfields = array();		
				$query2 = "SHOW INDEX FROM `$curTable`";
				$queryResult2 = DB::Query($query2);
				$keyName = null;

				$isAnyFullTextIndex = false;
				while ($row2 = DB::FetchAssoc($queryResult2)) 
				{
					if ($row2['Index_type'] == "FULLTEXT") 
					{
						$isAnyFullTextIndex = true;
						$FTfields[] = $row2['Column_name'];
					}
					if ($row2['Key_name'] == "PRIMARY")
					{
						$keyName = $row2['Column_name'];
					}
				}
				if ($isAnyFullTextIndex)
				{
					$this -> searchTablesNamesList[] = $curTable;
					$this -> searchTablesKeysList[] = $keyName;
					$this -> searchTablesIndexes[] = $FTfields;
				}
			
		}
		//print_r($this -> searchTablesNamesList);		
		//print_r($this -> searchTablesKeysList);		
		//print_r($this -> searchTablesIndexes);		
	}
	
	public $searchTablesNamesList = null;
	public $searchTablesKeysList = null;
	public $searchTablesIndexes = null;

	/**
	 * Функция поиска
	 * Возвращает результат sql-запроса из временной таблицы
	 * Поля 
	 * 	key - ключ в таблице
	 * 	tableName - Имя таблицы
	 * 	relevancy - релевантность запросу
	 *
	 * @param mixed $keyWords
	 * @return PDOStatement
	 */
	public function search($query, $limit = null, $start = null)
	{
	    $keyWords = $this -> PrepareKeyWords($query);
        $this -> keyWords = &$keyWords;

        $searchQuery = '';
	    if (is_array($keyWords))
	       $searchQuery = implode(' ', $keyWords);
	    elseif (is_string($keyWords))
	       $searchQuery = $keyWords;
	    elseif (empty($keyWords))
	       return null;

	    // Закавычим все слова с дефисом + экран
		$searchQuery = preg_replace('/([a-zA-Zа-яА-Я]+)-([a-zA-Zа-яА-Я]+)/u', '\\\'$1-$2\\\'', $searchQuery);

		$query = 'CREATE TEMPORARY TABLE xxx (`key` INT( 11 ) NOT NULL ,
		          `tableName` VARCHAR( 64 ) NOT NULL,
		          `relevancy` FLOAT NOT NULL 
		          ) TYPE = MYISAM ;';
		DB::Query($query);
		
        $lim = (!empty($limit) && is_numeric($limit)) ? (' LIMIT ' . 
            ((!is_numeric($start)) ? 0 : $start) .
            ', ' . $limit) : '';
		
        $this -> resultsCnt = 0;
		for ($i = 0; $i < count($this ->searchTablesIndexes); $i++)
		{
			$query = "
			INSERT INTO xxx ";
			$fields = "`".$this ->searchTablesIndexes[$i][0] ."`";
			for ($j = 1; $j < count($this ->searchTablesIndexes[$i]); $j++)
			{
				$fields .= ", `" . $this ->searchTablesIndexes[$i][$j] . "`";
			}
			$select = "SELECT `" . $this ->searchTablesKeysList[$i] . "` as 'key', '". 
					$this ->searchTablesNamesList[$i] ."' as 'tableName',
				   MATCH($fields) AGAINST (" . DB::Quote($searchQuery) . ") AS relevancy 
				   FROM `" . $this -> searchTablesNamesList[$i] ."` ;";
			$query .= $select;
			DB::Query($query);
		}
		$this -> resultsCnt = DB::QueryOneValue('SELECT COUNT(*) FROM xxx WHERE relevancy > 0');

		//$query = "SELECT * FROM xxx ORDER BY relevancy DESC";
		$query = 'SELECT * FROM xxx WHERE relevancy > 0 ORDER BY relevancy DESC '.$lim;
		return  DB::Query($query);
	}
	
	public function &LikeSearchInVarcharFields($keyWords)
	{
	    if (empty($keyWords)) return null;
	    $keyWordsCnt = count($keyWords);
	    $result = array();
	    $killChars = array('\'', '\\');
	    for ($i = 0; $i < count($keyWords); $i++)
	    {
	         $keyWords[$i] = str_replace($killChars, '', $keyWords[$i]);
	    }
	    
	    $cnt = count($this -> searchTablesNamesList);
	    for ($i = 0; $i < $cnt; $i++)
	    {
	        $query = 'SELECT COLUMN_NAME FROM information_schema.COLUMNS 
	               WHERE TABLE_SCHEMA = \''. DB::$DBName .'\' 
	               AND DATA_TYPE = \'varchar\' 
	               AND TABLE_NAME = \''.$this -> searchTablesNamesList[$i].'\'
	               AND COLUMN_NAME IN (\''.implode('\', \'', $this -> searchTablesIndexes[$i]).'\')';
	        $varcharColumns = DB::QueryToArray($query, null, 'COLUMN_NAME');
	        for ($j = 0; $j < count($varcharColumns); $j++)
	        {
	            $query = 'SELECT '.$this -> searchTablesKeysList[$i]. ' as \'key\',
	                              \''.$this -> searchTablesNamesList[$i].'\' as \'tableName\',
	                              \''.$varcharColumns[$j].'\' as \'columnName\',
	                              `'.$varcharColumns[$j].'` as \'value\'
	                       FROM '.$this -> searchTablesNamesList[$i].'
	                       WHERE UPPER(`'.$varcharColumns[$j].'`) LIKE UPPER(' . DB::Quote('%'.$keyWords[0].'%') .')';
	            for ($k = 1; $k < $keyWordsCnt; $k++)
	               $query .= ' OR UPPER(`'.$varcharColumns[$j].'`) LIKE UPPER(\'%'.$keyWords[$k].'%\') ';
	        }
	        $result = array_merge($result, DB::QueryToArray($query));
	    }
	    
	    // Ранжирование результатов
	    $cnt = count($result);
	    for ($i = 0; $i < $cnt; $i++)
	    {
	        $result[$i]['relevancy'] = 0;
	        for ($j = 0; $j < $keyWordsCnt; $j++)
	        {
	            if (stripos($result[$i]['value'], $keyWords[$j]) !== false)
	               $result[$i]['relevancy']++;
	        }
	    }
	    usort($result, 'MysqlSearchEngine::relevancyComapre');
	    return $result;
	}
	
	public static function relevancyComapre($a, $b)
	{
        if ($a['relevancy'] == $b['relevancy']) 
            return 0;
        return ($a['relevancy'] < $b['relevancy']) ? 1 : -1;
	}
	
	/**
	 * Подготавливает строку, переданную пользователем к поиску
	 *
	 * @param string $searchquery
	 */
	public function PrepareKeyWords($searchQuery)
	{
	     // уберём кавычки
		$searchQuery = str_replace('\'','',$searchQuery);
		// Разбиваем ключевые слова и выкидываем короткие
		$keyWords = preg_split ("/[\s,.:\";!?']+/",$searchQuery);

	    $russianWords = array();
		$englishWords = array();

		for ($i = 0; $i< count($keyWords); $i++)
		{
		    if (preg_match('/[a-zA-Z]+/', $keyWords[$i]))
		    {
		        if (strlen($keyWords[$i]) >= 3)
		          $englishWords[] = UTF8::strtoupper($keyWords[$i]);
		    }
		    else 
		    {
    			if (strlen($keyWords[$i]) < 4) continue;
    			$russianWords[] = UTF8::strtoupper($keyWords[$i]);
		    }
		}
		

	    // first we include phpmorphy library
        require_once(IncPaths::$LIBS_PATH. 'PhpMorphy/src/common.php');
        
        // set some options
        $opts = array(
        	// storage type, follow types supported
        	// PHPMORPHY_STORAGE_FILE - use file operations(fread, fseek) for dictionary access, this is very slow...
        	// PHPMORPHY_STORAGE_SHM - load dictionary in shared memory(using shmop php extension), this is preferred mode
        	// PHPMORPHY_STORAGE_MEM - load dict to memory each time when phpMorphy intialized, this useful when shmop ext. not activated. Speed same as for PHPMORPHY_STORAGE_SHM type
        	'storage' => PHPMORPHY_STORAGE_FILE,
        	// Enable prediction by suffix
        	'predict_by_suffix' => true, 
        	// Enable prediction by prefix
        	'predict_by_db' => true,
        	// TODO: comment this
        	'graminfo_as_text' => true,
        );

        // Path to directory where dictionaries located
        $dir = IncPaths::$LIBS_PATH . 'PhpMorphy/dicts/utf-8';

        // Create phpMorphy instance
        try 
        {
            if (!empty($russianWords))
        	   $morphyRu = new phpMorphy($dir, 'ru_RU', $opts);
        	if (!empty($englishWords))
        	   $morphyEn = new phpMorphy($dir, 'en_EN', $opts);
        }
        catch(phpMorphy_Exception $e) 
        {
        	//Log::Error('Error occured while creating phpMorphy instance: ' . $e->getMessage());
        	return array_merge($russianWords, $englishWords);
        }

        $allFormsRu = array();
        $allFormsEn = array();
        
        if (isset($morphyRu))
            $allFormsRu = $morphyRu -> getAllForms($russianWords);
        if (isset($morphyEn))
            $allFormsEn = $morphyEn -> getAllForms($englishWords);

        $allForms = array_merge($allFormsEn, $allFormsRu);

        $result = array();
        foreach ($allForms as $key => $value)
        {
            $result[$key] = 1;
            $cnt = count($allForms[$key]);
            for ($i = 0; $i < $cnt; $i++)
            {
                $result[$allForms[$key][$i]] = 1;
            }
        }

        return array_keys($result);
	}
}
?>
<?php
/**
 * SingleSelect
 * Простой селект, который может брать значения из связанного с
 * полем перечисления или из таблицы-справочника
 *
 * Настройки:
 * enum : bool (true для того, чтобы значения брались из
 *               связанного перечисления)
 * - referentTable : string (имя связанной таблицы со значениями)
 * - listingKey : string (имя поля с человеко-понятным значением)
 * - CSSClass : string
 *
 * @version 3.4
 *
 */
class SingleSelect extends Control
{
    /**
     * Имя таблицы со значениями
     *
     * @var string
     */
    private $referentTableName = null;

    /**
     * Имя поля с человеко-понятным значением
     *
     * @var string
     */
    private $listingKey = null;

    /**
     * Использовать связанное с полем перечисление
     *
     * @var bool
     */
    private $isEnum = false;

    /**
     * Значение по умолчанию
     *
     * @var int
     */
    protected $emptyValue = null;

    /**
     * Текстовое значение по умолчанию
     *
     * @var string
     */
    protected $emptyText = '-';

    /**
     * Порядок сортировки строк в таблице-справочнике,
     * откуда будут браться опшены селекта.
     *
     * Подставляется в DBTableManager -> SortOrder
     *
     * @var string
     */
    protected $orderBy = '';

    protected $parentKey = "parentID";
    protected $condition = array();
	protected $referKey	 = null;
	protected $counter	 = null;

	public function InitControl($value = null, $rowID = null)
    {
		$this -> LoadSettings();

//		FB::log($this->data, 'data');
//		FB::log($this->referKey, 'referKey');
//		FB::log($this->condition, "cond");

		$value = ($value === null) ? '' : htmlspecialchars($value);

		if ($this -> isEnum)
        {
            $query = "SELECT `column_type`
                      FROM `information_schema`.`columns`
                      WHERE `table_name` = '". $this -> controlsRegistry -> activeTableStructure -> TableName ."'
                       AND `column_name` = '". $this -> name . "'
                        AND `table_schema` = '".DB::$DBName."'";
            $result = DB::FetchAssoc(DB::Query($query));
            $items = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$result['column_type']));

            $cnt = count($items);

            $options = array();
            if (!$this -> required)
            {
                $options[] = array(
                    'value'        => $this -> emptyValue,
                    'text'        => $this -> emptyText,
                    'selected'    => ''
                );
            }
            for ($i = 0; $i < $cnt; $i++)
            {
                $option = array(
                    'value'        => $items[$i],
                    'text'        => $items[$i],
                    'selected'	 => ($value == $items[$i] ? '1' : '0')
				);

                $options[] = $option;
            }
        }
        else
        {
            // Вытаскиваем значения ReferentTable
            $referentTableManager			 = new DBTableManager($this->referentTableName);
			$referentTableManager->SortOrder = empty($this->orderBy) ? '"' . $this->listingKey . '"' : $this->orderBy;

			if($this->referKey && isset($this->data[$this->referKey]))
			{
				$this->condition[$this->referKey] = $this->data[$this->referKey];
				FB::log($this->condition, "cond");
			}

			$entries = $referentTableManager->Select($this->condition);

//            trace($entries);

			$isMultiLanguage = false; //$qResult[0]['isMultiLanguage'];
			if ($isMultiLanguage)
            {
                $relReferentTableManager = new DBTableManager($this -> referentTableName . ML_RELTABLE_POSTFIX);
                $contentReferentTableManager = new DBTableManager($this -> referentTableName . ML_CONTENTTABLE_POSTFIX);

                if (isset($_SESSION['lang']))
                    $currentLang = $_SESSION['lang'];
                else
                {
                    $systemSettings = new SystemSettings();
                    $currentLang = $systemSettings -> GetValue('DEFAULT_LANG');
                }
            }

            $cnt = sizeof($entries);
            $options = array();
            if (!$this -> required)
            {
                $options[] = array(
                    'value'        => $this -> emptyValue,
                    'text'        => $this -> emptyText,
                    'selected'    => ''
                );

            }
            elseif ($cnt == 0)
            {
                throw new Exception('No entries in reference table!', 3510);
            }

            if ($this->parentKey)
            {
                $optData = new Tree();
                $entries = $optData->SortTree($entries, $referentTableManager->TableStructure->PrimaryKey->Name, $this->listingKey, $this->parentKey);
				$cnt = sizeof($entries);
            }

			for ($i = 0; $i < $cnt; $i++)
            {
				$option = array(
                    'value'		 => $entries[$i][$referentTableManager->TableStructure->PrimaryKey->Name],
					'text'		 => $entries[$i][$this->listingKey] . ((!empty($this->counter) && isset($entries[$i][$this->counter])) ? " (" . $entries[$i][$this->counter] . ")" : ''),
					'selected'	 => ($value == $entries[$i][$referentTableManager->TableStructure->PrimaryKey->Name] ? '1' : ''),
					'disabled'	 => !empty($entries[$i]['disabled']) ? $entries[$i]['disabled'] : false,
					'level'		 => !empty($entries[$i]['level']) ? $entries[$i]['level'] : ''
				);


                if ($isMultiLanguage)
                {
                    $relRows = $relReferentTableManager -> Select(
                        array(
                            $referentTableManager -> TableStructure -> PrimaryKey -> Name     => $entries[$i][$referentTableManager -> TableStructure -> PrimaryKey -> Name],
                            'lang'                                                            => $currentLang
                        )
                    );

                    if (sizeof($relRows) == 0)
                        continue;
                        //throw new MultiLanguageException('У поля «'. $listingKey .'['. $entries[$i][$referentTableManager -> TableStructure -> PrimaryKey -> Name] .']' .'» нет связи для языка «'. $currentLang .'»');

                    $relRow = $relRows[0];

                    $contentRows = $contentReferentTableManager -> Select(
                        array(
                            'contentID'    => $relRow['contentID']
                        )
                    );

                    if (sizeof($contentRows) == 0)
                        throw new MultiLanguageException('У поля «'. $this -> listingKey .'['. $entries[$i][$referentTableManager -> TableStructure -> PrimaryKey -> Name] .']' .'» нет версии для языка «'. $currentLang .'»');

                    $option['text'] = $contentRows[0][$this -> listingKey];
                }
//                else
//                {
//                    $option['text'] = $entries[$i][$this -> listingKey];
//				}

				$options[] = $option;
            }
        }
        $this -> controlTplPath = parent::$CONTROLS_PATH.__CLASS__.'/control.tpl';

        $this -> controlTplVars = array();
        $this -> controlTplVars['NAME'] = $this -> name;
        $this -> controlTplVars['CLASS'] = $this -> CSSClass;
        $this -> controlTplVars['TIP'] = $this -> tip;
        $this -> controlTplVars['VALUE'] = $value;
        if ($this -> disabled) $this -> controlTplVars['READONLY'] = 1;
        if ($this -> required) $this -> controlTplVars['REQUIRED'] = 1;
        $this -> controlTplVars['OPTIONS'] = $options;
        $this -> controlTplVars['tableName'] = $this->tableName;
        $this -> controlTplVars['formIndex'] = (isset($this->formIndex))?$this->formIndex:0;

        $this -> type = __CLASS__;
    }

    protected function LoadSettings()
    {
//        trace($this->controlSettings);
        if (isset($this -> controlSettings['enum']) && $this -> controlSettings['enum'] != false)
           $this -> isEnum = true;
        else
        {
            if (!empty($this -> controlSettings['referentTable']))
               $this -> referentTableName = $this -> controlSettings['referentTable'];
            else
               throw new Exception('Setting `referentTable` for '.$this -> name.' is undefined.');

            if (!empty($this -> controlSettings['listingKey']))
               $this -> listingKey = $this -> controlSettings['listingKey'];
            else
               throw new Exception('Setting `listingKey` for '.$this -> name.' is undefined.');
        }

        if (!empty($this -> controlSettings['parentKey']))
            $this -> parentKey = $this -> controlSettings['parentKey'];

        if (!empty($this -> controlSettings['condition']))
            $this -> condition = $this -> controlSettings['condition'];

        if (!empty($this -> controlSettings['emptyValue']))
            $this -> emptyValue = $this -> controlSettings['emptyValue'];

        if (!empty($this -> controlSettings['emptyText']))
            $this -> emptyText = $this -> controlSettings['emptyText'];

        if (!empty($this -> controlSettings['orderBy']))
            $this -> orderBy = $this -> controlSettings['orderBy'];

		if(!empty($this->controlSettings['referKey']))
			$this->referKey = $this->controlSettings['referKey'];

		if(!empty($this->controlSettings['counter']))
			$this->counter = $this->controlSettings['counter'];
	}

	public function PostBackHandler(&$data, $rowID = null){}
}

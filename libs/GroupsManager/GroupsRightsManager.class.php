<?php
######################
#
#   Менеджер групповых прав
#	версия 1.0
#
######################
class GroupsRightsManager 
{
	/**
	 * Менеджер таблицы групповых прав.
	 *
	 * @var DBTableManager
	 */
	private $GroupRightsTableManager = null;
	
	
	function __construct()
	{
	}
	
	/**
	 * Узнаёт есть ли доступ у к странице или массиву страниц
	 * у пользователя
	 *
	 * @param mixed $pageID
	 * @param int $userID
	 */
	public function CheckAccess(&$pagesID, $userID)
	{
		$accessablePages = $this -> GetAccessablePagesListForUser($userID);
		if (is_array($pagesID))
		{
			for ($i = 0; $i < count($pagesID); $i++)
			{
				if (!in_array($pagesID[$i], $accessablePages) || !checkImplictAccess($accessablePages, $pagesID[$i]))
					unset($pagesID[$i]);
			}
			return $pagesID;
		}
		elseif (is_numeric($pagesID))
		{
			if (!in_array($pagesID, $accessablePages) || !checkImplictAccess($accessablePages, $pagesID))
				return false;
			else 
				return true;
		}	
	}
	
	private function checkImplictAccess(&$accessablePages, $pageID)
	{
		$struct = new SiteStructure();
		$parents = $struct -> GetParentsForPage($pageID);
		trace($parents);
		for ($i = 0; $i < count($accessablePages); $i++)
		{
			if (in_array($accessablePages[$i], $parents))return true;
		}
		return false;
	}
	
	public function GetAccessablePagesListForUser($userID)
	{
		$query = "SELECT pageID FROM `". GROUPSRIGHTS_TABLE_NAME ."` gr
					JOIN (`". USERSGROUPS_TABLE_NAME ."` ug)
					ON (gr.groupID = ug.groupID)
					WHERE ug.userID = $userID";
		return DB::QueryToArray($query);
	}
	
	public function GetAccessablePagesListForGroup($groupID)
	{
		$query = "SELECT pageID FROM `". GROUPSRIGHTS_TABLE_NAME ."` gr
					WHERE gr.groupID = $groupID";
		return DB::QueryToArray($query);
	}
	
	public function DeleteGroupRights($groupID)
	{
		$this -> InitGroupRightsTableManager();
		$this -> GroupRightsTableManager -> Delete(array('groupID' => $groupID), true);
	}
	
	public function AddGroupRights($groupID, $sections)
	{
		$this -> InitGroupRightsTableManager();
		$rows = array();
		for ($i = 0; $i < count($sections); $i++)
		{
			$rows[] = array('groupID' => $groupID, 'pageID' => $sections[$i]);
		}
		$this -> GroupRightsTableManager -> InsertRows($rows);
	}
	
	private function InitGroupRightsTableManager()
	{
		if ($this -> GroupRightsTableManager == null)
			$this -> GroupRightsTableManager = new DBTableManager(GROUPSRIGHTS_TABLE_NAME);
	}
}
?>
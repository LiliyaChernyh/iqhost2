<div style="width:550px; float:left;">
	<div id="tagsInputContainer_{$localVars.NAME}" {if isset($localVars.CLASS)}{$localVars.CLASS}{/if}>
        <div class="tagsInput_tags" id="{$localVars.NAME}_tags">
            {if isset($localVars.TAGS)}
                {foreach from=$localVars.TAGS item=TAG}
                <span>{$TAG.tag}<img src="/images/icons/cross_small.png" id="{$localVars.NAME}_tag_{$TAG.tagID}" class="{$localVars.NAME}_tagRemove tagRemove" align="top" title="Удалить тег «{$TAG.tag}»" /></span>
                {/foreach}
            {/if}
        </div>
        <div style="display: none;" id="{$localVars.NAME}_IDs">
            {if isset($localVars.TAGS)}
                {foreach from=$localVars.TAGS item=TAG}
                <input type="hidden" id="{$localVars.NAME}_tag_{$TAG.tagID}" name="{$localVars.NAME}__tags[]" value="{$TAG.tagID}" />
                {/foreach}
            {/if}
        </div>
		<div class="dynamicSelect" id="{$localVars.NAME}_container" style="margin-left: 10px;">
		    Новый тег: 
			<input type="text" id="{$localVars.NAME}_text" name="{$localVars.NAME}__text" value="" title="Начните ввод и появится подсказка" size="{$localVars.SIZE}" />
			<img src="/images/icons/add.png" class="addTagBtn" id="{$localVars.NAME}_addTag" />
			<label for="{$localVars.NAME}_text" class="error" style="display:none">Это поле необходимо заполнить</label>
			<input type="hidden" id="{$localVars.NAME}" name="{$localVars.NAME}__value" value="{$localVars.VALUE}"/>
		</div>
	</div>
</div>
<div style="clear:left"></div>
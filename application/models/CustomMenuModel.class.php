<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ManualMenuModel
 *
 * @author 
 */
class CustomMenuModel
{
    public $MenuListTN = 'MenuList';
    public $MenuTN = 'Menu';
    
    public function __construct()
    {       
    }
    
    public function GetListMenu(){
        $menuManager = new DBTableManager($this->MenuListTN);
        return $menuManager->Select();
    }
    
    public function AddMenu($attributes = array())
    {
        $menuManager = new DBTableManager($this->MenuListTN);
         $id = $menuManager->Insert($attributes);
         $innerMenuManager = new DBTableManager($this->MenuTN);
          $attr = array(                
                    'parentID'      =>  0,
                    'menuListID'      => $id,
                    'name'      => '-',
                    'alias'      => '-',
                    'priority'      => 0,
                    'isActive'      => 1
                    );
         $innerMenuManager->Insert($attr);
         return;
    }
    
    public function UpdateMenu($attributes = array(), $id)
    {
        $menuManager = new DBTableManager($this->MenuListTN);
        return $menuManager->Update($attributes, $id);
    }
    
    public function DeleteMenu($id)
    {       
         $menuManager = new DBTableManager($this->MenuListTN);
         return  $menuManager->Delete(array('menuListID' => $id));
    }
    
    public function GetParentName($id)
    {       
        
         $query = 'SELECT ml.`menuListID`, ml.`menuListName` 
                   FROM  ' . $this->MenuListTN . ' ml 
                   WHERE ml.`menuListID` = '.$id.'';
         $result = DB::QueryToArray($query);
         return $result[0];
    }
    public function GetInnerMenu($id)
    {       
         $menuManager = new DBTableManager($this->MenuTN);      
         $query = 'SELECT *
                   FROM  ' . $this->MenuTN . ' m 
                   WHERE m.`menuListID` = '.$id.' ORDER BY m.`priority` ASC';
         $result = DB::QueryToArray($query,'itemID');
         $query1 = 'SELECT m.`itemID`
                   FROM  ' . $this->MenuTN . ' m 
                   WHERE m.`name` = "-" AND m.`menuListID` = '.$id.'';
         $result1 = DB::QueryOneValue($query1);
         $catTree = Tree::Table2Tree($result, $result1, 'itemID', 'parentID');
         return  $catTree;
    }
    
    public function AddInnerMenu($attributes = array())
    {
         $innerMenuManager = new DBTableManager($this->MenuTN);
         return $innerMenuManager->Insert($attributes);
    }
    
    public function GetParent($id,$listID) {
        $query = 'SELECT *
                   FROM  ' . $this->MenuTN . ' m 
                   WHERE m.`parentID` = '.$id.' AND m.`menuListID`='.$listID.'';
        return DB::QueryToArray($query);
    }
    
    public function UpdateInnerMenu($attributes = array(), $id)
    {
        $menuManager = new DBTableManager($this->MenuTN);
        return $menuManager->Update($attributes, $id);
    }
    
    public function DeleteInnerMenu($id, $flag)
    {   $innerMenuManager = new DBTableManager($this->MenuTN);
        if ($flag)
        {         
         return  $innerMenuManager->Delete(array('itemID' => $id));     
        }
        elseif(!$flag) {
           
            $query = 'DELETE 
                      FROM  '.$this->MenuTN.' 
                      WHERE '.$this->MenuTN.'.parentID = '.$id.' OR '.$this->MenuTN.'.itemID = '.$id.'';            
                     return DB::AffectedRows(DB::Query($query)); 
        }
    
    }
    
}

?>

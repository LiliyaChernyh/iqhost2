<form method="POST" action="">
	<input type="hidden" name="routeID" value="{if !empty($ROUTE.routeID)}{$ROUTE.routeID}{/if}" />
	<div>
		<label>
			Родительская ветка:<br />
			<select name="parentID">
				<option value="0">[Корень]</option>
				{foreach from=$LINEAR_TREE item=ITEM}
					<option value="{$ITEM.routeID}" {if !empty($ROUTE.parentID) && ($ROUTE.parentID == $ITEM.routeID)}selected="selected"{/if}>{section name=sect loop=$ITEM.level+1}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{if empty($ITEM.pattern)}(Пустая строка){else}{$ITEM.pattern}{/if}</option>
				{/foreach}
			</select>
		</label>
	</div>
	<div>
		<label>
			Паттерн:<br />
			<input type="text" class="route-text" name="pattern" value="{if !empty($ROUTE.pattern)}{$ROUTE.pattern}{/if}" />
		</label>
	</div>
	<div>
		<label>
			Контроллер:<br />
			<input type="text" class="route-text" name="controller" value="{if !empty($ROUTE.controller)}{$ROUTE.controller}{/if}" />
		</label>
	</div>
	<div>
		<label>
			Действие:<br />
			<input type="text" class="route-text" name="action" value="{if !empty($ROUTE.action)}{$ROUTE.action}{else}Index{/if}" />
		</label>
	</div>
	<div>
		<label>
			Контроллер по-умолчанию:<br />
			<input type="text" class="route-text" name="defaultController" value="{if !empty($ROUTE.defaultController)}{$ROUTE.defaultController}{/if}" />
		</label>
	</div>
	<div>
		<label>
			Действие по-умолчанию:<br />
			<input type="text" class="route-text" name="defaultAction" value="{if !empty($ROUTE.defaultAction)}{$ROUTE.defaultAction}{/if}" />
		</label>
	</div>
	<div>
		<input type="submit" name="saveRoute" value="{if $mode=='edit'}Сохранить{else}Добавить{/if}" />
	</div>
</form>
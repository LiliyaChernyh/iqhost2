<?php
/**
 * для создания локальной конфигурации необходимо создать файл config-local.php
 * в котором определить дополнительные локальные настройки или переопределить настройки из основного конфига
 * Внимание! не добавляйте config-local.php в SVN
 */

DB::$DBName		 = 'iqhost';
DB::$DBUserName	 = 'iqhost';
DB::$DBPassword	 = 'bBBjtGwS';
DB::$DBHost      = '172.20.0.1';
/*DB::$DBUserName	 = 'root';
DB::$DBPassword	 = '';
DB::$DBHost      = 'localhost';*/
Password::$SALT = ''; 

// Логирование
DBLog::$LOG_ENABLED			 = true;

// debug info
FB::setEnabled(false);

// Имя домена для исполнения в CLI
AppSettings::$HTTP_HOST         = "";

// Настройка темплейтов фронта
AppSettings::$FRONT_TEMPLATE     = 'default';

// Настройка темплейтов админки
AppSettings::$ADMIN_TEMPLATE	 = 'new';
AppSettings::$ADMIN_TEMPLATE_COL = 2;
//AppSettings::$ADMIN_TEMPLATE = 'default';
//AppSettings::$ADMIN_TEMPLATE_COL = 1;

{include file='top.tpl'}

<div id="container">{include file='header.tpl'}

    {include file='navigation.tpl'}

    <br class="clear" />

	<div id="contentwrapper">
    <div id="content">
		{if isset($CONTENT_TPL)}{include file="$CONTENT_TPL"}
        {else}
        {if isset($CONTENT)}{$CONTENT}{/if}
        {/if}

	</div>
    </div>

	<div id="sidebar">
		{if isset($MENU_TPL)}{include file="$MENU_TPL"}{/if}

	</div>
</div>

{include file='footer.tpl'}
</body>
</html>
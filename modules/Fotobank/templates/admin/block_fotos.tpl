{if !empty($FOTOS)}
<ul class="list-group">
  {foreach from=$FOTOS item=FOTO}
  <li id="list-image_{$FOTO.imageID}" class="list-group-item list-image">
      <input type="hidden" value="{$FOTO.imageID}" class="block-fotos-input">
      <div class="block-image">
          {if $FOTO.count}<span class="badge count_download">{$FOTO.count}</span>{/if}
          <img src="{$FOTO.srcThumbnail}" width="152">
      </div>
      <div id="foto-info">
          <span><b>Имя файла:</b> <i>{$FOTO.name}</i></span></br>
          <span><b>Название:</b> <i>{$FOTO.caption}</i></span></br>
          <span><b>Описание:</b> <i>{$FOTO.description}</i></span>
      </div>
      <div id="foto-list-tags">
          {if isset($FOTO.tags)}
          <b>Список тегов:</b>
          <ul>
              {foreach from=$FOTO.tags item=FOTO_TAG}
              <li><i>{$TAGS[$FOTO_TAG.tagID]}</i></li>
              {/foreach}
          </ul>  
          {else}
          <b>Список тегов: пуст</b>    
          {/if}
      </div>
  </li>
  {/foreach}
</ul>
{else}
<p>Фото нет</p> 
{/if}
<?php

/**
 * Лента новостей
 *
 * @version 3.1
 *
 */
class NewsLine extends CommonModule
{

	/**
	 * Объект таблицы с новостями
	 *
	 * @var DBTableManager
	 */
	public $newsTableManager = null;

	/**
	 * Название таблицы новостей
	 *
	 * @var string
	 */
	public static $newsTableName = 'greeny_nl_news';

	/**
	 * Объект таблицы с категориями
	 *
	 * @var DBTableManager
	 */
	public $sectionsTableManager = null;

	/**
	 * Название таблицы категорий
	 *
	 * @var string
	 */
	public static $sectionsTableName = 'greeny_nl_sections';

	/**
	 * Название таблицы галереи
	 *
	 * @var string
	 */
	public static $galleryTableName = 'greeny_nl_images';

	/**
	 * Число найденных строк в методе GetNews
	 *
	 * @var int
	 */
	public $foundNewsCnt = 0;

	/**
	 * Конструктор.
	 *
	 * @return News
	 */
	public function __construct()
	{
		parent::__construct(__CLASS__);

		$this->newsTableManager				 = new DBTableManager(self::$newsTableName);
		$this->newsTableManager->SortOrder	 = '`publicationDate` DESC, `modificationDate` DESC, `newsID` DESC';

		$this->sectionsTableManager				 = new DBTableManager(self::$sectionsTableName);
		$this->sectionsTableManager->SortOrder	 = '`sectionID` ASC';
	}

	public function GetSections($mode = 0)
	{
        $sections = $this->sectionsTableManager->Select();
//        if (empty($sections))
//            return array(array("caption" => "Новости", 'alias' => ""));
        if ($mode != 0)
            array_unshift($sections, array('caption' => "Все новости", 'alias' => ""));
        if ($mode == 2)
            array_push($sections, array('caption' => "Без категорий", 'alias' => "unsort"));
        $sec      = array();
        foreach ($sections as $item)
        {
            $item['fullAlias']   = "/" . $this->settings['rootAlias'] . "/" . (!empty($item['alias']) ? $item['alias'] . "/" : '');
            $sec[$item['alias']] = $item;
        }
        return $sec;
    }

    public function AddSections($attr)
	{
		return $this->sectionsTableManager->Insert($this->CheckSection($attr));
	}

	public function CheckSection($attr)
	{
		if (!isset($attr['alias']))
		{
			$attr['alias'] = strtolower(Transliter::TranslitToStrictName($attr['caption']));
		}
		return $attr;
	}

	public function UpdateSections($sectionID, $caption)
	{
		return $this->sectionsTableManager->Update(array('caption' => $caption), $sectionID);
		;
	}

	public function WipeSections($arraySectionID)
	{
		foreach ($arraySectionID as $value)
			$this->sectionsTableManager->Delete(array('sectionID' => $value));
	}

	/**
	 * Производит 'мягкое' удаление новости (помещение в корзину)
	 *
	 * @param int $newsID
	 */
	public function DeleteNews($newsID)
	{
		// Проверяем корректность переданных данных
		if (!is_numeric($newsID))
			throw new ArgumentException('Неверный идентификатор новости');

		$this->newsTableManager->Update(array('isDeleted' => 1), $newsID);
	}

	/**
	 * Производит восстановление новости из корзины
	 *
	 * @param int $newsID
	 */
	public function RestoreNews($newsID)
	{
		// Проверяем корректность переданных данных
		if (!is_numeric($newsID))
			throw new ArgumentException('Неверный идентификатор новости');

		$this->newsTableManager->Update(array('isDeleted' => '0'), $newsID);
	}

	/**
	 * Подсчитывает количество новостей с заданными атрибутами
	 *
	 * @param array $attributes
	 */
	public function NewsNumber($attributes)
	{
		return $this->newsTableManager->Count($attributes);
	}

	/**
	 * Возвращает массив новостей, удовлетворяющийх заданным атрибутам
	 *
	 * @param array $filters
	 * @param int $limit
	 * @param int $start
	 * @return array
	 */
	public function GetNews($filters = null, $limit = null, $start = null)
	{
		$lim = (!empty($limit) && is_numeric($limit)) ? (' LIMIT ' .
			((!is_numeric($start)) ? 0 : $start) .
			', ' . $limit) : '';

		$condition	 = '';
		$joins		 = '';

		if (!is_null($filters))
		{
			$condArray	 = array();
			$joinsArray	 = array();
			foreach ($filters as $filter => $value)
			{
				switch ($filter)
				{
					case 'published':
						switch ($value)
						{
							case 'yes':
								$condArray[] = 'n.`publicationDate` <= ' . DB::Quote(date('Y-m-d H:i:s'));
								break;

							case 'no':
								$condArray[] = 'n.`publicationDate` > ' . DB::Quote(date('Y-m-d H:i:s'));
								break;
						}
						break;

					case 'hasRelease':
						if ($value == 1)
						{
							$condArray[] = 'n.`release` IS NOT NULL';
						}
						break;

					default:
						if (!is_null($value))
						{
							$condArray[] = 'n.`' . $filter . '` = ' . DB::Quote($value);
						}
						break;
				}
			}
			if (!empty($condArray))
				$condition	 = ' WHERE ' . implode(' AND ', $condArray);
			if (!empty($joinsArray))
				$joins		 = ' ' . implode(' ', $joinsArray);
		}

		$query = "
                SELECT SQL_CALC_FOUND_ROWS DISTINCT
                    n.*,
                    ns.*
                FROM `" . self::$newsTableName . "` n
                LEFT JOIN `" . self::$sectionsTableName . "` ns ON n.`sectionID` = ns.`sectionID`
                    " . $joins . "
                    " . $condition . "
                ORDER BY " . $this->newsTableManager->SortOrder . "
                    " . $lim;

		$result				 = DB::QueryToArray($query);
		$this->foundNewsCnt	 = DB::QueryOneValue("SELECT FOUND_ROWS()");

		return $result;
	}

	/**
	 * Возвращает новости, удовлетворяющую заданным атрибутам и с указанным полями
	 *
	 * @param array $attributes
	 * @return array
	 */
	public function GetFieldsNews($attributes = null, $fields = null)
	{
		return $this->newsTableManager->Select($attributes, null, null, $fields);
	}

	/**
	 * Возвращает новость с указанным ID
	 *
	 * @param array $attributes
	 * @return array
	 */
	public function GetSingleNews($newsID)
	{
		return $this->newsTableManager->Select(array('newsID' => $newsID));
	}

	public function WipeNews($newsID)
	{
		$this->newsTableManager->Delete(array('newsID' => $newsID));
	}

	/**
	 * Создание пустого элемента модуля при создании страницы, ассоциированной с модулем.
	 * Так как, в данном модуле создавать пустой элемент не нужно, функция просто возвращает успех.
	 *
	 * @param array $moduleNameArray
	 * @return mixed
	 */
	public function CreateEmptyElement($moduleNameArray)
	{
		return true;
	}

	public function DeleteObject()
	{
		return true;
	}

}

?>
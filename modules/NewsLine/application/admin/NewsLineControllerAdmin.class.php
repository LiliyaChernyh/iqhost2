<?php

/**
 * Администраторская часть модуля новостей
 *
 * @version 3.1
 *
 */
class NewsLineControllerAdmin extends AdminController {

    /**
     * Объект класса новостей
     *
     * @var NewsLine
     */
    private $newsLine = null;

    /**
     * Путь к шаблонам модуля
     *
     * @var string
     */
    private $moduleTplsPath = null;

    /**
     * Алиас данного модуля
     *
     * @var string
     */
    private $moduleAlias = null;
    private $fullAliases = null;

    /**
     * Физический путь к модулю
     *
     * @var string
     */
    private $modulePath = null;

    public function __construct() {
        parent::__construct();
        $this->fullAliases = Request::GetAliases();

        // Указываем относительный путь к админским шаблонам модуля
        $this->moduleTplsPath = IncPaths::$MODULES_PATH . 'NewsLine/templates/';
        $this->moduleAlias = AppSettings::$ADMIN_ALIAS . 'NewsLine/';
        $this->modulePath = IncPaths::$MODULES_PATH . 'NewsLine/';
        $this->view = new NewsLineViewAdmin();
        $this->newsLine = new NewsLine();

        ViewData::Assign('MODULE_TPLS_PATH', $this->moduleTplsPath);
        ViewData::Assign('MODULE_ALIAS', $this->moduleAlias);
        ViewData::Assign('DELETED_NEWS_NUMBER', $this->newsLine->NewsNumber(array('isDeleted' => 1)));
        ViewData::Append('JS_VARS', array('MODULE_ALIAS' => $this->moduleAlias));
    }

    /**
     * Возвращает HTML код страницы
     *
     * @return string
     */
    public function Index($args = null) {
        //получаем список категорий
        $sections = $this->newsLine->GetSections();
        ViewData::Assign('SECTIONS', $sections);

        if (isset($this->fullAliases[2])) {
            switch ($this->fullAliases[2]) {
                // Просмотр списка новостей
                case 'list':
                    return $this->NewsList(isset($this->fullAliases[3]) ? $this->fullAliases[3] : null);
                    break;

                // Добавление новости
                case 'add':
                    return $this->AddNews();
                    break;

                // Редактирование новости
                case 'edit':
                    return $this->EditNews();
                    break;

                // Удаление новости
                case 'delete':
                    return $this->DeleteNews();
                    break;

                // Удаление нескольких новостей
                case 'deletes':
                    return $this->DeleteManyNews();
                    break;

                // Работа с корзиной новостей
                case 'recycled':
                    return $this->Recycled();
                    break;

                // Категории
                case 'sections':
                    return $this->Sections();
                    break;

                //  Путеводитель
                case 'guide':
                    return $this->Guide();
                    break;

                default:
                    return $this->NewsList();
                    break;
            }
        } else {
            return $this->NewsList();
        }
    }

    /**
     * Показывает список всех категорий с возможностью редактирования.
     *
     */
    private function Sections() {
        if (isset($_POST['doAdd']) && isset($_POST['section'])) {
            $sections = $this->newsLine->AddSections(array('caption' => $_POST['section']));
        } else if (isset($_POST['doDel']) && isset($_POST['sections'])) {
            $arraySectionID = $_POST['sections'];
            $sections = $this->newsLine->WipeSections($arraySectionID);
        } else if (isset($_POST['doSave']) && isset($_POST['sections'][0]) && isset($_POST['section'])) {
            $this->newsLine->UpdateSections($_POST['sections'][0], $_POST['section']);
        }

        //получаем список категорий
        $sections = $this->newsLine->GetSections();
        ViewData::Assign('SECTIONS', $sections);
        $this->view->Sections();
        return $this->view;
    }

    /**
     * Показывает список всех новостей.
     * При этом они разбиваются на страницы.
     *
     */
    private function NewsList($section = null) {
        $newsOnPageNumber = $this->newsLine->settings['MessagesOnPageAdmin'];

        // Определяем номер страницы
        if (isset(Request::$GET['page']) && is_numeric(Request::$GET['page']))
            $pageNumber = Request::$GET['page'];
        else
            $pageNumber = 1;

        $attrs = array('isDeleted' => 0);
        if (!is_null($section)) {
            $attrs['sectionID'] = $section;
            ViewData::Assign('CUR_SECTION', $section);
        }

        $newsArray = $this->newsLine->GetNews($attrs, $newsOnPageNumber, ($pageNumber - 1) * $newsOnPageNumber);
        ViewData::Assign('NEWS_LIST', $newsArray);

        $allNewsNumber = $this->newsLine->NewsNumber($attrs);

        $hrefPrefix = $this->moduleAlias . 'list/' . (!is_null($section) ? 'section/' . $section . '/' : '') . 'page/';
        $this->setPaginator($newsOnPageNumber, $allNewsNumber, $pageNumber);

        $this->view->NewsList();
        return $this->view;
    }

    /**
     * Добавление новости
     *
     */
    private function AddNews() {
        if (isset($_POST['doSave'])) {
            $news = $_POST;
            $contentManager = new ContentManager();
            try {
                $newsID = $contentManager->FlushData(NewsLine::$newsTableName, $news);

                if (isset($_POST['async']))
                    return new JsonActionResult(array("msg" => "ok", "newsID" => $newsID, "refresh" => ($contentManager->formRefreshRequired ? true : false )));
                else
                    $this->Success('Данные сохранены', $this->moduleAlias . 'edit/' . $newsID . '/');
            } catch (Exception $e) {
                if (isset($_POST['async']))
                    die('{"error":"' . $e->getMessage() . '"}');
                else
                    $this->Error($e->getMessage(), $this->moduleAlias . 'add/');
            }
        }

        return $this->SetForm(NewsLine::$newsTableName, 'Создание новости');
    }

    private function EditNews() {
        $newsID = $this->fullAliases[3]; //('edit');

        if (is_null($newsID) || !is_numeric($newsID)) {
            if (Request::IsAsync()) {
                return new JsonActionResult(array('error' => "Возникла ошибка при редактировании новости: не указан ID новости"));
            } else {

                $this->Error('Возникла ошибка при редактировании новости: не указан ID новости', $this->moduleAlias);
            }
        }
        if (isset($_POST['doSave'])) {
            $contentManager = new ContentManager();
            $news = $_POST;

            try {
                $contentManager->FlushData(NewsLine::$newsTableName, $news);

                if (Request::IsAsync()) {
                    return new JsonActionResult(array("msg"=>"ok", "newsID"=> $newsID, "refresh"=>($contentManager->formRefreshRequired ? 'true' : 'false')));
                } else {
                    $this->Success('Данные сохранены', $this->moduleAlias . 'edit/' . $newsID . '/');
                }
            } catch (Exception $e) {
                if (Request::IsAsync()) {
                    return new JsonActionResult(array("error"=>$e->getMessage()));
                }
                else{
                    $this->Error($e->getMessage(), $this->moduleAlias . 'edit/' . $newsID . '/');
                }
            }
        }


        $delete = array(
            'url' => $this->moduleAlias . 'delete/',
            'name' => 'newsID',
            'value' => $newsID
        );
        return $this->SetForm(NewsLine::$newsTableName, 'Редактирование новости', $newsID, $delete);
    }

    private function DeleteNews() {
        if (isset($_POST['doDelete']) || isset($_POST['doDelete_x'])) {
            if (!isset($_POST['newsID']) || (!is_numeric($_POST['newsID'])))
                $this->Error('Возникла ошибка при удалении новости: не указан id новости', $this->moduleAlias);
            else
                return $newsID = $_POST['newsID'];

            try {
                $this->newsLine->DeleteNews($newsID);
                return $this->Success('Новость перемещена в корзину', $this->moduleAlias);
            } catch (Exception $e) {
                return $this->Error('Возникла ошибка при удалении новости: ' . $e->getMessage(), $this->moduleAlias . 'edit/' . $newsID . '/');
            }
        }
    }

    private function DeleteManyNews() {
        if (isset($_POST['doDelete']) || isset($_POST['doDelete_x'])) {
            if (!isset($_POST['toDelete']) || empty($_POST['toDelete']))
                $this->Error('Не отмечаны новости для удаления');

            $toDelete = $_POST['toDelete'];

            try {
                foreach ($toDelete as $newsID) {
                    $this->newsLine->DeleteNews($newsID);
                }
                return $this->Success('Отмеченные новости перемещены в корзину', $this->moduleAlias);
            } catch (Exception $e) {
                $this->Error('Возникла ошибка при удалении новости: ' . $e->getMessage(), $this->moduleAlias);
            }
        }
    }

    private function Recycled() {
        if (isset($_POST['doRestore'])) {
            $toEdit = (isset($_POST['toEdit'])) ? $_POST['toEdit'] : array();

            foreach ($toEdit as $newsID) {
                try {
                    $this->newsLine->RestoreNews($newsID);
                } catch (Exception $e) {
                    $this->Error('Возникла ошибка при восстановлении новости: ' . $e->getMessage(), $this->moduleAlias . 'recycled/');
                }
            }
            $this->Success('Отмеченные новости были восстановлены', $this->moduleAlias);
        }

        if (isset($_POST['doWipe'])) {
            $toEdit = (isset($_POST['toEdit'])) ? $_POST['toEdit'] : array();

            foreach ($toEdit as $newsID) {
                try {
                    $this->newsLine->WipeNews($newsID);
                } catch (Exception $e) {
                    $this->Error('Возникла ошибка при удалении новости: ' . $e->getMessage(), $this->moduleAlias . 'recycled/');
                }
            }

            $this->Success('Отмеченные новости были удалены', $this->moduleAlias . 'recycled/');
        }
        $this->view->Recycled();
        $newsArray = $this->newsLine->GetNews(array('isDeleted' => 1));
        ViewData::Assign('NEWS_LIST', $newsArray);
        return $this->view;
    }

    protected function SetForm($tableName, $title, $rowID = 0, $delete = null) {
        $this->view->AssignFormToTpl($tableName, $rowID);
        $this->view->Form();
        ViewData::Assign('TITLE', $title);
        if (!is_null($delete))
            ViewData::Assign('DELETE', $delete);
        return $this->view;
    }

    /**
     * Путеводитель :)
     *
     */
    private function Guide() {
        if (isset(parent::$RequestedAlias[2]) && (parent::$RequestedAlias[2] == 'newsline')) {
            header('Location: ' . $this->moduleAlias);
        } else {
            $this->Error('Ошибка перенаправления', $_SERVER['HTTP_REFERER']);
        }
    }

}

?>
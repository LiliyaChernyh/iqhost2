<?php
/**
 * Класс, инкапсулирующий функции работы с файлами
 * 
 * @version 1.1
 *
 */
class File
{
    const READ = 'READ';
    const WRITE = 'WRITE';
    const APPEND = 'APPEND';
    const READWRITE = 'READWRITE';

    private $path = null;
    private $stream = null;
    
    public function __construct($path, $mode = self::READ)
    {
        switch ($mode)
        {
            case self::READ:
            case self::READWRITE:
                if (!self::Exists($path))
                    throw new FileNotFoundException('Can not create stream. File not found.', 12001);
                break;
            case self::WRITE:
                $this -> path = $path;
                $this -> stream = @fopen($path, 'w+');
                if ($this -> stream === false)
                throw new Exception('Can not write file', 12002);
                break;
        }
    }
    
    public function __destruct()
    {
        if (!empty($this -> stream))
            fclose($this -> stream);
    }
    
    public function Write($contents)
    {
        if (!empty($this -> stream))
            fwrite($this -> stream, $contents);
    }
    
    public static function Exists($filePath)
    {
        return file_exists($filePath);
    }
    
    public static function Delete($filePath)
    {
    	if (!is_file($filePath))
    		throw new Exception($filePath . ' is not a file');

    	if (!is_writable($filePath))
    		throw new Exception($filePath . ' is not writable — can not delete it');
    	
    	unlink($filePath);
    }
}
?>
<?php
class DateFunctions
{
    /**
     * Названия знаков зодиака
     *
     * @var Array
     */
    public static $RuZodiacSignsNames = array('Овен', 'Телец', 'Близнецы', 
                                              'Рак', 'Лев', 'Дева', 
                                              'Весы', 'Скорпион', 'Стрелец',
                                              'Козерог', 'Водолей', 'Рыбы');
    /**
     * Zodiac signs names
     *
     * @var Array
     */
    public static $EnZodiacSignsNames = array('Aries', 'Taurus', 'Gemini', 
                                              'Cancer', 'Leo', 'Virgo', 
                                              'Libra', 'Scorpio', 'Sagittarius',
                                              'Capricorn', 'Aquarius', 'Pisces');

    /**
     * Преобразовывает дату в номер знака зодиака
     *
     * @param string $date
     * @return int
     */
    public static function DateToZodiacNum($date)
    {
        $stamp = (is_numeric($date)) ? $date : strtotime($date);
        if (!$stamp) return false;
        $parsedDate = getdate($stamp);
        $d = $parsedDate['mday'];
        $m = $parsedDate['mon'];
        if ($m == 1 && $d >=20 || $m == 2 && $d <=18) 
			return 10;
		else if ($m == 2 && $d >=19 || $m == 3 && $d <=20) 
			return 11;
		else if ($m == 3 && $d >=21 || $m == 4 && $d <=19) 
			return 0;
		else if ($m == 4 && $d >=20 || $m == 5 && $d <=20) 
			return 1;
		else if ($m == 5 && $d >=21 || $m == 6 && $d <=21) 
			return 2;
		else if ($m == 6 && $d >=22 || $m == 7 && $d <=22) 
			return 3;
		else if ($m == 7 && $d >=23 || $m == 8 && $d <=22) 
			return 4;
		else if ($m == 8 && $d >=23 || $m == 9 && $d <=22) 
			return 5;
		else if ($m == 9 && $d >=23 || $m == 10 && $d <=22) 
			return 6;
		else if ($m == 10 && $d >=23 || $m == 11 && $d <=21) 
			return 7;
		else if ($m == 11 && $d >=22 || $m == 12 && $d <=21) 
			return 8;
		else if ($m == 12 && $d >=22 || $m == 1 && $d <=19) 
			return 9;
    }
}
?>
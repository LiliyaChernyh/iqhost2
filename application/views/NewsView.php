<?php

class NewsView extends FrontView {

    public function __construct() {
        parent::__construct();
        $this->tplsPath .= 'news/';
    }

    public function Index() {
        $this->tpl->contentTpl = $this->tplsPath . 'index.tpl';
        return $this;
    }

    public function Item() {
        $this->tpl->contentTpl = $this->tplsPath . 'item.tpl';
        return $this;
    }

}

<h1>Профиль [<a href="{$SECTION_ALIAS}edit/" title="Редактировать профиль"><img src="{$ADMIN_IMAGES}edit.png" alt="редактировать" class="noborder" /></a>]</h1>

<div class="profile_row">
    <div class="profile_rowLeft">&nbsp;</div>
    <div class="profile_rowRight" style="font-size: 200%;">{$USER.login}</div>
</div>

<div class="profile_row">
    <div class="profile_rowLeft">Тип пользователя:</div>
    <div class="profile_rowRight">{$USER.roleInfo.name}</div>
</div>

<div class="profile_row">
    <div class="profile_rowLeft">Электронная почта:</div>
    <div class="profile_rowRight">{$USER.email}</div>
</div>

<div class="profile_row">
    <div class="profile_rowLeft">Дата регистрации:</div>
    <div class="profile_rowRight">{$USER.registrationDate}</div>
</div>

<div class="profile_row" style="margin-top: 10px;">
    <div class="profile_rowLeft">&nbsp;</div>
    <div class="profile_rowRight"><h2 style="margin: 0;">Информация профиля</h2></div>
</div>

<div class="profile_row">
    <div class="profile_rowLeft">ФИО:</div>
    <div class="profile_rowRight">{$USER.info.FIO}</div>
</div>

<div class="profile_row">
    <div class="profile_rowLeft">Дата рождения:</div>
    <div class="profile_rowRight">{$USER.info.birthdate|date_format:"%d.%m.%Y"}</div>
</div>

<div class="profile_row">
    <div class="profile_rowLeft">Пол:</div>
    <div class="profile_rowRight">{$USER.info.gender}</div>
</div>

<div style="clear: both;"></div>
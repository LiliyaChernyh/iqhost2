<?php

/**
 * Общий класс для всех модулей. Его наследуют все модули.
 *
 * @version 1.0
 * 
 */
class CommonModule
{
	/**
	 * Конструктор
	 *
	 */
	public function __construct($moduleName)
	{
            $this -> settings = self::LoadSettings($moduleName);
	}
	
	/**
	 * Настройки модуля
	 *
	 * @var array
	 */
	public $settings = null;
	
	/**
	 * Загружает настройки модуля
	 *
	 * @param string $moduleName Название модуля
	 */
	static function LoadSettings($moduleName)
	{
		$query = 'SELECT ms.* FROM `'. TablesNames::$MODULES_SETTINGS_TABLE_NAME . '` ms
					JOIN `' . TablesNames::$MODULES_TABLE_NAME . "` m
					ON m.`moduleID` = ms.`moduleID`
					WHERE m.`name` = '$moduleName'";

		return DB::QueryToArray($query, 'name', 'value');
	}
}

?>
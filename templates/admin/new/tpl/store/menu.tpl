<ul>
        <li class="has-drop {if !empty($METHOD_ALIAS) AND  ($METHOD_ALIAS == 'options' or $METHOD_ALIAS == 'attributes')} active{/if}">
            <a href="{$CONTROLLER_ALIAS}options/" title="Справочники">Справочники</a>
            <ul>
                <li{if !empty($METHOD_ALIAS) AND  $METHOD_ALIAS == 'options'} class="active"{/if}><a href="{$CONTROLLER_ALIAS}options/" title="Справочники">Справочник опций</a></li>
                <li{if !empty($METHOD_ALIAS) AND  $METHOD_ALIAS == 'attributes'} class="active"{/if}><a href="{$CONTROLLER_ALIAS}attributes/" title="Справочники">Справочник атрибутов</a></li>
            </ul>
        </li>

	<li{if !empty($METHOD_ALIAS) AND  $METHOD_ALIAS == 'brands'} class="active"{/if}>
            <a href="{$CONTROLLER_ALIAS}brands/" title="Бренды">Бренды</a>
        </li>

	<li{if !empty($METHOD_ALIAS) AND  $METHOD_ALIAS == 'collections'} class="active"{/if}>
            <a href="{$CONTROLLER_ALIAS}collections/" title="Категории">Коллекции</a>
        </li>

        <li{if !empty($METHOD_ALIAS) AND  $METHOD_ALIAS == 'categories'} class="active"{/if}>
            <a href="{$CONTROLLER_ALIAS}categories/" title="Типы товаров">Категории товаров</a>
        </li>
		<li{if !empty($METHOD_ALIAS) AND  $METHOD_ALIAS == 'products' and !empty($METHOD_SUB_ALIAS)} class="active"{/if}>
			<a href="{$CONTROLLER_ALIAS}products/" title="Товары">Товары</a>
			{if $METHOD_ALIAS == 'products' and !empty($METHOD_SUB_ALIAS)}
				<ul>
			<li{if !empty($METHOD_ALIAS) AND  $METHOD_SUB_ALIAS == 'products'} class="active" {/if}>
				<a href="{$CONTROLLER_ALIAS}products/" title="Все товары" class="allproducts">Все товары (<strong class='unsorted-count'>{if !empty($PRODUCTS_COUNTER.enabled)}{$PRODUCTS_COUNTER.enabled}{else}0{/if}</strong>)</a>
			</li>
			<li{if !empty($METHOD_ALIAS) AND  $METHOD_SUB_ALIAS == 'sortproducts'} class="active" {/if}>
				<a href="{$CONTROLLER_ALIAS}sortproducts/" title="Без категорий" class="unsortproducts">Без категорий (<strong class='unsorted-count'>{if !empty($PRODUCTS_COUNTER.unsorted)}{$PRODUCTS_COUNTER.unsorted}{else}0{/if}</strong>)</a>
			</li>
			<li{if !empty($METHOD_ALIAS) AND  $METHOD_SUB_ALIAS == 'disabledproducts'} class="active" {/if}>
				<a href="{$CONTROLLER_ALIAS}products/disabledproducts/" title="Отключенные" class="blockproduct">Отключенные (<strong class='unsorted-count'>{if !empty($PRODUCTS_COUNTER.disabled)}{$PRODUCTS_COUNTER.disabled}{else}0{/if}</strong>)</a>
			</li>
			<li{if !empty($METHOD_ALIAS) AND  $METHOD_SUB_ALIAS == 'deletedproducts'} class="active" {/if}>
				<a href="{$CONTROLLER_ALIAS}products/deletedproducts/" title="Черный список" class="delproduct">Черный список / Удаленные (<strong class='unsorted-count'>{if !empty($PRODUCTS_COUNTER.deleted)}{$PRODUCTS_COUNTER.deleted}{else}0{/if}</strong>)</a>
			</li>

		</ul>
		{/if}
	</li>
	<li{if !empty($METHOD_ALIAS) AND  $METHOD_ALIAS == 'orders'} class="active"{/if}>
		<a href="{$CONTROLLER_ALIAS}orders/" title="Заказы">Заказы</a>
        </li>
	</li>
		<li{if !empty($METHOD_ALIAS) AND  $METHOD_ALIAS == 'import'} class="active"{/if}>
            <a href="{$CONTROLLER_ALIAS}import/" title="Импорт товаров">Импорт товаров</a>
        </li>
    </ul>
			<div id="sidebar_bottom"></div>
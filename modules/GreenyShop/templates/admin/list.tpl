<h1>Список заказов</h1>

{if $ORDERS}
	{if $PAGINATOR}
	<div class="paginator">
		<span>{$PAGINATOR.itemsOnPage}</span> 
		<a href="{$PAGINATOR.firstLink}">1</a>  
		<a href="{$PAGINATOR.secondLink}">2</a> 
		<span>{$PAGINATOR.current}</span>/<span>{$PAGINATOR.total}</span>
		{if $PAGINATOR.nextLink} <a href="{$PAGINATOR.nextLink}">След</a>{/if}
	</div>
	{/if}
	<div style="clear:left;"></div>

	<table id="orders" class="highlightable">
	<tr>
		<th width="10%">Номер заказа</th>
		<th width="20%">Дата создания</th>
		<th width="20%">Статус</th>
		<th width="50%">Метка</th>
	</tr>
	{foreach from=$ORDERS item=ORDER}
	<tr class="{$ORDER.status}">
		<td><a href="{$ORDER.LINK}">{$ORDER.orderID}</a></td>
		<td>{$ORDER.creationDate}</td>
		<td>{$ORDER.statusDesc}</a></td>
		<td>{$ORDER.tip}</td>
	</tr>
	{/foreach}
	</table>

	{if $PAGINATOR}
	<div class="paginator">
		<span>{$PAGINATOR.itemsOnPage}</span> 
		<a href="{$PAGINATOR.firstLink}">1</a>  
		<a href="{$PAGINATOR.secondLink}">2</a> 
		<span>{$PAGINATOR.current}</span>/<span>{$PAGINATOR.total}</span>
		{if $PAGINATOR.nextLink} <a href="{$PAGINATOR.nextLink}">След</a>{/if}
	</div>
	{/if}
{else}
<div style="clear:both">Заказов нет.</div>
{/if}
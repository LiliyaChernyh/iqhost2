<?php
/**
 * Менеджер главного меню сайта
 *
 * @version 1.0
 */
class MainMenuManager 
{
    public static $mainMenuTableName = 'MainMenu';

    /**
     * Менеджер таблицы главного меню
     *
     * @var DBTableManager
     */
    private $mainMenuTableMngr;

    public function __construct()
    {
        if (!empty(TablesNames::$MAIN_MENU))
            self::$mainMenuTableName = TablesNames::$MAIN_MENU;
    }
    
    private function InitTableMngr()
    {
        if (empty($this -> mainMenuTableMngr))
            $this -> mainMenuTableMngr = new DBTableManager(self::$mainMenuTableName);
    }

    public function AddPageToMainMenu($pageID)
    {
        if ($this -> IsPageInMainMenu($pageID))
            return false;
        if (!is_numeric($pageID))
            throw new ArgumentException('pageID must be numeric');
        $this -> InitTableMngr();
        return $this -> mainMenuTableMngr -> Insert(array('pageID' => $pageID));
    }

    public function RemovePageFormMainMenu($pageID)
    {
        if (!is_numeric($pageID))
            throw new ArgumentException('pageID must be numeric');
        $this -> InitTableMngr();
        return $this -> mainMenuTableMngr -> Delete(array('pageID' => $pageID));
    }

    public function IsPageInMainMenu($pageID)
    {
        if (!is_numeric($pageID))
            throw new ArgumentException('pageID must be numeric');
        $this -> InitTableMngr();
        return $this -> mainMenuTableMngr -> Count(array('pageID' => $pageID)) > 0;
    }

    public function GetMainMenu($retreiveFullAliases = true, $onlyRootPages = false)
    {
        $onlyRootCond = '';
        if ($onlyRootPages)
            $onlyRootCond = ' AND p.`parentID` = 0';
        
        $query = 'SELECT * FROM '.self::$mainMenuTableName .' mm
                 JOIN '.TablesNames::$PAGE_STRUCTURE_TABLE_NAME .' p
                 ON (p.pageID = mm.pageID) 
                 WHERE p.isActive = 1 
                 AND p.isDeleted = 0 
                 '. $onlyRootCond .'
                 ORDER BY p.`pageID` ASC, p.priority DESC';
        $mainMenu = DB::QueryToArray($query);

        if ($retreiveFullAliases)
        {
            $pageStructure = PagesStructure::getInstance();
            for ($i = count($mainMenu) - 1; $i >= 0; $i--)
            {
                $mainMenu[$i]['fullAlias'] = $pageStructure -> GetFullAlias($mainMenu[$i]['pageID']);
            }
        }
        return $mainMenu;
    }
    
    public function GetMainMenuTree($parentID = 0)
    {
        $parentCond = ' AND p.`parentID` = '. $parentID;
        
        $query = '
            SELECT  *
            FROM    `'.self::$mainMenuTableName .'` mm
            JOIN    '.TablesNames::$PAGE_STRUCTURE_TABLE_NAME .' p
                 ON (p.`pageID` = mm.`pageID`) 
            WHERE   p.isActive = 1 
                AND p.isDeleted = 0 
            '. $parentCond .'
            ORDER BY p.priority DESC
        ';
        
        $pages = DB::QueryToArray($query);
        
        $pageStructure = PagesStructure::getInstance();
        
        if (empty($pages))
            return null;
        else 
        {
            $pagesCnt = count($pages);
            for ($i = 0; $i < $pagesCnt; $i++)
            {
	            $pages[$i]['fullAlias'] = $pageStructure -> GetFullAlias($pages[$i]['pageID']);
                $subPages = $this -> GetMainMenuTree($pages[$i]['pageID']);
                if (!empty($subPages))
                    $pages[$i]['children'] = $subPages;
            }
        }
        
        return $pages;
    }
	
	
	/**
     * Подтягиевает подменю
     * (внутренние страницы раздела)
     * 
     * @param int $parentID  id родительского страницы
     * @return array
     */
    public function GetSubMenuTree($parentID = 0)
    {
        $parentCond = ' AND p.`parentID` = '. $parentID;
        
        $query = '
            SELECT  *
            FROM '.TablesNames::$PAGE_STRUCTURE_TABLE_NAME .' p
            WHERE   p.isActive = 1 
                AND p.isDeleted = 0 
            '. $parentCond .'
            ORDER BY p.priority DESC
        ';
        
        $pages = DB::QueryToArray($query);
        
        $pageStructure = PagesStructure::getInstance();
        
        if (empty($pages))
            return null;
        else 
        {
            $pagesCnt = count($pages);
            for ($i = 0; $i < $pagesCnt; $i++)
            {
             $pages[$i]['fullAlias'] = $pageStructure -> GetFullAlias($pages[$i]['pageID']);
                $subPages = $this -> GetSubMenuTree($pages[$i]['pageID']);
                if (!empty($subPages))
                    $pages[$i]['children'] = $subPages;
            }
        }
        
        return $pages;
    }
}
?>
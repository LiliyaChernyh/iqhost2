<?php

/**
 * Класс, позволяющий проверять вводимые пользователем данные.
 *
 */
class Validator 
{
	/**
	 * Проверяет корректность формата e-mail
	 *
	 * @param string $email
	 * @return bool
	 */
	public static function EmailValidator($mail)
	{
		// Вырезаем крайние пробелы и переводим в нижний регистр
   		$mail = trim( strtolower($mail) );
 		
   		// Если строка пустая, возвращаем false
   		if (strlen($mail) == 0) 
   			return false;
   			
   		// Проверяем, чтобы в e-mail был ровно 1 символ @ и чтобы он был не в начала либо конце
		if (!ereg("[^@]{1,64}@[^@]{1,255}", $mail)) 
		{
			return false;
		}
		
		// Разбиваем e-mail на 2 части: до символа @ и после
		$mail_array = explode("@", $mail);
		
		// Разбиваем первую чать на символу "."
		$local_array = explode(".", $mail_array[0]);
		
		// Проверяем каждый кусок на соответствие регулярному выражению
		// и в случае несоответствия возвращается false
		for ($i = 0; $i < sizeof($local_array); $i++) 
		{
			if (!ereg("^[_a-z0-9-]+$", $local_array[$i])) 
			{
				return false;
			}
		}

		// Проверяем доменную часть e-mail
		if (!ereg("^[?[0-9.]+]?$", $mail_array[1])) 
		{
			$domain_array = explode(".", $mail_array[1]);
			if (sizeof($domain_array) < 2) 
			{
				return false;
			}
			
			for ($i = 0; $i < sizeof($domain_array); $i++) 
			{
				if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) 
				{
					return false;
				}
			}
		}

   		// Все нормально! E-mail корректен
   		return true;
	}
	
	/**
	 * Проверяет корректность URL
	 *
	 * @param string $url
	 * @return bool
	 */
	public static function UrlValidator($url)
	{
		// Вырезаем "левые" символы и крайние пробелы
		$url = trim( $url );
		
		// Если строка пустая, возвращаем false
		if (strlen($url) == 0)
			return false;
			
		// Проверяем URL на правильность с помощью регулярного выражения
		if (!preg_match("~^(?:(?:https?|ftp|telnet)://(?:[a-z0-9_-]{1,32}".
		   				"(?::[a-z0-9_-]{1,32})?@)?)?(?:(?:[a-z0-9-]{1,128}\.)+(?:com|net|".
		   				"org|mil|edu|arpa|gov|biz|info|aero|inc|name|[a-z]{2})|(?!0)(?:(?".
		   				"!0[^.]|255)[0-9]{1,3}\.){3}(?!0|255)[0-9]{1,3})(?:/[a-z0-9.,_@%&".
		   				"?+=\~/-]*)?(?:#[^ '\"&<>]*)?$~i", $url, $ok))
		   	return false;
		
		// Все нормально! URL корректен
		return true;
	}
	
	/**
	 * Проверяет строку на корректность формата даты
	 *
	 * @param string $date
	 * @return bool
	 */
	public static function DateValidator($date)
	{
		if (strtotime($date) == -1)
			return false;
		else 
			return true;
	}
	
	/**
	 * Проверяет IP адрес на валидность
	 *
	 * @param string $IP
	 * @return bool
	 */
	public static function IPValidator($IP)
    {
        // Для начала проверим формат IP адреса
        if (preg_match("/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/", $IP))
        {
            // Разделим IP на 4 части
            $parts = explode(".", $IP);

            // Проверим, чтобы каждая часть была целым числом в диапазоне 0-255
            for ($i = 0; $i < 4; $i++)
            {
                // Если не в диапазоне, возвращаем false
                if (intval($parts[$i]) > 255 || intval($parts[$i]) < 0)
                    return false;
            }
            return true;
        }
        else
            return false;
    }
	
}

?>
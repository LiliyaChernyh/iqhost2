{if isset($PAGINATOR)}
<div class="paginator">
     <span>{$PAGINATOR.itemsOnPage}</span> 
     <a href="{$PAGINATOR.firstLink}">1</a>  
     <a href="{$PAGINATOR.secondLink}">2</a> 
     <span>{$PAGINATOR.current}</span>/<span>{$PAGINATOR.total}</span>
     {if $PAGINATOR.nextLink} <a href="{$PAGINATOR.nextLink}">След.</a>{/if}
</div>
{/if}
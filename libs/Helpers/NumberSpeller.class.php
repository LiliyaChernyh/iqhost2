<?php
/**
* Класс для перевода чисел в пропись
* @version 1.0
*/
class NumberSpeller
{
	const GENDER_FEMALE = 0;
	const GENDER_MALE = 1;
	
	// сумма прописью string str_digit_str(integer)
	public static function SpellNumber($summ)
	{
		$tmp_num = (integer)$summ;  // текущий
		$str = "";
		$i = 1;            // счетчик триад
		$th = array("");

		// откусываем по три цифры с конца и обрабатываем функцией
		while(strlen($tmp_num) > 0)
		{
			$tri = "";
			if (strlen($tmp_num) > 3)
			{
				$dig = substr($tmp_num,strlen($tmp_num)-3);
				$tmp_num = substr($tmp_num,0, -3);
			}
			else
			{
				$dig = $tmp_num;
				$tmp_num = "";
			}
		   if ($i == 1) $th = " ";
		   else if ($i == 2) $th = " тысяч ";
		   else if ($i == 3) $th = " миллионов ";
		   else if ($i == 4) $th = " миллиардов ";
		   else if ($i == 5) $th = " биллионов ";
		   else if ($i == 6) $th = " триллионов ";
		   
			// вызываем функцию "сумма прописью" для нашей триады и присоединяем название разряда
			$str = $dig == 0 ? '' : self::SpellNumberTillThousand($dig).$th.$str;
			$i++;
		}
		
		// а теперь заменим неправильные падежи - их не так много, поэтому обойдемся массивом замен
		$tr_arr = array(
			"один тысяч" => "одна тысяча",
			"два тысяч" => "две тысячи",
			"три тысяч" => "три тысячи",
			"четыре тысяч" => "четыре тысячи",
			"один миллионов" => "один миллион",
			"два миллионов" => "два миллиона",
			"три миллионов" => "три миллиона",
			"четыре миллионов" => "четыре миллиона",
			"один миллиардов" => "один миллиард",
			"два миллиардов" => "два миллиарда",
			"три миллиардов" => "три миллиарда",
			"четыре миллиардов" => "четыре миллиарда", );

		// заменяем падежи
		$str = strtr($str, $tr_arr);
		return($str);
	}

	// сумма прописью до 999
	public static function SpellNumberTillThousand($dig, $gender = self::GENDER_MALE)
	{
		$str = "";
		
		// определяем массивы единиц, десятков и сотен
		$ed = array("",($gender == self::GENDER_MALE ? 'один':'одна'),
			($gender == self::GENDER_MALE ? 'два':'две'),"три","четыре","пять","шесть","семь","восемь","девять","десять",
			"одиннадцать","двенадцать","тринадцать","четырнадцать","пятнадцать","шестнадцать",
			"семнадцать","восемнадцать","девятнадцать","двадцать");
		$des = array("","десять","двадцать","тридцать","сорок","пятьдесят","шестьдесят","семьдесят","восемьдесят","девяносто");
		$sot = array("","сто","двести","триста","четыреста","пятьсот","шестьсот","семьсот","восемьсот","девятьсот");

		$dig = (int)$dig;
		if ($dig > 0 && $dig <= 20)
		{
			$str = $ed[$dig];
		}
		else if ($dig > 20 && $dig < 100)
		{
			$tmp1 = substr($dig,0,1);
			$tmp2 = substr($dig,1,1);
			$str = $des[$tmp1]." ".$ed[$tmp2];
		}
		else if ($dig > 99 && $dig < 1000)
	   {
			$tmp1 = substr($dig,0,1);
			if (substr($dig,1,2) > 20)
			{
				$tmp2 = substr($dig,1,1);
				$tmp3 = substr($dig,2,1);
				$str = $sot[$tmp1]." ".$des[$tmp2]." ".$ed[$tmp3];
			}
			else $str = $sot[$tmp1]." ".self::SpellNumberTillThousand(substr($dig,1,2));
		}
	  return $str;
	}

}
?>
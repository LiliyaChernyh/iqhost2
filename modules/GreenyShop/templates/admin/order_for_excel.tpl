<?xml version="1.0"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <LastAuthor>George</LastAuthor>
  <Created>2008-04-07T12:24:16Z</Created>
  <Version>11.5606</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12705</WindowHeight>
  <WindowWidth>13260</WindowWidth>
  <WindowTopX>480</WindowTopX>
  <WindowTopY>15</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Arial Cyr" x:CharSet="204"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s21">
   <Font ss:FontName="Arial Cyr" x:CharSet="204" ss:Size="22"/>
  </Style>
  <Style ss:ID="s22">
   <Font ss:FontName="Arial Cyr" x:CharSet="204" ss:Size="12"/>
  </Style>
  <Style ss:ID="s23">
   <NumberFormat/>
  </Style>
  <Style ss:ID="s24">
   <NumberFormat ss:Format="General Date"/>
  </Style>
  <Style ss:ID="s25">
   <Font ss:FontName="Arial Cyr" x:CharSet="204" ss:Size="12"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s26">
   <Font ss:FontName="Arial Cyr" x:CharSet="204" ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s27">
   <NumberFormat ss:Format="#,##0&quot;р.&quot;;[Red]\-#,##0&quot;р.&quot;"/>
  </Style>
  <Style ss:ID="s28">
   <NumberFormat ss:Format="0%"/>
  </Style>
  <Style ss:ID="s29">
   <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="order_52">
  <Table ss:ExpandedColumnCount="6" ss:ExpandedRowCount="59" x:FullColumns="1"
   x:FullRows="1">
   <Column ss:AutoFitWidth="0" ss:Width="160.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="79.5"/>
   <Row ss:AutoFitHeight="0" ss:Height="27">
    <Cell ss:StyleID="s21"><Data ss:Type="String">Заказ №{#var: ORDERID}</Data></Cell>
   </Row>
   <Row ss:Index="3" ss:AutoFitHeight="0" ss:Height="15">
    <Cell ss:StyleID="s22"><Data ss:Type="String">Информация о госте:</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">ФИО:</Data></Cell>
    <Cell><Data ss:Type="String">{#var: CUSTOMER_FIO}</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Почта:</Data></Cell>
    <Cell><Data ss:Type="String">{#var: CUSTOMER_EMAIL}</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Телефон:</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">{#var: CUSTOMER_PHONE}</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Адрес:</Data></Cell>
	<Cell><Data ss:Type="String">{#var: CUSTOMER_ADDRESS}</Data></Cell>
   </Row>
   <Row ss:Index="9" ss:AutoFitHeight="0" ss:Height="15">
    <Cell ss:StyleID="s22"><Data ss:Type="String">Информация о заказе:</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Дата проведения банкета:</Data></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">{#var: BANQUETDATE}</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Число гостей:</Data></Cell>
    <Cell><Data ss:Type="Number">{#var: GUESTSNUMBER}</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Время создания:</Data></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">{#var: CREATIONDATE}</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Номера столов:</Data></Cell>
    <Cell><Data ss:Type="String">{#var: TABLESNUMBERS}</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Статус заказа:</Data></Cell>
    <Cell><Data ss:Type="String">{#var: STATUSDESCRIPTION}</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Задаток:</Data></Cell>
    <Cell><Data ss:Type="Number">{#var: PREPAYMENT}</Data></Cell>
   </Row>
   <Row ss:Index="17" ss:AutoFitHeight="0" ss:Height="15">
    <Cell ss:StyleID="s22"><Data ss:Type="String">Корзина заказа:</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15">
    <Cell ss:StyleID="s25"><Data ss:Type="String">Наименование</Data></Cell>
    <Cell ss:StyleID="s25"><Data ss:Type="String">Артикул</Data></Cell>
    {#ifset: DISHES_DESCRIPTION}<Cell ss:StyleID="s25"><Data ss:Type="String">Описание</Data></Cell>{##ifset}
	<Cell ss:StyleID="s25"><Data ss:Type="String">Цена</Data></Cell>
    <Cell ss:StyleID="s25"><Data ss:Type="String">Количество</Data></Cell>
    <Cell ss:StyleID="s25"><Data ss:Type="String">Сумма</Data></Cell>
   </Row>
   {#block: dishes}
   {#ifset: DISHES_CATEGORY}<Row>
    <Cell ss:StyleID="s26"><Data ss:Type="String">{#var: DISHES_CATEGORY}</Data></Cell>
   </Row>{##ifset}
   <Row>
    <Cell><Data ss:Type="String">{#var: DISHES_NAME}</Data></Cell>
    <Cell><Data ss:Type="String">{#var: DISHES_ARTICULE}</Data></Cell>
    {#ifset: DISHES_DESCRIPTION}<Cell><Data ss:Type="String">{#var: DISHES_DESCRIPTION}</Data></Cell>{##ifset}
    <Cell ss:StyleID="s27"><Data ss:Type="Number">{#var: DISHES_PRICE}</Data></Cell>
    <Cell><Data ss:Type="Number">{#var: DISHES_AMOUNT}</Data></Cell>
    <Cell ss:StyleID="s27"><Data ss:Type="Number">{#var: DISHES_SUMM}</Data></Cell>
   </Row>{##block}
   <Row>
    <Cell ss:StyleID="s22"><Data ss:Type="String"> </Data></Cell>
   </Row>
   <Row>
    <Cell ss:StyleID="s22"><Data ss:Type="String">Дополнительные услуги:</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Наименование услуги</Data></Cell>
    <Cell><Data ss:Type="String">Цена / %</Data></Cell>
   </Row>
   {#block: additional_services}<Row>
    <Cell><Data ss:Type="String">{#var: ADDITIONAL_SERVICE_SERVICETITLE}</Data></Cell>
    <Cell><Data ss:Type="String">{#var: ADDITIONAL_SERVICE_COST}</Data></Cell>
   </Row>{##block}
 
   <Row>
    <Cell><Data ss:Type="String">Кухня:</Data></Cell>
    <Cell ss:StyleID="s27"><Data ss:Type="Number">{#var: KITCHEN_TOTAL}</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Бар:</Data></Cell>
    <Cell ss:StyleID="s27"><Data ss:Type="Number">{#var: BAR_TOTAL}</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">Кухня и бар:</Data></Cell>
    <Cell ss:StyleID="s27"><Data ss:Type="Number">{#var: DISHES_TOTAL}</Data></Cell>
   </Row>
   <Row>
    <Cell ss:StyleID="s29"><Data ss:Type="String">Стоимость всего заказа:</Data></Cell>
    <Cell ss:StyleID="s29"><Data ss:Type="Number">{#var: TOTAL_ORDER_COST}</Data></Cell>
   </Row>
   {#ifset: TOTAL_ORDER_COST_DISCOUNT}<Row>
    <Cell ss:StyleID="s29"><Data ss:Type="String">Стоимость всего заказа с учётом скидки на кухню и бар:</Data></Cell>
    <Cell ss:StyleID="s29"><Data ss:Type="Number">{#var: TOTAL_ORDER_COST_DISCOUNT}</Data></Cell>
   </Row>{##ifset}
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996"
     x:Right="0.78740157499999996" x:Top="0.984251969"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>6</ActiveRow>
     <ActiveCol>1</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
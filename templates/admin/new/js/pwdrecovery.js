$(document).ready(function()
{
    $("#sendTokenForm").validate(
    {
        rules : {
            email : {
                required : true,
                email : true
            }
        }
    });

    $("#newPasswordForm").validate(
    {
        rules : {
            password: {
				required: true,
				minLength: 6
			},
			passwordConf: {
				required: true,
				minlength: 6,
				equalTo: "#password"
			}
        },
		submitHandler : function(form) 
		{
			$(form).ajaxSubmit({
				success:  passwordChangeSubmitHandler
			});
		}
    });
});

function passwordChangeSubmitHandler(data, status)
{
	if (!standartAJAXResponseHandler(data, status))
		return;
	$("#newPasswordForm").slideToggle();
	$("#newPasswordForm").parent().append("������ ������� �������");
}
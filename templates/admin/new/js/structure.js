/**
* @version 2.1
*/
$(document).ready(function()
{
	reloadTree();
	$("#pageForm").validate(
			{
			rules: 
				{},
			submitHandler: function(form) {
					$(form).ajaxSubmit({success:saveFormResponse, beforeSubmit:showRequest, dataType:"json"});
					return false;
				}
			});
	$("#page_pageTypeID").change(changeformBlocks);
	$(":input[maxlength]").focus(ChooseLen).change(ChooseLen).keyup(ChooseLen).blur(HideLen);
});

function reloadTree()
{
    var struct = $("#structure");
    var structNew = $("<ul>").insertAfter(struct);
    struct.remove();
    structNew.attr("id", "structure");
    structNew.removeClass("treeview").treeview({
			url: CONTROLLER_ALIAS + "getstructure",
			persist: "location",
			animated: "fast"
		});
}

function saveFormResponse(data, status)
{
	$("#pageForm input:submit").attr("disabled",false);
	if (!standartAJAXResponseHandler(data, status)) return;
	if (data.msg == "ok") 
	{
        reloadTree();
		if (editMode)
		{
			$.jGrowl("Страница сохранена");
			if ((typeof REFRESH_AFTER_FLUSH != "undefined" && REFRESH_AFTER_FLUSH == 1) ||
				data.refresh)
			{
				$("#pageForm, #rightblock").slideToggle();
				var res = $("#submitResult2");
				res.children("a").eq(0).attr("href", window.location);
				$("#submitResult2").show();
				setTimeout("window.location.reload()", 4000);
			}
		}
		else
		{
			$("#pageForm").slideToggle();
			var res = $("#submitResult1");
			res.children("a").eq(0).attr("href", CONTROLLER_ALIAS + "edit/" + data.pageID + "/");
			$("#submitResult1").show();
		}
	};
}

function showRequest(form)
{
    $("#pageForm input:submit").attr("disabled",true);
	form.push({name:"async",value:"1"});
	form.push({name:$("#pageForm input:submit").attr("name"),value:"1"});
}

function changeformBlocks(event)
{
	var that = $(this);
	var pageTypeID = that.val();
	$("#contentFormContainer").hide().children(":gt(0)").remove();

	if (pageTypeID != 0)
	{
		that.attr("disabled", true);
		$("#loadBar").css("display", "inline");
	}
	else
		return;
	$.post(CONTROLLER_ALIAS + "getEditContentForm/", {pageTypeID: pageTypeID}, formBlocksLoadedHandler, "json");
}

function formBlocksLoadedHandler(data, status)
{
	$("#loadBar").hide();
	$("#page_pageTypeID").attr("disabled", false);

	if (!standartAJAXResponseHandler(data, status)) return;
		
	if (data.contentType == "module") return;
	var container = $("#contentFormContainer");
	var controlsToInit = new Array();
	
	for (var t = 0; t < data["formBlocks"].length; t++)
	{
		var blockHeader = $(document.createElement('div')).addClass("formBlockHeader").text(data["formBlocks"][t]["blockName"]);
		var block = $(document.createElement('div')).addClass("formBlock");

		for (var i = 0; i < data["formBlocks"][t]["controls"].length; i++)
		{		
			var formElement = $(document.createElement('div')).addClass("formElement");
			if (data["formBlocks"][t]["controls"][i]["className"])
				formElement.addClass(data["formBlocks"][t]["controls"][i]["className"]);

			var label = null;
			if (data["formBlocks"][t]['controls'][i]['description'])
				label = $(document.createElement('label')).html(data["formBlocks"][t]['controls'][i]['description']);

			switch (data["formBlocks"][t]['controls'][i]['type'])
			{
				case "HiddenInput":
					block.append((data['formBlocks'][t]['controls'][i]['controlHTML']));
					break;
				default:
					if (label != null)
					{
						label.attr("for", data["formBlocks"][t]['controls'][i]['name']);
						formElement.append(label).append(document.createElement('br'));
					}
					formElement.append($(data["formBlocks"][t]['controls'][i]['controlHTML']));
					block.append(formElement);

					// Если есть инициализирующий объект или подгружаемые JS файлы то пытаемся после их загрузки их запустить
					if (typeof data["formBlocks"][t]['controls'][i]['controlJSVars'] != "undefined" || 
						typeof data["formBlocks"][t]['controls'][i]['controlJSLibs'] != "undefined")
					{
						controlsToInit.push(data["formBlocks"][t]['controls'][i]);
					}
					break;
			}
		}
		container.append(blockHeader);
		container.append(block);
	}
	container.show();

	// Сохраним необходимые файлы в форме для загрузки после загрузки библиотек
	if (typeof data["includingJSFiles"] != "undefined")
		$(document).data("includingJSFiles", data["includingJSFiles"]);

	// Сохраним контролы, которые необходимо инициализировать
	$(document).data("controlsToInit", controlsToInit);

	// Загрузим необходимые JS-библиотеки
	// К сожалению, слишком много библиотек идёт вместе с css файлами, поэтому проще 
	// грузить заранее
	//if (typeof data["includingJSLibs"] != "undefined")
	//{
	//	LoadLibs(data["includingJSLibs"]);
	//}
	LoadNextJSFile();
}

function LoadLibs(libsList){}

function LoadNextJSFile()
{
	var filesToLoad = $(document).data("includingJSFiles");
	if (filesToLoad.length > 0)
	{
		var next = filesToLoad.shift();
		$.getScript(next, LoadNextJSFile);
	}
	else
	{
		InitControls();
	}
}

function InitControls()
{
	var controlsToInit = $(document).data("controlsToInit");
	for (var i = 0; i < controlsToInit.length; i++)
	{
		var InitObject = new Object();
		if (typeof controlsToInit[i]["controlJSVars"] != "undefined")
			InitObject = controlsToInit[i]["controlJSVars"];

		if (eval("typeof " + controlsToInit[i]["type"] + "Init == 'function'"))
		{
			eval(controlsToInit[i]["type"] + "Init(InitObject)");
		}
	}
}
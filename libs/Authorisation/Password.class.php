<?php
/**
 * Класс для создания смены и восстановления паролей
 *
 * @version 1.2
 * @author George
 */
 
//require_once 'PasswordHash.php'; 
 
class Password
{
    public static $SALT = '';
    
    /**
     * Возвращает захешированный пароль
     *
     * @param string $password
     * @return string
     */
    public static function HashPassword($password)
    {
        $t_hasher = new PasswordHash();
        $hash = $t_hasher->HashPassword($password.self::$SALT);	
        return $hash; /*md5($password.self::$SALT);*/
    }

    /**
     * Генерация случайных паролей
     * Нужно указать длину и силу [0-4]
     *
     * @param int $length
     * @param int $strength
     * @return string
     */
    public static function GeneratePassword($length=9, $strength=0)
	{
	    $vowels = 'aeuy';
	    $consonants = 'bdghjmnpqrstvz';
	    if ($strength >= 1)
	        $consonants .= 'BDGHJLMNPQRSTVWXZ';
	    if ($strength >= 2)
	        $vowels .= "AEUY";
	    if ($strength >= 3)
	        $consonants .= '23456789';
	    if ($strength >= 4)
	        $consonants .= '@#$%';

	    $password = '';
	    $alt = time() % 2 == 1;
	    $consonantsLen = strlen($consonants);
	    $vowelsLen = strlen($vowels);
	    for ($i = 0; $i < $length; $i++) 
	    {
	        $password .= $alt ? $consonants{(rand() % $consonantsLen)} : $vowels{(rand() % $vowelsLen)};
	        $alt = !$alt;
	    }
	    return $password;
	}
	
	/**
	 * Генерация случайных идентификаторов.
	 *
	 * @param int $length
	 * @param bool $unique
	 */
	public static function GenerateIdentity($length = 9, $unique = true)
	{
	    static $items = array(	'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 
        						'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
        						'z', 'x', 'c', 'v', 'b', 'n', 'm',
        						'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 
        						'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L',
        						'Z', 'X', 'C', 'V', 'B', 'N', 'M',
        						'1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        
        if ($unique)
            $identityTableManager = new DBTableManager(TablesNames::$USERS_IDENTITY_TABLE_NAME);
            
		$max = count($items) - 1;
		do 
		{
    		$identity = '';
    		for ($i = 0; $i < $length; $i++)
    			$identity .= $items[rand(0, $max)];
    		
    		$success = true;
    		if ($unique)
    		{
    		    $result = $identityTableManager -> Select(array('identity' => $identity));
    		    if (count($result) > 0)
                    $success = false;
    		}
		} while (!$success);
		
		return $identity;
	}
	
	/**
	 * Создаёт токен восстановления пароля и заносит его в базу данных
	 * В БД должна быть таблица greeny_pwdRecoveryTokens
	 *
	 * @param int $userID
	 * @return string
	 */
	public static function CreateNewPaswordToken($userID)
	{
	    $dbTable = new DBTableManager('greeny_pwdRecoveryTokens');
	    $dbTable -> Delete(array('userID' => $userID));
	    $time = time();
	    $token =  md5($userID.rand().$time);
	    $dbTable -> Insert(array('userID' => $userID, 'token' => $token));
	    return $token;
	}
	
	/**
	 * Проверяет токен и удаляет если старый
	 *
	 * @param string $token
	 * @return int
	 */
	public static function CheckToken($token)
	{
	    $dbTable = new DBTableManager('greeny_pwdRecoveryTokens');
	    $entry = $dbTable -> SelectFirst(array('token' => $token));
	    if (empty($entry) || time() - strtotime($entry['creationDate']) > 86400) return false;
	    return $entry['userID'];
	}
	
	/**
	 * Изменяет пароль
	 *
	 * @param string $token
	 * @param string $newPwd
	 * @return bool
	 */
	public static function ChangePassword($token, $newPwd)
	{
	    $userID = self::CheckToken($token);
	    if (!$userID) return false;
	    $usr = new UsersManager();
	    $usr -> ChangePassword($newPwd, $newPwd, null, $userID, false);
	    $dbTable = new DBTableManager('greeny_pwdRecoveryTokens');
	    $dbTable -> Delete(array('token' => $token));
	    return true;
	}
}
?>

        <ul>
            <li>
                <a href="{$MODULE_ALIAS}sections/" title="Редактор категорий">Категории</a>
            </li>
            {if isset($SECTIONS)}
            <ul class="sections">
                {foreach from=$SECTIONS item=SECTION}
                <li>
                    <a href="{$MODULE_ALIAS}list/{$SECTION.alias}" title="Показать новости из категории «{$SECTION.caption}»">
                        <span style="{if (isset($CUR_SECTION) && ($CUR_SECTION == $SECTION.sectionID))}font-weight: bold;{/if}">
                            {$SECTION.caption}
                        </span>
                    </a>
                </li>
                {/foreach}
            </ul>
            {/if}
            <li>
                <a href="{$MODULE_ALIAS}" title="Показать новости из всех категорий">Все новости</a>
            </li>
            <li>
                <a href="{$MODULE_ALIAS}recycled/" title="Список удаленных новостей">Корзина ({$DELETED_NEWS_NUMBER})</a>
            </li>
        </ul>
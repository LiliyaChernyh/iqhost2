<?php
/**
 * Интерфейс диспетчера событий
 * 
 * @version 1.1
 *
 */
interface IEventDispatcher
{
    /**
     * Регистрация обработчика события
     *
     * @param string $eventType
     * @param mixed $listener
     * @param string $handlerName
     */
    public function RegisterListener($eventType, $listener, $handlerName);
    
    /**
     * Удаление обработчика события
     *
     * @param string $eventType
     * @param mixed $listener
     * @param string $handlerName
     */
    public function RemoveListener($eventType, $listener, $handlerName = null);
    
    /**
     * Вызов обработчиков события
     *
     * @param string $eventType
     * @param array $args
     */
    public function Invoke($eventType, &$args = null);
}
?>
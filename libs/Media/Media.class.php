<?php

/**
 * Преобразование URL в html медиа-объекты
 * Набор статичных функций для получения медиа-объектов в формате HTML из ссылок
 * с разных источников.
 * Поддерживаемые источники:
 *  - youtube
 *  - soundcloud
 *  - mixcloud
 * @author ghost
 * @todo VK-player, ru-tube, vimeo
 */
class Media
{
    /**
	 * Преобразование ссылки youtube в встроенный плеер в iframe
	 * @param string $url
	 * @param string $width ширина окна
	 * @param string $height высота окна
	 * @param bool $fullscreen возможность раскрыть на полный экран
	 * @return string HTML код iframe, содержащий плеер.
	 */
	public static function YoutubeToIframe($url, $width = "100%", $height = 150, $fullscreen = true)
	{
		parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
		$youtube = '<iframe allowtransparency="true" scrolling="no" width="' . $width . '" height="' . $height . '" src="//www.youtube.com/embed/' . $my_array_of_vars['v'] . '" frameborder="0"' . ($fullscreen ? ' allowfullscreen' : NULL) . '></iframe>';
		return $youtube;
	}

	/**
	 * Преобразование ссылки youtube в массив ссылка/картинка для
	 * использования в FancyBox
	 * @param string $url ссылка youtube
	 * @return array массив с параметрами link и image
	 *
	 * Параметры массива:
	 * link - ссылка на видео;
	 * image - ссылка на изображение (превью видео);
	 */
	public static function YoutubeToFancy($url,$image = null)
	{
		$url1				 = parse_url($url);
		if ($url1['host'] != 'www.youtube.com')
			return false;
		parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
		$youtube['link']	 = 'http://www.youtube.com/embed/' . $my_array_of_vars['v'] . '?autoplay=1';
		$youtube['image']	 = !empty($image) ? $image : 'http://img.youtube.com/vi/' . $my_array_of_vars['v'] . '/mqdefault.jpg';
		return $youtube;
	}

	/**
	 * Преобразование ссылки SoundCloud в встроенный плеер в iframe
	 * @param type $url ссылка на ресурс SoundCloud
	 * @param type $width ширина блока
	 * @param type $height высота блока
	 * @return string iframe
	 * HTML код iframe, содержащий плеер.
	 */
	public static function SoundCloud($url, $width = "100%", $height = 300)
	{
		$soundcloud = '<iframe width="' . $width . '" height="' . $height . '" frameborder="no" scrolling="no" src="//w.soundcloud.com/player/?url=' . $url . '&auto_play=false&buying=true&liking=true&download=true&sharing=true&show_artwork=true&show_comments=true&show_playcount=true&show_user=true&hide_related=false&visual=false&start_track=0&callback=true"></iframe>';
		return $soundcloud;
	}

	/**
	 * Преобразование ссылки MixCloud в встроенный плеер в iframe
	 * @param string $url ссылка на ресурс MixCloud
	 * @return string iframe
	 * HTML код iframe, содержащий плеер.
	 */
	public static function MixCloud($url)
	{
		$soundcloud = file_get_contents('http://api.mixcloud.com/' . trim($url, "/") . "/embed-html/");
		return $soundcloud;
	}
}

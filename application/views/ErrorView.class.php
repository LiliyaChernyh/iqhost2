<?php
/**
 * Ошибки
 * 
 * @version 1.0
 *
 */
class ErrorView extends FrontView
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function PageNotFound()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath .'error/pageNotFound.tpl';
    }
}
?>
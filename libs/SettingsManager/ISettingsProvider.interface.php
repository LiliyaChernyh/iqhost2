<?php

/**
 * Интерфейс для провайдеров настроек
 *
 * @version 1.0
 */
interface ISettingsProvider
{
    /**
     * Получение информации о настройке
     *
     * @param string $settingPath
     * @return array
     */
    public function GetSetting($settingPath);
    
    /**
     * Получение настроек группы
     *
     * @param string $groupPath
     * @return array
     */
    public function GetSettings($groupPath);
    
    /**
     * Получение информации о группе
     *
     * @param string $groupPath
     * @return array
     */
    public function GetGroup($groupPath);
    
    /**
     * Получение дерева групп начиная с заданной группы.
     *
     * @param string $rootGroupPath
     * @param bool $withSettings
     * @return array
     */
    public function GetGroupsTree($rootGroupPath = null, $withSettings = false);
    
    /**
     * Полчение настроек в виде ассоциативного массива для удобного заполнения шаблона
     *
     * @param array|string $groupPath
     */
    public function GetSettingsAssoc($groupPath = null);
}

?>
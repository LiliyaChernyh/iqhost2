<h1>Управление меню</h1>

<div style="text-align: right; margin: -30px 0 5px 0;">
    <input type="button" id="goAddTheme" class="goToBtn" value="добавить меню" />
    <input type="hidden" id="goAddTheme_link" value="{$CONTROLLER_ALIAS}add/" />
</div>

{include file='paginator.tpl'}
<table class="highlightable" id="faqTable">
    <thead>
        <tr>
            <th>№</th>
            <th>Название</th>
            <th>Комментарий</th>
            <th style="width: 10px;"><img src="{$ADMIN_IMAGES}icons/pencil.png" /></th>
            <th style="width: 10px;"><img src="{$ADMIN_IMAGES}icons/delete_icon.png" /></th>
        </tr>
    </thead>
    <tbody>
{if !empty($LIST_MENU)}
    {foreach from=$LIST_MENU item=MENU key=I}
        <tr>
            <td style="display: none;"><a href="{$CONTROLLER_ALIAS}showinner/{$MENU.menuListID}/" /></td>
            <td style="width: 20px;">{$I+1}</td>
            <td>
                {$MENU.menuListName}
            </td>
            <td>
                {$MENU.menuListComment}
            </td>
            <td>
                <a href="{$CONTROLLER_ALIAS}edit/{$MENU.menuListID}/" title="Редактировать"><img src="{$ADMIN_IMAGES}edit.png" alt="Редактировать" style="border: 0;" /></a>
            </td>
            <td class="deleteEntry">
                <a href="{$CONTROLLER_ALIAS}del/{$MENU.menuListID}/" title="Удалить"><img src="{$ADMIN_IMAGES}delete_icon.png" alt="Удалить" style="border: 0;" /></a>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>
{else}
    </tbody>
</table>
    <div class="error">Записей не найдено</div>
{/if}

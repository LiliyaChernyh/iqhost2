<?php
/**
 * Представление для формы авторизации
 * 
 * @version 1.0
 *
 */
class RegistrationView extends AdminView 
{

    public function __construct()
    {
        parent::__construct('registration/register.tpl');
    }
}
?>
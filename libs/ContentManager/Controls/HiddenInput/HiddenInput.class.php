<?php
/**
 * Контрол для хранения идентификатора записи.
 * Обычно primaryKey, который нельзя редактировать,
 * но он должен присутствовать на форме.
 *
 * Возвращает уже отрендеренный HTML
 * в свойстве controlHTML
 *
 * @version 3.2
 *
 */
class HiddenInput extends Control
{
	public function InitControl($value = null, $rowID = null)
	{
		$tagPattern = '<input type="hidden" name="{name}" value="{value}" id="{id}" />';
		if ($value === null) $value = '';
		else
		{
			// убираем спецсимвлы
			$value = htmlspecialchars($value);
		}
		$formIndex = (isset($this->formIndex))?$this->formIndex:0;
		$fieldID = $this->name."_".$formIndex;

		$fieldName = $this->tableName."[".$formIndex."]"."[".$this -> name."]";
		$tagPattern = str_replace('{name}', $fieldName, $tagPattern);
		$tagPattern = str_replace('{value}', $value, $tagPattern);
		$tagPattern = str_replace('{id}', $fieldID, $tagPattern);
		$this -> controlHTML = $tagPattern;
		$this -> type = __CLASS__;
	}

	public function PostBackHandler(&$data, $rowID = null){}
}

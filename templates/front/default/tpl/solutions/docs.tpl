<div class="top-section">
    <div class="container">
        <div class="twocolumns">
            <div class="text-holder">
                <h1>Документы</h1>
                {if !empty($PAGES)}
                    <ul class="solution-menu">
                        {foreach from=$PAGES item=pageItem}
                            <li{if $PAGE.alias == $pageItem.alias} class="active"{/if}><a href="/solutions/{$pageItem.alias}/">{$pageItem.caption}</a></li>
                            {/foreach}
                    </ul>
                {/if}
                {if !empty($MENU_SOLUTION)}
                    <ul class="solution-menu">
                        {foreach from=$MENU_SOLUTION item=m} 
                            <li>
                                <a class="check" href="{$m.link}"><span>{$m.title}</span></a>
                            </li>
                        {/foreach}
                    </ul>
                {/if}
            </div>
            {if !empty($BANNER)}
                <div class="banner-center clearfix">
                    <div class="twocolumns">
                        <img src="/getimage/{$BANNER.imageID}/">
                    </div>
                </div>
                {if !empty($BANNER.text)}
                    <div class="advantages clearfix">
                        {$BANNER.text}
                    </div>
                {/if}
            {/if}
            <div class="section-right">
                <div class="twocolumns">
                    {if !empty($CONTENT.summary)}
                        {$CONTENT.summary}
                    {/if}

                    {if !empty($ADVANTAGES_SOLUTION)}
                        {foreach from=$ADVANTAGES_SOLUTION item=advantage}
                            <div class="section-right__wrapp">
                                {if !empty($advantage.imageID)}
                                    <div class="section-right__wrapp-img">
                                        <img src="/getimage/{$advantage.imageID}/" alt="{$advantage.title}">
                                    </div>
                                {/if}
                                <p>{$advantage.text}</p>
                            </div>
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>
{if !empty($CONTENT.text)}
    <div class="content-block">
        <div class="container">
            <h2>{$PAGE.caption}</h2>
            {$CONTENT.text}
        </div>
    </div>
{/if}

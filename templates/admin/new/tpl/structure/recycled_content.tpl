			<h1>Корзина</h1>
			
			{if !empty($DELETED_PAGES)}
			<div>
				Удаленные страницы:
				
				<form action="" method="post" id="recycled_form">
					<ol id="recycledPagesList" style="margin: 20px 0">
						{foreach from=$DELETED_PAGES item=deletedPage key=I}
						<li>
							<input type="checkbox" name="toEdit[]" value="{$deletedPage.pageID}" id="recycled_{$I}" />
							<label for="recycled_{$I}">{$deletedPage.caption}</label>
						</li>
						{/foreach}
					</ol>

					<div><a id="checkUncheckAll" style="display:none;cursor: pointer;" href="#">Отметить все</a></div>

					<div style="margin: 20px 0 20px 0px;float:left">
						<input type="submit" class="handyButton" name="doRestore" value=" Восстановить " style="width: 200px; height:30px; margin-right:100px; font-size: 1.3em" />
						<input type="submit" class="handyButton" name="doWipe" value=" Удалить навсегда " style="width: 200px; height:30px; font-size: 1.3em"/>
					</div>
				</form>
			</div>
			{else}
			<div style="font-size: 1.3em ">Корзина пуста</div>
			{/if}
$(document).ready(function(){
        if (!$.browser.opera)
                $("#container").css("margin-top", ($(window).height() / 2 -100) + "px");
        else
                $("#container").css("margin-top", (document.documentElement["clientHeight"] / 2 - 100) + "px");

        $("#loginForm").ajaxForm(
                {
                        beforeSubmit:showRequest,
                        success:AJAXResponseHandler, 
                        dataType:"json"
                }
        );

        $("#restorePwd").click(restorePwd);
});	

function restorePwd(event)
{
        event.preventDefault();
        $(this).hide();
        $('#register').hide();
        $("#userpass").parent().slideToggle("fast", killPwd);
        $("#remember_pass_container").slideToggle();
        $("label[for=username]").text("E-mail");
        $("<div id='insert_email'>Введите e-mail, который указывали при регистрации.</div>").
                addClass("text").
                hide().
                insertBefore($("#loginForm").attr("action", "/admin/pwdrecovery/sendtoken/")).
                slideToggle();
        $("#username").attr("name","email").attr("id","email");
        $("#submit").val("Отправить ключ").attr("name","doSend");
}

function killPwd()
{
        //$("#userpass").remove();
		$("#userpass").val('123456');
}

function showRequest(form, jObject, options)
{
        $("#submit").attr("disabled", true);
}

function AJAXResponseHandler(data, status)
{
        $("#submit").attr("disabled", false);
        var msg = "";
        if (data.error)
        {
            switch (data.errno)
            {
                case 100:
                        msg = "Возникла ошибка при отправлении письма.";
                        break;
                case 101:
                        msg = "Введите корректный e-mail";
                        break;
                case 102:
                        msg = "Пользователь с таким адресом не найден";
                        break;
                case 1:
                        msg = "Указанный адрес не найден";
                        break;
                case 5:
                        msg = "Некорректный логин или email";
                        break;
                case 6:
                        msg = "Неправельно введен пароль";
                        break;
                case 7:
                        msg = "Логин деактивирован";
                        break;
                case 8:
                        msg = "У вас недостаточно прав";
                        break;
                default :
                        msg = "Возникла ошибка";
                        break;
            }
            
            if (data.errno == 5) $('#username').parent().addClass('error').removeClass('success');
            if (data.errno == 6) $('#userpass').parent().addClass('error').removeClass('success');
            $('#message').hide().html(msg).addClass('error').removeClass('success').fadeIn();
        }
        else if (data.msg == "email")
        {   
            $('#insert_email').slideUp();;
            $('#message').hide().html(data.text).addClass('success').removeClass('error').fadeIn();
            $("#loginForm").slideToggle();
        }
        else if (data.msg == "ok")
        {
            window.location.reload();
            exit;
        }
}
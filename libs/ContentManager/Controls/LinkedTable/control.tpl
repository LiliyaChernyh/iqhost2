<script type="text/JavaScript">
$(document).ready(function()
	{literal}
	{
	{/literal}
	var controlName = "{$localVars.NAME}";
	{literal}
		
		$(".addLinkedTableRowControl").click(DuplicateLinkedTableRowControl);
		$(".killLinkedTableRowControl").click(KillLinkedTableRowControl);
		if (typeof newControlInstanceInitialiser[controlName] != "undefined")
			newControlInstanceInitialiser[controlName]($("#"+controlName+"_linkedTable_container"));
		if (typeof newControlInstancePostInitialiser[controlName] != "undefined")
			newControlInstancePostInitialiser[controlName]($("#"+controlName+"_linkedTable_container"));
	});
	
	// В подшаблоне должна быть определена такая функция
	// newControlInstanceInitialiser["{$localVars.NAME}"] = function(instance) {}
	
	function DuplicateLinkedTableRowControl(event)
	{
		event.preventDefault();
		var container = $(this).parent().siblings("table"); // table
		var instance = container.children(/*tbody*/).children(/*trs*/).filter(":last").clone(false);

		// нужно дать уникальный id
		var num = container.children().length;
		while ($("#"+instance.attr("id")+num).length > 0) num++;
		instance.attr("id", instance.attr("id")+num).children().each(function(i) 
		{
			var that = $(this).children();
			if (that.attr("id").length > 0) that.attr("id", that.attr("id")+"_"+num);
			if (that.is("a.killLinkedTableRowControl")) that.click(KillLinkedTableRowControl);
			else if(that.is(":text, textarea")) that.val("");
		});
		
		// ID контейнера в начале должен содержать имя контрола
		var nameParts = instance.attr("id").split("_");
		if (typeof newControlInstanceInitialiser[nameParts[0]] != "undefined")
			newControlInstanceInitialiser[nameParts[0]](instance);
		container.append(instance);
		if (typeof newControlInstancePostInitialiser[nameParts[0]] != "undefined")
			newControlInstancePostInitialiser[nameParts[0]](instance);
	}
	
	function KillLinkedTableRowControl(event) 
	{
		event.preventDefault();
		var that = $(this);
		var parent = that.parent().parent();
		var idparts = parent.attr("id").split("_");
		if (parent.siblings("tr[id*="+idparts[0]+"_"+idparts[1]+"]").length > 0)
			parent.remove();
		else
			parent.children().each(function() {$(this).children(":input").val("");});
	}
	{/literal}
</script>

<div style="width:550px;float:left;">
{*
В этом шаблоне должна быть задана таблица с уже созданными связанными записями,
а также контейнер с шаблонным контролом для этой таблицы.
Должен быть один table

В шаблоне можно разместить кноgre с классом .killLinkedTableRowControl
Можно раместить специфичные JS обработчики. Во внешнем шаблоне (в этом) определены функции 
размножения и убийства, которые по идее должны справляться со своими задачами...

Всо какбэ просто...
*}
{include file=$localVars.TABLE_CONTROL_TPL localVars=$localVars}
<div><a href="#" class="addLinkedTableRowControl">[ + ]</a></div>
</div>
<div style="clear:left"></div>
<div style="width:550px;float:left;">
    <select id="dependentSingleSelect_{$localVars.NAME}" name="{$localVars.NAME}" {if $localVars.CLASS}{$localVars.CLASS}{/if}>
	{if !empty($localVars.OPTIONS)}
		{foreach from=$localVars.OPTIONS item=OPTION}
		<option value="{$OPTION.value}"{if $OPTION.selected} selected="selected"{/if}>{$OPTION.text}</option>
		{/foreach}
	{/if}
    </select>
</div>
<div style="clear:left"></div>
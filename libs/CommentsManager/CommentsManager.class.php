<?php
require_once($_SERVER['DOCUMENT_ROOT']."/libs/DBManager/includes.php");
require_once($_SERVER['DOCUMENT_ROOT']."/libs/exceptions/includes.php");

/**
 * Класс управления комментариями
 * В базе должна находиться таблица comments
 * CREATE TABLE `comments` (
 * `commentID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 * `parentID` INT NOT NULL DEFAULT '0',
 * `pageID` INT NOT NULL ,
 * `userID` INT NOT NULL ,
 * `subject` VARCHAR( 128 ) NULL ,
 * `comment` TEXT NOT NULL ,
 * `creationTime` DATETIME NOT NULL 
 * ) TYPE = MYISAM ;
 * 
 * @version 1.1
 */

class CommentsManager
{
	public function CommentsManager($CommentsDBTableName = null, $UsersDBTableName = null)
	{
		$this -> DBTable = ($CommentsDBTableName == null) ? new DBTableManager("comments") : new DBTableManager($CommentsDBTableName);
		$this -> UsersDBTable = ($UsersDBTableName == null) ? new DBTableManager("phpbb_users") : new DBTableManager($UsersDBTableName);
	}
	
	/**
	 * Менеджер таблицы комментариев
	 *
	 * @var DBTableManager
	 */
	private $DBTable;
	
	/**
	 * Имя таблицы пользователей
	 *
	 * @var DBTableManager
	 */
	private $UsersDBTable;
	
	/**
	 * дерево комментариев
	 *
	 * @var array
	 */
	private $CommentTree;
	
	/**
	 * Функция возвращает дерево комментариев
	 * Деврево представлено в виде ассоциативных массивов
	 * commentTree = [value][commentTree, commentTree, [...]]
	 *
	 * @param number $pageID
	 * @param number $parentID
	 */
	public function GetCommentsTree($pageID, $parentID = null)
	{
		$query = "SELECT * FROM ". $this -> DBTable -> GetTableName() . " cmts
					JOIN " . $this -> UsersDBTable -> GetTableName() . " usrs
					ON cmts.userID=usrs.`" . $this -> UsersDBTable -> TableStructure -> PrimaryKey -> Name . "` 
					WHERE cmts.`pageID` = $pageID ";
		$query .= ($parentID == null) ? "" : "AND cmts.`parentID` = $parentID";
		$query .= " ORDER BY commentID";
		//echo $query;
		$queryResult = DB::Query($query);

		$allComments = array();
		while ($row = mysql_fetch_assoc($queryResult))
		{
			$allComments[] = $row; 
		}

		$this -> CommentTree = array();
		for ($i = 0; $i < count($allComments); $i++)
		{
			$this -> AddCommentToTree(&$allComments[$i]);
		}
		return $this -> CommentTree;
	}
	
	/**
	 * Рекрсивная функция добавления комментария в дерево
	 *
	 * @param array $comment
	 * @param array $target
	 * @return boolean
	 */
	private function AddCommentToTree($comment, $target = null)
	{
		if ($comment['parentID'] == 0)
		{
			$this -> CommentTree[] = array('value' => &$comment, 'children' => array());
			return true;
		}
		//return;
		if ($target == null)
		{ 
			$target = &$this -> CommentTree;
		}
		
		$cnt = count($target);
		for ($i = 0; $i < $cnt; $i++)
		{
			if ($target[$i]['value']['commentID'] == $comment['parentID'])
			{
				$target[$i]['children'][] = array('value' => &$comment, 'children' => array());
				return true;
			}
		}
		// Мы не смогли добавить этот коммент на этом уровне.
		// опять пройдём по уровню и вызовем добавление ко всем детям.
		// Мы не сделали этого в предыдущем цикле, поскольку деревья комментов
		// ветвятся не сильно и вероятность отыскать родителя на более высоком 
		// уровне выше, чего в, принципе, может и не быть :)

		for ($i = 0; $i < $cnt; $i++)
		{
			//echo $target[$i]['value']['commentID'];
			if (count($target[$i]['children']) == 0) continue;
			if ($this -> AddCommentToTree(&$comment, &$target[$i]['children']))
				return true;
		}
		
		// Так и не нашли родителя этого комментария
		return false;
	}
	
	public function AddComment($comment)
	{
		// проверяем параметр
		if ((!is_array($comment)) || 
		(!isset($comment['parentID'])) || 
		(!isset($comment['pageID'])) || 
		(!isset($comment['userID'])) ||
		(!isset($comment['subject'])) ||
		(!isset($comment['comment'])) ||
		(!isset($comment['creationTime'])))
			throw new ArgumentException("Параметр должен быть массивом, содержащим ключи parentID, userID, subject, comment, creationTime");
		
		$this -> DBTable -> Insert($comment);
	}
	
	public function DeleteComment($commentID)
	{
		if ($this -> CommentTree == null)
		{
			$rows = $this -> DBTable -> Select(array("commentID" => $commentID));
			if (count($rows) < 1) return;
			$this -> GetCommentsTree($rows[0]['pageID']);
		}
		$this -> DBTable -> Delete(array('commentID' => $commentID));
		$this -> DeleteChildren($commentID);
	}
	
	private function DeleteChildren($commentID, $target = null)
	{
		if ($target == null)
			$target = &$this -> CommentTree;
		//echo $commentID;
			//print_r($target);
		$cnt = count($target);
		for ($i = 0; $i < $cnt; $i++)
		{
			if ($target[$i]['value']['parentID'] == $commentID)
			{
				//echo $i."<br />";
				$this -> DeleteComment($target[$i]['value']['commentID']);
			}
			else 
			{
				if (count($target[$i]['children']) > 0)
				$this -> DeleteChildren($commentID, $target[$i]['children']);
			}
		}
		
	}
	
}
?>
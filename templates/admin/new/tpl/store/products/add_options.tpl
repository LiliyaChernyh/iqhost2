{if !empty($OPTIONS)}
	{foreach from=$OPTIONS key=key item=field}
		<div class="formBlockHeader {$field.optionAlias}">{$field.optionName}</div>
		<div><a href="#addoption" data-link="/admin/store/options/addproductoption/{$field.optionID}/{$productID}/" class="addoption">Добавить</a></div>
		<div class="formBlock">
			<table class="highlightable" id="faqTable_{$field.optionID}">
				<thead>
					<tr>
						<th></th>
						{*						<th>№</th>*}
						<th>Идентификатор</th>
						<th>Изображение</th>
						<th>Значение</th>
						<th>Размер</th>
						<th>Количество</th>
						<th style="width: 10px;">вкл/выкл</th>
						<th style="width: 10px;">Редактировать</th>
						<th style="width: 10px;">Удалить</th>
					</tr>
				</thead>
				<tbody>
					{assign var=fieldIndex value=0}
					{foreach from=$field.values key=key2 item=val}
						{include file='store/options/value.tpl' val=$val }
						{assign var=fieldIndex value=$fieldIndex+1}
					{/foreach}

				</tbody>
			</table>

		</div>
	{/foreach}
{else}
	Дла выбранного типа товаров дополнительных полей не найдено
{/if}
{*
{if !empty($OPTIONS)}
	{assign var="fieldIndex" value=0}
	{foreach from=$OPTIONS key=key item=field}
	    <div class="formBlockHeader {$field.optionAlias}">{$field.optionName}</div>
	    <div class="formBlock">
		<table class="highlightable" id="faqTable">
		    <thead>
			<tr>
			    <th colspan="2">№</th>
			    <th>Значение</th>
			    <th style="width: 10px;">вкл/выкл</th>
			    <th>Дополнительные данные значения</th>
			</tr>
		    </thead>
		    <tbody>
			{foreach from=$field.values key=key2 item=val}
			    <tr>
			    	<td class="hov">&nbsp;</td>
				<td style="width: 20px;">{$key2+1}</td>
				<td>
				    {$val.optionValue}
				</td>
				<td class="enableValue" align="center">
				    <div class="hold-checkbox">
					<input type="checkbox" class="ccCheckBox" name="options[{$val.optionID}][{$fieldIndex++}][optionValueID]" value="{$val.optionValueID}" id="checkbox_{$key}_{$key}" {if isset($val.productOptionID) and $val.isActive == 1}checked="checked"{/if}>
				    </div>
				</td>
				<td>
				    {$val.form}
				</td>
			    </tr>
			{/foreach}
		    </tbody>
		</table>

	    </div>
	{/foreach}
    {else}
	<div class="formBlock">
		<div class="error">
			Дла выбранного типа товаров дополнительных полей не найдено
		</div>
	</div>
    {/if}
	*}

The default stopword list is given in Section 11.8.3, “Full-Text Stopwords”. 
http://dev.mysql.com/doc/refman/5.0/en/fulltext-stopwords.html [^]
The default minimum word length and stopword list can be changed as described in Section 11.8.5, “Fine-Tuning MySQL Full-Text Search”.
http://dev.mysql.com/doc/refman/5.0/en/fulltext-fine-tuning.html [^]

Section 11.8.1, “Boolean Full-Text Searches”.
http://dev.mysql.com/doc/refman/5.0/en/fulltext-boolean.html [^]

http://dev.mysql.com/doc/refman/5.0/en/fulltext-query-expansion.html [^]

-------
mysql> SELECT SQL_CALC_FOUND_ROWS * FROM tbl_name
    -> WHERE id > 100 LIMIT 10;
mysql> SELECT FOUND_ROWS(); 
 
(0000561)
Egor_K 
09-18-07 15:23

 	try to use UNION 
 
(0000562)
Egor_K 
09-18-07 15:32

 	1. For example, we have 4 tables with fulltext index. We need to perform fast search on them.

2. Do the following:
CREATE TEMPORARY TABLE xxx SELECT id, name, MATCH(name) AGAINST ('search_string') AS relevancy FROM table1;
INSERT INTO xxx SELECT id, name, MATCH(name) AGAINST ('search_string') AS relevancy FROM table2 ... 

3. Then, when the temporary table is filled with the data, do the simple select from it:

SELECT id, name FROM xxx ORDER BY relevancy DESC

4. That's all.

I think, it is the optimal way to make a VERY FAST fulltext search from a number of tables. I hope, this will be helpful. 
 
(0000563)
Egor_K 
09-18-07 15:39

 	When using FULLTEXT queries on large set data sets it's critical to LIMIT your results. Doing some experimentation has made this very clear. Using a data set of 5.5 million rows of forum message posts indexed with a single FULLTEXT index:

select id, match(post) against('foo') as score from messages where match (body) against( 'foo' );
...
155323 rows in set (16 min 7.51 sec)

select id, match(post) against('foo') as score from messages where match (body) against( 'foo' ) limit 100;
...
100 rows in set (1.25 sec)

I ran a number of other tests on various terms known to be in the text with similar results.

These were run in reverse order shown. Make sure you return only the rows you need or you will suffer. For a search engine application returning pages of results keep in mind that nobody is going to ever see page 74! Cap the results to a reasonable maximum trading response time for completeness where possible. 
 
(0000564)
Egor_K 
09-18-07 16:22

 	http://dev.mysql.com/doc/refman/4.1/en/fulltext-search.html [^]
<h1>Заказы</h1>

{include file='paginator.tpl'}
<table class="highlightable">
    <thead>
        <tr>
            <th colspan="2">№</th>
            <th style="width: 200px;"><a href="?sort=userID" class="sort-table" data-field="userID">Пользователь</a></th>
            <th><a href="?sort=creationDate" class="sort-table" data-field="creationDate">Дата и время создания</th>
			<th><a href="?sort=status" class="sort-table" data-field="status">Статус</a></th>
        </tr>
    </thead>
    <tbody>
{if !empty($ORDERS)}
    {foreach from=$ORDERS item=order key=I}
        <tr>
        	<td style="display: none;"><a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/{$order.orderID}/" /></td>
        	<td class="hov">&nbsp;</td>
            <td style="width: 20px;">{$I+1}</td>
            <td>
                {$order.login}
            </td>
			<td>
				{$order.creationDate|date_format:'%d-%m-%Y %H:%M:%S'}
			</td>
			<td>{$order.statusText}</td>
            {*<td class="deleteEntry">
                <a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/del/{$THEME.productID}/" title="Удалить"><img src="{$ADMIN_IMAGES}delete_icon.png" alt="Удалить" style="border: 0;" /></a>
            </td>*}
        </tr>
    {/foreach}
    </tbody>
</table>
{else}
    </tbody>
</table>
    <div class="formBlock">
    	<div class="error">Записей не найдено</div>
    </div>
{/if}
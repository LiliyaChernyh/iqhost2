<h1>Управление импортом товаров из магазина {$STORE.importName}</h1>

<div style="text-align: right; margin: -30px 0 5px 0;">
    <input type="button" id="goAddTheme" class="goToBtn" value="Импорт товаров" />
    <input type="hidden" id="goAddTheme_link" value="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/add/{if !empty($STORE.importID)}{$STORE.importID}/{/if}" />
</div>
{include file='paginator.tpl'}
<table class="highlightable" id="faqTable">
    <thead>
        <tr>
            <th colspan="2">№</th>
            <th>URL файла</th>
            <th>Файл</th>
            <th width="200">Дата создания</th>
            <th width="200">Статус</th>
				{*            <th style="width: 10px;"><img src="{$ADMIN_IMAGES}icons/delete_icon.png" /></th>*}
        </tr>
    </thead>
    <tbody>
		{if !empty($FILES)}
			{foreach from=$FILES item=IMPORT key=I}
				<tr>
					{if $STORE.importID == 7}<td style="display: none;"><a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/edit/{$IMPORT.importID}/{$IMPORT.importFileID}/" /></td>{/if}
					<td class="hov">&nbsp;</td>
					<td style="width: 20px;">{$I+1}</td>
                    <td>
						{$IMPORT.fileUrl}
					</td>
					<td>
						{$IMPORT.fileName}
					</td>
					<td>
						{$IMPORT.storeXMLUpDate}
					</td>
					<td>
					{if !empty($IMPORT.log)}<a href="{$IMPORT.log}" target="_blanc">{$IMPORT.statusText}</a>{else}{$IMPORT.statusText}{/if}
				</td>
					{*<td class="deleteEntry">
					<a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/del/{$IMPORT.brandID}/" title="Удалить"><img src="{$ADMIN_IMAGES}delete_icon.png" alt="Удалить" style="border: 0;" /></a>
					</td>*}
				</tr>
			{/foreach}
		</tbody>
	</table>
{else}
</tbody>
</table>
<div class="formBlock">
	<div class="error">Записей не найдено</div>
</div>
{/if}

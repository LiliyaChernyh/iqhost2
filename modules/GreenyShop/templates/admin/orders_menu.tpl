{if $SHOW_LIST_LINK}<a href="{$MODULE_ALIAS}orders/">Список заказов</a>{/if}
{if $SHOW_FILTERS}
<div id="filtersBlock">
	<h1 style="margin-bottom:5px;">Фильтрация</h1>
	
	<form action="" method="post">
	<p>По периоду создания:  <span class="help" title="Укажите промежуток времени,  в который были созданы заказы.">?</span></p>	
		<label for="filter_creationTimeLeft" style="width:20px; display:block; float:left; margin-top:4px;">от</label>
		<input type="text" name="filter[creationTimeLeft]" id="filter_creationTimeLeft" value="{$FILTERS.creationTimeLeft}" title="Начальная дата диапазона" style="display:block; float:left; margin-top:4px; width: 90px;" />

		<label for="filter_creationTimeRight" style="width:20px; display:block; float:left; clear:left; margin-top:4px;">до</label>
		<input type="text" name="filter[creationTimeRight]" id="filter_creationTimeRight" value="{$FILTERS.creationTimeRight}" title="Конечная дата диапазона" style="display:block; float:left; margin-top:4px; width: 90px;" />
	
	<p style="clear:left; margin-top:10px; float:left;">По статусам:</p>
		{foreach from=$STATUSES item=STATUS}
		<label for="statusID_{$STATUS.statusID}">{$STATUS.description}</label><input type="checkbox" name="filter[status][]" value="{$STATUS.statusID}" />
		{/foreach}
		<input type="submit" name="doFilter" value=" Отфильровать " style="display:block;" class="handyButton" />
		<input type="submit" name="doClearFilters" value=" Снять фильтры " style="display:block; margin-top: 20px;" class="handyButton" />
	</form>	
</div>
{/if}
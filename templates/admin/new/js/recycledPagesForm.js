﻿$(document).ready(function() {
	$("#recycled_form").ajaxForm({
		beforeSubmit:  recycleFormSubmitHandler, 
        success:       recycleFormPostbackHandler,
		dataType:		"json"
	});
});

function recycleFormSubmitHandler(form, jo)
{
	if ($("#recycled_form input:checkbox:checked").length == 0)
	{
		$.jGrowl("Не выбрано ни одной страницы");
		return;
	}
}

function recycleFormPostbackHandler(data, status)
{
	if (!standartAJAXResponseHandler(data, status)) return;
	switch (data.action)
	{
		case 'deleted':
			$.jGrowl("Страницы были удалены");
			break;
			
		case 'restored':
			$.jGrowl("Страницы были восстановлены");
			break;
	}
	$("#recycled_form input:checkbox:checked").parent().remove();
	reloadTree();
	$("#pagesRecycled").text($("#recycled_form input:checkbox").length);
}
<?php

/**
 * Класс работы с пользователями с ролями.
 *
 * @version 2.3
 */
class UsersWithRolesManager extends UsersManager 
{
	function __construct()
	{
		parent::__construct();
		
		$this -> GetRoles();
	}
	
	public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }

	public $roles = null;
	public $rolesInfo = null;
	
	/**
	 * Возвращает массив ролей
	 *
	 * @return array
	 */
	public function GetRoles()
	{
		if ($this -> roles == null)
		{
			$query = "
                SELECT  `column_type` 
                FROM    `information_schema`.`columns` 
				WHERE   `table_name` ='". TablesNames::$USERS_TABLE_NAME ."' 
				    AND `column_name` = 'role'
				    AND `table_schema` = '". DB::$DBName ."'
		    ";

			$result = DB::FetchAssoc(DB::Query($query));
			$this -> roles = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$result['column_type']));
		}

		return $this -> roles;
	}
	
	public function GetRoleInfo($role)
	{
	    $query = "
            SELECT  * 
            FROM    `". TablesNames::$ROLES_TABLE_NAME ."`
            WHERE   `role` = '". $role ."'
	    ";
        $res = DB::QueryToArray($query);
        if (!empty($res))
            return array_shift($res);
	    return null;
	}
	
	public function GetRolesWithInfo()
	{
	    $query = "
            SELECT  * 
            FROM    `". TablesNames::$ROLES_TABLE_NAME ."`
            ORDER BY `level` ASC
	    ";

	    return DB::QueryToArray($query);
	}
	
	public function Role($role)
	{
	    if (!in_array($role, $this -> roles))
	        return null;
	        
        if (empty($this -> rolesInfo[$role]))
        {
            $roleInfo = $this -> GetRoleInfo($role);
            if (empty($roleInfo))
                return null;
            $this -> rolesInfo[$role] = $roleInfo;
        }

        return $this -> rolesInfo[$role] = $roleInfo;
	}
}

?>
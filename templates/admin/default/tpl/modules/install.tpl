
			<h1>Управление модулями</h1>
			
			<div>
				{if !empty($HAS_UNINSTALLED_MODULES)}
					Список неустановленных модулей:<br/><br/>
					{foreach from=$UNINSTALLED_MODULES item=UNINSTALLED_MODULE key=I}
						
							<form action="{$SECTION_ALIAS}install/{$UNINSTALLED_MODULE}/" method="post">
								<div><label>{$I+1}. {$UNINSTALLED_MODULE}</label>
								<input type="submit" class="submitLink" name="doInstall" value="[ установить ]" />
								</div>
							</form>
					{/foreach}
				{/if}

				{if !empty($NO_UNINSTALLED_MODULES)}
					Все модули установлены.
				{/if}
			</div>
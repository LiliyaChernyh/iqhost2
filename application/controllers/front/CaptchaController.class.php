<?php
/**
 * Контроллер капчи
 * 
 * @version 1.0
 *
 */
class CaptchaController extends Controller 
{
	/**
	 * Action
	 * Получение капчи
	 *
	 * @return EmptyActionResult
	 */
    public function GetCaptcha()
    {
        Captcha::GetCaptcha();
        return new EmptyActionResult();
    }
}
?>
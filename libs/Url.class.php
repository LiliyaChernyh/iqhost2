<?php

/**
 * Класс для работы с URL
 *
 */
class Url
{
    /**
     * Проверяет, существует ли заданный URL
     *
     * @param  string    $url
     * @access static
     * @return bool      True if the url is accessible and false if the url is unaccessible or does not exist
     * @throws Exception An exception will be thrown when Curl session fails to start
     */
    public static function Exists($url)
    {
        if ($url == null || trim($url) == '')
            throw new Exception('URL должно быть непустой строкой');
       
        $handle = curl_init($url);

        if ($handle == false)
            throw new Exception('Не удалось начать сессию Curl');

        curl_setopt($handle, CURLOPT_HEADER, false);
        curl_setopt($handle, CURLOPT_NOBODY, true);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, false);

        // grab Url
        $connectable = curl_exec($handle);

        /**
         * Закрываем соединение и высвобождаем память
         */
        curl_close($handle);
        
        return $connectable;
    }
    
    /**
     * Определяет размер файла
     *
     * @param 	string 		$url
     * @access 	static
     * @return 	int			Размер файла
     * @throws 	Exception	В случае невозможности соединения с сервером или невозможности чтения информации о размере файла
     */
    public static function FileSize($url)
    {
    	$urlParts = explode("/", str_replace("http://", "", $url));
    	
    	$fhost = $urlParts[0];
    	unset($urlParts[0]);
    	
    	$fname = "/".implode("/", $urlParts);
    	
		$response = "";
		if (!$fp = fsockopen($fhost, 80, &$errno, &$errstr, 30))
			throw new Exception("Не удалось установить соединение с сервером");
		
		fputs($fp, "HEAD $fname HTTP/1.0\nHOST: $fhost\n\n");
		while(!feof($fp)) 
			$response .= fgets($fp, 128);
		fclose($fp);
		
		if (ereg("Content-Length: ([0-9]+)", $response, $size)) 
		{
			if (is_numeric($size[1]))
				return $size[1];
			else
				throw new Exception("Не удалось определить размер файла.");
		}
		else 
			throw new Exception("Не удалось определить размер файла");
    }
}

?>
/**
*  Инициализатор контрола DateInput
*/
function DateInputInit(initObject)
{
	var params = {};
	if (typeof initObject.yearRange != "undefined" && initObject.yearRange != null)
	   params["yearRange"] = initObject.yearRange;

	$("#" + initObject.name).datepicker(params);
}
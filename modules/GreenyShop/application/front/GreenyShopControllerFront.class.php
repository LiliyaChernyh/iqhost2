<?php

/**
 * Контроллер для работы с модулем GreenyShop во фронт-енде
 *
 * @version 2.0
 */
class GreenyShopControllerFront extends FrontController
{
	/**
	 * Корзина
	 *
	 * @var array
	 */
	public $cart;

	/**
	 * GreenyShop Engine
	 *
	 * @var GreenyShop
	 */
	public $shop;


        public function __construct()
        {
            parent::__construct();
            $this -> view = new GreenyShopViewFront();
            $this -> model = new GreenyShopModelFront();
        }

        public function Index($args = null)
        {
            ViewData::Append('SCRIPT_FILE', AppSettings::$SCRIPTS_ALIAS .'contacts.js');

            // Используем статический метод для получения параметра rootAlias
            $settings = CommonModule::LoadSettings('GreenyShop');
            $rootAlias = $settings['rootAlias'];

            // Получаем страницу из базы
            $this -> page = new Page($rootAlias);

            // Получаем контент
            $pageContent = $this -> page -> Content();

            ViewData::Assign('CONTENT1', $pageContent['pageContent']);
            $this -> view -> Index();
            return $this -> view;
        }

	public function GreenyShopFront()
	{
		if(!isset($_SESSION))
		 	@session_start();
		$this -> LoadSessionVariables();
	}

	/**
	 * Пересчитывает корзину по тому, что содержится в POST-запросе
	 *
	 */
	public function Recount()
	{
		$goodsTable = new DBTableManager(TablesNames::$GOODS_TABLE_NAME);

		// Массив для контроля дубликатов
		$itemsIDs = array();
		$hasDoubles = false;

		foreach ($_POST['cart'] as $key => $value)
		{
			if (!is_numeric($value['itemID']))
				continue;
			if ($value['amount'] <= 0)
			{
				unset($this -> cart[$key]);
				continue;
			}
			elseif (is_numeric($value['amount']))
				$this -> cart[$key]['amount'] = $value['amount'];

			// Собираем дублирующиеся товары, которые есть с и без установки
			if (isset($itemsIDs[$value['itemID']]))
				$itemsIDs[$value['itemID']]++;
			else
			{
				$itemsIDs[$value['itemID']] = 1;
				$hasDoubles = truel;
			}

			if ($this -> cart[$key]['install'] != $value['install'])
			{
				// Проверим требуется продаётся ли только с установкой
				if (!$value['install'])
				{
					$good = array_pop($goodsTable -> Select(array('itemID' => $value['itemID'])));
					if (empty($good))
						continue;

					$this -> cart[$key]['install'] = ($good['installationRequired']) ? 1:0;
				}
				else $this -> cart[$key]['install'] = 1;
			};
		}

		// Теперь необходимо просуммировать дубликаты, если взяли
		// Одно и то же с и без установки
		if ($hasDoubles)
		{
			foreach ($itemsIDs as $itemID => $count)
			{
				if ($count > 1)
				{
					$firstKey = null;
					foreach($this -> cart as $key => $value)
					{
						if ($value['itemID'] == $itemID)
						{
							if ($firstKey == null)
							{
								$firstKey = $key;
							}
							else if($this -> cart[$firstKey]['install'] == $this -> cart[$key]['install'])
							{
								$this -> cart[$firstKey]['amount'] += $this -> cart[$key]['amount'];
								unset($this -> cart[$key]);
							}
						}
					}
				}
			}
		}

		$this -> SaveSessionVaribales();
	}

	/**
	 * Добавляет товар в корзину
	 *
	 * @param int $itemID
	 * @param int $amount
	 */
	public function AddToCart($itemID, $amount = 1, $withInstallation = false)
	{
		if (!is_numeric($itemID) || $itemID <= 0) throw new ArgumentException('itemID must be a positive number');
		if (!is_numeric($amount)) throw new ArgumentException('Amount must be a number');

		$isAdded = false;
		foreach ($this -> cart as $key => $value)
		{
			if ($value['itemID'] == $itemID &&
				$value['install'] == $withInstallation)
				{
					$this -> cart[$key]['amount'] += $amount;
					$isAdded = true;
					if ($this -> cart[$key]['amount'] <= 0)
						$this -> RemoveFromCart($itemID);
				}
		}

		if (!$isAdded && $amount > 0)
		{
			$entry = array();
			$entry['itemID'] = $itemID;
			$entry['install'] = $withInstallation;
			$entry['amount'] = $amount;
			$this -> cart[] = $entry;
		}

		$this -> SaveSessionVaribales();
	}

	public function RemoveFromCart($itemID, $withInstallation = false)
	{
		if (!is_numeric($itemID) || $itemID <= 0) throw new ArgumentException('itemID must be a positive number');

		foreach ($this -> cart as $key => $value)
		{
			if ($value['itemID'] == $itemID &&
				$value['install'] == $withInstallation)
				{
					unset($this -> cart[$key]);
				}
		}
		$this -> SaveSessionVaribales();
	}

	public function MakeOrder($customerID, $comments, $deliveryDate, $deliveryAdressID)
	{
		if (empty($this -> cart))
			throw new Exception('Невозможно оформить заказ. Корзина пуста.', 1012300);
		$orderAttrs = array();
		$orderAttrs['customerID'] = $customerID;
		$orderAttrs['tip'] = $comments;
		$stamp = strtotime($deliveryDate);
		if ($stamp > 0)
			$orderAttrs['deliveryDate'] = date('Y-m-d', $stamp);
		$orderAttrs['deliveryAdressID'] = $deliveryAdressID;

		$this -> shop = new GreenyShop();
		$this -> shop -> currentOrderID = $this -> shop -> CreateOrder($orderAttrs);
		//trace($this -> cart);
		$goodsTable = new DBTableManager(TablesNames::$GOODS_TABLE_NAME);
		foreach ($this -> cart as $key => $value)
		{
			$item = array_pop($goodsTable -> Select(array('itemID' => $value['itemID'])));
			//trace($item);
			if (empty($item))
                continue;
			if ($item['installationRequired'])
                $value['install'] = 1;
			$price = $value['install'] ? $item['priceWithInstallation'] : $item['priceWithoutInstallation'];
			$this -> shop -> AddToCart($value['itemID'], $value['amount'], $value['install'], $price);
		}
		$_SESSION['orderID'] = $this -> shop -> currentOrderID;

		$this -> ClearSessionCart();
		return $_SESSION['orderID'];
	}

	private function LoadSessionVariables()
	{
		$this -> cart = array();
		if (isset($_SESSION['cart']))
		{
			$this -> cart = unserialize($_SESSION['cart']);
		}
	}

	private function SaveSessionVaribales()
	{
        unset($_SESSION['cart']);
        if ($this->cart != null)
        {
			$_SESSION['cart'] = serialize($this -> cart);
		}
	}

	private function ClearSessionCart()
	{
        unset($_SESSION['cart']);
    }

}
?>
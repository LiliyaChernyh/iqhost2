var user_data = null;

$(document).ready(function(){
     /* checkbox */
    
     // Просчет состояния checkbox
     $('.checkbox input').each(function(){
          if ($(this).val() == '0'){
               $(this).removeAttr('checked').parent('.checkbox').css('background-position','0 top');
          }else{
               $(this).attr('checked','checked').parent('div.checkbox').css('background-position','0 -13px');
          }
     });
     
     //Изменение состояния checkbox
     $('.checkbox input').click(function(event){
          event.stopPropagation();
         
          if ($(this).val() == '0'){
               $(this).val('1');
               $(this).attr('checked','checked').parent('div.checkbox').css('background-position','0 -13px');
               
               var tagID = $(this).closest('a').children('input').val();
               $('#block-fotos_' + tagID + ' .list-image').each(function(){
                   $(this).addClass('active');
               });
          }else{
               $(this).val('0');
               $(this).removeAttr('checked').parent('.checkbox').css('background-position','0 top');
                
               var tagID = $(this).closest('a').children('input').val();
               $('#block-fotos_' + tagID + ' .list-image').each(function(){
                   $(this).removeClass('active');
               });
          }
          
          //запись имени input для сохранения формы
          if (window.user_data !== null){
            $input = $(this).closest('a').children('input');
            if ($(this).val() == 1){
                if ($input.attr('name') !== 'del_tagID[]'){
                    $input.attr('name', 'add_tagID[]');
                }else{
                    $input.removeAttr('name');
                }
            }else{
                if ($input.attr('name') !== 'add_tagID[]'){
                    $input.attr('name', 'del_tagID[]');
                }else{
                    $input.removeAttr('name');
                }
            }
         }
     });
     
     /* checkbox [end] */
     
     //class active для списка фото
     $('.list-image').live('click',function(){
        //запоминаем предыдущее значение
        var $head_foto = $('#head-foto_' + $(this).closest('div').attr('data-tag-id'));
        var $checkbox_val = $head_foto.find('.checkbox input').val();
        
        //меняем активность элемента
        $(this).toggleClass('active'); 
         
        //checkbox и фото
        var $ul = $(this).parent();
        var $li = $ul.children('li');
        var $li_active = $ul.children('li.active');
        var $head_input = $head_foto.find('input[type=hidden]');

        if ($li_active.length == 0){
            $head_foto.find('.checkbox').css('background-position','0 top')
                    .children('input').val('0');
            
            //именуем элементы
//            if ($checkbox_val == 1){
//                if ($head_input.attr('name') == 'add_tagID[]'){
//                    $head_input.removeAttr('name');
//                }else{
//                    $head_input.attr('name', 'del_tagID[]');
//                }
//            }else if($checkbox_val == 2){
//                if ($head_input.attr('name') != 'add_tagID[]'){//////////////////////////////////////////////////////////////
//                    $li.each(function(){
//                        if ($(this).children('input').attr('name') == 'add_imageID[]'){
//                            $(this).children('input').removeAttr('name');
//                        }else{
//                            $(this).children('input').attr('name', 'del_imageID[]');
//                        }
//                    });
//                }
//            }
        }else if ($li.length == $li_active.length){ 
            $head_foto.find('.checkbox').css('background-position','0 -13px')
                    .children('input').val('1');
            
            //именуем элементы
//            if ($head_input.attr('name') == 'del_tagID[]'){
//                $head_input.removeAttr('name');
//            }else{
//                $head_input.attr('name', 'add_tagID[]');
//            }
//            
//            $li.each(function(){
//                if ($(this).children('input').attr('name') == 'add_imageID[]'){
//                    $(this).children('input').removeAttr('name');
//                }else{
//                    $(this).children('input').attr('name', 'del_imageID[]');
//                }
//            });
        }else{
            $head_foto.find('.checkbox').css('background-position','0 -26px')
                    .children('input').val('2');
            
            //именуем элементы
//            if ($checkbox_val == 1){
//                if ($head_input.attr('name') == 'add_tagID[]'){
//                    $head_input.removeAttr('name');
//                }else{
//                    $head_input.attr('name', 'del_tagID[]');
//                }
//                
//                $li_active.each(function(){
//                    if ($(this).children('input').attr('name') == 'del_imageID[]'){
//                        $(this).children('input').removeAttr('name');
//                    }else{
//                        $(this).children('input').attr('name', 'add_imageID[]');
//                    }           
//                });
//            }else{
//                $li.each(function(){
//                    if ($(this).hasClass('active')){
//                        if ($(this).children('input').attr('name') == 'del_imageID[]'){
//                            $(this).children('input').removeAttr('name');
//                        }else{
//                            $(this).children('input').attr('name', 'add_imageID[]');
//                        }
//                    }else{
//                        if ($(this).children('input').attr('name') == 'add_imageID[]'){
//                            $(this).children('input').removeAttr('name');
//                        }else{
//                            $(this).children('input').attr('name', 'del_imageID[]');
//                        }
//                    }
//                });
//            }
        }   
    });
    
    //class active для списка пользователей
    $('.list-user').live('click',function(){
        $('.list-user').each(function(){
            $(this).removeClass('active');
        });
        
        $(this).toggleClass('active');
        
        //очищаем имена
        $('input[type=hidden]').removeAttr('name');
    });
    
    //получаем фото выделенного пользователя
    $('.list-user').live('click',function(){
        $.ajax({
            url: '/admin/Fotobank/',
            data: ({userID: $(this).children('input').val()}),
            async: false,
            dataType: 'json',
            type: 'POST',
            success: function(data){
                if (data.success !== undefined){
                    window.user_data = data;
                    
                    //очищаем значения
                    $('.checkbox').css('background-position','0 top')
                            .children('input').val('0');
                    
                    $('.list-image').each(function(){
                        $(this).removeClass('active');
                    });
                    
                    //выставляем значения
                    if (data.tags !== undefined){
                        for (var i in data.tags) {
                            $('#head-foto_' + data.tags[i].tagID).find('.checkbox').css('background-position','0 -13px')
                                    .children('input').val('1');
                            
                            $('#block-fotos_' + data.tags[i].tagID).find('li.list-image').addClass('active');
                        }
                    }
                    
                    if (data.fotos !== undefined){
                        for (var i in data.fotos) {
                            var $hold = $('#list-image_' + data.fotos[i].imageID);
                            $hold.addClass('active');
                            
                            if ($('#head-foto_' + data.fotos[i].tagID).find('.checkbox').children('input').val() == '0')
                                $('#head-foto_' + data.fotos[i].tagID).find('.checkbox').css('background-position','0 -26px')
                                        .children('input').val('2');
                            
                        }
                    }
                }
            }
        });
    });
    
    $('#foto').submit(function(){
        if (($('.list-user.active').length == 0) || (window.user_data == null)){
            alert('Выберите пользователя');
            return false;
        }
        $('.list-user.active input').attr('name', 'userID');
        
        //запись имени input для сохранения формы
        if (window.user_data !== null){
            var ar_tags = new Array();
            for (var i in window.user_data.tags) {
                ar_tags[i] = window.user_data.tags[i].tagID;
            }
            var ar_fotos = new Array();
            for (var i in window.user_data.fotos) {
                ar_fotos[i] = window.user_data.fotos[i].imageID;
            }

            $('.head-foto-input').each(function(){
                var $checkbox_val = $(this).parent().find('.checkbox input').val();
                if (ar_tags.indexOf($(this).val()) == -1){
                    //нет в массиве
                    if ($checkbox_val == '1'){
                        $(this).attr('name', 'add_tagID[]');
                    }
                }else{
                    //есть в массиве
                    if ($checkbox_val != '1'){
                        $(this).attr('name', 'del_tagID[]');
                    }
                }
            });
            $('.block-fotos-input').each(function(){
                var $checkbox_val = $('#head-foto_' + $(this).closest('div').attr('data-tag-id')).find('.checkbox input').val();
                var $list_image_active = $(this).parent().hasClass('active');
                if (ar_fotos.indexOf($(this).val()) == -1){
                    //нет в массиве
                    if (($checkbox_val != '1') && $list_image_active){
                        $(this).attr('name', 'add_imageID[]');
                    }
                }else{
                    //есть в массиве
                    if (!$list_image_active || ($list_image_active && ($checkbox_val == '1'))){
                        $(this).attr('name', 'del_imageID[]');
                    }
                }
            });
        }
    });
});

function InitCollabse(){
    new jQueryCollapse($('#block-list-users'), {
        close: function() {
              this.slideUp(150);
        },
        open: function() {
              this.slideDown(150);
        },
        /*accordion: true,*/
        persist: true
    });

    new jQueryCollapse($("#block-list-tags"), {
        open: function() {
            this.slideDown(150);
        },
        close: function() {
              this.slideUp(150);
        },
        /*accordion: true,*/
        persist: true
    });
    
    
    //автоподгрузка фото
    new jQueryCollapse($(".block-list-fotos"), {
        open: function() {
            var $hold = this;
            
            $.ajax({
                url: '/admin/Fotobank/?tagID=' + $hold.attr('data-tag-id'),
                cache: false,
                async: false,
                dataType: 'html',
                success: function(html){
                    $hold.html(html);
                    $hold.slideDown(150);
                    
                    var checkbox_val = $('#head-foto_' + $hold.attr('data-tag-id')).find('div.checkbox input').val();
                    if (checkbox_val == 1){
                        $hold.find('li.list-image').each(function(){
                            $(this).addClass('active');
                        });
                    }else if(checkbox_val == 2){
                        $hold.find('li.list-image').each(function(){
                            for (var i in window.user_data.fotos) {
                                if ($(this).children('input').val() == window.user_data.fotos[i].imageID){
                                    $(this).addClass('active');
                                }
                            }
                        });
                    }
                }
            });
        },
        close: function() {
            this.slideUp(150);
        },
        /*accordion: true,*/
        /*persist: true*/
    });
};
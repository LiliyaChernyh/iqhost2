<?php
###############################
#
#	ChangesLogger
#	версия 1.0.1
#
###############################
define("CHANGES_TABLE_NAME", "Changes");
define("FIELDS_REGISTRY_TABLE_NAME", "LoggedFieldsRegistry");
class ChangesLogger
{
	/**
	 * Заносит значения ассоциативного массива $fieldsAndValues
	 * в таблицу логов, если они отличаются от текущих и
	 * эти поля необходимо логгировать (что указывается в таблице
	 * LoggedFileldsRegistry)
	 *
	 * @param mixed $table
	 * @param array $fieldsAndValues
	 * @param int $rowID
	 * @param int $userID
	 */
	public static function Log(&$table, $fieldsAndValues, $rowID, $userID = null)
	{
		if (is_string($table))
		{
			$TableMngr = new DBTableManager($table);
		}
		elseif (is_object($table) && get_class($table) == "DBTableManager") 
		{
		    $TableMngr = &$table;
		}
		else 
			throw new ArgumentException("Неверный тип аргумента. Должна быт строка или DBTableManager");
		
		$rows = $TableMngr -> Select(array($TableMngr->TableStructure->PrimaryKey->Name => $rowID));
		if (count($rows) == 0) throw new Exception("Запись не найдена. Логгирование невозможно");
		
		$row = $rows[0];
		$isUpdated = false;
		// Теперь сверим поля делаем это сейчас, поскольку если не
		// различий нет, то и не будет делать нижеописанные запросы
		foreach ($fieldsAndValues as $key => $value)
		{
			if (isset($row[$key]))
			{
				//echo $value . " == " . $row[$key] . "<br/>";
				if ($value == (get_magic_quotes_gpc() ? addslashes($row[$key]):$row[$key]) || $TableMngr -> TableStructure -> FieldsAssoc[$key] -> Type == "blob")
					unset($fieldsAndValues[$key]);
				else $isUpdated = true;
			}
			else 
			{
				unset($fieldsAndValues[$key]);
			}
		}

		if (!$isUpdated) return 0;
		
		$query = "SELECT * FROM ". FIELDS_REGISTRY_TABLE_NAME ." 
					WHERE tableName = '" .$TableMngr -> TableStructure -> TableName . "'";
		
		$fiedsToLog = DB::QueryToArray($query, "fieldName", "description");
		
		if (count($fiedsToLog) == 0) return 0;
		$changesTableMngr = new DBTableManager(CHANGES_TABLE_NAME);
		$rows = array();
		foreach ($fieldsAndValues as $key => $value)
		{
			$rows[] = array("tableName" => $TableMngr -> TableStructure -> TableName,
							"fieldName" => $key,
							"rowID" => $rowID,
							"oldValue" => (!get_magic_quotes_gpc() ? $row[$key] : addslashes($row[$key])), // Должно экранироватся в TableManager -> Insert
							"newValue" => $value,
							"changeDate" => date("Y-m-d H:i:s"),
							"userID" => ($userID == null ? "NULL" : $userID));
 		}
 		$changesTableMngr -> InsertRows($rows);
 		return 1;
	}
	
	/**
	 * Возвращает историю изменений для заданной
	 * записи в таблице. Возможно указать конкретное поле.
	 *
	 * @param string $tableName
	 * @param string $rowID
	 * @param string $fieldName
	 */
	public static function GetLogHistory($tableName, $rowID, $fieldName = null)
	{
		$changesTableMngr = new DBTableManager(CHANGES_TABLE_NAME);
		$attrs = array('tableName' => $tableName, 'rowID' => $rowID);
		if ($fieldName != null) $attrs['filedName'] = $fieldName;
		return $changesTableMngr -> Select($attrs);
	}
	
	/**
	 * Возвращает состояние поля или записи в определённое время если есть.
	 *
	 * @param string $tableName
	 * @param string $rowID
	 * @param string $date
	 * @param string $fieldName
	 */
	public static function GetStateForDate($tableName, $rowID, $datetime, $fieldName = null)
	{
		if (!preg_match("/(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2}):(\d{2})/", $datetime))
			throw new ArgumentException("Неверный формат даты!");
		$query = "SELECT * FROM ". CHANGES_TABLE_NAME ." 
				WHERE tableName='$tableName'
					AND rowID = '$rowID' ". 
					($fieldName == null ? "": " AND fieldName='$fieldName' ").
					"AND changeDate >= '$datetime'";
		return $queryResult = DB::QueryToArray($query);
	}
	
}
?>
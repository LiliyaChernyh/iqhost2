<script type="text/javascript">
	// JS code is in /modules/AnyEditor/anyeditor.js
	var editMode = false;
	{if !empty($EDIT_MODE)}editMode = true;{/if}
</script>
<h1>{$CONTENT_HEADER}</h1>
<div>
	<a href="{$CONTROLLER_ALIAS}items/?table={$TABLE_NAME}">Вернуться к списку записей</a>
	{if isset($EDIT_MODE)}
		<br/>
		<a href="{$CONTROLLER_ALIAS}add/?table={$TABLE_NAME}">Добавить новую запись</a>
	{/if}
</div>
<form action="" method="post" id="contentForm" enctype="multipart/form-data">

		{include file="formBlocks.tpl"}

	<div style="margin-left:350px">
		<input type="submit" value="Сохранить" name="{$FORM_SUBMIT_NAME}" class="handyButton" />
	</div>

</form>
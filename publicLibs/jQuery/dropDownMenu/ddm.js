(function($) {
  // plugin definition
  $.fn.dropDownMenu = function(options) 
  {
    var opts = $.extend({}, $.fn.dropDownMenu.defaults, options);
	$(document).click(dropDownMenu_close);
    return this.each(function ()
	{
		$(this).children("li").hover(dropDownMenu_open, dropDownMenu_timer);
	});
  };
  
  $.fn.dropDownMenu.defaults = {ddmenuTimeout: 500,   ddMenuCloseTimer: 0};
  $.fn.dropDownMenu.ddmenuitem = 0;
  
  function dropDownMenu_open()
  {	dropDownMenu_canceltimer();
	dropDownMenu_close();
	$.fn.dropDownMenu.ddmenuitem = $(this).find('ul').eq(0).css('visibility', 'visible');}

  function dropDownMenu_close()
  {	if($.fn.dropDownMenu.ddmenuitem) $.fn.dropDownMenu.ddmenuitem.css('visibility', 'hidden');}

  function dropDownMenu_timer()
  {	$.fn.dropDownMenu.defaults.ddMenuCloseTimer = window.setTimeout(dropDownMenu_close, $.fn.dropDownMenu.defaults.ddmenuTimeout);}

  function dropDownMenu_canceltimer()
  {	if($.fn.dropDownMenu.defaults.ddMenuCloseTimer)
	{	window.clearTimeout($.fn.dropDownMenu.defaults.ddMenuCloseTimer);
		$.fn.dropDownMenu.defaults.ddMenuCloseTimer = null;}}

})(jQuery);
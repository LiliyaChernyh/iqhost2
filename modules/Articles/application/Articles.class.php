<?php

/**
 * Статьи
 *
 * @version 3.1
 *
 */
class Articles extends CommonModule
{
    /**
     * Объект таблицы со статьями
     *
     * @var DBTableManager
     */
    public $articlesTableManager = null;

    /**
     * Название таблицы статей
     *
     * @var string
     */
    public static $articlesTableName = 'greeny_articles';

    /**
     * Объект таблицы с категориями статей
     *
     * @var DBTableManager
     */
    public $sectionsTableManager = null;

    /**
     * Название таблицы категорий статей
     *
     * @var string
     */
    public static $sectionsTableName = 'greeny_articles_sections';

    /**
     * Число найденных строк в методе GetArticles
     *
     * @var int
     */
    public $foundArticlesCnt = 0;

    /**
     * Конструктор.
     *
     * @return Articles
     */
    public function __construct()
    {
        parent::__construct(__CLASS__);
        $this->articlesTableManager          = new DBTableManager(self::$articlesTableName);
        $this->articlesTableManager->SortOrder = 'a.`publicationDate` DESC, a.`modificationDate` DESC, a.`articleID` DESC';

        $this->sectionsTableManager              = new DBTableManager(self::$sectionsTableName);
        $this->sectionsTableManager->SortOrder = '`asectionID` ASC';

        //$this -> LoadSettings('Articles');
        //trace($this -> LoadSettings('Articles'));
    }

    public function GetSections($mode=0)
    {
        $sections = $this->sectionsTableManager->Select(['isDeleted'=>0]);
        if($mode != 0) array_unshift($sections, array('caption' => "Все новости", 'alias' => ""));
        if($mode == 2) array_push($sections, array('caption' => "Без категорий", 'alias' => "unsort"));
        $sec      = array();
        foreach($sections as $item)
        {
            $item['caption'] = UTF8::ucfirst($item['caption']);
            $item['fullAlias']   = "/" . $this->settings['rootAlias'] . "/" . (!empty($item['alias']) ? $item['alias'] . "/" : '');
            $sec[$item['alias']] = $item;
        }
        return $sec;

    }

    public function AddSections($attr)
    {
        return $this->sectionsTableManager->Insert($attr);
    }

    public function UpdateSections($sectionID, $caption)
    {
        return $this->sectionsTableManager->Update(array('caption' => $caption), $sectionID);
        ;
    }


    /**
     * Производит 'мягкое' удаление статьи (помещение в корзину)
     *
     * @param int $articleID
     */
    public function DeleteSection($sectionID)
    {
        // Проверяем корректность переданных данных
        if(!is_numeric($sectionID)) throw new ArgumentException('Неверный идентификатор категории');


        $res = DB::QueryToArray("SELECT asectionID FROM ".self::$articlesTableName." WHERE asectionID = ".$sectionID."");

        $count = count($res);

        if ($count > 0)
            {
                return false;
            }
        else
            {
               $this->sectionsTableManager->Update(array('isDeleted' => 1), $sectionID);
            }


    }

    public function GetRecycledSections()
    {
        $sections = $this->sectionsTableManager->Select(['isDeleted'=>1]);
        $sec      = array();
        foreach($sections as $item)
        {
            $item['caption'] = UTF8::ucfirst($item['caption']);
            $item['fullAlias']   = "/" . $this->settings['rootAlias'] . "/" . (!empty($item['alias']) ? $item['alias'] . "/" : '');
            $sec[$item['alias']] = $item;
        }
        return $sec;

    }

     public function RestoreSections($sectionID)
    {
        // Проверяем корректность переданных данных
        if(!is_numeric($sectionID)) throw new ArgumentException('Неверный идентификатор категории');

        $this->sectionsTableManager->Update(array('isDeleted' => '0'), $sectionID);
    }

     public function WipeSections($sectionID)
    {
        $this->sectionsTableManager->Delete(array('asectionID' => $sectionID));
    }

    /**
     * Производит 'мягкое' удаление статьи (помещение в корзину)
     *
     * @param int $articleID
     */
    public function DeleteArticles($articleID)
    {
        // Проверяем корректность переданных данных
        if(!is_numeric($articleID)) throw new ArgumentException('Неверный идентификатор новости');



        $this->articlesTableManager->Update(array('isDeleted' => 1), $articleID);
    }

    /**
     * Производит восстановление статьи из корзины
     *
     * @param int $articleID
     */
    public function RestoreArticles($articleID)
    {
        // Проверяем корректность переданных данных
        if(!is_numeric($articleID)) throw new ArgumentException('Неверный идентификатор новости');

        $this->articlesTableManager->Update(array('isDeleted' => '0'), $articleID);
    }

    /**
     * Подсчитывает количество статей с заданными атрибутами
     *
     * @param array $attributes
     */
    public function ArticlesNumber($attributes)
    {
        return $this->articlesTableManager->Count($attributes);
    }

    /**
     * Возвращает массив статей, удовлетворяющийх заданным атрибутам
     *
     * @param array $filters
     * @param int $limit
     * @param int $start
     * @return array
     */
    public function GetArticles($filters = null, $limit = null, $start = null)
    {
        $lim = (!empty($limit) && is_numeric($limit)) ? (' LIMIT ' .
            ((!is_numeric($start)) ? 0 : $start) .
            ', ' . $limit) : '';

        $condition = '';
        $joins     = '';

        if(!is_null($filters))
        {
            $condArray  = array();
            $joinsArray = array();
            foreach($filters as $filter => $value)
            {
                switch($filter)
                {
                    case 'published':
                        switch($value)
                        {
                            case 'yes':
                                $condArray[] = 'a.`publicationDate` <= ' . DB::Quote(date('Y-m-d H:i:s'));
                                break;

                            case 'no':
                                $condArray[] = 'a.`publicationDate` > ' . DB::Quote(date('Y-m-d H:i:s'));
                                break;
                        }
                        break;

                    case 'hasRelease':
                        if($value == 1)
                        {
                            $condArray[] = 'a.`release` IS NOT NULL';
                        }
                        break;

                    default:
                        if(!is_null($value))
                        {
                            $condArray[] = 'a.`' . $filter . '` = ' . DB::Quote($value);
                        }
                        break;
                }
            }
            if(!empty($condArray)) $condition = ' WHERE ' . implode(' AND ', $condArray);
            if(!empty($joinsArray)) $joins     = ' ' . implode(' ', $joinsArray);
        }

        $query                    = "
                SELECT SQL_CALC_FOUND_ROWS DISTINCT
                    a.*,
                    g.caption as cat_caption,g.alias as cat_alias
                FROM `" . self::$articlesTableName . "` a
                LEFT JOIN `" . self::$sectionsTableName . "` g ON a.`asectionID` = g.`asectionID`
                    " . $joins . "
                    " . $condition . "
                ORDER BY " . $this->articlesTableManager->SortOrder . "
                    " . $lim;
//                    trace($query);
        $result                   = DB::QueryToArray($query);
        $this->foundArticlesCnt = DB::QueryOneValue("SELECT FOUND_ROWS()");

        return $result;
    }

    /**
     * Возвращает статьи, удовлетворяющие заданным атрибутам и с указанным полями
     *
     * @param array $attributes
     * @return array
     */
    public function GetFieldsArticles($attributes = null, $fields = null)
    {
        return $this->articlesTableManager->Select($attributes, null, null, $fields);
    }

    /**
     * Возвращает статью с указанным ID
     *
     * @param array $attributes
     * @return array
     */
    public function GetSingleArticles($articleID)
    {
        return $this->articlesTableManager->Select(array('articleID' => $articleID));
    }

    public function WipeArticles($articleID)
    {
        $this->articlesTableManager->Delete(array('articleID' => $articleID));
    }

    /**
     * Создание пустого элемента модуля при создании страницы, ассоциированной с модулем.
     * Так как, в данном модуле создавать пустой элемент не нужно, функция просто возвращает успех.
     *
     * @param array $moduleNameArray
     * @return mixed
     */
    public function CreateEmptyElement($moduleNameArray)
    {
        return true;
    }

    public function DeleteObject()
    {
        return true;
    }

    public function CountSections()
    {
        $count = DB::QueryOneRecordToArray("SELECT count(*) as total FROM `".self::$sectionsTableName."` WHERE isDeleted =1");
        return $count;
    }

    public function CountArticles()
    {
        $count = DB::QueryOneRecordToArray("SELECT count(*) as total, sum(if(isPublished=1 , if( isDeleted=0,1,0),0)) as published, sum(if(isPublished=0 , if( isDeleted=0,1,0),0)) as unpublished, sum(if(isDeleted=1,1,0)) as recycled FROM `".self::$articlesTableName."`");
        return $count;
    }

}

?>
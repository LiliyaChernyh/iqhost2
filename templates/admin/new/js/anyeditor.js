$(document).ready(function ()
{
	$(document).ajaxError(function(event, request, settings){$.jGrowl('AJAX Error')});
	$('#contentForm').validate(
	{
	   submitHandler: function(form) {
		   	$(form).ajaxSubmit({success:saveFormResponse, beforeSubmit:showRequest, dataType:"json"});
			return false;
		}
   });
   $("[required]").each(function(){ $(this).rules("add", {"required" : true});});
   $(".deleteEntry, .deleteEntry a").unbind("click").click(deleteItemClick);
});

function showRequest(form, jObject, options)
{
	form.push({name:"async",value:"1"});
	if (editMode)
		form.push({name:"doSave",value:"1"});
	else
		form.push({name:"doAdd",value:"1"});
		
	$("#contentForm input[type=submit]").attr("disabled", true);
}

function saveFormResponse(data, status)
{
	if (!standartAJAXResponseHandler(data, status)) return;
	if (data.id > 0)
	{
		if (editMode)
		{
			$.jGrowl("Запись изменена");
			if (data.refresh)
				window.location.reload();
		}
		else
		{
		    /*
			var a = $("<a href='#'>Назад</a>");
			a.click(goBack);
			$("#contentForm").slideToggle().parent().append(a);
			$.jGrowl("Запись добавлена");
			*/
		    
		    $.jGrowl("Запись добавлена");
		    $('#contentForm').resetForm();
			if (data.refresh)
			{
				var parts = window.location.href.split("/");
				if (parts[parts.length - 1] == "" || parts[parts.length - 1].toLowerCase() == "add")
					parts.pop();
				if (parts[parts.length - 1] == "" || parts[parts.length - 1].toLowerCase() == "add")
					parts.pop();
				var url = parts.join("/") + "&rowID="+data.id;
				url = url.replace('/add/', '/edit/');
				window.location.href = url;
			}
		}
	}
	else $.jGrowl("Запись не " + editMode ? "отредактирована" : "добавлена", {header:"Возникла ошибка"});
	
	$("#contentForm input[type=submit]").attr("disabled", false);
}

function goBack(event)
{
	event.preventDefault();
	history.back(1);
}

function deleteItemClick(event)
{
	event.preventDefault();
	
	var that = $(this);
	var link = (that.is("a")) ? that.attr("href") : that.children('a').attr('href');
	$.post(link, { async: true }, itemDeleteResponse, "json");
	return false;
}

function itemDeleteResponse(data, status)
{
	if (!standartAJAXResponseHandler(data, status)) return;
	if (data.msg == "ok")
	{
		$(".deleteEntry a[href*=\"/"+data.rowID+"/\"]").parent().parent().remove();
		var list = $("#entriesList");
		$.jGrowl("Запись удалена");
		if ($("#entriesList tbody").length == 0) list.replaceWith( "Таблица пуста" );
	}
	else if(data.dependentEntries)
	{
		$('#dialog').jqm({modal: false});
		var dependentItemsContainer = $("#dialog div").eq(1).empty();
		var link = 0;
		for (var i = 0; i < data.dependentEntries.length; i++)
		{
			dependentItemsContainer.append($("<h2>"+data.dependentEntries[i].tableName+"</h2>"));
			for (var l = 0; l < data.dependentEntries[i].itemsLinks.length; l++)
			{
				link = data.dependentEntries[i].itemsLinks[l];
				dependentItemsContainer.append($("<div>- <a href='"+link+"'>"+link+"</a></div>"));
			}
		}
		$('#dialog').jqmShow();
	}
}
<?php

/**
 * Модель ВсеРедактора
 *
 * @version 2.1
 */
class AnyEditor extends Singleton
{
    /**
     * Список таблиц, которые редактируются данным модулем.
     * Данный список формируется один раз и сохраняется.
     * Он используется для формирования меню по выбору нужной таблицы.
     *
     * @var array
     */
    protected static $editingObjects = null;

    /**
     * Общее число записей, удовлетворяющих поисковому запросу в методе GetTableItems()
     *
     * @var int
     */
    public $entireItemsNumber = 0;

    /**
     * Общее число страниц, на которые разбиваются все записи таблицы
     *
     * @var int
     */
    public $totalPagesNumber = 0;

    /**
     * Число отображаемых записей на одной странице
     *
     * @var int
     */
    public $itemsOnPage = 0;

    /**
     * Возвращает список таблиц с ссылками для редактирования их записей
     *
     * @param string $anyEditorControllerAlias
     * @return array
     */
    public static function GetEditingObjects($anyEditorControllerAlias = 'ae')
    {
        // Если метод вызывается в первый раз (массив с объектами пуст),
        // то заполняется массив с объектами
        if (is_null(self::$editingObjects))
        {
            if (empty(AnyEditorSettings::$EditableTables))
                return null;

            foreach (AnyEditorSettings::$EditableTables as $alias => $item)
            {
                self::$editingObjects[] = array(
                    'name'  => $item['ruName'],
                    'link'	 => AppSettings::$ADMIN_ALIAS . $anyEditorControllerAlias . '/items/?table=' . $alias,
					'alias'	 => $item['tableName']
				);
			}
        }

        return self::$editingObjects;
    }

    /**
     * Enter description here...
     *
     * @return AnyEditor
     */
    public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }

    /**
     * Возврашает записи таблицы.
     *
     * @param string $tableName Название таблицы
     * @param int $pageNumber Номер страницы (при пагинации)
     * @param string $sortFiled Название поля для сортировки
     * @param string $sortOrder Направление сортировки
     * @param array $filters Фильтры
     * @return array
     */
    public function GetTableItems($tableName, $pageNumber = 1, $sortFiled = null, $sortOrder = null, $filters = null)
    {
            // Создаем структуру таблицы
	    $tableStructure = DBTableStructure::getInstance($tableName);

            //trace($tableStructure);

	    // Получаем настройки по данной таблицы из файла настроек
	    $tableSettings = AnyEditorSettings::$EditableTables[strtolower($tableName)];
            $PrimaryKey = $tableStructure -> PrimaryKey -> Name;

            //trace($tableSettings);

	    // Если список полей не пуст
	    if (!is_null($tableSettings['listFields']))
	    {
			// Если установлено поле linkTable и linkField
			$str_lt = '';
			$listFields = array();
			foreach ($tableSettings['listFields'] as $key => $value){
                            if (isset($value['linkTable']) and isset($value['linkField'])){
                                if (isset($value['MultipleSelectTable'])){
                                    $keys = '(SELECT GROUP_CONCAT(lt.'.$value['linkField'].')
                                              FROM '.$value['linkTable'].' lt
                                              INNER JOIN '.$value['MultipleSelectTable'].' mst ON lt.`'.$value['MultipleSelectField'].'` = mst.`'.$value['MultipleSelectField'].'`
                                              WHERE mst.`'.$PrimaryKey.'` = t.`'.$PrimaryKey.'`) as '.$key;
                                }else{
                                    $keys = $value['linkTable'].'.`'.$value['linkField'].'` as '.$key;
                                    $str_lt .= ' INNER JOIN '.$value['linkTable'].' ON t.`'.$key.'` = '.$value['linkTable'].'.`'.$key.'` ';
                                }

                                $listFields[$keys] = $value;
                            }else
                                $listFields[$key] = $value;
			}
			$tableSettings['listFields'] = $listFields;

			//trace($str_lt);
			//trace($tableSettings['listFields']);

                        // и он записан в упрощенном виде (обычный массив с названиями полей),
                        // то проводим его к нужному виду (ассоциативный массив, где ключ - название поля)
                        $keys = array_keys($tableSettings['listFields']);

                        if (is_array($tableSettings['listFields'][$keys[0]]))
                            $tableSettings['listFields'] = array_keys($tableSettings['listFields']);

                        $cnt = count($tableSettings['listFields']);
                        for ($i = 0; $i < $cnt; $i++)
                            if (!strpos($tableSettings['listFields'][$i], '.')) $tableSettings['listFields'][$i] = 't.`'. trim($tableSettings['listFields'][$i], '`') .'`';
	    }

	    // Если задан список полей, то нужно сформировать соответствующий кусок SQL-запроса
	    if (!empty($tableSettings['listFields']))
	    {
	        // Добавляем первичный ключ в список, если его там нет
	        if (!in_array($tableStructure -> PrimaryKey -> Name, $tableSettings['listFields']))
                $tableSettings['listFields'][] = 't.`'. $tableStructure -> PrimaryKey -> Name .'`';

            // Часть SQL-запроса, определяющая получаемые поля
	        $fields = implode(', ', $tableSettings['listFields']);
	    }
	    else
	        $fields = ' t.* ';

        // Depricated! Подгружаем фильтры из файла настроек (но она нафиг не нужны там)
	    $filter = empty($tableSettings['filter']) ? '' : ' WHERE '. $tableSettings['filter'];

        // Добавим внешние поля
        $referentTables = array();
		$referentCache = array();
		$listingKeys = array();
		if (!empty($tableSettings['referentTables']))
		{
		    $refTabCnt = count($tableSettings['referentTables']);
		    for ($i = 0; $i < $refTabCnt; $i++)
		    {
                $referentTables[$tableSettings['referentTables'][$i]['fieldName']] = DBTableManager::getInstance($tableSettings['referentTables'][$i]['tableName']);
                $listingKeys[$tableSettings['referentTables'][$i]['fieldName']] = $tableSettings['referentTables'][$i]['listingKey'];
		    }
		}

	    // Если заданы фильтры, формируем по ним соответствующая часть SQL-запроса
	    $joins = '';
	    $joinsArray = array();
	    if (!empty($filters))
	    {
	        $condArray = array();
	        foreach ($filters -> rules as $rule)
	        {
	            $prefix = '';
	            $field = $rule -> field;
	            // Определяем, является ли данное поле внешним ключом
	            if (!empty($referentTables) && isset($referentTables[$rule -> field]) && isset($listingKeys[$rule -> field]))
                {
                    $referentTableName = $referentTables[$rule -> field] -> GetTableName();
                	$joinsArray[$referentTableName] = 'JOIN `'. $referentTableName .'` '. $referentTableName .' ON t.`'. $rule -> field .'` = '. $referentTableName .'.`'. $referentTables[$rule -> field] -> TableStructure -> PrimaryKey -> Name .'`';
                	$field = $listingKeys[$rule -> field];
                	$prefix = $referentTableName .'.';
                }

                switch ($rule -> op)
                {
                    case 'eq':
                        $condArray[] = $prefix .'`'. $field .'` = '. DB::Quote($rule -> data);
                        break;
                    case 'ne':
                        $condArray[] = $prefix .'`'. $field .'` <> '. DB::Quote($rule -> data);
                        break;
                    case 'bw':
                        $condArray[] = $prefix .'`'. $field .'` LIKE '. DB::Quote($rule->data.'%');
                        break;
                    case 'cn':
                        $condArray[] = $prefix .'`'. $field .'` LIKE '. DB::Quote('%'.$rule->data.'%');
                        break;
                }
            }
            $condition = implode(' '. $filters -> groupOp .' ', $condArray);
            if (empty($filter))
                $filter = ' WHERE '. $condition;
            else
                $filter .= ' AND '. $condition;
	    }

	    // Сортировка
            $order = ' '. (!empty($sortOrder) ? $sortOrder : 'desc');
	    if (!empty($sortFiled))
	    {
	        if (!empty($referentTables) && isset($referentTables[$sortFiled]) && isset($listingKeys[$sortFiled]))
	        {
                $referentTableName = $referentTables[$sortFiled] -> GetTableName();
                if (!isset($joinsArray[$referentTableName]))
                    $joinsArray[$referentTableName] = 'JOIN `'. $referentTableName .'` '. $referentTableName .' ON t.`'. $sortFiled .'` = '. $referentTableName .'.`'. $referentTables[$sortFiled] -> TableStructure -> PrimaryKey -> Name .'`';

                $orderBy = ' ORDER BY '. $referentTableName .'.`'. $listingKeys[$sortFiled] .'`';
	        }
	        else
                $orderBy = ' ORDER BY t.`'. $sortFiled .'`';
                $orderBy .= $order;
	    }
	    else
                if(isset($tableSettings['orderBy']) && !empty($tableSettings['orderBy'])){
                    $orderBy = ' ORDER BY '.$tableSettings['orderBy'];
                }else{
                    $orderBy = ' ORDER BY t.`'. $tableStructure -> PrimaryKey -> Name .'`';
                    $orderBy .= $order;
                };

	    if (!empty($joinsArray))
            $joins = ' '. implode(' ', $joinsArray);

	    $limit     = empty($tableSettings['limitOnPage']) ? '' : ' LIMIT '. ($pageNumber-1)*$tableSettings['limitOnPage'] .', '. $tableSettings['limitOnPage'];
	    $query = '
	       SELECT  SQL_CALC_FOUND_ROWS
	               '.$fields.'
	       FROM    `'. $tableName .'` t'.
	       $joins.
               $str_lt.
               $filter.
               $orderBy.
               $limit;

            //trace($query);

	    $items = DB::QueryToArray($query, $tableStructure -> PrimaryKey -> Name);
	    $itemsCnt = count($items);

	    $this -> entireItemsNumber = DB::QueryOneValue('SELECT FOUND_ROWS()');
	    $this -> totalPagesNumber = ceil($this -> entireItemsNumber / $tableSettings['limitOnPage']);
	    if (!empty($tableSettings['limitOnPage']))
            $this -> itemsOnPage = $tableSettings['limitOnPage'];


		// Массив $referentTables определяется выше
        if (!empty($referentTables))
        {
        	//$entries = array();
        	foreach ($items as $itemKey => $itemValue)
        	{
                foreach ($items[$itemKey] as $fieldName => $fieldValue)
        	    {
        	        if (isset($referentTables[$fieldName]))
        	        {
        	            if (isset($referentCache[$fieldName][$fieldValue]))
        	            {
        	                $items[$itemKey][$fieldName] = $referentCache[$fieldName][$fieldValue];
        	            }
        	            else
        	            {
                            $result = $referentTables[$fieldName] -> Select(array($referentTables[$fieldName] -> TableStructure -> PrimaryKey -> Name => $fieldValue));
                            if (!empty($result))
        						$items[$itemKey][$fieldName] = $result[0][$listingKeys[$fieldName]];
        	            }
        	        }
        	    }

        	    //$entry['values'] = $list[$i];
        	    //$entries[] = $entry;
        	}
        }

	    return $items;
    }

    /**
     * Возвращает массив заголовков таблицы
     *
     * @param string $tableName
     * @param array $itemsRow
     * @return array
     */
    public function GetTableHeaders($tableName, $itemsRow = null)
    {
        // Создаем структуру таблицы
	    $tableStructure = DBTableStructure::getInstance($tableName);

	    // Получам информацию из реестра полей
        $fieldsRegistryTableManager = DBTableManager::getInstance(TablesNames::$FIELDS_REGISRTY_TABLE_NAME);
        $registeredFields = $fieldsRegistryTableManager -> Select(array('tableName' => $tableName));
        $rfcnt = count($registeredFields);

        // Настройки таблицы из файла настроек
        $tableSettings = AnyEditorSettings::$EditableTables[strtolower($tableName)];

        // Если не задан $itemsRow, сгенерим его сами
        if (empty($itemsRow))
        {
            $itemsRow = DB::QueryToArray('SHOW COLUMNS FROM '. $tableName, 'Field');
        }

        $headers = array();
		foreach ($itemsRow as $key => $value)
		{
		    $header = array();
		    if ($key == $tableStructure -> PrimaryKey -> Name)
		    {
		        // Для первичного ключа добавляем настройки по умолчанию (если их нет)
                $header = array('name' => $key, 'caption' => '#', 'width' => 40, 'align' => 'right');
                if (!empty($tableSettings['listFields'][$key]))
                    $header = array_merge($header, $tableSettings['listFields'][$key]);
		    }
		    else
		    {
    		    //$header = $key;
    		    for ($i = 0; $i < $rfcnt; $i++)
    		    {
    		        // Фильтруем мусор (те записи, которые не являются полями)
    		        if ($registeredFields[$i]['fieldName'] == $key)
    		        {
                        $header = array('name' => $registeredFields[$i]['fieldName'], 'caption' => $registeredFields[$i]['description']);
                        if (!empty($tableSettings['listFields'][$key]))
                            $header = array_merge($header, $tableSettings['listFields'][$key]);
    		        }
                        //$header = $registeredFields[$i];
                        //$header = $registeredFields[$i]['description'];
    		    }
		    }
		    if (!empty($header))
                $headers[] = $header;
		}

		return $headers;
    }
}
?>
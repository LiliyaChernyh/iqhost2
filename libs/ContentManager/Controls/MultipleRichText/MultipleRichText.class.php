<?php
/**
 * MultipleRichText
 * Контрол для вставки произвольного числа FCKEditor в форму.
 * Требуется сам fckeditor и jQuery обёртка для него
 *
 * @version 1.2
 *
 */
class MultipleRichText extends Control implements IFlushingControl
{
    /**
     * Ширина контрола
     *
     * @var string
     */
    private $width = '100%';

    /**
     * Высота контрола в пикселях
     *
     * @var int
     */
    private $height = '100px';

    /**
     * Название тулбара FCK
     *
     * @var string
     */
    private $toolbarSet = 'Basic';

    private $referentTableName = null;

    public function InitControl($value = null, $rowID = null)
    {
        $this -> controlTplPath = parent::$CONTROLS_PATH .__CLASS__.'/control.tpl';

        if ($value === null)
            $value = '';
        else
        {
            $value = str_replace("\r\n", "", $value);
            $value = str_replace("\n", "", $value);
            $value = str_replace("'", "\'", $value);
            $value = str_ireplace("</script>", "</scr' + 'ipt>", $value);
        }

        if (!empty($this -> controlSettings['referentTable']))
			$this -> referentTableName = $this -> controlSettings['referentTable'];
		else
			throw new Exception('Referent Table for '. $this -> name .' is undefined. It should be defined in FieldsRegistry');

        if (!empty($this -> controlSettings['width']))
            $this -> width = $this -> controlSettings['width'];

        if (!empty($this -> controlSettings['height']))
            $this -> heigth = $this -> controlSettings['height'];

        if (!empty($this -> controlSettings['toolbarSet']))
            $this -> toolbarSet = $this -> controlSettings['toolbarSet'];

        $this -> controlTplVars['NAME'] = $this -> name;
        $this -> controlTplVars['VALUE'] = $value;
        $this -> controlTplVars['TOOLBARSET'] = $this -> toolbarSet;
        $this -> controlTplVars['WIDTH'] = $this -> width;
        $this -> controlTplVars['HEIGHT'] = $this -> height;
        $this -> controlTplVars['TIP'] = $this -> tip;

        // Используемые JS-библиотеки
        $this -> controlJSLibs = array();
        $this -> controlJSLibs[] = 'FCK';

        // Инициализирующая функция
        $this -> controlJSFiles = array();
        $this -> controlJSFiles[] = parent::$CONTROLS_ALIAS.__CLASS__.'/init.js';

        // Пременные, которые передаются инициализирующей JS функции
        $this -> controlJSVars['name'] = $this -> name;
        $this -> controlJSVars['toolbar'] = $this -> toolbarSet;
        $this -> controlJSVars['width'] = $this -> width;
        $this -> controlJSVars['height'] = $this -> height;

        if (!empty($rowID))
        {
            // Загружаем уже имеющиеся данные
            $referentTableManager = new DBTableManager($this -> controlSettings['referentTable']);
            $rows = $referentTableManager -> Select(array(
                $this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID
            ));

            if (!empty($rows))
            {
                $rowsCnt = count($rows);
                $rowsToTpl = array();
                for ($i = 0; $i < $rowsCnt; $i++)
                {
                    $rowsToTpl[] = array(
                        'rowID' => $rows[$i][$referentTableManager -> TableStructure -> PrimaryKey -> Name],
                        'value' => $rows[$i][$this -> controlSettings['textFieldName']]
                    );
                }
                $this -> controlTplVars['ROWS'] = $rowsToTpl;
            }
        }

        $this -> type = __CLASS__;
    }

    public function Flush(&$data, $rowID)
    {
        // Создаем менеджер таблица, в которой храним значения
        $referentTableManager = new DBTableManager($this -> controlSettings['referentTable']);

        // Удаляем отмеченные элементы
        if (!empty($data['delete']))
		{
		    $cnt = count($data['delete']);
		    for ($i = 0; $i < $cnt; $i++)
		    {
		        $referentTableManager -> Delete(array($referentTableManager -> TableStructure -> PrimaryKey -> Name => $data['delete'][$i]));

		        // Убираем эту запись из массива для редактирования, если она там есть
		        if (isset($data['edit'][$data['delete'][$i]]))
                    unset($data['edit'][$data['delete'][$i]]);
		    }
		}

		// Редактируем
		if (!empty($data['edit']))
		{
		    foreach ($data['edit'] as $ID => $value)
		    {
		        $referentTableManager -> Update(array($this -> controlSettings['textFieldName']   => $value), $ID);
		    }
		}

		// Добавляем новые
		if (!empty($data['add']))
		{
		    $cnt = count($data['add']);
		    for ($i = 0; $i < $cnt; $i++)
		    {
		        if (empty($data['add'][$i]))
                    continue;

		        $referentTableManager -> Insert(array(
                    $this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID,
                    $this -> controlSettings['textFieldName']                               => $data['add'][$i]
		        ));
		    }
		}
    }

    public function PostBackHandler(&$data, $rowID = null){}
}
?>
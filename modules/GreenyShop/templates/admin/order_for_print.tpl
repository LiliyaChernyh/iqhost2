<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-language" content="ru" />
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />			

<title>Система управления заказами McCoy</title>

<style>
body {
	font-family: Verdana, Tahoma, Arial;
	font-size: 8pt;
	margin: 0;
	padding: 0;
}
table#orderInfo, table#cart, table#additionalServicesTable, table#customerInfo
{
	border: 1px solid #AAA;
	border-collapse: collapse;
}
table#orderInfo td, table#cart td,  table#additionalServicesTable td, table#customerInfo td
{
	border-bottom: 1px solid #DDD;
	border-collapse: collapse;
}
table#cart th, table#additionalServicesTable th
{
	border-bottom: 1px solid #999;
	border-collapse: collapse;
}
table#orderInfo td#tdTitle
{
	width: 250px;
	font-weight: bold;
}

table#customerInfo td#tdTitle
{
	width: 100px;
	font-weight: bold;
}
</style>
</head>
<body>
<h2>Заказ №{#var: ORDERID}</h2>

<h3>Информация о госте:</h3>
<table id="customerInfo" cellspacing="0" cellpadding="5">
	<tr>
		<td id="tdTitle">
			ФИО: 
		</td>
		<td>
			{#var: CUSTOMER_FIO}
		</td>
	</tr>
	<tr>
		<td id="tdTitle">
			Почта: 
		</td>
		<td>
			{#var: CUSTOMER_EMAIL}
		</td>
	</tr>
	<tr>
		<td id="tdTitle">
			Телефон: 
		</td>
		<td>
			{#var: CUSTOMER_PHONE}
		</td>
	</tr>
	<tr>
		<td id="tdTitle">
			Адрес: 
		</td>
		<td>
			{#var: CUSTOMER_ADDRESS}
		</td>
	</tr>	
</table>
			
<h3>Информация о заказе:</h3>
<table id="orderInfo" cellspacing="0" cellpadding="5">
	<tr>
		<td id="tdTitle">
			Дата проведения банкета:
		</td>
		<td>
			{#var: BANQUETDATE}
		</td>
	</tr>
	<tr>
		<td id="tdTitle">
			Число гостей:
		</td>
		<td>
			{#var: GUESTSNUMBER}
		</td>
	</tr>
	<tr>
		<td id="tdTitle">
			Время создания:
		</td>
		<td>
			<span class="selected">{#var: CREATIONDATE}</span>
		</td>
	</tr>
	<tr>
		<td id="tdTitle">
			Номера столов:
		</td>
		<td>
			{#var: TABLESNUMBERS}
		</td>
	</tr>
	<tr>
		<td id="tdTitle">
			Статус заказа:
		</td>
		<td>
			{#var: STATUSDESCRIPTION}
		</td>
	</tr>
	<tr>
		<td id="tdTitle">
			Задаток:
		</td>
		<td>
			{#var: PREPAYMENT}
		</td>
	</tr>
</table>

<p>
	<h3>Корзина заказа:</h3>
	<table id="cart" cellspacing="0" cellpadding="5" width="90%">
		<tr>
			<th width="20%">Наименование</th>
			{#ifset: SHOW_EXTRA}
			<th width="49%">Описание</th>
			{##ifset}
			<th width="5%">Артикул</th>
			<th width="5%">Цена</th>
			<th width="5%">Ко&shy;ли&shy;чес&shy;тво</th>
			<th width="5%">Пор&shy;ции</th>
			<th width="6%">Сум&shy;ма</th>
		</tr>
		{#block: dishes}
		{#ifset: DISHES_CATEGORY}<tr><td colspan="7" style="background-color:#DFDFDF">{#var: DISHES_CATEGORY}</td></tr>{##ifset}
		<tr class="{#var: DISHES_ODD_EVEN}">
			<td>{#var: DISHES_NAME}</td>
			{#ifset: SHOW_EXTRA}
			<td>
			{##ifset}
			{#ifset: DISHES_IMAGE}
				<img src="{#var: DISHES_IMAGE}" align="left" />
			{##ifset}
			{#var: DISHES_DESCRIPTION}
			{#ifset: SHOW_EXTRA}
			</td>
			{##ifset}
			<td>{#var: DISHES_ARTICULE}</td>
			<td>{#var: DISHES_PRICE} р.</td>
			<td>{#var: DISHES_AMOUNT}</td>
			<td>{#var: DISHES_PORTIONS}</td>
			<td>{#var: DISHES_SUMM} р.</td>
		</tr>
		{##block}
	</table>

</p>

<p>
	<h3>Дополнительные услуги</h3>
	<table id="additionalServicesTable" cellspacing="0" cellpadding="5">
		<thead>
			<tr>
				<th>Наименование услуги</th>
				<th width="13%">Цена / %</th>
			</tr>
		</thead>
		<tbody>
			{#block: additional_services}
			<tr style="border: 1px solid #EEE">
				<td>
					{#var: ADDITIONAL_SERVICE_SERVICETITLE}
				</td>
				<td>
					{#var: ADDITIONAL_SERVICE_COST}
				</td>
			</tr>
			{##block}
		</tbody>
	</table>
</p>

<div style="font-weight:bold; margin:0 20px 10px 0;clear:both">
	<div style="width:200px;float:left;margin:5px 0">Кухня: {#var: KITCHEN_TOTAL} р.</div>{#ifset: KITCHEN_TOTAL_DISCOUNT}<div style="float:left;margin:5px 0"> Со скидкой: {#var: KITCHEN_TOTAL_DISCOUNT} p.</div>{##ifset}
	<div style="width:200px;float:left;clear:left;margin:5px 0">Бар: {#var: BAR_TOTAL} р.</div>{#ifset: BAR_TOTAL_DISCOUNT}<div style="float:left;margin:5px 0"> Со скидкой: {#var: BAR_TOTAL_DISCOUNT} p.</div>{##ifset}
	<div style="width:200px;float:left;clear:left;margin:5px 0">Кухня и бар: {#var: DISHES_TOTAL} р.</div>{#ifset: DISHES_TOTAL_DISCOUNT}<div style="float:left;margin:5px 0"> Со скидкой: {#var: DISHES_TOTAL_DISCOUNT} p.</div>{##ifset}
	<div style="clear:both">&nbsp;</div>
	<div style="margin:10px 0">
		Стоимость всего заказа: {#var: TOTAL_ORDER_COST} р. <br/>
		{#ifset: TOTAL_ORDER_COST_DISCOUNT}Со скидкой на бар и кухню: {#var: TOTAL_ORDER_COST_DISCOUNT} р.{##ifset}
	</div>
</div>

</body>
</html>
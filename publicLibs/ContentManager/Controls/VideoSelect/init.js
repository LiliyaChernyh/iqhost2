function VideoSelectInit(initObject)
{
	var controlName = initObject.name;
	$(".addToLinked").data("controlName", controlName).click(AddToLinkedVideosClickHandler);
	$(".removeFromLinked").data("controlName", controlName).click(RemoveFromLinkedVideosClickHandler);
	
	$("#videosLinker_"+controlName).data("maxSelect", initObject.maxSelect);
	
	$("#loadMoreVideos")
		.data("controlName", controlName)
		.data("currentOffset", 0)
		.data("disabled", false)
		.data("currentCorrection", 0) // Коррекция будущего сдвига в случае, если часть видео из списка уже добавлена, но страница не обновлялась
		.data("serverScript", initObject.serverScript)
		.click(LoadMoreVideosClickHandler)
	
	// Подпишемся на POST BACK формы
	//$("#linkedVideosHiddenInputs")
	//	.parents("form").ajaxForm(LinkedVideosPostbackHandler);
};

function AddToLinkedVideosClickHandler(event)
{
	event.preventDefault();
	var link = $(this);
	var controlName = link.data("controlName");
	if ($("#linkedVideos").children().length == $("#videosLinker_"+controlName).data("maxSelect"))
	{
		$.jGrowl("Вы можете выбрать только одно видео");
		return;
	}
	
	var video = link.parent().parent();
	var id = video.attr("id").substring(6);
	if ($("#linkedVideosHiddenInputs").find("input[name*=__saved][value="+id +"]").length == 0)
	{
		var input = $("<input>");
		input.attr("type", "hidden");
		input.attr("name", controlName + "__added[]");
		input.val(id);
		$("#linkedVideosHiddenInputs").append(input);
	}
	$("#linkedVideosHiddenInputs").find("input[name*=__delete][value="+id +"]").remove();

	var videoClone = video.clone();
	videoClone
		.find("a.jsLink")
		.text("Убрать из связанных")
		.data("controlName", controlName)
		.click(RemoveFromLinkedVideosClickHandler);
	$("#linkedVideos").append(videoClone);
	video.remove();
}

function RemoveFromLinkedVideosClickHandler(event)
{
	event.preventDefault();
	var link = $(this);
	var video = link.parent().parent();
	var id = video.attr("id").substring(6);
	var controlName = link.data("controlName");
	
	var saved = $("#linkedVideosHiddenInputs").find("input[name*=__saved][value="+id +"]");
	if (saved.length > 0)
	{
		var input = $("<input>")
				.attr("type", "hidden")
				.attr("name", controlName + "__delete[]")
				.val(id);
		$("#linkedVideosHiddenInputs").append(input);
	}
	$("#linkedVideosHiddenInputs").find("input[name*=__added][value="+id +"]").remove();
	
	var videoClone = video.clone();
	videoClone
		.find("a.jsLink")
		.data("controlName", controlName)
		.text("Добавить в связанные")
		.click(AddToLinkedVideosClickHandler);
	$("#otherVideos").prepend(videoClone);
	video.remove();
}

function LinkedVideosPostbackHandler()
{
	var added = $("#linkedVideosHiddenInputs").find("input[name*=__added]");
	// В случае добавления видео из ранее недобавленных,
	//	необходимо сдвинуть общий сдвиг на число этих добавленных, чтобы избежать "провалов"
	// Однако в случае удаления это сделать сложнее, поэтому удалённое видео в списке может появиться дважды
	$("#loadMoreVideos").data("currentCorrection", $("#loadMoreVideos").data("currentCorrection") + added.length);
	added.each(function()
	{
		if ($(this).val() == 0) return;
		var name = $(this).attr("name");
		$(this).attr("name", name.substring(0, name.length - 9) + "__saved[]");
	});
	
	$("#linkedVideosHiddenInputs").find("input[name*=__delete]").each(function()
	{
		if ($(this).val() == 0) return;
		var name = $(this).attr("name");
		$("#linkedVideosHiddenInputs").find("input[name*=__saved][value="+$(this).val()+"]").remove();
		$(this).remove();
	});
}

function LoadMoreVideosClickHandler(event)
{
	event.preventDefault();
	var that = $(this);
	if (that.data("disabled")) return;
	$.post(that.data("serverScript"), 
			{
			"currentOffset": that.data("currentOffset"),
			"offsetCorrection": that.data("currentCorrection"),
			"videoID": $("#videoID").val()}, 
			LoadMoreVideosResponseHandler, 
			"json");
	that.data("disabled", true).text("Пожалуйста, подождите");
}

function LoadMoreVideosResponseHandler(data, status)
{
	var loadVideos = $("#loadMoreVideos")
						.data("disabled", false)
						.text("Ещё! Ещё!");
	if (!standartAJAXResponseHandler(data, status)) return;
	if (data.videos.length == 0)
	{
		$.jGrowl("Больше нет");
		loadVideos.slideToggle();
	}
	
	loadVideos.data("currentOffset", data.newOffset);
	var controlName = loadVideos.data("controlName");
	
	var container = $("#otherVideos");
	for (var i = 0; i < data.videos.length; i++)
	{
		var video = $("<div>")
						.addClass("videoInList")
						.attr("id", "video_" + data.videos[i]["dataID"]);
		video.append(
			$("<div>")
				.addClass("videoThumbnail")
				.append(
					$("<a>")
						.attr("href", ADMIN_ALIAS + "anyeditor/videos/edit/" + data.videos[i]["videoID"] + "/")
						.attr("target", "_blank")
						.append(
							$("<img>")
								.attr("src", data.videos[i]["srcSmall"] != null ? data.videos[i]["srcSmall"] : "/images/admin/no-image.png")
								)
						)
					)
			 .append(
				$("<div>")
					.addClass("videoTitle")
					.text(data.videos[i]["title"])
					)
			 .append(
				$("<div>")
					.addClass("videoLinkageControls")
					.append(
						$("<a href='#'>")
							.addClass("addToLinked")
							.addClass("jsLink")
							.text("Добавить в связанные")
							.click(AddToLinkedVideosClickHandler)
							.data("controlName", controlName)
							)
					);
		container.append(video);
	}
}
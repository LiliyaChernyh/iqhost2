<?php
/**
 * Контрол для отображения обычного TextArea
 *
 * @version 3.2
 *
 */
class TextArea extends Control
{
    /**
     * Число строк
     *
     * @var int
     */
	private $rows = 7;

	/**
	 * Ширина поля в символах
	 *
	 * @var int
	 */
	private $cols = 60;

	public function InitControl($value = null, $rowID = null)
	{
		$this -> LoadSettings();

		$value = ($value === null) ? '' : htmlspecialchars($value);

		$this -> controlTplPath = parent::$CONTROLS_PATH. __CLASS__.'/control.tpl';

		$this -> controlTplVars = array();
		$this -> controlTplVars['NAME']  = $this -> name;
		$this -> controlTplVars['ROWS']  = $this -> rows;
		$this -> controlTplVars['COLS']  = $this -> cols;
		$this -> controlTplVars['CLASS'] = $this -> CSSClass;
		$this -> controlTplVars['TIP']   = $this -> tip;
		$this -> controlTplVars['VALUE'] = $value;

		if ($this -> disabled) $this -> controlTplVars['READONLY'] = 1;
		if ($this -> required) $this -> controlTplVars['REQUIRED'] = 1;

		$this -> type = __CLASS__;

		$this -> controlTplVars['tableName'] = $this->tableName;
		$this -> controlTplVars['formIndex'] = (isset($this->formIndex))?$this->formIndex:0;

	}

	private function LoadSettings()
	{
		if (!empty($this -> controlSettings['cols']) && is_numeric($this -> controlSettings['cols']))
			$this -> cols = $this -> controlSettings['cols'];

		if (!empty($this -> controlSettings['rows']) && is_numeric($this -> controlSettings['rows']))
			$this -> rows = $this -> controlSettings['rows'];
	}

	public function PostBackHandler(&$data, $rowID = null) {}
}

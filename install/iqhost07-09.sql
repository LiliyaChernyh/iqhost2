-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.53 - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных iqhost
CREATE DATABASE IF NOT EXISTS `iqhost` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `iqhost`;

-- Дамп структуры для таблица iqhost.Actions
CREATE TABLE IF NOT EXISTS `Actions` (
  `actionID` int(11) NOT NULL AUTO_INCREMENT,
  `summary` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `imageID` int(11) DEFAULT NULL,
  `publicDate` timestamp NULL DEFAULT NULL,
  `modifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`actionID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.Actions: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `Actions` DISABLE KEYS */;
INSERT INTO `Actions` (`actionID`, `summary`, `text`, `imageID`, `publicDate`, `modifiedDate`) VALUES
	(1, 'текст', '<p>текст</p>', 5, '2017-09-07 00:00:00', '2017-09-07 09:03:18');
/*!40000 ALTER TABLE `Actions` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.Advantages
CREATE TABLE IF NOT EXISTS `Advantages` (
  `advantageID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `priority` int(11) DEFAULT '0',
  `isActive` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`advantageID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.Advantages: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `Advantages` DISABLE KEYS */;
INSERT INTO `Advantages` (`advantageID`, `title`, `text`, `priority`, `isActive`) VALUES
	(1, 'Огромный опыт работы более 10 лет', 'Наши специалисты могут решить любую задачу, мы знаем о хостинге сайтов почти все!', 0, 1),
	(2, 'Удобная панель управления ISP Manager', 'Новая 5я версия панели управления хостингом автоматизирует большинство задач и подойдет как новичку, так и самому опытному пользователю ', 0, 1);
/*!40000 ALTER TABLE `Advantages` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.greeny_fieldsRegistry
CREATE TABLE IF NOT EXISTS `greeny_fieldsRegistry` (
  `entryID` int(11) NOT NULL AUTO_INCREMENT,
  `tableName` varchar(128) NOT NULL,
  `fieldName` varchar(128) NOT NULL,
  `canChange` tinyint(1) NOT NULL DEFAULT '1',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `controlType` varchar(63) NOT NULL DEFAULT 'LineInput',
  `description` varchar(255) DEFAULT NULL,
  `isListingKey` tinyint(1) NOT NULL DEFAULT '0',
  `isMultiLanguage` tinyint(1) NOT NULL DEFAULT '0',
  `tip` varchar(255) DEFAULT NULL,
  `controlSettings` text,
  PRIMARY KEY (`entryID`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.greeny_fieldsRegistry: ~56 rows (приблизительно)
/*!40000 ALTER TABLE `greeny_fieldsRegistry` DISABLE KEYS */;
INSERT INTO `greeny_fieldsRegistry` (`entryID`, `tableName`, `fieldName`, `canChange`, `visible`, `controlType`, `description`, `isListingKey`, `isMultiLanguage`, `tip`, `controlSettings`) VALUES
	(1, 'Users', 'userID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
	(2, 'Users', 'login', 1, 1, 'LineInput', 'Логин', 1, 0, 'Введите логин', '{ "blOrder" : 0, "blClass" : " ", "blName" : "Основые данные пользователя", "elemOrder" : 10, "size" : 60, "class" : " " }'),
	(3, 'Users', 'FIO', 1, 1, 'LineInput', 'ФИО', 0, 0, '', '{ "blOrder" : 0, "blClass" : " ", "elemOrder" : 20, "size" : 60, "class" : " " }'),
	(4, 'Users', 'email', 1, 1, 'LineInput', 'E-mail', 0, 0, NULL, '{\r\n"blOrder" : 0,\r\n"blName" : "",\r\n"elemOrder" : 15,\r\n"size" : 60\r\n}'),
	(5, 'Users', 'role', 1, 1, 'SingleSelect', 'Тип пользователя', 0, 0, '', '{ "enum" : false, "referentTable" : "Roles", "listingKey":"name", "blOrder" : 0, "blClass" : " ", "elemOrder" : 30, "emptyValue" : 0, "emptyText" : " - " }'),
	(6, 'Users', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{"blOrder" : 0, "blClass" : " ", "elemOrder" : 40, "isCheckedByDef": 0} '),
	(7, 'Users', 'registrationDate', 1, 1, 'DateMaskedInput', 'Дата регистрации', 0, 0, '', '{ "blOrder" : 0, "blClass" : "", "elemOrder" : 50, "size" : 20, "insertCurrentDateIfEmpty" : true, "ignoreUserInput" : false, "mask" : "99.99.9999 99:99:99", "dateFormat" : "d.m.Y H:i:s" }'),
	(8, 'UsersInfo', 'birthdate', 1, 1, 'DateMaskedInput', 'Дата рождения', 0, 0, '', '{\r\n	"blOrder" : 0, \r\n	"blClass" : "", \r\n	"elemOrder" : 25,\r\n        "size" : 15, \r\n	"ifNull" : "null", \r\n	"ifNotNull" : "null",\r\n	"mask" : "99.99.9999",\r\n	"dateFormat" : "d.m.Y",\r\n	"minDate" : "01.01.1900",\r\n	"maxDate" : "01.01.2009"\r\n}'),
	(9, 'UsersInfo', 'userID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
	(10, 'UsersInfo', 'FIO', 1, 1, 'LineInput', 'ФИО', 1, 0, NULL, '{ "blOrder" : 0, "blName" : "Дополнительные данные пользователя", "blClass" : " ", "elemOrder" : 10, "size" : 60, "class" : " " }'),
	(11, 'UsersInfo', 'gender', 1, 1, 'SingleSelect', 'Пол', 0, 0, NULL, '{ "enum" : "true", "blOrder" : 0, "blClass" : " ", "elemOrder" : 27 }'),
	(12, 'Roles', 'role', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
	(13, 'Roles', 'name', 1, 1, 'LineInput', 'Тип пользователя', 1, 0, '', '{ "blOrder" : 0, "blClass" : " ", "elemOrder" : 10, "size" : 60, "class" : " " }'),
	(14, 'Roles', 'level', 1, 1, 'LineInput', 'Уровень доступа', 0, 0, '', '{ "blOrder" : 0, "blClass" : " ", "elemOrder" : 20, "size" : 60, "class" : " " }'),
	(15, 'simplePages', 'pageContent', 1, 1, 'RichText', 'Содержание', 0, 0, 'Содержание страницы', '{\r\n  "blOrder" : 0,\r\n  "blName" : "",\r\n  "elemOrder" : 10\r\n}'),
	(16, 'simplePages', 'simplePageID', 0, 0, 'HiddenInput', NULL, 0, 0, NULL, NULL),
	(17, 'greeny_settings', 'groupID', 1, 1, 'SingleSelect', 'Группа настроек', 0, 0, '', '{ "referentTable" : "greeny_settingsGroups", "listingKey" : "desc", "blOrder" : 0, "blClass" : " ", "elemOrder" : 10 }'),
	(18, 'greeny_settings', 'name', 1, 1, 'LineInput', 'Название', 0, 0, '', '{ "blOrder" : 0, "blClass" : " ", "elemOrder" : 20, "size" : 60, "class" : " " }'),
	(19, 'greeny_settings', 'desc', 1, 1, 'LineInput', 'Описание', 1, 0, '', '{ "blOrder" : 0, "blClass" : " ", "elemOrder" : 30, "size" : 60, "class" : " " }'),
	(20, 'greeny_settings', 'value', 1, 1, 'LineInput', 'Значение', 0, 0, '', '{ "blOrder" : 0, "blClass" : " ", "elemOrder" : 40, "size" : 60, "class" : " " }'),
	(21, 'greeny_settingsGroups', 'groupID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
	(22, 'greeny_settingsGroups', 'parentID', 1, 1, 'SingleSelect', 'Родительская группа', 0, 0, '', '{ "referentTable" : "greeny_settingsGroups", "blOrder" : 0, "blClass" : " ", "elemOrder" : 10 }'),
	(23, 'greeny_settingsGroups', 'name', 1, 1, 'LineInput', 'Назване группы', 0, 0, '', '{ "blOrder" : 0, "blClass" : " ", "elemOrder" : 20, "size" : 60, "class" : " " }'),
	(24, 'greeny_settingsGroups', 'desc', 1, 1, 'LineInput', 'Описание группы', 1, 0, '', '{ "blOrder" : 0, "blClass" : " ", "elemOrder" : 30, "size" : 60, "class" : " " }'),
	(25, 'greeny_settings', 'settingID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
	(98, 'MenuList', 'menuListID', 1, 1, 'HiddenInput', '', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0 }'),
	(99, 'MenuList', 'menuListName', 1, 1, 'LineInput', 'Название', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0, "size" : 60, "className" : " " }'),
	(100, 'MenuList', 'menuListAlias', 1, 1, 'LineInput', 'Алиас', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0, "size" : 60, "className" : " " }'),
	(101, 'MenuList', 'menuListComment', 1, 1, 'TextArea', 'Комментарий', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0, "cols" : 60, "rows" : 5, "className" : " " }'),
	(102, 'Menu', 'itemID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
	(103, 'Menu', 'parentID', 1, 1, 'SingleSelectCustom', 'Родительский пункт', 0, 0, '', '{ "referentTable" : "Menu" , "listingKey" : "name", "blOrder" : 0, "blClass" : " ", "elemOrder" : 10, "emptyText" : " - " , "parentKey":"parentID" }'),
	(104, 'Menu', 'name', 1, 1, 'LineInput', 'Название', 0, 0, '', '{ "blOrder" : 0, "blClass" : " ", "elemOrder" : 10, "size" : 60, "class" : " " }'),
	(105, 'Menu', 'alias', 1, 1, 'LineInput', 'Алиас', 1, 0, '', '{ "blOrder" : 0, "blClass" : " ", "elemOrder" : 20, "size" : 60, "class" : " " }'),
	(106, 'Menu', 'priority', 1, 1, 'LineInput', 'Порядок ', 0, 0, '', '{ "blOrder" : 0, "elemOrder" :30, "size" : 60, "className" : " " }'),
	(107, 'Menu', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{ "checkedDefault" : true, "blOrder" : 0, "elemOrder" :40, "className" : " " }'),
	(108, 'Menu', 'menuListID', 0, 0, 'HiddenInput', '', 0, 0, '', NULL),
	(109, 'Menu', 'imageID', 1, 1, 'DamnUpload', 'Изображение (только для меню услуг 40x40)', 0, 0, '', '{\r\n	"blOrder"	:	0,\r\n	"elemOrder"	:	0,\r\n	"image"	:	{\r\n		"resize"	:	{\r\n			"strong"	:	false,\r\n			"width"	:	1000,\r\n			"height"	:	1000\r\n		},\r\n		"thumb"	:	{\r\n			"resize"	:	{\r\n				"strong"	:	false,\r\n				"width"	:	150,\r\n				"height"	:	150\r\n			},\r\n			"postfix"	:	"_small"\r\n		}\r\n	},\r\n	"includeThumbCropDialog"	:	false,\r\n	"maxFileSize"	:	5048576,\r\n	"filesType"	:	"image",\r\n	"maxFilesNumber"	:	1,\r\n	"linkingTable"	:	"",\r\n	"allowedExtensions"	:	"*.jpg; *.jpeg; *.gif; *.png",\r\n	"filesTable"	:	"greeny_images",\r\n	"filesTableKey"	:	"imageID",\r\n	"destinationDirectory"	:	"uploaded/menu/",\r\n	"uploadScript"	:	"/admin/upload/"\r\n}'),
	(110, 'Advantages', 'advantageID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
	(111, 'Advantages', 'title', 1, 1, 'LineInput', 'Название', 0, 0, NULL, '{ "blOrder" : 0, "elemOrder" : 20, "size" : 60 }'),
	(112, 'Advantages', 'text', 1, 1, 'TextArea', 'Текст', 0, 0, NULL, '{ "blOrder" : 0, "elemOrder" : 30, "cols" : 60, "rows" : 5, "className" : " " }'),
	(113, 'Advantages', 'priority', 1, 1, 'LineInput', 'Порядок ', 0, 0, '', '{ "blOrder" : 0, "elemOrder" :50, "size" : 60, "className" : " " }'),
	(114, 'Advantages', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{ "checkedDefault" : true, "blOrder" : 0, "elemOrder" :60, "className" : " " }'),
	(115, 'News', 'newsID', 1, 1, 'HiddenInput', '', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0 }'),
	(116, 'News', 'summary', 1, 1, 'TextArea', 'Краткое описание', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0, "cols" : 60, "rows" : 5, "className" : " " }'),
	(117, 'News', 'text', 1, 1, 'RichText', 'Текст', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0, "width" : "100%", "height" : 500, "toolbarSet" : "" }'),
	(118, 'News', 'imageID', 1, 1, 'DamnUpload', 'Изображение 540x405', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0, "image" : { "resize" : { "strong" : false, "width" : 1000, "height" : 1000 }, "thumb" : { "resize" : { "strong" : false, "width" : 150, "height" : 150 }, "postfix" : "_small" } }, "includeThumbCropDialog" : false, "maxFileSize" : 5048576, "filesType" : "image", "maxFilesNumber" : 1, "linkingTable" : "", "allowedExtensions" : "*.jpg; *.jpeg; *.gif; *.png", "filesTable" : "greeny_images", "filesTableKey" : "imageID", "destinationDirectory" : "uploaded/news/[ID]/", "uploadScript" : "/admin/upload/" }'),
	(119, 'News', 'publicDate', 1, 1, 'DateInput', 'Дата публикации', 0, 0, '', '{ "dateFormat" : "d.m.Y", "yearRange" : "-10:+5", "insertCurrentDateIfEmpty" : false, "ignoreUserInput" : false, "blOrder" : 0, "blName" : " ", "elemOrder" : 0 }'),
	(120, 'Actions', 'actionID', 1, 1, 'HiddenInput', '', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0 }'),
	(121, 'Actions', 'summary', 1, 1, 'TextArea', 'Краткое описание', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0, "cols" : 60, "rows" : 5, "className" : " " }'),
	(122, 'Actions', 'text', 1, 1, 'RichText', 'Текст', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0, "width" : "100%", "height" : 500, "toolbarSet" : "" }'),
	(123, 'Actions', 'imageID', 1, 1, 'DamnUpload', 'Изображение 540x405', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0, "image" : { "resize" : { "strong" : false, "width" : 1000, "height" : 1000 }, "thumb" : { "resize" : { "strong" : false, "width" : 150, "height" : 150 }, "postfix" : "_small" } }, "includeThumbCropDialog" : false, "maxFileSize" : 5048576, "filesType" : "image", "maxFilesNumber" : 1, "linkingTable" : "", "allowedExtensions" : "*.jpg; *.jpeg; *.gif; *.png", "filesTable" : "greeny_images", "filesTableKey" : "imageID", "destinationDirectory" : "uploaded/actions/[ID]/", "uploadScript" : "/admin/upload/" }'),
	(124, 'Actions', 'publicDate', 1, 1, 'DateInput', 'Дата публикации', 0, 0, '', '{ "dateFormat" : "d.m.Y", "yearRange" : "-10:+5", "insertCurrentDateIfEmpty" : false, "ignoreUserInput" : false, "blOrder" : 0, "blName" : " ", "elemOrder" : 0 }'),
	(125, 'Solutions', 'solutionID', 1, 1, 'HiddenInput', '', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0 }'),
	(126, 'Solutions', 'summary', 1, 1, 'RichText', 'Краткий текст', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0, "width" : "100%", "height" : 500, "toolbarSet" : "" }'),
	(127, 'Solutions', 'text', 1, 1, 'RichText', 'Полный текст', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0, "width" : "100%", "height" : 500, "toolbarSet" : "" }'),
	(128, 'Solutions', 'imageID', 1, 1, 'DamnUpload', 'Изображение 297x208', 0, 0, '', '{ "blOrder" : 0, "elemOrder" : 0, "image" : { "resize" : { "strong" : false, "width" : 1000, "height" : 1000 }, "thumb" : { "resize" : { "strong" : false, "width" : 150, "height" : 150 }, "postfix" : "_small" } }, "includeThumbCropDialog" : false, "maxFileSize" : 5048576, "filesType" : "image", "maxFilesNumber" : 1, "linkingTable" : "", "allowedExtensions" : "*.jpg; *.jpeg; *.gif; *.png", "filesTable" : "greeny_images", "filesTableKey" : "imageID", "destinationDirectory" : "uploaded/actions/[ID]/", "uploadScript" : "/admin/upload/" }');
/*!40000 ALTER TABLE `greeny_fieldsRegistry` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.greeny_files
CREATE TABLE IF NOT EXISTS `greeny_files` (
  `fileID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `src` varchar(255) NOT NULL,
  PRIMARY KEY (`fileID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.greeny_files: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `greeny_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `greeny_files` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.greeny_images
CREATE TABLE IF NOT EXISTS `greeny_images` (
  `imageID` int(11) NOT NULL AUTO_INCREMENT,
  `src` varchar(255) NOT NULL,
  `srcSmall` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.greeny_images: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `greeny_images` DISABLE KEYS */;
INSERT INTO `greeny_images` (`imageID`, `src`, `srcSmall`) VALUES
	(1, '/uploaded/menu/ico01.png', '/uploaded/menu/ico01_small.png'),
	(2, '/uploaded/menu/ico02.png', '/uploaded/menu/ico02_small.png'),
	(3, '/uploaded/menu/ico03.png', '/uploaded/menu/ico03_small.png'),
	(4, '/uploaded/news/1/img01.jpg', '/uploaded/news/1/img01_small.jpg'),
	(5, '/uploaded/news/1/img02.jpg', '/uploaded/news/1/img02_small.jpg');
/*!40000 ALTER TABLE `greeny_images` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.greeny_log
CREATE TABLE IF NOT EXISTS `greeny_log` (
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userID` int(11) DEFAULT NULL,
  `ip` int(11) NOT NULL,
  `type` enum('info','warning','error') NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=ARCHIVE DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.greeny_log: 6 rows
/*!40000 ALTER TABLE `greeny_log` DISABLE KEYS */;
INSERT INTO `greeny_log` (`time`, `userID`, `ip`, `type`, `message`) VALUES
	('2017-09-06 00:40:19', NULL, 2130706433, 'info', 'Authorisation, username=admin, userID=1'),
	('2017-09-07 06:27:33', 1, 2130706433, 'info', 'New page created. pageID = 2'),
	('2017-09-07 08:19:57', 1, 2130706433, 'info', 'New page created. pageID = 3'),
	('2017-09-07 08:21:37', 1, 2130706433, 'info', 'New page created. pageID = 4'),
	('2017-09-07 09:01:41', 1, 2130706433, 'info', 'New page created. pageID = 5'),
	('2017-09-07 09:03:18', 1, 2130706433, 'info', 'New page created. pageID = 6'),
	('2017-09-07 10:43:51', 1, 2130706433, 'info', 'New page created. pageID = 7');
/*!40000 ALTER TABLE `greeny_log` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.greeny_modules
CREATE TABLE IF NOT EXISTS `greeny_modules` (
  `moduleID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(127) NOT NULL DEFAULT '',
  `version` varchar(20) DEFAULT '',
  `rootAlias` varchar(50) DEFAULT NULL,
  `isVisible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`moduleID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.greeny_modules: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `greeny_modules` DISABLE KEYS */;
INSERT INTO `greeny_modules` (`moduleID`, `name`, `description`, `version`, `rootAlias`, `isVisible`) VALUES
	(1, 'AnyEditor', 'Всё Редактор', '', NULL, 0);
/*!40000 ALTER TABLE `greeny_modules` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.greeny_modulesSettings
CREATE TABLE IF NOT EXISTS `greeny_modulesSettings` (
  `settingID` int(11) NOT NULL AUTO_INCREMENT,
  `moduleID` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `order` mediumint(9) NOT NULL DEFAULT '0',
  `isVisible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`settingID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.greeny_modulesSettings: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `greeny_modulesSettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `greeny_modulesSettings` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.greeny_pageStructure
CREATE TABLE IF NOT EXISTS `greeny_pageStructure` (
  `pageID` int(11) NOT NULL AUTO_INCREMENT,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `caption` varchar(255) NOT NULL,
  `pageTypeID` int(11) DEFAULT NULL,
  `contentID` int(11) DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(255) NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `isSection` tinyint(1) NOT NULL DEFAULT '0',
  `isLogin` tinyint(1) NOT NULL DEFAULT '0',
  `isNoindex` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pageID`),
  KEY `pageTypeID` (`pageTypeID`),
  CONSTRAINT `greeny_pageStructure_ibfk_1` FOREIGN KEY (`pageTypeID`) REFERENCES `greeny_pageTypes` (`pageTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.greeny_pageStructure: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `greeny_pageStructure` DISABLE KEYS */;
INSERT INTO `greeny_pageStructure` (`pageID`, `parentID`, `caption`, `pageTypeID`, `contentID`, `title`, `description`, `keywords`, `priority`, `alias`, `isActive`, `isDeleted`, `isSection`, `isLogin`, `isNoindex`) VALUES
	(1, 0, 'Главная страница', 1, 1, 'Главная страница', 'Главная страница', 'Главная страница', 1000, 'main', 1, 0, 1, 0, 0),
	(2, 0, 'Документы', 1, 2, '', NULL, NULL, 0, 'docs', 1, 0, 0, 0, 0),
	(3, 0, 'Новости', NULL, 0, '', NULL, NULL, 0, 'news', 1, 0, 1, 0, 0),
	(4, 3, 'Новость №1', 2, 1, '', NULL, NULL, 0, 'news1', 1, 0, 0, 0, 0),
	(5, 0, 'Акции', NULL, 0, '', NULL, NULL, 0, 'actions', 1, 0, 1, 0, 0),
	(6, 5, 'Акция №1', 3, 1, '', NULL, NULL, 0, 'action1', 1, 0, 0, 0, 0),
	(7, 0, 'Решения', NULL, 0, '', NULL, NULL, 0, 'solutions', 1, 0, 1, 0, 0);
/*!40000 ALTER TABLE `greeny_pageStructure` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.greeny_pageTypes
CREATE TABLE IF NOT EXISTS `greeny_pageTypes` (
  `pageTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `handlerType` enum('module','table') NOT NULL DEFAULT 'module',
  `constantName` varchar(125) NOT NULL DEFAULT '',
  `noCreatePage` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Запрещает создание новых страниц данного типа из админки',
  PRIMARY KEY (`pageTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.greeny_pageTypes: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `greeny_pageTypes` DISABLE KEYS */;
INSERT INTO `greeny_pageTypes` (`pageTypeID`, `name`, `description`, `handlerType`, `constantName`, `noCreatePage`) VALUES
	(1, 'simplePages', 'Обычная страница', 'table', 'SIMPLE_PAGE_ID', 0),
	(2, 'News', 'Страница новости', 'table', 'NEWS_ID', 0),
	(3, 'Actions', 'Страница акции', 'table', 'ACTIONS_ID', 0),
	(4, 'Solutions', 'Страница решения', 'table', 'SOLUTIONS_ID', 0);
/*!40000 ALTER TABLE `greeny_pageTypes` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.greeny_pwdRecoveryTokens
CREATE TABLE IF NOT EXISTS `greeny_pwdRecoveryTokens` (
  `userID` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.greeny_pwdRecoveryTokens: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `greeny_pwdRecoveryTokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `greeny_pwdRecoveryTokens` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.greeny_routes
CREATE TABLE IF NOT EXISTS `greeny_routes` (
  `routeID` int(11) NOT NULL AUTO_INCREMENT,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `pattern` varchar(64) NOT NULL,
  `controller` varchar(64) NOT NULL,
  `action` varchar(64) NOT NULL DEFAULT 'Index',
  `defaultController` varchar(64) DEFAULT NULL,
  `defaultAction` varchar(64) DEFAULT NULL,
  `allowedGroups` varchar(128) NOT NULL DEFAULT '0',
  PRIMARY KEY (`routeID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.greeny_routes: ~32 rows (приблизительно)
/*!40000 ALTER TABLE `greeny_routes` DISABLE KEYS */;
INSERT INTO `greeny_routes` (`routeID`, `parentID`, `pattern`, `controller`, `action`, `defaultController`, `defaultAction`, `allowedGroups`) VALUES
	(1, 0, '*', '0', '1', 'DefaultController', 'Index', '0'),
	(2, 1, 'admin', 'AdminController', 'Index', NULL, NULL, '-1'),
	(3, 2, 'upload', 'UploadAdminController', 'Index', NULL, NULL, '-1'),
	(4, 2, 'cropthumbnail', 'CropThumbnailAdminController', 'Index', NULL, NULL, '-1'),
	(5, 2, 'structure', 'StructureController', '2', NULL, NULL, '-1'),
	(6, 2, 'anyeditor', 'AnyEditorController', '2', NULL, NULL, '-1'),
	(7, 2, 'pwdrecovery', 'PasswordRecovery', '2', NULL, NULL, '-1'),
	(8, 2, 'routing', 'RoutingController', 'Index', NULL, NULL, '-1'),
	(9, 8, 'edit', 'RoutingController', 'Edit', NULL, NULL, '-1'),
	(10, 8, 'add', 'RoutingController', 'Add', NULL, NULL, '-1'),
	(11, 8, 'delete', 'RoutingController', 'Delete', NULL, NULL, '-1'),
	(12, 2, '*', 'ModuleLinkController', 'Index', NULL, NULL, '-1'),
	(13, 1, 'ajax', 'AjaxController', '1', NULL, NULL, '0'),
	(14, 1, 'logout', 'FrontController', 'Logout', NULL, NULL, '0'),
	(15, 1, 'contacts', 'ContactsController', 'Index', NULL, NULL, '0'),
	(16, 1, 'getcaptcha', 'CaptchaGoogleController', 'GetCaptcha', NULL, NULL, '0'),
	(17, 1, 'sitemap', 'SitemapController', 'Index', NULL, NULL, '0'),
	(18, 1, '*', 'SimplePageController', 'Index', NULL, 'Index', '0'),
	(19, 1, '', 'IndexController', 'Index', NULL, 'Index', '0'),
	(20, 2, 'modules', 'ModulesController', '2', NULL, NULL, '0'),
	(21, 2, 'profile', 'ProfileController', '2', NULL, NULL, '0'),
	(22, 2, 'settings', 'SettingsController', 'Index', NULL, NULL, '0'),
	(23, 2, 'users', 'UsersController', 'Index', NULL, NULL, '0'),
	(24, 2, 'fieldsregistry', 'FieldsregistryController', 'Index', NULL, NULL, '0'),
	(25, 2, 'maintenance', 'MaintenanceController', 'Index', NULL, NULL, '0'),
	(26, 2, 'editor', 'EditorController', 'Index', NULL, NULL, '0'),
	(27, 2, 'custommenu', 'CustomMenuAdminController', 'Index', NULL, NULL, '0'),
	(28, 1, 'getimage', 'ImagesController', 'Index', NULL, NULL, '0'),
	(29, 1, 'actions', 'ActionsController', 'Index', NULL, NULL, '0'),
	(30, 29, '*', 'ActionsController', 'Item', NULL, NULL, '0'),
	(31, 1, 'news', 'NewsController', 'Index', NULL, NULL, '0'),
	(32, 31, '*', 'NewsController', 'Item', NULL, NULL, '0');
/*!40000 ALTER TABLE `greeny_routes` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.greeny_settings
CREATE TABLE IF NOT EXISTS `greeny_settings` (
  `settingID` int(11) NOT NULL AUTO_INCREMENT,
  `groupID` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `isVisible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`settingID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.greeny_settings: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `greeny_settings` DISABLE KEYS */;
INSERT INTO `greeny_settings` (`settingID`, `groupID`, `name`, `desc`, `value`, `isVisible`) VALUES
	(1, 4, 'fromEmail', 'Почтовый адрес отправителя', 'grant@ayaco.ru', 1),
	(2, 4, 'fromName', 'Имя отправителя', 'Грант', 1),
	(3, 1, 'siteName', 'Название сайта', 'CMS`кааа', 1),
	(4, 1, 'version', 'Версия CMS', '2.0', 0),
	(5, 2, 'defaultTitle', 'Заголовок страницы по умолчанию (если не задан в параметрах страницы)', 'Стандартный заголовок', 1),
	(6, 2, 'defaultDescription', 'Мета-описание страницы по умолчанию (если не задан в параметрах страницы)', 'Описание страницы', 1),
	(7, 2, 'defaultKeywords', 'Ключевые слова страницы по умолчанию (если не задан в параметрах страницы)', 'Ключевые слова', 1),
	(8, 1, 'LOGIN_FRONT_END', 'Авторизация на фронт-енде', '0', 1),
	(9, 3, 'maintenanceEnabled', 'Режим обслуживания ', '0', 1),
	(10, 3, 'maintenancePageText', 'Содержимое страницы обслуживания ', '<h1>Сайт находится на техническом обслуживании</h1>', 1);
/*!40000 ALTER TABLE `greeny_settings` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.greeny_settingsGroups
CREATE TABLE IF NOT EXISTS `greeny_settingsGroups` (
  `groupID` int(11) NOT NULL AUTO_INCREMENT,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  PRIMARY KEY (`groupID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.greeny_settingsGroups: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `greeny_settingsGroups` DISABLE KEYS */;
INSERT INTO `greeny_settingsGroups` (`groupID`, `parentID`, `name`, `desc`) VALUES
	(1, 0, 'general', 'Общие настройки'),
	(2, 0, 'front', 'Настройки фронт-энда'),
	(3, 0, 'admin', 'Настройки админки'),
	(4, 1, 'mailer', 'Почтовая рассылка');
/*!40000 ALTER TABLE `greeny_settingsGroups` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.MainMenu
CREATE TABLE IF NOT EXISTS `MainMenu` (
  `entryID` int(11) NOT NULL AUTO_INCREMENT,
  `pageID` int(11) NOT NULL,
  PRIMARY KEY (`entryID`),
  KEY `pageID` (`pageID`),
  CONSTRAINT `MainMenu_ibfk_1` FOREIGN KEY (`pageID`) REFERENCES `greeny_pageStructure` (`pageID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.MainMenu: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `MainMenu` DISABLE KEYS */;
/*!40000 ALTER TABLE `MainMenu` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.Menu
CREATE TABLE IF NOT EXISTS `Menu` (
  `itemID` int(11) NOT NULL AUTO_INCREMENT,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `menuListID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `imageID` int(11) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `isButton` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemID`),
  KEY `FK_Menu_MenuList` (`menuListID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Дамп данных таблицы iqhost.Menu: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `Menu` DISABLE KEYS */;
INSERT INTO `Menu` (`itemID`, `parentID`, `menuListID`, `name`, `alias`, `imageID`, `priority`, `isActive`, `isButton`) VALUES
	(1, 0, 1, '-', '-', NULL, 0, 1, 0),
	(2, 1, 1, 'Документы', '/docs/', NULL, 1, 1, 0),
	(3, 0, 2, '-', '-', NULL, 0, 1, 0),
	(4, 3, 2, 'Клиентам', '#', NULL, 1, 1, 0),
	(5, 4, 2, 'Возможности хостинга', '#', NULL, 1, 1, 0),
	(6, 3, 2, 'Партнерам', '#', NULL, 2, 1, 0),
	(7, 0, 3, '-', '-', NULL, 0, 1, 0),
	(8, 7, 3, 'Хостинг сайтов', '/service/site-hosting/', 1, 1, 1, 0),
	(9, 7, 3, 'Хостинг 1С Битрикс', '/service/site1c-hosting/', 2, 2, 1, 0),
	(10, 7, 3, 'Аренда сервера', '/service/server/', 3, 3, 1, 0);
/*!40000 ALTER TABLE `Menu` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.MenuList
CREATE TABLE IF NOT EXISTS `MenuList` (
  `menuListID` int(11) NOT NULL AUTO_INCREMENT,
  `menuListName` varchar(255) NOT NULL,
  `menuListAlias` varchar(255) NOT NULL,
  `menuListComment` varchar(255) NOT NULL,
  PRIMARY KEY (`menuListID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Дамп данных таблицы iqhost.MenuList: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `MenuList` DISABLE KEYS */;
INSERT INTO `MenuList` (`menuListID`, `menuListName`, `menuListAlias`, `menuListComment`) VALUES
	(1, 'Верхнее меню', 'TOP_MENU', 'Верхнее меню'),
	(2, 'Меню в футере', 'FOOTER_MENU', 'Меню в футере'),
	(3, 'Меню услуг', 'SERVICE_MENU', 'Меню услуг');
/*!40000 ALTER TABLE `MenuList` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.News
CREATE TABLE IF NOT EXISTS `News` (
  `newsID` int(11) NOT NULL AUTO_INCREMENT,
  `summary` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `imageID` int(11) DEFAULT NULL,
  `publicDate` timestamp NULL DEFAULT NULL,
  `modifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`newsID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.News: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `News` DISABLE KEYS */;
INSERT INTO `News` (`newsID`, `summary`, `text`, `imageID`, `publicDate`, `modifiedDate`) VALUES
	(1, 'описание', '<p>текст</p>', 4, '2017-09-07 00:00:00', '2017-09-07 08:21:37');
/*!40000 ALTER TABLE `News` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.Roles
CREATE TABLE IF NOT EXISTS `Roles` (
  `role` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role`),
  KEY `Role` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.Roles: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `Roles` DISABLE KEYS */;
INSERT INTO `Roles` (`role`, `name`, `level`) VALUES
	('admin', 'Администратор', 100),
	('moderator', 'Модератор', 80),
	('user', 'Пользователь', 20);
/*!40000 ALTER TABLE `Roles` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.simplePages
CREATE TABLE IF NOT EXISTS `simplePages` (
  `simplePageID` int(11) NOT NULL AUTO_INCREMENT,
  `pageContent` text NOT NULL,
  PRIMARY KEY (`simplePageID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.simplePages: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `simplePages` DISABLE KEYS */;
INSERT INTO `simplePages` (`simplePageID`, `pageContent`) VALUES
	(1, '<p>Текст главной страницы</p>'),
	(2, '<h3>Программный продукт <strong>DataCore SANsymphony-V</strong>предназначен для построения высокопроизводительных отказоустойчивых систем хранения данных с блочным доступом через Fibre Channel или iSCSI.</h3>\r\n<p>SANsymphony-V работает на любом совместимом с Microsoft Windows Server 2008R2/2012/2012R2 оборудовании, что позволяет избавиться от привязки к определенному вендору. Основное предназначение систем на базе DataCore SANsymphony-V &mdash; виртуализация внешних систем хранения данных. Различные СХД могут иметь недостаточную производительность, функционал, проблемы с взаимной совместимость. При помощи DataCore SANsymphony-V можно объединить уже имеющиеся у заказчика СХД в единый пул с увеличением производительности (за счет кэширования в RAM и использования ярусного хранения данных), повышением уровня отказоустойчивости (за счет построения 2-узловых систем с синхронной репликацией) и добавлением необходимого функционала (например, поддержки VAAI/ODX)</p>\r\n<div class="img-area">\r\n<div class="alignleft"><img src="/uploaded/img03.jpg" alt="image description" width="540" height="405" /></div>\r\n<h3>Программный продукт <strong>DataCore SANsymphony-V</strong>предназначен для построения высокопроизводительных отказоустойчивых систем хранения данных с блочным доступом через Fibre Channel или iSCSI.</h3>\r\n<p>SANsymphony-V работает на любом совместимом с Microsoft Windows Server 2008R2/2012/2012R2 оборудовании, что позволяет избавиться от привязки к определенному вендору. Основное предназначение систем на базе DataCore SANsymphony-V &mdash; виртуализация внешних систем хранения данных. Различные СХД могут иметь недостаточную производительность, функционал, проблемы с взаимной совместимость. При помощи DataCore SANsymphony-V можно объединить уже имеющиеся у заказчика СХД в единый пул с увеличением производительности (за счет кэширования в RAM и использования ярусного хранения данных), повышением уровня отказоустойчивости (за счет построения 2-узловых систем с синхронной репликацией) и добавлением необходимого функционала (например, поддержки VAAI/ODX)</p>\r\n</div>\r\n<h3>Программный продукт <strong>DataCore SANsymphony-V</strong>предназначен для построения высокопроизводительных отказоустойчивых систем хранения данных с блочным доступом через Fibre Channel или iSCSI.</h3>\r\n<p>SANsymphony-V работает на любом совместимом с Microsoft Windows Server 2008R2/2012/2012R2 оборудовании, что позволяет избавиться от привязки к определенному вендору. Основное предназначение систем на базе DataCore SANsymphony-V &mdash; виртуализация внешних систем хранения данных. Различные СХД могут иметь недостаточную производительность, функционал, проблемы с взаимной совместимость. При помощи DataCore SANsymphony-V можно объединить уже имеющиеся у заказчика СХД в единый пул с увеличением производительности (за счет кэширования в RAM и использования ярусного хранения данных), повышением уровня отказоустойчивости (за счет построения 2-узловых систем с синхронной репликацией) и добавлением необходимого функционала (например, поддержки VAAI/ODX)</p>');
/*!40000 ALTER TABLE `simplePages` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.Solutions
CREATE TABLE IF NOT EXISTS `Solutions` (
  `solutionID` int(11) NOT NULL AUTO_INCREMENT,
  `summary` text NOT NULL,
  `text` text NOT NULL,
  `imageID` int(11) DEFAULT NULL,
  PRIMARY KEY (`solutionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Дамп данных таблицы iqhost.Solutions: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `Solutions` DISABLE KEYS */;
/*!40000 ALTER TABLE `Solutions` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.Users
CREATE TABLE IF NOT EXISTS `Users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `FIO` varchar(255) NOT NULL,
  `role` enum('user','moderator','admin') NOT NULL DEFAULT 'user',
  `isActive` tinyint(1) NOT NULL,
  `registrationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.Users: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` (`userID`, `login`, `email`, `password`, `FIO`, `role`, `isActive`, `registrationDate`) VALUES
	(1, 'admin', 'ie@committee.ru', '$2a$08$4D9weBAt/hWz3Wmb1buEwOsBjbyOKrhy0nPYiSXGw4md1Gido2d26', 'admin', 'admin', 1, '2009-07-01 20:00:00');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.UsersIdentity
CREATE TABLE IF NOT EXISTS `UsersIdentity` (
  `entryID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL DEFAULT '0',
  `identity` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`entryID`),
  KEY `userID` (`userID`),
  CONSTRAINT `UsersIdentity_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.UsersIdentity: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `UsersIdentity` DISABLE KEYS */;
/*!40000 ALTER TABLE `UsersIdentity` ENABLE KEYS */;

-- Дамп структуры для таблица iqhost.UsersInfo
CREATE TABLE IF NOT EXISTS `UsersInfo` (
  `userID` int(11) NOT NULL,
  `FIO` varchar(255) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` enum('Мужской','Женский') NOT NULL DEFAULT 'Мужской',
  PRIMARY KEY (`userID`),
  CONSTRAINT `UsersInfo_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы iqhost.UsersInfo: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `UsersInfo` DISABLE KEYS */;
INSERT INTO `UsersInfo` (`userID`, `FIO`, `birthdate`, `gender`) VALUES
	(1, 'Admin', '1979-06-26', 'Мужской');
/*!40000 ALTER TABLE `UsersInfo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

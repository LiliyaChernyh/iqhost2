<h1>Свойства заказа</h1>

<a href="javascript:history.back()">Назад</a>
<div>
	{assign var=OrderDate value=$ORDER->GetCreationDate()}
	<div>Дата заказа: {$OrderDate}</div>
	<div>Стоимость: {$ORDER->GetOrderPrice()} р.</div>
	<div>
		{assign var=orderStatusID value=$ORDER->GetStatus()}
		Статус заказа:
		<select id="orderStatusSelect">
			<option value="0"></option>
			{foreach from=$STATUSES item=status key=statusID}
				<option value="{$statusID}" {if $statusID == $orderStatusID}selected="selected"{/if}>{$status}</option>
			{/foreach}
		</select>
	</div>
	<div>Способ доставки: {$ORDER->GetDeliveryMethodText()}</div>
	<div>Стоимость доставки: {$ORDER->GetDeliveryPrice()}</div>
	<div>Номер трека: {$ORDER->GetDeliveryData()|default:'Нет'}</div>
	<div>Способ оплаты: {$ORDER->GetPaymentMethodText()}</div>

	<div>Оплачен: {if $ORDER->GetIsPaid()}<span id="isPaid" class="paid">Да</span>{else}<span id="isPaid" class="not-paid">Нет</span>{/if}</div>
        <div>Заказчик: {$ORDER->GetUserInfo()}</div>
        <div>Адрес оплаты: {$ORDER->GetPaymentAddressText()}</div>
        {assign var=Delivery value=$ORDER->GetDeliveryAddressText()}
        <div>Адрес доставки: {if !empty($Delivery)}{$Delivery}{else}адрес оплаты совпадает с адресом доставки{/if}</div>
	<div>Продукты:</div>
	{*	<ul>
	{foreach from=$PRODUCTS item=product}
			<li><a href="/admin/store/products/edit/{$product.productID}/">{$product.name} - {$product.amount} шт. ({$product.productPrice} р.)</a></li>
		{/foreach}
	</ul>
	*}		<div class="wrap">
			<div class="casing-title">
				<span class="order-title">Продукт:</span>
				<span class="price-title">Цена за штуку:</span>
				<span class="count-title">Кол-во(шт.):</span>
				<span class="sum-title">Сумма:</span>
			</div>

			{foreach from=$PRODUCTS key=key item=product}
			<div class="casing cart" id="{$key}">
				<figure class="hold-icon"><img src="/getimage/{$product.image}/50x50xS/" alt="image" width="50" height="50" /></figure>
				<div class="article">
				<span class="art">Артикул</span>&nbsp;<strong>{$product.articul}</strong>
				<a href="/catalog/product/{$product.alias}/">{$product.name}</a>
			</div>
			<div class="param">
				{if isset($product.options[0].size)}
					<dl>
						<dt>Размер:</dt>
						<dd>{$product.options[0].size}</dd>
					</dl>
				{/if}
				{if isset($product.options[0].optionValue)}
					<dl>
						<dt>{$product.options[0].optionFrontName}:</dt>
						<dd>{$product.options[0].optionValue}</dd>
					</dl>
				{/if}
				{foreach from=$product.details item=attr}
					{if !empty($attr.productAttributeValue) AND $attr.attributeGroup != 'box'}
						<dl>
							<dt>{$attr.attributeName}:</dt>
							<dd>{$attr.productAttributeValue}</dd>
						</dl>
					{/if}
				{/foreach}
				{if ($product.weight > 0)}
					<dl>
						<dt>Вес:</dt>
						<dd>{$product.weight} г</dd>
					</dl>
				{/if}
			</div>
			<div class="hold-price">
				<span class="price"><span>{$product.productPrice|number_format:2}</span> р.</span>
			</div>
			<div class="hold-amount">
				<span>{$product.amount}</span>
			</div>
			{*								<div class="hold-color">
			<span class="color">Красный</span>
			</div>
			*}
			<div class="addcart">
				<span class="price">{$product.productPrice*$product.amount|number_format:2} р.</span>
			</div>
		</div>
				{*{$ORDER->GetOrderPrice()} р.*}
			{/foreach}
			<div class="total-amount">
				<div class="total-index">
					<strong class="total-sum">Общая сумма заказа:</strong>
					<span class="sum">{$ORDER->GetOrderPrice()|number_format:2} р.</span>
				</div>
			</div>

		</div>
</div>
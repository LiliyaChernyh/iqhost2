<h1>Добавление пользователя</h1>

{literal}
<script type="text/javascript">
    $(document).ready(function(){
        $('#autoGenerate').change(function(){
            if ($(this).attr("checked"))
            {
                $('.access').hide();
            }
            else
            {
                $('.access').show();
            }
        })
    });
</script>
{/literal}

<form action="" method="post" id="contentForm" enctype="multipart/form-data">
    <div class="formBlockHeader">Параметры доступа</div>
	<div class="formBlock">
        <div class="formElement">
			<label for="autoGenerate" class="formElementLabel">Автогенерация пароля</label>
			<input type="hidden" name="autoGenerate" value="0" />
			<input type="checkbox" name="autoGenerate" id="autoGenerate" value="1" />
		</div>
        <div class="formElement access">
			<label for="password" class="formElementLabel">Пароль</label>
			<input type="password" name="password" id="password" value="" size="60" />
		</div>
        <div class="formElement access">
			<label for="passwordConf" class="formElementLabel">Подтверждение пароля</label>
			<input type="password" name="passwordConf" id="passwordConf" value="" size="60" />
		</div>
	</div>
	<div style="clear:both"></div>

	{include file='formBlocks.tpl'}

	<div style="margin-left:350px">
		<input type="submit" value="Сохранить" name="doSave" class="handyButton" />
	</div>

</form>
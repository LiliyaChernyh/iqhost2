<?php
/**
 * LinkedTable
 * 
 * 
 * Контрол с произвольным подшаблоном для свяи с другой таблицей.
 * Можно добавлять несколько записей в другую таблицу.
 * Для использования нужно только настроить и задать дополнительный шаблон
 * с JS обработчиками
 * 
 * К концу я понял, что сделать его универсальным нереально.
 * Надо подключать формогенератор как-то, чтобы было универсально.
 * тот контрол справится максимум с простыми текстовыми полями...
 * иначе слишком сложно.
 * И с формгенератором будет ещё сложнее, поэтому титрые контролы лучше делать
 * руками индивидуально для каждого проекта.
 * 
 * 
 * версия 1.0
 *
 */
class LinkedTable extends Control 
{	
	public static $ClientFilesPath = '/libs/ContentManager/Controls/LinkedTable/';

	/**
	 * Класс, подставляемый в контейнер контрола
	 *
	 * @var string
	 */
	private $className = null;
	/**
	 * Имя таблицы, участвующей в связи многие ко многим
	 *
	 * @var string
	 */
	private $referentTableName;
	/**
	 * Дополнительный внутренний шаблон
	 * 
	 * @var string
	 */
	private $linkedTableControlTpl;
	
	
	/**
	 * $field и $value передавать как null они нам тут не нужны
	 *
	 * @param DBFieldStructure $field
	 * @param array $controlParams
	 * @param string $value
	 * @param DBTableManager $controlsRegistry
	 */
	public function __construct(&$field = null, &$controlParams, &$controlsRegistry, &$activeTableStructure)
	{
		if ($controlsRegistry == null)
			throw new ArgumentException('$controlsRegistry must be not null');
		if ($activeTableStructure == null)
			throw new ArgumentException('$activeTableStructure must be not null');
	
		parent::__construct($field, $controlParams, $controlsRegistry, $activeTableStructure);
	}
	
	public function InitControl($value = null, $rowID = null)
	{
	    $this -> LoadSettings();
	    $this -> controlTplPath = CONTROLS_PATH.'LinkedTable/control.tpl';
		
	    $this -> controlTplVars = array();
	    $this -> controlTplVars['NAME'] = $this -> controlParams['fieldName'];
		$this -> controlTplVars['LIBS_PATH'] = self::$ClientFilesPath;
		$this -> controlTplVars['TIP'] =  $this -> controlParams['tip'];
		$this -> controlTplVars['CLASS'] =  $this -> className;
		$this -> controlTplVars['TABLE_CONTROL_TPL'] = CONTROLS_PATH.'LinkedTable/' . $this -> linkedTableControlTpl;
		
		// Вытаскиваем значения ReferentTable - это та таблица, в которой хранятся связанные записи
		$referentTableManager = new DBTableManager($this -> referentTableName);
		$referentTableListingRow = $this -> controlsRegistry -> Select(array('tableName' => $this -> referentTableName, 'isListingKey' => 1));
		
		if (count($referentTableListingRow) == 0) throw new Exception('Referent Table '.$this -> referentTableName.' is not registered in controlsRegistry or ListingKey is not defined.');
		$listingKey = $referentTableListingRow[0]['fieldName'];
		
		// Проверим есть ли в referentTable связанные записи
		if ($rowID != null)
		{
            $linkedRows = $referentTableManager -> Select(array($this -> activeTableStructure ->  PrimaryKey -> Name => $rowID));
            $this -> controlTplVars['savedEntries'] = $linkedRows;
		}
		
		$this -> name = $field->Name;
		$this -> type = 'LinkedTable';
		$this -> description = $this -> controlParams['description'];		
	}
	
	/**
	 * Вытаскивает различные настройки элемента управления (из JSON)
	 *
	 */
	private function LoadSettings()
	{
		$this -> className = '';
		
		// Посмотрим заданы ли JSON настройки
		if (isset($this -> controlParams['controlSettings']) && $this -> controlParams['controlSettings'] != '')
		{
			$Json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
			$Jresult = $Json -> decode($this -> controlParams['controlSettings']);
			
			if (isset($Jresult['referentTable']))
				$this -> referentTableName = $Jresult['referentTable'];
			else 
				throw new Exception('Referent Table for '.$field -> Name.' is undefined. It should be defined in FieldsRegistry');
			
			if (isset($Jresult['class'])) $this -> className = 'class="'.$Jresult['class'].'"';
			
			if (isset($Jresult['linkedTableControlTpl']))
				$this -> linkedTableControlTpl = $Jresult['linkedTableControlTpl'];
			else 
				throw new Exception('Linked Table template linkedTableControlTpl must be defined.');
			
			$this -> InitBlockProps($Jresult);
		}
		else 
		{
			throw new Exception('Settings for '.$field -> Name.' are undefined. They should be defined in FieldsRegistry. "referentTable" setting needed.');
		}
	}
	
	public function Flush(&$data, $rowID)
	{
		$this -> LoadSettings();
		$referentTableStructure = new DBTableStructure($this -> referentTableName);
		$referentTblMngr = new DBTableManager($referentTableStructure -> TableName, $referentTableStructure);
		
		$newRows = array();
		$keys = array_keys($data);
		$entriesCnt = count($data[$keys[0]]);
		for ($i = 0; $i < $entriesCnt; $i++)
		{
		    $entry = array();
		    $allEmpty = true;
		    for ($j = 0; $j < count($keys); $j++)
		    {
		        $entry[$keys[$j]] = (preg_match('//u', $data[$keys[$j]][$i])) ? trim($data[$keys[$j]][$i]) : trim($data[$keys[$j]][$i]);
		        if (!empty($entry[$keys[$j]])) $allEmpty = false;
		        if (strpos($keys[$j], 'Date')) $entry[$keys[$j]] = date('Y-m-d', strtotime( $entry[$keys[$j]]));
		    }
		    if ($allEmpty) continue;
		    $entry[$this -> activeTableStructure -> PrimaryKey -> Name] = $rowID;
            $referentTblMngr -> Insert($entry);		    
		}
		
		// Удаляем записи
		foreach ($data as $key => $value)
		{
			if (strpos($key, 'kill') !== false)
			{
				$killArr = explode('_', $key);
				
				if (isset($killArr[1]) && is_numeric($killArr[1]))
				{
					$referentTblMngr -> Delete(array($referentTableStructure -> PrimaryKey -> Name => $killArr[1]));
				}
			}
		}
	}
}
?>
<script type="text/JavaScript">

	// В подшаблоне должна быть определена такая функция, если она нужна, конечно
	if (typeof newControlInstanceInitialiser == "undefined")
		newControlInstanceInitialiser = new Object();
	if (typeof newControlInstancePostInitialiser == "undefined")
		newControlInstancePostInitialiser = new Object();
	/**
	* Функция инициализации нового экземпляра
	* @param jQuery instance
	*/
	newControlInstanceInitialiser["{$localVars.NAME}"] = function(instance)
	{literal}
	{
	{/literal}
	var controlName = "{$localVars.NAME}";
	{literal}
	
		var num = $("[id*="+controlName+"_linkedTable_container]").length;
		if (instance.attr("id") == controlName+"_linkedTable_container") num = 0;
		instance.children("td").eq(1/*gender*/).children().each(function()
		{
			var that = $(this);
			var parts;
			if (that.is("label"))
			{
				parts = that.attr("for").split("_");
				parts[parts.length-1] = num;
				that.attr("for", parts.join("_"));
			}
			else if (that.is("input"))
			{
				parts = that.attr("id").split("_");
				parts[parts.length-1] = num;
				that.attr("id", parts.join("_"));
				
				that.attr("name", controlName+"__gender["+num+"]");
			}
		});
		var dateInput = instance.children("td").eq(0).children("input");
		dateInput.attr("name", controlName+"__birthDate["+num+"]");
	}
	{/literal}
	newControlInstancePostInitialiser["{$localVars.NAME}"] = function(instance)
	{literal}
	{
	{/literal}
	var controlName = "{$localVars.NAME}";
	{literal}
		// Всё из-за глюка datepicker'a клон поля придётся удалить и создать заново.
		var  dateInput = instance.children("td").eq(0).children("input");
		var dateInputName = dateInput.attr("name");
		dateInput.remove();
		dateInput = $("<input type=\"text\" >");
		dateInput.addClass("hasDatePicker");
		dateInput.attr("name", dateInputName);
		instance.children("td").eq(0).append(dateInput);
		var dt = new Date();
		dateInput.datepicker({yearRange: "1970:"+dt.getFullYear()});
	}
	{/literal}
</script>
<table>
	<tr>
		<td>Дата рождения</td>
		<td>Пол</td>
		<td>Описание</td>
		<td>Удалить</td>
	</tr>
{if $localVars.savedEntries}
{foreach from=$localVars.savedEntries item=entry}
	<tr>
		<td class="birthDate">{$entry.birthDate}</td>
		<td>{$entry.gender}</td>
		<td>{$entry.description}</td>
		<td><input type="checkbox" name="{$localVars.NAME}__kill_{$entry.childID}" /></td>
	</tr>
{/foreach}
{/if}
	<tr id="{$localVars.NAME}_linkedTable_container" {if $localVars.CLASS}{$localVars.CLASS}{/if} style="vertical-align:top">
		<td><input type="text" id="{$localVars.NAME}_birthDate_0" name="{$localVars.NAME}__birthDate[]" value="" title="" /></td>
		<td>
		<label for="{$localVars.NAME}_gender_m_0">м</label><input type="radio" id="{$localVars.NAME}_gender_m_0" name="{$localVars.NAME}__gender[0]" value="м" title="" />
		<label for="{$localVars.NAME}_gender_f_0">ж</label><input type="radio" id="{$localVars.NAME}_gender_f_0" name="{$localVars.NAME}__gender[0]" value="ж" title="" />
		</td>
		<td><textarea cols="20" rows="3" name="{$localVars.NAME}__description[]"></textarea></td>
		<td><a href="#" class="killLinkedTableRowControl">[ x ]</a></td>
	</tr>
</table>
<?php
/**
 * LineInput
 * Контрол для представления поля в виде
 * простого текстового поля input type="text"
 *
 * @version 3.4
 *
 */
class LineInput extends Control
{
    private $size = 60;

    public function InitControl($value = null, $rowID = null)
    {
        $value = (is_null($value)) ? '' : htmlspecialchars($value);

        $this -> controlTplPath = parent::$CONTROLS_PATH.__CLASS__.'/control.tpl';

        $this -> controlTplVars = array();
        $this -> controlTplVars['NAME'] = $this -> name;

        if (!empty($this -> controlSettings['size']))
            $this -> size = $this -> controlSettings['size'];
        $this -> controlTplVars['SIZE'] = $this -> size;
        $this -> controlTplVars['CLASS'] = $this -> CSSClass;
        $this -> controlTplVars['LENGTH'] = !empty($this -> controlSettings['length']) ? $this -> controlSettings['length'] : $this -> size ;
        $this -> controlTplVars['TIP'] = $this -> tip;
        if ((is_null($value) || (strlen($value) == 0)) && isset($this->controlSettings['defaultValue']))
        {
            $this -> controlTplVars['VALUE'] = $this->controlSettings['defaultValue'];
        }
        else
        {
        	$this -> controlTplVars['VALUE'] = $value;
		}

        $this -> controlTplVars['tableName'] = $this->tableName;
        $this -> controlTplVars['formIndex'] = (isset($this->formIndex))?$this->formIndex:0;
        if ($this -> disabled) $this -> controlTplVars['READONLY'] = 1;
        if ($this -> required) $this -> controlTplVars['REQUIRED'] = 1;

        $this -> type = __CLASS__;
    }
    public function PostBackHandler(&$data, $rowID = null) {}
}

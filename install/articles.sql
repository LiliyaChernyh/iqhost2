CREATE TABLE `greeny_articles_sections` (
  `asectionID` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `modificationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `greeny_articles_sections`
  ADD PRIMARY KEY (`asectionID`);

ALTER TABLE `greeny_articles_sections`
  MODIFY `asectionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;


CREATE TABLE `greeny_articles` (
  `articleID` int(11) NOT NULL,
  `asectionID` int(11) DEFAULT NULL,
  `caption` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `imageID` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `summary` varchar(1000) DEFAULT NULL,
  `body` text NOT NULL,
  `publicationDate` datetime NOT NULL,
  `modificationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isPublished` tinyint(1) DEFAULT '1',
  `isDeleted` tinyint(1) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `greeny_articles`
  ADD PRIMARY KEY (`articleID`),
  ADD KEY `FK_ASECTION` (`asectionID`);

ALTER TABLE `greeny_articles`
  MODIFY `articleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `greeny_articles`
  ADD CONSTRAINT `FK_ASECTION` FOREIGN KEY (`asectionID`) REFERENCES `greeny_articles_sections` (`asectionID`) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `greeny_fieldsRegistry` (`entryID`, `tableName`, `fieldName`, `canChange`, `visible`, `controlType`, `description`, `isListingKey`, `isMultiLanguage`, `tip`, `controlSettings`) VALUES
(null, 'greeny_articles', 'articleID', 0, 0, 'HiddenInput', '', 0, 0, NULL, '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"blName\":\"Основная информация\" }'),
(null, 'greeny_articles', 'asectionID', 1, 1, 'SingleSelect', 'Категория', 0, 0, NULL, '{ \"enum\" : false, \"referentTable\" : \"greeny_articles_sections\", \"listingKey\" : \"caption\", \"blOrder\" : 0, \"elemOrder\" : 10, \"emptyValue\" : \"\", \"className\" : \" \", \"emptyText\" : \" - \", \"orderBy\":\"asectionID\" }'),
(null, 'greeny_articles', 'caption', 1, 1, 'LineInput', 'Заголовок', 0, 0, NULL, '{ \"blOrder\" : 0, \"elemOrder\" : 20, \"size\" : 60 }'),
(null, 'greeny_articles', 'alias', 1, 1, 'LineInput', 'Алиас', 0, 0, 'для создания ЧПУ УРЛ', '{ \"blOrder\" : 0, \"elemOrder\" : 30, \"size\" : 60 }'),
(null, 'greeny_articles', 'imageID', 1, 1, 'DamnUpload', 'Фотография', 0, 0, NULL, '{ \"blOrder\" : 0, \"elemOrder\" : 40, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 2800, \"height\" : 2000 }, \"thumb\" : { \"resize\" : { \"strong\" : true, \"width\" : 220, \"height\" : 165 }, \"postfix\" : \"_small\" }}, \"includeThumbCropDialog\" : true, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/articles/[ID]/\", \"uploadScript\" : \"/admin/upload/\" }'),
(null, 'greeny_articles', 'summary', 1, 1, 'TextArea', 'Краткое содержание', 0, 0, NULL, '{ \"blOrder\" : 0, \"elemOrder\" : 50, \"cols\" : 60, \"rows\" : 5 }'),
(null, 'greeny_articles', 'body', 1, 1, 'RichText', 'Полный текст', 0, 0, NULL, '{ \"blOrder\" : 0, \"elemOrder\" : 60, \"width\" : \"100%\", \"height\" : 500 }'),
(null, 'greeny_articles', 'publicationDate', 1, 1, 'DateInput', 'Дата публикации', 0, 0, NULL, '{ \"dateFormat\" : \"d.m.Y\", \"yearRange\" : \"-10:+5\", \"insertCurrentDateIfEmpty\" : true, \"ignoreUserInput\" : false, \"blOrder\" : 1,  \"elemOrder\" : 10 ,\"blName\":\"Настройки публикации\"}'),
(null, 'greeny_articles', 'isPublished', 1, 1, 'CheckBox', 'Публиковать', 0, 0, NULL, '{ \"checkedDefault\" : true, \"blOrder\" : 1, \"elemOrder\" : 20 }'),
(null, 'greeny_articles', 'title', 1, 1, 'LineInput', 'Title', 0, 0, NULL, '{ \"blOrder\" : 2, \"elemOrder\" : 10, \"size\" : 60 , \"blName\" : \"Мета-информация\"}'),
(null, 'greeny_articles', 'description', 1, 1, 'LineInput', 'Description', 0, 0, NULL, '{ \"blOrder\" : 2, \"elemOrder\" : 0, \"size\" : 60 }'),
(null, 'greeny_articles', 'keywords', 1, 1, 'LineInput', 'Keywords', 0, 0, NULL, '{ \"blOrder\" : 2, \"elemOrder\" : 0, \"size\" : 60 }'),
(null, 'greeny_articles_sections', 'asectionID', 1, 1, 'HiddenInput', '', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),
(null, 'greeny_articles_sections', 'caption', 1, 1, 'LineInput', 'Заголовок', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),
(null, 'greeny_articles_sections', 'alias', 1, 1, 'LineInput', 'Алиас', 0, 0, 'для создания ЧПУ УРЛ', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),
(null, 'greeny_articles_sections', 'title', 1, 1, 'LineInput', 'Title', 0, 0, '', '{ \"blOrder\" : 1, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\", \"blName\" : \"Мета-информация\" }'),
(null, 'greeny_articles_sections', 'description', 1, 1, 'LineInput', 'Description', 0, 0, '', '{ \"blOrder\" : 1, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),
(null, 'greeny_articles_sections', 'keywords', 1, 1, 'LineInput', 'Keywords', 0, 0, '', '{ \"blOrder\" : 1, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }');

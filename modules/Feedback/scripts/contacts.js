﻿$(document).ready(function() {
	$("#contactsForm").ajaxForm(
		{
			beforeSubmit:  contactsFormSubmitHandler,
			success:       contactsFormResponseHandler,
			dataType:	   "json"
		}
	);
	
	$("#captchaImg").css("cursor","pointer").click(function()
	{
		$(this).attr("src", $(this).attr("src") + "?" + Math.random());
	});
});

function contactsFormSubmitHandler(form, jO)
{
	// Validation
	$(".error:visible").hide();
	if (!$("#fio").val().match(/\S/))
	{
		$("#enterName:hidden").slideToggle();
		return false;
	}
	
	if (!$("#message").val().match(/\S/))
	{
		$("#enterMessage:hidden").slideToggle();
		return false;
	}
	
	if (!$("#email").val().match(/\S/) && !$("#phone").val().match(/\S/))
	{
		$("#enterEmailOrPhone:hidden").slideToggle();
		return false;
	}
	
	if ($("#email").val() != "" && !(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test($("#email").val())))
	{
		$("#wrongEmail:hidden").slideToggle();
		return false;
	}
}

function contactsFormResponseHandler(data, status)
{
	if (typeof data.error != "undefined")
	{
		if (typeof data.errno != "undefined")
		{
			switch(data.errno)
			{
				case 555:
					$("#captchaImg").click();
					$("#wrongCaptcha").slideToggle();
					return;
					break;
				case '42S02':
					data.error += " Установи feedback-модуль, балда!";
					break;
			}
		}
		$("#error:hidden").slideToggle();
		$("#error").text(data.error);
		return;
	}
	
	$("#contactsSuccess").slideToggle();
	$("#contactsFormBlock").slideToggle();
	
}
<ul>
	{foreach from=$MODULES_MENU item=MENU_MODULE}
		<li{if $ACTION_ALIAS == $MENU_MODULE.name|lower} class="active"{/if}>
			<a href="{$ADMIN_ALIAS}{$MENU_MODULE.name}/">{$MENU_MODULE.description} {$MENU_MODULE.name}</a>
			<a href="{$SECTION_ALIAS}edit/{$MENU_MODULE.name}/" title="Редактировать настройки модуля  '{$MENU_MODULE.description}'" style="font-size: 80%;">[ edit ]</a>
		</li>
	{/foreach}
	<li {if $ACTION_ALIAS == 'install'} class="active"{/if}>
		<span style="color:gray;font-size:80%;letter-spacing:-1px;">(+)</span> <a href="{$SECTION_ALIAS}install/" title="Установить модуль">Установить модуль</a>
	</li>
</ul>
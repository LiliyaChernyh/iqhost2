<?php
class CaptchaGoogleController extends Controller
{
	public function GetCaptcha()
	{
		if (!isset($_SESSION)) 
			@session_start();
		$captcha = new CaptchaGoogle();

		// OPTIONAL Change configuration...
		//$captcha->wordsFile = 'words/es.php';
		$captcha->session_var = 'CAPTCHA';
		$captcha->imageFormat = 'png';
		//$captcha->scale = 3; $captcha->blur = true;
		$captcha->resourcesPath = IncPaths::$ROOT_PATH."/libs/CaptchaGoogle/resources";
		$captcha->backgroundColor = array(225, 236, 242);
		
		// OPTIONAL Simple autodetect language example
		/*
		if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
		    $langs = array('en', 'es');
		    $lang  = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
		    if (in_array($lang, $langs)) {
		        $captcha->wordsFile = "words/$lang.php";
		    }
		}
		*/
		
		// Image generation
		$captcha->CreateImage();
		return new EmptyActionResult();
	}
	
	public static function CheckCaptcha($captcha)
    {
        @session_start();	
        $ok = (isset($_SESSION['CAPTCHA']) && ($captcha == $_SESSION['CAPTCHA']));
        unset($_SESSION['CAPTCHA']);
        return $ok;
    }
}
<?php
require_once(IncPaths::$MODULES_PATH.'CommonModule.class.php');

/**
 * Ядро интернет-магазина.
 *
 * @version 3.1
 */
class GreenyShop  extends CommonModule
{
	public static $cartTableName = 'greeny_sh_carts';
	public static $ordersTableName = 'greeny_sh_orders';
	public static $statusesTableName = 'greeny_sh_statuses';
	public static $deliveryAdressesTableName = 'greeny_sh_deliveryAdresses';
        
        function __construct()
	{
            parent::__construct(__CLASS__);
	}
	
	/**
	 * Менеджер таблицы клиентов
	 *
	 * @var DBTableManager
	 */
	private $customersTable = null;
	
	/**
	 * ID текущего заказчика
	 *
	 * @var int
	 */
	public $currentCustomerID = null;
	
	/**
	 * Менеджер таблицы заказов
	 *
	 * @var DBTableManager
	 */
	private $ordersTable = null;
	
	/**
	 * ID текущего заказа
	 * 
	 * @var int
	 */
	public $currentOrderID;
	
	/**
	 * Счётчик заказов. Обновляется функцией GetOrders, поскольку она 
	 * возвращает только тот список, который пользователь может просматривать
	 *
	 * @var int
	 */
	public $ordersCnt = null;
	
	/**
	 * Массив статусов и соответствующих им ID
	 * new 
	 * received
	 * confirmed
	 * sent
	 * delivered
	 * closed
	 *
	 * @var array
	 */
	private $statusesAssoc = null;
	
	/**
	 * Список статусов
	 * @var array
	 */
	private $statuses = null;
	
	/**
	 * Менеджер таблицы тележек
	 *
	 * @var DBTableManager
	 */
	private $cartsTable = null;
	
	/**
	 * Менеджер таблицы адресов доставки
	 *
	 * @var DBTableManager
	 */
	private $deliveryAdressesTable = null;
	
	
	/**
	 * Загтружает статусы
	 *
	 */
	private function LoadStatuses()
	{
		if ($this-> statusesAssoc != null && $this -> statuses != null)
			return;
		$statusesMngr = new DBTableManager(self::$statusesTableName);
		$statusesRows = $statusesMngr -> Select();
		$this -> statuses = array();
		$this -> statusesAssoc = array();
		for ($i = 0; $i < count($statusesRows); $i++)
		{
			$this -> statuses[$statusesRows[$i]['statusID']] = $statusesRows[$i];
			$this -> statusesAssoc[$statusesRows[$i]['name']] = $statusesRows[$i]['statusID'];
		}			
	}
	
	/**
	 * Возвращает список со статусами
	 * @return array
	 */
	public function GetStatuses()
	{
		$this -> LoadStatuses();
		return $this -> statuses;
	}
	
	/**
	 * Возвращает ассоциативный массив со статусами
	 * @return array
	 */
	public function GetStatusesAssoc()
	{
		$this -> LoadStatuses();
		return $this -> statusesAssoc;
	}
	
	/**
	 * Инициализация менеджера таблицы заказов
	 * @return void
	 */
	private function InitOrdersTable()
	{
		if ($this -> ordersTable == null)
			$this -> ordersTable = new DBTableManager(self::$ordersTableName); 
	}
	
	/**
	 * Инициализация менеджера таблицы тележек
	 *
	 */
	private function InitCartsTable()
	{
		if ($this -> cartsTable == null)
			$this -> cartsTable =  new DBTableManager(self::$cartTableName);
	}
	
	/**
	 * Инициализация менеджера таблицы адресов заказа
	 *
	 */
	private function InitDeliveryAdressesTable()
	{
		if ($this -> deliveryAdressesTable == null)
			$this -> deliveryAdressesTable =  new DBTableManager(self::$deliveryAdressesTableName);
	}
	
	/**
	 * Создаёт новый заказ
	 *
	 * @return int
	 */
	public function CreateOrder($orderAttrs)
	{
		$this -> LoadStatuses();
		$this -> InitOrdersTable();
		return $this -> ordersTable -> Insert(array_merge($orderAttrs, array('creationTime' => date("Y-m-d H:i:s"), 'statusID' => $this -> statusesAssoc['new'])));
	}
	
	/**
	 * Функция меняет статус
	 * В качестве нового статуса можно передать его имя или ID 
	 *
	 * @param mixed $newStatus
	 * @param mixed $orderID
	 * @return bool
	 */
	public function ChangeStatus($newStatus, $orderID = null)
	{
		if ($orderID == null) $orderID = $this -> currentOrderID;
		if ($orderID == null) return false;
		$this -> InitOrdersTable();
		$this -> LoadStatuses();

		if (is_numeric($newStatus))
		{
			if (array_search($newStatus, array_values($this -> statusesAssoc)) === false) throw new ArgumentException("Не найден указанный статус");
		}
		elseif (is_string($newStatus))
		{
			if (!isset($this -> statusesAssoc[$newStatus])) throw new ArgumentException("Не найден указанный статус");
			$newStatus = $this -> statusesAssoc[$newStatus];
		}
		if (is_array($orderID))
		{
			for ($i = 0; $i < count($orderID); $i++)
			{
				$this -> ordersTable -> Update(array('statusID' => $newStatus), $orderID[$i]);
			}
		}
		else if (is_numeric($orderID))
			$this -> ordersTable -> Update(array('statusID' => $newStatus), $orderID);
		else throw new ArgumentException("Неверный тип orderID");
		
		return true;
	}
	
	/**
	 * Функция изменяет параметры заказа
	 *
	 * @param array $attrs
	 * @param int $orderID
	 */
	public function UpdateOrder($attrs, $orderID)
	{
		$this -> InitOrdersTable();
		//ChangesLogger::Log($this -> ordersTable, $attrs, $orderID, (isset($_SESSION['userID']) ? $_SESSION['userID'] : null));
		$this -> ordersTable -> Update($attrs, $orderID);
	}	
	
	/**
	 * Добавление товара в корзину
	 *
	 * @param int $itemID
	 * @param int $amount
	 */
	public function AddToCart($itemID, $amount = 1, $withInstallation = false, $price = 0)
	{
		if (!is_numeric($itemID)) throw new ArgumentException("Неверный тип аргумента itemID");
		if ($this -> ordersTable == null) $this -> CreateOrder();
		if ($this -> cartsTable == null) $this -> InitCartsTable();	
		
		return $this -> cartsTable -> Insert(
			array('orderID' => $this -> currentOrderID, 
			      'itemID'  => $itemID, 
				  'amount'  => $amount, 
				  'install' => $withInstallation ? 1:0,
				  'price'   => $price));
	}
	
	/**
	 * Удаляет из тележки предмет
	 *
	 * @param int $cartID
	 */
	public function RemoveFromCart($cartID)
	{
		if (!is_numeric($cartID)) throw new ArgumentException("Неверный тип аргумента");
		if ($this -> cartsTable == null) $this -> InitCartsTable();
		$this -> cartsTable -> Delete(array('cartID' => $cartID));
	}
	
	/**
	 * Возвращает массив, содержащий всю тележку или 
	 * инфу по конкретному Item
	 *
	 * @param int $orderID
	 * @param int $itemID
	 */
	public function GetCart($orderID, $itemID = null)
	{
		if (!is_numeric($orderID)) throw new ArgumentException("Неверный тип аргумента");
		$this -> InitCartsTable();
		$attrs = array('orderID' => $orderID);
		if ($itemID != null) $attrs['itemID'] = $itemID;
		return $this -> cartsTable -> Select($attrs);
	}
	
	/**
	 * Изменить количество какого-нибудь товара
	 *
	 * @param int $cartID
	 * @param int $amount
	 */
	public function ChangeAmount($cartID, $amount)
	{
		if (!is_numeric($cartID) || !is_numeric($amount)) throw new ArgumentException("Неверный тип аргумента");
		$this -> InitCartsTable();
		$this -> cartsTable -> Update(array('amount' => $amount), $cartID); 		
	}
	
	public function GetGoodsTableNameSetting()
	{
		return $this -> settings['goodsTable'];
	}
	
	/**
	 * Функция получает список заказов.
	 * Можно использовать фильтры, а также передавать ID пользователя (менеджера)
	 * Тогда вернутся только те заказы, к которым имеет доступ данный менеджер.
	 * Точнее если этот ID есть  в группе доступа
	 * 
	 * Передавать userID Тока если не админ
	 *
	 * @param unknown_type $filter
	 * @param unknown_type $limit
	 * @param unknown_type $start
	 * @param unknown_type $userID
	 * @param unknown_type $orderBy
	 * @return unknown
	 */
	public function GetOrders($filters = null, $limit = null, $start = null, $orderBy = null)
	{
            if (!empty($filters))
            {
		if (is_string($filters))
                    $filter = ' WHERE '.$filters;
                else if (is_array($filters))
                {
                    $filterParts = array();

                        if (!empty($filters['FIO']))
                        {
                            if (is_string($filters['FIO']))
                                $filters['FIO'] = explode(' ', $filters['FIO']);
                                    
                            $fioFilter = '';
                            for ($i = 0; $i < count($filters['FIO']); $i++)
                            {
                                if (empty($filters['FIO'][$i])) continue;
                                $fioFilter .= ($i == 0 ? ' (' : ' OR '). ' LOWER(u.FIO) LIKE \'%'.$filters['FIO'][$i].'%\' ';
                            }
                            
                            $fioFilter .= ')';
                            $filterParts[] = $fioFilter;
                        }

                        if (!empty($filters['creationDateLeft']))
                            $filterParts[] = ' t.creationTime >= \''.$filters['creationDateLeft'].' 00:00:00\' ';

                        if (!empty($filters['creationDateRight']))
                            $filterParts[] = ' t.creationTime <= \''.$filters['creationDateRight'].' 23:59:59\' ';

                        if (!empty($filters['statuses']))
                        {
                            $statusesFilter = '';
                            for ($i = 0; $i < count($filters['statuses']); $i++)
                                $statusesFilter .= ($i == 0 ? ' (' : ' OR '). ' t.statusID  = '.$filters['statuses'][$i].' ';
                            
                            $statusesFilter .= ')';
                            $filterParts[] = $statusesFilter;
                        }

                        if (!empty($filterParts)){
                            $filter = ' WHERE '.implode(' AND ', $filterParts);
                        }else{
                            $filter = null;
                        }
                }
            }else{
                $filter = null;
            }
		
            $this -> ordersCnt = null;

            $lim = (($limit != null) and (!is_nan($limit))) ? (' LIMIT ' . 
                ((($start == null) || (is_nan($start))) ? 0 : $start) .
                ', ' . $limit) : '';

            if (empty($orderBy)) 
		$orderBy = ' t.orderID DESC ';
            
            $orderBy = ' ORDER BY '.$orderBy; 

            $query = 'SELECT SQL_CALC_FOUND_ROWS * 
                                    FROM `'. self::$ordersTableName .'` t
                                            JOIN '.TablesNames::$USERS_TABLE_NAME.' u
                                            ON (t.customerID = u.userID)
                                    ';

            $query .= $filter.$orderBy.$lim;
            
            try 
            {
                    $res = DB::QueryToArray($query);
                    $cntResult = DB::QueryToArray("SELECT FOUND_ROWS() as cntVal, 'cnt'", "cnt", "cntVal");
                    $this -> ordersCnt = $cntResult['cnt'];
            }
            catch (SQLException $e)
            {
                    throw new Exception("Ошибка в запросе. Невозможно получить заказы.<br/>".$e -> getMessage());
            }
            
            return $res;
	}
	
	/**
	 * Получение заказов конкретного пользователя
	 *
	 * @param array $attributes
	 * @param int $userID
	 * @return array
	 */
	public function GetOrdersForUser($attributes = null, $userID)
	{
		$filter = "";
		if ($attributes != null)
		{
			if (!is_array($attributes)) 
				throw new ArgumentException("Неверный тип аргумента");
			
			$fieldsNames = $this -> ordersTable -> TableStructure -> GetFieldsNames();
				
			$condArray = array();
			foreach ($attributes as $field => $value)
			{
				// Проверяем, есть ли поле $field в таблице
				if (in_array($field, $fieldsNames))
				{
					$condArray[] = "`$field` = '$value'";
				}
			}
			
			if (!empty($condArray))
				$filter = " WHERE ".implode(" AND ", $condArray);
		}
		if (!empty($filter)) $filter .= " AND ";
		
		$query = "SELECT SQL_CALC_FOUND_ROWS * FROM `". self::$ordersTableName ."` o
					WHERE $filter
					o.customerID = $userID
					GROUP BY o.orderID 
					ORDER BY o.orderID DESC
					$lim";
		try 
		{
			$res = DB::QueryToArray($query);
			$cntResult = DB::QueryToArray("SELECT FOUND_ROWS() as cntVal, 'cnt'", "cnt", "cntVal");
			$this -> ordersCnt = $cntResult['cnt'];
		}
		catch (SQLException $e)
		{
			throw new Exception("Ошибка в запросе. Невозможно получить заказы для данного пользователя.<br/>".$e -> getMessage());
		}
		return $res;
	}
	
	public function GetOrder($orderID)
	{
		if (!is_numeric($orderID) || empty($orderID))
			throw new ArgumentException('ID заказа должен быть числом', 10122);

		$query = 'SELECT * FROM '.self::$ordersTableName.' o
				 JOIN '.TablesNames::$USERS_TABLE_NAME.' u ON (u.userID = o.customerID)
				 /*LEFT JOIN '.self::$deliveryAdressesTableName.' d ON (o.deliveryAdressID = d.deliveryAdressID)*/
				 WHERE o.orderID = '. $orderID;
                
                $res = DB::QueryToArray($query);
                
		return array_pop($res);
	}
	
	public function CountOrders($attrs = null)
	{
		if ($this -> ordersCnt == null)
		{
			$this -> InitOrdersTable();
			return $this -> ordersTable -> Count($attrs);
		}
		else
		{
			return $this -> ordersCnt;
		}
	}
	
	/**
	 * Проверяет только менеджеров и заказчиков.
	 * Админа не пустит. Админа проверять до вызова функции.
	 *
	 * @param int $userID
	 * @param int $orderID
	 */
	public function CheckUserAccess($userID, $orderID)
	{
		$query = "SELECT * FROM `". self::$ordersTableName ."` o
						
						WHERE o.customerID = $userID
						AND o.orderID = $orderID GROUP BY o.orderID";
		$qr = DB::Query($query);
		if (DB::ReturnedRows($qr) > 0) return true;
		else return false;
	}
	
	/**
	 * Получаем корзину с родительскими разделами
	 *
	 * @param int $orderID
	 */
	public function GetFullCart($orderID)
	{
		$goodsTableName = $this -> settings['goodsTable'];
		if (empty($goodsTableName))
			throw new Exception('Не найдена настройка имени таблицы с товарами', 101221);
/*
		$query = 'SELECT c.*, g.*, ps2.`caption`
			 FROM `'. self::$cartTableName .'` c
				JOIN (`'. $goodsTableName .'` g,
					  `'. TablesNames::$PAGE_STRUCTURE_TABLE_NAME .'` ps,
					  `'. TablesNames::$PAGE_STRUCTURE_TABLE_NAME .'` ps2,
					  `'. TablesNames::$PAGES_TYPE_TABLE_NAME .'` pt)
					ON (c.`itemID` = g.`itemID` 
					AND ps.`contentID` = c.`itemID`
					AND ps2.`pageID` = ps.`parentID`
					AND ps.pageTypeID = pt.pageTypeID)
					WHERE c.orderID = '. $orderID .'
						AND pt.`constantName` = \'GOOD_TYPE_ID\' ';
						*/
		$query = 'SELECT c.*, g.*
			 FROM `'. self::$cartTableName .'` c
				JOIN (`'. $goodsTableName .'` g)
					ON (c.`itemID` = g.`itemID`)
					WHERE c.orderID = '. $orderID ;               
                
		return DB::QueryToArray($query);
	}

	
	/**
	 * Редактирование списка товаров заказа
	 *
	 * @param array $cart
	 * @param int $orderID
	 */
	public function EditOrderCart(
		/**
		 * Массив товарров заказа. Ключ - ID товара, значение - количество
		 */
		$cart, $orderID)
	{
		// Проверяем ID заказа на корректность
		if (!is_numeric($orderID))
			throw new ArgumentException("Неверно указан ID заказа");
		
		$this -> InitOrdersTable();
				
		$order = $this -> ordersTable -> Select( array('orderID' => $orderID), 1, 0);
		if (count($order) == 0)
			throw new ArgumentException("Заказа с указанным ID [$orderID] не существует");
		
		$this -> InitCartsTable();
		
		$currentOrderCart = $this -> cartsTable -> Select(	array('orderID' => $orderID) );
		
		foreach ($currentOrderCart as $entry)
		{
			$goodID = $entry['itemID'];
			$entryID = $entry['entryID'];
			if (isset($cart[$goodID]))
			{
				if (($cart[$goodID] != $entry['amount']) && (is_numeric($entry['amount'])))
				{
					$attrs = array(	'orderID' 	=> $orderID, 
									'itemID'	=> $goodID,
									'amount'	=> $cart[$goodID]);
					
					//$this -> ordersDishesTable -> Insert($attrs);
				//	ChangesLogger::Log($this -> ordersDishesTable, $attrs, $entryID, (isset($_SESSION['userID']) ? $_SESSION['userID'] : null));
					$this -> cartsTable -> Update( $attrs, $entryID );
				}
				unset($cart[$goodID]);
			}
		}
		
		// Добавляем новые товары в заказ
		foreach ($cart as $goodID => $amount)
		{
			if ( !is_numeric($goodID) || !is_numeric($amount) || ($amount == 0) )
				continue;
			$attrs = array(	'orderID' 	=> $orderID, 
							'itemID'	=> $goodID,
							'amount'	=> $amount);
			
			$this -> cartsTable -> Insert($attrs);
		}
	}
	
	public function DeleteOrder($orderID)
	{
		if (!is_numeric($orderID))
			throw new ArgumentException('ID заказа должен быть числом', 10100);
		$this -> InitOrdersTable();
		$attrs = array('orderID' => $orderID);
		$this -> ordersTable -> Delete($attrs);
		$this -> InitCartsTable();
		$this -> cartsTable -> Delete($attrs, true);
	}
	
	public function DeleteOrders($ordersIDs)
	{
		for ($i = count($ordersIDs) - 1; $i >= 0; $i--)
		{
			$this -> DeleteOrder($ordersIDs[$i]);
		}
	}
}

?>
/**
*  Инициализатор контрола RichText
*/
function RichTextInit(initObject)
{
    $('textarea.fck').each(function() {
        var that = $(this);
        var parts = that.attr("class").split(" ");
        that.fck({
			BasePath: '/publicLibs/fckeditor/',
			ToolbarSet: parts[1],
			Width: initObject.width,
			Height: initObject.height
		})
    });
}

/*
    var oFCKeditor = new FCKeditor('{name}');
    oFCKeditor.BasePath = \"/libs/fckeditor/\";
    oFCKeditor.Width = \"{width}\";
    oFCKeditor.Height = \"{height}\";
    oFCKeditor.Value = '{value}';
    oFCKeditor.ToolbarSet = '{toolbarSet}';
    oFCKeditor.Create();
*/
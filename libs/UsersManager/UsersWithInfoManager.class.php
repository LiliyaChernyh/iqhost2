<?php

require_once('UsersWithRolesManager.class.php');

/**
 * Класс работы с пользователями, у которых есть дополнительная таблица с информацией.
 *
 * @version 1.1
 */
class UsersWithInfoManager extends UsersWithRolesManager 
{
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Enter description here...
	 *
	 * @return UsersWithInfoManager
	 */
	public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }
    
    private $usersCnt = null;
	
    /**
     * Возвращает информацию о пользователе из таблицы UsersInfo
     *
     * @param int $userID
     * @return array
     */
	public function GetInfo($userID)
	{
	    // Общая информация
	    $query = "
            SELECT  *
            FROM    `". TablesNames::$USERS_INFO_TABLE_NAME ."` ui
            WHERE   ui.`userID` = ". $userID ."
	    ";
	    
	    $result = DB::QueryToArray($query);
	    
	    if (empty($result))
	        return null;
	        
        $info = array_shift($result);
        
        return $info;
	}
	
	/**
	 * Возвращает массив с информацией о пользователях.
	 * Можно фильтровать пользователей по полям талицы Users
	 *
	 * @param array $filters
	 * @param int $limit
	 * @param int $start
	 * @return array
	 */
	public function GetUsersWithMainInfo($filters = null, $limit = null, $start = null)
	{
	    $lim = (!empty($limit) && is_numeric($limit)) ? (' LIMIT ' . 
                    ((!is_numeric($start)) ? 0 : $start) .
                    ', ' . $limit) : '';
        
        $condition = '';
        $joins = '';
        
        if (!is_null($filters))
        {
            $condArray = array();
            $joinsArray = array();
            foreach ($filters as $filter => $value)
            {
                switch ($filter)
                {
                    case 'name':
                        if (!empty($value))
                        {
                            $value = UTF8::strtolower($value);
                            $condArray[] = 'LOWER(u.`FIO`) LIKE ' . DB::Quote('%'. $value .'%');
                        }
                        break;
                        
                    case 'login':
                        if (!empty($value))
                        {
                            $value = UTF8::strtolower($value);
                            $condArray[] = 'LOWER(u.`login`) LIKE ' . DB::Quote('%'. $value .'%');
                        }
                        break;
                        
                    case 'email':
                        if (!empty($value))
                        {
                            $value = UTF8::strtolower($value);
                            $condArray[] = 'LOWER(u.`email`) LIKE ' . DB::Quote('%'. $value .'%');
                        }
                        break;
                        
                    case 'role':
                        if (!empty($value) && $value != 'all')
                        {
                            $condArray[] = "r.`role` = ". DB::Quote($value);
                        }
                        break;
                        
                    case 'activity':
                        switch ($value)
                        {
                            case 'active':
                                $condArray[] = "(u.`isActive` = 1)";
                                break;
                                
                            case 'inactive':
                                $condArray[] = "(u.`isActive` = 0)";
                                break;
                        }
                        break;
                        
                    case 'curRole':
                        if (!empty($value))
                        {
                            $condArray[] = "r.`level` <= ". $value;
                        }
                        break;
                }
            }
            if (!empty($condArray))
                $condition = ' WHERE '. implode(' AND ', $condArray);
            if (!empty($joinsArray))
                $joins = ' '. implode(' ', $joinsArray);
        }
        
        $query = "
            SELECT SQL_CALC_FOUND_ROWS DISTINCT     
                        u.*, r.`name` as roleName
            FROM        `". TablesNames::$USERS_TABLE_NAME ."` u
            LEFT JOIN        `". TablesNames::$USERS_INFO_TABLE_NAME ."` ui
                ON      u.`userID` = ui.`userID`
            JOIN        `". TablesNames::$ROLES_TABLE_NAME ."` r
                ON      u.`role` = r.`role`
            ". $joins ."
            ". $condition ."
            ORDER BY    r.`level` DESC, u.`login`
        ". $lim;
	    $result = DB::QueryToArray($query);
	    $this -> usersCnt = DB::QueryOneValue("SELECT FOUND_ROWS()");
	    
	    return $result;
	}
	
	public function CountUsers()
	{
	    return $this -> usersCnt;
	}
}

?>
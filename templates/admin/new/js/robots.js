$(document).ready(function ()
{
	$(document).ajaxError(function(event, request, settings){$.jGrowl('AJAX Error')});

	$('#robotsEdit').submit(function() {
		$(this).ajaxSubmit({success:saveFormResponse, dataType:"json"});
		return false;
	});
	

});

function saveFormResponse(data, status)
{
	if (!standartAJAXResponseHandler(data, status)) return;
	$.jGrowl(data.text, {header: data.msg});
}

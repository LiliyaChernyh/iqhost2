<header id="header">
    <div class="top-panel">
        <div class="holder">
            <div class="logo"><a href="/"><img src="{$IMAGES_ALIAS}/logo.svg" alt="IQ Host" width="162" height="115"></a></div>
            <div class="bar">
                <div class="info">
                    {if isset($SETTINGS.front.phone) && !empty($SETTINGS.front.phone)}<a href="tel:{$SETTINGS.front.phone.SMALL}">{$SETTINGS.front.phone.ccode} ({$SETTINGS.front.phone.tcode}) <strong>{$SETTINGS.front.phone.body}</strong></a>{/if}
                    {if isset($SETTINGS.front.lk) && !empty($SETTINGS.front.lk)}<a href="{$SETTINGS.front.lk}" target="_blank" class="btn">Личный кабинет</a>{/if}
                </div>
               {include file="navigation.tpl"}
            </div>
            <a href="#" class="nav-opener"><span></span></a>
        </div>
    </div>
    <div class="heading">
        <span>Запустить проект</span>
    </div>
    {include file="navigation-inner.tpl"}
</header>
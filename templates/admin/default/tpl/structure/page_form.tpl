<h1>{$HEADER}</h1>
{if !empty($RIGHTBLOCK_ENABLED)}
    <div id="rightblock">
        {if isset($MODULE_LINK)}
        <div class="linkButton">
            <a href="{$MODULE_LINK}"><img src="{$ADMIN_IMAGES}12-em-pencil.png" alt="Переидти к молулю" class="pictogramm" />[ <span style="text-decoration:underline">Управление модулем</span> ]</a>
        </div>
        {/if}

        {if !empty($DELETE_ENABLED)||!empty($CLONE_ENABLED)}
        <div class="linkButton">
            {if !empty($DELETE_ENABLED)}
            <a href="{$DELETE_SCRIPT}" onclick="return AreYouSure();" title="Переместить в корзину | Удалить"><img src="{$ADMIN_IMAGES}delete_icon.png" alt="Переместить в корзину | Удалить" class="pictogramm" />[ <span style="text-decoration:underline">Удалить страницу</span> ]</a>
            {/if}
            {if !empty($CLONE_ENABLED)}
            <br/><br/>
            <a href="{$CONTROLLER_ALIAS}clonepage/{$PAGE.pageID}/"><img src="{$ADMIN_IMAGES}icons/page_copy.png" alt="Переместить в корзину | Удалить" class="pictogramm" />[ <span style="text-decoration:underline">Клонировать страницу</span> ]</a>
            {/if}
        </div>
        {/if}
    </div>
{/if}

<form action="" method="post" enctype="multipart/form-data" id="pageForm">
  {if isset($PAGE.pageID)}
  <div> <input type="hidden" name="pageID" value="{$PAGE.pageID}" /></div>
  {/if}
	<div class="formBlockHeader">Служебные параметры</div>

	<div class="formBlock">
		<div class="formElement">
			<label for="page_caption">Название страницы <span class="smallLightGrey" style="display:none; margin-left:10px;">(<span id="page_caption_count"></span>)</span>
					<span class="helper" title="Служебный параметр. Название будет отображаться в списках.">?</span>
			</label>
			<br />
			<input type="text" id="page_caption" name="page_caption" class="required" size="80" value="{$PAGE.caption}" maxlength="255" /><sup style="color:red">*</sup>
		</div>

		<div class="formElement">
			<label for="page_alias">Псевдо-URL страницы {if !empty($ALIAS_READONLY)}<span class="smallLightGrey">(Неизменяем)</span>{/if}<span class="smallLightGrey" style="display:none; margin-left:10px;">(англ, <span id="page_alias_count"></span>)</span>
				<span class="helper" title="Адрес страницы, набираемый в адресной строке браузера.">?</span>
			</label>
			<br />
			<input type="text" id="page_alias" name="page_alias" size="80" title="выводится в адресе сайта" value="{$PAGE.alias}" maxlength="255" {if !empty($ALIAS_READONLY)}readonly="readonly"{/if} />
		</div>

		<div class="formElement">
			<label for="page_parerntID">Родительский раздел <span class="helper" title="Какому разделу принадлежит страница.">?</span>
			</label>
			<br />
			<select name="page_parentID" id="page_parerntID" style="width:507px;">
    			<option value="0"> - </option>
    			{foreach from=$PARENT_OPTION item=OPTION}
    			<option value="{$OPTION.value}"{if $OPTION.value == $PAGE.parentID} selected="selected"{/if}>{$OPTION.text}</option>
                {/foreach}
			</select>
		</div>

		<div class="formElement">
			<label for="page_pageTypeID">Тип страницы <span class="helper" title="От типа страницы зависит её содержимое.  К странице может быть подключён модуль.  В зависимости от типа меняется форма редактирования   содержимого (появляется ниже).">?</span>
			</label><br />
			<select name="page_pageTypeID" id="page_pageTypeID" {if !empty($DISABLE_PAGE_TYPES_OPTION)}disabled="disabled"{/if}>
				<option value="0">  </option>
				{foreach from=$PAGE_TYPES_OPTION item=OPTION}
				<option value="{$OPTION.pageTypeID}"{if $PAGE.pageTypeID == $OPTION.pageTypeID} selected="selected"{/if}>{$OPTION.description}</option>
				{/foreach}
			</select><span id="loadBar" style="display:none"><img src="{$ADMIN_IMAGES}loading.gif" style="margin:0 3px -3px 5px" alt="Загрузка"/>Загрузка</span>
		</div>

		<div style="height:30px; margin-top:25px">
			<div class="formInlineElement">
			    <input type="hidden" name="page_isSection" value="0" />
				<input type="checkbox" title="Поставьте галку, чтобы страница  стала родительским разделом." name="page_isSection" id="page_isSection" style="cursor:pointer" value="1" {if $PAGE.isSection}checked="checked"{/if} />
				<label for="page_isSection"  title="Поставьте галку, чтобы страница  стала родительским разделом.">Раздел</label>
			</div>

			<div class="formInlineElement">
			    <input type="hidden" name="page_isActive" value="0" />
				<input type="checkbox" title="Уберите галку, если хотите, чтобы страница не отображалась на сайте" name="page_isActive" id="page_isActive" style="cursor:pointer" value="1" {if $PAGE.isActive}checked="checked"{/if}/>
				<label for="page_isActive" title="Уберите галку, если хотите, чтобы страница не отображалась на сайте" >Активна?</label>
			</div>

			<div class="formInlineElement">
				<label for="page_priority">Порядок
				</label>
				<input type="text" size="5" value="{$PAGE.priority}" name="page_priority" id="page_priority" />
				<span class="helper" title="Число определяет порядок вывода страницы в списках.  Чем больше число, тем выше страница в списке.">?</span>
			</div>
		</div>


                <div style="height:30px; margin-top:10px">
                        {if !empty($SHOW_MAINMENU_CHECKBOX)}
                        <div class="formInlineElement">
                                <input type="checkbox" title="Поставьте галку, чтобы страница попала в главное меню." name="showInMainMenu" id="showInMainMenu" style="cursor:pointer" value="1" {if !empty($SHOW_PAGE_IN_MAIN_MENU)}checked="checked"{/if} />
                                <label for="showInMainMenu"  title="Поставьте галку, чтобы страница попала в главное меню.">Включить в главное меню</label>
                        </div>
                        {/if}

                        <div class="formInlineElement">
                            <input type="hidden" name="page_isLogin" value="0" />
                                <input type="checkbox" title="Уберите галку, если хотите, чтобы страница была доступна всем" name="page_isLogin" id="page_isLogin" style="cursor:pointer" value="1" {if $PAGE.isLogin}checked="checked"{/if}/>
                                <label for="page_isLogin" title="Уберите галку, если хотите, чтобы страница была доступна всем" >Запаролить</label>
                        </div>

                        <div class="formInlineElement">
                            <input type="hidden" name="page_isNoindex" value="0" />
                                <input type="checkbox" title="Уберите галку, если хотите, чтобы страница была доступна всем" name="page_isNoindex" id="page_isNoindex" style="cursor:pointer" value="1" {if $PAGE.isNoindex}checked="checked"{/if}/>
                                <label for="page_isNoindex" title="Уберите галку, если хотите, чтобы страница была доступна всем" >noindex</label>
                        </div>
                </div>

	</div>

    <div id="contentFormContainer" {if !empty($HIDE_CONTENT_FORM)}style="display:none;"{/if}>
		{if !isset($FORM_BLOCKS)}
			<div class="formBlockHeader">Содержимое</div>
		{else}
		    {* блоки форм с JS *}
			{include file='formBlocks.tpl'}
		{/if}
	</div>

	<div class="formBlockHeader">Мета-информация</div>
	<div class="formBlock">
		<div class="formElement">
			<label for="page_title">Заголовок окна браузера
			<span class="smallLightGrey" style="display:none; margin-left:10px;">(<span id="page_title_count"></span>)</span>
			<span class="helper" title="Для поисковых систем.  Выводится в заголовке окна браузера">?</span>
			</label><br />
			<input type="text" id="page_title" name="page_title" size="95" value="{$PAGE.title}" maxlength="255" />
		</div>

		<div class="formElement">
			<label for="page_description">Описание
			<span class="smallLightGrey" style="display:none; margin-left:10px;">(<span id="page_description_count"></span>)</span>
			<span class="helper" title="Для поисковых систем.  Выводится в META-теге description">?</span>
			</label><br />
			<input type="text" id="page_description" name="page_description" size="95" value="{$PAGE.description}" maxlength="255" />
		</div>

		<div class="formElement">
			<label for="page_keywords">Ключевые слова через запятую
			<span class="smallLightGrey" style="display:none; margin-left:10px;">(<span id="page_keywords_count"></span>)</span>
			<span class="helper" title="Для поисковых систем.  Выводится в META-теге keywords">?</span>
			</label><br />
			<input type="text" id="page_keywords" name="page_keywords" size="95" value="{$PAGE.keywords}" maxlength="255" />
		</div>
	</div>

	<div style="clear: both; text-align: center;"><input type="submit" class="handyButton" value="Сохранить" name="{$SUBMIT_NAME}" /></div>
	</form>

	<div id="submitResult1" style="float:left; display:none;">Страница успешно сохранена. Вы можете <a href="">продолжить редактирование</a>.</div>
	<div id="submitResult2" style="float:left; display:none;">Страница успешно сохранена.<br/>Необходима перезагрузка формы.<br/>Она произойдёт автоматически или вы можете <a href="">перезагрузить её сами</a>.</div>
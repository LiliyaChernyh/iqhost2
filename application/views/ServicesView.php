<?php

class ServicesView extends FrontView {

    public function __construct() {
        parent::__construct();
        $this->tplsPath .= 'services/';
    }

    public function Index() {
        $this->tpl->contentTpl = $this->tplsPath . 'index.tpl';
        return $this;
    }
    
    public function Domains() {
        $this->tpl->contentTpl = $this->tplsPath . 'domains.tpl';
        return $this;
    }
    
    public function Hosting() {
        $this->tpl->contentTpl = $this->tplsPath . 'hosting.tpl';
        return $this;
    }
    
    public function Servers() {
        $this->tpl->contentTpl = $this->tplsPath . 'servers.tpl';
        return $this;
    }
    
    public function Service() {
        $this->tpl->contentTpl = $this->tplsPath . 'service.tpl';
        return $this;
    }    
    
    public function Mail() {
        $this->tpl->contentTpl = $this->tplsPath . 'mail.tpl';
        return $this;
    }
}

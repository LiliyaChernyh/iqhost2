<?php

require_once(IncPaths::$LIBS_PATH .'Cache/ICache.interface.php');

/**
 * Кеширующий класс
 *
 * @version 1.0
 */
class Cache implements ICache 
{
    /**
     * Создает объект self::$driver соответствующего класса
     *
     * @param string $driverName
     */
    public static function Init()
    {
        $args = func_get_args();
        $driverName = isset($args[0]) ? $args[0] : self::$defaultDriverName;
        
        // Генерируем название класса
        $driverClassName = ucfirst($driverName) .'Cache';
        
        if (!is_null(self::$driver) && (get_class(self::$driver) == $driverClassName))
            return;
        
        // Генрируем название файла драйвера
        $driverFileName = $driverClassName .'.class.php';
        
        // Генерируем полный путь к файлу драйвера
        $driverPath = IncPaths::$LIBS_PATH .'Cache/Drivers/'. $driverFileName;
        
        // Пытаемся загрузить файл
        if (!Autoload::TryLoad($driverPath))
            throw new Exception('Указанный драйвер не сущестует');
            //throw new Exception('Указанный драйвер не сущестует', 'cache.driverNotFound', $driverName);

        if (!in_array('ICacheDriver', class_implements($driverClassName, false)))
            throw new Exception('Указанный драйвер не валиден');
            //throw new Exception('Указанный драйвер не валиден', 'cache.wrongDriver', $driverName);
             
        // Создаем класс
        self::$driver = new $driverClassName();
    }
    
    /**
     * Драйвер кеширования
     *
     * @var ICacheDriver
     */
    public static $driver = null;
    
    /**
     * Драйвер по умолчанию
     *
     * @var string
     */
    public static $defaultDriverName = 'Files';
    
    /**
     * Время жизни закешированного объекта по умолчанию (в секундах)
     * 0 - без ограничений
     *
     * @var int
     */
    public static $lifetime = 0;
    
    /**
     * Сохраняет данные в кеше
     *
     * @param string $dataID
     * @param mixed $data
     * @param int $lifetime
     * @return bool
     */
    public static function Save($dataID, $data, $tags = null, $lifetime = null)
    {
        // Инициализируем класс
        self::Init();
        
        // Если некорректно задан lifetime, берем значение по умолчанию
        if (is_null($lifetime) || !is_numeric($lifetime) || $lifetime < 0)
            $lifetime = self::$lifetime;

        self::$driver -> Save($dataID, $data, $tags, $lifetime);
    }
    
    /**
     * Берет данные из кеша, если они там есть
     *
     * @param string $dataID
     * @return mixed
     */
    public static function Load($dataID)
    {
        // Инициализируем класс
        self::Init();
        
        self::$driver -> Load($dataID);
    }
    
    /**
     * Возвращает все данные из кеша, помеченные указанными тегами
     * Передавать можно как массив тегов, так и строку с одним тегом
     *
     * @param array|string $tags
     */
    public static function Find($tags)
    {
        // Инициализируем класс
        self::Init();
        
        self::$driver -> Find($tags);
    }
    
    /**
     * Удаляет данные из кеша
     *
     * @param string $dataID
     * @return bool
     */
    public static function Delete($dataID)
    {
        // Инициализируем класс
        self::Init();
        
        self::$driver ->  Delete($dataID);
    }
    
    /**
     * Удаляет данные из кеша, помеченные указанными тегами
     * Передавать можно как массив тегов, так и строку с одним тегом
     *
     * @param array|string $tags
     * @return bool
     */
    public static function DeleteByTags($tags)
    {
        // Инициализируем класс
        self::Init();
        
        self::$driver -> DeleteByTags($tags);
    }
}

?>
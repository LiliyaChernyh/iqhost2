$(document).ready(function()
{
    if (!$.browser.opera)
        $("#container").css("margin-top", ($(window).height() / 2 - 200) + "px");
    else
        $("#container").css("margin-top", (document.documentElement["clientHeight"] / 2 - 200) + "px"); 
    
    $("#contentForm").ajaxForm({
            success:       passwordChangeSubmitHandler,
            dataType:	   "json"
    });
    
    $("#captchaImg").css("cursor","pointer").click(function(){
        $(this).attr("src", $(this).attr("src") + "?" + Math.random());
    }) 
});

function passwordChangeSubmitHandler(data, status)
{
    if (data.error){
        if (data.error == 'login'){
            $('#login').parent().addClass('error').removeClass('success');
            $('#message').hide().html(data.text).addClass('error').removeClass('success').fadeIn();
        }else if (data.error == 'email'){
            $('#email').parent().addClass('error').removeClass('success');
            $('#message').hide().html(data.text).addClass('error').removeClass('success').fadeIn();
        }else if (data.error == 'captcha'){
            $('#captcha').parent().addClass('error').removeClass('success');
            $('#message').hide().html(data.text).addClass('error').removeClass('success').fadeIn();
        }
        else{
            $('#message').hide().html(data.text).addClass('error').removeClass('success').fadeIn();
        }
        
        $("#captchaImg").trigger('click');
    }else if(data.msg == 'ok'){
        $('#message').hide().html(data.text).addClass('success').removeClass('error').fadeIn();
        $("#contentForm").slideUp();
    }
}
			<h1>Создание заказа</h1>
			{if $NO_GROUP_WARNING}
				<h1 style="color: #AA5566">Вы не сможете создать заказ, поскольку не принадлежите ни одной из групп менеджеров!</h1>
			{/if}
			<form action="{$ORDER_FORM_ACTION}" method="post">
			<table width="100%">
				<tr>
					<td style="border-right:none">Дата проведения банкета:</td>
					<td style="border-left:none">
					<input type="text" id="banquetDate" name="banquetDate"  title="Дата проведения банкета." value="{$BANQUETDATE}" />
  					<img src="/images/calendar.png" id="triggerbanquetDate" style="cursor: pointer; margin: 0 0 -3px 0;" title="Выбрать дату" />
  
					<script type="text/javascript">
  					Calendar.setup(
	    			{
		      			inputField		:	"banquetDate",         // ID of the input field
		      			ifFormat		:	"%d.%m.%Y %H:%M",    // the date format
		      			showsTime		:	true,
		      			firstDay 		:	1,
		      			button			:	"triggerbanquetDate"       // ID of the button
		    			}
	  				);
					</script>
					</td>
					<td style="border-right:none">Число гостей:</td>
					<td style="border-left:none"><input type="text" name="guestsNumber" title="Количество гостей" value="{$GUESTSNUMBER}" />
					</td>
				</tr>
				<tr>
					<td style="border-right:none">Номера столов:</td>
					<td style="border-left:none"><input type="text" name="tablesNumbers" title="Номера столов. Перечислены через запятую." value="{$TABLESNUMBERS}" /></td>
					<td style="border-right:none">Задаток:</td>
					<td style="border-left:none"><input type="text" name="prePayment" title="Сумма задатка (предварительного платежа)." value="{$PREPAYMENT}" /></td>
				</tr>
			</table>
			<ul style="margin-left: 0">
		   		<li>Имя зарегистрированного гостя:
		   		<input type="text" id="userLogin" name="userLogin" value="" onkeyup="ajax_showOptions(this,'getClients','{$CURRENTUSER_USERID}',event)">
				<input type="hidden" id="userLogin_hidden" name="userID">
				
		   		<!--input type="text" name="userLogin" value="{$USERLOGIN}" style="margin: 0 10px 0 128px;" /-->
		   		{if $USER_LOGIN_INCORRECT}
		   		<span style="color: #AA5566">Login не найден</span>
		   		{/if}
		   		<span style="margin-left: 20px">[<a href="/users/add/" target="_blank">Зарегистрировать нового гостя</a>]</span>
		   		<span style="margin-left: 20px">[<a href="/users/list/" target="_blank">Список гостей</a>]</span>
		   		</li>
				{if $MANAGER_VIEW}
				<li>Принять заказ&nbsp;&nbsp;&nbsp;<input type="checkbox" name="doAcceptOrder" />
				<span style="margin-left:10px; font-weight:bold" title="Поставьте галку, если за этот заказ отвечаете вы.">?</span>
				{/if}
				<!--input type="hidden" name="responsibleManagerID" value="{$RESPONSIBLEMANAGERID}" style="margin-left: 57px" /-->
				
				{if $ADMIN_VIEW}
				<li>Ответственный менеджер:
				<select name="responsibleManagerID" style="margin-left: 61px">
				<option value="NULL">Выбрать менеджера</option>
				{$MANAGERS_OPTIONS}
				</select>
				</li>
				{/if}
				{if $MANAGERS_GROUPS}
				<li>Группа менеджеров: <select name="groupID" style="margin-left: 100px"> 
				{$MANAGERS_GROUPS}
				</select>
				<span style="margin-left:10px; font-weight:bold" title="Группа менеджеров,  имеющих доступ к заказу.    Ответсвенный менеджер может  не принадлежать этой группе.">?</span>
				</li>
				{/if}
				
				</li>
				<li>Метка:
					<input type="text" name="tip" size="100" maxlength="255" value="{$TIP}" style="margin-left: 55px" /><span style="margin-left:10px; font-weight:bold" title="Метка заказа отображается в общем списке заказов.  Это маленький комментарий к заказу.">?</span>
				</li>
			</ul>
			<p><h3>Дополнительные услуги</h3>
				<table id="additionalServicesTable">
				<thead>
				<tr>
				<th>Наименование услуги</th>
				<th width="13%">Цена / % <span style="margin-left:10px; font-weight:bold; cursor: pointer" title="Можно указать процент от  итоговой суммы корзины  заказа, например,  10% чаевых">?</span></th>
				<th width="1%">&nbsp;</th>
				</tr>
				</thead>
				<tbody>

				</tbody>
				</table>
				<script type="text/javascript">
				addAdditionalService(); 
				ge('edit_dropdown_text0').value="Обслуживание"; 
				ge('additionalServiceCost0').value="10%"; 
				</script>
				<a href="#" onclick="return addAdditionalService()">[ добавить ]</a>  
			</p>
			<div style="clear:left; margin: 10px 0">&nbsp;</div>
			<input type="Submit" value="Создать заказ" name="doCreateOrder" style="margin: -10px auto; display:block;" class="handyButton" {if $NO_GROUP_WARNING}disabled{/if}/>
		</form>
			
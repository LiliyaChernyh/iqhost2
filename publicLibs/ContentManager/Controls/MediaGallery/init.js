function MediaGalleryInit(initObject)
{
	var name = initObject.name;
	var mediaGallery = $("." + name + " table");
	var initMediaGalleryRow = $("." + name + " table .default").clone().removeAttr("class");
	$("." + name + " table .default").remove();

	$("#" + name + "_add").live('click', function() {
//		var defaultRow = $("." + name + " table .default");
//		defaultRow.clone().removeAttr("class").appendTo(defaultRow.parent()).show();
//		console.log(initObject);
		initMediaGalleryRow.clone().appendTo(mediaGallery).show();
		return false;
	});
}

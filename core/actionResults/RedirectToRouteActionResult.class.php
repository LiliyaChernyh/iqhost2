<?php
/**
 * Результат работы контроллера в виде редиректа на другой контроллер
 * 
 * @version 1.0
 *
 */
class RedirectToRouteActionResult extends ActionResult
{
    /**
     * Путь
     *
     * @var Route
     */
    private $route;

    public function __construct($route)
    {
       $this -> route = $route;
    }

    public function ExecuteResult()
    {
        if (is_null($this -> route))
            return;

        $controllerName = $this -> route -> GetControllerName();
        $actionName = $this -> route -> GetActionName();

        if (!class_exists($controllerName))
            throw new PageNotFoundException('Не найден класс контроллера '.$controllerName);

        $controller = new $controllerName();

        // Инициализация контроллера
        $actionResult = $controller -> Init();
        $actionResult -> ExecuteResult();

        if ($actionResult -> IsExecutionStopped())
            return;

        // Выполнение нужного действия
        if (!method_exists($controller, $actionName))
            throw new PageNotFoundException('Не найден метод контроллера '.$actionName);

        $actionResult = $controller -> $actionName($this -> route -> GetArgs());
        if (empty($actionResult))
            throw new Exception('Вызываемый метод контроллера должен возвращать объект класса ActionResult');
            
        // Вызываем "пост-обработчик" контроллера
        $controller -> Complete();
        
        $actionResult -> ExecuteResult();
    }
}
?>
$(document).ready(function()
{ 
    $('input[type=submit], input[type=button], input[type=image], .helper').addClass('handyButton');
    $('a, img, input, textarea, .helper').tooltip({showURL: false});

    $(".paginator").each(assignPaginator);
    $(".ddm").dropDownMenu();

    $('.goToBtn').click(LinkBtnClick);
	$(".openInNewWin").click(function (event) {
		event.preventDefault(); 
		event.stopPropagation();
		window.open($(this).attr("href"));
	})

    $(".jsLink").click(function(e) {e.preventDefault();});
    $('.richEditor').nicEditor();

    $(".maintenanceForm #maintenancePageText").each(function() {
        $( this ).fck({BasePath: '/publicLibs/fckeditor/'});
    });
});

function standartAJAXResponseHandler(data, status)
{
	if (status == 'success')
	{
		if (data.error)
		{
			$.jGrowl(data.error);
			return false;
		}
	}
	else
	{
		$.jGrowl("Ошибка запроса");
		return false;
	}
	return true;
}

function standartAJAXBeforeSubmit(form, jObject, options)
{
	form.push({name:"async",value:"1"});
}

function LinkBtnClick()
{
    var btnID = $(this).attr('id');
    var link = $('#' + btnID + '_link').val();
    if (typeof link == 'undefined')
        return;
        
    window.location = link;
}

function assignPaginator()
{
	if (typeof $.fn.pagination == "undefined") return;
	var that = $(this);
	var links = that.children("a");
	var firstLink = links.eq(0).attr("href");
	var secondLink = links.eq(1).attr("href");
	if (!firstLink || !secondLink || firstLink.length != secondLink.length)
	{
		that.append("Должны быть заданы первые две ссылки. Их длины должны совпадать.");
		return;
	}
	var linkPrefix = "";
	var linkPostfix = "";
	var pref = true;
	for (var i = 0; i < firstLink.length; i++)
	{
		if (firstLink.charAt(i) == secondLink.charAt(i) && pref)
			linkPrefix += firstLink.charAt(i);
		else if (!pref)
			linkPostfix += firstLink.charAt(i);
		else
			pref = false;
	}
	var spans = that.children("span");
	var pagesTotal = spans.eq(2).text();
	var itemsOnPage = spans.eq(0).text();
	var current = spans.eq(1).text();
	that.pagination(pagesTotal*itemsOnPage, 
	{
		items_per_page:itemsOnPage,
		num_display_entries: 30,
		current_page:current,
		link_to:linkPrefix+"__id__"+linkPostfix,
		next_text:"След.",
		prev_text:"Пред.",
		callback:paginatorLinkClick
	});
}

function paginatorLinkClick(pageID, container){}

function AreYouSure(Message)
{
     if (typeof Message == 'undefined') Message = "";
     return confirm("Вы уверены? " + Message);
}

function ChooseLen(event) 
{	
	var sender = event.target == undefined? event.srcElement:event.target;
	if (sender.maxLength < 0) return;
	var M = sender.value.length;
	var span = document.getElementById(sender.id+"_count");
	if (M == undefined || span == undefined) return;
	span.innerHTML = M + " / " + sender.maxLength;
	var parent = span.parentElement == undefined ? span.parentNode : span.parentElement;
	
	parent.style.display = "inline";
}
function HideLen(event) 
{	
	var sender = event.target == undefined? event.srcElement:event.target;
	var span = document.getElementById(sender.id+"_count");
	var parent = span.parentElement == undefined ? span.parentNode : span.parentElement;
	parent.style.display = "none";
}

function checkAll(formName, cbName)
{
    alert('checkAll');
	var form = document.forms[formName];

	for (var i=0; i < form[cbName].length; i++) 
		form[cbName][i].checked = true;
}

function uncheckAll(formName, cbName)
{
    alert('uncheckAll');
	var form = document.forms[formName];
	
	for (var i=0; i < form[cbName].length; i++) 
		form[cbName][i].checked = false;
}

function clearTextField(fieldId)
{
    alert('Clear Text Field');
	var textField = document.getElementById(fieldId);
	
	textField.value = "";
}
function isEmpty(str) 
{
	if (str == "") return true;
    for (var i = 0; i < str.length; i++)
		if (str.charAt(i) != " ")
            return false;
    return true;
}

function checkEmail(email)
{
		var arr;
		arr=email.split("@");
		if (arr.length<2) 
		{
			alert("Некорректный e-mail.");
			return false;
		}
		arr=email.split(".");
		if (arr.length<2) 
		{
			alert("Некорректный e-mail.");
			return false;
		}
		return true;
}
function checkform(event) {

	f = event.target == undefined? event.srcElement:event.target
	// <!--это для того, чтобы отображать имена полей -->
	//<!--var errMSG = "";--> 
	var isErr = false; 
	msg = "Все поля, помеченые звёздочкой, необходимо заполнить.";
    for (var i = 0; i<f.elements.length; i++)
	{
		
    	if (f.elements[i].getAttribute("required") != null) 
		{
    		if (isEmpty(f.elements[i].value))  
			{
				    //<!-- Это массив с именами незаполненныйх полей заполняем -->
                    //<!--errMSG += "  " + f.elements[i].name + "\\n"; -->
					isErr= true;
			}
			else if ((f.elements[i].name == "email") && (!checkEmail(f.elements[i].value)))
			{
				isErr= true;
				msg = "Неверный email";
			}
		}
	}
           // <!--if ("" != errMSG) { -->
	if (isErr) 
	{
               //<!--alert("Не заполнены обязательные поля:\\n" + errMSG); -->
		
		alert(msg);
        return false;
    }
    else
        return true;
}
(function ($) {
    $.fn.nicEditor = function() {
        if (typeof nicEditor == 'undefined')
            return;
        this.each(function (key, elem) {
            var tarea = $(this);
            if (!tarea.attr('id'))
                tarea.attr('id', 'nicEditor' + Math.floor((Math.random() * 10000)));
            var areaId = tarea.attr('id');
            var short = tarea.hasClass('short');
            var toggleLink = $('<a class="nicEditorToggler" title="Переключить редактор" href="#">Переключить редактор</a><br clear="all">').on('click', function(e) {
                var link = $(this);
                if (link.data.nicEditor)
                {
                    link.data.nicEditor.removeInstance(areaId);
                    link.data.nicEditor = null;
                }
                else
                    link.data.nicEditor = new nicEditor({fullPanel : !short}).panelInstance(areaId);
                
                return false;
            });
            tarea.before(toggleLink);
        });
    };
}(jQuery));
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MainTableSort
 *
 * @author ghost
 */
class MainTableSort extends Control implements IFlushingControl
{

	private $mainTableFor	 = null;
	private $linkedField	 = null;
	private $listingField	 = null;
	private $raitingField	 = null;

	public function InitControl($value = null, $rowID = null)
	{
        $this->LoadSettings();

		$mainTableManager			 = new DBTableManager($this->mainTableFor);
		$mainTableManager->SortOrder = '`' . $this->raitingField . '`';
		$entries					 = $mainTableManager->Select(array($this->linkedField => $rowID));
		$records					 = array();
		$cnt						 = sizeof($entries);

		if ($cnt == 0)
		{
			$this->controlHTML	 = 'no rows';
			$this->name			 = $this->name;
			$this->description	 = $this->description;
			return;
		}

		foreach ($entries as $value)
		{
			$records[] = array(
				'id'	 => $value[$mainTableManager->TableStructure->PrimaryKey->Name],
				'name'	 => $value[$this->listingField]
			);
		}
		$this->controlTplPath				 = parent::$CONTROLS_PATH . __CLASS__ . '/control.tpl';
		$this->controlTplVars				 = array();
		$this->controlTplVars['NAME']		 = $this->name;
		$this->controlTplVars['NAMETABLE']	 = $this->mainTableFor;
		$this->controlTplVars['TIP']		 = $this->tip;
		$this->controlTplVars['formIndex']	 = (isset($this->formIndex)) ? $this->formIndex : 0;
        $this->controlTplVars['RECORDS']	 = $records;
        $this->controlTplVars['field']		 = $mainTableManager->TableStructure->PrimaryKey->Name;
		$this->type							 = __CLASS__;
		$this->controlJSFiles				 = array();
		$this->controlJSFiles[]				 = parent::$CONTROLS_ALIAS . __CLASS__ . '/init.js';
	    $this->controlJSVars['name']		 = $this->name;
		$this->controlJSVars['libsPath']	 = AppSettings::$LIBS_ALIAS;
	}

	public function Flush(&$data, $rowID)
	{
        $this->LoadSettings();
		$mainTableManager = new DBTableManager($this->mainTableFor);

		if (!isset($data[$mainTableManager->TableStructure->PrimaryKey->Name]))
			return;

		foreach ($data[$mainTableManager->TableStructure->PrimaryKey->Name] as $key => $val)
		{
			$mainTableManager->Update(array($this->raitingField => ($key + 1)), $val);
		}
//		FB::log($data, "data");
//		FB::log($rowID, "rowID");
	}

	public function PostBackHandler(&$data, $rowID = null)
	{

	}

	protected function LoadSettings()
	{
		$errmsg = $this->name . ' is undefined. It should be defined in FieldsRegistry';

		if (!empty($this->controlSettings['mainTable']))
			$this->mainTableFor = $this->controlSettings['mainTable'];
		else
			throw new Exception('main Table for ' . $errmsg);

		if (!empty($this->controlSettings['linkedField']))
			$this->linkedField = $this->controlSettings['linkedField'];
		else
			throw new Exception('Listing field ' . $errmsg);

		if (!empty($this->controlSettings['listingField']))
			$this->listingField = $this->controlSettings['listingField'];
		else
			throw new Exception('Listing field ' . $errmsg);

		if (!empty($this->controlSettings['raitingField']))
			$this->raitingField = $this->controlSettings['raitingField'];
		else
			throw new Exception('Raiting field ' . $errmsg);
	}

}

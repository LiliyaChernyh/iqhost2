<?php

class ErrorController extends FrontController
{
    public function __construct()
    {
        parent::__construct();

        // Создаем представление
        $this -> view = new ErrorView();
    }

    public function Index($args = null)
    {

    }

    /**
     * Enter description here...
     *
     * @param array $args
     * @return ActionResult
     */
    public function PageNotFound($args = null)
    {
		header("HTTP/1.0 404 Not Found");
		$this -> view -> PageNotFound();

        $this -> page = new Page(array(
            'caption'   => 'Страница не найдена',
            'title'     => 'Страница не найдена'
        ), false);
        
        if (!empty($exeption)) {
            $mes = trim($exeption->getMessage());
            if (!empty($mes)) { // вывод информации об исключении (сообщения)
                ViewData::Assign('errorInfo', $exeption->getCode() . ':' . $exeption->getMessage());
            }
        }

        return $this -> view;
    }
}

?>
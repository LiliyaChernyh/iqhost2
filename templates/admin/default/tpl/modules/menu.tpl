<h1>Модули:</h1>
<p>
    <br />
    {foreach from=$MODULES_MENU item=MENU_MODULE}
        {if $MENU_MODULE.isVisible}
            <a href="{$ADMIN_ALIAS}{$MENU_MODULE.name}/">{$MENU_MODULE.description}</a>
            <a href="{$SECTION_ALIAS}edit/{$MENU_MODULE.name}/" title="Редактировать настройки модуля  '{$MENU_MODULE.description}'" style="font-size: 80%;">[ {$MENU_MODULE.name} {if !empty($MENU_MODULE.version)}v{$MENU_MODULE.version}{/if}]</a>
            <br />
        {/if}
    {/foreach}
    <br />
    <span style="color:gray;font-size:80%;letter-spacing:-1px;">(+)</span> <a href="{$SECTION_ALIAS}install/" title="Установить модуль">Установить модуль</a>
</p>
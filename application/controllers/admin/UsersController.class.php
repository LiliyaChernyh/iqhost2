<?php
/**
 * Контроллер пользователей
 *
 *  @version 2.2
 */
class UsersController extends AdminController
{
        /**
	 * Менеджер пользователей
	 *
	 * @var CurrentProjectUsersManager
	 */
	protected $usersManager = null;

        /**
        * Путь к редактированию пользователям
        *
        */
        protected $sectionAlias;

	public function __construct()
	{
            parent::__construct();

            // Инициализируем менеджер пользователей
            $this -> usersManager = CurrentProjectUsersManager::getInstance();

            $this -> view = new UsersView();

            $this -> sectionAlias = AppSettings::$ADMIN_ALIAS.AppSettings::$USERS_ALIAS;
            ViewData::Assign('SECTION_ALIAS', $this -> sectionAlias);
	}

	public function Index($args = null)
	{
            //из разряда - не пускаем сюда лохов всяких
            $user = $this -> user -> ToArray();
	    if ($user['role'] != 'admin')
	        return $this -> Error('Нет права доступа: только пользователи с правами администратора имеют право для входа в раздел.', AppSettings::$ADMIN_ALIAS);

            if (isset($args[0]))
            {
                    switch ($args[0])
                    {
                        // Список пользователей
                        case 'list':
                                return $this -> UsersList();
                                break;

                        // Редактирование основных параметров пользователя (таблица Users)
                        case 'edit':
                                return $this -> EditUser($args);
                                break;

                        // Редактирование дополнительных данных пользователя (таблица UsersInfo)
                        case 'info':
                                return $this -> EditInfo($args);
                                break;

                        // Создание пользователя. Создается запись в таблице Users
                        case 'add':
                                return $this -> AddUser();
                                break;

                        // Добавление дополнительных данных к пользователю
                        case 'addinfo':
                                return $this -> AddInfo($args);
                                break;

                        // Смена пароля
                        case 'passchange':
                                return $this -> PasswordChange($args);
                                break;

                        // Восстановление пароля на указанный при регистрации e-mail
                        case 'restore_password':
                                return $this -> RestorePassword($args);
                                break;

//                        case 'logout':
//                                $this -> Logout();
//                                break;

                        case 'delete':
                                return $this -> Delete($args);
                                break;

                        default:
                                throw new PageNotFoundException('');
                                break;
                    }
            }
            else
            {
                    return $this -> UsersList();
            }
	}

        protected function PasswordChange($args = null)
	{
            $user = $this -> user -> ToArray();
	    if ($user['role'] != 'admin')
	        return $this -> Error('Нет права доступа', $this -> sectionAlias.'list/');

            if (isset($args[1]) && is_numeric($args[1])) $userID = $args[1];
            else return $this -> Error('Не задан ID пользователя', $this -> sectionAlias .'list/', 1);

            $user = $this -> usersManager -> GetUser(array('userID' => $userID));

            if (empty($user))
                return $this -> Error('Невозможно найти указанного пользователя', $this -> sectionAlias .'list/');

	    if (isset($_POST['doSave']))
	    {
	        if (empty($_POST['newPass']) || empty($_POST['newPassConf']))
	        {
	            $_SESSION['passError'] = 'Необходимо указать новый пароль в обоих полях';
	            return $this -> Error('Необходимо указать новый пароль в обоих полях', $this -> sectionAlias .'passchange/'. $userID .'/', true);
	        }

	        try
	        {
	            $this -> usersManager -> ChangePassword($_POST['newPass'], $_POST['newPassConf'], '', $user['userID'], false);

                    DBLog::info('User password changed, userID=%d', $userID);
	            return $this -> Success('Пароль сменен', $this -> sectionAlias);
	        }
	        catch (Exception $e)
	        {
	            $_SESSION['passError'] = $e -> getMessage();
	            return $this -> Error($e -> getMessage(), $this -> sectionAlias .'passchange/'. $user['userID'] .'/', true);
	        }
	    }

	    if (isset($_SESSION['passError']))
	    {
	        ViewData::Assign('ERROR', $_SESSION['passError']);
	        unset($_SESSION['passError']);
        }

	    $user['roleInfo'] = $this -> usersManager -> Role($user['role']);
	    ViewData::Assign('EDIT_USER', $user);
	    ViewData::Assign('MAIN_EDIT', 1);
	    ViewData::Assign('INFO_EDIT', 1);

            $this -> view -> Password();
            return $this -> view;
	}

	protected function RestorePassword($args = null)
	{
            // Получаем ID пользователя
	    if (isset($args[1]) && is_numeric($args[1])) $userID = $args[1];
            else return $this -> Error('Не задан ID пользователя', $this -> sectionAlias .'list/', 1);

            $user = $this -> usersManager -> GetUser(array('userID' => $userID));

            if (empty($user))
                return $this -> Error('Невозможно найти указанного пользователя', $this -> sectionAlias .'list/');

            try
            {
                $login = $user['login'];
                $password = Password::GeneratePassword(8, 2);;

                $this -> usersManager -> ChangePassword($password, $password, null, $userID, false);

                if ($this -> SendUserEmail('Восстановление пароля', $user['FIO'], $user['email'], $login, $password))
                    return $this -> Success('Новый пароль отправлен на почту', $this -> sectionAlias .'edit/'.$userID.'/', 1);
                else
                    return $this -> Error('Не задан ID пользователя', $this -> sectionAlias .'list/', 1);
            }
            catch (Exception $e)
            {
                if (isset($_POST['async']))
                    die('{"error":"'. $e -> getMessage().'"}');

                //$this -> SavePost();
                return $this -> Error($e -> getMessage(), $this -> sectionAlias .'list/');
            }
	}

	/**
	 * Отображает список пользователей
	 *
	 */
	protected function UsersList()
	{
	    $onPage = 20;
            if (isset($_GET['page']) && is_numeric($_GET['page'])) $pageNumber = $_GET['page'];
            else $pageNumber = 1;

            if (isset($_POST['doFilter']))
	    {
	        if (!empty($_POST))
	            $_SESSION['usersFilters'] = serialize($_POST);

                return $this -> Success('', $this -> sectionAlias .'list/', true);
	    }

	    if (isset($_POST['doCancel']))
	    {
            unset($_SESSION['usersFilters']);
            return $this->Success('', $this->sectionAlias . 'list/', true);
        }

            $user = $this -> user -> ToArray();
	    $curRoleInfo = $this -> usersManager -> GetRoleInfo($user['role']);

	    $filters = array();
	    if (isset($_SESSION['usersFilters']))
	    {
    		$filters = unserialize($_SESSION['usersFilters']);
    		$filters['level'] = $curRoleInfo['level'];

    		$roles = $this -> usersManager -> GetRolesWithInfo();
                $rolesCnt = count($roles);
                for ($i = 0; $i < $rolesCnt; $i++)
                {
                    if ($roles[$i]['role'] == $filters['role'])
                        $roles[$i]['selected'] = 1;
                }
                $filters['roles'] = $roles;
            }

            ViewData::Assign('FILTERS', $filters);

	    $users = $this -> usersManager -> GetUsersWithMainInfo($filters, $onPage, $onPage * ($pageNumber -1));

	    ViewData::Assign('USERS', $users);

	    $this -> setPaginator($onPage, $this -> usersManager -> CountUsers(), $pageNumber, $this -> sectionAlias . 'list/page/');

            $this -> view -> UsersList();
            return $this -> view;
	}

	/**
	 * Добавление нового пользователя.
	 *
	 */
	protected function AddUser()
	{
            if (isset($_POST['doSave']))
            {
                try
                {
                    $login = $_POST['login'];

                    // Получаем пароль
                    if (empty($_POST['autoGenerate']))
                    {
                        $password = $_POST['password'];
                        $passwordConf = $_POST['passwordConf'];
                    }
                    else
                    {
                        $password = Password::GeneratePassword(8, 2);
                        $passwordConf = $password;
                    }

                    // Добавляем пользователя в базу
                    $userID = $this -> usersManager -> AddUser($_POST['FIO'], $login, $_POST['email'], $password, $passwordConf, $_POST['role'], 1);

                    // Если задан почтовый адрес, необходимо отравить параметры доступа пользователя на этот адрес
                    if (!empty($_POST['email']))
                    {
                        $this->SendUserEmail('Регистрация на сайте', $_POST['FIO'], $_POST['email'], $login, $password);
                    }

                    DBLog::Info('User created, userID=%d', $userID);

                    if (isset($_POST['async']))
                        die('{"id":"'.$res.'"}');
                    else
                        return $this -> Success('Пользователь создан', $this -> sectionAlias .'addinfo/'. $userID .'/');
                }
                catch (Exception $e)
                {
                        if (isset($_POST['async']))
                            die('{"error":"'. $e -> getMessage().'"}');

                        //$this -> SavePost();
                        return $this -> Error($e -> getMessage(), $this -> sectionAlias .'add/');
                }
            }

            $this -> AssignFormToTpl(TablesNames::$USERS_TABLE_NAME);

            $this -> view -> AddUser();
            return $this -> view;
	}

	public function AddInfo($args = null, $aliasSuccess = null)
	{
            // Получаем ID пользователя, для которого надо добавить информацию
	    if (isset($args[1]) && is_numeric($args[1])) $userID = $args[1];
            else return $this -> Error('Не задан ID пользователя', $this -> sectionAlias .'list/', 1);

	    $usersInfoTableManager = new DBTableManager(TablesNames::$USERS_INFO_TABLE_NAME);

            // Проверим, создана ли уже для указанного пользователя записать в таблице информации
            $res = $usersInfoTableManager -> Count(array('userID' => $userID));

            // Если информация уже есть - ошибка
            if ($res != 0)
                $this -> Error('Для указанного пользователя уже создана информация', $this -> sectionAlias .'info/'. $userID .'/');

	    if (isset($_POST['doSave']))
            {
                try
                {
                    $usersInfoTableManager -> Insert(array('userID' => $userID));
                    $_POST['userID'] = $userID;

                    $cm = new ContentManager();
                    $res = $cm -> FlushData(TablesNames::$USERS_INFO_TABLE_NAME, $_POST);

                    if (isset($_POST['async']))
                        die('{"id":"'.$res.'"}');
                    else{
                        if ($aliasSuccess)
                            return $this -> Success('Информация пользователя добавлена', $aliasSuccess);
                        else
                            return $this -> Success('Информация пользователя добавлена', $this -> sectionAlias);
                    }
                }
                catch (Exception $e)
                {
                    if (isset($_POST['async']))
                        die('{"error":"'. $e -> getMessage().'"}');

                    //$this -> SavePost();
                    return $this -> Error($e -> getMessage(), $this -> sectionAlias .'addinfo/'. $userID .'/');
                }
            }

	    $this -> AssignFormToTpl($usersInfoTableManager -> TableStructure -> TableName);

            $this -> view -> AddInfo();
            return $this -> view;
	}

	protected function EditUser($args = null)
	{
            if (isset($args[1]) && is_numeric($args[1])) $userID = $args[1];
            else return $this -> Error('Не задан ID пользователя', $this -> sectionAlias .'list/', 1);

            $user = $this -> usersManager -> GetUser(array('userID' => $userID));

            if (empty($user))
                return $this -> Error('Невозможно найти указанного пользователя', $this -> sectionAlias .'list/');

            if (isset($_POST['doSave']))
            {
                try
                    {
                    $cm = new ContentManager();
                    $res = $cm -> FlushData(TablesNames::$USERS_TABLE_NAME, $_POST);
                    //$this -> usersManager -> ChangeActivation($_POST['isActive'], $userID);
                    if ($res)
                    {
                        DBLog::Info('User edit success, edited userID=%d', $userID);

                        if (isset($_POST['async']))
                            die('{"id":"'.$res.'"}');
                        else
                            return $this -> Success('Информация о пользователе обновлена', $this -> sectionAlias .'list/');
                    }
                    else
                        throw new Exception('Произошла неизвестная ошибка.');

                    }
                    catch (Exception $e)
                    {
                        if (isset($_POST['async']))
                            die('{"error":"'. $e -> getMessage().'"}');

                        //$this -> SavePost();
                        return $this -> Error($e -> getMessage(), $this -> sectionAlias .'edit/'. $userID .'/');
                    }
            }

	    $user['roleInfo'] = $this -> usersManager -> Role($user['role']);

            ViewData::Assign('MAIN_EDIT', 1);
	    ViewData::Assign('EDIT_USER', $user);

	    $this -> AssignFormToTpl(TablesNames::$USERS_TABLE_NAME, $userID);

            $this -> view -> EditUser();
            return $this->view;
	}

	/**
	 * Редактирование дополнительных данных пользователя (UsersInfo)
	 *
	 */
	protected function EditInfo($args = null)
	{
            if (isset($args[1]) && is_numeric($args[1])){
                $userID = $args[1];

                //проверяем, есть ли информация с таким $userID
                if (!$this -> usersManager -> GetInfo($userID))
                    return $this -> Success('У данного пользователя нет дополнительной информации. Сейчас будет создана.', $this -> sectionAlias .'addinfo/'.$userID.'/');
            }else
                return $this -> Error('Не задан ID пользователя', $this -> sectionAlias .'list/', 1);

            $user = $this -> usersManager -> GetUser(array('userID' => $userID));

            if (empty($user))
                return $this -> Error('Невозможно найти указанного пользователя', $this -> sectionAlias .'list/');

            if (isset($_POST['doSave']))
            {
                try
                    {
                    $cm = new ContentManager();
                    $res = $cm -> FlushData(TablesNames::$USERS_INFO_TABLE_NAME, $_POST);
                    if ($res)
                    {
                        DBLog::Info('User info chanhed, userID=%d', $userID);

                        if (isset($_POST['async']))
                            die('{"id":"'.$res.'"}');
                        else
                            return $this -> Success('Информация о пользователе обновлена', $this -> sectionAlias .'list/');
                    }
                    else
                        throw new Exception('Произошла неизвестная ошибка.');

                    }
                    catch (Exception $e)
                    {
                        if (isset($_POST['async']))
                        die('{"error":"'. $e -> getMessage().'"}');

                        //$this -> SavePost();
                        return $this -> Error($e -> getMessage(), $this -> sectionAlias .'edit/'. $userID .'/');
                    }
            }

	    $user['roleInfo'] = $this -> usersManager -> Role($user['role']);

	    ViewData::Assign('INFO_EDIT', 1);
	    ViewData::Assign('EDIT_USER', $user);

	    $this -> AssignFormToTpl(TablesNames::$USERS_INFO_TABLE_NAME, $userID);

            $this -> view -> EditUser();
            return $this -> view;
	}

        protected function Delete($args = null)
	{
            $user = $this -> user -> ToArray();
	    if ($user['role'] != 'admin')
	        return $this -> Error('Нет права доступа', $this -> sectionAlias.'list/');

            if (isset($args[1]) && is_numeric($args[1])) $userID = $args[1];
            else return $this -> Error('Не задан ID пользователя', $this -> sectionAlias .'list/', 1);

            $tableUsers = new DBTableManager(TablesNames::$USERS_TABLE_NAME);
            $tableUsers -> Delete(array('userID' => $userID));

	    return $this -> Success('Пользователь удалён', $this -> sectionAlias.'list/');
	}

        protected function SendUserEmail($msg, $name, $email, $login, $password){
            $mail = new BitPlatformMailer();

            // Выбираем шаблон сообщения
            $tpl = new Template($this -> view -> tplsPath.'mail/newUser.tpl');

            // Получаем общие настройки и передаем в шаблон
            $settings = $this -> settingsManager -> GetSettingsAssoc('general');
            $tpl -> assign('SETTINGS', $settings);

            // Хост
            $tpl -> assign('HOST_NAME', 'http://'. $_SERVER['HTTP_HOST']);

            // Логин и пароль
            $tpl -> assign('LOGIN', $login);
            $tpl -> assign('PASSWORD', $password);
            $tpl -> assign('SUB_MSG', $msg);

            $mail -> Subject = $msg.' «'. $settings['general']['siteName'] .'»';
            //$mail -> ClearAddresses();
            $mail -> AddAddress($email, $name);
            $mail -> MsgHTML($tpl -> Parse());

            return $mail -> Send();
        }
}
?>
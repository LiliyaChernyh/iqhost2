<select name="{$localVars.tableName}[{$localVars.formIndex}][{$localVars.NAME}]" id="{$localVars.NAME}_{$localVars.formIndex}" class="select2 {if $localVars.CLASS}{$localVars.CLASS}{/if}" {if !empty($localVars.READONLY)} readonly="readonly"{/if}{if $localVars.TIP} title="{$localVars.TIP}"{/if} style="width:400px;">
	{foreach from=$localVars.OPTIONS item=OPTION}
		<option value="{$OPTION.value}"{if $OPTION.selected} selected="selected"{/if} {if !empty($OPTION.disabled)} disabled="disabled"{/if} {if isset($OPTION.level) AND $OPTION.level== 2} class="level1"{/if}>{$OPTION.text}</option>
	{/foreach}
</select>
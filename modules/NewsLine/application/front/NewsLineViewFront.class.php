<?php

/**
 * Представление для работы с модулем NewsLine во фронт-енде
 *
 * @version 2.0
 *
 */
class NewsLineViewFront extends FrontView
{

	public function __construct()
	{
		$this->moduleName = 'NewsLine';
		parent::__construct();
	}

	public function Index()
	{
		$this->tpl->contentTpl = $this->tplsPath . 'index.tpl';

		ViewData::Append('STYLE_FILE', $this->publicFilesPath . 'css/style.css');
	}

	public function Item()
	{
		$this->tpl->contentTpl = $this->tplsPath . 'item.tpl';

		ViewData::Append('SCRIPT_FILE', $this->publicFilesPath . 'scripts/script.js');
		ViewData::Append('STYLE_FILE', $this->publicFilesPath . 'css/style.css');
	}

}

?>
<footer id="footer" class="container">
    <div class="column">
        <h3>Контакты</h3>
        <ul class="tabset">
            <li>
                <a href="#tab1-0">Москва</a>
            </li>
            {*<li>
                <a href="#tab2-0">London UK</a>
            </li>*}
        </ul>
        <div class="tab-content">
            <div id="tab1-0">
                {if isset($SETTINGS.front.phone) && !empty($SETTINGS.front.phone)}
                <div class="phone">
                    <a href="tel:{$SETTINGS.front.phone.SMALL}">{$SETTINGS.front.phone.ccode} ({$SETTINGS.front.phone.tcode}) <strong>{$SETTINGS.front.phone.body}</strong></a>
                </div>
                {/if}
                <dl>
                    {if isset($SETTINGS.front.email) && !empty($SETTINGS.front.email)}<dt>Email : </dt>
                    <dd><a href="mailto:{$SETTINGS.front.email}">{$SETTINGS.front.email}</a></dd>{/if}
                    {if isset($SETTINGS.front.support) && !empty($SETTINGS.front.support)}<dt>Тех поддержка: </dt>
                    <dd><a href="mailto:{$SETTINGS.front.support}">{$SETTINGS.front.support}</a></dd>{/if}
                </dl>
                <div class="info">
                   {if isset($SETTINGS.front.company) && !empty($SETTINGS.front.company)}<p><strong>{$SETTINGS.front.company}</strong></p>{/if}
                    <dl>
                        {if isset($SETTINGS.front.zip) && !empty($SETTINGS.front.zip)}<dt>Почтовый адрес: </dt>
                        <dd>{$SETTINGS.front.zip}</dd>{/if}
                       {if isset($SETTINGS.front.address) && !empty($SETTINGS.front.address)}<dt>Юридический адрес: </dt>
                        <dd>{$SETTINGS.front.address}</dd>{/if}
                        {if isset($SETTINGS.front.inn) && !empty($SETTINGS.front.inn)}<dt>ИНН: </dt>
                        <dd>{$SETTINGS.front.inn}{/if} {if isset($SETTINGS.front.okpo) && !empty($SETTINGS.front.okpo)}ОКПО: {$SETTINGS.front.okpo}</dd>{/if}
                    </dl>
                </div>
            </div>
            {*<div id="tab2-0">
                <div class="phone">
                    <a href="tel:xxxxxxxxx">+x (xxx) <strong>xxx-xxxx</strong></a>
                </div>
                <dl>
                    <dt>Email : </dt>
                    <dd><a href="#">...</a></dd>
                    <dt>Тех поддержка: </dt>
                    <dd><a href="#">...</a></dd>
                </dl>
                <div class="info">
                    <p><strong>ООО «Ай-Кью Хостинг»</strong></p>
                    <dl>
                        <dt>Почтовый адрес: </dt>
                        <dd>...</dd>
                        <dt>Юридический адрес: </dt>
                        <dd>...</dd>
                        <dt>ИНН: </dt>
                        <dd>...</dd>
                    </dl>
                </div>
            </div>*}
        </div>
    </div>
    <div class="column">
        {if !empty($CUSTOMMENU)}
            {foreach from=$CUSTOMMENU key=key item=item}
                {if $key=='FOOTER_MENU'}
                    <nav class="footer-nav">
                        {foreach from=$item key=key item=itemMenu}
                        <div class="col">
                            {if $itemMenu.alias == "#"}<h3>{$itemMenu.name}</h3>{/if}
                            {if isset($itemMenu.children) && !empty($itemMenu.children)}
                            <ul>
                                {foreach from=$itemMenu.children key=key item=child}
                                <li><a href="{$child.alias}">{$child.name}</a></li>
                                {/foreach}
                            </ul>
                            {/if}
                        </div>
                        {/foreach}                        
                    </nav>
                {/if}
            {/foreach}
        {/if}
        <ul class="social-list">
            {if isset($SETTINGS.front.vkontakte) && !empty($SETTINGS.front.vkontakte)}<li><a href="{$SETTINGS.front.vkontakte}">vkontakte</a></li>{/if}
            {if isset($SETTINGS.front.facebook) && !empty($SETTINGS.front.facebook)}<li><a href="{$SETTINGS.front.facebook}" class="facebook">Facebook</a></li>{/if}
            {if isset($SETTINGS.front.instagram) && !empty($SETTINGS.front.instagram)}<li><a href="{$SETTINGS.front.instagram}" class="instagram">Instagram</a></li>{/if}
        </ul>
    </div>
</footer>
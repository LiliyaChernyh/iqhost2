<?php

class SolutionsView extends FrontView {

    public function __construct() {
        parent::__construct();
        $this->tplsPath .= 'solutions/';
    }

    public function Index() {
        $this->tpl->contentTpl = $this->tplsPath . 'index.tpl';
        return $this;
    }

    public function Item() {
        $this->tpl->contentTpl = $this->tplsPath . 'item.tpl';
        return $this;
    }

    public function Docs() {
        $this->tpl->contentTpl = $this->tplsPath . 'docs.tpl';
        return $this;
    }
}

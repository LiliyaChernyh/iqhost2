<?php
/**
 * Управление древовидной структурой сайта.
 * 
 * @version 3.2
 *
 */
class SiteStructure
{	
	/**
	 * Конструктор
	 *
	 * @return SiteStructure
	 */
	function __construct()
	{
		$this -> structureTableMngr = DBTableManager::getInstance(TablesNames::$PAGE_STRUCTURE_TABLE_NAME);
		$this -> structureTableMngr -> SortOrder = "`priority` DESC";
		//self::$currentStructure = &$this;
	}

	/**
	 * Менеджер таблицы в БД
	 *
	 * @var DBTableManager
	 */
	public $structureTableMngr;

	/**
	 * Экземпляр данного класса
	 *
	 * @var SiteStructure
	 */
	public static $instance = null;

	/**
	 * Возвращает экземпляр данного класса.
	 * Если он был уже создан, заново не создается.
	 *
	 * @return SiteStructure
	 */
	public static function getInstance()
	{
	    if (is_null(self::$instance))
            self::$instance = new SiteStructure();

        return self::$instance;
	}
	
	/**
	 * Функция производит поиск страницы по ее полному алиасу
	 *
	 * @param string $fullPageAlias
	 * @param bool $onlyActive
	 * @return array
	 */
	public function GetPageByFullAlias($fullPageAlias, $onlyActive = null)
	{
	    $onlyActive = is_null($onlyActive) ? false : true;

	    if (is_array($fullPageAlias))
	       $aliases = $fullPageAlias;
	    elseif (is_string($fullPageAlias))
	    {
    		// Обрезаем слешы по краям
    		$fullPageAliasNoSlash = trim($fullPageAlias, '/');
    			
    		// Разбиваем на массив алиасов
    		$aliases = explode('/', $fullPageAliasNoSlash);
	    }
	    else 
	       throw new ArgumentException('$fullPageAlias Должен быть строкой или массивом', 4000);

		$page = array();
		$parentID = 0;
		$cnt = count($aliases);
		for ($i = 0; $i < $cnt; $i++)
		{
		    $page = $this -> GetPage(array('parentID' => $parentID, 'alias' => $aliases[$i], 'isDeleted' => 0));
		    
		    if (is_null($page))
                return null;
                
            $parentID = $page['pageID'];
		}
		
		if ($onlyActive && !$page['isActive'])
			return null;
			
		return $page;
	}
	
	/**
     * Формирует полный алиас страницы по ID
     *
     * @param int $pageID
     * @return string
     */
	function GetFullAlias($pageID)
	{
		$page = $this -> GetPage(array('pageID' => $pageID));
	
		$parentID = $page['parentID'];
		$alias = '/'. $page['alias'] .'/';
	
		while ($parentID != 0)
		{
			$parentPage = $this -> GetPage(array('pageID' => $parentID));
			$alias = '/'. $parentPage['alias'].$alias;
			$parentID = $parentPage['parentID'];
		}
		
		return $alias;
	}
	
	/**
	 * Добавляет страницу
	 *
	 * @param array $attributes
	 * @return int
	 */
	function AddPage($attributes)
	{
		return $this -> structureTableMngr -> Insert($attributes);
	}
	
	/**
	 * Обновляет атрибуты страницы в структуре и базе
	 *
	 * @param int $pageID
	 * @param array $attributes
	 */
	function EditPage($pageID, $attributes)
	{	
		$this -> structureTableMngr -> Update($attributes, $pageID);
	}
	
	/**
	 * Перемещение в корзину
	 *
	 * @param int $pageID
	 */
	public function RecyclePage($pageID)
	{
		$this -> structureTableMngr -> Update(array('isDeleted' => 1), $pageID);
		$this -> RecycleChildPages($pageID);
	}
	
	private function RecycleChildPages($pageID)
	{
		$this -> structureTableMngr -> UpdateRows(array('isDeleted' => 1), array('key' => 'parentID', 'value' => $pageID));
		$children = $this -> GetChildPages($pageID);
		$cnt = count($children);
		for ($i = 0; $i < $cnt; $i++)
		{
			$this -> RecycleChildPages($children[$i]['pageID']);
		}
	}
	
	public function WipePage($pageID)
	{
		// Удаляем страницу из структуры
		$this -> structureTableMngr -> Delete(array('pageID' => $pageID));
	}
	
	public function Restore($pageID)
	{
		$query = "SELECT t1.isDeleted, t1.pageID from " . $this -> structureTableMngr -> GetTableName() . 
				" t1 JOIN ". $this -> structureTableMngr -> GetTableName() . 
				" t2 ON t1.pageID=t2.parentID WHERE t2.pageID = $pageID";
		$queryResult = DB::Query($query);
		$hasNoPapa = (mysql_num_rows($queryResult) == 0);
		if (!$hasNoPapa) 
		{
			$row = mysql_fetch_assoc($queryResult);
			$hasNoPapa =  $row['isDeleted'] == 1;
		}
		
		if ($hasNoPapa)
		{
			$this -> EditPage($pageID, array('isDeleted' => 0, 'parentID' => 0));
		}
		else 
			$this -> EditPage($pageID, array('isDeleted' => 0));
	}
	
	/**
	 * Возвращяет указатель на страницу
	 *
	 * @param int $pageID
	 * @return array
	 */
	public function GetPageById($pageID)
	{
		return $this -> GetPage(array('pageID' => $pageID));
	}
	
	/**
	 * Выдает массив с номерами дочерних страниц для страницы $parentID
	 *
	 * @param int $pageID
	 * @return array
	 */
	function GetChildPages($pageID, $attributes = null)
	{
		if ($attributes != null) $attributes['parentID'] = $pageID;
		else $attributes = array('parentID' => $pageID);
		return $this -> structureTableMngr -> Select($attributes);
	}
	
	/**
	 * Возвращает страницу, соответствующую заданным атрибутам
	 *
	 * @param array $attributes
	 * @return array
	 */
	public function GetPage($attributes)
	{
		$pages = $this -> GetPages($attributes, 1);
		return isset($pages[0]) ? $pages[0] : null;
	}
	
	/*
	public function GetCurrentPage()
	{
		return $this->currentPage;
	}
	*/
	
	/**
	 * Возвращет массив страниц, соответствующих заданным атрибутам
	 *
	 * @param array $attributes
	 */
	public function GetPages($attributes, $limit = null, $start = null)
	{
		return $this -> structureTableMngr -> Select($attributes, $limit, $start);
	}
	
	/**
	 * Подсчитывает количество страниц с заданными атрибутами
	 *
	 * @param array $attributes
	 */
	public function PagesNumber($attributes)
	{
		return $this -> structureTableMngr -> Count($attributes);
	}
	
	/**
	 * Стоит дерево (наверх), частью которого является страница (До ParentID = 0) и без детей
	 * return [page{parent_id = 2, id = 3}, page{parent_id=1, id=2}, page{parent_id = 0, id = 1}]
	 * 
	 * @param int $pageID
	 * @return array
	 */
	public function GetParentsForPage($pageID)
	{
		try 
		{
			$page = $this -> GetPage(array('pageID' => $pageID));
		}
		catch (PageNotFoundException $e)
		{
			return null;
		}
		
		$res = array();
		$res[] = &$page;
		return ($page['parentID'] == 0 ? $res : array_merge($res, $this -> GetParentsForPage($page['parentID'])));
	}
	
	/**
	 * Функция генерирует поддерево структуры для заданной страницы
	 * Поддерево содержит саму страницу, её детей (если есть),
	 * а также всех братьев
	 *
	 * @param int $pageID
	 * @return array
	 */
	public function GetSubTreeForPage($pageID)
	{
		$parents = $this -> GetParentsForPage($pageID);
		if ($parents == null) return null;
		
		$subTree = array();
		$currentLevel = &$subTree;
		for ($i = count($parents)-1; $i>-1; $i--)
		{
			// Получим братьев
			$brothers = $this -> GetPages(array('parentID' => $parents[$i]['parentID'], 'isActive' =>1, 'isDeleted' => 0));
			$nextLevel = null;
			for ($j = 0; $j < count($brothers); $j++)
			{
				$currentItem = array('value' => &$brothers[$j]);
				// Проверяем является ли этот узел отцом или праотцом первоначального
				if ($brothers[$j]['pageID'] == $parents[$i]['pageID'])
				{
					$currentItem['children'] = array();
					$nextLevel = &$currentItem['children'];
				}
				else
				{
					$currentItem['children'] = null;
				}
				$currentLevel[] = $currentItem;
			}
			if (!is_array($nextLevel)) break;
			$currentLevel = &$nextLevel;
		}
		// Получим братьев
		$brothers = $this -> GetPages(array('parentID' => $parents[0]['pageID'], 'isActive' =>1, 'isDeleted' => 0));
			
		for ($j = 0; $j < count($brothers); $j++)
			$currentLevel[] = array('value' => &$brothers[$j], 'children' => null);

		return $subTree;
	}
	
	/**
	 * Возвращает дерево страниц, начиная с $sectionPageID
	 *
	 * @param int $sectionPageID
	 * @return array
	 */
	public function GetTreeForSection($sectionPageID, $attributes = null)
	{
		$tree = array();
		$attr = ($attributes == null) ? array() : $attributes;
		
		$attr['pageID'] = $sectionPageID;
		$page = $this -> GetPage( $attr );
		
		$tree = array(	'value'		=>	$page,
						'children'	=>	array()
					);

		$attr = ($attributes == null) ? array() : $attributes;	
		$attr['parentID'] = $page['pageID'];
		$pages = $this -> GetPages( $attr );
			
		foreach ($pages as $child)
		{
			$tree['children'][] = $this -> GetTreeForSection($child['pageID'], $attributes);
		}

		return $tree;
	}

	/**
	 * Возвращает список ID родительских страниц
	 * 
	 * @param int $pageID
	 * @return array
	 */
	public function GetParentIDs($pageID)
	{
		if ($pageID == 0)
			return array();

		$currentPage = $this -> GetPage( array('pageID' => $pageID) );

		$parent_id = $currentPage['parentID'];

		$parents = array();
		$parents[] = $pageID;
		while ($parent_id != 0)
		{
			$parents[] = $parent_id;
			$page = $this -> GetPages( array('pageID' => $parent_id) );
			$parent_id = $page[0]['parentID'];		
		}

		return $parents;
	}

    /**
     * Получение из алиаса id страницы, модуля - обработчика и параметров для модуля.
     * Возвращаемый массив page + [fullAlias] + [params => {param1,param3}]
     *
     * @param array $pageAlias
     * @return array
     */
    public function GetExistingPage($pageAlias)
    {
        $aliasNo = count($pageAlias);
        $testAlias = '/';
        $fullAliasLength = -1;
        for ($i = 0; $i < $aliasNo; $i++)
        {
            $testAlias .= $pageAlias[$i].'/';
            $testPage = $this -> GetPageByFullAlias($testAlias);
            if($testPage == null)
            {
                $testPage = $prevPage;
                $fullAliasLength = $i;
                break;
            }
            else 
            {
                $prevPage = $testPage;
            }
        }
        if($fullAliasLength == -1)
        {
            $fullAliasLength = $aliasNo;
        }
        $testPage['fullAlias'] = array_slice($pageAlias,0,$fullAliasLength);
        $testPage['params'] = array_slice($pageAlias,$fullAliasLength - 1);
        return $testPage;
    }

	// Возвращает всех потомков
	// детей вглубь
	public function GetOffsprings($pageID, $attrs)
	{
		$children = $this -> GetChildPages($pageID, $attrs);
		$cnt = count($children);
		$result = array();
		$result = $children;
		for($i = 0; $i < $cnt; $i++)
		{
			$result = array_merge($result, $this -> GetOffsprings($children[$i]['pageID'], $attrs));
		}
		return $result;
	}
}
?>
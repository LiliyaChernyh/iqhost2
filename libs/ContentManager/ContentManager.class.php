<?php

require_once(IncPaths::$LIBS_PATH . 'ContentManager/FormGenerator.class.php');

define('ML_RELTABLE_POSTFIX', '_lang');
define('ML_CONTENTTABLE_POSTFIX', '_content');

/**
 * Класс, управляющий контентом в базе данных.
 *
 * @version 3.8
 *
 */
class ContentManager
{

	public function __construct()
	{

	}

	/**
	 * Enter description here...
	 *
	 * @var unknown_type
	 */
	public $recentPageType = null;

	/**
	 * Флаг успешности сохранения данных.
	 * Используется в асинхронном режиме.
	 *
	 * @var bool
	 */
	public $isSuccess = true;

	/**
	 * Сообщение об ошибке.
	 * Используется в асинхронном режиме.
	 *
	 * @var string
	 */
	public $error = null;

	/**
	 * Флаг необходимости обновления формы после отправки данных.
	 * Обновляется при генерации формы.
	 *
	 * @var bool
	 */
	public $formRefreshRequired = false;

	/**
	 * Flushes data to the database
	 *
	 * @param string $tableName Название таблицы
	 * @param array $data Данные для записи в таблицу
	 * @param bool $alwaysInsert Добавить запись, даже если передан существующий ID
	 */
	public function FlushData($tableName, $data, $alwaysInsert = false, $parentID = null)
	{
		$controlsProvider = new DBControlsRegistryProvider();
		$activeTableStructure = new DBTableStructure($tableName);
		$controlsProvider->activeTableStructure = &$activeTableStructure;

		$realTableFields = array();
		$cnt = count($activeTableStructure->Fields);
		for ($i = 0; $i < $cnt; $i++)
			$realTableFields[$activeTableStructure->Fields[$i]->Name] = &$activeTableStructure->Fields[$i];

//	FB::info($data);
		// Массив полей, которые можно обновить прямо отсюда
		$fieldsToUpdate = array();
		$primaryKey = array();

		// Массив "виртуальных" полей (которых нет в таблице)
		$virtualTableFields = array();

		$flushingControls = array();

		// Массив многоязычных полей
		$multiLanguageFields = array();
//		FB::log($tableName,"table name");
//		FB::log($data,"data");
		// Перехватываем ошибку, если она возникнет во время записи данных в базу
		try
		{
			if (isset($data[$tableName]) and is_array($data[$tableName]))
			{
//                FB::log($data, $tableName);
                foreach ($data[$tableName] as $dataKey => $dataIndex)
				{
//                    FB::log($dataIndex, "data");
//                    FB::log($dataKey, "key");
                    $primaryKey[$dataKey] = null;
					foreach ($dataIndex as $dataName => $dataValue)
					{
						if ($dataName == $activeTableStructure->PrimaryKey->Name)
						{
//                          FB::info("if ".$dataName);
                            if (!empty($dataValue) && is_numeric($dataValue) && $dataValue != 0)
							{
								$primaryKey[$dataKey] = $dataValue;
//								FB::info($primaryKey[$dataKey], "PK");
							}
						}
						elseif (isset($realTableFields[$dataName]))
						{
//                          FB::info("elseif ".$dataName);
                            // Создаём контрол и запускаем PostBackHandler
							$controlParams = $controlsProvider->GetControlParams($tableName, $dataName);
							if (empty($controlParams))
							{
//                                FB::error("stop");
								continue;
							}
							$control = ControlFactory::CreateControl($controlParams, $controlsProvider, $tableName, null, $dataIndex);

							$control->PostBackHandler($dataValue, $primaryKey[$dataKey]);
							$fieldsToUpdate[$dataKey][$dataName] = $dataValue;
						}
						else
						{
//			    FB::info("else".$dataName);
							if (strpos($dataName, "__") === false)
							{
								$virtualTableFields[$dataKey][$dataName] = $dataValue;
								$controlName = $dataName;
							}
							else
							{
								$dataNameParts = explode("__", $dataName);

								// Если название разделилось не на 2 части, значит оно задано неверно.
								if (sizeof($dataNameParts) != 2)
									continue;

								$virtualTableFields[$dataKey][$dataNameParts[0]][$dataNameParts[1]] = $dataValue;
								$controlName = $dataNameParts[0];
//				FB::info($virtualTableFields[$dataKey]);
//				FB::info($controlName);
							}
							// Создаём контрол и запускаем PostBackHandler
							$controlParams = $controlsProvider->GetControlParams($tableName, $controlName);
//			    FB::info($controlName,'controlName');
//			    FB::info($controlParams,'controlParams');
							if(empty($controlParams))
							{
								continue;
							}

							$control = ControlFactory::CreateControl($controlParams, $controlsProvider, $tableName, null, $dataIndex);

							$control->PostBackHandler($dataValue, $primaryKey);
							$flushingControls[$dataKey][$controlName] = $control;
						}
					}
//		    FB::info($primaryKey,"PK");
				}
			}


//			FB::info($primaryKey, "PK");
//			FB::info($fieldsToUpdate, "fieldsToUpdate");
			foreach ($primaryKey as $dataKey => $val)
			{
				// Добавим PK в Insert-запрос
				if ($primaryKey[$dataKey] && $alwaysInsert)
				{
					$fieldsToUpdate[$dataKey][$activeTableStructure->PrimaryKey->Name] = $primaryKey[$dataKey];
//					FB::info("------------ here -----------");
				}

				// Создадим менеджер нашей таблицы
				$editingTable = new DBTableManager($activeTableStructure->TableName);

				$isNew = false;
//				FB::log($primaryKey[$dataKey], "PK");
//				FB::log($fieldsToUpdate[$dataKey], "FTU");
//				FB::log($alwaysInsert, "alwaysInsert");

				// Запишем "простые" данные в таблицу
				if ($primaryKey[$dataKey] == null || $alwaysInsert)
				{
//                    FB::log($fieldsToUpdate[$dataKey],'INSERT');
//					FB::log($parentID,"ddd");
					$primaryKey[$dataKey] = $editingTable->Insert($fieldsToUpdate[$dataKey], $parentID);
					$isNew = true;
				}
				else
				{
//					FB::log($parentID,"ddd");
//					FB::log($fieldsToUpdate[$dataKey], 'UPDATE');
					$editingTable->Update($fieldsToUpdate[$dataKey], $primaryKey[$dataKey],null,$parentID);
				}

				//Если всегда вставляем, то primaryKey должен передаться извне
				if ($alwaysInsert)
				{
					$primaryKey[$dataKey] = $data[$activeTableStructure->PrimaryKey->Name];
				}
				if (empty($primaryKey[$dataKey]))
					throw new Exception('Primary key is empty. Can not flush', 782);

				// Обработаем "виртуальные" поля
				if (isset($virtualTableFields[$dataKey]) and sizeof($virtualTableFields[$dataKey]) > 0)
				{
//                    FB::info($virtualTableFields,"here");
					foreach ($virtualTableFields[$dataKey] as $fieldName => $fieldData)
					{
						$controlParams = $controlsProvider->GetControlParams($tableName, $fieldName);

						// Если такое поле не зарегистрировано...
						if (empty($controlParams))
							continue;

						// !!!! multilanguage сейчас никак не отрабатывается
						// Соберем многоязычные поля
						// Заглушка
						$controlParams['controlSettings']['isMultiLanguage'] = false;
						if ($controlParams['controlSettings']['isMultiLanguage'] && !is_array($fieldData))
						{
//                            FB::log("multi");
							$multiLanguageFields[$fieldName] = $fieldData;
						}
						else
						{
//                            FB::log($tableName,"table");
//							FB::log($primaryKey[$dataKey],"primaryKEy");
//							FB::log($fieldData,'fieldData');
							$flushingControls[$dataKey][$fieldName]->Flush($fieldData, $primaryKey[$dataKey]);
							if ($flushingControls[$dataKey][$fieldName]->refreshFormAfterFlush)
								$this->formRefreshRequired = true;
						}
					}
				}

				// Записываем многоязычные поля, если они есть
				if (sizeof($multiLanguageFields) > 0)
				{
					$contentTableManager = new DBTableManager($activeTableStructure->TableName . ML_CONTENTTABLE_POSTFIX);
					$relTableManager = new DBTableManager($activeTableStructure->TableName . ML_RELTABLE_POSTFIX);
					$systemSettings = new SystemSettings();
					$currentLang = isset($_SESSION['lang']) ? $_SESSION['lang'] : $systemSettings->GetValue('DEFAULT_LANG');

					$relRows = $relTableManager->Select(
						array(
								$activeTableStructure->PrimaryKey->Name	 => $primaryKey[$dataKey],
								'lang'									 => $currentLang
						)
					);

					if ($isNew || sizeof($relRows) == 0)
					{
						$contentID = $contentTableManager->Insert($multiLanguageFields);

						$entryID = $relTableManager->Insert(
							array(
									$activeTableStructure->PrimaryKey->Name	 => $primaryKey[$dataKey],
									'lang'									 => $currentLang,
									'contentID'								 => $contentID
							)
						);
					}
					else
					{
						$contentTableManager->Update($multiLanguageFields, $relRows[0]['contentID']);
					}
				}
			}
		}
		catch (Exception $e)
		{
			/**
			 * Возникла ошибка.
			 * Сохраняем в сессию данные и текущее время.
			 * Выкидываем Exception.
			 */
			@session_start();

			$_SESSION['savedData'] = serialize($data);
			$_SESSION['savedDataTime'] = time();

			$this->isSuccess = false;
			$this->error = $e->getMessage();
//			FB::error($this->error);
			throw $e;
		}
//        FB::info($primaryKey,"PK");
		if (sizeof($primaryKey) == 1)
			return array_shift($primaryKey);

		return $primaryKey;
	}

	/**
	 * Удаляет данные из таблицы.
	 * Можно передавать либо менеджер таблицы, либо структуру, либо ее имя
	 *
	 * @param int $rowID
	 * @param DBTableManager $tableManager
	 * @param DBTableStructure $tableStructure
	 * @param string $tableName
	 */
	public function DeleteContent($rowID, $tableManager = null, $tableStructure = null, $tableName = null)
	{
		if (is_null($tableManager))
		{
			if (!is_null($tableStructure))
				$tableManager = new DBTableManager($tableStructure->TableName, $tableStructure);
			elseif (!is_null($tableName))
				$tableManager = new DBTableManager($tableName);
			else
				throw new ArgumentException('Недостаточно данных, чтобы создать менеджер таблицы');
		}

		$activeTableStructure = &$tableManager->TableStructure;

		// Вообще говоря надо разработать систему нахождения модели данных и удаления через неё
		// Удалеяем запись из таблицы
		return $tableManager->Delete(array($activeTableStructure->PrimaryKey->Name => $rowID));
	}

	private function GetTableStructure($tableName)
	{
		// Создадим струткуру таблицы
		//DBTableExtendedStructure::ClearCreatedTables();
		//$TableStructure = new DBTableExtendedStructure($tableName);
		$TableStructure = new DBTableStructure($tableName);

		return $TableStructure;
	}

	public function CreateEmptyContent($pageTypeID)
	{
		// Получим тип информацию о типе содержимого
		$typesTable = new DBTableManager(TablesNames::$PAGES_TYPE_TABLE_NAME);
		$Qresult = $typesTable->Select(array('pageTypeID' => $pageTypeID));

		if (count($Qresult) == 0)
		{
			throw new Exception('Не существует указанного типа. ID не существует.');
		}
		$pageType = $Qresult[0];
		$this->recentPageType = $pageType;

		switch ($pageType['handlerType'])
		{
			case 'module':
				$module = null;
				$moduleNameArr = explode(".", $pageType['name']);
				$moduleName = $moduleNameArr[0];
				try
				{
					$moduleLib = MODULES_PATH . "$moduleName/$moduleName.class.php";
					if (!file_exists($moduleLib))
						throw new FileNotFoundException("Не удалось обнаружить модуль <br />$moduleLib");
					require_once($moduleLib);
					$text = "\$module = new $moduleName();";
					eval($text);
				}
				catch (Exception $e)
				{
					throw new Exception('Не удалось обнаружить указанный модуль.');
				}
				if ($module != null)
				{
					try
					{
						return $module->CreateEmptyElement($moduleNameArr);
					}
					catch (Exception $e)
					{
						throw new Exception('Не удалось создать пустую запись');
					}
				}
				break;
			case 'table':
				$tblmngr = new DBTableManager($pageType['name']);
				try
				{
					return $tblmngr->Insert(array());
				}
				catch (Exception $e)
				{
					throw new Exception("Не удалось создать пустую запись");
				}
				break;
			default:
				throw new Exception("Указанный обработчик типа ($pageType[handlerType]) не поддерживается");
		}
	}

	public function CreateEmptyMLElement($tableName, $rowID, $lang = null)
	{
		if ($lang == null)
			$lang = isset($_SESSION['lang']) ? $_SESSION['lang'] : $this->systemSettings->GetValue('DEFAULT_LANG');

		$tableStructure = new DBTableStructure($tableName);

		$relTableManager = new DBTableManager($tableName . ML_RELTABLE_POSTFIX);
		$relRows = $relTableManager->Select(
			array(
					$tableStructure->PrimaryKey->Name	 => $rowID,
					'lang'								 => $lang
			)
		);

		if (sizeof($relRows) > 0)
			throw new MultiLanguageException('Версия для языка «' . $lang . '» уже существует');

		$contentTableManager = new DBTableManager($tableName . ML_CONTENTTABLE_POSTFIX);
		$contentID = $contentTableManager->Insert(array());

		$entryID = $relTableManager->Insert(
			array(
					$tableStructure->PrimaryKey->Name	 => $rowID,
					'lang'								 => $lang,
					'contentID'							 => $contentID
			)
		);
	}

}


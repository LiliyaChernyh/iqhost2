<h1>Управление справочником опций (списочные параметры)</h1>

<div style="text-align: right; margin: -30px 0 5px 0;">
    <input type="button" id="goAddTheme" class="goToBtn" value="Добавить поле" />
    <input type="hidden" id="goAddTheme_link" value="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/add/" />
</div>

{include file='paginator.tpl'}
<div class="holder-table">
	<table class="highlightable" id="faqTable">
	    <thead>
	        <tr>
	            <th colspan="2">№</th>
	            <th>Название поля</th>
	            <th>Название поля для фронта</th>
	            <th style="width: 10px;"><img src="{$IMAGES_ALIAS}icons/delete_icon.png" /></th>
	        </tr>
	    </thead>
	    <tbody>
	{if !empty($OPTIONS)}
	    {foreach from=$OPTIONS item=THEME key=I}
	        <tr>
	        	<td class="hov">&nbsp;</td>
	            <td style="display: none;"><a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/edit/{$THEME.optionID}/" /></td>
	            <td style="width: 20px;">{$I+1}</td>
	            <td>
	                {$THEME.optionName}
	            </td>
	            <td>
	                {$THEME.optionFrontName}
	            </td>
	            <td class="deleteEntry">
	                <a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/del/{$THEME.optionID}/" title="Удалить"><img src="{$ADMIN_IMAGES}delete_icon.png" alt="Удалить" style="border: 0;" /></a>
	            </td>
	        </tr>
	    {/foreach}
	    </tbody>
	</table>
</div>
<div id="dialog-delete-record" style="display:none;" title="Подтвердить удаление">
	<p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Вы действительно хотите удалить поле "<span id="field-name" style="font-weight: bold;"></span>"? Поле используется в <span id="products-number"></span> продуктах.</p>
</div>
{else}
    </tbody>
</table>
</div>
    <div class="formBlock">
    	<div class="error">Записей не найдено</div>
    </div>
{/if}

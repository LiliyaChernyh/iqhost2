<?php
/**
 * Интерфейс провайдера таблицы маршрутизации
 *
 * @author grom
 */
interface IRoutingTableProvider
{
	/**
	 * @return Array Routing Tree
	 */
	public function &GetRoutingTree();
}
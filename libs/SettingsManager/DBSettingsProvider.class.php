<?php

require_once(IncPaths::$LIBS_PATH .'SettingsManager/ISettingsProvider.interface.php');

/**
 * Провайдер настроек из базы данных
 *
 * @version 1.1
 */
class DBSettingsProvider extends Singleton implements ISettingsProvider
{
    public function __construct()
    {

    }

    public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }

    /**
     * Уже загруженные группы (кэш)
     *
     * @var array
     */
    protected $groupsCache = null;

    /**
     * Массив с указателями на группы в кэше
     * Ключи массива - идентификаторы (groupID).
     *
     * @var array
     */
    protected $groupsPointers = null;

    /**
     * Уже загруженные настройки (кэш)
     *
     * @var array
     */
    protected $settingsCache = null;

    /**
     * Массив с указателями на настройку в кэше
     * Ключи массива - идентификаторы (settingID).
     *
     * @var array
     */
    protected $settingsPointers = null;

    /**
     * Менеджер таблицы с настройками
     *
     * @var DBTableManager
     */
    protected $settingsTableManager = null;

    /**
     * Менеджер таблицы с группами настроек
     *
     * @var DBTableManager
     */
    protected $groupsTableManager = null;

    /**
     * Инициализация менеджера таблицы настроек
     *
     */
    protected function InitSettingsTableManager()
    {
        if (is_null($this -> settingsTableManager))
            $this -> settingsTableManager = new DBTableManager(TablesNames::$SETTINGS_TABLE_NAME);
    }

    /**
     * Инициализация менеджера таблицы групп
     *
     */
    protected function InitGroupsTableManager()
    {
        if (is_null($this -> groupsTableManager))
            $this -> groupsTableManager = new DBTableManager(TablesNames::$SETTINGS_GROUPS_TABLE_NAME);
    }

    /**
     * Возвращает массив настройки.
     *
     * @param string|int $settingPath
     */
    public function GetSetting($settingPath)
    {
        if (is_numeric($settingPath))
        {
            // Настройка запрашивается по ее settingID
            $settingID = $settingPath;

            // Если настройка еще не загружалась, загружаем ее и записываем в "кэш"
            if (!isset($this -> settingsPointers[$settingID]))
            {
                // Вытаскиваем настройку из базы
                $this -> InitSettingsTableManager();
                $setting = $this -> settingsTableManager -> SelectFirst(array('settingID' => $settingID));
                if (empty($setting))
                    throw new Exception('Настройки с идентификатором ['. $settingID .'] нет в базе');

                // Вычисляем путь настройки
                $groupID = $setting['groupID'];
                $groupPath = $this -> GetGroupPath($groupID);
                $settingPath = $groupPath .'.'. $setting['name'];

                // Записываем настройку в кэш
                $this -> settingsCache[$settingPath] = $setting;
                $this -> settingsPointers[$setting['settingID']] = &$this -> settingsCache[$settingPath];
            }

            return $this -> settingsPointers[$settingID];
        }
        else
        {
            // Если настройка еще не загружалась, загружаем ее и записываем в "кэш"
            if (!isset($this -> settingsCache[$settingPath]))
            {
                // Вычисляем путь группы и название настройки
                $groupPath = substr($settingPath, 0, strrpos($settingPath, '.'));
                $settingName = substr($settingPath, strrpos($settingPath, '.') + 1);

                // Вычисляем идентификатор группы
                $groupID = $this -> GetGroupID($groupPath);

                // Вытаскиваем настройку из базы
                $this -> InitSettingsTableManager();
                $setting = $this -> settingsTableManager -> SelectFirst(array('groupID' => $groupID, 'name' => $settingName));
                if (empty($setting))
                    throw new Exception('Настройки с идентификатором ['. $settingID .'] нет в базе');

                // Записываем настройку в кэш
                $this -> settingsCache[$settingPath] = $setting;
                $this -> settingsPointers[$setting['settingID']] = &$this -> settingsCache[$settingPath];
            }

            return $this -> settingsCache[$settingPath];
        }
    }


    /**
     * Возвращает значение настройки
     *
     * @param string|int $settingPath путь или ID настройки
     * @return mixed значение настройки
     */
    public function GetValue($settingPath)
    {
        $setting = $this->GetSetting($settingPath);
        if (isset($setting['value']))
        {
            return $setting['value'];
        }
    }


    /**
     * Возвращает настройки группы
     *
     * @param string|int $groupPath
     * @return array
     */
    public function GetSettings($groupPath)
    {
        if (is_numeric($groupPath))
        {
            // Настройки запрашиваются по идентификатору группы
            $groupID = $groupPath;
        }
        else
        {
            // Вычисляем идентификатор группы
            $groupID = $this -> GetGroupID($groupPath);
        }

        // Вытаскиваем настройки из базы для данной группы
        $this -> InitSettingsTableManager();
        $settings = $this -> settingsTableManager -> Select(array('groupID' => $groupID));

        return $settings;
    }

    /**
     * Возвращает информацию о группе
     *
     * @param string|int $groupPath
     */
    public function GetGroup($groupPath)
    {
        if (is_numeric($groupPath))
        {
            // Настройки запрашиваются по идентификатору группы
            $groupID = $groupPath;

            // Инициируем вычисление пути, чтобы нужная группа добавилась в "кэш"
            $groupPath = $this -> GetGroupPath($groupID);
        }
        else
        {
            // Инициируем вычисление пути, чтобы нужная группа добавилась в "кэш"
            $groupID = $this -> GetGroupID($groupPath);
        }

        return $this -> groupsCache[$groupPath];
    }

    /**
     * Возвращает дерево групп начиная с заданной группы.
     *
     * @param string|int $rootGroupPath
     * @param bool $withSettings
     * @return array
     */
    public function GetGroupsTree($rootGroupPath = null, $withSettings = false)
    {
        $tree = array();
        if (is_null($rootGroupPath))
        {
            $rootGroupID = 0;
        }
        else
        {
            if (is_numeric($rootGroupPath))
            {
                // Настройки запрашиваются по идентификатору группы
                $rootGroupID = $rootGroupPath;
            }
            else
            {
                // Вычисляем идентификатор группы
                $rootGroupID = $this -> GetGroupID($rootGroupPath);
            }

            $group = $this -> GetGroup($rootGroupPath);

            $tree = $group;
        }

        $tree['groups'] = $this -> GetSubGroupsTreeRecurs($rootGroupID, $withSettings);

        if ($withSettings)
        {
            $settings = $this -> GetSettings($rootGroupID);
            if (!empty($settings))
                $tree['settings'] = $settings;
        }

        return $tree;
    }

    public function GetSettingsAssoc($groupPath = null)
    {
        // Если задан входной параметр, строим дерево всех настроек
        if (is_null($groupPath))
        {
            $tree = $this -> GetGroupsTree(null, true);
        }
        else
        {
            // Если входной параметр - массив, то это дерево
            if (is_array($groupPath))
            {
                $tree = $groupPath;
            }
            else
            {
                // Входной параметр - путь к группе. Получаем из этого пути дерево
                $tree = $this -> GetGroupsTree($groupPath, true);
            }
        }

        $assoc = array();

        if (empty($tree['name']))
        {
            $groupsCnt = count($tree['groups']);
            for ($i = 0; $i < $groupsCnt; $i++)
            {
                $assoc = array_merge($assoc, $this -> GetSettingsAssoc($tree['groups'][$i]));
            }
        }
        else
        {
            $groupName = $tree['name'];

            // Вытаскиваем подгруппы
            if (!empty($tree['groups']))
            {
                $groupArray = array();
                $groupsCnt = count($tree['groups']);
                for ($i = 0; $i < $groupsCnt; $i++)
                {
                    $groupArray = array_merge($groupArray, $this -> GetSettingsAssoc($tree['groups'][$i]));
                }

                if (!empty($groupArray))
                    $assoc[$groupName] = $groupArray;
            }

            // Вытаскиваем настройки
            if (!empty($tree['settings']))
            {
                $settingsCnt = count($tree['settings']);
                for ($i = 0; $i < $settingsCnt; $i++)
                {
                    $assoc[$groupName][$tree['settings'][$i]['name']] = $tree['settings'][$i]['value'];
                }
            }
        }

        return $assoc;
    }

    /**
     * Рекурсивная функция возвращения дерева группы
     *
     * @param string|int $groupPath
     * @param bool $withSettings
     * @return array
     */
    protected function GetSubGroupsTreeRecurs($groupPath, $withSettings = false)
    {
        if (is_numeric($groupPath))
        {
            // Настройки запрашиваются по идентификатору группы
            $groupID = $groupPath;
        }
        else
        {
            // Вычисляем идентификатор группы
            $groupID = $this -> GetGroupID($groupPath);
        }

        $this -> InitGroupsTableManager();

        $groups = $this -> groupsTableManager -> Select(array('parentID' => $groupID));
        $groupsCnt = count($groups);

        for ($i = 0; $i < $groupsCnt; $i++)
        {
            if ($withSettings)
            {
                $settings = $this -> GetSettings($groups[$i]['groupID']);
                if (!empty($settings))
                    $groups[$i]['settings'] = $settings;
            }

            $subGroups = $this -> GetSubGroupsTreeRecurs($groups[$i]['groupID'], $withSettings);
            if (!empty($subGroups))
                $groups[$i]['groups'] = $subGroups;
        }

        return $groups;
    }

    /**
     * Возвращает путь группы по его идентификатору
     *
     * @param int $groupID
     * @return string
     */
    protected function GetGroupPath($groupID)
    {
        if ($groupID == 0)
            return '';

        if (!isset($this -> groupsPointers[$groupID]))
        {
            $this -> InitGroupsTableManager();
            $group = $this -> groupsTableManager -> SelectFirst(array('groupID' => $groupID));

            if (empty($group))
                throw new Exception('Не существует группы с ID = '. $groupID);

            $parentPath = $this -> GetGroupPath($group['parentID']);

            $group['path'] = empty($parentPath) ? $group['name'] : $parentPath .'.'. $group['name'];

            $this -> groupsCache[$group['path']] = $group;
            $this -> groupsPointers[$groupID] = &$this -> groupsCache[$group['path']];
        }

        return $this -> groupsPointers[$groupID]['path'];
    }

    /**
     * Возвращает ID группы по ее пути
     *
     * @param string $groupPath
     */
    protected function GetGroupID($groupPath)
    {
        if (empty($groupPath))
            throw new Exception('Не указан путь к группе');

        if (!isset($this -> groupsCache[$groupPath]))
        {
            $this -> InitGroupsTableManager();

            if (strpos($groupPath, '.') > 0)
            {
                $parentPath = substr($groupPath, 0, strrpos($groupPath, '.'));
                $parentID = $this -> GetGroupID($parentPath);
                $groupName = substr($groupPath, strrpos($groupPath, '.') + 1);
            }
            else
            {
                $parentPath = '';
                $parentID = 0;
                $groupName = $groupPath;
            }

            $group = $this -> groupsTableManager -> SelectFirst(array('parentID' => $parentID, 'name' => $groupName));
            $group['path'] = empty($parentPath) ? $group['name'] : $parentPath .'.'. $group['name'];
            $this -> groupsCache[$group['path']] = $group;
            $this -> groupsPointers[$group['groupID']] = &$this -> groupsCache[$group['path']];
        }

        return $this -> groupsCache[$groupPath]['groupID'];
    }
}

?>
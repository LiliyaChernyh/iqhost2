<?php
/**
 * Представление для работы с модулем Articles во фронт-енде
 * 
 * @version 2.0
 *
 */
class ArticlesViewAdmin extends AdminView
{
    /*
     * Название модуля
     */
    protected $moduleName = 'Articles';
    
    /*
     * Папка модуля
     */
    protected $publicFilesPath = null;
    
    public function __construct()
    {
        parent::__construct();
        $this -> publicFilesPath = AppSettings::$MODULES_ALIAS.$this->moduleName.'/';
        $this -> tplsPath = IncPaths::$MODULES_PATH.$this->moduleName.'/templates/admin/';
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
    }
    
    public function ArticlesList()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'list.tpl';
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
    
    public function Sections()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'sections.tpl';     
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
     public function RecycledSections()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'recycled-sect.tpl';     
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
     public function Recycled()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'recycled.tpl';     
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
    
     public function Menu()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'menu.tpl';     
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
    
    public function Form()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'form.tpl';     
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
}
?>
{if isset($paginator)}
	{assign var=page_delta value=3}
	{if !empty($searchWord)}
		{assign var=fl value="&q=$searchWord"}
	{else}
		{assign var=fl value=""}
	{/if}
	{if !empty($searchCategory)}
		{assign var=cat value="&category=$searchCategory"}
	{else}
		{assign var=cat value=""}
	{/if}

	<div class="paginator">
	<ul class="number-page">
		{if $paginator.pageCurrent < $page_delta}{assign var=page_delta value=$page_delta*2-$paginator.pageCurrent}{/if}
		{if $paginator.pageCount-$paginator.pageCurrent < $page_delta}{assign var=page_delta value=$page_delta*2-$paginator.pageCount+$paginator.pageCurrent}{/if}
		{if $paginator.pageCurrent > 1 }
			<li>
				<a class="link-prev" href="?{$cat}{$fl}&page={$paginator.pageCurrent-1}">prev</a>
			</li>
		{else}
			<li>
				<a class="link-prev" href="#">prev</a>
			</li>
		{/if}

		{section name=pages loop=$paginator.pageCount+1 start=1}
			{if $smarty.section.pages.index > $paginator.pageCurrent-$page_delta and $smarty.section.pages.index < $paginator.pageCurrent+$page_delta}
				{if $smarty.section.pages.index == $paginator.pageCurrent}
					<li class="active"><a href="#">{$smarty.section.pages.index}</a></li>
				{else}
				<li><a href="?{$cat}{$fl}&page={$smarty.section.pages.index}">{$smarty.section.pages.index}</a></li>
				{/if}
			{elseif $smarty.section.pages.index == $paginator.pageCurrent-$page_delta}
				<!--a href="{*url page=$smarty.section.pages.index*}">...</a-->
				{if $paginator.pageCurrent > $page_delta}
					<li><a href="?{$cat}{$fl}&page=1">1</a></li>
					{if $paginator.pageCurrent > $page_delta + 1}
					<li><a href="?{$cat}{$fl}&page=2">2</a></li>
					{/if}
					{if $paginator.pageCurrent > $page_delta + 2}
						<li><span>...</span></li>
					{/if}
				{/if}
			{elseif $smarty.section.pages.index == $paginator.pageCurrent+$page_delta}
				{if $paginator.pageCount - $paginator.pageCurrent > $page_delta}
					<li><span>...</span></li>
				{/if}
				{if $paginator.pageCurrent < $paginator.pageCount - $page_delta}
				<li><a href="?{$cat}{$fl}&page={$paginator.pageCount-1}">{$paginator.pageCount-1}</a></li>
			{/if}
				{if $paginator.pageCurrent <= $paginator.pageCount}
				<li><a href="?{$cat}{$fl}&page={$paginator.pageCount}">{$paginator.pageCount}</a></li>
			{/if}
			{/if}
		{/section}
		{if $paginator.pageCurrent < $paginator.pageCount }
			<li >
				<a class="link-next" href="?{$cat}{$fl}&page={$paginator.pageCurrent+1}">next</a>
			</li>
		{else}
			<li>
				<a class="link-next" href="#">next</a>
			</li>

		{/if}
	{*
		{if $paginator.pageCurrent>1}в†ђ<a id="PrevLink" href="?page={$paginator.pageCurrent-1}">РЅР°Р·Р°Рґ</a>{/if}
		{if $paginator.pageCurrent<$paginator.pageCount}<a id="NextLink" href="?page={$paginator.pageCurrent+1}">РІРїРµСЂРµРґ</a>в†’{/if}
	*}
		</ul>
</div>
{/if}
<?php
/**
 * Менеджер данных
 * 
 * @version 1.1
 *
 */
abstract class AbstractDataManager
{
    /**
     * Менеджер таблицы
     *
     * @var DBTableManager
     */
    protected $tableManger;
    
    /**
     * Имя основной таблицы
     *
     * @var string
     */
    public $tableName;
    
    /**
     * Счётчик. Обновляется при последнем запросе на все записи
     *
     * @var int
     */
    public $entriesCnt;
    
    public function __construct($tableName)
    {
        $this -> tableName = $tableName;
    }
    
    protected function InitTableManager()
    {
        if (is_null($this -> tableManger))
            $this -> tableManger = new DBTableManager($this -> tableName);
    }
}
?>
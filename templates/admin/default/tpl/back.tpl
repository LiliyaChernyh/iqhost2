{#block: top}

<div id="container">
		
	{#block: header}
		
	{#block: navigation}
				
	<br class="clear" />
	
	<div id="sidebar">
				
			<p>
				<a href="back.php" title="Написать письмо разработчикам CMS">Обрантая связь</a>
			</p>
						
			<div id="sidebar_bottom"></div>
				
	</div>
				
	<div id="content">
	
		<h1>Обратная связь:</h1>
		
		<div>{#var: STATUS}</div>
				
		<form action="back.php" method="POST">
			<textarea name="message" cols="70" rows="15"></textarea>
			<br><br>
			<div align="center" style="width: 600px;">
				<input type="submit" name="doSend" value="Отправить">
			</div>
		</form>

	</div>
		
</div>
		
<div id="footer">
</div>

{#block: bottom}

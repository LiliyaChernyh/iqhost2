<h1>Добавление настройки</h1>

<form action="" method="post" id="contentForm" enctype="multipart/form-data">
    {include file='formBlocks.tpl}

	<div style="margin-left:350px">
		<input type="submit" value="Сохранить" name="doSave" class="handyButton" />
	</div>

</form>

{if $SELECTED_GROUP_ID}
<script>
    var selectedGroupID = {$SELECTED_GROUP_ID};

    {literal}
    $(document).ready(function(){
        $("#contentForm select[name='groupID'] option[value="+ selectedGroupID +"]").attr("selected", true);
    });
    {/literal}
</script>
{/if}
<?php
/**
 * Стандартный диспетчер событий Битплатформы
 * 
 * @version 1.1
 *
 */
class EventDispatcher extends Singleton implements IEventDispatcher 
{
    private $listeners = array();
    
    public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }
    
    public function RegisterListener($eventType, $listener, $handlerName)
    {
        if (!isset($this -> listeners[$eventType]))
            $this -> listeners[$eventType] = array();
        
        $this -> listeners[$eventType][] = array('listener' => &$listener, 'handler' => &$handlerName);
    }
    
    public function RemoveListener($eventType, $listener, $handlerName = null)
    {
        if (empty($this -> listeners[$eventType]))
            return false;
        foreach ($this -> listeners[$eventType] as $i => $value)
        {
            if ($this -> listeners[$eventType][$i]['listener'] == $listener
                && (empty($handlerName) 
                    || (!empty($handlerName) && $handlerName == $this -> listeners[$eventType][$i]['handler'])))
            {
                unset($this -> listeners[$eventType][$i]);
            }
        }
    }
    
    public function Invoke($eventType, &$args = null)
    {
        if (empty($this -> listeners[$eventType]))
            return false;
        foreach ($this -> listeners[$eventType] as $i => $value)
        {
            $listener = &$this -> listeners[$eventType][$i]['listener'];
            $handler = &$this -> listeners[$eventType][$i]['handler'];
            // Если объект
            if (is_object($listener))
            {
                $listener -> $handler($args);
            }
            else if (is_string($listener))
            {
                // Если Синглетон
                if (in_array('getInstance', get_class_methods($listener)))
                {
                    eval('$listenerObj = '.$listener.'::getInstance();');
                    $listenerObj -> $handler($args);
                }
                // Если просто объект
                else 
                {
                    $listenerObj = new $listener();
                    $listenerObj -> $handler($args);
                    $this -> listeners[$eventType][$i]['listener'] = $listenerObj;
                }
            }
        }
    }
}
?>
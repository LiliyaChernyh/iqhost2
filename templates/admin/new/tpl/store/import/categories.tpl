<h1>Управление импортом товаров: соответствие категорий</h1>
<form id="importControl" method="post">
	<table>
		<tr>
		<th colspan="2">Категория из выгрузки</th>
		<th>Категория в базе</th>
		<th></th>
		</tr>
			{foreach from=$IMPORT_CATEGORIES item=icat key=key}
		<tr>
			<td class="hov">&nbsp;</td>
			<td><input type="hidden" name="category[{$key}][name]" value="{$icat.name}">{$icat.name}</td>
			<td><select name="category[{$key}][categoryID]"><option value="" >-</option>{foreach from=$CATEGORIES item=cat}<option value="{$cat.categoryID}" {if !empty($icat.categoryID) and $icat.categoryID == $cat.categoryID}selected="selected"{/if}>{'&nbsp;'|str_repeat:$cat.level*3}{$cat.categoryName}</option>{/foreach}</select></td>
			<td><button class="addCategory" name="addCategory" data-import-category="{$icat.name}">Добавить</button></td>
		</tr>
			{/foreach}

	</table>
	<div class="formBlock">
		<button type="submit">Сохранить</button>
	</div>
</form>
			<div id="addCategoryForm" class="modal" style="border: 2px solid rgb(191, 239, 1); overflow: hidden; display: block; width: 550px;">
</div>
<?php

/**
 * Администраторская часть модуля Фотобанк
 * 
 * @version 1.0
 *
 */
class FotobankControllerAdmin extends AdminController 
{
	/**
	 * Объект класса
	 *
	 * @var Feedback
	 */
	private $fotobank = null;
	
        private $fullAliases = null;
        
	/**
	 * Путь у шаблонам админской панели
	 *
	 * @var string
	 */
	private $moduleTplsPath = null;
	
	private $moduleAlias = null;
	
	private $modulePath = null;
	
	public function __construct()
	{
                parent::__construct();
                $this -> fullAliases = Request::GetAliases();
            
		// Указываем относительный путь к админским шаблонам модуля
		$this -> moduleTplsPath = IncPaths::$MODULES_PATH.'Fotobank/admintpls/';
		$this -> moduleAlias = AppSettings::$ADMIN_ALIAS.'Fotobank/';
		$this -> modulePath = '/modules/Fotobank/';
                
                $this -> view = new FotobankViewAdmin();
                // Инициализируем объект класса
                $this -> fotobank = new Fotobank();
                
                ViewData::Assign('ROOT_ALIAS', AppSettings::$ADMIN_ALIAS);
		ViewData::Assign('MODULE_ALIAS', $this -> moduleAlias);
                ViewData::Assign('MODULE_PATH', $this -> modulePath);
	}

	public function Index($args = null)
	{ 
            if (isset($this -> fullAliases[2]))
            {
                    switch ($this -> fullAliases[2])
                    {
                            case 'file-upload':
                                    //защищаемся от лохов
                                    $userID = $this->user->GetUserID();
                                    if (!$userID) return new JsonActionResult(array('error' => 'Вы не авторизованы'));
                                
                                    return $this -> FileUploads();
                                    break;
                            case 'uploads':
                                    return $this -> Uploads();
                                    break;
                            default:
                                    return $this -> Users();
                                    break;
                    }
            }
            else 
                    return $this -> Users();
                
	}
        
        private function FileUploads()
        {  
            if (isset($this -> fullAliases[3]) && is_numeric($this -> fullAliases[3])) $tagID = $this -> fullAliases[3];
            else $tagID = null;
            
            return $this->fotobank->FileUpload($tagID);
        }
        
        private function Uploads()
        { 
            //сохраняем значения формы 
            if (isset($_POST['save'])){
                if (isset($_POST['caption'])) 
                    $ar_caption = array_keys($_POST['caption']);
                else
                    $ar_caption = array();
                
                if (isset($_POST['description'])) 
                    $ar_description = array_keys($_POST['description']);
                else
                    $ar_description = array();
                
                if (isset($_POST['tagsID']))
                    $ar_tagsID = array_keys($_POST['tagsID']);
                else
                    $ar_tagsID = array();
                
                //получаем список imageID для изменений
                $ar_imagesID = array_unique(array_merge($ar_caption, $ar_description, $ar_tagsID));
                foreach ($ar_imagesID as $imageID){
                    if (isset($_POST['caption'][$imageID])) $caption_new = $_POST['caption'][$imageID];
                    else $caption_new = null;
                    
                    if (isset($_POST['description'][$imageID])) $description_new = $_POST['description'][$imageID];
                    else $description_new = null;
                    
                    if (isset($_POST['tagsID'][$imageID])) $tagsID_new = $_POST['tagsID'][$imageID];
                    else $tagsID_new = null;
                    
                    $this->fotobank->update_foto($imageID, $caption_new, $description_new, $tagsID_new);
                }
            }
            
            if (isset($this -> fullAliases[3]) && is_numeric($this -> fullAliases[3])) $tagID = $this -> fullAliases[3];
            else $tagID = null;
           
            //формируем дерево тегов и получаем название тега
            $tag_caption = $tagID;
            $treeTags = $this->fotobank->get_treeTags(&$tag_caption);
            if ($tagID === '0') $tag_caption = 'ФОТО БЕЗ ТЕГОВ';
            
            ViewData::Assign('TAG_CAPTION', $tag_caption);
            ViewData::Assign('TAG_ID', $tagID);
            ViewData::Assign('TREE_TAGS', $treeTags);
            $this -> view -> Uploads($tagID);
            return $this -> view;
        }

	private function Users()
	{    
            if (Request::IsAsync()){
                try{
                    //получаем фото пользователя
                    if (isset($_POST['userID']) && is_numeric($_POST['userID'])){
                        //доступные теги пользователя
                        $tags = $this->fotobank->get_user_tags($_POST['userID']);
                        $tags_json = json_encode($tags);

                        //отдельные фото пользователя
                        $fotos = $this->fotobank->get_user_fotos($_POST['userID']);
                        $fotos_json = json_encode($fotos); 

                        return new JsonActionResult('{"success":"'.$_POST['userID'].'", "tags":'.$tags_json.', "fotos":'.$fotos_json.'}');
                    }
                }catch (Exception $e){
                    return new JsonActionResult('{"error":"'.$e->getMessage().'"}');
                }

                //автоподгрузка фото
                try{
                    if (isset($_GET['tagID']) && is_numeric($_GET['tagID'])){
                        $fotos = $this->fotobank->get_fotos_by_tagID($_GET['tagID']);
                        $all_tags = $this->fotobank->get_all_tags();

                        ViewData::Assign('FOTOS', $fotos);
                        ViewData::Assign('TAGS', $all_tags);
                        $this->view = new ViewResult('..'.$this -> modulePath.'templates/admin/block_fotos.tpl');
                        $this->view->ExecuteResult();
                    }else{
                        echo('<p class="error">Ошибка!</p>');
                    }    
                }catch (Exception $e){
                    echo('<p class="error">Ошибка: '.$e->getMessage().'</p>');
                }

                return new EmptyActionResult();
            }
            
            //сохраняем значения формы 
            if (isset($_POST['save']) && isset($_POST['userID']) && is_numeric($_POST['userID'])){
                //редактируем фото пользователя
                if (isset($_POST['del_imageID']) && is_array($_POST['del_imageID'])){
                    $this->fotobank->del_user_fotos($_POST['userID'], $_POST['del_imageID']);
                }
                if (isset($_POST['add_imageID']) && is_array($_POST['add_imageID'])){
                    $this->fotobank->add_user_fotos($_POST['userID'], $_POST['add_imageID']);
                }
                
                //редактируем теги пользователя
                if (isset($_POST['del_tagID']) && is_array($_POST['del_tagID'])){
                    $this->fotobank->del_user_tags($_POST['userID'], $_POST['del_tagID']);
                }
                if (isset($_POST['add_tagID']) && is_array($_POST['add_tagID'])){
                    $this->fotobank->add_user_tags($_POST['userID'], $_POST['add_tagID']);
                }
            }
            
            $fchar_users = $this->fotobank->get_first_char_users();
            $types_and_tags = $this->fotobank->get_types_and_tags();

            ViewData::Assign('FCHAR_USERS', $fchar_users);
            ViewData::Assign('TYPES_AND_TAGS', $types_and_tags);
            $this -> view -> Index();
            return $this -> view;
	}
}
?>
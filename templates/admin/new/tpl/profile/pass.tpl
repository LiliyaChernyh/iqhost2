<h1>Смена пароля</h1>

<form action="" method="post" id="contentForm">

    <div class="profile_row">
        <div class="profile_rowLeft">Теущий пароль:</div>
        <div class="profile_rowRight"><input type="password" name="oldPass" value="" size="30" /></div>
    </div>

    <div class="profile_row">
        <div class="profile_rowLeft">Новый пароль:</div>
        <div class="profile_rowRight"><input type="password" name="newPass" value="" size="30" /></div>
    </div>
    
    <div class="profile_row">
        <div class="profile_rowLeft">Еще раз:</div>
        <div class="profile_rowRight"><input type="password" name="newPassConf" value="" size="30" /></div>
    </div>
   {if isset($ERROR)}
    <div class="profile_row">
        <div class="profile_rowLeft">&nbsp;</div>
        <div class="profile_rowRight" style="color: red;">{$ERROR}</div>
    </div>
    {/if}
    
    <div style="margin: 20px 0 0 170px;">
        <input type="submit" value="Сохранить" name="doSave" class="handyButton" />
    </div>

</form>
<div style="clear: both;"></div>
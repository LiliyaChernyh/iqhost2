<h1>Категории статей</h1>
{literal}
<script type="text/javascript">
    $(document).ready(function(){
        $('.deleteCheckBox').change(EnableDisableDeleteButton);
    });
    
    function EnableDisableDeleteButton()
    {
        var checkedNumber = $('.deleteCheckBox:checked').length;
        
        if (checkedNumber == 0)
            $('#deleteItems').attr('disabled', true);
        else
            $('#deleteItems').attr('disabled', false);
    }

</script>
{/literal}

<div style="text-align: right; margin: -30px 0 5px 0;">
<form action="" method="post">
    <input type="button" id="goAddNews" class="goToBtn" value="Добавить категорию статей" />
    <input type="hidden" id="goAddNews_link" value="{$MODULE_ALIAS}addsection/" />
</form>
</div>

{if $SECTIONS|@count == 0}
    Список статей пуст
{else}
    {include file='paginator.tpl'}
    <form action="{$MODULE_ALIAS}deletessection/" method="POST" onsubmit="return AreYouSure()">
    <table id="entriesList" class="highlightable" style="clear: left;">
        <thead>
            <tr>
                <th width="5%">№</th>
                <th width="20%">Заголовок категории</th>
                <th width="20%">Алиас</th>
                <th width="15%">Дата изменения</th>
                <th style="text-align: center;" width="5%"><img src="{$ADMIN_IMAGES}delete.png" title="Поставьте галочку, если хотите удалить категорию." /></th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$SECTIONS item=SECTION key=I}
            <tr>
                <td style="display: none;"><a href="{$MODULE_ALIAS}editsection/{$SECTION.asectionID}/"></a></td>
                <td>{$SECTION.asectionID}</td>
                <td>{$SECTION.caption}</td>
                <td>{$SECTION.alias}</td>
                <td>{$SECTION.modificationDate|@date_format:"%d.%m.%Y %H:%M"}</td>
                </td>
                <td class="skip"><a class="del" data-id="{$SECTION.asectionID}" data-url="{$MODULE_ALIAS}deletesection/" href="#" ><img src="{$ADMIN_IMAGES}delete.png" title="Поставьте галочку, если хотите удалить категорию" /></a></td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    </form>
{/if}


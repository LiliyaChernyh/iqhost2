<?php

class SolutionsController extends FrontController {

    public function __construct()
    {
        parent::__construct();
        $this->view = new SolutionsView();
        $this->model = new SolutionsModel();
    }

    public function Index($args = null)
    {
        if (isset($args) && empty($args))
            throw new PageNotFoundException('Page not found', 404);
        /* $alias = Request::GetAliases();
          $this->page = new Page($alias);
          $content = $this->page->Content();
          $section = new Page('solutions');
          $ps = PagesStructure::getInstance();
          $childPages = $ps->GetChildren($section->pageID, array('isActive' => 1, 'isDeleted' => 0));
          $pages = array();
          foreach ($childPages as $child) {
          $pages[] = array(
          'alias' => $child['alias'],
          'caption' => $child['caption'],
          'active' => ($child['alias'] == $this->page->alias),
          );
          }
          ViewData::Assign('SOLUTION', $content);
          return $this->view->Index(); */
    }

    public function Item($args = null)
    {
        $this->page = new Page(Request::GetAliases());
        $content = $this->page->Content();

        $section = new Page('solutions');
        $ps = PagesStructure::getInstance();
        $childPages = $ps->GetChildren($section->pageID, array('isActive' => 1, 'isDeleted' => 0));
        $pages = array();
        foreach ($childPages as $child) {
            $pages[] = array(
                'alias' => $child['alias'],
                'caption' => $child['caption'],
                'active' => ($child['alias'] == $this->page->alias),
            );
        }

        $menu = $this->model->getMenuSolution($content['solutionID']);
//        $middle = round(count($menu)/2);
//        foreach ($menu as $k=>$v){
//            if($k < $middle){
//                $menu['one'][] = $v;
//            }
//            else{
//                $menu['two'][] = $v;
//            }
//        }

        ViewData::Assign('PAGES', $pages);
        ViewData::Assign('CONTENT', $content);

        if (!empty($content['bannerSolutionID'])) {
            ViewData::Assign('BANNER', $this->model->getBanner($content['bannerSolutionID']));
        }
        ViewData::Assign('ADVANTAGES_SOLUTION', $this->model->getAdvantages($content['solutionID']));
        ViewData::Assign('MENU_SOLUTION', $menu);

        return $this->view->Item();
    }

    public function Docs($args = null)
    {
        $alias = Request::GetAliases();
        $this->page = new Page($alias);
        $content = $this->page->Content();

        $ps = PagesStructure::getInstance();
        $childPages = $ps->GetChildren($this->page->pageID, array('isActive' => 1, 'isDeleted' => 0));
        $fullAlias = $this->page->FullAlias();

        $pages = array();
        foreach ($childPages as $child) {
            $pages[] = array(
                'alias' => $child['alias'],
                'caption' => $child['caption'],
                'active' => ($child['alias'] == $this->page->alias),
            );
        }

        $menu = $this->model->getMenuSolution($content['solutionID']);

        ViewData::Assign('PAGES', $pages);
        ViewData::Assign('CONTENT', $content);

        if (!empty($content['bannerSolutionID'])) {
            ViewData::Assign('BANNER', $this->model->getBanner($content['bannerSolutionID']));
        }
        ViewData::Assign('ADVANTAGES_SOLUTION', $this->model->getAdvantages($content['solutionID']));
        ViewData::Assign('MENU_SOLUTION', $menu);

        return $this->view->Docs();
    }

}

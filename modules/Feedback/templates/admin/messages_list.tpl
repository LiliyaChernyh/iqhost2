        <h1>Полученные сообщения</h1>

        <div>

                {include file='paginator.tpl'}

                {if !empty($MESSAGES)}
                <form action="{$MODULE_ALIAS}" method="post">
                <table width="100%" class="highlightable">
                        <tr>
                                <th width="5%">Заголовок</th>
                                <th width="30%">Пользователь</th>
                                <th width="40%">Сообщение</th>
                                <th width="15%">Дата отправки</th>
                                <th width="10%">Опубликован</th>
                        </tr>
                        {foreach from=$MESSAGES item=MESSAGE key=I}
                        <tr class="{if $I%2}odd{else}even{/if}">
                                <td style="text-align:center;"><a href="{$MODULE_ALIAS}message/{$MESSAGE.messageID}/" title="Просмотреть сообщение">{$MESSAGE.messageID}</a></td>
                                <td>{$MESSAGE.userName} {if $MESSAGE.userEmail}[<a href="mailto:{$MESSAGE.userEmail}">{$MESSAGE.userEmail}</a>]{/if}</td>
                                <td>{$MESSAGE.text|truncate:60}</td>
                                <td style="text-align:center;">{$MESSAGE.sendDate}</td>
                                <td style="text-align:center;" class="{if $MESSAGE.isModeration}success{else}error{/if}">{if $MESSAGE.isModeration}Да{else}Нет{/if}</td>
                        </tr>
                        {/foreach}
                </table>

                </form>
                {else}
                    Сообщений нет
                {/if}
        </div>
-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Апр 13 2018 г., 12:26
-- Версия сервера: 5.6.35
-- Версия PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `iqhost`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Actions`
--

CREATE TABLE `Actions` (
  `actionID` int(11) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `imageID` int(11) DEFAULT NULL,
  `publicDate` timestamp NULL DEFAULT NULL,
  `modifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Actions`
--

INSERT INTO `Actions` (`actionID`, `summary`, `text`, `imageID`, `publicDate`, `modifiedDate`) VALUES
(1, 'текст', '<p>текст</p>', 44, '2017-09-06 00:00:00', '2017-12-21 10:58:51'),
(2, 'текст', '<p>&nbsp;текст</p>', 6, NULL, '2017-09-08 09:33:39'),
(3, 'текст', '<p>&nbsp;текст</p>', 45, NULL, '2017-12-21 11:01:31');

-- --------------------------------------------------------

--
-- Структура таблицы `Advantages`
--

CREATE TABLE `Advantages` (
  `advantageID` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `priority` int(11) DEFAULT '0',
  `isActive` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Advantages`
--

INSERT INTO `Advantages` (`advantageID`, `title`, `text`, `priority`, `isActive`) VALUES
(1, 'Огромный опыт работы более 10 лет', 'Наши специалисты могут решить любую задачу, мы знаем о хостинге сайтов почти все!', 0, 1),
(2, 'Удобная панель управления ISP Manager', 'Новая 5я версия панели управления хостингом автоматизирует большинство задач и подойдет как новичку, так и самому опытному пользователю ', 0, 1),
(3, 'Выгодные тарифы и акции', 'Стоимость услуг хостинга в Москве - одна из лучших!\r\n\r\nДля новых и постоянных клиентов разные акции и подарки', 0, 1),
(4, 'Защита и безопасность данных', 'Безопасность данных ваших сайтов обеспечивает антивирусная система virusdie\r\n\r\nРегулярные бекапы защищают данные выделенных серверов', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `Colocation`
--

CREATE TABLE `Colocation` (
  `colocationID` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `load` int(11) NOT NULL,
  `priceM` int(11) NOT NULL,
  `linkM` varchar(255) NOT NULL,
  `priceK` int(11) NOT NULL,
  `linkK` varchar(255) NOT NULL,
  `percentK` int(11) DEFAULT NULL,
  `priceP` int(11) NOT NULL,
  `linkP` varchar(255) NOT NULL,
  `percentP` int(11) DEFAULT NULL,
  `priceY` int(11) NOT NULL,
  `linkY` varchar(255) NOT NULL,
  `percentY` int(11) DEFAULT NULL,
  `port` varchar(255) DEFAULT NULL,
  `polosa` varchar(255) DEFAULT NULL,
  `trafik` varchar(255) DEFAULT NULL,
  `backup` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `bonus` varchar(255) DEFAULT NULL,
  `textWhite` text NOT NULL,
  `priority` int(11) DEFAULT '0',
  `isActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `Colocation`
--

INSERT INTO `Colocation` (`colocationID`, `title`, `summary`, `load`, `priceM`, `linkM`, `priceK`, `linkK`, `percentK`, `priceP`, `linkP`, `percentP`, `priceY`, `linkY`, `percentY`, `port`, `polosa`, `trafik`, `backup`, `ip`, `bonus`, `textWhite`, `priority`, `isActive`) VALUES
(1, 'IQ COLO1U', '<p><b>Размещение 1U</b></p>\r\n<p>Мощность питания до 350Ватт</p>\r\n<p>Канал 100Мбит/сек&nbsp;</p>\r\n<p>1 IPv4 адресс</p>\r\n<p>&nbsp;</p>', 350, 3000, 'http://', 8500, 'http://', 10, 17000, 'http://', 15, 33000, 'http://', 20, '100', 'shared*', 'unlimited', '20Гб', '1', 'SSL', '<p><strong>При размещении сервера:</strong></p>\r\n<p>Дополнительный юнит (без розетки): 2000 руб.</p>\r\n<p>Поддержка (тариф): 	&quot;Начальный&quot;</p>\r\n<p>*shared - канал делится на несколько пользователей.</p>\r\n<p>&nbsp;</p>', 0, 1),
(2, 'IQV 100F', '<p><strong>4 Гб</strong></p>\r\n<p>10 сайтов на аккаунте</p>\r\n<p>Без ограничений трафика</p>', 3000, 3450, '', 8350, '', 10, 15700, '', 15, 25500, '', 20, '100', 'shared*', 'unlimited', '20Гб', '1', 'SSL', '<p><strong>При аренде сервера:</strong></p>\r\n<p>Дополнительный юнит: 1800 руб.</p>\r\n<p>Превышение трафика: 1 руб за каждые 1Гб</p>\r\n<p>Поддержка (тариф): 	\"Начальный\"</p>\r\n<p>*shared - канал делится на несколько пользователей.</p>', 0, 1),
(3, 'IQ 1000S', '<p><strong>6 Гб</strong></p>\r\n<p>10 сайтов на аккаунте</p>\r\n<p>Без ограничений трафика</p>', 4000, 4450, '', 10350, '', 10, 16700, '', 15, 26500, '', 20, '100', 'shared*', 'unlimited', '20Гб', '1', 'SSL', '<p><strong>При аренде сервера:</strong></p>\r\n<p>Дополнительный юнит: 1800 руб.</p>\r\n<p>Превышение трафика: 1 руб за каждые 1Гб</p>\r\n<p>Поддержка (тариф): 	\"Начальный\"</p>\r\n<p>*shared - канал делится на несколько пользователей.</p>', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `ColocationOptions`
--

CREATE TABLE `ColocationOptions` (
  `optionID` int(11) NOT NULL,
  `colocationID` int(11) NOT NULL,
  `imageID` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `value` varchar(255) NOT NULL,
  `priority` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `ColocationOptions`
--

INSERT INTO `ColocationOptions` (`optionID`, `colocationID`, `imageID`, `title`, `value`, `priority`) VALUES
(1, 1, 39, 'Бонусы', 'SSL', 5),
(2, 1, 40, 'Поддержка (тариф)', 'Начальный', 4),
(3, 1, 41, 'Включенный трафик', 'unlimited', 2),
(4, 1, 42, 'Бесплатно ip', '20Гб', 3),
(5, 1, 43, 'Полоса пропускания', 'shared*', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `Domains`
--

CREATE TABLE `Domains` (
  `domainID` int(11) NOT NULL,
  `zone` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `priority` int(11) DEFAULT '0',
  `isActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Domains`
--

INSERT INTO `Domains` (`domainID`, `zone`, `price`, `priority`, `isActive`) VALUES
(1, 'RU', 150, 0, 1),
(2, 'SU', 150, 0, 1),
(3, 'INFO', 150, 0, 1),
(4, 'ONLINE', 150, 0, 1),
(5, 'РФ', 150, 0, 1),
(6, 'COM', 150, 0, 1),
(7, 'NET', 150, 0, 1),
(19, 'NET', 150, 0, 1),
(18, 'NET', 150, 0, 1),
(17, 'NET', 150, 0, 1),
(16, 'NET', 150, 0, 1),
(15, 'NET', 150, 0, 1),
(14, 'NET', 150, 0, 1),
(20, 'NET', 150, 0, 1),
(22, 'NET', 150, 0, 1),
(23, 'NET', 150, 0, 1),
(24, 'NET', 150, 0, 1),
(25, 'NET', 150, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `GPU`
--

CREATE TABLE `GPU` (
  `gpuID` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `load` int(11) NOT NULL,
  `priceM` int(11) NOT NULL,
  `linkM` varchar(255) NOT NULL,
  `priceK` int(11) NOT NULL,
  `linkK` varchar(255) NOT NULL,
  `percentK` int(11) DEFAULT NULL,
  `priceP` int(11) NOT NULL,
  `linkP` varchar(255) NOT NULL,
  `percentP` int(11) DEFAULT NULL,
  `priceY` int(11) NOT NULL,
  `linkY` varchar(255) NOT NULL,
  `percentY` int(11) DEFAULT NULL,
  `diskSpace` varchar(255) DEFAULT NULL,
  `processorDevices` varchar(255) DEFAULT NULL,
  `ram` varchar(255) DEFAULT NULL,
  `textWhite` text NOT NULL,
  `priority` int(11) DEFAULT '0',
  `isActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `GPU`
--

INSERT INTO `GPU` (`gpuID`, `title`, `summary`, `load`, `priceM`, `linkM`, `priceK`, `linkK`, `percentK`, `priceP`, `linkP`, `percentP`, `priceY`, `linkY`, `percentY`, `diskSpace`, `processorDevices`, `ram`, `textWhite`, `priority`, `isActive`) VALUES
(1, 'IQ 1080X1', '<p><strong>GPU: GTX1080Ti</strong></p>\r\n<div>\r\n<p>CPU: Xeon 1230v5 4x3.4Ghz</p>\r\n<p>RAM: 16Gb DDR4</p>\r\n<p>HDD: 1x240Gb SSD</p>\r\n<p>Расположение : RU</p>\r\n</div>\r\n<p>&nbsp;</p>', 2000, 2450, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D331%26period%3D12%26project%3D1', 7350, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D331%26period%3D12%26project%3D1', 10, 14700, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D331%26period%3D12%26project%3D1', 15, 24500, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D331%26period%3D12%26project%3D1', 20, '128 Гб  SSD', '4 x 2,4 Ghz', '8 Гб DDR3', '<p><strong>GPU сервера на базе NVIDIA GTX1080Ti</strong></p>\r\n<p><img src=\"/uploaded/gtx1080.png\" width=\"220\" height=\"116\" alt=\"\" /></p>\r\n<p><strong>Описание:</strong></p>\r\n<p>GP104 - 2560 CUDA Core</p>\r\n<p>RAM - 12Gb GDDR5X</p>\r\n<p><strong>Производительность:</strong></p>\r\n<p>FP32:&nbsp; &nbsp; &nbsp;8 TFLOPS</p>\r\n<p>FP64: 257 GFLOPS</p>\r\n<p><strong>Назначение:</strong></p>\r\n<p>Транскодинг, нейросети, онлайн игры, HPC вычисления с одинарной точностью</p>', 0, 1),
(2, 'IQ 1080X2', '<p><strong>GPU: 2 x GTX1080Ti</strong></p>\r\n<div>\r\n<p>CPU: Xeon 1230v5 4x3.4Ghz</p>\r\n<p>RAM: 32Gb DDR4</p>\r\n<p>HDD: 1x240Gb SSD</p>\r\n<p>Расположение : RU</p>\r\n</div>', 3000, 3450, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D331%26period%3D12%26project%3D1', 8350, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D331%26period%3D12%26project%3D1', 10, 15700, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D331%26period%3D12%26project%3D1', 15, 25500, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D331%26period%3D12%26project%3D1', 20, '128 Гб  SSD', '4 x 2,4 Ghz', '8 Гб DDR3', '<p><strong>GPU сервера на базе NVIDIA GTX1080Ti</strong></p>\r\n<p><img src=\"/uploaded/gtx1080.png\" width=\"220\" height=\"116\" alt=\"\" /></p>\r\n<p><strong>Описание:</strong></p>\r\n<p>GP104 - 2560 CUDA Core</p>\r\n<p>RAM - 12Gb GDDR5X</p>\r\n<p><strong>Производительность:</strong></p>\r\n<p>FP32:&nbsp; &nbsp; &nbsp;8 TFLOPS</p>\r\n<p>FP64: 257 GFLOPS</p>\r\n<p><strong>Назначение:</strong></p>\r\n<p>Транскодинг, нейросети, онлайн игры, HPC вычисления с одинарной точностью</p>', 0, 1),
(3, 'IQ GRIDX1', '<p><strong>GPU: NVIDIA GRID K2</strong></p>\r\n<div>\r\n<p>CPU: Xeon 1230v5 4x3.4Ghz</p>\r\n<p>RAM: 32Gb DDR4</p>\r\n<p>HDD: 1x480Gb SSD</p>\r\n<p>Расположение : RU</p>\r\n</div>', 4000, 4450, 'http://', 10350, 'http://', 10, 16700, 'http://', 15, 26500, 'http://', 20, '128 Гб  SSD', '4 x 2,4 Ghz', '8 Гб DDR3', '<p><strong>GPU сервера на базе NVIDIA GRID K2</strong></p>\r\n<p><strong><img src=\"/uploaded/k80.gif\" width=\"248\" height=\"116\" alt=\"\" /></strong></p>\r\n<p><strong>Описание:</strong></p>\r\n<p>2 x GK104 - 3072 CUDA Core</p>\r\n<p>RAM - 8Gb GDDR5X</p>\r\n<p><strong>Производительность:</strong></p>\r\n<p>2 vGPU&nbsp; (возможна устновка до 2х карт в один сервер)</p>\r\n<p><strong>Назначение:</strong></p>\r\n<p>Виртуализация GPU для виртуальных рабочих мест (тонких клиентов), стримминг видео игр (хостинг видео игр).&nbsp;<span style=\"font-family: Arial, Tahoma, Verdana, sans-serif, Helvetica, Calibri;\">GPU сервера на базе GRID применяются для виртуализации графики, которая обеспечивает ускорение работы виртуальных десктопов и приложений на графических процессорах NVIDIA</span></p>', 0, 1),
(4, 'IQ vGPU1', '<p>&nbsp;<strong>GPU: GTX1080</strong></p>\r\n<div>\r\n<p>vCPU:&nbsp; 2x3.5Ghz (<strong><span style=\"font-family: Arial, Tahoma, Verdana, sans-serif, Helvetica, Calibri; font-size: 13px; font-weight: 700;\">E5-2637v4</span></strong><span style=\"font-family: Arial, Tahoma, Verdana, sans-serif, Helvetica, Calibri; font-size: 13px; font-weight: 700;\">)</span></p>\r\n<p>vRAM: 8Gb DDR4</p>\r\n<p>vHDD: 128Gb SSD</p>\r\n<p>Расположение : NL</p>\r\n</div>', 500, 1000, 'http://', 3000, 'http://', 10, 6000, 'http://', 15, 12000, 'http://', 20, NULL, NULL, NULL, '<p>&nbsp;Виртуальный сервер с GPU NVIDIA GTX 1080 / 1080Ti на базе ESXi VMWare 6.0 с гарантированными ресурсами.</p>\r\n<p>&nbsp;Возможности:</p>\r\n<p>- Изменение количества GPU на лету (от 1 до 4 GPU)</p>\r\n<p>- Изменение ресурсов сервера на лету (процессор, память, диск)</p>\r\n<p>- Полность гарантированные ресурсы за счет аппаратаной виртуализации</p>\r\n<p>- Быстрая установка сервера</p>\r\n<p>- Поддержка 24/7</p>\r\n<p>&nbsp;</p>', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `GPUOptions`
--

CREATE TABLE `GPUOptions` (
  `optionID` int(11) NOT NULL,
  `gpuID` int(11) NOT NULL,
  `imageID` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `value` varchar(255) NOT NULL,
  `priority` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `GPUOptions`
--

INSERT INTO `GPUOptions` (`optionID`, `gpuID`, `imageID`, `title`, `value`, `priority`) VALUES
(1, 2, 36, 'Память сервера', '32 Гб DDR3', 3),
(2, 2, 37, 'Процессор на сервере', 'Intel E3-1230v5', 1),
(3, 2, 38, 'Жесткий диск', '240 Гб  SSD', 2),
(4, 1, 87, 'Память сервера', '16 Гб DDR4', 3),
(5, 1, 88, 'Процессор на сервере', 'Intel E3-1230v5', 1),
(6, 1, 89, 'Жесткий диск', '240 Гб SSD', 2),
(7, 3, 90, 'Процессор на сервере', 'Intel E3-1230v5', 1),
(8, 3, 91, 'Память сервера', '32 Гб DDR4', 3),
(9, 3, 92, 'Жесткий диск', '480 Гб SSD', 2),
(10, 3, 93, 'Скорость сети', '1000Мбит/сек', 4),
(11, 1, 94, 'Скорость сети', '100Мбит/сек', 4),
(12, 2, 95, 'Скорость сети', '100Мбит/сек', 4),
(13, 4, 96, 'Скорость сети', '100Мбит/сек', 4),
(14, 4, 97, 'Память сервера', '8Gb DDR4', 2),
(15, 4, 98, 'Доступное кол-во ядер', '2 x 3.5Ghz', 1),
(16, 4, 99, 'Размер диска', '128Gb SSD', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_articles`
--

CREATE TABLE `greeny_articles` (
  `articleID` int(11) NOT NULL,
  `asectionID` int(11) DEFAULT NULL,
  `caption` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `imageID` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `summary` varchar(1000) DEFAULT NULL,
  `body` text NOT NULL,
  `publicationDate` datetime NOT NULL,
  `modificationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isPublished` tinyint(1) DEFAULT '1',
  `isDeleted` tinyint(1) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `greeny_articles`
--

INSERT INTO `greeny_articles` (`articleID`, `asectionID`, `caption`, `alias`, `imageID`, `author`, `summary`, `body`, `publicationDate`, `modificationDate`, `isPublished`, `isDeleted`, `title`, `description`, `keywords`) VALUES
(1, 1, 'test', 'test', '100', NULL, 'test', '<p>&nbsp;test test test</p>', '2018-04-12 00:00:00', '2018-04-12 16:09:43', 1, 0, NULL, NULL, NULL),
(2, 2, 'test2', 'test2', NULL, NULL, 'test2test2test2', '<p>&nbsp;test2test2test2test2test2test2test2test2</p>\r\n<p>test2test2test2test2test2test2test2test2</p>', '2018-04-12 00:00:00', '2018-04-12 15:50:56', 1, 0, NULL, NULL, NULL),
(3, 1, 'test3', 'test3', NULL, NULL, 'test3 test3test3test3test3 test3test3test3 test3test3test3', '', '2018-04-12 00:00:00', '2018-04-12 15:57:01', 1, 0, NULL, NULL, NULL),
(4, 1, 'test4', 'test4', '101', NULL, 'test4test4test4 test4 test4test4test4 test4', '<p>&nbsp;test4test4test4test4test4test4test4test4test4test4test4test4test4</p>\r\n<p>test4test4test4test4test4test4test4</p>', '2018-04-12 00:00:00', '2018-04-12 15:57:49', 1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_articles_sections`
--

CREATE TABLE `greeny_articles_sections` (
  `asectionID` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `modificationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `greeny_articles_sections`
--

INSERT INTO `greeny_articles_sections` (`asectionID`, `caption`, `alias`, `title`, `description`, `keywords`, `modificationDate`, `isDeleted`) VALUES
(1, 'test1', 'test1', NULL, NULL, NULL, '2018-04-12 15:04:27', 0),
(2, 'test2', 'test23', NULL, NULL, NULL, '2018-04-12 16:19:00', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_fieldsRegistry`
--

CREATE TABLE `greeny_fieldsRegistry` (
  `entryID` int(11) NOT NULL,
  `tableName` varchar(128) NOT NULL,
  `fieldName` varchar(128) NOT NULL,
  `canChange` tinyint(1) NOT NULL DEFAULT '1',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `controlType` varchar(63) NOT NULL DEFAULT 'LineInput',
  `description` varchar(255) DEFAULT NULL,
  `isListingKey` tinyint(1) NOT NULL DEFAULT '0',
  `isMultiLanguage` tinyint(1) NOT NULL DEFAULT '0',
  `tip` varchar(255) DEFAULT NULL,
  `controlSettings` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `greeny_fieldsRegistry`
--

INSERT INTO `greeny_fieldsRegistry` (`entryID`, `tableName`, `fieldName`, `canChange`, `visible`, `controlType`, `description`, `isListingKey`, `isMultiLanguage`, `tip`, `controlSettings`) VALUES
(1, 'Users', 'userID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(2, 'Users', 'login', 1, 1, 'LineInput', 'Логин', 1, 0, 'Введите логин', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"blName\" : \"Основые данные пользователя\", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),
(3, 'Users', 'FIO', 1, 1, 'LineInput', 'ФИО', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),
(4, 'Users', 'email', 1, 1, 'LineInput', 'E-mail', 0, 0, NULL, '{\r\n\"blOrder\" : 0,\r\n\"blName\" : \"\",\r\n\"elemOrder\" : 15,\r\n\"size\" : 60\r\n}'),
(5, 'Users', 'role', 1, 1, 'SingleSelect', 'Тип пользователя', 0, 0, '', '{ \"enum\" : false, \"referentTable\" : \"Roles\", \"listingKey\":\"name\", \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"emptyValue\" : 0, \"emptyText\" : \" - \" }'),
(6, 'Users', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{\"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"isCheckedByDef\": 0} '),
(7, 'Users', 'registrationDate', 1, 1, 'DateMaskedInput', 'Дата регистрации', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \"\", \"elemOrder\" : 50, \"size\" : 20, \"insertCurrentDateIfEmpty\" : true, \"ignoreUserInput\" : false, \"mask\" : \"99.99.9999 99:99:99\", \"dateFormat\" : \"d.m.Y H:i:s\" }'),
(8, 'UsersInfo', 'birthdate', 1, 1, 'DateMaskedInput', 'Дата рождения', 0, 0, '', '{\r\n	\"blOrder\" : 0, \r\n	\"blClass\" : \"\", \r\n	\"elemOrder\" : 25,\r\n        \"size\" : 15, \r\n	\"ifNull\" : \"null\", \r\n	\"ifNotNull\" : \"null\",\r\n	\"mask\" : \"99.99.9999\",\r\n	\"dateFormat\" : \"d.m.Y\",\r\n	\"minDate\" : \"01.01.1900\",\r\n	\"maxDate\" : \"01.01.2009\"\r\n}'),
(9, 'UsersInfo', 'userID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(10, 'UsersInfo', 'FIO', 1, 1, 'LineInput', 'ФИО', 1, 0, NULL, '{ \"blOrder\" : 0, \"blName\" : \"Дополнительные данные пользователя\", \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),
(11, 'UsersInfo', 'gender', 1, 1, 'SingleSelect', 'Пол', 0, 0, NULL, '{ \"enum\" : \"true\", \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 27 }'),
(12, 'Roles', 'role', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(13, 'Roles', 'name', 1, 1, 'LineInput', 'Тип пользователя', 1, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),
(14, 'Roles', 'level', 1, 1, 'LineInput', 'Уровень доступа', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),
(15, 'simplePages', 'pageContent', 1, 1, 'RichText', 'Содержание', 0, 0, 'Содержание страницы', '{\r\n  \"blOrder\" : 0,\r\n  \"blName\" : \"\",\r\n  \"elemOrder\" : 10\r\n}'),
(16, 'simplePages', 'simplePageID', 0, 0, 'HiddenInput', NULL, 0, 0, NULL, NULL),
(17, 'greeny_settings', 'groupID', 1, 1, 'SingleSelect', 'Группа настроек', 0, 0, '', '{ \"referentTable\" : \"greeny_settingsGroups\", \"listingKey\" : \"desc\", \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10 }'),
(18, 'greeny_settings', 'name', 1, 1, 'LineInput', 'Название', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),
(19, 'greeny_settings', 'desc', 1, 1, 'LineInput', 'Описание', 1, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(20, 'greeny_settings', 'value', 1, 1, 'LineInput', 'Значение', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(21, 'greeny_settingsGroups', 'groupID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(22, 'greeny_settingsGroups', 'parentID', 1, 1, 'SingleSelect', 'Родительская группа', 0, 0, '', '{ \"referentTable\" : \"greeny_settingsGroups\", \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10 }'),
(23, 'greeny_settingsGroups', 'name', 1, 1, 'LineInput', 'Назване группы', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),
(24, 'greeny_settingsGroups', 'desc', 1, 1, 'LineInput', 'Описание группы', 1, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(25, 'greeny_settings', 'settingID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(98, 'MenuList', 'menuListID', 1, 1, 'HiddenInput', '', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),
(99, 'MenuList', 'menuListName', 1, 1, 'LineInput', 'Название', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \" }'),
(100, 'MenuList', 'menuListAlias', 1, 1, 'LineInput', 'Алиас', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \" }'),
(101, 'MenuList', 'menuListComment', 1, 1, 'TextArea', 'Комментарий', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"cols\" : 60, \"rows\" : 5, \"className\" : \" \" }'),
(102, 'Menu', 'itemID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(103, 'Menu', 'parentID', 1, 1, 'SingleSelectCustom', 'Родительский пункт', 0, 0, '', '{ \"referentTable\" : \"Menu\" , \"listingKey\" : \"name\", \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"emptyText\" : \" - \" , \"parentKey\":\"parentID\" }'),
(104, 'Menu', 'name', 1, 1, 'LineInput', 'Название', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),
(105, 'Menu', 'alias', 1, 1, 'LineInput', 'Алиас', 1, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),
(106, 'Menu', 'priority', 1, 1, 'LineInput', 'Порядок ', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" :30, \"size\" : 60, \"className\" : \" \" }'),
(107, 'Menu', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{ \"checkedDefault\" : true, \"blOrder\" : 0, \"elemOrder\" :40, \"className\" : \" \" }'),
(108, 'Menu', 'menuListID', 0, 0, 'HiddenInput', '', 0, 0, '', NULL),
(109, 'Menu', 'imageID', 1, 1, 'DamnUpload', 'Изображение (только для меню услуг 40x40)', 0, 0, '', '{\r\n	\"blOrder\"	:	0,\r\n	\"elemOrder\"	:	0,\r\n	\"image\"	:	{\r\n		\"resize\"	:	{\r\n			\"strong\"	:	false,\r\n			\"width\"	:	1000,\r\n			\"height\"	:	1000\r\n		},\r\n		\"thumb\"	:	{\r\n			\"resize\"	:	{\r\n				\"strong\"	:	false,\r\n				\"width\"	:	150,\r\n				\"height\"	:	150\r\n			},\r\n			\"postfix\"	:	\"_small\"\r\n		}\r\n	},\r\n	\"includeThumbCropDialog\"	:	false,\r\n	\"maxFileSize\"	:	5048576,\r\n	\"filesType\"	:	\"image\",\r\n	\"maxFilesNumber\"	:	1,\r\n	\"linkingTable\"	:	\"\",\r\n	\"allowedExtensions\"	:	\"*.jpg; *.jpeg; *.gif; *.png\",\r\n	\"filesTable\"	:	\"greeny_images\",\r\n	\"filesTableKey\"	:	\"imageID\",\r\n	\"destinationDirectory\"	:	\"uploaded/menu/\",\r\n	\"uploadScript\"	:	\"/admin/upload/\"\r\n}'),
(110, 'Advantages', 'advantageID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(111, 'Advantages', 'title', 1, 1, 'LineInput', 'Название', 0, 0, NULL, '{ \"blOrder\" : 0, \"elemOrder\" : 20, \"size\" : 60 }'),
(112, 'Advantages', 'text', 1, 1, 'TextArea', 'Текст', 0, 0, NULL, '{ \"blOrder\" : 0, \"elemOrder\" : 30, \"cols\" : 60, \"rows\" : 5, \"className\" : \" \" }'),
(113, 'Advantages', 'priority', 1, 1, 'LineInput', 'Порядок ', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" :50, \"size\" : 60, \"className\" : \" \" }'),
(114, 'Advantages', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{ \"checkedDefault\" : true, \"blOrder\" : 0, \"elemOrder\" :60, \"className\" : \" \" }'),
(115, 'News', 'newsID', 1, 1, 'HiddenInput', '', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),
(116, 'News', 'summary', 1, 1, 'TextArea', 'Краткое описание', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"cols\" : 60, \"rows\" : 5, \"className\" : \" \" }'),
(117, 'News', 'text', 1, 1, 'RichText', 'Текст', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(118, 'News', 'imageID', 1, 1, 'DamnUpload', 'Изображение 540x405', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/news/[ID]/\", \"uploadScript\" : \"/admin/upload/\" }'),
(119, 'News', 'publicDate', 1, 1, 'DateInput', 'Дата публикации', 0, 0, '', '{ \"dateFormat\" : \"d.m.Y\", \"yearRange\" : \"-10:+5\", \"insertCurrentDateIfEmpty\" : false, \"ignoreUserInput\" : false, \"blOrder\" : 0, \"blName\" : \" \", \"elemOrder\" : 0 }'),
(120, 'Actions', 'actionID', 1, 1, 'HiddenInput', '', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),
(121, 'Actions', 'summary', 1, 1, 'TextArea', 'Краткое описание', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"cols\" : 60, \"rows\" : 5, \"className\" : \" \" }'),
(122, 'Actions', 'text', 1, 1, 'RichText', 'Текст', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(123, 'Actions', 'imageID', 1, 1, 'DamnUpload', 'Изображение 540x405', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/actions/[ID]/\", \"uploadScript\" : \"/admin/upload/\" }'),
(124, 'Actions', 'publicDate', 1, 1, 'DateInput', 'Дата публикации', 0, 0, '', '{ \"dateFormat\" : \"d.m.Y\", \"yearRange\" : \"-10:+5\", \"insertCurrentDateIfEmpty\" : false, \"ignoreUserInput\" : false, \"blOrder\" : 0, \"blName\" : \" \", \"elemOrder\" : 0 }'),
(125, 'Solutions', 'solutionID', 1, 1, 'HiddenInput', '', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),
(126, 'Solutions', 'summary', 1, 1, 'RichText', 'Краткий текст', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(127, 'Solutions', 'text', 1, 1, 'RichText', 'Полный текст', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(128, 'Solutions', 'imageID', 1, 1, 'DamnUpload', 'Изображение 297x208', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/actions/[ID]/\", \"uploadScript\" : \"/admin/upload/\" }'),
(129, 'Domains', 'domainID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(130, 'Domains', 'zone', 1, 1, 'LineInput', 'Зона', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),
(131, 'Domains', 'price', 1, 1, 'LineInput', 'Стоимость (р/год)', 1, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),
(132, 'Domains', 'priority', 1, 1, 'LineInput', 'Порядок ', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" :30, \"size\" : 60, \"className\" : \" \" }'),
(133, 'Domains', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{ \"checkedDefault\" : true, \"blOrder\" : 0, \"elemOrder\" :40, \"className\" : \" \" }'),
(134, 'Servers', 'serverID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(135, 'Servers', 'title', 1, 1, 'LineInput', 'Название', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),
(136, 'Servers', 'summary', 1, 1, 'RichText', 'Краткий текст (в слайдере)', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 10, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(138, 'Servers', 'load', 1, 1, 'LineInput', 'Рекомендованная нагрузка', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),
(139, 'Servers', 'priceM', 1, 1, 'LineInput', 'Стоимость за месяц', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(140, 'Servers', 'priceK', 1, 1, 'LineInput', 'Стоимость за квартал', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(141, 'Servers', 'percentK', 1, 1, 'LineInput', 'Процент выгоды (за квартал)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 50, \"size\" : 60, \"class\" : \" \" }'),
(142, 'Servers', 'priceP', 1, 1, 'LineInput', 'Стоимость за полугодие', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 60, \"size\" : 60, \"class\" : \" \" }'),
(143, 'Servers', 'percentP', 1, 1, 'LineInput', 'Процент выгоды (за полугодие)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 70, \"size\" : 60, \"class\" : \" \" }'),
(144, 'Servers', 'priceY', 1, 1, 'LineInput', 'Стоимость за год', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 80, \"size\" : 60, \"class\" : \" \" }'),
(145, 'Servers', 'percentY', 1, 1, 'LineInput', 'Процент выгоды (за год)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 90, \"size\" : 60, \"class\" : \" \" }'),
(146, 'Servers_', 'diskSpace', 1, 1, 'LineInput', 'Дисковое пространство', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 110, \"size\" : 60, \"class\" : \" \" }'),
(147, 'Servers_', 'processorDevices', 1, 1, 'LineInput', 'Процессорные устройства', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 120, \"size\" : 60, \"class\" : \" \" }'),
(148, 'Servers_', 'ram', 1, 1, 'LineInput', 'Оперативная память', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 130, \"size\" : 60, \"class\" : \" \" }'),
(149, 'Servers', 'textWhite', 1, 1, 'RichText', 'Текст (в белом блоке)', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 140, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(150, 'Servers', 'priority', 1, 1, 'LineInput', 'Порядок ', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" :150, \"size\" : 60, \"className\" : \" \" }'),
(151, 'Servers', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{ \"checkedDefault\" : true, \"blOrder\" : 0, \"elemOrder\" :160, \"className\" : \" \" }'),
(152, 'Hosting', 'hostingID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(153, 'Hosting', 'title', 1, 1, 'LineInput', 'Название', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),
(154, 'Hosting', 'summary', 1, 1, 'RichText', 'Краткий текст (в слайдере)', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 10, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(155, 'Hosting', 'load', 1, 1, 'LineInput', 'Рекомендованная нагрузка', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),
(156, 'Hosting', 'priceM', 1, 1, 'LineInput', 'Стоимость за месяц', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(157, 'Hosting', 'priceK', 1, 1, 'LineInput', 'Стоимость за квартал', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(158, 'Hosting', 'percentK', 1, 1, 'LineInput', 'Процент выгоды (за квартал)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 50, \"size\" : 60, \"class\" : \" \" }'),
(159, 'Hosting', 'priceP', 1, 1, 'LineInput', 'Стоимость за полугодие', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 60, \"size\" : 60, \"class\" : \" \" }'),
(160, 'Hosting', 'percentP', 1, 1, 'LineInput', 'Процент выгоды (за полугодие)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 70, \"size\" : 60, \"class\" : \" \" }'),
(161, 'Hosting', 'priceY', 1, 1, 'LineInput', 'Стоимость за год', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 80, \"size\" : 60, \"class\" : \" \" }'),
(162, 'Hosting', 'percentY', 1, 1, 'LineInput', 'Процент выгоды (за год)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 90, \"size\" : 60, \"class\" : \" \" }'),
(163, 'Hosting_', 'diskSpace', 1, 1, 'LineInput', 'Дисковое пространство', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 110, \"size\" : 60, \"class\" : \" \" }'),
(164, 'Hosting_', 'razmeshenie', 1, 1, 'LineInput', 'Сайтов к размещению', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 120, \"size\" : 60, \"class\" : \" \" }'),
(165, 'Hosting_', 'domains', 1, 1, 'LineInput', 'Домены для парковки', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 130, \"size\" : 60, \"class\" : \" \" }'),
(166, 'Hosting', 'textWhite', 1, 1, 'RichText', 'Текст (в белом блоке)', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 140, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(167, 'Hosting', 'priority', 1, 1, 'LineInput', 'Порядок ', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" :150, \"size\" : 60, \"className\" : \" \" }'),
(168, 'Hosting', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{ \"checkedDefault\" : true, \"blOrder\" : 0, \"elemOrder\" :160, \"className\" : \" \" }'),
(169, 'Service', 'serviceID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(170, 'Service', 'textGray', 1, 1, 'RichText', 'Текст в сером блоке', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 10, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(171, 'Service', 'textWhite', 1, 1, 'RichText', 'Текст в белом блоке', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 10, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(172, 'HostingBitrix', 'hostingID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(173, 'HostingBitrix', 'title', 1, 1, 'LineInput', 'Название', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),
(174, 'HostingBitrix', 'summary', 1, 1, 'RichText', 'Краткий текст (в слайдере)', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 10, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(175, 'HostingBitrix', 'load', 1, 1, 'LineInput', 'Рекомендованная нагрузка', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),
(176, 'HostingBitrix', 'priceM', 1, 1, 'LineInput', 'Стоимость за месяц', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(177, 'HostingBitrix', 'priceK', 1, 1, 'LineInput', 'Стоимость за квартал', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(178, 'HostingBitrix', 'percentK', 1, 1, 'LineInput', 'Процент выгоды (за квартал)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 50, \"size\" : 60, \"class\" : \" \" }'),
(179, 'HostingBitrix', 'priceP', 1, 1, 'LineInput', 'Стоимость за полугодие', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 60, \"size\" : 60, \"class\" : \" \" }'),
(180, 'HostingBitrix', 'percentP', 1, 1, 'LineInput', 'Процент выгоды (за полугодие)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 70, \"size\" : 60, \"class\" : \" \" }'),
(181, 'HostingBitrix', 'priceY', 1, 1, 'LineInput', 'Стоимость за год', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 80, \"size\" : 60, \"class\" : \" \" }'),
(182, 'HostingBitrix', 'percentY', 1, 1, 'LineInput', 'Процент выгоды (за год)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 90, \"size\" : 60, \"class\" : \" \" }'),
(183, 'HostingBitrix_', 'diskSpace', 1, 1, 'LineInput', 'Дисковое пространство', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 110, \"size\" : 60, \"class\" : \" \" }'),
(184, 'HostingBitrix_', 'razmeshenie', 1, 1, 'LineInput', 'Сайтов к размещению', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 120, \"size\" : 60, \"class\" : \" \" }'),
(185, 'HostingBitrix_', 'domains', 1, 1, 'LineInput', 'Домены для парковки', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 130, \"size\" : 60, \"class\" : \" \" }'),
(186, 'HostingBitrix', 'textWhite', 1, 1, 'RichText', 'Текст (в белом блоке)', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 140, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(187, 'HostingBitrix', 'priority', 1, 1, 'LineInput', 'Порядок ', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" :150, \"size\" : 60, \"className\" : \" \" }'),
(188, 'HostingBitrix', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{ \"checkedDefault\" : true, \"blOrder\" : 0, \"elemOrder\" :160, \"className\" : \" \" }'),
(189, 'VPS', 'vpsID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(190, 'VPS', 'title', 1, 1, 'LineInput', 'Название', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),
(191, 'VPS', 'summary', 1, 1, 'RichText', 'Краткий текст (в слайдере)', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 10, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(192, 'VPS', 'load', 1, 1, 'LineInput', 'Рекомендованная нагрузка', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),
(193, 'VPS', 'priceM', 1, 1, 'LineInput', 'Стоимость за месяц', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(194, 'VPS', 'priceK', 1, 1, 'LineInput', 'Стоимость за квартал', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(195, 'VPS', 'percentK', 1, 1, 'LineInput', 'Процент выгоды (за квартал)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 50, \"size\" : 60, \"class\" : \" \" }'),
(196, 'VPS', 'priceP', 1, 1, 'LineInput', 'Стоимость за полугодие', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 60, \"size\" : 60, \"class\" : \" \" }'),
(197, 'VPS', 'percentP', 1, 1, 'LineInput', 'Процент выгоды (за полугодие)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 70, \"size\" : 60, \"class\" : \" \" }'),
(198, 'VPS', 'priceY', 1, 1, 'LineInput', 'Стоимость за год', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 80, \"size\" : 60, \"class\" : \" \" }'),
(199, 'VPS', 'percentY', 1, 1, 'LineInput', 'Процент выгоды (за год)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 90, \"size\" : 60, \"class\" : \" \" }'),
(200, 'VPS_', 'diskSpace', 1, 1, 'LineInput', 'Дисковое пространство', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 110, \"size\" : 60, \"class\" : \" \" }'),
(201, 'VPS_', 'processorDevices', 1, 1, 'LineInput', 'Процессорные устройства', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 120, \"size\" : 60, \"class\" : \" \" }'),
(202, 'VPS_', 'ram', 1, 1, 'LineInput', 'Оперативная память', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 130, \"size\" : 60, \"class\" : \" \" }'),
(203, 'VPS', 'textWhite', 1, 1, 'RichText', 'Текст (в белом блоке)', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 140, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(204, 'VPS', 'priority', 1, 1, 'LineInput', 'Порядок ', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" :150, \"size\" : 60, \"className\" : \" \" }'),
(205, 'VPS', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{ \"checkedDefault\" : true, \"blOrder\" : 0, \"elemOrder\" :160, \"className\" : \" \" }'),
(206, 'GPU', 'gpuID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(207, 'GPU', 'title', 1, 1, 'LineInput', 'Название', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),
(208, 'GPU', 'summary', 1, 1, 'RichText', 'Краткий текст (в слайдере)', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 10, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(209, 'GPU', 'load', 1, 1, 'LineInput', 'Рекомендованная нагрузка', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),
(210, 'GPU', 'priceM', 1, 1, 'LineInput', 'Стоимость за месяц', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(211, 'GPU', 'priceK', 1, 1, 'LineInput', 'Стоимость за квартал', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(212, 'GPU', 'percentK', 1, 1, 'LineInput', 'Процент выгоды (за квартал)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 50, \"size\" : 60, \"class\" : \" \" }'),
(213, 'GPU', 'priceP', 1, 1, 'LineInput', 'Стоимость за полугодие', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 60, \"size\" : 60, \"class\" : \" \" }'),
(214, 'GPU', 'percentP', 1, 1, 'LineInput', 'Процент выгоды (за полугодие)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 70, \"size\" : 60, \"class\" : \" \" }'),
(215, 'GPU', 'priceY', 1, 1, 'LineInput', 'Стоимость за год', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 80, \"size\" : 60, \"class\" : \" \" }'),
(216, 'GPU', 'percentY', 1, 1, 'LineInput', 'Процент выгоды (за год)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 90, \"size\" : 60, \"class\" : \" \" }'),
(217, 'GPU_', 'diskSpace', 1, 1, 'LineInput', 'Дисковое пространство', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 110, \"size\" : 60, \"class\" : \" \" }'),
(218, 'GPU_', 'processorDevices', 1, 1, 'LineInput', 'Процессорные устройства', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 120, \"size\" : 60, \"class\" : \" \" }'),
(219, 'GPU_', 'ram', 1, 1, 'LineInput', 'Оперативная память', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 130, \"size\" : 60, \"class\" : \" \" }'),
(220, 'GPU', 'textWhite', 1, 1, 'RichText', 'Текст (в белом блоке)', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 140, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(221, 'GPU', 'priority', 1, 1, 'LineInput', 'Порядок ', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" :150, \"size\" : 60, \"className\" : \" \" }'),
(222, 'GPU', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{ \"checkedDefault\" : true, \"blOrder\" : 0, \"elemOrder\" :160, \"className\" : \" \" }'),
(223, 'Colocation', 'colocationID', 0, 0, 'HiddenInput', '', 0, 0, '', ''),
(224, 'Colocation', 'title', 1, 1, 'LineInput', 'Название', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),
(225, 'Colocation', 'summary', 1, 1, 'RichText', 'Краткий текст (в слайдере)', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 10, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(226, 'Colocation', 'load', 1, 1, 'LineInput', 'Рекомендованная нагрузка', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),
(227, 'Colocation', 'priceM', 1, 1, 'LineInput', 'Стоимость за месяц', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(228, 'Colocation', 'priceK', 1, 1, 'LineInput', 'Стоимость за квартал', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(229, 'Colocation', 'percentK', 1, 1, 'LineInput', 'Процент выгоды (за квартал)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 50, \"size\" : 60, \"class\" : \" \" }'),
(230, 'Colocation', 'priceP', 1, 1, 'LineInput', 'Стоимость за полугодие', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 60, \"size\" : 60, \"class\" : \" \" }'),
(231, 'Colocation', 'percentP', 1, 1, 'LineInput', 'Процент выгоды (за полугодие)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 70, \"size\" : 60, \"class\" : \" \" }'),
(232, 'Colocation', 'priceY', 1, 1, 'LineInput', 'Стоимость за год', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 80, \"size\" : 60, \"class\" : \" \" }'),
(233, 'Colocation', 'percentY', 1, 1, 'LineInput', 'Процент выгоды (за год)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 90, \"size\" : 60, \"class\" : \" \" }'),
(234, 'Colocation_', 'port', 1, 1, 'LineInput', 'Скорость порта Мбит/сек', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 110, \"size\" : 60, \"class\" : \" \" }'),
(235, 'Colocation_', 'polosa', 1, 1, 'LineInput', 'Полоса пропускания', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 120, \"size\" : 60, \"class\" : \" \" }'),
(236, 'Colocation_', 'trafik', 1, 1, 'LineInput', 'Включенный трафик', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 130, \"size\" : 60, \"class\" : \" \" }'),
(237, 'Colocation_', 'backup', 1, 1, 'LineInput', 'Бесплатно ip', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 130, \"size\" : 60, \"class\" : \" \" }'),
(238, 'Colocation_', 'ip', 1, 1, 'LineInput', 'Поддержка (тариф)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 130, \"size\" : 60, \"class\" : \" \" }'),
(239, 'Colocation_', 'bonus', 1, 1, 'LineInput', 'Бонусы', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 130, \"size\" : 60, \"class\" : \" \" }'),
(240, 'Colocation', 'textWhite', 1, 1, 'RichText', 'Текст (в белом блоке)', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 140, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),
(241, 'Colocation', 'priority', 1, 1, 'LineInput', 'Порядок ', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" :150, \"size\" : 60, \"className\" : \" \" }'),
(242, 'Colocation', 'isActive', 1, 1, 'CheckBox', 'Активность', 0, 0, '', '{ \"checkedDefault\" : true, \"blOrder\" : 0, \"elemOrder\" :160, \"className\" : \" \" }'),
(243, 'Hosting', 'gallery', 1, 1, 'DamnUpload', 'Опции', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 140, \r\n\"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 2000, \"height\" : 2000 }, \r\n\"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 86, \"height\" : 55 }, \"postfix\" : \"_small\" } }, \r\n\"includeThumbCropDialog\" : false, \r\n\"maxFileSize\" : 20194304, \r\n\"filesType\" : \"image\", \r\n\"maxFilesNumber\" : 20,\r\n \"linkingTable\" : \"HostingOptions\", \r\n\"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \r\n\"filesTable\" : \"greeny_images\", \r\n\"filesTableKey\" : \"imageID\", \r\n\"destinationDirectory\" : \"uploaded/hosting/\", \r\n\"uploadScript\" : \"/admin/upload/\", \"includeTitle\" : false, \"titleField\" : \"\", \"usePriority\" : false, \"priorityField\" : \"\", \"textFields\" : { \"title\" : { \"label\" : \"Название опции\", \"showLabel\" : true, \"containerType\": \"line\" },  \"value\" : { \"label\" : \"Значение\", \"showLabel\" : true, \"containerType\": \"line\" },  \"priority\" : { \"label\" : \"Порядок\", \"showLabel\" : true, \"containerType\": \"line\" }} }'),
(244, 'HostingBitrix', 'gallery', 1, 1, 'DamnUpload', 'Опции', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 140, \r\n\"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 2000, \"height\" : 2000 }, \r\n\"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 86, \"height\" : 55 }, \"postfix\" : \"_small\" } }, \r\n\"includeThumbCropDialog\" : false, \r\n\"maxFileSize\" : 20194304, \r\n\"filesType\" : \"image\", \r\n\"maxFilesNumber\" : 20,\r\n \"linkingTable\" : \"HostingBitrixOptions\", \r\n\"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \r\n\"filesTable\" : \"greeny_images\", \r\n\"filesTableKey\" : \"imageID\", \r\n\"destinationDirectory\" : \"uploaded/hostingBitrix/\", \r\n\"uploadScript\" : \"/admin/upload/\", \"includeTitle\" : false, \"titleField\" : \"\", \"usePriority\" : false, \"priorityField\" : \"\", \"textFields\" : { \"title\" : { \"label\" : \"Название опции\", \"showLabel\" : true, \"containerType\": \"line\" },  \"value\" : { \"label\" : \"Значение\", \"showLabel\" : true, \"containerType\": \"line\" },  \"priority\" : { \"label\" : \"Порядок\", \"showLabel\" : true, \"containerType\": \"line\" }} }'),
(245, 'Servers', 'gallery', 1, 1, 'DamnUpload', 'Опции', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 140, \r\n\"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 2000, \"height\" : 2000 }, \r\n\"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 86, \"height\" : 55 }, \"postfix\" : \"_small\" } }, \r\n\"includeThumbCropDialog\" : false, \r\n\"maxFileSize\" : 20194304, \r\n\"filesType\" : \"image\", \r\n\"maxFilesNumber\" : 20,\r\n \"linkingTable\" : \"ServersOptions\", \r\n\"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \r\n\"filesTable\" : \"greeny_images\", \r\n\"filesTableKey\" : \"imageID\", \r\n\"destinationDirectory\" : \"uploaded/servers/\", \r\n\"uploadScript\" : \"/admin/upload/\", \"includeTitle\" : false, \"titleField\" : \"\", \"usePriority\" : false, \"priorityField\" : \"\", \"textFields\" : { \"title\" : { \"label\" : \"Название опции\", \"showLabel\" : true, \"containerType\": \"line\" },  \"value\" : { \"label\" : \"Значение\", \"showLabel\" : true, \"containerType\": \"line\" },  \"priority\" : { \"label\" : \"Порядок\", \"showLabel\" : true, \"containerType\": \"line\" }} }'),
(246, 'VPS', 'gallery', 1, 1, 'DamnUpload', 'Опции', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 140, \r\n\"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 2000, \"height\" : 2000 }, \r\n\"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 86, \"height\" : 55 }, \"postfix\" : \"_small\" } }, \r\n\"includeThumbCropDialog\" : false, \r\n\"maxFileSize\" : 20194304, \r\n\"filesType\" : \"image\", \r\n\"maxFilesNumber\" : 20,\r\n \"linkingTable\" : \"VPSOptions\", \r\n\"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \r\n\"filesTable\" : \"greeny_images\", \r\n\"filesTableKey\" : \"imageID\", \r\n\"destinationDirectory\" : \"uploaded/vps/\", \r\n\"uploadScript\" : \"/admin/upload/\", \"includeTitle\" : false, \"titleField\" : \"\", \"usePriority\" : false, \"priorityField\" : \"\", \"textFields\" : { \"title\" : { \"label\" : \"Название опции\", \"showLabel\" : true, \"containerType\": \"line\" },  \"value\" : { \"label\" : \"Значение\", \"showLabel\" : true, \"containerType\": \"line\" },  \"priority\" : { \"label\" : \"Порядок\", \"showLabel\" : true, \"containerType\": \"line\" }} }'),
(247, 'GPU', 'gallery', 1, 1, 'DamnUpload', 'Опции', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 140, \r\n\"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 2000, \"height\" : 2000 }, \r\n\"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 86, \"height\" : 55 }, \"postfix\" : \"_small\" } }, \r\n\"includeThumbCropDialog\" : false, \r\n\"maxFileSize\" : 20194304, \r\n\"filesType\" : \"image\", \r\n\"maxFilesNumber\" : 20,\r\n \"linkingTable\" : \"GPUOptions\", \r\n\"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \r\n\"filesTable\" : \"greeny_images\", \r\n\"filesTableKey\" : \"imageID\", \r\n\"destinationDirectory\" : \"uploaded/gpu/\", \r\n\"uploadScript\" : \"/admin/upload/\", \"includeTitle\" : false, \"titleField\" : \"\", \"usePriority\" : false, \"priorityField\" : \"\", \"textFields\" : { \"title\" : { \"label\" : \"Название опции\", \"showLabel\" : true, \"containerType\": \"line\" },  \"value\" : { \"label\" : \"Значение\", \"showLabel\" : true, \"containerType\": \"line\" },  \"priority\" : { \"label\" : \"Порядок\", \"showLabel\" : true, \"containerType\": \"line\" }} }'),
(248, 'Colocation', 'gallery', 1, 1, 'DamnUpload', 'Опции', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 140, \r\n\"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 2000, \"height\" : 2000 }, \r\n\"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 86, \"height\" : 55 }, \"postfix\" : \"_small\" } }, \r\n\"includeThumbCropDialog\" : false, \r\n\"maxFileSize\" : 20194304, \r\n\"filesType\" : \"image\", \r\n\"maxFilesNumber\" : 20,\r\n \"linkingTable\" : \"ColocationOptions\", \r\n\"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \r\n\"filesTable\" : \"greeny_images\", \r\n\"filesTableKey\" : \"imageID\", \r\n\"destinationDirectory\" : \"uploaded/colocation/\", \r\n\"uploadScript\" : \"/admin/upload/\", \"includeTitle\" : false, \"titleField\" : \"\", \"usePriority\" : false, \"priorityField\" : \"\", \"textFields\" : { \"title\" : { \"label\" : \"Название опции\", \"showLabel\" : true, \"containerType\": \"line\" },  \"value\" : { \"label\" : \"Значение\", \"showLabel\" : true, \"containerType\": \"line\" },  \"priority\" : { \"label\" : \"Порядок\", \"showLabel\" : true, \"containerType\": \"line\" }} }'),
(249, 'Hosting', 'linkM', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(250, 'Hosting', 'linkK', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(251, 'Hosting', 'linkP', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 60, \"size\" : 60, \"class\" : \" \" }'),
(252, 'Hosting', 'linkY', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 80, \"size\" : 60, \"class\" : \" \" }'),
(253, 'Servers', 'linkM', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(254, 'Servers', 'linkK', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(255, 'Servers', 'linkP', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 60, \"size\" : 60, \"class\" : \" \" }'),
(256, 'Servers', 'linkY', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 80, \"size\" : 60, \"class\" : \" \" }'),
(257, 'HostingBitrix', 'linkM', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(258, 'HostingBitrix', 'linkK', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(259, 'HostingBitrix', 'linkP', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 60, \"size\" : 60, \"class\" : \" \" }'),
(260, 'HostingBitrix', 'linkY', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 80, \"size\" : 60, \"class\" : \" \" }'),
(261, 'GPU', 'linkM', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(262, 'GPU', 'linkK', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(263, 'GPU', 'linkP', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 60, \"size\" : 60, \"class\" : \" \" }'),
(264, 'GPU', 'linkY', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 80, \"size\" : 60, \"class\" : \" \" }'),
(265, 'VPS', 'linkM', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(266, 'VPS', 'linkK', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(267, 'VPS', 'linkP', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 60, \"size\" : 60, \"class\" : \" \" }'),
(268, 'VPS', 'linkY', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 80, \"size\" : 60, \"class\" : \" \" }'),
(269, 'Colocation', 'linkM', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),
(270, 'Colocation', 'linkK', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),
(271, 'Colocation', 'linkP', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 60, \"size\" : 60, \"class\" : \" \" }'),
(272, 'Colocation', 'linkY', 1, 1, 'LineInput', 'Ссылка (с http://)', 0, 0, '', '{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 80, \"size\" : 60, \"class\" : \" \" }'),
(281, 'greeny_articles', 'articleID', 0, 0, 'HiddenInput', '', 0, 0, NULL, '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"blName\":\"Основная информация\" }'),
(282, 'greeny_articles', 'asectionID', 1, 1, 'SingleSelect', 'Категория', 0, 0, NULL, '{ \"enum\" : false, \"referentTable\" : \"greeny_articles_sections\", \"listingKey\" : \"caption\", \"blOrder\" : 0, \"elemOrder\" : 10, \"emptyValue\" : \"\", \"className\" : \" \", \"emptyText\" : \" - \", \"orderBy\":\"asectionID\" }'),
(283, 'greeny_articles', 'caption', 1, 1, 'LineInput', 'Заголовок', 0, 0, NULL, '{ \"blOrder\" : 0, \"elemOrder\" : 20, \"size\" : 60 }'),
(284, 'greeny_articles', 'alias', 1, 1, 'LineInput', 'Алиас', 0, 0, 'для создания ЧПУ УРЛ', '{ \"blOrder\" : 0, \"elemOrder\" : 30, \"size\" : 60 }'),
(285, 'greeny_articles', 'imageID', 1, 1, 'DamnUpload', 'Фотография', 0, 0, NULL, '{ \"blOrder\" : 0, \"elemOrder\" : 40, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 2800, \"height\" : 2000 }, \"thumb\" : { \"resize\" : { \"strong\" : true, \"width\" : 220, \"height\" : 165 }, \"postfix\" : \"_small\" }}, \"includeThumbCropDialog\" : true, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/articles/[ID]/\", \"uploadScript\" : \"/admin/upload/\" }'),
(286, 'greeny_articles', 'summary', 1, 1, 'TextArea', 'Краткое содержание', 0, 0, NULL, '{ \"blOrder\" : 0, \"elemOrder\" : 50, \"cols\" : 60, \"rows\" : 5 }'),
(287, 'greeny_articles', 'body', 1, 1, 'RichText', 'Полный текст', 0, 0, NULL, '{ \"blOrder\" : 0, \"elemOrder\" : 60, \"width\" : \"100%\", \"height\" : 500 }'),
(289, 'greeny_articles', 'publicationDate', 1, 1, 'DateInput', 'Дата публикации', 0, 0, NULL, '{ \"dateFormat\" : \"d.m.Y\", \"yearRange\" : \"-10:+5\", \"insertCurrentDateIfEmpty\" : true, \"ignoreUserInput\" : false, \"blOrder\" : 1,  \"elemOrder\" : 10 ,\"blName\":\"Настройки публикации\"}'),
(290, 'greeny_articles', 'isPublished', 1, 1, 'CheckBox', 'Публиковать', 0, 0, NULL, '{ \"checkedDefault\" : true, \"blOrder\" : 1, \"elemOrder\" : 20 }'),
(291, 'greeny_articles', 'title', 1, 1, 'LineInput', 'Title', 0, 0, NULL, '{ \"blOrder\" : 2, \"elemOrder\" : 10, \"size\" : 60 , \"blName\" : \"Мета-информация\"}'),
(292, 'greeny_articles', 'description', 1, 1, 'LineInput', 'Description', 0, 0, NULL, '{ \"blOrder\" : 2, \"elemOrder\" : 0, \"size\" : 60 }'),
(293, 'greeny_articles', 'keywords', 1, 1, 'LineInput', 'Keywords', 0, 0, NULL, '{ \"blOrder\" : 2, \"elemOrder\" : 0, \"size\" : 60 }'),
(294, 'greeny_articles_sections', 'asectionID', 1, 1, 'HiddenInput', '', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),
(295, 'greeny_articles_sections', 'caption', 1, 1, 'LineInput', 'Заголовок', 0, 0, '', '{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),
(296, 'greeny_articles_sections', 'alias', 1, 1, 'LineInput', 'Алиас', 0, 0, 'для создания ЧПУ УРЛ', '{ \"blOrder\" : 0, \"elemOrder\" : 30, \"size\" : 60 }'),
(299, 'greeny_articles_sections', 'title', 1, 1, 'LineInput', 'Title', 0, 0, '', '{ \"blOrder\" : 1, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\", \"blName\" : \"Мета-информация\" }'),
(300, 'greeny_articles_sections', 'description', 1, 1, 'LineInput', 'Description', 0, 0, '', '{ \"blOrder\" : 1, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),
(301, 'greeny_articles_sections', 'keywords', 1, 1, 'LineInput', 'Keywords', 0, 0, '', '{ \"blOrder\" : 1, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }');

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_files`
--

CREATE TABLE `greeny_files` (
  `fileID` int(10) UNSIGNED NOT NULL,
  `src` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_images`
--

CREATE TABLE `greeny_images` (
  `imageID` int(11) NOT NULL,
  `src` varchar(255) NOT NULL,
  `srcSmall` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `greeny_images`
--

INSERT INTO `greeny_images` (`imageID`, `src`, `srcSmall`) VALUES
(4, '/uploaded/news/1/img01.jpg', '/uploaded/news/1/img01_small.jpg'),
(6, '/uploaded/actions/2/img03.jpg', '/uploaded/actions/2/img03_small.jpg'),
(8, '/uploaded/news/2/img03.jpg', '/uploaded/news/2/img03_small.jpg'),
(9, '/uploaded/news/3/img02.jpg', '/uploaded/news/3/img02_small.jpg'),
(11, '/uploaded/menu/ico05.png', '/uploaded/menu/ico05_small.png'),
(12, '/uploaded/menu/ico06.png', '/uploaded/menu/ico06_small.png'),
(21, '/uploaded/actions/2/img04.jpg', '/uploaded/actions/2/img04_small.jpg'),
(22, '/uploaded/actions/1/img04.jpg', '/uploaded/actions/1/img04_small.jpg'),
(23, '/uploaded/actions/1/img04_91.jpg', '/uploaded/actions/1/img04_91_small.jpg'),
(26, '/uploaded/hosting/ico14.png', '/uploaded/hosting/ico14_small.png'),
(27, '/uploaded/hostingBitrix/ico13.png', '/uploaded/hostingBitrix/ico13_small.png'),
(28, '/uploaded/hostingBitrix/ico12.png', '/uploaded/hostingBitrix/ico12_small.png'),
(29, '/uploaded/hostingBitrix/ico14.png', '/uploaded/hostingBitrix/ico14_small.png'),
(30, '/uploaded/servers/ico12.png', '/uploaded/servers/ico12_small.png'),
(31, '/uploaded/servers/ico15.png', '/uploaded/servers/ico15_small.png'),
(32, '/uploaded/servers/ico16.png', '/uploaded/servers/ico16_small.png'),
(33, '/uploaded/vps/ico12.png', '/uploaded/vps/ico12_small.png'),
(34, '/uploaded/vps/ico15.png', '/uploaded/vps/ico15_small.png'),
(35, '/uploaded/vps/ico16.png', '/uploaded/vps/ico16_small.png'),
(36, '/uploaded/gpu/ico16.png', '/uploaded/gpu/ico16_small.png'),
(37, '/uploaded/gpu/ico15.png', '/uploaded/gpu/ico15_small.png'),
(38, '/uploaded/gpu/ico12.png', '/uploaded/gpu/ico12_small.png'),
(39, '/uploaded/colocation/ico14.png', '/uploaded/colocation/ico14_small.png'),
(40, '/uploaded/colocation/ico12.png', '/uploaded/colocation/ico12_small.png'),
(41, '/uploaded/colocation/ico13.png', '/uploaded/colocation/ico13_small.png'),
(42, '/uploaded/colocation/ico15.png', '/uploaded/colocation/ico15_small.png'),
(43, '/uploaded/colocation/ico16.png', '/uploaded/colocation/ico16_small.png'),
(44, '/uploaded/actions/1/index.jpg', '/uploaded/actions/1/index_small.jpg'),
(45, '/uploaded/actions/3/index_2.jpg', '/uploaded/actions/3/index_2_small.jpg'),
(46, '/uploaded/menu/index.png', '/uploaded/menu/index_small.png'),
(47, '/uploaded/menu/index_2.png', '/uploaded/menu/index_2_small.png'),
(48, '/uploaded/menu/index_3.png', '/uploaded/menu/index_3_small.png'),
(49, '/uploaded/menu/index_4.png', '/uploaded/menu/index_4_small.png'),
(50, '/uploaded/menu/index_5.png', '/uploaded/menu/index_5_small.png'),
(51, '/uploaded/menu/index_6.png', '/uploaded/menu/index_6_small.png'),
(52, '/uploaded/menu/index_7.png', '/uploaded/menu/index_7_small.png'),
(53, '/uploaded/menu/index_8.png', '/uploaded/menu/index_8_small.png'),
(54, '/uploaded/hosting/index_10.png', '/uploaded/hosting/index_10_small.png'),
(55, '/uploaded/hosting/index_9_898.png', '/uploaded/hosting/index_9_898_small.png'),
(56, '/uploaded/hosting/index_9_863.png', '/uploaded/hosting/index_9_863_small.png'),
(57, '/uploaded/hosting/index_9_717.png', '/uploaded/hosting/index_9_717_small.png'),
(58, '/uploaded/hosting/index_10_452.png', '/uploaded/hosting/index_10_452_small.png'),
(59, '/uploaded/hosting/domain.png', '/uploaded/hosting/domain_small.png'),
(60, '/uploaded/hosting/hd.png', '/uploaded/hosting/hd_small.png'),
(61, '/uploaded/hosting/www.png', '/uploaded/hosting/www_small.png'),
(62, '/uploaded/hosting/hd_15.png', '/uploaded/hosting/hd_15_small.png'),
(63, '/uploaded/hosting/domain_56.png', '/uploaded/hosting/domain_56_small.png'),
(64, '/uploaded/hosting/www_20.png', '/uploaded/hosting/www_20_small.png'),
(65, '/uploaded/hosting/domain_36.png', '/uploaded/hosting/domain_36_small.png'),
(66, '/uploaded/hosting/www_28.png', '/uploaded/hosting/www_28_small.png'),
(67, '/uploaded/hosting/hd_20.png', '/uploaded/hosting/hd_20_small.png'),
(68, '/uploaded/hostingBitrix/domain.png', '/uploaded/hostingBitrix/domain_small.png'),
(69, '/uploaded/hostingBitrix/hd.png', '/uploaded/hostingBitrix/hd_small.png'),
(70, '/uploaded/hostingBitrix/www.png', '/uploaded/hostingBitrix/www_small.png'),
(71, '/uploaded/hostingBitrix/domain_11.png', '/uploaded/hostingBitrix/domain_11_small.png'),
(72, '/uploaded/hostingBitrix/www_19.png', '/uploaded/hostingBitrix/www_19_small.png'),
(73, '/uploaded/hostingBitrix/hd_8.png', '/uploaded/hostingBitrix/hd_8_small.png'),
(74, '/uploaded/servers/hd.png', '/uploaded/servers/hd_small.png'),
(76, '/uploaded/servers/cpu.png', '/uploaded/servers/cpu_small.png'),
(77, '/uploaded/servers/ram.png', '/uploaded/servers/ram_small.png'),
(78, '/uploaded/servers/cpu_68.png', '/uploaded/servers/cpu_68_small.png'),
(79, '/uploaded/servers/ram_51.png', '/uploaded/servers/ram_51_small.png'),
(80, '/uploaded/servers/hd_87.png', '/uploaded/servers/hd_87_small.png'),
(81, '/uploaded/servers/ram_70.png', '/uploaded/servers/ram_70_small.png'),
(82, '/uploaded/servers/cpu_76.png', '/uploaded/servers/cpu_76_small.png'),
(83, '/uploaded/servers/hd_76.png', '/uploaded/servers/hd_76_small.png'),
(84, '/uploaded/servers/ram_70_56.png', '/uploaded/servers/ram_70_56_small.png'),
(85, '/uploaded/servers/cpu_83.png', '/uploaded/servers/cpu_83_small.png'),
(86, '/uploaded/servers/hd_57.png', '/uploaded/servers/hd_57_small.png'),
(87, '/uploaded/gpu/ram.png', '/uploaded/gpu/ram_small.png'),
(88, '/uploaded/gpu/cpu.png', '/uploaded/gpu/cpu_small.png'),
(89, '/uploaded/gpu/hd.png', '/uploaded/gpu/hd_small.png'),
(90, '/uploaded/gpu/cpu_31.png', '/uploaded/gpu/cpu_31_small.png'),
(91, '/uploaded/gpu/ram_62.png', '/uploaded/gpu/ram_62_small.png'),
(92, '/uploaded/gpu/hd_70.png', '/uploaded/gpu/hd_70_small.png'),
(93, '/uploaded/gpu/net.png', '/uploaded/gpu/net_small.png'),
(94, '/uploaded/gpu/net_87.png', '/uploaded/gpu/net_87_small.png'),
(95, '/uploaded/gpu/net_72.png', '/uploaded/gpu/net_72_small.png'),
(96, '/uploaded/gpu/net_79.png', '/uploaded/gpu/net_79_small.png'),
(97, '/uploaded/gpu/ram_90.png', '/uploaded/gpu/ram_90_small.png'),
(98, '/uploaded/gpu/cpu_77.png', '/uploaded/gpu/cpu_77_small.png'),
(99, '/uploaded/gpu/hd_82.png', '/uploaded/gpu/hd_82_small.png'),
(100, '/uploaded/articles/1/img032.jpg', '/uploaded/articles/1/img032_small.jpg'),
(101, '/uploaded/articles/4/img03.jpg', '/uploaded/articles/4/img03_small.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_log`
--

CREATE TABLE `greeny_log` (
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userID` int(11) DEFAULT NULL,
  `ip` int(11) NOT NULL,
  `type` enum('info','warning','error') NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `greeny_log`
--

INSERT INTO `greeny_log` (`time`, `userID`, `ip`, `type`, `message`) VALUES
('2017-12-30 18:28:13', NULL, 2147483647, 'info', 'Authorisation, username=admin, userID=1'),
('2017-12-30 18:30:12', 1, 2147483647, 'info', 'Password changed'),
('2017-12-30 18:30:24', 1, 2147483647, 'info', 'User logout'),
('2017-12-30 18:30:50', NULL, 2147483647, 'info', 'Authorisation, username=admin, userID=1'),
('2018-01-09 13:50:39', NULL, 2147483647, 'warning', 'Authorisation failed, username=admin, code=6'),
('2018-01-09 13:52:00', NULL, 2147483647, 'info', 'Authorisation, username=admin, userID=1'),
('2018-01-15 20:38:08', NULL, 1539891281, 'warning', 'Authorisation failed, username=admin, code=6'),
('2018-01-15 20:38:11', NULL, 1539891281, 'warning', 'Authorisation failed, username=admin, code=6'),
('2018-02-03 10:43:56', NULL, 1539891281, 'warning', 'Authorisation failed, username=admin, code=6'),
('2018-02-03 10:44:24', NULL, 1539891281, 'info', 'Authorisation, username=admin, userID=1'),
('2018-02-15 05:07:11', NULL, 609615220, 'info', 'Authorisation, username=admin, userID=1'),
('2018-03-08 10:24:59', NULL, 2147483647, 'info', 'Authorisation, username=admin, userID=1'),
('2018-03-10 07:51:08', NULL, 2147483647, 'info', 'Authorisation, username=admin, userID=1'),
('2018-03-27 15:52:56', NULL, 1854653070, 'info', 'Authorisation, username=admin, userID=1'),
('2018-03-27 16:27:39', 1, 1854653070, 'info', 'Page edit success (pageID=29)'),
('2018-03-27 16:28:04', 1, 1854653070, 'info', 'Page edit success (pageID=29)'),
('2018-03-27 16:28:41', 1, 1854653070, 'info', 'Page edit success (pageID=29)'),
('2018-03-27 16:29:23', 1, 1854653070, 'info', 'Page edit success (pageID=29)'),
('2018-03-27 16:52:12', 1, 1854653070, 'info', 'Page edit success (pageID=29)'),
('2018-03-29 05:24:00', NULL, 1854653070, 'info', 'Authorisation, username=admin, userID=1'),
('2018-03-31 05:05:42', NULL, 1854465741, 'info', 'Authorisation, username=admin, userID=1'),
('2018-03-31 07:10:49', 1, 1854465741, 'info', 'Page edit success (pageID=30)'),
('2018-03-31 07:13:05', 1, 1854465741, 'info', 'Page edit success (pageID=30)'),
('2018-03-31 07:30:33', 1, 1854465741, 'info', 'Page edit success (pageID=31)'),
('2018-03-31 10:59:50', 1, 1854465741, 'info', 'Page edit success (pageID=31)'),
('2018-03-31 11:01:40', 1, 1854465741, 'info', 'Page edit success (pageID=31)'),
('2018-03-31 11:43:59', 1, 1854465741, 'info', 'Page edit success (pageID=32)'),
('2018-03-31 12:13:25', 1, 1854465741, 'info', 'Page edit success (pageID=32)'),
('2018-03-31 15:31:10', 1, 1854465741, 'info', 'Page edit success (pageID=32)'),
('2018-03-31 15:31:56', 1, 1854465741, 'info', 'Page edit success (pageID=32)'),
('2018-04-09 11:53:22', NULL, 2147483647, 'info', 'Authorisation, username=admin, userID=1'),
('2018-04-09 11:55:06', NULL, 2147483647, 'warning', 'Authorisation failed, username=iq21069, code=5'),
('2018-04-12 15:11:08', 1, 0, 'info', 'Page (pageID=37) deleted'),
('2018-04-12 15:11:13', 1, 0, 'info', 'Page wiped (pageID=37)'),
('2018-04-12 15:11:55', 1, 0, 'info', 'Page (pageID=20) deleted'),
('2018-04-12 15:12:03', 1, 0, 'info', 'Page edit success (pageID=36)'),
('2018-04-12 15:12:13', 1, 0, 'info', 'Page edit success (pageID=36)'),
('2018-04-12 15:44:38', 1, 0, 'info', 'Page edit success (pageID=36)');

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_modules`
--

CREATE TABLE `greeny_modules` (
  `moduleID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(127) NOT NULL DEFAULT '',
  `version` varchar(20) DEFAULT '',
  `rootAlias` varchar(50) DEFAULT NULL,
  `isVisible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `greeny_modules`
--

INSERT INTO `greeny_modules` (`moduleID`, `name`, `description`, `version`, `rootAlias`, `isVisible`) VALUES
(1, 'AnyEditor', 'Всё Редактор', '', NULL, 0),
(2, 'Articles', 'Статьи', '3.0', 'articles', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_modulesSettings`
--

CREATE TABLE `greeny_modulesSettings` (
  `settingID` int(11) NOT NULL,
  `moduleID` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `order` mediumint(9) NOT NULL DEFAULT '0',
  `isVisible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `greeny_modulesSettings`
--

INSERT INTO `greeny_modulesSettings` (`settingID`, `moduleID`, `name`, `value`, `description`, `order`, `isVisible`) VALUES
(1, 2, 'rootAlias', 'articles', 'Алиас во фрон-енде', 0, 1),
(2, 2, 'MessagesOnPageAdmin', '10', 'По сколько сообщений отображать на странице в админке', 20, 1),
(3, 2, 'MessagesOnPageFront', '10', 'По сколько сообщений отображать на странице на фрон-енде', 20, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_pageStructure`
--

CREATE TABLE `greeny_pageStructure` (
  `pageID` int(11) NOT NULL,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `caption` varchar(255) NOT NULL,
  `pageTypeID` int(11) DEFAULT NULL,
  `contentID` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(255) NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `isSection` tinyint(1) NOT NULL DEFAULT '0',
  `isLogin` tinyint(1) NOT NULL DEFAULT '0',
  `isNoindex` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `greeny_pageStructure`
--

INSERT INTO `greeny_pageStructure` (`pageID`, `parentID`, `caption`, `pageTypeID`, `contentID`, `title`, `description`, `keywords`, `priority`, `alias`, `isActive`, `isDeleted`, `isSection`, `isLogin`, `isNoindex`) VALUES
(1, 0, 'Главная страница', 1, 1, 'IQ Host', 'Главная страница', 'Главная страница', 1000, 'main', 1, 0, 1, 0, 0),
(2, 0, 'Документы', 1, 2, '', NULL, NULL, 0, 'docs', 1, 0, 0, 0, 0),
(3, 0, 'Новости', NULL, 0, '', NULL, NULL, 0, 'news', 1, 0, 1, 0, 0),
(4, 3, 'Новость №1', 2, 1, '', NULL, NULL, 0, 'news1', 1, 0, 0, 0, 0),
(5, 8, 'Акции №2', 3, 2, '', NULL, NULL, 0, 'actions2', 1, 0, 0, 0, 0),
(6, 8, 'Акция №1', 3, 1, 'Акция №1', NULL, NULL, 0, 'action1', 1, 0, 0, 0, 0),
(7, 0, 'Решения', NULL, 0, '', NULL, NULL, 0, 'solutions', 1, 0, 1, 0, 0),
(8, 0, 'Акции', NULL, 0, '', NULL, NULL, 0, 'actions', 1, 0, 1, 0, 0),
(9, 8, 'Акции №3', 3, 3, NULL, NULL, NULL, 0, 'akcii', 1, 0, 0, 0, 0),
(10, 3, 'Новость №2', 2, 2, '', NULL, NULL, 0, 'novostq-n2', 1, 0, 0, 0, 0),
(11, 3, 'Новость №3', 2, 3, '', NULL, NULL, 0, 'novostq-n3', 1, 0, 0, 0, 0),
(12, 7, 'Решения для виртуализации', 4, 1, '', NULL, NULL, 0, 'resheniya-dlya-virtyalizacii', 1, 0, 0, 0, 0),
(13, 7, 'Решения для 1С Бухгалтерии', 4, 2, '', NULL, NULL, 0, 'resheniya-dlya-1c', 1, 0, 0, 0, 0),
(14, 0, 'Услуги', NULL, 0, '', NULL, NULL, 0, 'services', 1, 0, 1, 0, 0),
(15, 14, 'Аренда сервера', 1, 4, '', NULL, NULL, 0, 'servers', 1, 0, 0, 0, 0),
(16, 14, 'Доменные имена', 1, 3, '', NULL, NULL, 0, 'domains', 1, 0, 0, 0, 0),
(17, 14, 'Хостинг сайтов', 1, 5, 'Виртуальный хостинг Ай-Кью Хост', 'Виртуальный хостинг в России', 'Хостинг онлйан магазин, безлимитный хостинг, SSL хостинг, дешевый хостинг', 0, 'hosting', 1, 0, 0, 0, 0),
(20, 0, 'Инструкции', 1, 6, NULL, NULL, NULL, 0, 'instructions', 1, 1, 0, 0, 0),
(21, 0, 'Способы оплаты', 1, 7, '', NULL, NULL, 0, 'payment', 1, 0, 0, 0, 0),
(22, 0, 'О нас', 1, 8, '', NULL, NULL, 0, 'about', 1, 0, 0, 0, 0),
(23, 0, 'Лицензии', 1, 9, '', NULL, NULL, 0, 'licenses', 1, 0, 0, 0, 0),
(24, 0, 'Дата-Центр', 1, 10, '', NULL, NULL, 0, 'data-center', 1, 0, 0, 0, 0),
(25, 0, 'Наши клиенты', 1, 11, '', NULL, NULL, 0, 'clients', 1, 0, 0, 0, 0),
(26, 0, 'Партнерская программа', 1, 12, '', NULL, NULL, 0, 'program', 1, 0, 0, 0, 0),
(27, 0, 'Наш логотип', 1, 13, '', NULL, NULL, 0, 'logo', 1, 0, 0, 0, 0),
(29, 14, 'Хостинг 1С Битрикс', 1, 14, NULL, NULL, NULL, 0, 'hosting-1s-bitriks', 1, 0, 0, 0, 0),
(30, 0, 'Аренда VPS', 1, 15, NULL, NULL, NULL, 0, 'arenda-vps', 0, 0, 0, 0, 0),
(31, 14, 'Аренда GPU', 1, 16, NULL, NULL, NULL, 0, 'arenda-gpu', 1, 0, 0, 0, 0),
(32, 14, 'Размещение серверов', 1, 17, NULL, NULL, NULL, 0, 'colocation', 1, 0, 0, 0, 0),
(33, 14, 'Почта', 7, 4, '', NULL, NULL, 0, 'mail', 1, 0, 0, 0, 0),
(34, 14, 'SSl сертификаты', 7, 5, '', NULL, NULL, 0, 'ssl', 1, 0, 0, 0, 0),
(35, 14, 'Техническая поддержка', 7, 6, '', NULL, NULL, 0, 'support', 1, 0, 0, 0, 0),
(36, 0, 'Инструкции', 1, 18, NULL, NULL, NULL, 0, 'articles', 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_pageTypes`
--

CREATE TABLE `greeny_pageTypes` (
  `pageTypeID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `handlerType` enum('module','table') NOT NULL DEFAULT 'module',
  `constantName` varchar(125) NOT NULL DEFAULT '',
  `noCreatePage` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Запрещает создание новых страниц данного типа из админки'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `greeny_pageTypes`
--

INSERT INTO `greeny_pageTypes` (`pageTypeID`, `name`, `description`, `handlerType`, `constantName`, `noCreatePage`) VALUES
(1, 'simplePages', 'Обычная страница', 'table', 'SIMPLE_PAGE_ID', 0),
(2, 'News', 'Страница новости', 'table', 'NEWS_ID', 0),
(3, 'Actions', 'Страница акции', 'table', 'ACTIONS_ID', 0),
(4, 'Solutions', 'Страница решения', 'table', 'SOLUTIONS_ID', 0),
(6, 'Servers', 'Страница аренды сервера', 'table', 'SERVER_ID', 0),
(7, 'Service', 'Страница услуг', 'table', 'SERVICE_ID', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_pwdRecoveryTokens`
--

CREATE TABLE `greeny_pwdRecoveryTokens` (
  `userID` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_routes`
--

CREATE TABLE `greeny_routes` (
  `routeID` int(11) NOT NULL,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `pattern` varchar(64) NOT NULL,
  `controller` varchar(64) NOT NULL,
  `action` varchar(64) NOT NULL DEFAULT 'Index',
  `defaultController` varchar(64) DEFAULT NULL,
  `defaultAction` varchar(64) DEFAULT NULL,
  `allowedGroups` varchar(128) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `greeny_routes`
--

INSERT INTO `greeny_routes` (`routeID`, `parentID`, `pattern`, `controller`, `action`, `defaultController`, `defaultAction`, `allowedGroups`) VALUES
(1, 0, '*', '0', '1', 'DefaultController', 'Index', '0'),
(2, 1, 'admin', 'AdminController', 'Index', NULL, NULL, '-1'),
(3, 2, 'upload', 'UploadAdminController', 'Index', NULL, NULL, '-1'),
(4, 2, 'cropthumbnail', 'CropThumbnailAdminController', 'Index', NULL, NULL, '-1'),
(5, 2, 'structure', 'StructureController', '2', NULL, NULL, '-1'),
(6, 2, 'anyeditor', 'AnyEditorController', '2', NULL, NULL, '-1'),
(7, 2, 'pwdrecovery', 'PasswordRecovery', '2', NULL, NULL, '-1'),
(8, 2, 'routing', 'RoutingController', 'Index', NULL, NULL, '-1'),
(9, 8, 'edit', 'RoutingController', 'Edit', NULL, NULL, '-1'),
(10, 8, 'add', 'RoutingController', 'Add', NULL, NULL, '-1'),
(11, 8, 'delete', 'RoutingController', 'Delete', NULL, NULL, '-1'),
(12, 2, '*', 'ModuleLinkController', 'Index', NULL, NULL, '-1'),
(13, 1, 'ajax', 'AjaxController', '1', NULL, NULL, '0'),
(14, 1, 'logout', 'FrontController', 'Logout', NULL, NULL, '0'),
(15, 1, 'contacts', 'ContactsController', 'Index', NULL, NULL, '0'),
(16, 1, 'getcaptcha', 'CaptchaGoogleController', 'GetCaptcha', NULL, NULL, '0'),
(17, 1, 'sitemap', 'SitemapController', 'Index', NULL, NULL, '0'),
(18, 1, '*', 'SimplePageController', 'Index', NULL, 'Index', '0'),
(19, 1, '', 'IndexController', 'Index', NULL, 'Index', '0'),
(20, 2, 'modules', 'ModulesController', '2', NULL, NULL, '0'),
(21, 2, 'profile', 'ProfileController', '2', NULL, NULL, '0'),
(22, 2, 'settings', 'SettingsController', 'Index', NULL, NULL, '0'),
(23, 2, 'users', 'UsersController', 'Index', NULL, NULL, '0'),
(24, 2, 'fieldsregistry', 'FieldsregistryController', 'Index', NULL, NULL, '0'),
(25, 2, 'maintenance', 'MaintenanceController', 'Index', NULL, NULL, '0'),
(26, 2, 'editor', 'EditorController', 'Index', NULL, NULL, '0'),
(27, 2, 'custommenu', 'CustomMenuAdminController', 'Index', NULL, NULL, '0'),
(28, 1, 'getimage', 'ImagesController', 'Index', NULL, NULL, '0'),
(29, 1, 'actions', 'ActionsController', 'Index', NULL, NULL, '0'),
(30, 29, '*', 'ActionsController', 'Item', NULL, NULL, '0'),
(31, 1, 'news', 'NewsController', 'Index', NULL, NULL, '0'),
(32, 31, '*', 'NewsController', 'Item', NULL, NULL, '0'),
(33, 1, 'solutions', 'SolutionsController', 'Index', NULL, NULL, '0'),
(34, 33, '*', 'SolutionsController', 'Item', NULL, NULL, '0'),
(35, 1, 'services', 'ServicesController', 'Index', NULL, NULL, '0'),
(36, 35, 'domains', 'ServicesController', 'Domains', NULL, NULL, '0'),
(37, 1, 'whois', 'ServicesController', 'WhoIS', NULL, NULL, '0'),
(38, 35, 'servers', 'ServicesController', 'Servers', NULL, NULL, '0'),
(39, 35, 'hosting', 'ServicesController', 'Hosting', NULL, NULL, '0'),
(40, 35, '*', 'ServicesController', 'Service', NULL, NULL, '0'),
(41, 35, 'hosting-1s-bitriks', 'ServicesController', 'HostingBitrix', NULL, NULL, '0'),
(42, 35, 'arenda-vps', 'ServicesController', 'VPS', NULL, NULL, '0'),
(43, 35, 'arenda-gpu', 'ServicesController', 'GPU', NULL, NULL, '0'),
(44, 35, 'colocation', 'ServicesController', 'Colocation', NULL, NULL, '0'),
(45, 1, 'articles', 'ArticlesControllerFront', 'Index', NULL, NULL, '0'),
(46, 2, 'Articles', 'ArticlesControllerAdmin', 'Index', NULL, NULL, '0');

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_settings`
--

CREATE TABLE `greeny_settings` (
  `settingID` int(11) NOT NULL,
  `groupID` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `isVisible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `greeny_settings`
--

INSERT INTO `greeny_settings` (`settingID`, `groupID`, `name`, `desc`, `value`, `isVisible`) VALUES
(1, 4, 'fromEmail', 'Почтовый адрес отправителя', 'grant@ayaco.ru', 1),
(2, 4, 'fromName', 'Имя отправителя', 'Грант', 1),
(3, 1, 'siteName', 'Название сайта', 'iqHost', 1),
(4, 1, 'version', 'Версия CMS', '2.0', 0),
(5, 2, 'defaultTitle', 'Заголовок страницы по умолчанию (если не задан в параметрах страницы)', 'Стандартный заголовок', 1),
(6, 2, 'defaultDescription', 'Мета-описание страницы по умолчанию (если не задан в параметрах страницы)', 'Описание страницы', 1),
(7, 2, 'defaultKeywords', 'Ключевые слова страницы по умолчанию (если не задан в параметрах страницы)', 'Ключевые слова', 1),
(8, 1, 'LOGIN_FRONT_END', 'Авторизация на фронт-енде', '0', 1),
(9, 3, 'maintenanceEnabled', 'Режим обслуживания ', '0', 1),
(10, 3, 'maintenancePageText', 'Содержимое страницы обслуживания ', '<h1>Сайт находится на техническом обслуживании</h1>', 1),
(11, 2, 'phone', 'Телефон', '+7 (495) 008-8346', 1),
(12, 2, 'email', 'Email', 'info@iqhost.ru', 1),
(13, 2, 'support', 'Техподдержка', 'support@iqhost.ru', 1),
(14, 2, 'company', 'Юридическое лицо', 'ООО «Ай-Кью Хостинг»', 1),
(15, 2, 'zip', 'Почтовый адрес', '125315, Россия, г. Москва, ул. Часовая, дом 14, офис. 16', 1),
(16, 2, 'address', 'Юридический адрес', '111024, Россия, г. Москва, Авиамоторная ул., дом 50, стр. 2', 1),
(17, 2, 'inn', 'ИНН', '7722378860', 1),
(18, 2, 'okpo', 'ОКПО', '05116635', 1),
(19, 2, 'vkontakte', 'vkontakte', '#', 1),
(20, 2, 'facebook', 'facebook', '#', 1),
(21, 2, 'instagram', 'instagram', '#', 1),
(22, 2, 'lk', 'Личный кабинет', 'https://billing.iqhost.ru/manager/billmgr', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `greeny_settingsGroups`
--

CREATE TABLE `greeny_settingsGroups` (
  `groupID` int(11) NOT NULL,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `greeny_settingsGroups`
--

INSERT INTO `greeny_settingsGroups` (`groupID`, `parentID`, `name`, `desc`) VALUES
(1, 0, 'general', 'Общие настройки'),
(2, 0, 'front', 'Настройки фронт-энда'),
(3, 0, 'admin', 'Настройки админки'),
(4, 1, 'mailer', 'Почтовая рассылка');

-- --------------------------------------------------------

--
-- Структура таблицы `Hosting`
--

CREATE TABLE `Hosting` (
  `hostingID` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `load` varchar(255) NOT NULL,
  `priceM` int(11) NOT NULL,
  `linkM` varchar(255) NOT NULL,
  `priceK` int(11) NOT NULL,
  `linkK` varchar(255) NOT NULL,
  `percentK` int(11) DEFAULT NULL,
  `priceP` int(11) NOT NULL,
  `linkP` varchar(255) NOT NULL,
  `percentP` int(11) DEFAULT NULL,
  `priceY` int(11) NOT NULL,
  `linkY` varchar(255) NOT NULL,
  `percentY` int(11) DEFAULT NULL,
  `diskSpace` varchar(255) DEFAULT NULL,
  `razmeshenie` varchar(255) DEFAULT NULL,
  `domains` varchar(255) DEFAULT NULL,
  `textWhite` text NOT NULL,
  `priority` int(11) DEFAULT '0',
  `isActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `Hosting`
--

INSERT INTO `Hosting` (`hostingID`, `title`, `summary`, `load`, `priceM`, `linkM`, `priceK`, `linkK`, `percentK`, `priceP`, `linkP`, `percentP`, `priceY`, `linkY`, `percentY`, `diskSpace`, `razmeshenie`, `domains`, `textWhite`, `priority`, `isActive`) VALUES
(1, 'IQ Start', '<p><strong>3 Гб</strong></p>\r\n<p>10 сайтов на аккаунте</p>\r\n<p>баз данных без огр.</p>\r\n<p>трафик не ограничен</p>', '10-300', 89, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D15%26period%3D1%26project%3D1&licence_agreement=on', 240, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D15%26period%3D3%26project%3D1&licence_agreement=on', 10, 453, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D15%26period%3D6%26project%3D1&licence_agreement=on', 15, 854, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D15%26period%3D12%26project%3D1&licence_agreement=on', 20, '3 Гб', '10 сайтов', '50 доменов', '<p><strong>Тариф для небольших сайтов:</strong></p>\r\n<p>Идеально подходит для любого стартап проекта, сайта визитки, сайта небольшого магазина или студии</p>\r\n<p><strong>Бонусы:</strong></p>\r\n<p>При оплате за год домен в зоне RU в подарок&nbsp;</p>\r\n<p><strong>Возможности:</strong></p>\r\n<p>Выделенный IP адрес, SSL сертификат</p>', 0, 1),
(2, 'IQ Premium', '<p><strong>25 Гб</strong></p>\r\n<p>50 сайтов на аккаунте</p>\r\n<p>Баз данных без огр.</p>\r\n<p>Трафик не ограничен</p>', '1000-5000', 339, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D40%26period%3D1%26project%3D1&licence_agreement=on', 915, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D40%26period%3D3%26project%3D1&licence_agreement=on', 10, 1728, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D40%26period%3D6%26project%3D1&licence_agreement=on', 20, 3254, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D40%26period%3D12%26project%3D1&licence_agreement=on', 20, '3 Гб', '10 сайтов', '50 доменов', '<p><strong>Тариф для больших сайтов:</strong></p>\r\n<p>Идеально подходит для тяжелых сайтов, видео порталов позволяет хранить большой объем данных</p>\r\n<p><strong>Бонусы при оплате тарифа сроком на 1 год</strong></p>\r\n<p>домен в зоне RU в подарок</p>\r\n<p>SSL сертификат</p>\r\n<p>выделенный IP адрес&nbsp;</p>\r\n<p><strong>Возможности:</strong></p>\r\n<p>Удаленный бекап по расписанию</p>\r\n<p>&nbsp;</p>', 3, 1),
(3, 'IQ Standart', '<p>10Гб места&nbsp;</p>\r\n<p>30 сайтов</p>\r\n<p>баз данных не огр.</p>\r\n<p>трафик не ограничен.&nbsp;</p>', '300-1000', 189, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D32%26period%3D1%26project%3D1&licence_agreement=on', 510, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D32%26period%3D3%26project%3D1&licence_agreement=on', 10, 964, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D32%26period%3D6%26project%3D1&licence_agreement=on', 15, 1814, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D32%26period%3D12%26project%3D1&licence_agreement=on', 20, NULL, NULL, NULL, '<p><strong>Тариф для популярных сайтов:</strong></p>\r\n<p>Идеально подходит для любого стартап проекта, сайта компании, сайта онлайн магазина или студии</p>\r\n<p><strong>Бонусы:</strong></p>\r\n<p>При оплате за год домен в зоне RU в подарок&nbsp;</p>\r\n<p><strong>Возможности:</strong></p>\r\n<p>Выделенный IP адрес, SSL сертификат, защита от вирусов</p>', 2, 1),
(4, 'IQ Maximum', '<p>50Гбайт места на диске</p>\r\n<p>100 сайтов на аккаунте&nbsp;</p>\r\n<p>баз данных без ограничений</p>\r\n<p>трафик не ограничен</p>', '2000-20000 ', 639, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D154%26period%3D1%26project%3D1&licence_agreement=on', 1726, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D154%26period%3D3%26project%3D1&licence_agreement=on', 10, 3259, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D154%26period%3D6%26project%3D1&licence_agreement=on', 15, 6135, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D154%26period%3D12%26project%3D1&licence_agreement=on', 20, NULL, NULL, NULL, '<p>&nbsp;<strong>Тариф для больших сайтов:</strong></p>\r\n<p>Идеально подходит для тяжелых сайтов, библиотек, позволяет хранить большой объем данных</p>\r\n<p><strong>Бонусы при оплате тарифа сроком на 1 год</strong></p>\r\n<p>домен в зоне RU в подарок</p>\r\n<p>SSL сертификат</p>\r\n<p>выделенный IP адрес&nbsp;</p>\r\n<p><strong>Возможности:</strong></p>\r\n<p>Удаленный бекап по расписанию</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `HostingBitrix`
--

CREATE TABLE `HostingBitrix` (
  `hostingID` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `load` varchar(255) NOT NULL,
  `priceM` int(11) NOT NULL,
  `linkM` varchar(255) NOT NULL,
  `priceK` int(11) NOT NULL,
  `linkK` varchar(255) NOT NULL,
  `percentK` int(11) DEFAULT NULL,
  `priceP` int(11) NOT NULL,
  `linkP` varchar(255) NOT NULL,
  `percentP` int(11) DEFAULT NULL,
  `priceY` int(11) NOT NULL,
  `linkY` varchar(255) NOT NULL,
  `percentY` int(11) DEFAULT NULL,
  `diskSpace` varchar(255) DEFAULT NULL,
  `razmeshenie` varchar(255) DEFAULT NULL,
  `domains` varchar(255) DEFAULT NULL,
  `textWhite` text NOT NULL,
  `priority` int(11) DEFAULT '0',
  `isActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `HostingBitrix`
--

INSERT INTO `HostingBitrix` (`hostingID`, `title`, `summary`, `load`, `priceM`, `linkM`, `priceK`, `linkK`, `percentK`, `priceP`, `linkP`, `percentP`, `priceY`, `linkY`, `percentY`, `diskSpace`, `razmeshenie`, `domains`, `textWhite`, `priority`, `isActive`) VALUES
(1, 'Bitrix -\"Первый\"', '<p>Диск:&nbsp; &nbsp; &nbsp; &nbsp;15 Гб</p>\r\n<p>Сайтов:&nbsp; &nbsp;3 шт</p>\r\n<p>Трафик:&nbsp; &nbsp;не огр.</p>\r\n<p>Лицензия: &quot;Первый Сайт&quot;</p>\r\n<p>&nbsp;</p>', '300-3000', 370, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D255%26period%3D1%26project%3D1&licence_agreement=on', 999, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D255%26period%3D3%26project%3D1&licence_agreement=on', 10, 1887, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D255%26period%3D6%26project%3D1&licence_agreement=on', 15, 3552, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D255%26period%3D12%26project%3D1&licence_agreement=on', 20, '3 Гб', '10 сайтов', '50 доменов', '<p><strong>Тариф для небольших сайтов:</strong></p>\r\n<p>Идеально подходит для редакции &quot;Первый сайт&quot;, сайта небольшого интернет магазина или корпоративного сайта на 50-200 страниц.</p>\r\n<p><strong>Бонусы при оплате за 1 год:</strong></p>\r\n<p>Редакция 1С Битрикс &quot;Первый сайт&quot; в подарок</p>\r\n<p><strong>Возможности:</strong></p>\r\n<p>Выделенный IPv4 адрес</p>\r\n<p>SSL сертификат</p>', 1, 1),
(2, 'Bitrix - \"Старт\"', '<p>Диск:&nbsp; &nbsp; &nbsp; &nbsp;25 Гб</p>\r\n<p>Сайтов:&nbsp; &nbsp;5 шт</p>\r\n<p>Трафик:&nbsp; &nbsp;не огр.</p>\r\n<p>Лицензия: Скидка 10%</p>', '1000-10000', 875, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dbasket%26pricelist%3D320%26period%3D1%26project%3D1&licence_agreement=on', 2363, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dbasket%26pricelist%3D320%26period%3D3%26project%3D1&licence_agreement=on', 10, 4371, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dbasket%26pricelist%3D320%26period%3D6%26project%3D1&licence_agreement=on', 15, 8400, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dbasket%26pricelist%3D320%26period%3D12%26project%3D1&licence_agreement=on', 20, '3 Гб', '10 сайтов', '50 доменов', '<p><strong>Тариф для небольших сайтов:</strong></p>\r\n<p>Идеально подходит для редакции &quot;Старт&quot;, сайта интернет магазина или корпоративного сайта на 100-500 страниц.</p>\r\n<p><strong>Бонусы при оплате за 1 год:</strong></p>\r\n<p>Редакция 1С Битрикс &quot;Старт&quot; в подарок</p>\r\n<p><strong>Возможности:</strong></p>\r\n<p>Выделенный IPv4 адрес</p>\r\n<p>SSL сертификат</p>', 3, 1),
(3, 'Bitrix - Pro', '<p>Диск:&nbsp; &nbsp; &nbsp; &nbsp;35 Гб</p>\r\n<p>Сайтов:&nbsp; &nbsp;10 шт</p>\r\n<p>Трафик:&nbsp; &nbsp;не огр.</p>\r\n<p>Лицензия: Скидка 15%</p>', '1000-10000', 500, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dbasket%26pricelist%3D265%26period%3D1%26project%3D1&licence_agreement=on', 1350, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dbasket%26pricelist%3D265%26period%3D3%26project%3D1&licence_agreement=on', 10, 2550, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D265%26period%3D6%26project%3D1&licence_agreement=on', 15, 5000, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D265%26period%3D12%26project%3D1&licence_agreement=on', 20, NULL, NULL, NULL, '<p>&nbsp;<strong>Тариф для нагруженных сайтов:</strong></p>\r\n<p>Поддерживает любую редакцию 1С-Битрикс, сайта&nbsp; интернет магазина или корпоративного сайта на 50-200 страниц.</p>\r\n<p><strong>Бонусы при оплате за 1 год:</strong></p>\r\n<p>Выделенный IPv4 адрес и SSL сертификат</p>\r\n<p><strong>Возможности:</strong></p>\r\n<p>Удаленное резервирование</p>\r\n<p>SLA соглашение</p>', 2, 1),
(4, 'Bitrix - виртуальная машина X1', '<p>HDD: 20 Гб (SSD)&nbsp;</p>\r\n<p>CPU:&nbsp; 2 x 3.0Ghz</p>\r\n<p>RAM: 2Gb</p>\r\n<p>Расположение: RU/NL</p>', '10000', 1250, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D255%26period%3D1%26project%3D1', 3500, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startpage%3Dvhost%26startform%3Dvhost%252Eorder%252Eparam%26pricelist%3D255%26period%3D3%26project%3D1&licence_agreement=on', 5, 6500, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D255%26period%3D6%26project%3D1&licence_agreement=on', 8, 12000, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D255%26period%3D12%26project%3D1&licence_agreement=on', 12, NULL, NULL, NULL, '<p>&nbsp;<strong>При аренде виртуальной машины:</strong></p>\r\n<p>- безлимитный трафик</p>\r\n<p>- Скидка на лицензию до 15%</p>\r\n<p>- Подключение к системе мониторинга по email/sms</p>\r\n<p><strong>Бонусы:</strong></p>\r\n<p>- выделенный IP адрес</p>\r\n<p>- SSL сертификат</p>', 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `HostingBitrixOptions`
--

CREATE TABLE `HostingBitrixOptions` (
  `optionID` int(11) NOT NULL,
  `hostingID` int(11) NOT NULL,
  `imageID` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `value` varchar(255) NOT NULL,
  `priority` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `HostingBitrixOptions`
--

INSERT INTO `HostingBitrixOptions` (`optionID`, `hostingID`, `imageID`, `title`, `value`, `priority`) VALUES
(4, 1, 27, 'Домены для парковки', 'без ограничений', 3),
(5, 1, 28, 'Место на диске', '15 Гбайт', 2),
(6, 1, 29, 'web домены', '3 сайта', 1),
(7, 2, 68, 'Домены для парковки', 'без ограничений', 3),
(8, 2, 69, 'Место на диске', '25 Гбайт', 2),
(9, 2, 70, 'web доменов', '5 сайтов', 1),
(10, 3, 71, 'Домены для парковки', 'без ограничений', 3),
(11, 3, 72, 'web домены', '10 сайтов', 1),
(12, 3, 73, 'Место на диске', '35 Гбайт', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `HostingOptions`
--

CREATE TABLE `HostingOptions` (
  `optionID` int(11) NOT NULL,
  `hostingID` int(11) NOT NULL,
  `imageID` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `priority` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `HostingOptions`
--

INSERT INTO `HostingOptions` (`optionID`, `hostingID`, `imageID`, `title`, `value`, `priority`) VALUES
(3, 1, 26, 'web домены', '10 сайтов', 1),
(4, 1, 57, 'Место на диске', '3 Гбайт', 2),
(5, 1, 58, 'Домены для парковки', 'без ограничений', 3),
(6, 3, 59, 'Домены для парковки', 'без ограничений', 3),
(7, 3, 60, 'Место на диске', '10 Гбайт', 2),
(8, 3, 61, 'web домены', '30 сайтов', 1),
(9, 2, 62, 'Место на диске', '25 Гбайт', 2),
(10, 2, 63, 'Домены для парковки', 'без ограничений', 3),
(11, 2, 64, 'web домены', '50 сайтов', 1),
(12, 4, 65, 'Домены для парковки', 'без ограничений', 3),
(13, 4, 66, 'web домены', '100 сайтов', 1),
(14, 4, 67, 'Место на диске', '50 Гбайт', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `MainMenu`
--

CREATE TABLE `MainMenu` (
  `entryID` int(11) NOT NULL,
  `pageID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `Menu`
--

CREATE TABLE `Menu` (
  `itemID` int(11) NOT NULL,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `menuListID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `imageID` int(11) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `isButton` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `Menu`
--

INSERT INTO `Menu` (`itemID`, `parentID`, `menuListID`, `name`, `alias`, `imageID`, `priority`, `isActive`, `isButton`) VALUES
(1, 0, 1, '-', '-', NULL, 0, 1, 0),
(2, 1, 1, 'Документы', '/docs/', NULL, 1, 1, 0),
(3, 0, 2, '-', '-', NULL, 0, 1, 0),
(4, 3, 2, 'Клиентам', '#', NULL, 1, 1, 0),
(5, 4, 2, 'Возможности хостинга', '/services/hosting/', NULL, 1, 1, 0),
(6, 3, 2, 'Партнерам', '#', NULL, 2, 1, 0),
(7, 0, 3, '-', '-', NULL, 0, 1, 0),
(8, 7, 3, 'Хостинг сайтов', '/services/hosting/', 46, 1, 1, 0),
(9, 7, 3, 'Хостинг 1С Битрикс', '/services/hosting-1s-bitriks/', 47, 2, 1, 0),
(10, 7, 3, 'Аренда сервера', '/services/servers/', 48, 3, 1, 0),
(11, 1, 1, 'Новости', '/news/', NULL, 4, 1, 0),
(12, 1, 1, 'Акции', '/actions/', NULL, 4, 1, 0),
(13, 1, 1, 'Решения', '/solutions/resheniya-dlya-virtyalizacii/', NULL, 2, 1, 0),
(14, 1, 1, 'Инструкции', '/articles/', NULL, 3, 1, 0),
(15, 1, 1, 'Способы оплаты', '/payment/', NULL, 5, 1, 0),
(16, 4, 2, 'Решения и услуги', '/solutions/resheniya-dlya-virtyalizacii/', NULL, 2, 1, 0),
(17, 4, 2, 'Инструкции пользователя', '/instructions/', NULL, 3, 1, 0),
(18, 4, 2, 'Оформление документов', '/docs/', NULL, 4, 1, 0),
(19, 6, 2, 'Наши клиенты', '/clients/', NULL, 1, 1, 0),
(20, 6, 2, 'Партнерская программа', '/program/', NULL, 2, 1, 0),
(21, 6, 2, 'Наш логотип', '/logo/', NULL, 3, 1, 0),
(23, 3, 2, 'Компания', '#', NULL, 0, 1, 0),
(24, 23, 2, 'О Нас', '/about/', NULL, 1, 1, 0),
(25, 23, 2, 'Лицензии', '/licenses/', NULL, 2, 1, 0),
(26, 23, 2, 'Дата-Центр', '#', NULL, 3, 1, 0),
(27, 7, 3, 'Аренда VPS', '/services/arenda-vps/', 49, 4, 1, 0),
(28, 7, 3, 'Аренда GPU', '/services/arenda-gpu/', 11, 5, 1, 0),
(29, 7, 3, 'Размещение серверов (Colocation)', '/services/colocation/', 12, 6, 1, 0),
(30, 7, 3, 'Доменные имена', '/services/domains/', 50, 7, 1, 0),
(31, 7, 3, 'Почта', '/services/mail/', 51, 8, 1, 0),
(32, 7, 3, 'SSl сертификаты', '/services/ssl/', 52, 9, 1, 0),
(33, 7, 3, 'Техническая поддержка', '/services/support/', 53, 10, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `MenuList`
--

CREATE TABLE `MenuList` (
  `menuListID` int(11) NOT NULL,
  `menuListName` varchar(255) NOT NULL,
  `menuListAlias` varchar(255) NOT NULL,
  `menuListComment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `MenuList`
--

INSERT INTO `MenuList` (`menuListID`, `menuListName`, `menuListAlias`, `menuListComment`) VALUES
(1, 'Верхнее меню', 'TOP_MENU', 'Верхнее меню'),
(2, 'Меню в футере', 'FOOTER_MENU', 'Меню в футере'),
(3, 'Меню услуг', 'SERVICE_MENU', 'Меню услуг');

-- --------------------------------------------------------

--
-- Структура таблицы `News`
--

CREATE TABLE `News` (
  `newsID` int(11) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `imageID` int(11) DEFAULT NULL,
  `publicDate` timestamp NULL DEFAULT NULL,
  `modifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `News`
--

INSERT INTO `News` (`newsID`, `summary`, `text`, `imageID`, `publicDate`, `modifiedDate`) VALUES
(1, 'описание', '<p>текст</p>', 4, '2017-09-06 20:00:00', '2017-09-07 04:21:37'),
(2, 'текст', '<p>&nbsp;текст</p>', 8, NULL, '2017-09-08 09:44:41'),
(3, 'текст', '<p>&nbsp;текст</p>', 9, NULL, '2017-09-08 09:45:09');

-- --------------------------------------------------------

--
-- Структура таблицы `Roles`
--

CREATE TABLE `Roles` (
  `role` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Roles`
--

INSERT INTO `Roles` (`role`, `name`, `level`) VALUES
('admin', 'Администратор', 100),
('moderator', 'Модератор', 80),
('user', 'Пользователь', 20);

-- --------------------------------------------------------

--
-- Структура таблицы `Servers`
--

CREATE TABLE `Servers` (
  `serverID` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `load` int(11) NOT NULL,
  `priceM` int(11) NOT NULL,
  `linkM` varchar(255) NOT NULL,
  `priceK` int(11) NOT NULL,
  `linkK` varchar(255) NOT NULL,
  `percentK` int(11) DEFAULT NULL,
  `priceP` int(11) NOT NULL,
  `linkP` varchar(255) NOT NULL,
  `percentP` int(11) DEFAULT NULL,
  `priceY` int(11) NOT NULL,
  `linkY` varchar(255) NOT NULL,
  `percentY` int(11) DEFAULT NULL,
  `diskSpace` varchar(255) DEFAULT NULL,
  `processorDevices` varchar(255) DEFAULT NULL,
  `ram` varchar(255) DEFAULT NULL,
  `textWhite` text NOT NULL,
  `priority` int(11) DEFAULT '0',
  `isActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Servers`
--

INSERT INTO `Servers` (`serverID`, `title`, `summary`, `load`, `priceM`, `linkM`, `priceK`, `linkK`, `percentK`, `priceP`, `linkP`, `percentP`, `priceY`, `linkY`, `percentY`, `diskSpace`, `processorDevices`, `ram`, `textWhite`, `priority`, `isActive`) VALUES
(1, 'IQ SRV1', '<p>Сервер начального уровня</p>\r\n<p>CPU: AMD&nbsp; Opteron 4x2.3Ghz</p>\r\n<p>RAM: 8Gb DDR3&nbsp;</p>\r\n<p>HDD: 146Gb SAS/15k</p>\r\n<p>Расположение:&nbsp; RU</p>', 2000, 2450, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D223%26period%3D1%26project%3D1&licence_agreement=on', 7350, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D223%26period%3D3%26project%3D1&licence_agreement=on', 10, 14700, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D223%26period%3D6%26project%3D1&licence_agreement=on', 15, 24500, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D223%26period%3D12%26project%3D1&licence_agreement=on', 20, '128 Гб  SSD', '4 x 2,4 Ghz', '8 Гб DDR3', '<p><strong>При аренде сервера:</strong></p>\r\n<p>Порт на 100 Мбит, безлимитный трафик, круглосуточно IP KVM по заявке. место для хранения резервных копий сервера, подключение к системе мониторинга и оповещения по sms.</p>\r\n<p><strong>Бонусы: </strong></p>\r\n<p>Выделенный IP адрес, SSL сертификат</p>', 0, 1),
(2, 'IQ SRV2', '<p>Сервер среднего уровня</p>\r\n<p>CPU:&nbsp; Xeon 1230v2 4x3.2Ghz</p>\r\n<p>RAM:&nbsp; 16Gb DDR3&nbsp;</p>\r\n<p>HDD:&nbsp; &nbsp;128Gb SSD</p>\r\n<p>Расположение: RU</p>', 10000, 5500, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D226%26period%3D1%26project%3D1&licence_agreement=on', 14850, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D226%26period%3D3%26project%3D1&licence_agreement=on', 10, 28050, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D226%26period%3D6%26project%3D1&licence_agreement=on', 15, 52800, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D226%26period%3D12%26project%3D1&licence_agreement=on', 20, '128 Гб  SSD', '4 x 2,4 Ghz', '8 Гб DDR3', '<p><strong>При аренде сервера:</strong></p>\r\n<p>Порт на 100 Мбит, безлимитный трафик, круглосуточно IP KVM по заявке. место для хранения резервных копий сервера, подключение к системе мониторинга и оповещения по sms.</p>\r\n<p><strong>Бонусы: </strong></p>\r\n<p>Выделенный IP адрес, SSL сертификат</p>', 0, 1),
(3, 'IQ SRV3', '<p>Cервер среднего уровня</p>\r\n<p>CPU: Xeon 1650v3 6x3.5Ghz</p>\r\n<p>RAM: 32Gb DDR3&nbsp;</p>\r\n<p>HDD: 2x256Gb SSD</p>\r\n<p>Расположение: RU</p>', 20000, 12500, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D229%26period%3D1%26project%3D1&licence_agreement=on', 33750, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D229%26period%3D3%26project%3D1&licence_agreement=on', 10, 63750, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D229%26period%3D6%26project%3D1&licence_agreement=on', 15, 120000, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D229%26period%3D12%26project%3D1&licence_agreement=on', 20, '128 Гб  SSD', '4 x 2,4 Ghz', '8 Гб DDR3', '<p><strong>При аренде сервера:</strong></p>\r\n<p>Порт на 100 Мбит, безлимитный трафик, круглосуточно IP KVM по заявке. место для хранения резервных копий сервера, подключение к системе мониторинга и оповещения по sms.</p>\r\n<p><strong>Бонусы: </strong></p>\r\n<p>Выделенный IP адрес, SSL сертификат</p>', 0, 1),
(4, 'IQ SX1', '<p>&nbsp;Мощный сервер</p>\r\n<p>CPU: Xeon 2690v2 16x2.9Ghz</p>\r\n<p>RAM: 128Gb DDR3</p>\r\n<p>HDD: 2x512Gb SSD</p>\r\n<p>Расположение : RU</p>', 50000, 23100, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D235%26period%3D1%26project%3D1&licence_agreement=on', 62370, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D235%26period%3D3%26project%3D1&licence_agreement=on', 10, 117810, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D235%26period%3D6%26project%3D1&licence_agreement=on', 15, 221760, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D235%26period%3D12%26project%3D1&licence_agreement=on', 20, NULL, NULL, NULL, '<p><strong>&nbsp;При аренде сервера:</strong></p>\r\n<p>- Порт 100Мбит безлимит/ 1Гбит (20Тб трафика)&nbsp;</p>\r\n<p>- Постоянное подключение IPMI (IPKVM)</p>\r\n<p>- Подключение к системе мониторинга по email/sms</p>\r\n<p><strong>Бонусы:</strong></p>\r\n<p>- выделенная сеть IPv6</p>\r\n<p>- SSL сертификат</p>', 0, 1),
(5, 'IQ SX2', '<p>&nbsp; Мощный сервер</p>\r\n<p>CPU: Xeon 2640v4 20x2.4Ghz</p>\r\n<p>RAM: 128Gb DDR3</p>\r\n<p>HDD: 2x960Gb SSD</p>\r\n<p>Расположение : RU</p>', 100000, 33248, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D238%26period%3D1%26project%3D1&licence_agreement=on', 89770, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D238%26period%3D3%26project%3D1&licence_agreement=on', 10, 169565, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D238%26period%3D6%26project%3D1&licence_agreement=on', 15, 319180, 'https://billing.iqhost.ru/manager/billmgr?func=register&redirect=startform%3Dquickorder%26redirect%3Dpayment%26pricelist%3D238%26period%3D12%26project%3D1&licence_agreement=on', 20, NULL, NULL, NULL, '<p>&nbsp;<strong>&nbsp;При аренде сервера:</strong></p>\r\n<p>- Порт 100Мбит безлимит/ 1Гбит (20Тб трафика)&nbsp;</p>\r\n<p>- Постоянное подключение IPMI (IPKVM)</p>\r\n<p>- Подключение к системе мониторинга по email/sms</p>\r\n<p><strong>Бонусы:</strong></p>\r\n<p>- выделенная сеть IPv6</p>\r\n<p>- SSL сертификат</p>', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `ServersOptions`
--

CREATE TABLE `ServersOptions` (
  `optionID` int(11) NOT NULL,
  `serverID` int(11) NOT NULL,
  `imageID` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `value` varchar(255) NOT NULL,
  `priority` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `ServersOptions`
--

INSERT INTO `ServersOptions` (`optionID`, `serverID`, `imageID`, `title`, `value`, `priority`) VALUES
(1, 1, 30, 'Дисковое пространство', '146 Гб  SAS', 1),
(2, 1, 31, 'Кол-во ядер и частота', '4 x 2,3 Ghz', 2),
(3, 1, 32, 'Оперативная память', '8 Гб DDR3', 3),
(4, 2, 74, 'Дисковое пространство', '128 Гб SSD', 1),
(6, 2, 76, 'Кол-во ядер и частота', '4 х 3.2Ghz', 2),
(7, 2, 77, 'Оперативная память', '16 Гб DDR3', 3),
(8, 3, 78, 'Кол-во ядер и частота', '6 х 3.5Ghz', 2),
(9, 3, 79, 'Оперативная память', '32Гб DDR3', 3),
(10, 3, 80, 'Дискововое пространство', '2 х 256 Гб SSD', 1),
(11, 4, 81, 'Оперативная память', '128 Гб DDR3', 3),
(12, 4, 82, 'Кол-во ядер и частота', '32 x 2.9Ghz ', 2),
(13, 4, 83, 'Дисковое пространство', '2 х 512 Гб SSD', 1),
(14, 5, 84, 'Оперативная память', '128 Гб DDR4', 3),
(15, 5, 85, 'Кол-во ядер и частота', '20 х 2.4Ghz', 2),
(16, 5, 86, 'Дисковое пространство', '2 х 960 Гб SSD', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `Service`
--

CREATE TABLE `Service` (
  `serviceID` int(11) NOT NULL,
  `textGray` text NOT NULL,
  `textWhite` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Service`
--

INSERT INTO `Service` (`serviceID`, `textGray`, `textWhite`) VALUES
(6, '<p>&nbsp;Текст страницы Техническая поддержка</p>', NULL),
(4, '<p>&nbsp;Текст страницы Почта</p>', NULL),
(5, '<p>&nbsp;Текст страницы SSl сертификаты</p>', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `simplePages`
--

CREATE TABLE `simplePages` (
  `simplePageID` int(11) NOT NULL,
  `pageContent` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `simplePages`
--

INSERT INTO `simplePages` (`simplePageID`, `pageContent`) VALUES
(1, '<p>Виртуальный хостинг сайтов на базе Linux идеально подходит для размещения вашего сайта, персонального блога или интернет магазина.</p>\r\n<p><strong>Основные характеристики:</strong></p>\r\n<p>Текстовый блок</p>'),
(2, '<h3>Программный продукт <strong>DataCore SANsymphony-V</strong>предназначен для построения высокопроизводительных отказоустойчивых систем хранения данных с блочным доступом через Fibre Channel или iSCSI.</h3>\r\n<p>SANsymphony-V работает на любом совместимом с Microsoft Windows Server 2008R2/2012/2012R2 оборудовании, что позволяет избавиться от привязки к определенному вендору. Основное предназначение систем на базе DataCore SANsymphony-V &mdash; виртуализация внешних систем хранения данных. Различные СХД могут иметь недостаточную производительность, функционал, проблемы с взаимной совместимость. При помощи DataCore SANsymphony-V можно объединить уже имеющиеся у заказчика СХД в единый пул с увеличением производительности (за счет кэширования в RAM и использования ярусного хранения данных), повышением уровня отказоустойчивости (за счет построения 2-узловых систем с синхронной репликацией) и добавлением необходимого функционала (например, поддержки VAAI/ODX)</p>\r\n<div class=\"img-area\">\r\n<div class=\"alignleft\"><img src=\"/uploaded/img03.jpg\" alt=\"image description\" width=\"540\" height=\"405\" /></div>\r\n<h3>Программный продукт <strong>DataCore SANsymphony-V</strong>предназначен для построения высокопроизводительных отказоустойчивых систем хранения данных с блочным доступом через Fibre Channel или iSCSI.</h3>\r\n<p>SANsymphony-V работает на любом совместимом с Microsoft Windows Server 2008R2/2012/2012R2 оборудовании, что позволяет избавиться от привязки к определенному вендору. Основное предназначение систем на базе DataCore SANsymphony-V &mdash; виртуализация внешних систем хранения данных. Различные СХД могут иметь недостаточную производительность, функционал, проблемы с взаимной совместимость. При помощи DataCore SANsymphony-V можно объединить уже имеющиеся у заказчика СХД в единый пул с увеличением производительности (за счет кэширования в RAM и использования ярусного хранения данных), повышением уровня отказоустойчивости (за счет построения 2-узловых систем с синхронной репликацией) и добавлением необходимого функционала (например, поддержки VAAI/ODX)</p>\r\n</div>\r\n<h3>Программный продукт <strong>DataCore SANsymphony-V</strong>предназначен для построения высокопроизводительных отказоустойчивых систем хранения данных с блочным доступом через Fibre Channel или iSCSI.</h3>\r\n<p>SANsymphony-V работает на любом совместимом с Microsoft Windows Server 2008R2/2012/2012R2 оборудовании, что позволяет избавиться от привязки к определенному вендору. Основное предназначение систем на базе DataCore SANsymphony-V &mdash; виртуализация внешних систем хранения данных. Различные СХД могут иметь недостаточную производительность, функционал, проблемы с взаимной совместимость. При помощи DataCore SANsymphony-V можно объединить уже имеющиеся у заказчика СХД в единый пул с увеличением производительности (за счет кэширования в RAM и использования ярусного хранения данных), повышением уровня отказоустойчивости (за счет построения 2-узловых систем с синхронной репликацией) и добавлением необходимого функционала (например, поддержки VAAI/ODX)</p>'),
(3, '<p>символьное имя, служащее для идентификации областей — единиц административной автономии в сети Интернет — в составе вышестоящей по иерархии такой области.</p>\r\n                <p>Каждая из таких областей называется доме́ном.</p>\r\n                <p>Общее пространство имён Интернета функционирует благодаря DNS — системе доменных имён.</p>'),
(4, '<p>Аренда физических серверов - отличное решение для маштабирования интернет проекта</p>\r\n<p>Высокая производительность и полный контроль в ваших руках.</p>\r\n<p><b>Месторасположение:</b></p>\r\n<p>Россия&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>DATA PRO (Tier3+)</strong></p>\r\n<p>Netherlands&nbsp; &nbsp;<strong>EVOSWITCH</strong></p>\r\n<p>USA&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong>DATA HOUSE</strong></p>\r\n<p>&nbsp;</p>'),
(5, '<p>Виртуальный хостинг сайтов на базе Linux идеально подходит для размещения вашего сайта, персонального блога или интернет магазина.</p>\r\n<p><strong>Основные характеристики:</strong></p>\r\n<ul>\r\n    <li>php 5.2 5.6 7.0 7.1</li>\r\n    <li>Поддержка SSL&nbsp;</li>\r\n    <li>Доступ к SSH</li>\r\n    <li>Защита от вирусов: virusdie</li>\r\n    <li>Поддержка большого числа CMS</li>\r\n    <li>Панель управления ISP Manager 5</li>\r\n</ul>\r\n<p><strong>Частые вопросы:</strong></p>\r\n<ul>\r\n    <li>&nbsp; &nbsp; &nbsp; Как заказать услугу &quot;хостинг сайта&quot;</li>\r\n    <li>&nbsp; &nbsp; &nbsp; Как продлить хостинг&nbsp;</li>\r\n    <li>&nbsp; &nbsp; &nbsp; Как сменить версию php</li>\r\n    <li>&nbsp; &nbsp; &nbsp; Как добавить ресурсы&nbsp; &nbsp;&nbsp;</li>\r\n</ul>\r\n<p>&nbsp; &nbsp; &nbsp;&nbsp;</p>\r\n<p>&nbsp;</p>'),
(6, '<p>Текст страницы</p>'),
(7, '<p>текст страницы</p>'),
(8, '<p>текст страницы</p>'),
(9, '<p>текст страницы</p>'),
(10, '<p>текст страницы</p>'),
(11, '<p>текст страницы</p>'),
(12, '<p>текст страницы</p>'),
(13, '<p>Текст страницы</p>'),
(14, '<p>Хостинг для платформы 1С Битрикс, вы можете выбрать удобный для вас вариант:</p>\r\n<p>- Виртуальный хостинг 1С Битрикс</p>\r\n<p>- Виртуальный сервер 1С Битрикс&nbsp;</p>\r\n<p><strong>В состав услуги входит:</strong></p>\r\n<p>- Последняя версия php 7.2</p>\r\n<p>- Opcache</p>\r\n<p>- Nginx/Apache/Mysql</p>\r\n<p>- Оптимизация&nbsp;</p>\r\n<p>- Лицензия (или скидка на покупку)</p>\r\n<p>- Техническая поддержка 24/7</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>'),
(15, ''),
(16, '<p>-&nbsp;<span style=\"font-family: Arial, Tahoma, Verdana, sans-serif, Helvetica, Calibri;\">GPU NVIDIA GTX1080 / GTX1080TI, GRID K2 и другие</span></p>\r\n<p>-&nbsp;<span style=\"font-family: Arial, Tahoma, Verdana, sans-serif, Helvetica, Calibri;\">Задачи машинного обучения, deep learning и нейросети</span></p>\r\n<p>-&nbsp;<span style=\"font-family: Arial, Tahoma, Verdana, sans-serif, Helvetica, Calibri;\">Виртуализация GPU и онлайн-игры со стримингом</span></p>\r\n<p>-&nbsp;<span style=\"font-family: Arial, Tahoma, Verdana, sans-serif, Helvetica, Calibri;\">Транскодинг видео с аппаратным ускорением (Wowza, ffmpeg)</span></p>\r\n<p>-&nbsp;<span style=\"font-family: Arial, Tahoma, Verdana, sans-serif, Helvetica, Calibri;\">HPC вычисления и CAD приложения</span></p>\r\n<p>- Виртуализация GPU и рабочих мест (десктопов)&nbsp;</p>'),
(17, '<p>Размещение сервера &quot;под ключ&quot; в датацентре - комплексная услуга Colocation от компании Ай-Кью Хост.</p>\r\n<p><strong>Состав услуги &quot;Colocation&quot;:</strong></p>\r\n<p>- Согласование технических условий</p>\r\n<p>- Доставка оборудования в Дата Центр</p>\r\n<p>- Установка и подключение</p>\r\n<p><strong>Параметры размещения оборудования:</strong></p>\r\n<p>- Размещение оборудования 19&quot; в стойке 42U</p>\r\n<p>- Среднее потребление 100Ватт/юнит</p>\r\n<p>- Безлимитный трафик на порту 100Мбит/сек</p>\r\n<p>- Бесплатный порт удаленного KVM (ILO/IPMI)</p>\r\n<p>- 1 IPv4 адрес на каждый порт</p>\r\n<p>- Бесплатный бекап от 50Гб</p>\r\n<p>- Доступ к оборудованию 24/7</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>'),
(18, '<p>&nbsp;текст страницы инструкции</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `Solutions`
--

CREATE TABLE `Solutions` (
  `solutionID` int(11) NOT NULL,
  `summary` text NOT NULL,
  `text` text NOT NULL,
  `imageID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `Solutions`
--

INSERT INTO `Solutions` (`solutionID`, `summary`, `text`, `imageID`) VALUES
(1, '<p>Конфигурации для малого, среднего бизнеса и крупных предприятий в зависимости от количества пользователей</p>\r\n<p>Без капитальных затрат на серверную инфраструктуру.</p>\r\n<p>Минимальные сроки реализации.</p>\r\n<p>Масштабируемость &mdash; возможность изменения ресурсов &quot;на лету&quot;, в зависимости от нужд заказчика.</p>\r\n<p>Все оборудование размещено в сертифицированном ЦОД TIER III, соответствующем повышенным требованиям к безопасности и защите данных.</p>', '<h3>Программный продукт <strong>DataCore SANsymphony-V</strong>предназначен для построения высокопроизводительных отказоустойчивых систем хранения данных с блочным доступом через Fibre Channel или iSCSI.</h3>\r\n<p>SANsymphony-V работает на любом совместимом с Microsoft Windows Server 2008R2/2012/2012R2 оборудовании, что позволяет избавиться от привязки к определенному вендору. Основное предназначение систем на базе DataCore SANsymphony-V &mdash; виртуализация внешних систем хранения данных. Различные СХД могут иметь недостаточную производительность, функционал, проблемы с взаимной совместимость. При помощи DataCore SANsymphony-V можно объединить уже имеющиеся у заказчика СХД в единый пул с увеличением производительности (за счет кэширования в RAM и использования ярусного хранения данных), повышением уровня отказоустойчивости (за счет построения 2-узловых систем с синхронной репликацией) и добавлением необходимого функционала (например, поддержки VAAI/ODX)</p>\r\n<div class=\"img-area\">\r\n<div class=\"alignleft\"><img src=\"/uploaded/img03(2).jpg\" alt=\"image description\" width=\"540\" height=\"405\" /></div>\r\n<h3>Программный продукт <strong>DataCore SANsymphony-V</strong>предназначен для построения высокопроизводительных отказоустойчивых систем хранения данных с блочным доступом через Fibre Channel или iSCSI.</h3>\r\n<p>SANsymphony-V работает на любом совместимом с Microsoft Windows Server 2008R2/2012/2012R2 оборудовании, что позволяет избавиться от привязки к определенному вендору. Основное предназначение систем на базе DataCore SANsymphony-V &mdash; виртуализация внешних систем хранения данных. Различные СХД могут иметь недостаточную производительность, функционал, проблемы с взаимной совместимость. При помощи DataCore SANsymphony-V можно объединить уже имеющиеся у заказчика СХД в единый пул с увеличением производительности (за счет кэширования в RAM и использования ярусного хранения данных), повышением уровня отказоустойчивости (за счет построения 2-узловых систем с синхронной репликацией) и добавлением необходимого функционала (например, поддержки VAAI/ODX)</p>\r\n</div>\r\n<h3>Программный продукт <strong>DataCore SANsymphony-V</strong>предназначен для построения высокопроизводительных отказоустойчивых систем хранения данных с блочным доступом через Fibre Channel или iSCSI.</h3>\r\n<p>SANsymphony-V работает на любом совместимом с Microsoft Windows Server 2008R2/2012/2012R2 оборудовании, что позволяет избавиться от привязки к определенному вендору. Основное предназначение систем на базе DataCore SANsymphony-V &mdash; виртуализация внешних систем хранения данных. Различные СХД могут иметь недостаточную производительность, функционал, проблемы с взаимной совместимость. При помощи DataCore SANsymphony-V можно объединить уже имеющиеся у заказчика СХД в единый пул с увеличением производительности (за счет кэширования в RAM и использования ярусного хранения данных), повышением уровня отказоустойчивости (за счет построения 2-узловых систем с синхронной репликацией) и добавлением необходимого функционала (например, поддержки VAAI/ODX)</p>', 23),
(2, '<p>Конфигурации для малого, среднего бизнеса и крупных предприятий в зависимости от количества пользователей</p>\r\n<p>Без капитальных затрат на серверную инфраструктуру.</p>\r\n<p>Минимальные сроки реализации.</p>\r\n<p>Масштабируемость &mdash; возможность изменения ресурсов &quot;на лету&quot;, в зависимости от нужд заказчика.</p>\r\n<p>Все оборудование размещено в сертифицированном ЦОД TIER III, соответствующем повышенным требованиям к безопасности и защите данных.</p>', '<h3>Программный продукт <strong>DataCore SANsymphony-V</strong>предназначен для построения высокопроизводительных отказоустойчивых систем хранения данных с блочным доступом через Fibre Channel или iSCSI.</h3>\r\n<p>SANsymphony-V работает на любом совместимом с Microsoft Windows Server 2008R2/2012/2012R2 оборудовании, что позволяет избавиться от привязки к определенному вендору. Основное предназначение систем на базе DataCore SANsymphony-V &mdash; виртуализация внешних систем хранения данных. Различные СХД могут иметь недостаточную производительность, функционал, проблемы с взаимной совместимость. При помощи DataCore SANsymphony-V можно объединить уже имеющиеся у заказчика СХД в единый пул с увеличением производительности (за счет кэширования в RAM и использования ярусного хранения данных), повышением уровня отказоустойчивости (за счет построения 2-узловых систем с синхронной репликацией) и добавлением необходимого функционала (например, поддержки VAAI/ODX)</p>\r\n<div class=\"img-area\">\r\n<div class=\"alignleft\"><img src=\"/uploaded/img03(2).jpg\" alt=\"image description\" width=\"540\" height=\"405\" /></div>\r\n<h3>Программный продукт <strong>DataCore SANsymphony-V</strong>предназначен для построения высокопроизводительных отказоустойчивых систем хранения данных с блочным доступом через Fibre Channel или iSCSI.</h3>\r\n<p>SANsymphony-V работает на любом совместимом с Microsoft Windows Server 2008R2/2012/2012R2 оборудовании, что позволяет избавиться от привязки к определенному вендору. Основное предназначение систем на базе DataCore SANsymphony-V &mdash; виртуализация внешних систем хранения данных. Различные СХД могут иметь недостаточную производительность, функционал, проблемы с взаимной совместимость. При помощи DataCore SANsymphony-V можно объединить уже имеющиеся у заказчика СХД в единый пул с увеличением производительности (за счет кэширования в RAM и использования ярусного хранения данных), повышением уровня отказоустойчивости (за счет построения 2-узловых систем с синхронной репликацией) и добавлением необходимого функционала (например, поддержки VAAI/ODX)</p>\r\n</div>\r\n<h3>Программный продукт <strong>DataCore SANsymphony-V</strong>предназначен для построения высокопроизводительных отказоустойчивых систем хранения данных с блочным доступом через Fibre Channel или iSCSI.</h3>\r\n<p>SANsymphony-V работает на любом совместимом с Microsoft Windows Server 2008R2/2012/2012R2 оборудовании, что позволяет избавиться от привязки к определенному вендору. Основное предназначение систем на базе DataCore SANsymphony-V &mdash; виртуализация внешних систем хранения данных. Различные СХД могут иметь недостаточную производительность, функционал, проблемы с взаимной совместимость. При помощи DataCore SANsymphony-V можно объединить уже имеющиеся у заказчика СХД в единый пул с увеличением производительности (за счет кэширования в RAM и использования ярусного хранения данных), повышением уровня отказоустойчивости (за счет построения 2-узловых систем с синхронной репликацией) и добавлением необходимого функционала (например, поддержки VAAI/ODX)</p>', 21);

-- --------------------------------------------------------

--
-- Структура таблицы `Users`
--

CREATE TABLE `Users` (
  `userID` int(11) NOT NULL,
  `login` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `FIO` varchar(255) NOT NULL,
  `role` enum('user','moderator','admin') NOT NULL DEFAULT 'user',
  `isActive` tinyint(1) NOT NULL,
  `registrationDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Users`
--

INSERT INTO `Users` (`userID`, `login`, `email`, `password`, `FIO`, `role`, `isActive`, `registrationDate`) VALUES
(1, 'admin', 'ie@committee.ru', '$2a$08$QnweAyKANxgCSx3DdMBLnuy2kJLD.HlYATHi6TEdcAogfRomkjDE.', 'admin', 'admin', 1, '2009-07-01 20:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `UsersIdentity`
--

CREATE TABLE `UsersIdentity` (
  `entryID` int(11) NOT NULL,
  `userID` int(11) NOT NULL DEFAULT '0',
  `identity` varchar(16) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `UsersInfo`
--

CREATE TABLE `UsersInfo` (
  `userID` int(11) NOT NULL,
  `FIO` varchar(255) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` enum('Мужской','Женский') NOT NULL DEFAULT 'Мужской'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `UsersInfo`
--

INSERT INTO `UsersInfo` (`userID`, `FIO`, `birthdate`, `gender`) VALUES
(1, 'Admin', '1979-06-26', 'Мужской');

-- --------------------------------------------------------

--
-- Структура таблицы `VPS`
--

CREATE TABLE `VPS` (
  `vpsID` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `load` int(11) NOT NULL,
  `priceM` int(11) NOT NULL,
  `linkM` varchar(255) NOT NULL,
  `priceK` int(11) NOT NULL,
  `linkK` varchar(255) NOT NULL,
  `percentK` int(11) DEFAULT NULL,
  `priceP` int(11) NOT NULL,
  `linkP` varchar(255) NOT NULL,
  `percentP` int(11) DEFAULT NULL,
  `priceY` int(11) NOT NULL,
  `linkY` varchar(255) NOT NULL,
  `percentY` int(11) DEFAULT NULL,
  `diskSpace` varchar(255) DEFAULT NULL,
  `processorDevices` varchar(255) DEFAULT NULL,
  `ram` varchar(255) DEFAULT NULL,
  `textWhite` text NOT NULL,
  `priority` int(11) DEFAULT '0',
  `isActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `VPS`
--

INSERT INTO `VPS` (`vpsID`, `title`, `summary`, `load`, `priceM`, `linkM`, `priceK`, `linkK`, `percentK`, `priceP`, `linkP`, `percentP`, `priceY`, `linkY`, `percentY`, `diskSpace`, `processorDevices`, `ram`, `textWhite`, `priority`, `isActive`) VALUES
(1, 'IQ VPS1', '<p><strong>3 Гб</strong></p>\r\n<p>10 сайтов на аккаунте</p>\r\n<p>Без ограничений трафика</p>', 2000, 2450, '', 7350, '', 10, 14700, '', 15, 24500, '', 20, '128 Гб  SSD', '4 x 2,4 Ghz', '8 Гб DDR3', '<p><strong>При аренде сервера:</strong></p>\r\n<p>Порт на 100 Мбит, безлимитный трафик, круглосуточно IP KVM по заявке. место для хранения резервных копий сервера, подключение к системе мониторинга и оповещения по sms.</p>\r\n<p><strong>Бонусы: </strong></p>\r\n<p>Выделенный IP адрес, SSL сертификат</p>', 0, 1),
(2, 'IQV VPS2', '<p><strong>4 Гб</strong></p>\r\n<p>10 сайтов на аккаунте</p>\r\n<p>Без ограничений трафика</p>', 3000, 3450, '', 8350, '', 10, 15700, '', 15, 25500, '', 20, '128 Гб  SSD', '4 x 2,4 Ghz', '8 Гб DDR3', '<p><strong>При аренде сервера:</strong></p>\r\n<p>Порт на 100 Мбит, безлимитный трафик, круглосуточно IP KVM по заявке. место для хранения резервных копий сервера, подключение к системе мониторинга и оповещения по sms.</p>\r\n<p><strong>Бонусы: </strong></p>\r\n<p>Выделенный IP адрес, SSL сертификат</p>', 0, 1),
(3, 'IQ VPS3', '<p><strong>6 Гб</strong></p>\r\n<p>10 сайтов на аккаунте</p>\r\n<p>Без ограничений трафика</p>', 4000, 4450, '', 10350, '', 10, 16700, '', 15, 26500, '', 20, '128 Гб  SSD', '4 x 2,4 Ghz', '8 Гб DDR3', '<p><strong>При аренде сервера:</strong></p>\r\n<p>Порт на 100 Мбит, безлимитный трафик, круглосуточно IP KVM по заявке. место для хранения резервных копий сервера, подключение к системе мониторинга и оповещения по sms.</p>\r\n<p><strong>Бонусы: </strong></p>\r\n<p>Выделенный IP адрес, SSL сертификат</p>', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `VPSOptions`
--

CREATE TABLE `VPSOptions` (
  `optionID` int(11) NOT NULL,
  `vpsID` int(11) NOT NULL,
  `imageID` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `value` varchar(255) NOT NULL,
  `priority` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `VPSOptions`
--

INSERT INTO `VPSOptions` (`optionID`, `vpsID`, `imageID`, `title`, `value`, `priority`) VALUES
(1, 1, 33, 'Дисковое пространство', '128 Гб  SSD', 0),
(2, 1, 34, 'Процессорные устройства', '4 x 2,4 Ghz', 0),
(3, 1, 35, 'Оперативная память', '8 Гб DDR3', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Actions`
--
ALTER TABLE `Actions`
  ADD PRIMARY KEY (`actionID`);

--
-- Индексы таблицы `Advantages`
--
ALTER TABLE `Advantages`
  ADD PRIMARY KEY (`advantageID`);

--
-- Индексы таблицы `Colocation`
--
ALTER TABLE `Colocation`
  ADD PRIMARY KEY (`colocationID`);

--
-- Индексы таблицы `ColocationOptions`
--
ALTER TABLE `ColocationOptions`
  ADD PRIMARY KEY (`optionID`),
  ADD KEY `FK_HostingOptions_Hosting` (`colocationID`);

--
-- Индексы таблицы `Domains`
--
ALTER TABLE `Domains`
  ADD PRIMARY KEY (`domainID`);

--
-- Индексы таблицы `GPU`
--
ALTER TABLE `GPU`
  ADD PRIMARY KEY (`gpuID`);

--
-- Индексы таблицы `GPUOptions`
--
ALTER TABLE `GPUOptions`
  ADD PRIMARY KEY (`optionID`),
  ADD KEY `FK_GPUOptions_GPU` (`gpuID`);

--
-- Индексы таблицы `greeny_articles`
--
ALTER TABLE `greeny_articles`
  ADD PRIMARY KEY (`articleID`),
  ADD KEY `FK_ASECTION` (`asectionID`);

--
-- Индексы таблицы `greeny_articles_sections`
--
ALTER TABLE `greeny_articles_sections`
  ADD PRIMARY KEY (`asectionID`);

--
-- Индексы таблицы `greeny_fieldsRegistry`
--
ALTER TABLE `greeny_fieldsRegistry`
  ADD PRIMARY KEY (`entryID`);

--
-- Индексы таблицы `greeny_files`
--
ALTER TABLE `greeny_files`
  ADD PRIMARY KEY (`fileID`);

--
-- Индексы таблицы `greeny_images`
--
ALTER TABLE `greeny_images`
  ADD PRIMARY KEY (`imageID`);

--
-- Индексы таблицы `greeny_modules`
--
ALTER TABLE `greeny_modules`
  ADD PRIMARY KEY (`moduleID`);

--
-- Индексы таблицы `greeny_modulesSettings`
--
ALTER TABLE `greeny_modulesSettings`
  ADD PRIMARY KEY (`settingID`);

--
-- Индексы таблицы `greeny_pageStructure`
--
ALTER TABLE `greeny_pageStructure`
  ADD PRIMARY KEY (`pageID`),
  ADD KEY `pageTypeID` (`pageTypeID`);

--
-- Индексы таблицы `greeny_pageTypes`
--
ALTER TABLE `greeny_pageTypes`
  ADD PRIMARY KEY (`pageTypeID`);

--
-- Индексы таблицы `greeny_pwdRecoveryTokens`
--
ALTER TABLE `greeny_pwdRecoveryTokens`
  ADD PRIMARY KEY (`token`);

--
-- Индексы таблицы `greeny_routes`
--
ALTER TABLE `greeny_routes`
  ADD PRIMARY KEY (`routeID`);

--
-- Индексы таблицы `greeny_settings`
--
ALTER TABLE `greeny_settings`
  ADD PRIMARY KEY (`settingID`);

--
-- Индексы таблицы `greeny_settingsGroups`
--
ALTER TABLE `greeny_settingsGroups`
  ADD PRIMARY KEY (`groupID`);

--
-- Индексы таблицы `Hosting`
--
ALTER TABLE `Hosting`
  ADD PRIMARY KEY (`hostingID`);

--
-- Индексы таблицы `HostingBitrix`
--
ALTER TABLE `HostingBitrix`
  ADD PRIMARY KEY (`hostingID`);

--
-- Индексы таблицы `HostingBitrixOptions`
--
ALTER TABLE `HostingBitrixOptions`
  ADD PRIMARY KEY (`optionID`),
  ADD KEY `FK_HostingOptions_Hosting` (`hostingID`);

--
-- Индексы таблицы `HostingOptions`
--
ALTER TABLE `HostingOptions`
  ADD PRIMARY KEY (`optionID`),
  ADD KEY `FK_HostingOptions_Hosting` (`hostingID`);

--
-- Индексы таблицы `MainMenu`
--
ALTER TABLE `MainMenu`
  ADD PRIMARY KEY (`entryID`),
  ADD KEY `pageID` (`pageID`);

--
-- Индексы таблицы `Menu`
--
ALTER TABLE `Menu`
  ADD PRIMARY KEY (`itemID`),
  ADD KEY `FK_Menu_MenuList` (`menuListID`);

--
-- Индексы таблицы `MenuList`
--
ALTER TABLE `MenuList`
  ADD PRIMARY KEY (`menuListID`);

--
-- Индексы таблицы `News`
--
ALTER TABLE `News`
  ADD PRIMARY KEY (`newsID`);

--
-- Индексы таблицы `Roles`
--
ALTER TABLE `Roles`
  ADD PRIMARY KEY (`role`),
  ADD KEY `Role` (`role`);

--
-- Индексы таблицы `Servers`
--
ALTER TABLE `Servers`
  ADD PRIMARY KEY (`serverID`);

--
-- Индексы таблицы `ServersOptions`
--
ALTER TABLE `ServersOptions`
  ADD PRIMARY KEY (`optionID`),
  ADD KEY `FK_HostingOptions_Hosting` (`serverID`);

--
-- Индексы таблицы `Service`
--
ALTER TABLE `Service`
  ADD PRIMARY KEY (`serviceID`);

--
-- Индексы таблицы `simplePages`
--
ALTER TABLE `simplePages`
  ADD PRIMARY KEY (`simplePageID`);

--
-- Индексы таблицы `Solutions`
--
ALTER TABLE `Solutions`
  ADD PRIMARY KEY (`solutionID`);

--
-- Индексы таблицы `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`userID`);

--
-- Индексы таблицы `UsersIdentity`
--
ALTER TABLE `UsersIdentity`
  ADD PRIMARY KEY (`entryID`),
  ADD KEY `userID` (`userID`);

--
-- Индексы таблицы `UsersInfo`
--
ALTER TABLE `UsersInfo`
  ADD PRIMARY KEY (`userID`);

--
-- Индексы таблицы `VPS`
--
ALTER TABLE `VPS`
  ADD PRIMARY KEY (`vpsID`);

--
-- Индексы таблицы `VPSOptions`
--
ALTER TABLE `VPSOptions`
  ADD PRIMARY KEY (`optionID`),
  ADD KEY `FK_VPSOptions_VPS` (`vpsID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Actions`
--
ALTER TABLE `Actions`
  MODIFY `actionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `Advantages`
--
ALTER TABLE `Advantages`
  MODIFY `advantageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `Colocation`
--
ALTER TABLE `Colocation`
  MODIFY `colocationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `ColocationOptions`
--
ALTER TABLE `ColocationOptions`
  MODIFY `optionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `Domains`
--
ALTER TABLE `Domains`
  MODIFY `domainID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `GPU`
--
ALTER TABLE `GPU`
  MODIFY `gpuID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `GPUOptions`
--
ALTER TABLE `GPUOptions`
  MODIFY `optionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `greeny_articles`
--
ALTER TABLE `greeny_articles`
  MODIFY `articleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `greeny_articles_sections`
--
ALTER TABLE `greeny_articles_sections`
  MODIFY `asectionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `greeny_fieldsRegistry`
--
ALTER TABLE `greeny_fieldsRegistry`
  MODIFY `entryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=302;
--
-- AUTO_INCREMENT для таблицы `greeny_files`
--
ALTER TABLE `greeny_files`
  MODIFY `fileID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `greeny_images`
--
ALTER TABLE `greeny_images`
  MODIFY `imageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT для таблицы `greeny_modules`
--
ALTER TABLE `greeny_modules`
  MODIFY `moduleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `greeny_modulesSettings`
--
ALTER TABLE `greeny_modulesSettings`
  MODIFY `settingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `greeny_pageStructure`
--
ALTER TABLE `greeny_pageStructure`
  MODIFY `pageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблицы `greeny_pageTypes`
--
ALTER TABLE `greeny_pageTypes`
  MODIFY `pageTypeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `greeny_routes`
--
ALTER TABLE `greeny_routes`
  MODIFY `routeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT для таблицы `greeny_settings`
--
ALTER TABLE `greeny_settings`
  MODIFY `settingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `greeny_settingsGroups`
--
ALTER TABLE `greeny_settingsGroups`
  MODIFY `groupID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `Hosting`
--
ALTER TABLE `Hosting`
  MODIFY `hostingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `HostingBitrix`
--
ALTER TABLE `HostingBitrix`
  MODIFY `hostingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `HostingBitrixOptions`
--
ALTER TABLE `HostingBitrixOptions`
  MODIFY `optionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `HostingOptions`
--
ALTER TABLE `HostingOptions`
  MODIFY `optionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `MainMenu`
--
ALTER TABLE `MainMenu`
  MODIFY `entryID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `Menu`
--
ALTER TABLE `Menu`
  MODIFY `itemID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT для таблицы `MenuList`
--
ALTER TABLE `MenuList`
  MODIFY `menuListID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `News`
--
ALTER TABLE `News`
  MODIFY `newsID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `Servers`
--
ALTER TABLE `Servers`
  MODIFY `serverID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `ServersOptions`
--
ALTER TABLE `ServersOptions`
  MODIFY `optionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `Service`
--
ALTER TABLE `Service`
  MODIFY `serviceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `simplePages`
--
ALTER TABLE `simplePages`
  MODIFY `simplePageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `Solutions`
--
ALTER TABLE `Solutions`
  MODIFY `solutionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `Users`
--
ALTER TABLE `Users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `UsersIdentity`
--
ALTER TABLE `UsersIdentity`
  MODIFY `entryID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `VPS`
--
ALTER TABLE `VPS`
  MODIFY `vpsID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `VPSOptions`
--
ALTER TABLE `VPSOptions`
  MODIFY `optionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `ColocationOptions`
--
ALTER TABLE `ColocationOptions`
  ADD CONSTRAINT `FK_ColocationOptions_Colocation` FOREIGN KEY (`colocationID`) REFERENCES `Colocation` (`colocationID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `GPUOptions`
--
ALTER TABLE `GPUOptions`
  ADD CONSTRAINT `FK_GPUOptions_GPU` FOREIGN KEY (`gpuID`) REFERENCES `GPU` (`gpuID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `greeny_articles`
--
ALTER TABLE `greeny_articles`
  ADD CONSTRAINT `FK_ASECTION` FOREIGN KEY (`asectionID`) REFERENCES `greeny_articles_sections` (`asectionID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `greeny_pageStructure`
--
ALTER TABLE `greeny_pageStructure`
  ADD CONSTRAINT `greeny_pageStructure_ibfk_1` FOREIGN KEY (`pageTypeID`) REFERENCES `greeny_pageTypes` (`pageTypeID`);

--
-- Ограничения внешнего ключа таблицы `HostingBitrixOptions`
--
ALTER TABLE `HostingBitrixOptions`
  ADD CONSTRAINT `FK_HostingBitrixOptions_HostingBitrix` FOREIGN KEY (`hostingID`) REFERENCES `HostingBitrix` (`hostingID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `HostingOptions`
--
ALTER TABLE `HostingOptions`
  ADD CONSTRAINT `FK_HostingOptions_Hosting` FOREIGN KEY (`hostingID`) REFERENCES `Hosting` (`hostingID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `MainMenu`
--
ALTER TABLE `MainMenu`
  ADD CONSTRAINT `MainMenu_ibfk_1` FOREIGN KEY (`pageID`) REFERENCES `greeny_pageStructure` (`pageID`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `ServersOptions`
--
ALTER TABLE `ServersOptions`
  ADD CONSTRAINT `FK_ServersOptions_Servers` FOREIGN KEY (`serverID`) REFERENCES `Servers` (`serverID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `UsersIdentity`
--
ALTER TABLE `UsersIdentity`
  ADD CONSTRAINT `UsersIdentity_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`);

--
-- Ограничения внешнего ключа таблицы `UsersInfo`
--
ALTER TABLE `UsersInfo`
  ADD CONSTRAINT `UsersInfo_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `VPSOptions`
--
ALTER TABLE `VPSOptions`
  ADD CONSTRAINT `FK_VPSOptions_VPS` FOREIGN KEY (`vpsID`) REFERENCES `VPS` (`vpsID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/**
*  Инициализатор контрола
*/
function DamnUploadInit(initObject)
{
    var controlName = initObject.name;
    var maxFilesNumber = 1;
    var formIndex = initObject.formIndex;
	var tableName = initObject.tableName;
	var filesNumber = initObject.filesNumber;
    var maxFilesNumber = 1;
	var blockFileUpload = false;
	if (typeof initObject.maxFilesNumber != 'undefined')
		maxFilesNumber = initObject.maxFilesNumber;
	if (filesNumber == maxFilesNumber)
		blockFileUpload = true;
	else
		maxFilesNumber -= filesNumber;

	if (typeof initObject.includeThumbCropDialog != "undefined" && initObject.includeThumbCropDialog)
	{
		var o = new Object();
		if (typeof initObject.thumbWidth != "undefined")
			o.thumbWidth = initObject.thumbWidth;
		if (typeof initObject.thumbHeight != "undefined")
			o.thumbHeight = initObject.thumbHeight;
		if (typeof initObject.thumbCropScript != "undefined")
			o.formAction = initObject.thumbCropScript;

		$.CropThumbnailDialog.Init(o);

	    $("#" + initObject.name + '_' + formIndex + "_controlContainer .thumbCropDialogActivator").bind(
			"click",
			o,
			function(e)
			{
				e.preventDefault();
				var a = $(this).siblings("a:eq(0)");
				if (typeof e.data != "undefined")
					$.CropThumbnailDialog.Init(o);

				$.CropThumbnailDialog.Show(a.attr("href"), a.children().attr("src"));
			}
		);
	}

	/*******************************************/
    var $fileField = $("#" + controlName + '_' + formIndex + "_fileInput");
	var $filesContainer = $("#" + controlName + '_' + formIndex + "_filesContainer");
//	var dropTarget = $("#" + controlName + '_' + formIndex + "_filesDropTarget");
	var $inputsPlaceholder = $("#" + "uploadedFilesInputsPlaceHolder_" + controlName + '_' + formIndex);
	//var $dropTarget = $("#" + controlName + "_filesDropTarget");
	var $dropTarget = $("#fsUploadProgress_" + controlName + "_" + formIndex);

	var $inputsPlaceholder = $("#" + "uploadedFilesInputsPlaceHolder_" + controlName);
//	var $dropTarget = $("#" + controlName + "_controlContainer");

	var f1 = function () { $(this).css({'border-color': $(this).data('oldBorderColor')}); return false; };
	$dropTarget.on('dragover', function () { var $this = $(this); $this.data('oldBorderColor', $this.css('border-color')); $this.css({'border-color': 'black'}); return false; })
		.on('dragend', f1)
		.on('drop', f1)
		.on('dragleave', f1);

    if (blockFileUpload)
		$fileField.attr('disabled', true);
	$("#" + controlName + '_' + formIndex + "_filesContainer .file-block input:checkbox").live("click", function () {
		if (this.checked) {
			filesNumber--;
			maxFilesNumber++;
			$fileField.attr('disabled', false);
		}
		else
		{
			filesNumber++;
			maxFilesNumber--;
			$fileField.attr('disabled', true);
		}
	});

	$fileField.damnUploader({
		url: initObject.uploadScript,
		fieldName: "Filedata",
		dropping: true,
		//dataType: "json",
		multiple: (maxFilesNumber > 1) ? true : false,
		dropBox: $dropTarget,
		data: {
			"mode": "damnupload"
		},
		onAllComplete :function() {
			//console.log('All uploads complete');
		},
		onSelect: function(file) {
			// create progress block
			var $progressContainer = $("#fsUploadProgress_" + controlName + '_' + formIndex);
			var $progressBlock = $('\n\
				<div class="upload-result">\n\
					<div class="upload-result-filename">' + file.name + '</div>\n\
					<progress value="0" max="100"></progress>\n\
				</div>\n\
\n');
			$progressBlock.appendTo($progressContainer);
			var $progressBar = $progressBlock.find("progress");
			//$progressBlock.find("progress").val(79);

			var uploadId = this.damnUploader('addItem', {
				file: file,
				onProgress: function(percents) {
					$progressBar.val(percents);
				},
				onComplete: function(successfully, data, errorCode) {
					if (successfully) {
						try {
							var ans = JSON.parse(data);
						}
						catch (e) {
							$.jGrowl("Ошибка получения ответа от сервера");
							throw(e);
						}
						if ("undefined" == typeof(ans.state))
						{
							$.jGrowl("Неверный ответ сервера");
							return;
						}

						if ("error" == ans.state)
						{
							$.jGrowl("Ошибка: " + ans.msg);
							$progressBar.replaceWith("<div class='upload-result-status error'>Ошибка: " + ans.msg + "</div>");
							return;
						}
						$progressBar.replaceWith("<div class='upload-result-status'>Завершено</div>");
						html = '<input type="hidden" name="' + tableName + '[' + formIndex + '][' + controlName + '__files][]" value="' + ans.filePath + '" />';

						$filesContainer.append(html);

					} else {
						$progressBar.replaceWith("<div class='upload-result-status'>Ошибка загрузки</div>");
						//console.info('Ошибка при загрузке. Код ошибки: '+errorCode); // errorCode содержит код HTTP-ответа, либо 0 при проблеме с соединением
					}
				}
			});

			// пустое поле input'а - файлы переданы через drag'n'drop
			if (this.val().length === 0)
			{
				var du = this;
				if (du.uploadTimer)
				{
					window.clearTimeout(du.uploadTimer);
					du.uploadTimer = 0;
				}

				// инициируем загрузку асинхронно
				du.uploadTimer = window.setTimeout(function() {
					du.damnUploader('startUpload');
					du.val('');
				}, 200);
			}

			return false; // отменить стандартную обработку выбора файла
		}
	});

	$fileField.on("change", function(){
		$fileField.damnUploader('startUpload');
		$fileField.val('');
	});

}
<link rel="stylesheet" type="text/css" href="{$STYLES_ALIAS}main.css" media="all"/>
<link href="{$STYLES_ALIAS}style.css/?family=Roboto:400,700" rel="stylesheet"> 

{if isset($STYLE_FILE)}
    {foreach from=$STYLE_FILE item=FILE}
        <link rel="stylesheet" href="{$FILE}" type="text/css" />
    {/foreach}
{/if}

{if isset($JS_VARS)}
    <script type="text/javascript">
        {foreach from=$JS_VARS item=JS_VAR}
            {foreach from=$JS_VAR item=JS_VAR_VALUE key=JS_VAR_NAME}
                    {if is_array($JS_VAR_VALUE) || is_object($JS_VAR_VALUE)}
    var {$JS_VAR_NAME} = {$JS_VAR_VALUE|@json_encode};
                    {else}
    var {$JS_VAR_NAME} = "{$JS_VAR_VALUE}";
                {/if}
            {/foreach}
        {/foreach}
    </script>
{/if}

<script type="text/javascript" src="{$SCRIPTS_ALIAS}jquery-2.0.3.min.js"></script>
<script src="{$SCRIPTS_ALIAS}jquery.main.js" defer></script> 
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/validator/validate.js"></script><!-- Валидатор форм -->
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/validator/validate.ext.js"></script><!-- Локализатор валидатора форм -->
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jquery.form.js"></script>
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jCache.js"></script>
<script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jquery.cachePost.js"></script>
<script type="text/javascript" src="{$SCRIPTS_ALIAS}script.js"></script>

{if isset($SCRIPT_FILE)}
    {foreach from=$SCRIPT_FILE item=FILE}
        <script type="text/javascript" src="{$FILE}"></script>
    {/foreach}
{/if}
{literal}
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" async>
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter47098620 = new Ya.Metrika({
                        id: 47098620,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () {
                        n.parentNode.insertBefore(s, n);
                    };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/47098620" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
{/literal}

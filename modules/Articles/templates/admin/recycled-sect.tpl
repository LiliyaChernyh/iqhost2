<h1>Корзина удаленных категорий статей</h1>

{if $SECTION_LIST|@count == 0}
Корзина пуста
{else}
    <form action="" method="POST">
    <div style="text-align: right; margin: -30px 0 10px 0">
        <input type="button" id="checkerBtn" value="Отметить все" />
    </div>
    <table id="entriesList" class="highlightable">
        <thead>
    		<tr>
    			<th width="5%">№</th>
                <th width="20%">Заголовок категории</th>
    			<th width="20%">Алиас</th>
                <th width="15%">Дата изменения</th>
    			<th width="5%"></th>
    		</tr>
    	</thead>
    	<tbody>
            {foreach from=$SECTION_LIST item=SECTION_ITEM key=I}
    		<tr>
                <td style="display: none;"><a href="{$MODULE_ALIAS}editsection/{$SECTION_ITEM.asectionID}/"></a></td>
    			<td>{$SECTION_ITEM.asectionID}</td>
                <td>{$SECTION_ITEM.caption}</td>
                <td>{$SECTION_ITEM.alias}</td>
                <td>{$SECTION_ITEM.modificationDate|@date_format:"%d.%m.%Y %H:%M"}</td>
    			<td class="skip"><input type="checkbox" name="toEdit[]" class="checks" value="{$SECTION_ITEM.asectionID}" /></td>
    		</tr>
    		{/foreach}
    	</tbody>
    </table>
    
    <div class="deleteBtn" style="margin-top: 10px; text-align: right">
    	<input type="submit" id="deleteItems" name="doWipe" value="Удалить отмеченные" disabled="disabled" />
    	<input type="submit" id="restoreItems" name="doRestore" value="Восстановить отмеченные" disabled="disabled" />
    </div>
    </form>
    
    {literal}
    <script type="text/JavaScript"> 
        $(document).ready(function()
    	{
    	    $('.checks').change(ActivateButtons);
    	    $('#checkerBtn').click(CheckUncheck);
    	});
    	
    	function CheckUncheck()
    	{
    	    if ($('#checkerBtn').val() == 'Отметить все')
    	    {
    	        $('.checks').attr('checked', true);
    	        $('#checkerBtn').val('Снять отметки');
    	    }
    	    else
    	    {
    	        $('.checks').attr('checked', false);
    	        $('#checkerBtn').val('Отметить все');
    	    }
    	    ActivateButtons();
    	}
    	
    	function ActivateButtons(event)
    	{
    	    var checked = $(".checks:checked").length;
    	    
    	    if (checked > 0)
    	    {
    	        $('#restoreItems').attr("disabled", false);
    	        $('#deleteItems').attr("disabled", false);
    	    }
    	    else
    	    {
    	        $('#restoreItems').attr("disabled", true);
    	        $('#deleteItems').attr("disabled", true);
    	    }
    	}
    </script>
    {/literal}
{/if}
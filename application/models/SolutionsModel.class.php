<?php

class SolutionsModel {

    public function GetSolutions($sectionId)
    {

        $query = 'SELECT ps.`caption`, ps.`alias`
                    FROM   `greeny_pageStructure`  ps LEFT JOIN  `Solutions` s  ON
                    ps.`contentID` = s.`solutionID`                    
                    WHERE ps.`parentID` = ' . $sectionId . ' AND ps.`isDeleted` = 0 AND  ps.`isActive` = 1 ORDER BY ps.`priority` DESC;';

        $result = DB::QueryToArray($query);
        return $result;
    }

    public function getBanner($bannerId)
    {
        $query = 'SELECT * FROM `BannersSolution` b WHERE b.`bannerSolutionID` = ' . $bannerId;
        return DB::QueryOneRecordToArray($query);
    }

    public function getAdvantages($solutionID)
    {
        $query = 'SELECT * FROM `AdvantagesSolution_Solutions` ass'
                .' LEFT JOIN `AdvantagesSolution` a ON ass.`advantageSolutionID` = a.`advantageSolutionID`'
                .' WHERE ass.`solutionID` = ' . $solutionID . ' AND a.`isActive` = 1';
        return DB::QueryToArray($query);
    }
    
    public function getMenuSolution($solutionID)
    {
        $query = 'SELECT * FROM `MenuSolution_Solutions` ms'
                .' LEFT JOIN `MenuSolution` m ON ms.`menuSolutionID` = m.`menuSolutionID`'
                .' WHERE ms.`solutionID` = ' . $solutionID . ' AND m.`isActive` = 1';
        return DB::QueryToArray($query);
    }
}

?>

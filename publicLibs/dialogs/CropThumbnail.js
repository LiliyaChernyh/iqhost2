(function($)
{
	$.CropThumbnailDialog = new Object();
	$.CropThumbnailDialog.initialized = false;
	$.CropThumbnailDialog.thumbPath = null;
	$.CropThumbnailDialog.Init = function (opt)
	{

		var defaults =
			{
				formAction		:	"/admin/cropthumbnail/",
				thumbWidth		:	150,
				thumbHeight		:	100,
				image			:	null
			};

		var options =  defaults;
		if (typeof(opt) != 'object') opt = { };
			options = $.extend(options, opt);
		this.options = options;

		if (this.initialized)
		{
			$("#cropThumbWidth").val(options.thumbWidth);
			$("#cropThumbHeight").val(options.thumbHeight);
			$("#thumbnailCropContainer")
				.css("width", this.options.thumbWidth)
				.css("height", this.options.thumbHeight);
			return;
		}

		var div  = $("<div>").attr("id", "CropThumbnailDialogContainer");
		var form = $("<form action='"+options.formAction+"' method='post'></form>")
						.append($("<div style='margin:5px;float:right'>")
							.append($("<input />").attr("type", "hidden").attr("name", "x1").attr("id", "cropThumbX1"))
							.append($("<input />").attr("type", "hidden").attr("name", "x2").attr("id", "cropThumbX2"))
							.append($("<input />").attr("type", "hidden").attr("name", "y1").attr("id", "cropThumbY1"))
							.append($("<input />").attr("type", "hidden").attr("name", "y2").attr("id", "cropThumbY2"))
							.append($("<input />").attr("type", "hidden").attr("name", "thumbWidth").attr("id", "cropThumbWidth").val(options.thumbWidth))
							.append($("<input />").attr("type", "hidden").attr("name", "thumbHeight").attr("id", "cropThumbHeight").val(options.thumbHeight))
							.append($("<input />").attr("type", "hidden").attr("name", "img").attr("id", "cropThumbImg"))
							.append($("<input />").attr("type", "hidden").attr("name", "thumb").attr("id", "cropThumbPath"))
							.append($("<input />")
										.attr("type", "submit")
										.attr("name", "save")
										.attr("id", "cropThumbSubmit")
										.attr("value", "ok")
										.css("padding","5px 20px")
										.css("margin","0")
										.css("display", "block")
									)
						);

		div.append("<table style='width:98%'>"+
						"<tbody><tr>"+
							"<td><div id='bigCroppingImageContainer'></div></td>"+
							"<td><div id='thumbnailCropContainer'></div></td>"+
						"</tr></tbody></table>");

		div.append(form);

		form.ajaxForm({
			beforeSubmit:  function()
				{
					if ($("#cropThumbX1").val() == $("#cropThumbX2").val())
					{
						alert("Не выбрана область обрезки");
						return false;
					}
					$("#cropThumbSubmit").attr("disabled", true);
				},
			success:       function(data, status)
				{
					$("#cropThumbSubmit").attr("disabled", false);
					if (typeof data.error != "undefined")
					{
						alert(data.error);
					}
					else if (typeof data.thumb != "undefined")
					{
						$("img[src*='"+data.thumb+"']")
							.attr("src", data.thumb + "?r=" + Math.random())
							/*.load(function() {
								var src = $(this).attr("src");
								var quest = src.indexOf("?");
								if (quest > 0)
								{
									src = src.substring(0, quest);
									$(this).attr("src", src);
								}
							})*/;
						$("#CropThumbnailDialogContainer").dialog("close");
					}
				},
			dataType:	"json"
		});

		div.find("#thumbnailCropContainer")
				.css("width", this.options.thumbWidth)
				.css("height", this.options.thumbHeight)
				.css("overflow", "hidden")

		if (options.image != null)
		{
			this.SetImage(options.image);
		}

		$("body").append(div);
		div.dialog({
			autoOpen:false,
			title 	:		"Редактирование уменьшенной копии изображения",
			resizable : ! ($.browser.msie && ($.browser.version == 6 || $.browser.version == 7))
			/*,

			buttons : {"Ok": function() {
				alert($(this).find("form").length);
			}}*/
		});

		this.initialized = true;
	};

	$.CropThumbnailDialog.SetImage = function (image)
	{
		if (!this.initialized)
			this.Init();
		var div = $("#CropThumbnailDialogContainer");
		var opt = this.options;
		div.find("#bigCroppingImageContainer")
				.empty()
				.append($("<img>")
							.attr("src", image)
							.attr("id", "bigCroppingImage")
							.load(function()
							{
							    var width = $(this).get(0).width;
							    var height = $(this).get(0).height;
								/*if (width + opt.thumbWidth > 3 * $(window).width() / 5)
								{
									width = 3 * $(window).width() / 5 - opt.thumbWidth;
									$(this).width(width);
								}*/

								$("#CropThumbnailDialogContainer")
									.dialog('option', 'width', width + opt.thumbWidth + 40)
									.dialog('option', 'height', height + 70)
									.dialog('option', 'position', 'center');

								var showPreview = function (coords)
								{
									if (parseInt(coords.w) > 0)
									{
										var rx = opt.thumbWidth / coords.w;
										var ry = opt.thumbHeight / coords.h;

										$('#thumbnailCrop').css({
											width: Math.round(rx * width) + 'px',
											height: Math.round(ry * height) + 'px',
											marginLeft: '-' + Math.round(rx * coords.x) + 'px',
											marginTop: '-' + Math.round(ry * coords.y) + 'px'
										});

										$("#cropThumbX1").val(coords.x);
										$("#cropThumbX2").val(coords.x2);
										$("#cropThumbY1").val(coords.y);
										$("#cropThumbY2").val(coords.y2);
									}
								}
								$(this).Jcrop({onChange: showPreview,
											   onSelect: showPreview,
												aspectRatio: opt.thumbWidth/opt.thumbHeight});


								$("#cropThumbImg").val($(this).attr("src"));
							})
						);
		div.find("#thumbnailCropContainer")
				.empty()
				.append($("<img>").attr("src", image).attr("id", "thumbnailCrop"));
	}

	$.CropThumbnailDialog.Show = function(image, thumb)
	{
		if (!this.initialized)
			this.Init();

		if (typeof image === "string")
			this.SetImage(image);

		this.thumbPath = thumb;
		$("#cropThumbPath").val(thumb);

		$("#CropThumbnailDialogContainer").dialog("open");
	}
})(jQuery);
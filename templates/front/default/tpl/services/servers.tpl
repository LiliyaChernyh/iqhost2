<div class="top-section">
    {if !empty($SERVERS)}
        <div class="container">
            <div class="twocolumns">
                <div class="text-holder">
                    <h1>{$PAGE.caption}</h1>
                    {$CONTENT.pageContent}
                </div>
                <div class="slick-slider">
                    {assign var='foo_start' value=0}
                    {foreach from=$SERVERS item=server key=key}
                        <div>
                            <form  class="hold-form" action="#">
                                <fieldset class="">
                                    <div class="info-holder">
                                        <div class="heading">
                                            <h2>{$server.title}</h2>
                                            <dl>
                                                <dt>Рекомендованная нагрузка:</dt>
                                                <dd>
                                                    <img src="{$IMAGES_ALIAS}/ico11.png" alt="image description" width="57" height="42">
                                                    <span class="text">
                                                        <strong>{$server.load}</strong>
                                                        <span>посетителей / сутки</span>
                                                    </span>
                                                </dd>
                                            </dl>
                                        </div>
                                        <ul class="price-list">
                                            <li class="active">
                                                <input type="radio" class="customRadio" name="radio{$key}" id="rad{$foo_start}" data-href="{$server.linkM}" value="0" />
                                                <label for="rad{$foo_start}"></label>
                                                <span class="price"><span class="num">{$server.priceM}</span> / месяц</span>
                                            </li>
                                            <li>
                                                <input type="radio" checked="checked" class="customRadio" name="radio{$key}" id="rad{$foo_start+1}" data-href="{$server.linkK}" value="1" />
                                                <label for="rad{$foo_start+1}"></label>
                                                <span class="price"><span class="num">{$server.priceK}</span> / квартал</span>
                                                <span class="comment">c выгодой <span>{$server.percentK}%</span></span>
                                            </li>
                                            <li>
                                                <input type="radio" class="customRadio" name="radio{$key}" id="rad{$foo_start+2}" data-href="{$server.linkP}" value="2" />
                                                <label for="rad{$foo_start+2}"></label>
                                                <span class="price"><span class="num">{$server.priceP}</span> / полугодие</span>
                                                <span class="comment">c выгодой <span>{$server.percentP}%</span></span>
                                            </li>
                                            <li>
                                                <input type="radio" class="customRadio" name="radio{$key}" id="rad{$foo_start+3}" data-href="{$server.linkY}" value="3" />
                                                <label for="rad{$foo_start+3}"></label>
                                                <span class="price"><span class="num">{$server.priceY}</span> / год</span>
                                                <span class="comment">c выгодой <span>{$server.percentY}%</span></span>
                                            </li>
                                        </ul>
                                        <div class="threecolumns">
                                            <div class="column">
                                                {$server.textWhite}
                                            </div>
                                            {if !empty($server.gallery)}
                                                <div class="column">
                                                    <ul class="description-list">
                                                        {foreach from=$server.gallery item=option}
                                                            <li>
                                                                <span class="ico-holder"><img src="/getimage/{$option.imageID}/" alt="{$option.title}"></span>
                                                                <span class="text">
                                                                    <strong>{$option.value}</strong>
                                                                    <span>{$option.title}</span>
                                                                </span>
                                                            </li>
                                                        {/foreach}                                   
                                                    </ul>
                                                </div>
                                            {/if}
                                            <div class="column">
                                                <a href="" class="btn" id="shopCart">Заказать</a>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        {assign var=foo_start value=$foo_start+4}
                    {/foreach}
                </div>
            </div>
            <div class="slider-nav">
                {foreach from=$SERVERS item=server}
                    <div>
                        <div class="block">
                            <div class="heading">
                                <h3>{$server.title}</h3>
                                <span class="price">{$server.priceM}</span>
                            </div>
                            {$server.summary}
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    {/if}
</div>
{include file="include-area.tpl"}
<?php
/**
 * Контроллер фронт-энда
 *
 * @version 1.0
 *
 */
class FrontController extends BaseFrontController
{
    /**
     * Пользователь
     *
     * @var User
     */
    protected $user = null;

    /**
     * Хлебные крошки
     *
     * @var User
     */
    protected $breadcrumbs = array();

    /**
     * Авторизация для фронт-енда
     *
     * @var User
     */
    protected $front_login = null;

    /**
     * Логирование страницы
     *
     * @var User
     */
    protected $login_page = null;
    protected $menuName = array();
    protected $menu = array();

    public function __construct() {
        @session_start();
        parent::__construct();

        $generalSettings = $this->settingsManager->GetSetting('general.LOGIN_FRONT_END');
        $this->front_login = $generalSettings['value'];

        $this->model = new FrontModel();
        try {
            $this->login_page = $this->CheckLoginPages(Request::GetAliases());
        } catch (Exception $e) {
            $this->login_page = true;
        }

        //авторизация во фронте
        if ($this->front_login || $this->login_page) {
            // Пробуем восстановить из сессии
            $this->user = new User();

            if (!$this->user->IsAuthorised()) {
                // Попытка авторизации через куки
                $this->TryAuthorise();
            }
        }
        $this->menuName = $this->model->GetMenuName();
        if (!empty($this->menuName)) {
            foreach ($this->menuName as $key => $value) {
                $this->menu[$value['menuListAlias']] = $this->model->GetMenuByAlias($value['menuListAlias']);
            }
        }
        $menu = $this->menu;
        Menu::setActiveElementsInFirstLevel($menu);
        ViewData::Assign('CUSTOMMENU', $menu);
//        FB::log($menu,'menu');
        ViewData::Assign('headerImage', AppSettings::$IMAGES_ALIAS . 'bg-grup.jpg');
        
        $advantagesTable = DBTableManager::getInstance('Advantages');
        $advantagesTable -> SortOrder = 'priority ASC';
        $advantages = $advantagesTable -> Select(array('isActive' => 1), 4);
        $ps = PagesStructure::getInstance();
        $action = $ps->GetPageByFullAlias('/actions/');
        $childPagesActions = $ps->GetChildren($action->pageID, array('isActive' => 1, 'isDeleted' => 0));
        $pagesActions = array();
        foreach ($childPagesActions as $child) {
            $childPage = new Page($child['pageID']);
            $pageContent = $childPage->Content();
            $pagesActions[] = array(
                'pageID'  => $childPage->pageID,
                'alias'   => $childPage->alias,
                'caption' => $child['caption'],
                'imageID' => $pageContent['imageID'],
            );
        }

        $actions = $pagesActions;
        ViewData::Assign('ADVANTAGES', $advantages);  
        ViewData::Assign('ACTIONS', $actions); 
        
        $this->DBBP = new DBBP();
    }

    public function Complete() {
        parent::Complete();

        //хлебные крошки
        if (empty($this->page->pageID)) return;
        $bc = $this->SetBreadCrumbs($this->page->pageID, 0);
        $bc = array_merge($this -> breadcrumbs, $bc);
        $breadcrumbs = array_reverse($bc);
        ViewData::Assign('BREADCRUMBS', $breadcrumbs);
    }

    /**
     *  Функция построения хлебных крошек
     *
     */
    protected function SetBreadCrumbs($pageID, $level)
    {
        $pageStructure = PagesStructure::getInstance();

        $query = "SELECT * FROM `greeny_pageStructure` WHERE `pageID` = ".$pageID;
        $result = DB::QueryToArray($query);
        $array = array();

        $array[$level]['name'] = $result[0]['caption'];
        $array[$level]['alias'] = $pageStructure->GetFullAlias($result[0]['pageID']);

        if ($result[0]['parentID'] != 0)
        {
                $array = $array + $this->SetBreadCrumbs($result[0]['parentID'], $level + 1);
        }

        return $array;
    }

    /**
     *  Авторизация через куки
     *
     */
    public function TryAuthorise()
    {
        $res = Authorisation::CookieAuthorisation();

        if (is_array($res))
        {
            $this -> user -> InitUser($res);
        }
    }

    /**
     * Переопределяем инициализирующий экшн
     *
     */
    public function Init()
    {
        if (($this->front_login || $this->login_page) && !$this -> user -> IsAuthorised()){
            return $this -> Authorisation();
        }else{
            return new EmptyActionResult();
        }
    }

    /**
     * Action
     * Форма авторизации
     *
     * @return ActionResult
     */
    public function Authorisation()
    {
        $res = Authorisation::PostAuthorisation(false);

        if (is_array($res)){
            $this -> user -> InitUser($res);
            throw new Exception('{"msg":"ok","text":"welcome"}');
        }elseif(is_numeric($res) && ($res != 1)){
            throw new Exception('{"error":"ошибка","errno":'.$res.'}');
        }

        if ($_SERVER['REQUEST_URI'] == '/registration/') return $this -> Registration();

        //если ajax запрос был, то возращаем json
        ViewData::Assign('FROTN_LOGIN', $this->front_login);
        $this -> view = new AuthorisationView();
        $this -> view -> StopExecution();
        return $this -> view;
    }

    /**
     * Action
     * Выход пользователя
     *
     * @return ActionResult
     */
    public function Logout()
    {
        DBLog::Info('User logout');
        $this->userInfo = null;
        unset($_SESSION['userInfo']);
        unset($_SESSION['userID']);

        //чистим куки
        setcookie('username', '', 1220000000, '/');
        setcookie('userpass', '', 1220000000, '/');

        return new RedirectActionResult(AppSettings::$ROOT_ALIAS);
    }

    /**
     * Action
     * Регистрация пользователя
     *
     * @return ActionResult
     */
    public function Registration()
    {
        if (Request::IsAsync()){
            if (Request::$POST['login']){
                //проверяем капчу
                if (!Captcha::CheckCaptcha(Request::$POST['captcha']))
                    throw new Exception('{"error":"captcha","text":"Неверно введена капча"}');

                $password = strip_tags(Request::$POST['password']);
                $user_reg = array();
                $user_reg['login'] = strip_tags(trim(Request::$POST['login']));
                $user_reg['password'] = Password::HashPassword($password);
                $user_reg['FIO'] = strip_tags(trim(Request::$POST['FIO']));
                $user_reg['email'] = strip_tags(trim(Request::$POST['email']));

                //проверяем логин
                if (!$this->model->checkLoginUser($user_reg['login'])){
                    //проверяем E-mail на уникальность
                    if(!$this->model->checkEmailUser($user_reg['email'])){
                        //записываем в базу
                        if ($this->model->saveUser($user_reg)){
                            try
                            {
                                //отправляем почту админу
                                $mailer = new BitPlatformMailer();
                                $generalSettings = $this -> settingsManager -> GetSettingsAssoc('general');
                                $mailer -> Subject = '['.$generalSettings['general']['siteName'].'] Сообщение с сайта';
                                $mailer -> Body = '

        На сайте зарегистрирован новый пользователь:

        ФИО: '.$user_reg['FIO'].'
        login: '.$user_reg['login'].'
        E-mail: '.$user_reg['email'].'

        C уважением, BitPlatform.';

                                $emails = $generalSettings['general']['mailer']['fromEmail'];
                                $emails = explode(';', $emails);

                                foreach ($emails as $email)
                                {
                                    $email = trim($email);
                                    if (!empty($email))
                                    {
                                        $mailer -> ClearAddresses();
                                        $mailer -> AddAddress($email);
                                    }
                                }
                                $mailer -> Send();

                                //отправляем почту пользователю
                                $mailer = new BitPlatformMailer();
                                $mailer -> ContentType = 'text/html';
                                $mailer -> Subject = '['.$generalSettings['general']['siteName'].'] Сообщение с сайта';
                                $mailer -> Body = $mailer -> Body = '

        Вы зарегистрировались на '.$generalSettings['general']['siteName'].', как только администратор потвердит ваш аккаунт, вам будет выслано ответное письмо. Приятного вам дня!)

        Ваши данные:
        login: '.$user_reg['login'].'
        password: '.$password.'

        C уважением, '.$generalSettings['general']['siteName'].'.';

                                $mailer -> ClearAddresses();
                                $mailer -> AddAddress($user_reg['email']);
                                $result = $mailer -> Send();
                            }
                            catch(Exception $e)
                            {
                                throw new Exception('{"error":"0","text":"Ошибка отправки письма"}');
                            }

                            throw new Exception('{"msg":"ok","text":"Спасибо за регистрацию. Вы получите уведомление на указанный почтовый адрес после того, как администратор подтвердит вашу учетную запись."}');
                        }else throw new Exception('{"error":"0","text":"Ошибка записи в базу!"}');
                    }else throw new Exception('{"error":"email","text":"Такой E-mail уже существует"}');
                }else throw new Exception('{"error":"login","text":"Текущий логин занят, попробуйте другой."}');
            }else throw new Exception('{"error":"0","text":"Неизвестная ошибка!"}');
        }

        $this -> view = new RegistrationView();
        $this -> view -> StopExecution();
        return $this -> view;
    }

    /**
     * Проверка страниц на их авторизацию (доступ к каждой отдельно)
     *
     */
    public function CheckLoginPages($str)
    {
        //если отключена авторизация для фронт-енда, то возращаем true
        if ($this->front_login) return true;

        //разрешаем доступ определенным урлам 1-го уровня (например, регистрации на фронт-енде),
        //если это не страница (что бы не возвращала 404 ошибку)
        $ar_noCheckLoginPagesURL = array('registration');
        if (isset($str[0])){
            //для абстрактных страниц 1-го уровня
            if ((count($str) == 1) && in_array($str[0], $ar_noCheckLoginPagesURL)) return false;
            //для главной страницы
            if (empty($str[0])) return false;
        }

        try{
            $aliases = $this->model->getLoginPageAliases();
            if (is_array($aliases)){
                $fullAlias = new Page($str);
                if (isset($fullAlias->pageID) && in_array($fullAlias->pageID, $aliases)) return true;
            }
        }catch(Exception $e){
            return false;
        }

        return false;
    }
}
?>
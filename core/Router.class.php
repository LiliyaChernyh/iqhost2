<?php
/**
 * Роутер битплатформы
 *
 * @version 1.1
 *
 */
class Router implements IRouter
{
    /**
     * Провайдер таблицы маршрутов
     *
     * @var IRoutingTableProvider
     */
    private $routingTableProvider;

    /**
     * Дерево маршрутов
     *
     * @var array
     */
    private $routingTree;

    /**
     * Имя файла, в который сохраняется скомпилированные маршруты
     *
     * @var string
     */
    private $cacheFile = 'CompiledRoutes';

    /**
     * Массив с уже вычислкнными маршрутами
     *
     * @var array
     */
    private $compiledRoutes = null;

    /**
     * Флаг для включение и выключения кеширования
     *
     * @var bool
     */
    private $useCompiledRoutes = false;

    public function __construct(IRoutingTableProvider &$routingTableprovider)
    {
        $this -> routingTableProvider = &$routingTableprovider;
        $this -> routingTree = &$this -> routingTableProvider -> GetRoutingTree();

	//проверяем на наличие запятой (нескольких алиасов) и делим дерево, если нашли
        $this -> splitRoutingTree($this -> routingTree);

        //добавляем руты подключенных(!) модулей, если они не прописаны явно
        //$this -> addRouteActiveModule($this -> routingTree);

        if ($this -> useCompiledRoutes)
        {
            // Загрузим маршруты из кеша
            $cache = ObjectsCache::GetCache($this -> cacheFile);

            $this -> compiledRoutes = array();
            if (is_array($cache) && !empty($cache))
            {
                $this -> compiledRoutes = $cache;
            }
        }
    }

    /**
     * @return $routingTree
     *
     */
//    public function addRouteActiveModule(&$arrayRoutingTree)
//    {
//        $query = 'SELECT
//                    gm.`moduleID`,
//                    gms.`name`,
//                    gms.`value`
//                  FROM `'.TablesNames::$MODULES_TABLE_NAME.'` gm
//                  INNER JOIN `'.TablesNames::$MODULES_SETTINGS_TABLE_NAME.'` gms ON gm.`moduleID` = gms.`moduleID`
//                  WHERE
//                    gms.`name` = "pattern" OR
//                    gms.`name` = "controller" OR
//                    gms.`name` = "action"
//                  ';
//
//        $res = DB::QueryToArray($query);
//
//        $ar = array();
//        foreach ($res as $item)
//            $ar[$item['moduleID']][$item['name']] = $item['value'];
//
//        foreach ($ar as $item)
//            $arrayRoutingTree[0]['children'][] = $item;
//    }

    /**
     * @return $routingTree
     *
     */
    public function splitRoutingTree(&$arrayRoutingTree)
    {
        foreach ($arrayRoutingTree as $key => &$ar){
            if (isset($ar['children'])){
                //trace($ar);
                $ar['children'] = $this -> splitRoutingTree($ar['children']);
            }
            if (isset($ar['pattern']) && strpos($ar['pattern'],',')){
                $ar_aliases = explode(',', $ar['pattern']);
                $ar_save = $ar;

                foreach ($ar_aliases as &$item){
                    $ar_save['pattern'] = $item;
                    $arrayRoutingTree[] = $ar_save;
                }

                unset($arrayRoutingTree[$key]);
            }
        }

        $arrayRoutingTree = array_values($arrayRoutingTree);
        return $arrayRoutingTree;
    }

    /**
     * @return Route
     *
     */
    public function GetRoute()
    {
        $alias = Request::GetAliases();
        if ($this -> useCompiledRoutes)
        {
            $aliasString = empty($alias) ? '/' : '/'. implode('/', $alias) .'/';

            $aliasHash = md5($aliasString);

            if (isset($this -> compiledRoutes[$aliasHash]))
            {
                $routeParts = $this -> compiledRoutes[$aliasHash];

                // 0 - контроллер, 1 - действие, 2 - массив аргументов
                if (!empty($routeParts[0]) && is_string($routeParts[0]) &&
                    !empty($routeParts[1]) && is_string($routeParts[1]) &&
                    isset($routeParts[2]) && is_array($routeParts[2]))
                {
                    $route = new Route($routeParts[0], $routeParts[1], $routeParts[2]);
                    return $route;
                }
                else
                {
                    // Если мы попали сюда, значит маршрут в файле записан неверно.
                    // Удалим его оттуда
                    unset($this -> compiledRoutes[$aliasHash]);
                }
            }
        }

        $routeRule = $this -> GetRouteRuleRecursive($this -> routingTree);

        $controllerName = null;
        $setDefaultController = false;

        if (isset($routeRule['controller']))
        {
            if (is_numeric($routeRule['controller']))
            {
                // Если имя контроллера необходимо взять непосредственно из алиаса,
                // то нужно учитывать, что его имя может быть в любом регистре
                if (isset($alias[$routeRule['controller']]) && $alias[$routeRule['controller']] != '')
                {
                    $controllerName = $alias[$routeRule['controller']];
                    $controllerClassName = Autoload::FindCachedClass($controllerName, false);
                    if (empty($controllerClassName))
                    {
                        $path = Autoload::FindClass(AppSettings::$CONTROLLERS_PATH, $controllerName, true, false);
                        if (!empty($path))
                            $controllerClassName = substr($path, strrpos($path, '/') + 1, strlen($controllerName));
                    }
                    $controllerName = $controllerClassName;
                }
                else
                    $setDefaultController = true;
            }
            else
                $controllerName = $routeRule['controller'];
        }
        else
            $setDefaultController = true;

        if ($setDefaultController && isset($routeRule['defaultController']))
            $controllerName = $routeRule['defaultController'];

        $actionName = null;
        $setDefaultAction = false;
        if (isset($routeRule['action']))
        {
            if (is_numeric($routeRule['action']))
            {
                // Если имя метода необходимо взять непосредственно из алиаса,
                // то нужно учитывать, что его имя может быть в любом регистре
                if (isset($alias[$routeRule['action']]) && $alias[$routeRule['action']] != '')
                {
                    $actionName = $alias[$routeRule['action']];
                    $actionName = strtolower($actionName);
                    $methods = get_class_methods($controllerName);
                    $methodExists = false;
                    $cnt = count($methods);
                    for ($i = 0; $i < $cnt && !$methodExists; $i++)
                    {
                        if (strtolower($methods[$i]) == $actionName)
                        {
                            $actionName = $methods[$i];
                            $methodExists = true;
                        }
                    }
                }
                else
                    $setDefaultAction = true;
            }
            else
                $actionName = $routeRule['action'];
        }
        else
            $setDefaultAction = true;

        if ($setDefaultAction && isset($routeRule['defaultAction']))
            $actionName = $routeRule['defaultAction'];

        // Вытасикваем массив с аргументами из алиасов
        $args = array();
        //$args = $alias;
        // Action есть в алиасе. Берем все, что после него
        if (!$setDefaultAction)
        {
            $pos = array_search(strtolower($actionName), $alias);
            if ($pos)
            {
            	if (isset($alias[$pos + 1]))
                	$args = array_slice($alias, $pos + 1);
                else
                	$args = array();
            }
        	else
        	{
        	    if (!empty($routeRule['pattern']))
        	    {
            	    $pos = array_search($routeRule['pattern'], $alias);
                    if ($pos !== false && isset($alias[$pos + 1]))
                        $args = array_slice($alias, $pos + 1);
        	    }
        	}
        }
        // Controller есть в алиасе. Берем все, что после него
        elseif (!$setDefaultController)
        {
            if (!empty($routeRule['pattern']))
    	    {
                $pos = array_search($routeRule['pattern'], $alias);
                if ($pos !== false && isset($alias[$pos + 1]))
                    $args = array_slice($alias, $pos + 1);
    	    }
        }
        // Берем весь алиас
        else
            $args = $alias;

        $route = new Route($controllerName, $actionName, $args);

        if ($this -> useCompiledRoutes)
        {
            // Запишем найденный маршрут в файл
            $this -> compiledRoutes[$aliasHash] = array($controllerName, $actionName, $args);
            ObjectsCache::Cache($this -> compiledRoutes, $this -> cacheFile);
        }

        return $route;
    }

    /**
     * Заглушка, работает не всегда и только с именами контроллеров
     * Метод получения алиаса по маршруту.
     *
     * @param string $controllerName
     * @param string $actionName
     * @param array $args
     */
    public function GetURL($controllerName, $actionName = null, $args = null)
    {
        $res = $this -> GetRoteRulesForController($controllerName, $this -> routingTree[0]);
        if (!empty($res))
        {
            $path = '/';
            if (!empty($res['parents']))
            {
                // Начинаем с предпоследнего и двигаемся в обратном порядке
                for ($i = count($res['parents']) - 2; $i >= 0; $i--)
                {
                    $path .= $res['parents'][$i]['pattern'] .'/';
                }
            }
            $path .= $res['pattern'].'/';
            return $path;
        }
        return '';
    }

    private function GetRouteRuleRecursive($root, $depth = 0)
    {
        $result = array();
        $cnt = count($root);
        $match = null;

        $aliases = Request::GetAliases();

        for ($i = 0; $i < $cnt; $i++)
        {
            // Пути проверяем на полное совпадение или на *
            $isMatching = true;
            $isMatchingExactly = true;

            if (!isset($root[$i]['pattern']))
                throw new Exception('Не задан параметр pattern  в таблице маршрутов');
                //throw new Exception('Не задан параметр pattern  в таблице маршрутов', 'routing_pattern_undefined', $root[$i]);

            // Выберем первую часть паттерна, если он задан в виде массива
            $firstPattern = is_array($root[$i]['pattern']) ? array_shift($root[$i]['pattern']) : $root[$i]['pattern'];

            // при нулевой глубине проверяем имя хоста
            if ($depth == 0)
            {
                if (Request::GetHost() == $firstPattern)
                {
                    $match = &$root[$i];
                    break;
                }
                else if ($firstPattern == '*')
                {
                    $match = &$root[$i];
                    $isMatchingExactly = false;
                    break;
                }
                else
                {
                     $isMatchingExactly = false;
                     $isMatching = false;
                }
            }
            // При глубине больше 0 проверяем алиас
            else
            {
                // Если паттерн задан в виде массива с несколькими возможными значениями
                if (is_array($root[$i]['pattern']))
                {
                    $patternsCnt = count($root[$i]['pattern']);

                    for ($j = 0; $j < $patternsCnt && $isMatching; $j++)
                    {
                        $alias = isset($aliases[$depth - 1 + $j]) ? $aliases[$depth - 1 + $j] : null;
                        if ($root[$i]['pattern'][$j] != $alias)
                        {
                            $isMatchingExactly = false;
                            if ($root[$i]['pattern'][$j] != '*')
                            {
                                $isMatching = false;
                            }
                        }
                    }
                }
                else // Считаем, что тут строка или число
                {
                    if (strlen($root[$i]['pattern']) == 0 && strlen($aliases[$depth - 1]) == 0)
                    {
                        $isMatchingExactly = true;
                    }
                    else
                    {
                        if ($root[$i]['pattern'] != $aliases[$depth - 1])
                            $isMatchingExactly = false;

                        if ($root[$i]['pattern'] != '*')
                            $isMatching = false;

                    }
                }

                if ($isMatchingExactly)
                {
                    $match = $root[$i];
                    break;
                }
                else if ($isMatching)
                    $match = $root[$i];
            }

            if ($root[$i]['pattern'] == '*')
                $match = $root[$i];
        }

        // Теперь заполним правило маршрутизатора или вызовем рекурсивно для детей
        if (!empty($match))
        {
            // Получим имя контроллера
            if (isset($match['controller']))
                 $result['controller'] = $match['controller'];

            if (isset($match['action']))
                 $result['action'] = $match['action'];

            if (isset($match['defaultController']))
                 $result['defaultController'] = $match['defaultController'];

            if (isset($match['defaultAction']))
                 $result['defaultAction'] = $match['defaultAction'];

            if (isset($match['pattern']) && !empty($match['pattern']) && !is_numeric($match['pattern']) && $match['pattern'] != '*')
                 $result['pattern'] = $match['pattern'];


            if (($isMatching || $isMatchingExactly) && !empty($match['children']) && isset($aliases[$depth]))
            {
                $match = $this -> GetRouteRuleRecursive($match['children'], $depth + 1);

                // перекрытие стандартных значений
                if (isset($match['controller']))
                     $result['controller'] = $match['controller'];

                if (isset($match['action']))
                     $result['action'] = $match['action'];

                if (isset($match['defaultController']))
                     $result['defaultController'] = $match['defaultController'];

                if (isset($match['defaultAction']))
                     $result['defaultAction'] = $match['defaultAction'];

                if (isset($match['pattern']) && !empty($match['pattern']) && !is_numeric($match['pattern']) && $match['pattern'] != '*')
                     $result['pattern'] = $match['pattern'];
            }

            return $result;
        }
        else
            return null;
    }

    private function GetRoteRulesForController($controllerName, $root)
    {
        if (isset($root['controller']) && $root['controller'].'' == $controllerName)
        {
            unset($root['children']);
            return $root;
        }
        else if (!empty($root['children']))
        {
            for ($i = 0; $i < count($root['children']); $i++)
            {
                $res = $this -> GetRoteRulesForController($controllerName, $root['children'][$i]);

                if (!empty($res))
                {
                    if (!isset($res['parents']))
                        $res['parents'] = array();
                    unset($root['children']);
                    $res['parents'][] = $root;
                    return $res;
                }
            }
        }
    }
}
?>
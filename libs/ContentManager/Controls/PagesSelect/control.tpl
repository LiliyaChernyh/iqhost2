<select name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if !empty($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}]{else}{$localVars.NAME}{/if}" id="{$localVars.NAME}"{if isset($localVars.CLASS)} class="{$localVars.CLASS}"{/if}{if !empty($localVars.READONLY)} readonly="readonly"{/if}{if $localVars.TIP} title="{$localVars.TIP}"{/if}>
{foreach from=$localVars.PAGES_LIST item=PAGE_ITEM}
    {if isset($PAGE_ITEM.canBeSelected)}
        <option value="{$PAGE_ITEM.pageID}"{if $PAGE_ITEM.pageID == $localVars.VALUE} selected="selected"{/if}>{if isset($PAGE_ITEM.level)}{$PAGE_ITEM.caption|indent:$PAGE_ITEM.level*5:"&nbsp;"}{else}{$PAGE_ITEM.caption}{/if}</option>
    {else}
        <optgroup label="{$PAGE_ITEM.caption|indent:$PAGE_ITEM.level*5:"&nbsp;"}"></optgroup>
    {/if}
{/foreach}
</select>
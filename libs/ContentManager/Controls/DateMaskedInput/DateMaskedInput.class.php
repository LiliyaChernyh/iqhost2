<?php

/**
 * DateMaskedInput
 *
 * @version 2.4
 *
 * Settings:
 *  - dateFormat: Формат вывода даты. Для функции date()
 *  - mask: Маска, по которой будет выполняться ввод даты
 *  - size: Размер поля для ввода
 *  - insertCurrentDateIfEmpty: Вставлять текущую дату если пустое значение. (was ifNull)
 *  - ignoreUserInput: Игнорировать введённые данные и подставлять текущую дату (was ifNotNull)
 *
 */
class DateMaskedInput extends Control 
{
    /**
     * атрибут size тега intut
     *
     * @var int
     */
    private $size = 60;

    /**
     * Вставка текущей даты, если поле не заполнено
     *
     * @var bool
     */
    private $insertCurrentDateIfEmpty = false;

    /**
     * Игнорировать введённые пользователем данные
     *
     * @var bool
     */
    private $ignoreUserInput = false;

    /**
     * Формат даты для вывода в поле
     *
     * @var string
     */
    private $dateFormat = 'd.m.Y';

    /**
     * Маска для jQuery - плагина
     *
     * @var string
     */
    private $mask = '99.99.9999';

	public function InitControl($value = null, $rowID = null)
	{
        $this->LoadSettings();

        if (empty($value) && $this->insertCurrentDateIfEmpty)
            $value = date($this->dateFormat);
        else if (!empty($value))
            $value = date($this->dateFormat, strtotime($value));

        $this->controlTplPath = parent::$CONTROLS_PATH . __CLASS__ . '/control.tpl';

        // Переменные для шаблона
        $this->controlTplVars = array();
        $this->controlTplVars['NAME'] = $this->name;
        $this->controlTplVars['SIZE'] = $this->size;
        $this->controlTplVars['TIP'] = $this->tip;
        $this->controlTplVars['VALUE'] = $value;
        $this->controlTplVars['tableName'] = $this->tableName;
        $this->controlTplVars['formIndex'] = (isset($this->formIndex))?$this->formIndex:0;
        if ($this->disabled)
            $this->controlTplVars['READONLY'] = 1;

        if ($this->required)
            $this->controlTplVars['REQUIRED'] = 1;

        // Используемые JS-библиотеки
        $this->controlJSLibs = array();
        $this->controlJSLibs[] = 'JMASK';

        // Инициализирующая функция
        $this->controlJSFiles = array();
        $this->controlJSFiles[] = parent::$CONTROLS_ALIAS . __CLASS__ . '/init.js';

        // Переменные, необходимые JS
        $this->controlJSVars = array();
        $this->controlJSVars['mask'] = $this->mask;
        $this->controlJSVars['name'] = $this->name;

        if (isset($this->tableName))
            $this->controlTplVars['tableName'] = $this->tableName;
        if (isset($this->formIndex))
            $this->controlTplVars['formIndex'] = $this->formIndex;

        $this->type = __CLASS__;
    }

	private function LoadSettings()
	{
        if (isset($this->controlSettings['size']) && is_numeric($this->controlSettings['size']))
            $this->size = $this->controlSettings['size'];

        if (!empty($this->controlSettings['insertCurrentDateIfEmpty']))
            $this->insertCurrentDateIfEmpty = $this->controlSettings['insertCurrentDateIfEmpty'];

        if (!empty($this->controlSettings['ignoreUserInput']))
            $this->ignoreUserInput = $this->controlSettings['ignoreUserInput'];

        if (!empty($this->controlSettings['dateFormat']))
            $this->dateFormat = $this->controlSettings['dateFormat'];

        if (!empty($this->controlSettings['mask']))
            $this->mask = $this->controlSettings['mask'];
    }

	public function PostBackHandler(&$data, $rowID = null)
	{
        $this->LoadSettings();

        $writeFormat = strpos($this->dateFormat, 'H') === false ? 'Y-m-d' : 'Y-m-d H:i:s';

		if (isset($data) && !empty($data))
		{
            if ($this->ignoreUserInput)
                $data = date($writeFormat);
            else
                $data = date($writeFormat, strtotime($data));
        }
        else if ($this->insertCurrentDateIfEmpty)
            $data = date($writeFormat);
    }
}

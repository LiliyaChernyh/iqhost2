<h1>Контакты</h1>

<div>
	{if isset($CONTENT1)}{$CONTENT1}{/if}

	<div id="contactsSuccess" style="display:none">
		Ваше сообщение отправлено.
	</div>

	<div id="error" class="error" style="display:none">

	</div>

	<div id="contactsFormBlock">
	<form action="" method="post" id="contactsForm" class="form">

		<div class="row">
			<div id="enterName" class="error">Введите ваше имя</div>
			<label for="fio">Имя</label>
			<div class="control">
				<input type="text" name="fio" id="fio" value="" />
			</div>
		</div>
		
		<div class="row">
			<div id="enterEmailOrPhone" class="error">Введите телефон или e-mail</div>
			<label for="phone">Контактный телефон</label>
			<div class="control">
				<input type="text" name="phone" id="phone" value="" />
			</div>
		</div>
		
		<div class="row">
			<div id="wrongEmail" class="error">Введите корректный e-mail</div>
			<label for="email">E-mail</label>
			<div class="control">
				<input type="text" name="email" id="email" value="" />
			</div>
		</div>

		<div class="row">
			<div id="enterMessage" class="error">Введите сообщение</div>
			<label for="message">Сообщение</label>
			<div class="control">
				<textarea name="message" id="message" maxlength="1000"></textarea>
			</div>
		</div>
		
		<div class="row">
			<div id="wrongCaptcha" class="error">Неверно введены цифры с картинки</div>
			<label for="captcha">Введите цифры на картинке</label>
			<div class="control">
				<img src="/getcaptcha/" alt="captcha" id="captchaImg" title="Кликните по картинке, чтобы обновить её" style="display:block;float:left"/>
				<input type="text" name="captcha" id="captcha" class="required" style="height:24px; width:50px; padding-top:3px; font-size:1.5em;display:block;float:left" />
			</div>
		</div>

		<div class="row">
			<input type="submit" name="doSend" value="Отправить" style="width: 100px; cursor: pointer;" />
		</div>
		
	</form>
	</div>
</div>
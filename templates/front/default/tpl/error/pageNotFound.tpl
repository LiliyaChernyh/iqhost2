  <div id="wrapper">
    <div class="content-block">
                <div class="container">
        <h2>Страница не найдена.</h2>

        {if !empty($errorInfo)}
            <p>{$errorInfo}</p>
        {else}
            <p>Запрашиваемой страницы не существует на сайте. Используйте навигационное меню.</p>
        {/if}
    </div>
</div>
</div>
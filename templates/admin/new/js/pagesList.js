var pagesListSettings = {
    check: false,
    clone: false,
    reorder: true
};

jQuery.fn.myToggle = function(speed, easing, callback)
{
    return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
};

$(document).ready(function(){
    InitHandlers();
    ApplySettings();
});

function InitHandlers()
{
    // Возможность выделять страницы
    if (pagesListSettings.check)
        $('#pagesList span.checkbox input').live('click', CheckUnchekRow);

    $('#pagesList .row').live('mouseover', function(){ $(this).addClass('hover'); $(this).children('.rightControlsContainer').show(); });
    $('#pagesList .row').live('mouseout', function(){ $(this).removeClass('hover');  $(this).children('.rightControlsContainer').hide(); });

    $('.item.delete img').live('mouseover', function () {
		$(this).attr('src', ADMIN_IMAGES + 'icons/cross.png')
	});
	$('.item.delete img').live('mouseout', function () {
		$(this).attr('src', ADMIN_IMAGES + 'icons/cross_bg.png')
	});

    $('.item.clone img').live('mouseover', function () {
		$(this).attr('src', ADMIN_IMAGES + 'icons/page_copy.png')
	});
	$('.item.clone img').live('mouseout', function () {
		$(this).attr('src', ADMIN_IMAGES + 'icons/page_copy_bg.png')
	});

    $('.item.edit img').live('mouseover', function () {
		$(this).attr('src', ADMIN_IMAGES + 'icons/pencil.png')
	});
	$('.item.edit img').live('mouseout', function () {
		$(this).attr('src', ADMIN_IMAGES + 'icons/pencil_bg.png')
	});

    $('#pagesList .caption span img').live('click', ToggleInnerPages);

    $('#pagesList .captionContainer').live('click', function(e)
    {
        if ( $(e.target).is('img') ) { return; }
        $(this).siblings('.leftControlsContainer').children('.checkbox').children('input').click();
    });

    $('.delete img').live('click', function(){
        if (AreYouSure('Вместе со страницей удалятся и все внутренние страницы.'))
        {
            var pageID = $(this).parent().parent().parent().children('.leftControlsContainer').children('.checkbox').children('input[type=checkbox]').val();
            $.get(
                CONTROLLER_ALIAS +'delete/'+ pageID +'/',
                {},
                DeletePageResponse,
                "json"
            );
        }
    });
    $('.clone img').live('click', function(){

    });
    $('.edit img').live('click', function(){
        var pageID = $(this).parent().parent().parent().children('.leftControlsContainer').children('.checkbox').children('input[type=checkbox]').val();
        if (typeof(pageID) != "undefined")
            window.location = CONTROLLER_ALIAS + 'edit/'+ pageID +'/';
        else
            $.jGrowl('Не удалось получить ID страницы');
    });

    SetSortable();
}

function ApplySettings()
{
    var leftContainerWidth = 7;
    if (pagesListSettings.check)
    {
        leftContainerWidth += 20;
    }
    else
    {
        $('#pagesList span.checkbox').hide();
        $('#pagesList .captionContainer').css('cursor', 'default');
    }

    if (pagesListSettings.reorder)
    {
        leftContainerWidth += 20;
    }

    if (pagesListSettings.clone)
    {

    }
    else
    {
        $('#pagesList span.clone').hide();
    }

    $('#pagesList .leftControlsContainer').css('width', leftContainerWidth + 'px');
}

function DeletePageResponse(data, status)
{
    if (!standartAJAXResponseHandler(data, status)) return;

    if (typeof(data.pageID) == 'undefined')
        $.jGrowl('Произошла ошибка при удалении');
    else
    {
        $('.pageID_'+ data.pageID).remove();
        $('.parent_'+ data.pageID).remove();
        ReloadTree();

        $.jGrowl('Страница удалена');
    }
}

function SetSortable()
{
    $('.listContainer').sortable({
        handle: 'span.arrows img',
        axis: 'y',
        containment: 'parent',
        placeholder: 'row placeholder',
        start: PreSort,
        stop: ReOrganize
    });
}

function DestroySortable()
{
    $('.listContainer').sortable('destroy');
}

var savedPageID = null;
var savedAfterID = null;

function PreSort(event, ui)
{
    // Получим id страницы
    var liClass = ui.item.attr('class');
    if (typeof(liClass) != 'undefined' && liClass.length > 0)
    {
        var liClassArray = liClass.split(' ');
        var pageIDClass = liClassArray[1];
        var pageIDClassArray = pageIDClass.split('_');
        savedPageID = pageIDClassArray[1];
    }

    // Получим id предыдущей страницы
    var parentLI = ui.item.prevAll('li').eq(0).attr('class');
    if (typeof(parentLI) == 'undefined')
        savedAfterID = 0;
    else
    {
        var liClassArray = parentLI.split(' ');
        var parentIDClass = liClassArray[1];
        var parentIDClassArray = parentIDClass.split('_');
        savedAfterID = parentIDClassArray[1];
    }
}

function ReOrganize(event, ui)
{
    // Если текущий элемент встал между родителем и его детьми,
    // надо воссоединить их :) То есть, поставить этот элемент после детей
    var childrenUL = ui.item.next('ul');
    var parentID;
    if (typeof(childrenUL) != 'undefined')
    {
        var ulClass = childrenUL.attr('class');

        if (typeof(ulClass) != 'undefined')
        {
            var ulClassArray = ulClass.split(' ');
            var parentIDClass = ulClassArray[1];
            var parentIDClassArray = parentIDClass.split('_');
            parentID = parentIDClassArray[1];

            $("ul.parent_" + parentID).clone().insertAfter("li.pageID_" + parentID).end().remove();
        }
    }

    var liClass = ui.item.attr('class');

    var pageID;
    if (typeof(liClass) != 'undefined' && liClass.length > 0)
    {
        var liClassArray = liClass.split(' ');
        var pageIDClass = liClassArray[1];
        var pageIDClassArray = pageIDClass.split('_');
        pageID = pageIDClassArray[1];

        if (pageID.length != 0 && typeof($("ul.parent_"+pageID)) != 'undefined')
        {
            $("ul.parent_" + pageID).clone().insertAfter("li.pageID_" + pageID).end().remove();
        }
    }

    DestroySortable();
    SetSortable();

    // Отправляем запрос на изменения порядка
    if (typeof(parentID) == 'undefined')
    {
        var parentLI = ui.item.prevAll('li').eq(0).attr('class');
        if (typeof(parentLI) == 'undefined')
            parentID = 0;
        else
        {
            var liClassArray = parentLI.split(' ');
            var parentIDClass = liClassArray[1];
            var parentIDClassArray = parentIDClass.split('_');
            parentID = parentIDClassArray[1];
        }
    }

    //$.jGrowl(savedPageID + ' ' + savedAfterID);
    //$.jGrowl(pageID + ' ' + parentID);
    // Если ничего не изменилось, запрос не отправляем
    if (pageID == savedPageID && parentID == savedAfterID)
    {
        console.log('Ничего не изменилось');
    }
    else
    {
        //console.log('pageID = ' + pageID + ' after = ' + parentID);
        $.post(
            CONTROLLER_ALIAS + 'reorderpages/',
            {pageID : pageID, after : parentID},
            ReOrganizedResponse,
            'json'
        );
    }
}

function ReOrganizedResponse(data, status)
{
    if (!standartAJAXResponseHandler(data, status)) return;

    ReloadTree();
}

function ToggleInnerPages(event)
{
    event.stopPropagation();
    var pageID = $(this).parent().parent().parent().parent().children('.leftControlsContainer').children('.checkbox').children('input[type=checkbox]').val();

    if ($(this).parent().hasClass('unloaded'))
    {
        $(this).attr('src', ADMIN_IMAGES + 'loading.gif');
        LoadChildren(pageID);
    }
    else
    {
        if ($(this).parent().hasClass('opened'))
        {
            $(this).attr('src', ADMIN_IMAGES + 'icons/arrow_right.png');
            //$('ul.parent_'+pageID).hide();
            $('ul.parent_'+pageID).myToggle();
            $(this).parent().removeClass('opened').addClass('closed');
        }
        else
        {
            $(this).attr('src', ADMIN_IMAGES + 'icons/arrow_down.png');
            $('ul.parent_'+pageID).myToggle();
            $(this).parent().removeClass('closed').addClass('opened');
        }
    }
}

function LoadChildren(pageID)
{
    $('input[type=checkbox][value=' + pageID + ']').parent().parent().parent().children('.captionContainer').children('span.caption').children('span').children('img').attr('src', ADMIN_IMAGES + 'ajax-loader.gif');

    $.post(
        CONTROLLER_ALIAS + 'getsubpageslist/',
        {pageID : pageID},
        SetSubPages,
        'json'
    );
}

function SetSubPages(data, status)
{
    if (!standartAJAXResponseHandler(data, status)) return;

    var liNode = $('input[type=checkbox][value='+ data.pageID +']').parent().parent().parent();
    liNode.children('.captionContainer').children('span.caption').children('span').removeClass('unloaded').addClass('opened');
    var parentClasses = liNode.parent().attr('class');
    var classesArray = parentClasses.split(' ');
    var levelClass;

    if (classesArray[0] == 'listContainer')
        levelClass = classesArray[2];
    else
        levelClass = classesArray[1];

    var levelArray = levelClass.split('_');
    var level = levelArray[1];

    if (typeof(level) == 'undefined')
        level = 0;
    else
        level = level*1;

    level += 1;
    if (data.pages.length > 0)
    {
        var html = '<ul class="listContainer parent_' + data.pageID + ' level_'+ level +'">';
        //for (var page in data.pages)
        for (var i = 0; i < data.pages.length; i++)
        {
            var page = data.pages[i];
            html += '<li class="row pageID_' + page.pageID + '">';
            html += '<div class="leftControlsContainer"><span class="item checkbox"><input alt="" name="checked[]" value="' + page.pageID + '" type="checkbox"></span><span class="item arrows"><img src="' + ADMIN_IMAGES + 'icons/arrows_updown.png" alt=""></span></div>';
			html += '<div class="captionContainer"><span class="item caption" style="margin-left: ' + level * 15 + 'px;"><span class="toggleArrow ' + (page.isSection == 1 ? 'unloaded' : '') + '">' + (page.isSection == 1 ? '<img src="' + ADMIN_IMAGES + 'icons/arrow_right.png" align="absmiddle" />' : '') + '</span>' + page.caption + '</span></div>';
			html += '<div class="rightControlsContainer"><span class="item delete"><img src="' + ADMIN_IMAGES + 'icons/cross_bg.png" alt=""></span><span class="item clone"><img src="' + ADMIN_IMAGES + 'icons/page_copy_bg.png" alt=""></span><span class="item edit"><img src="' + ADMIN_IMAGES + 'icons/pencil_bg.png" alt=""></span></div>';

            html += '</li>';
        }

        html += '</ul>';

        liNode.after(html);
        //InitHandlers();

        var img = liNode.children('.captionContainer').children('span.caption').children('span').children('img');
        img.attr('src', ADMIN_IMAGES + 'icons/arrow_down.png');
        $('ul.parent_' + data.pageID).hide().myToggle();
        img.parent().removeClass('unloaded').addClass('opened');

        DestroySortable();
        SetSortable();
        ApplySettings();
    }
}

function CheckUnchekRow()
{
    if ($(this).attr('checked'))
    {
        $(this).parent().parent().parent().addClass('checked');
    }
    else
    {
        $(this).parent().parent().parent().removeClass('checked');
    }
}

function ReloadTree()
{
    var struct = $("#structure");
    var structNew = $("<ul>").insertAfter(struct);
    struct.remove();
    structNew.attr("id", "structure");
    structNew.removeClass("treeview").treeview({
			url: CONTROLLER_ALIAS + "getstructure",
			persist: "location",
			animated: "fast"
		});
}
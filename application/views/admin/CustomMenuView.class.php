<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ManualMenuView
 *
 * @author 
 */
class CustomMenuView extends AdminView
{
	  public function __construct($tplName = 'main.tpl') {
	parent::__construct($tplName);
        $this -> tplsPath = $this -> tplsPath .'custommenu/';
      
         // подключается css файл
        ViewData::Append('STYLE_FILE', AppSettings::$ADMIN_STYLES . 'store.css');
//        ViewData::Append('STYLE_FILE', AppSettings::$ADMIN_STYLES . 'tabs.css');

        // подключается store.js
        ViewData::Append('SCRIPT_FILE', AppSettings::$ADMIN_SCRIPTS . 'menu.js');
//        ViewData::Append('SCRIPT_FILE', AppSettings::$ADMIN_SCRIPTS . 'tabs.js');
          }
          
        public function Index()
          {
              $this -> tpl -> contentTpl = $this -> tplsPath .'index.tpl';
          }
  
        public function Menu()
        {
            $this -> tpl -> contentTpl = $this -> tplsPath .'add.tpl';
            
        }
        public function EditMenu()
        {
            $this -> tpl -> contentTpl = $this -> tplsPath .'edit.tpl';
              // Шаблон с меню
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
        }
        
        public function Inner()
        {
            $this -> tpl -> contentTpl = $this -> tplsPath .'inner.tpl';
            ViewData::Append('STYLE_FILE', AppSettings::$ADMIN_STYLES . 'customMenu.css');
              // Шаблон с меню
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
        }
        
        public function InnerMenu()
        {
            $this -> tpl -> contentTpl = $this -> tplsPath .'addInner.tpl';
              // Шаблон с меню
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
        }
        public function EditInnerMenu()
        {
            $this -> tpl -> contentTpl = $this -> tplsPath .'editInner.tpl';
              // Шаблон с меню
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
        }
}

?>

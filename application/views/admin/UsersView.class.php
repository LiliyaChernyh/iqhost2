<?php
/**
 *
 * Представление для редактирования пользователей в Админке
 *
 * @version 2.2
 *
 */
class UsersView extends AdminView
{
    public function __construct($mainTpl = null)
	{
		if (!$mainTpl && AppSettings::$ADMIN_TEMPLATE_COL == 1)
		{
			$mainTpl = 'single_column.tpl';
		}
		elseif (!$mainTpl)
		{
			$mainTpl = 'main.tpl';
		}

		parent::__construct($mainTpl);

		$this -> tplsPath = $this -> tplsPath.'users/';

        ViewData::Append("STYLE_FILE", AppSettings::$ADMIN_STYLES.'user.css');
    }

    public function UsersList()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'list.tpl';
    }

    public function EditUser()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'edit.tpl';
    }

    public function AddUser()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'add.tpl';
    }

    public function AddInfo()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'addinfo.tpl';
    }

    public function Password()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'pass.tpl';
    }
}

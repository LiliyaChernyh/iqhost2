<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Регистрация</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="{$ADMIN_CSS}form_bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="{$ADMIN_CSS}registration.css" type="text/css" />
    <script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jquery.js"></script>
    <script type="text/javascript" src="{$LIBS_ALIAS}jQuery/jquery.form.js"></script><!--  AJAX forms-->
    <script type="text/javascript" src="{$LIBS_ALIAS}jQuery/validator/validate.min.js"></script><!-- Валидатор форм -->
    <script type="text/javascript" src="{$ADMIN_SCRIPTS}registration.js"></script>
</head>
<body>
    <div id="container">    
        <h1>Регистрация</h1>

        <div id="message" style="display:none;"></div>

        <form action="" method="post" id="contentForm" enctype="multipart/form-data">
                <div class="formBlock">
                    <div class="formElement">
                                <input type="text" name="login" id="login" value="" size="45" data-required />
                                <label for="login" class="formElementLabel">Логин</label>
                    </div>

                    <div class="formElement">
                                <input type="password" name="password" id="password" value="" size="45" data-required data-validate="password" />
                                <label for="password" class="formElementLabel">Пароль*</label>
                                <span class="info">* - не менее 6 символов</span>
                    </div>

                    <div class="formElement">
                                <input type="password" name="passwordConf" id="passwordConf" value="" size="45" data-required data-validate="passwordConf" />
                                <label for="passwordConf" class="formElementLabel">Еще раз пароль*</label>
                    </div>

                    <div class="formElement">
                                <input type="text" name="FIO" id="FIO" value="" size="45" data-required />
                                <label for="FIO" class="formElementLabel">ФИО</label>
                    </div>

                    <div class="formElement">
                                <input type="text" name="email" id="email" value="" size="45" data-required data-validate="email" />
                                <label for="email" class="formElementLabel">E-mail</label>
                    </div>
                    
                    <div class="formElement">
                                <input type="text" name="captcha" id="captcha" class="required" style="float:left; height: 35px;" data-required />
                                <img src="/getcaptcha/" alt="captcha" id="captchaImg" />
                    </div>
                </div>
                <div style="clear:both"></div>

                <div class="btn-reg">
                        <input type="submit" value="Зарегистрироваться" name="doRegister" class="handyButton" />
                </div>
        </form>
    <div>
        
    {literal}
    <script>
        $('#contentForm').validate({
                onChange : true,
                onKeyup : true,
                eachValidField : function() {
                        $(this).closest('div').removeClass('error').addClass('success');
                },
                eachInvalidField : function() {
                        $(this).closest('div').removeClass('success').addClass('error');
                }
        });

        $.validateExtend({ 
                password : {
                        onKeyup : false,
                        conditional : function(value) {
                                return (value.length >= 6);
                        }
                },
                passwordConf : {
                        conditional : function(value) {
                                return (value == $('#password').val());
                        }
                },
                email : {
                        pattern : /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/ 
                }
        });
    </script>
    {/literal}
</body>
</html>
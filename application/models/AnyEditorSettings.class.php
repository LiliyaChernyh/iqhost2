<?php

class AnyEditorSettings {

    public static $EditableTables = array(
        // lowercase key!
        'advantages' => array(
            'tableName' => 'Advantages',
            'ruName' => 'Преимущества (на главной)',
            'ruItem' => 'Преимущества (на главной)',
            'listFields' => array(
                'advantageID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'title' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`priority` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'domains' => array(
            'tableName' => 'Domains',
            'ruName' => 'Доменные имена',
            'ruItem' => 'Доменные имена',
            'listFields' => array(
                'domainID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'zone' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                ),
                'price' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`priority` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'hosting' => array(
            'tableName' => 'Hosting',
            'ruName' => 'Хостинг сайтов',
            'ruItem' => 'Хостинг сайтов',
            'listFields' => array(
                'hostingID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'title' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`priority` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'hostingbitrix' => array(
            'tableName' => 'HostingBitrix',
            'ruName' => 'Хостинг 1С Битрикс',
            'ruItem' => 'Хостинг 1С Битрикс',
            'listFields' => array(
                'hostingID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'title' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`priority` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'servers' => array(
            'tableName' => 'Servers',
            'ruName' => 'Аренда серверов',
            'ruItem' => 'Аренда серверов',
            'listFields' => array(
                'serverID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'title' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`priority` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'vps' => array(
            'tableName' => 'VPS',
            'ruName' => 'Аренда VPS',
            'ruItem' => 'Аренда VPS',
            'listFields' => array(
                'vpsID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'title' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`priority` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'gpu' => array(
            'tableName' => 'GPU',
            'ruName' => 'Аренда GPU',
            'ruItem' => 'Аренда GPU',
            'listFields' => array(
                'gpuID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'title' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`priority` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'colocation' => array(
            'tableName' => 'Colocation',
            'ruName' => 'Размещение серверов',
            'ruItem' => 'Размещение серверов',
            'listFields' => array(
                'colocationID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'title' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`priority` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'advantagessolution' => array(
            'tableName' => 'AdvantagesSolution',
            'ruName' => 'Преимущества (для решений)',
            'ruItem' => 'Преимущества (для решений)',
            'listFields' => array(
                'advantageSolutionID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'title' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                ),
                'isActive' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`advantageSolutionID` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'bannerssolution' => array(
            'tableName' => 'BannersSolution',
            'ruName' => 'Баннеры (для решений)',
            'ruItem' => 'Баннеры (для решений)',
            'listFields' => array(
                'bannerSolutionID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'title' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`bannerSolutionID` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'menusolution' => array(
            'tableName' => 'MenuSolution',
            'ruName' => 'Меню (для решений)',
            'ruItem' => 'Меню (для решений)',
            'listFields' => array(
                'menuSolutionID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'title' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                ),
                'link' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`menuSolutionID` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'ratesservice' => array(
            'tableName' => 'RatesService',
            'ruName' => 'Тарифы (для сервисов)',
            'ruItem' => 'Тарифы (для сервисов)',
            'listFields' => array(
                'rateServiceID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'title' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                ),
                'link' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                ),
                'isActive' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`rateServiceID` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'mailboxfeatures' => array(
            'tableName' => 'MailboxFeatures',
            'ruName' => 'Возможности (почтовый ящик)',
            'ruItem' => 'Возможности (почтовый ящик)',
            'listFields' => array(
                'mailboxFeatureID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'text' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                ),
                'isActive' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`mailboxFeatureID` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
        'mailboxservices' => array(
            'tableName' => 'MailboxServices',
            'ruName' => 'Услуги (почтовый ящик)',
            'ruItem' => 'Услуги (почтовый ящик)',
            'listFields' => array(
                'mailboxServiceID' => array(
                    'width' => 40,
                    'align' => 'right',
                    'sortable' => true,
                    'searchable' => false
                ),
                'title' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                ),
                'isActive' => array(
                    'width' => 500,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true
                )
            ),
            'filter' => null,
            'orderBy' => '`mailboxServiceID` ASC',
            'limitOnPage' => 20,
            'allowAddition' => 1,
            'allowRemove' => 1
        ),
            /*
              'news'      => array(
              'tableName'     => 'News',
              'ruName'        => 'Новости',
              'ruItem'        => 'Новость',
              'listFields'    => array(
              'newsID' => array(
              'width'         => 40,
              'align'         => 'right',
              'sortable'      => true,
              'searchable'    => false
              ),
              'title' => array(
              'width'         => 500,
              'align'         => 'left',
              'sortable'      => true,
              'searchable'    => true
              ),
              'creationDate' => array(
              'width'         => 200,
              'align'         => 'right',
              'sortable'      => true,
              'searchable'    => false
              )
              ),
              'filter'            => null,
              'orderBy'           => '`newsID`',
              'limitOnPage'       => 20,
              'allowAddition'     => 0,
              'allowRemove'       => 0
              ),
              'videos'	=>	array(
              'tableName'			=> 'Videos',
              'ruName'			=> 'Видео-файлы',
              'ruItem'			=> 'Видео',
              'listFields'		=> array('title', 'description', 'creationDate', 'fileSize'),
              'filter'			=> null,
              'orderBy'			=> '`creationDate` DESC',
              'limitOnPage'		=> 50,
              'allowAddition'		=> 1,
              'allowRemove'		=> 1,
              'eventListeners'	=> array(
              'AnyEditorEntryAddPreFlush'
              => array(
              array(
              'class' => 'VideosEventListener',
              'method' => 'AddPreFlushHandler'
              )
              ),
              'AnyEditorEntryEditPreFlush'
              => array(
              array(
              'class' => 'VideosEventListener',
              'method' => 'EditPreFlushHandler'
              )
              )
              )
              ) */

//        'greeny_nl_news'    => array(
//            'tableName'     => 'greeny_nl_news',
//            'ruName'        => 'Новости',
//            'ruItem'        => 'Новости',
//            'listFields'    => array(
//                'newsID' => array(
//                    'width'         => 40,
//                    'align'         => 'right',
//                    'sortable'      => true,
//                    'searchable'    => false
//                ),
//                'title' => array(
//                    'width'         => 500,
//                    'align'         => 'left',
//                    'sortable'      => true,
//                    'searchable'    => true
//                ),
//                'publicationDate' => array(
//                    'width'         => 200,
//                    'align'         => 'center',
//                    'sortable'      => true,
//                    'searchable'    => false
//                )
//            ),
//            'filter'            => null,
//            'orderBy'           => '`messageID`',
//            'limitOnPage'       => 20,
//            'allowAddition'     => 0,
//            'allowRemove'       => 0
//        ),
//
//        'fb_typetags'    => array(
//            'tableName'     => 'fb_typeTags',
//            'ruName'        => 'Типы тегов',
//            'ruItem'        => 'Типы тегов',
//            'listFields'    => array(
//                'typeTagID' => array(
//                    'width'         => 40,
//                    'align'         => 'right',
//                    'sortable'      => true,
//                    'searchable'    => false
//                ),
//                'caption' => array(
//                    'width'         => 500,
//                    'align'         => 'left',
//                    'sortable'      => true,
//                    'searchable'    => true
//                ),
//            ),
//            'filter'            => null,
//            'orderBy'           => '`typeTagID`',
//            'limitOnPage'       => 20,
//            'allowAddition'     => 1,
//            'allowRemove'       => 1
//        ),
//
//        'fb_tags'    => array(
//            'tableName'     => 'fb_Tags',
//            'ruName'        => 'Теги',
//            'ruItem'        => 'Теги',
//            'listFields'    => array(
//                'tagID' => array(
//                    'width'         => 40,
//                    'align'         => 'right',
//                    'sortable'      => true,
//                    'searchable'    => false
//                ),
//                'caption' => array(
//                    'width'         => 500,
//                    'align'         => 'left',
//                    'sortable'      => true,
//                    'searchable'    => true
//                ),
//                'typeTagID' => array(
//                    'width'         => 500,
//                    'align'         => 'left',
//                    'sortable'      => true,
//                    'searchable'    => true,
//                    'linkTable'     => 'fb_typeTags',
//                    'linkField'     => 'caption'
//                ),
//            ),
//            'filter'            => null,
//            'orderBy'           => '`tagID`',
//            'limitOnPage'       => 20,
//            'allowAddition'     => 1,
//            'allowRemove'       => 1
//        ),
    );

}

?>
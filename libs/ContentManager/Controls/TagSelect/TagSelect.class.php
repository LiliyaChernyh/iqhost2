<?php

class TagSelect extends Control implements IFlushingControl
{
    /**
     * Максимальный размер множественного выбора.
     * Число вариантов выбора, умещающихся в окошко контрола без прокрутки.
     *
     * @var int
     */
    public static $MaxSelectSize = 10;

    /**
     * Имя таблицы со значениями
     *
     * @var string
     */
    private $referentTableName = null;

    /**
     * Имя поля с человеко-понятным значением
     *
     * @var string
     */
    private $listingKey = null;

    /**
     * Имя таблицы, хранящей множественные
     *
     * @var string
     */
    private $linkingTableName = null;

    /**
     * Тип отображения элемента управления. 
     * Возможные значения: 'select' (default), 'checkbox'
     *
     * @var string
     */
    private $controlType = 'select';

    public function InitControl($value = null, $rowID = null)
    {
        $this -> LoadSettings();
//        $this -> controlTplPath = ($this -> controlType == 'select') ?
//            parent::$CONTROLS_PATH.__CLASS__.'/control_select.tpl' :
//            parent::$CONTROLS_PATH.__CLASS__.'/control_checkboxes.tpl';
        $this -> controlTplPath = parent::$CONTROLS_PATH.__CLASS__.'/control.tpl';

        $this -> controlTplVars = array();        
        $this -> controlTplVars['NAME'] = $this -> name;
        $this -> controlTplVars['TIP'] = $this -> tip;
        $this->controlTplVars['SIZE'] = $this -> controlSettings['size'];
        $this->controlTplVars['CLASS'] = $this -> controlSettings['className'];
        $this -> controlTplVars['tableName'] = $this->tableName;
        $this -> controlTplVars['formIndex'] = (isset($this->formIndex))?$this->formIndex:0;
        
        $this -> controlJSVars = array();
        $this -> controlJSVars['name'] = $this -> name;
        $this -> controlJSVars['itemName'] = $this->tableName."[".$this->formIndex."][".$this -> name."]";
        $this -> controlJSFiles[] = parent::$CONTROLS_ALIAS.__CLASS__.'/init.js';
        
        if ($this -> required) $this -> controlTplVars['REQUIRED'] = 1;
        if ($this -> disabled) $this -> controlTplVars['READONLY'] = 1;

        // Вытаскиваем значения ReferentTable - это та таблица, в которой хранятся option'ы селекта
        $referentTableManager = new DBTableManager($this -> referentTableName);
        $referentTableManager->SortOrder = $this -> listingKey . ' ASC';
        $referentTableRows = $referentTableManager -> Select();

        // Связывающая таблица
        $selectedRows = array();
        if ($rowID != null)
        {
            $linkingTableManager = new DBTableManager($this -> linkingTableName);
            $linkingTableSelectedRows = $linkingTableManager -> Select(array($this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID));
            $cnt = count($linkingTableSelectedRows);
            for ($i = 0; $i < $cnt; $i++)
                $selectedRows[] = $linkingTableSelectedRows[$i][$referentTableManager -> TableStructure -> PrimaryKey -> Name];
        }

        $cnt = count($referentTableRows);
        $options = '';
        if ($cnt == 0)
        {
            $this -> controlHTML = 'no rows';
            $this -> name = $this -> name;
            $this -> type = __CLASS__;
            $this -> description = $this -> description;
            return;
        }
        
//        FB::log($referentTableRows, 'refRows');
//        FB::log($selectedRows, 'selectedRows');

        $options_available = array();
        $options_selected = array();
        for ($i = 0; $i < $cnt; $i++)
        {
            $selected = ($rowID != null && in_array($referentTableRows[$i][$referentTableManager -> TableStructure -> PrimaryKey -> Name], $selectedRows));
            $option = array();
            $option['TEXT'] = $referentTableRows[$i][$this -> listingKey];
            $option['VALUE'] = $referentTableRows[$i][$referentTableManager -> TableStructure -> PrimaryKey -> Name];
            //$option['SELECTED'] = ($rowID != null && in_array($referentTableRows[$i][$referentTableManager -> TableStructure -> PrimaryKey -> Name], $selectedRows));
            
            if ($selected)
                $options_selected[] = $option;
            else
                $options_available[] = $option;
            
//            if ($this -> controlType != 'select')
//              $option['ID'] = $this -> name.'Label'.$i;
        }
        $this -> controlTplVars['OPTIONS_AVAILABLE'] = $options_available;
        $this -> controlTplVars['OPTIONS_SELECTED'] = $options_selected;
//        FB::log($this -> controlTplVars['OPTIONS_AVAILABLE']);
        
        $this -> type = __CLASS__;
    }

    /**
     * Сюда приходит массив ID полей в ReferentTable
     *
     * @param array $data
     * @param int $rowID
     */
    public function Flush(&$data, $rowID)
    {
        $this -> LoadSettings();
        $linkingTableManager = new DBTableManager($this -> linkingTableName);
        $referentTableStructure = new DBTableStructure($this -> referentTableName);
        //$linkingTableSelectedRows = $linkingTableManager -> Select(array($this -> activeTableStructure -> PrimaryKey -> Name => $rowID));
        
        // Удалим все текущие записи.
        // Это довольно опасная операция... Надо все тщательно проверить :)
        if ($rowID == null || !is_numeric($rowID) || $rowID < 1)
            throw new ArgumentException("Неверно задан rowID -> $rowID");
            
        $linkingTableManager -> Delete( 
            array(
                $this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID
            ), true
        );
        
        // Теперь добавим новые записи
        foreach ($data as $referentID)
        {
            if (is_numeric($referentID) && $referentID > 0)
                $linkingTableManager -> Insert(
                    array(
                        $this -> controlsRegistry -> activeTableStructure -> PrimaryKey -> Name => $rowID,
                        $referentTableStructure -> PrimaryKey -> Name => $referentID
                    )
                );
        }
    }
    
    public function PostBackHandler(&$data, $rowID = null) {}
    
    protected function LoadSettings()
    {
        $errmsg = $this -> name. ' is undefined. It should be defined in FieldsRegistry';
        if (!empty($this -> controlSettings['referentTable']))
            $this -> referentTableName = $this -> controlSettings['referentTable'];
        else 
            throw new Exception('Referent Table for ' . $errmsg);

        if (!empty($this -> controlSettings['listingKey']))
            $this -> listingKey = $this -> controlSettings['listingKey'];
        else 
            throw new Exception('listing Key for ' . $errmsg);

        if (!empty($this -> controlSettings['linkingTable']))
            $this -> linkingTableName = $this -> controlSettings['linkingTable'];
        else 
            throw new Exception('linking Table for ' . $errmsg);

        if (!empty($this -> controlSettings['controlType']))
            $this -> controlType = $this -> controlSettings['controlType'];
    }
}
?>
function TagSelectInit(initObject)
{
    var $hold = $('[data-name="' + initObject.name + '"]');
    var $btnAdd = $hold.find('.button-add-tag');
    var $btnRemove = $hold.find('.button-remove-tag');
    var $selectAvail = $hold.find('.tags-avail');
    var $selectSelected = $hold.find('.tags-selected');
    var $hiddenHolder = $hold.find('.hidden-holder');
    
    var updateFields = function() {
        $hiddenHolder.empty();
        var $options = $selectSelected.find('option');
        if ($options.length == 0)
        {
            $('<input/>').attr('type', 'hidden').attr('name', initObject.itemName + '[]').val(0).appendTo($hiddenHolder);
        }
        else
        {
            $options.each(function() {
                var $opt = $(this);
                $('<input/>').attr('type', 'hidden').attr('name', initObject.itemName + '[]').val($opt.val()).appendTo($hiddenHolder);
            });
        }
    };
    
    $btnAdd.on('click', function(e) {
        //console.log();
        $selectAvail.find(':selected').detach().appendTo($selectSelected).removeAttr('selected');
        
        updateFields();
    });
    
    $btnRemove.on('click', function(e) {
        var scrollPos = $selectAvail.scrollTop();
        //var $options = $selectAvail.find('option').detach().sort().appendTo($selectAvail);
        $selectSelected.find(':selected').detach().prependTo($selectAvail)//.removeAttr('selected');
        // sort alphabetical
        $selectAvail.find('option').sort(function(a,b) {
            return (a.innerHTML > b.innerHTML) ? 1 : ((a.innerHTML < b.innerHTML) ? -1 : 0);
        }).appendTo($selectAvail);
        
        $selectAvail.scrollTop(scrollPos); //restore scroll pos
        
        updateFields();
    });
}
<?php

class ServicesModel {
    public function GetSolutions($sectionId)
    {

        $query = 'SELECT ps.`caption`, ps.`alias`
                    FROM   `greeny_pageStructure`  ps LEFT JOIN  `Solutions` s  ON
                    ps.`contentID` = s.`solutionID`                    
                    WHERE ps.`parentID` = ' . $sectionId . ' AND ps.`isDeleted` = 0 AND  ps.`isActive` = 1 ORDER BY ps.`priority` DESC;';

        $result = DB::QueryToArray($query);
        return $result;
    }

    public function getBanner($bannerId)
    {
        $query = 'SELECT * FROM `BannersSolution` b WHERE b.`bannerSolutionID` = ' . $bannerId;
        return DB::QueryOneRecordToArray($query);
    }

    public function getAdvantages($serviceID)
    {
        $query = 'SELECT * FROM `AdvantagesSolution_Service` ass'
                . ' LEFT JOIN `AdvantagesSolution` a ON ass.`advantageSolutionID` = a.`advantageSolutionID`'
                . ' WHERE ass.`serviceID` = ' . $serviceID . ' AND a.`isActive` = 1';
        return DB::QueryToArray($query);
    }

    public function getMenuService($serviceID)
    {
        $query = 'SELECT * FROM `MenuSolution_Service` ms'
                . ' LEFT JOIN `MenuSolution` m ON ms.`menuSolutionID` = m.`menuSolutionID`'
                . ' WHERE ms.`serviceID` = ' . $serviceID . ' AND m.`isActive` = 1';
        return DB::QueryToArray($query);
    }
    
    public function getRatesService($serviceID)
    {
        $query = 'SELECT * FROM `RatesService_Service` rs'
                . ' LEFT JOIN `RatesService` r ON rs.`rateServiceID` = r.`rateServiceID`'
                . ' WHERE rs.`serviceID` = ' . $serviceID . ' AND r.`isActive` = 1';
        return DB::QueryToArray($query);
    }
    
    public function getFeatures()
    {
        $query = 'SELECT * FROM `MailboxFeatures` m  WHERE m.`isActive` = 1 ORDER BY m.`order` ASC';
        return DB::QueryToArray($query);
    }  
    
    public function getMailboxServices()
    {
        $query = 'SELECT * FROM `MailboxServices` m  WHERE m.`isActive` = 1';
        return DB::QueryToArray($query);
    }
}

?>

<?php

require_once(IncPaths::$LIBS_PATH .'PHPMailer/class.phpmailer.php');

class BitPlatformMailer extends PHPMailer 
{
    public function BitPlatformMailer()
    {
        $settingsManager = DBSettingsManager::getInstance();
        
        $mailerSettings = $settingsManager -> GetSettingsAssoc('general.mailer');
        
        $this -> Sender = $mailerSettings['mailer']['fromEmail'];
        $this -> From = $mailerSettings['mailer']['fromEmail'];
        $this -> FromName = $mailerSettings['mailer']['fromName'];
        $this -> CharSet = 'utf-8';
    }
}

?>
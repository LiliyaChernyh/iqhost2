<?php
/**
 * Представление для работы с модулем Feedback во бек-енде
 * 
 * @version 1.2
 *
 */
class FeedbackViewAdmin extends AdminView
{
    public function __construct()
    {
        $this -> moduleName = 'Feedback';
        parent::__construct();
        
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
    }
    
    public function Index()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'messages_list.tpl';
        
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
    
    public function Item()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'message.tpl';
        
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
}
?>
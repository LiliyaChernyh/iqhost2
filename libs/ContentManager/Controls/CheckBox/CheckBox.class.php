<?php
/**
 * Контрол для представления CheckBox.
 * Используется обычно для управления булевыми полями.
 *
 * @version 3.3
 *
 */
class CheckBox extends Control
{
	/**
	 * Отмечено ли по умолчанию
	 *
	 * @var bool
	 */
	private $checkedDefault = false;

	public function InitControl($value = null, $rowID = null)
	{
	    $this -> LoadSettings();

	    $this -> controlTplPath = parent::$CONTROLS_PATH.__CLASS__.'/control.tpl';

	    $this -> controlTplVars = array();
	    $this -> controlTplVars['NAME'] = $this -> name;
	    $this -> controlTplVars['CLASS'] = $this -> CSSClass;
	    $this -> controlTplVars['TIP'] = $this -> tip;
	    $this -> controlTplVars['VALUE'] = $value;
	    if (($value == 1) || (is_null($value) && $this -> checkedDefault)) $this -> controlTplVars['CHECKED'] = 1;

	    if ($this -> disabled) $this -> controlTplVars['READONLY'] = 1;
	    if ($this -> required) $this -> controlTplVars['REQUIRED'] = 1;

	    $this -> type = __CLASS__;
	    $this -> description = $this -> description;

	    if (isset($this->tableName)) $this -> controlTplVars['tableName'] = $this->tableName;
	    $this -> controlTplVars['formIndex'] = (isset($this->formIndex))? $this->formIndex: 0;
	}

	private function LoadSettings()
	{
    	if (!empty($this -> controlSettings['checkedDefault']))
            $this -> checkedDefault = $this -> controlSettings['checkedDefault'];
	}

	public function PostBackHandler(&$data, $rowID = null) {}
}

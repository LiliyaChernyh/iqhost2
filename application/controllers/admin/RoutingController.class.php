<?php
/**
 * Контроллер для интерфейса редактирования маршрутов
 *
 * @author grom
 */
class RoutingController extends AdminController
{
	/**
	 * @var RoutingTableDBProvider
	 */
	private $routingTableProvider;

	public function __construct()
	{
		parent::__construct();

		$this->routingTableProvider = new RoutingTableDBProvider();
		$this->view = new RoutingView();
	}

	public function Index($args = null)
	{
		if (!empty($args[0]) && ('cleancache' == $args[0]))
			return $this->CleanCache();

		$tree = $this->routingTableProvider->GetRoutingTree();

		return $this->view->Tree($tree);
	}

	public function Edit($args = null)
	{
		if (empty($args[0]))
			return new RedirectActionResult($this->controllerAlias);

		$routeID = (int)$args[0];

		if (!empty(Request::$POST['saveRoute']))
		{
			$newRoute = array(
				'routeID'           => (int) Request::$POST['routeID'],
				'parentID'          => (int) Request::$POST['parentID'],
				'pattern'           => trim(Request::$POST['pattern']),
				'controller'        => trim(Request::$POST['controller']),
				'action'            => trim(Request::$POST['action']),
				'defaultController' => empty(Request::$POST['defaultController']) ? NULL : trim(Request::$POST['defaultController']),
				'defaultAction'     => empty(Request::$POST['defaultAction']) ? NULL : trim(Request::$POST['defaultAction']),
			);
//			trace($newRoute);
			if ($this->routingTableProvider->SaveRoute($newRoute))
				new RedirectActionResult($this->controllerAlias);
			else
				return $this->Error('Ошибка изменения маршрута', $this->controllerAlias);
		}

		$route = $this->routingTableProvider->GetRoute($routeID);
		$linear = $this->routingTableProvider->GetLinearTree();
		return $this->view->EditRoute($route, $linear);
	}

	public function Add($args = null)
	{
		if (!empty(Request::$POST['saveRoute']))
		{
			$newRoute = array(
				'parentID'          => (int) Request::$POST['parentID'],
				'pattern'           => trim(Request::$POST['pattern']),
				'controller'        => trim(Request::$POST['controller']),
				'action'            => trim(Request::$POST['action']),
				'defaultController' => empty(Request::$POST['defaultController']) ? NULL : trim(Request::$POST['defaultController']),
				'defaultAction'     => empty(Request::$POST['defaultAction']) ? NULL : trim(Request::$POST['defaultAction']),
			);
			if ($this->routingTableProvider->SaveRoute($newRoute))
				return new RedirectActionResult($this->controllerAlias);
			else
				return $this->Error('Ошибка добавления маршрута', $this->controllerAlias);
		}

		$linear = $this->routingTableProvider->GetLinearTree();
		$after = empty(Request::$GET['after']) ? null : (int)Request::$GET['after'];
		return $this->view->AddRoute($linear, $after);
	}

	public function Delete($args = null)
	{
		if (empty($args[0]))
			return new RedirectActionResult($this->controllerAlias);

		$routeID = (int)$args[0];

		if ($this->routingTableProvider->DeleteRouteRecursive($routeID))
			return new RedirectActionResult($this->controllerAlias);
		else
			return $this->Error('Ошибка удаления маршрута', $this->controllerAlias);
	}

	public function CleanCache()
	{
		$this->routingTableProvider->CleanCache();
		return new RedirectActionResult($this->controllerAlias);
	}
}
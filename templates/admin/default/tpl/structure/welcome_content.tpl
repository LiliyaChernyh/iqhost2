		
<h1>Система управления сайтом {$SETTINGS.general.siteName}</h1>

<form action="{$CONTROLLER_ALIAS}directAccess" method="post">
<ul>
    <li>
        Для быстрого перехода к редактированию страницы, введите её полный <abbr title="путь, например:  http://site.ru/page/1/">URL</abbr>-адрес:
        <br />
        <input type="text" name="directAccess" value="" size="100" /> <input type="submit"  class="handyButton" name="go" value=" Go " />
    </li>
    <li>
        Или найдите её в меню слева
        <br />
        &larr;
    </li>
</ul>
</form>

<script type="text/javascript">
{literal}$(document).ready(function() {if (!$.support.boxModel)	$("#changeBrowser").slideToggle();});{/literal}
</script>
<div id="changeBrowser" style="display:none;">
<p style="font-size:1.2em;color:#c00">Вы используете устаревший браузер!</p>

<p style="font-size:1.2em"> Для комфортной работы в системе нужен один из следующих <span class="helper" title="Программа для просмотра сайтов">браузеров</span>:
</p>
	<ul>
		<li><a href="http://www.google.com/chrome">Google Chrome</a> </li>
		<li><a href="http://getfirefox.com">Mozilla Firefox v3+</a>  </li>
	</ul>
</div>

<noscript>
	<div style="color:#C00; font-size:1.3em">Внимание, JavaScript должен быть включён в браузере!</div>
</noscript>

<p>
&laquo;<a href="http://www.BitPlatform.ru">Битплатформа</a>&raquo; &mdash; платформа для разработки сайтов и веб-приложений. Разработана и поддерживается компанией <a href="http://ayaco.ru">АЯКО</a>.
</p>

<p>По вопросам распространения обращайтесь:</p>

<table border="0"  style="border:none;margin-top:-20px;">
<tr>
	<td style="border:none;">e-mail: </td>
	<td style="border:none;"><a href="mailto:make-a-deal@bitplatform.ru">make-a-deal@bitplatform.ru</a></td>
</tr>
<tr>
	<td style="border:none;">тел: </td>
	<td style="border:none;">+7 (985) 233-99-60</td>
</tr>

</table>


<p>
<strong class="helper" title="Доменное имя, по которому открывается ваш сайт,  например: YANDEX.RU, AYACO.RU">Ваш домен:</strong><a href="http://www.nic.ru/whois/?query={$HTTP_HOST}">{$HTTP_HOST}</a><br />
<strong class="helper" title="IP адрес компьютера, на котором размещён ваш сайт">IP адрес вашего сервера:</strong> <a href="http://www.nic.ru/whois/?query={$SERVER_ADDR}">{$SERVER_ADDR}</a>
</p>		

<?php
/**
 * Объект запроса
 *
 *  @version 1.0
 */
class Request
{
    const GET    = 'GET';
    const POST   = 'POST';
    const PUT    = 'PUT';
    const DELETE = 'DELETE';
    const HEAD   = 'HEAD';

    /**
     * Массив с переменными, переданными в строке запроса
     *
     * @var array
     */
    public static $GET;

    /**
     * Массив с переменными, переданными через POST
     *
     * @var array
     */
    public static $POST;

    /**
     * Массив с куками
     *
     * @var array
     */
    public static $COOKIE;

    /**
     * Реферер
     *
     * @var string
     */
    public static $REFERER;

    /**
     * Если пришёл AJAX запрос
     *
     * @var bool
     */
    private static $isAsync = false;

    /**
     * Метод запроса
     *
     * @var string
     */
    private static $requestMethod;

    /**
     * Массив частей URL
     *
     * @var array
     */
    private static $aliases;

    /**
     * Геттер типа запроса запроса
     *
     * @return string
     */
    public static function GetRequestMethod()
    {
        return self::$requestMethod;
    }

    /**
     * Проверяет является ли запрос асинхронным
     *
     * @return bool
     */
    public static function IsAsync()
    {
        return self::$isAsync;
    }

    /**
     * Возвращает флаг ассинхронного запроса
     *
     * @param bool $isAsync
     */
    public static function SetAsync($isAsync = true)
    {
        self::$isAsync = $isAsync;
    }

    /**
     * Возвращает массив алиасов или конкретный алиас
     *
     * @param mixed $index
     * @return mixed
     */
    public static function GetAliases($index = null)
    {
        if (is_null($index))
            return self::$aliases;

        if (is_numeric($index))
            return isset(self::$aliases[$index]) ? self::$aliases[$index] : null;

        if (is_string($index))
        {
            $key = array_search($index, self::$aliases);
    		if ($key === false)
    			return null;
    		else
    		    return isset(self::$aliases[$key + 1]) ? self::$aliases[$key + 1] : null;
        }

        return null;
    }
    
    /**
     * Получит список фрагментов ULR до указанного уровня
     * 
     * @param int $count  число фрагментов
     * @return array      массив фрагментов
     */
    public static function GetAliasesLimited($count)
    {
        $frag = array();
        
        if (!empty($count) && is_int($count)) {
            $aliases = self::$aliases;
            $counter = 1;

            foreach ($aliases as $alias) { // выбираем данные только до указанного уровня
                if ($counter <= $count) {
                    $frag[] = $alias;
                } else {
                    break;
                }

                $counter++;
            }
        }
        
        return $frag;        
    }
  
    /**
     * Получит последний фрамент запрашиваемого URL (пути)
     *
     * @return string
     */
    public static function GetLastUrlFragment()
    {
        return self::$aliases[count(self::$aliases) - 1];
    }
    
    /**
     * Добавить фрагмент -- можно использовать для назначения раздела по-умолчанию-
     * для выбора дочерней страницы по-умолчанию (если вид корневой не определён) при последующей загрузке с помощью $this->page = new Page(Request::GetAliases());
     * 
     * Например: можно загрузить вместо запрощенной /projects страницу /projects/telecom
     * 
     * Пример использования:
     *   Request::addAliasFragment('telecom');
     *   $this->page = new Page(Request::GetAliases());
     * 
     * @param string $additionalAlias   некая строка, собственный alias дочерней страницы
     * @return array
     */
    public static function addAliasFragment($additionalAlias)
    {
        return self::$aliases[] = $additionalAlias;
    }
    
    public static function GetHost()
    {
        return isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : AppSettings::$HTTP_HOST;
    }

    public static function GetProtocol()
    {
        $https = false || (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off');
        return $https ? 'https' : 'http';
    }

    public static function GetBaseUrl()
    {

        $port  = empty($_SERVER['SERVER_PORT']) ? 80 : $_SERVER['SERVER_PORT'];
        return sprintf("%s://%s%s/", self::GetProtocol(), self::GetHost(), (($port == 80 && !$https) || ($port == 443 && $https)) ? '' : ':' . $port
        );
    }

    /**
     * Инициализация запроса
     *
     */
    public static function Init()
    {
        self::$GET  = &$_GET;
        self::$POST = &$_POST;
        self::$COOKIE = &$_COOKIE;
        self::$REFERER = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';

        // Сохраняем тип запроса
        if (isset($_SERVER['REQUEST_METHOD']))
        {
            switch ($_SERVER['REQUEST_METHOD'])
            {
                case 'GET':
                    self::$requestMethod = self::GET;
                    break;
                case 'POST':
                    self::$requestMethod = self::POST;
                    break;
                case 'PUT':
                    self::$requestMethod = self::PUT;
                    break;
                case 'DELETE':
                    self::$requestMethod = self::DELETE;
                    break;
                case 'HEAD':
                    self::$requestMethod = self::HEAD;
                    break;
                default:
                    self::$requestMethod = self::GET;
                    break;
            }
        }
        else
        {
            self::$requestMethod = self::GET;
        }

        // Проверяем является ли запрос асинхронным
        // Должно быть передано async или ajax в посте или по заголовку jQuery
        if ( //isset(self::$POST['async'])
          //|| isset(self::$POST['ajax'])
          //||
          (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
             && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest'))
             self::SetAsync();

        // Заполняем массив алиасов
        if (!isset($requestedPageAlias))
        {
            // Получаем алиас обрабатываемой страницы
            $requestedPageAlias = isset($_GET['page_alias']) ? $_GET['page_alias'] : (isset($_SERVER['argv'][1]) ? $_SERVER['argv'][1] : '');

            // Отрезаем слешы по краям
            $requestedPageAlias = trim($requestedPageAlias, '/');

            // Переводим в нижний регистр
            $requestedPageAlias = UTF8::strtolower($requestedPageAlias);

            // Разбиваем весь путь алиасы.
            self::$aliases = explode('/', $requestedPageAlias);
        }
        else
            self::$aliases = null;
    }
}
?>
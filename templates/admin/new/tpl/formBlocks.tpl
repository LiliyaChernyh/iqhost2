{if isset($FORM_BLOCKS)}
<script type="text/javascript">
{literal}
$(document).ready(function()
{
{/literal}
{* Вызовем инициализирующие функции и передадим им переменные *}
    {foreach from=$FORM_BLOCKS item=BLOCK}
        {foreach from=$BLOCK.controls item=CONTROL}

            if (typeof {$CONTROL.type}Init != "undefined")
            {ldelim}
                var {$CONTROL.type}{$CONTROL.name}InitObj = {$CONTROL.controlJSVarsJSON};
                {$CONTROL.type}Init({$CONTROL.type}{$CONTROL.name}InitObj);
            {rdelim}
        {/foreach}
    {/foreach}

{literal}
})
{/literal}
</script>

{foreach from=$FORM_BLOCKS item=BLOCK}
<div class="formBlockHeader">{if !empty($BLOCK.blockName)}{$BLOCK.blockName}{/if}</div>
<div class="formBlock">

	{foreach from=$BLOCK.controls item=CONTROL}
		<div class="formElement{if isset($CONTROL.CSSClass)} {$CONTROL.CSSClass}{/if}{if isset($CONTROL.type)} {$CONTROL.type}{/if}">
			<label for="{$CONTROL.name}{if isset($CONTROL.formIndex)}_{$CONTROL.formIndex}{/if}" class="formElementLabel">
				{$CONTROL.description}
				{if !empty($CONTROL.tip)}<span class="helper" title="{$CONTROL.tip}">?</span>{/if}
			</label>
			{if isset($CONTROL.controlHTML)}
				{$CONTROL.controlHTML}
			{else}
				{if isset($CONTROL.controlTplPath)}
					{include file=$CONTROL.controlTplPath localVars=$CONTROL.controlTplVars}
				{/if}
			{/if}
			{* {$CONTROL|var_dump} *}
		</div>
	{/foreach}
</div>
<div style="clear:both"></div>
{/foreach}
{/if}
<?php
/**
 * Фабрика контролов.
 * Создаёт контрол по параметрам из реестра контролов
 *
 * @version 1.2
 *
 */
class ControlFactory
{
    /**
     * Путь к папке с контролами
     *
     * @var string
     */
    public static $controlsPath;

    public static function CreateControl($controlProps, $controlsRegistry = null, $tableName = null, $formIndex = null, $data = array())
	{
    	$controlType = $controlProps['controlType'];
    	if (empty(self::$controlsPath))
    	   self::$controlsPath = IncPaths::$LIBS_PATH . 'ContentManager/Controls/';
    	   //throw new Exception('Не задан путь к контролам', 3230);
    	if (!file_exists(self::$controlsPath))
    	   throw new Exception('Заданного пути к контролам не существует', 3231);
		// Смотрим есть ли такой тип элемента управления
		$controlClassDefinitionFile = self::$controlsPath . $controlType .'/'.$controlType.'.class.php';
		if (file_exists($controlClassDefinitionFile))
		{
			require_once($controlClassDefinitionFile);
			$null = null;

			$control = new $controlType(
                            isset($controlProps['name'])          ? $controlProps['name'] : null,
                            isset($controlProps['description'])   ? $controlProps['description'] : null,
                            isset($controlProps['tip'])           ? $controlProps['tip'] : null,
                            isset($controlProps['required'])      ? $controlProps['required'] : null,
                            isset($controlProps['visible'])       ? $controlProps['visible'] : null,
                            isset($controlProps['disabled'])      ? $controlProps['disabled'] : null,
                            isset($controlProps['controlOrder'])  ? $controlProps['controlOrder'] : null,
                            isset($controlProps['blockOrder'])    ? $controlProps['blockOrder'] : null,
                            isset($controlProps['blockName'])     ? $controlProps['blockName'] : null,
                            isset($controlProps['CSSClass'])      ? $controlProps['CSSClass'] : null,
                            isset($controlProps['controlSettings']) ? $controlProps['controlSettings'] : null,
                            $controlsRegistry,
							isset($tableName) ? $tableName : null, isset($formIndex) ? $formIndex : null, !empty($data) ? $data : array()
			);
			$control -> fieldRegistryID = $controlProps['entryID'];
		}
		else
		{
			throw new FileNotFoundException('Не найден класс контрола '.$controlClassDefinitionFile, 3233);
		}

        return $control;
    }
}
?>
<h1>Список статей</h1>

{literal}
<script type="text/javascript">
    $(document).ready(function(){
        $('.deleteCheckBox').change(EnableDisableDeleteButton);
    });

    function EnableDisableDeleteButton()
    {
        var checkedNumber = $('.deleteCheckBox:checked').length;

        if (checkedNumber == 0)
            $('#deleteItems').attr('disabled', true);
        else
            $('#deleteItems').attr('disabled', false);
    }
</script>
{/literal}

<div style="text-align: right; margin: -30px 0 5px 0;">
<form action="" method="post">
    <input type="button" id="goAddNews" class="goToBtn" value="Добавить статью" />
    <input type="hidden" id="goAddNews_link" value="{$MODULE_ALIAS}add/" />
</form>
</div>

{if $ARTICLES_LIST|@count == 0}
    Список статей пуст
{else}
     {include file='paginator_new.tpl'}
    <form action="{$MODULE_ALIAS}deletes/" method="POST" onsubmit="return AreYouSure()">
    <table id="entriesList" class="highlightable" style="clear: left;">
    	<thead>
    		<tr>
    			<th width="5%">№</th>
    			<!-- <th width="5%">&nbsp;</th> -->
    			<th width="20%">Заголовок статьи</th>
    			<th width="30%">Категория</th>
    			<th width="15%">Дата публикации</th>
    			<th width="15%">Дата изменения</th>
    			<th width="10%" style="text-align:center;"><span>Опубликована</span></th>
    			<th width="5%" style="text-align:center;"><img src="{$ADMIN_IMAGES}delete.png" title="Поставьте галочку, если хотите удалить статью" /></th>
    		</tr>
    	</thead>
    	<tbody>
            {foreach from=$ARTICLES_LIST item=ARTICLES_ITEM key=I}
    		<tr>
                <td style="display: none;"><a href="{$MODULE_ALIAS}edit/{$ARTICLES_ITEM.articleID}/"></a></td>
    			<td>{$ARTICLES_ITEM.articleID}</td>
    			<!-- <td>{if !empty($ARTICLES_ITEM.imageID)}<img src="/getimage/{$ARTICLES_ITEM.imageID}/50x50/">{else} {/if}</td> -->
    			<td>{$ARTICLES_ITEM.caption|truncate:50}</td>
    			<td>{$ARTICLES_ITEM.cat_caption|truncate:60}</td>
    			<td>{$ARTICLES_ITEM.publicationDate|@date_format:"%d.%m.%Y %H:%M"}</td>
    			<td>{$ARTICLES_ITEM.modificationDate|@date_format:"%d.%m.%Y %H:%M"}</td>
    			<td class="skip">{if $ARTICLES_ITEM.isPublished == 1}<span style="color: green;">Да</span>{else}<span style="color: red;">Нет</span>{/if}</td>
    			<td class="skip"><input type="checkbox" name="toDelete[]" class="deleteCheckBox" value="{$ARTICLES_ITEM.articleID}" /></td>
    		</tr>
    		{/foreach}
    	</tbody>
    </table>

    <div class="deleteBtn" style="margin-top: 10px; text-align: right;">
    	<input type="submit" id="deleteItems" name="doDelete" value="Удалить отмеченные" />
    </div>
    </form>
{/if}

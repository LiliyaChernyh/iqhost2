<?php
/**
 * Подгрузка файлов, которые в любом случае будут использоваться в проекте для
 * *ускорения работы autoload'a
 */
require_once(IncPaths::$LIBS_PATH . 'IO/File.class.php');
require_once(IncPaths::$LIBS_PATH . 'Cache/Cache.class.php');
require_once(IncPaths::$LIBS_PATH . 'FirePHPCore/fb.php');
require_once(IncPaths::$CORE_PATH . 'ObjectsCache.class.php');
require_once(IncPaths::$APPLICATION_PATH . 'AppSettings.class.php');

/*
$rootPath = RootDir();
// Подключаем файл с основными настройками приложения
require_once($rootPath. 'AppSettings.class.php');

/*
define('ROOT_PATH', RootDir());
define('LIBS_PATH', ROOT_PATH .'libs/');
define('MODULES_PATH', ROOT_PATH .'modules/');
define('SYSTEM_PATH', '/');
define('LIBS_ALIAS', SYSTEM_PATH .'libs/');
define('MODULES_ALIAS', SYSTEM_PATH .'modules/');
*/
/*
require_once(AppSettings::$LIBS_PATH. 'UTF8/UTF8.class.php');
require_once(AppSettings::$APPLICATION_PATH. 'SuperPowerSection.class.php');
require_once(AppSettings::$APPLICATION_PATH. 'Section.class.php');

require_once(AppSettings::$LIBS_PATH. 'DBManager/includes.php');
require_once(AppSettings::$APPLICATION_PATH. 'config.php');

require_once(AppSettings::$LIBS_PATH. 'exceptions/includes.php');
require_once(AppSettings::$LIBS_PATH. 'Smarty/Template.class.php');

require_once(AppSettings::$LIBS_PATH. 'Structure/SiteStructure.class.php');
require_once(AppSettings::$CORE_PATH. 'Singleton.abstract.php');
require_once(AppSettings::$LIBS_PATH. 'SettingsManager/DBSettingsManager.class.php');
require_once(AppSettings::$LIBS_PATH. 'JSON/Services_JSON.class.php');
require_once(AppSettings::$LIBS_PATH. 'FileManager/FileManager.class.php');
require_once(AppSettings::$LIBS_PATH. 'FileManager/ImageManager.class.php');
require_once(AppSettings::$LIBS_PATH. 'UsersManager/UsersWithRolesManager.class.php');
require_once(AppSettings::$LIBS_PATH. 'Validator/Validator.class.php');
require_once(AppSettings::$LIBS_PATH. 'Modules/Modules.class.php');
require_once(AppSettings::$LIBS_PATH. 'BitPlatformMailer/BitPlatformMailer.class.php');

require_once(AppSettings::$LIBS_PATH. 'CurrentProject/CurrentProjectUsersManager.class.php');

define('DEFAULT_ALIAS', 'main');

function trace($toTrace)
{
	echo '<pre>'; print_r($toTrace); echo '</pre>';
}

function RootDir()
{
	$fullDirPath = str_replace('\\', '/', getcwd());
    $localDirPath = substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
    return str_replace($localDirPath, '/', $fullDirPath) . '/';
}

function processing_time($START = false)
{
    return $START ? round(time() + microtime() - $START, 4) : time() + microtime();
}
*/
?>
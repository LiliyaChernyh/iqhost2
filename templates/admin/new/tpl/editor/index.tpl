{include file="editor/head.tpl"}
<div>
	Список файлов для редактирования:
	<ul>
		{foreach from=$FILES item=file key=key}
			<li>
				<a href="/admin/editor/{$key}/">{$file}</a>
			</li>
		{/foreach}
	</ul>
</div>
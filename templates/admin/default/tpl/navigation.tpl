	{if isset($USER)}
	<ul id="nav" class="ddm">
		<li>
			<a href="{$ADMIN_ALIAS}" title="Управления структурой сайта">Страницы</a>
		</li>
		{if isset($ANYEDITOR_OBJECTS)}
		<li>
			<a href="{$ADMIN_ALIAS}AnyEditor/" title="">Данные</a>
                        <ul>
                        {foreach from=$ANYEDITOR_OBJECTS item=OBJECT}
                                <li><a href="{$OBJECT.link}" title="">{$OBJECT.name}</a></li>
                        {/foreach}
                        </ul>
		</li>
		{/if}
                <li>
                        <a href="{$ADMIN_ALIAS}modules/">Модули</a>
                        {if !empty($ACTIVE_MODULES)}
                        <ul>
                        {foreach from=$ACTIVE_MODULES item=MODULE}
                                <li><a href="{$ADMIN_ALIAS}{$MODULE.name}" title="">{$MODULE.description}</a></li>
                        {/foreach}
                        </ul>
                        {/if}
                </li>
		<li>
			<a href="{$ADMIN_ALIAS}profile/" title="Управление учетной записью">Профиль</a>
		</li>
		{if $USER.role == 'admin'}
		<li>
			<a href="#">Администрирование</a>
			<ul>
				<li><a href="{$ADMIN_ALIAS}users/" title="Управление учётными записями">Пользователи</a></li>
				<li><a href="{$ADMIN_ALIAS}settings/" title="Настройки сайта">Настройки</a></li>
				<li><a href="{$ADMIN_ALIAS}routing/" title="Роутинг">Роутинг</a></li>
                <li><a href="{$ADMIN_ALIAS}fieldsregistry/" title="Регистратор полей">Fieldregistry</a></li>
                <li><a href="{$ADMIN_ALIAS}maintenance/" title="Обслуживание сайта">Обслуживание</a></li>
			</ul>
		</li>
		{/if}
		<li>
			<a href="{$ADMIN_ALIAS}profile/logout/" title="Выйти из системы" onclick="return AreYouSure()">Выход [ <strong>{$USER.login}</strong> ]</a>
		</li>
	</ul>
	{/if}
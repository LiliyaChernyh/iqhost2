{if !empty($CUSTOMMENU)}
    {foreach from=$CUSTOMMENU key=key item=item}
        {if $key=='SERVICE_MENU'}
            <div class="carousel">
        <div class="main-nav">
            <ul id="nav">
                {foreach from=$item key=key item=itemMenu}
                    <li{if $itemMenu.alias == $PAGE.fullAlias} class="active"{elseif $PAGE.fullAlias == "/main/" && $itemMenu.alias == "/services/hosting/"} class="active"{/if}><a href="{$itemMenu.alias}">{if isset($itemMenu.imageID) && !empty($itemMenu.imageID)}<span class="img"><img src="/getimage/{$itemMenu.imageID}/" alt="image description" width="40"></span>{/if}{$itemMenu.name}</a></li>
                    {/foreach}
                </ul>
        </div>
        <a href="#" class="btn-prev">Prev</a>
        <a href="#" class="btn-next">Next</a>
    </div>
        {/if}
    {/foreach}
{/if}
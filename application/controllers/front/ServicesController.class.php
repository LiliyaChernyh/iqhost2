<?php

class ServicesController extends FrontController {

    public function __construct()
    {
        parent::__construct();
        $this->view = new ServicesView();
        $this->model = new ServicesModel();
    }

    public function Index($args = null)
    {
        if (isset($args) && empty($args))
            throw new PageNotFoundException('Page not found', 404);
        /* $alias = Request::GetAliases();
          $this->page = new Page($alias);
          $content = $this->page->Content();
          $section = new Page('solutions');
          $ps = PagesStructure::getInstance();
          $childPages = $ps->GetChildren($section->pageID, array('isActive' => 1, 'isDeleted' => 0));
          $pages = array();
          foreach ($childPages as $child) {
          $pages[] = array(
          'alias' => $child['alias'],
          'caption' => $child['caption'],
          'active' => ($child['alias'] == $this->page->alias),
          );
          }
          ViewData::Assign('SOLUTION', $content);
          return $this->view->Index(); */
    }

    public function Domains($args = null)
    {
        $this->page = new Page(Request::GetAliases());
        $content = $this->page->Content();
        $domainsTable = DBTableManager::getInstance('Domains');
        $domainsTable->SortOrder = 'priority ASC';
        $domains = $domainsTable->Select(array('isActive' => 1));
        ViewData::Assign('CONTENT', $content);
        ViewData::Assign('DOMAINS', $domains);
        return $this->view->Domains();
    }

    public function Hosting($args = null)
    {
        $this->page = new Page(Request::GetAliases());
        $count = count($args);
        $content = $this->page->Content();
        $hostingTable = DBTableManager::getInstance('Hosting');
        $hostingTable->SortOrder = 'priority ASC';
        $hosting = $hostingTable->Select(array('isActive' => 1));
        foreach ($hosting as $key => &$value) {
            $optionsTable = DBTableManager::getInstance('HostingOptions');
            $optionsTable->SortOrder = 'priority ASC';
            $hosting[$key]['gallery'] = $optionsTable->Select(array('hostingID' => $hosting[$key]['hostingID']));
        }

        ViewData::Assign('CONTENT', $content);
        ViewData::Assign('HOSTING', $hosting);

        return $this->view->Hosting();
    }

    public function HostingBitrix($args = null)
    {
        $this->page = new Page(Request::GetAliases());
        $count = count($args);
        $content = $this->page->Content();
        $hostingTable = DBTableManager::getInstance('HostingBitrix');
        $hostingTable->SortOrder = 'priority ASC';
        $hosting = $hostingTable->Select(array('isActive' => 1));
        foreach ($hosting as $key => &$value) {
            $optionsTable = DBTableManager::getInstance('HostingBitrixOptions');
            $optionsTable->SortOrder = 'priority ASC';
            $hosting[$key]['gallery'] = $optionsTable->Select(array('hostingID' => $hosting[$key]['hostingID']));
        }
//        trace($servers);
        ViewData::Assign('CONTENT', $content);
        ViewData::Assign('HOSTING', $hosting);

        return $this->view->Hosting();
    }

    public function Servers($args = null)
    {
        $this->page = new Page(Request::GetAliases());
        $count = count($args);
        $content = $this->page->Content();
        $serversTable = DBTableManager::getInstance('Servers');
        $serversTable->SortOrder = 'priority ASC';
        $servers = $serversTable->Select(array('isActive' => 1));
        foreach ($servers as $key => &$value) {
            $optionsTable = DBTableManager::getInstance('ServersOptions');
            $optionsTable->SortOrder = 'priority ASC';
            $servers[$key]['gallery'] = $optionsTable->Select(array('serverID' => $servers[$key]['serverID']));
        }
//        trace($servers);
        ViewData::Assign('CONTENT', $content);
        ViewData::Assign('SERVERS', $servers);

        return $this->view->Servers();
    }

    public function VPS($args = null)
    {
        $this->page = new Page(Request::GetAliases());
        $count = count($args);
        $content = $this->page->Content();
        $serversTable = DBTableManager::getInstance('VPS');
        $serversTable->SortOrder = 'priority ASC';
        $servers = $serversTable->Select(array('isActive' => 1));
        foreach ($servers as $key => &$value) {
            $optionsTable = DBTableManager::getInstance('VPSOptions');
            $optionsTable->SortOrder = 'priority ASC';
            $servers[$key]['gallery'] = $optionsTable->Select(array('vpsID' => $servers[$key]['vpsID']));
        }
//        trace($servers);
        ViewData::Assign('CONTENT', $content);
        ViewData::Assign('SERVERS', $servers);

        return $this->view->Servers();
    }

    public function GPU($args = null)
    {
        $this->page = new Page(Request::GetAliases());
        $count = count($args);
        $content = $this->page->Content();
        $serversTable = DBTableManager::getInstance('GPU');
        $serversTable->SortOrder = 'priority ASC';
        $servers = $serversTable->Select(array('isActive' => 1));
        foreach ($servers as $key => &$value) {
            $optionsTable = DBTableManager::getInstance('GPUOptions');
            $optionsTable->SortOrder = 'priority ASC';
            $servers[$key]['gallery'] = $optionsTable->Select(array('gpuID' => $servers[$key]['gpuID']));
        }
//        trace($servers);
        ViewData::Assign('CONTENT', $content);
        ViewData::Assign('SERVERS', $servers);

        return $this->view->Servers();
    }

    public function WhoIS($args = null)
    {
        if (Request::IsAsync()) {
            try {
                if (isset(Request::$POST['domain']))
                    $domain = Request::$POST['domain'];
                $api = "http://whoisapi.netfox.ru/api_v1/?domain=$domain";
                $result = file_get_contents($api);
            } catch (Exception $e) {
                return new JsonActionResult(array('error' => $e->getMessage(), 'errno' => $e->getCode()));
            }
            if ($result)
                return new JsonActionResult(array('msg' => 'Домен занят.', 'message' => 'Домен занят.'));
            else
                return new JsonActionResult(array('msg' => 'Домен свободен.', 'message' => 'Домен свободен.'));
        }
        else
            return new JsonActionResult(array('msg' => 'error'));
    }

    public function Service($args = null)
    {
        $alias = Request::GetAliases();
        $this->page = new Page($alias);
        $content = $this->page->Content();

        $section = '';
        if (count($alias) > 2) {
            $section = new Page(array($alias[0], $alias[1]));
            $fullAlias = $section->FullAlias();
        }
        $ps = PagesStructure::getInstance();
        if (!empty($section)) {
            $childPages = $ps->GetChildren($section->pageID, array('isActive' => 1, 'isDeleted' => 0));
        }
        else {
            $childPages = $ps->GetChildren($this->page->pageID, array('isActive' => 1, 'isDeleted' => 0));
            $fullAlias = $this->page->FullAlias();
        }
        $pages = array();
        foreach ($childPages as $child) {
            $pages[] = array(
                'alias' => $child['alias'],
                'caption' => $child['caption'],
                'active' => ($child['alias'] == $this->page->alias),
            );
        }

        if (!empty($content['serviceID'])) {
            $menu = $this->model->getMenuService($content['serviceID']);
            ViewData::Assign('MENU_SOLUTION', $menu);
        }

        ViewData::Assign('PAGES', $pages);
        ViewData::Assign('CONTENT', $content);
        ViewData::Assign('ALIAS', $fullAlias);

        if (!empty($content['bannerSolutionID'])) {
            ViewData::Assign('BANNER', $this->model->getBanner($content['bannerSolutionID']));
        }
        if (!empty($content['serviceID'])) {
            ViewData::Assign('ADVANTAGES_SOLUTION', $this->model->getAdvantages($content['serviceID']));
        }
        if (!empty($content['serviceID'])) {
            ViewData::Assign('RATES', $this->model->getRatesService($content['serviceID']));
        }

        return $this->view->Service();
    }
    
    public function Mail($args = null)
    {
        $this->page = new Page(Request::GetAliases());
        $content = $this->page->Content();

        $ps = PagesStructure::getInstance();
        $childPages = $ps->GetChildren($this->page->pageID, array('isActive' => 1, 'isDeleted' => 0));
        $alias = $this->page->FullAlias(); 
        $pages = array();
        foreach ($childPages as $child) {
            $pages[] = array(
                'alias' => $child['alias'],
                'caption' => $child['caption'],
                'active' => ($child['alias'] == $this->page->alias),
            );
        }

        if (!empty($content['serviceID'])) {
            $menu = $this->model->getMenuService($content['serviceID']);
            ViewData::Assign('MENU_SOLUTION', $menu);
        }
        
        $features = $this->model->getFeatures();
        $middle = round(count($features)/2);
        foreach ($features as $k=>$v){
            if($k < $middle){
                $features['one'][] = $v;
            }
            else{
                $features['two'][] = $v;
            }
        }

        ViewData::Assign('PAGES', $pages);
        ViewData::Assign('CONTENT', $content);
        ViewData::Assign('ALIAS', $alias);
        ViewData::Assign('FEATURES', $features);
        ViewData::Assign('MAILBOX_SERVICES', $this->model->getMailboxServices());

        if (!empty($content['bannerSolutionID'])) {
            ViewData::Assign('BANNER', $this->model->getBanner($content['bannerSolutionID']));
        }
        
        return $this->view->Mail();
    }

}

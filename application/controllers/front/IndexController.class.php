<?php
/**
 * Контроллер для отображения главной страницы
 *
 * @version 1.0
 *
 */
class IndexController extends FrontController 
{
    public function __construct()
    {
        parent::__construct();

        // Создаем представление
        $this -> view = new IndexView();
    }

    public function Index($args = null)
    {
        // Получаем страницу из базы
        $this -> page = new Page('main');
        $content = $this->page->Content();
        $hostingTable = DBTableManager::getInstance('Hosting');
        $hostingTable -> SortOrder = 'priority ASC';
        $hosting = $hostingTable->Select(array('isActive' => 1));
        foreach ($hosting as $key => &$value) {
            $optionsTable = DBTableManager::getInstance('HostingOptions');
            $optionsTable->SortOrder = 'priority ASC';
            $hosting[$key]['gallery'] = $optionsTable->Select(array('hostingID' => $hosting[$key]['hostingID']));
        }
//        trace($servers);
        ViewData::Assign('CONTENT', $content);
        ViewData::Assign('HOSTING', $hosting);
        
        return $this -> view;
    }
    
}
?>
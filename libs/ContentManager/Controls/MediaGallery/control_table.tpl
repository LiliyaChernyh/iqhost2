<input type="hidden" name="{$localVars.tableName}[{$localVars.formIndex}][{$localVars.NAME}][]" value="0"/>
<div class="feedback_messageItem {$localVars.NAME}">
	<table class="highlightable " style="width: 1020px;">
		<tr>
			{foreach from=$localVars.FIELDS key=name item=field}
				<th>
					{$field.name}
				</th>
			{/foreach}
			<th width="20%">Добавить поле <a href="#" id="{$localVars.NAME}_add">+</a></th>
		</tr>
		<tr style="display:none" class="default">
			{foreach from=$localVars.FIELDS key=name item=field}
				<td>
					{if $field.type == "select"}
						<select name="{$localVars.NAME}__{$name}[]">
							<option value=""> --- </option>
							{foreach from=$field.data item=OPTION}
								<option value="{$OPTION.VALUE}">{$OPTION.TEXT}</option>
							{/foreach}
						</select>
					{elseif $field.type == "hidden"}
						<input type="hidden" name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}__{$name}]{else}{$localVars.NAME}__{$name}{/if}[]" value=""  />
					{elseif $field.type == "media"}
						{*						<textarea name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}__{$name}]{else}{$localVars.NAME}__{$name}{/if}[]" style="top:-9999px"></textarea>*}
					{else}
						<input type="text" name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}__{$name}]{else}{$localVars.NAME}__{$name}{/if}[]" value=""  />
					{/if}
				</td>
			{/foreach}
			<td style="text-align: center;">
				<input class="btn" value="-" type="button" alt="" onclick="{literal}$(this).parents('tr').remove();{/literal}"/>
			</td>
		</tr>
		{foreach from=$localVars.OPTIONS key=ind item=item}
			<tr id="itms_{$ind}">
				{foreach from=$item key=field item=val}
					<td {if $localVars.FIELDS[$field].type == "media"}width="300"{/if}>
						{if $localVars.FIELDS[$field].type == "select"}
							<select name="{$localVars.NAME}__{$field}[]">
								<option value=""> --- </option>
								{foreach from=$localVars.FIELDS[$field].data item=OPTION}
									<option value="{$OPTION.VALUE}" {if $val == $OPTION.VALUE} selected="selected"{/if}>{$OPTION.TEXT}</option>
								{/foreach}
							</select>
						{elseif $localVars.FIELDS[$field].type == "hidden"}
							<input type="hidden" name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}__{$field}]{else}{$localVars.NAME}__{$field}{/if}[]" value="{$val}"  />
						{elseif $localVars.FIELDS[$field].type == "media"}
							<textarea name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}__{$field}]{else}{$localVars.NAME}__{$field}{/if}[]" style="position: absolute; left: -1000px;">{$val}</textarea>
							<input type="checkbox" name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}__{$field}_update]{else}{$localVars.NAME}__{$field}_update{/if}[]" value="1" /><label>Update</label>
						{if !empty($val)}{$val}{else}формат не опознан{/if}
					{else}
						<input type="text" name="{if !empty($localVars.tableName)}{$localVars.tableName}[{if isset($localVars.formIndex)}{$localVars.formIndex}][{/if}{$localVars.NAME}__{$field}]{else}{$localVars.NAME}__{$field}{/if}[]" id="{$localVars.NAME}__{$field}" value="{$val}" style="width:90%" />
					{/if}
					</td>
				{/foreach}
				<td style="text-align: center;">
					<input class="btn" value="-" type="button" alt="" onclick="{literal}$(this).parents('tr').remove();{/literal}"/>
				</td>
			</tr>
		{/foreach}
	</table>
</div>

<?php

/**
 * Класс для работы с Фотобанком
 *
 * @version  1.0
 */
class Fotobank extends CommonModule
{
    public function __construct()
    {
            parent::__construct(__CLASS__);
    }
        
    public function FileUpload($tagID = null)
    {
        @$upload_handler = new UploadHandler(array('tagID' => $tagID,
                                                  'script_url' => '/admin/Fotobank/file-upload/'.$tagID,
                                                  'upload_dir' => $_SERVER['DOCUMENT_ROOT'].'/uploaded/foto/',
                                                  'upload_url' => '/uploaded/foto/',
                                                  'accept_file_types' => '/\.(gif|jpe?g|png)$/i',
                                                  'min_width' => 376,
                                                  'min_height' => 376,
                                                  /* максимальное количество фоток для разовой загрузки */
                                                  'max_number_of_files' => 50,
                                                  'access_control_allow_origin' => 'PUT',
                                                  'image_versions' => array(
                                                      /* WQUXGA — 3840×2400 — 9,2 МПикс */
                                                      '' => array(
                                                          'max_width' => 3840,
                                                          'max_height' => 2400,
                                                          'jpeg_quality' => 95
                                                      ),
                                                      'medium' => array(
                                                          'crop' => true,
                                                          'max_width' => 376,
                                                          'max_height' => 376,
                                                          'jpeg_quality' => 80
                                                      ),
                                                      'thumbnail' => array(
                                                          'crop' => true,
                                                          'max_width' => 152,
                                                          'max_height' => 152
                                                      )
                                                  )
                                            )
                          );
        
        exit();
    }
    
    public function saveRecipeImage($recipeID, $imageID)
    {
        $query = 'INSERT INTO RecipeImages (`recipeID`, `imageID`, `order`)
                  VALUE ('.$recipeID.', '.$imageID.', 0)';
   
        return DB::Query($query);
    }
    
    public function deleteRecipeImage($recipeID, $imageID)
    {
        $query = 'DELETE FROM RecipeImages
                  WHERE 
                    `recipeID` = '.$recipeID.' AND
                    `imageID` = '.$imageID;
   
        return DB::Query($query);
    }
    
    public function get_treeTags($tag_caption)
    {
        $query = 'SELECT * FROM `fb_typeTags`
                  ORDER BY `caption`';
        $typeTags = DB::QueryToArray($query);
        
        $query = 'SELECT * FROM `fb_Tags`
                  ORDER BY `caption`';
        $tags = DB::QueryToArray($query);
        
        //получаем $tag_caption
        if (!is_null($tag_caption))
            foreach ($tags as $item){
                if ($item['tagID'] == $tag_caption) $tag_caption = $item['caption'];
            }

        foreach ($typeTags as &$typeTag){
            foreach ($tags as $tag){
                if ($typeTag['typeTagID'] == $tag['typeTagID']){
                    $typeTag['tags'][] = $tag;
                    
                }
            }
        }
     
        //удаляем типы тегов (разделы) без тегов
        foreach ($typeTags as $key => $item){
            if (!isset($item['tags'])){
                unset($typeTags[$key]);
            }
        }
  
        return $typeTags;
    }
    
    public function update_foto($imageID, $caption, $description, $tagsID)
    {
        $ar = array();
        if (!is_null($caption)) $ar[] = ' `caption` = "'.$caption.'" ';
        if (!is_null($description)) $ar[] = ' `description` = "'.$description.'" ';
        $set = implode(",", $ar);
        
        if ($set){
            $query = 'UPDATE `'.TablesNames::$IMAGES_TABLE_NAME.'` 
                      SET 
                        '.$set.'  
                      WHERE 
                        `imageID` = "'.$imageID.'"';

            $res_update_img = DB::Query($query);
        }

        if (is_array($tagsID)){
            $query = 'DELETE FROM `fb_TagsImages`
                      WHERE `imageID` = "'.$imageID.'"';
            
            $res_del_tags = DB::Query($query);

            foreach ($tagsID as $value) {
                if ($value != 0){
                    $query = 'INSERT INTO `fb_TagsImages`
                              SET 
                                `tagID` = "'.$value.'",
                                `imageID` = "'.$imageID.'"';

                    $res_insert_tags = DB::Query($query);
                }
            }
        }
    }
    
    public function get_first_char_users()
    {
        $query = 'SELECT
                    LEFT(usr.`FIO`,1) AS `char`,
                    usr.`FIO`,
                    usr.`userID`
                  FROM `Users` usr
                  WHERE usr.`role` = "user"
                  ORDER BY `char` ASC';
        
        $res = DB::QueryToArray($query);
        
        $ar = array();
        foreach ($res as $item){
            $ar[$item['char']][$item['userID']] = $item['FIO'];
        }

        return $ar;
    }
    
    public function get_types_and_tags($userID = null)
    {
        if (!is_null($userID)){
            $query = 'SELECT
                        tg.`tagID`
                      FROM `fb_Tags` tg 
                      INNER JOIN `fb_UsersTags` ut ON ut.`tagID` = tg.`tagID`
                      WHERE ut.`userID` = "'.$userID.'"
                      ORDER BY tg.`caption` ASC';
        
            $res_user_tg = DB::QueryToArray($query);
            
            if (is_array($res_user_tg)){
                $ar_user_tg = array();
                foreach ($res_user_tg as $item){
                    $ar_user_tg[] = $item['tagID'];
                }
            }
        }
        
        $query = 'SELECT
                    tt.*
                  FROM `fb_typeTags` tt
                  ORDER BY tt.`caption` ASC';

        $res_tt = DB::QueryToArray($query);

        $query = 'SELECT
                    tg.*
                  FROM `fb_Tags` tg 
                  ORDER BY tg.`caption` ASC';
        
        $res_tg = DB::QueryToArray($query);
        
        foreach ($res_tt as $key => &$item_tt){
            foreach ($res_tg as $key_tg => $item_tg){
                if ($item_tt['typeTagID'] == $item_tg['typeTagID']){
                    //считаем количество фото в теге
                    if (is_null($userID) or isset($ar_user_tg)?in_array($item_tg['tagID'], $ar_user_tg):1){
                        $query = 'SELECT COUNT(*)
                                  FROM `fb_TagsImages` ti 
                                  WHERE ti.`tagID` = "'.$item_tg['tagID'].'"';
                        
                        $item_tg['count_foto'] = DB::QueryOneValue($query);
                    }else{
                        $query = 'SELECT COUNT(*)
                                  FROM `fb_TagsImages` ti 
                                  INNER JOIN `fb_UsersImages` ui ON ui.`imageID` = ti.`imageID` 
                                  WHERE ti.`tagID` = "'.$item_tg['tagID'].'" AND
                                        ui.`userID` = "'.$userID.'"';
                        
                        $item_tg['count_foto'] = DB::QueryOneValue($query);
                        
                        if ($item_tg['count_foto'] == 0) continue;
                    }
                    
                    $item_tt['children'][] = $item_tg;
                }
            }
            
            if (!isset($item_tt['children'])) unset($res_tt[$key]);
        }
        
        //добавляем раздел "Без тега" ************************************************************
        if ($count_foto = $this->get_fotos_by_tagID(0, true, $userID)){
            $ar = array();
            $ar['typeTagID'] = 0;
            $ar['caption'] = 'ФОТО БЕЗ ТЕГОВ';
            $ar['children'] = array(0 => array(
                'tagID' => 0,
                'typeTagID' => 0,
                'caption' => 'РАЗДЕЛ',
                'count_foto' => $count_foto
            ));
            
            $res_tt[] = $ar;
        }
        // ***************************************************************************************

        return $res_tt;
    }
 
    public function get_fotos_by_tagID($tagID, $ok_count = false, $userID = null)
    {
        if ($ok_count) $fields = ' COUNT(*) ';
        else $fields = ' img.* ';
        
        $join = null;
        $cond = null;
        if ($tagID == 0){
            $cond = ' WHERE NOT img.`imageID` IN (SELECT ti.`imageID` FROM `fb_TagsImages` ti) ';
        }else{
            $join = ' INNER JOIN `fb_TagsImages` ti ON ti.`imageID` = img.`imageID` ';
            $cond = ' WHERE ti.`tagID` = "'.$tagID.'" ';
        }
        
        if (!is_null($userID)){
            //если пользователь имеет доступ к данному тегу, то выводим все фото тега
            $query = 'SELECT COUNT(*) FROM `fb_UsersTags` ut
                      WHERE ut.`userID` = "'.$userID.'" AND
                            ut.`tagID` = "'.$tagID.'"';
            
            $ok = DB::QueryOneValue($query);
            if (!$ok){
                $join .= ' INNER JOIN `fb_UsersImages` ui ON img.`imageID` = ui.`imageID` ';
                $cond .= ' AND ui.`userID` = "'.$userID.'"';
            }
        }
        
        $query = 'SELECT
                  '.$fields.'  
                  FROM `'.TablesNames::$IMAGES_TABLE_NAME.'` img
                  '.$join
                   .$cond.'
                  ORDER BY img.`imageID` ASC';

        if ($ok_count) return DB::QueryOneValue($query);
        else $res = DB::QueryToArray($query);
      
        if (is_array($res))
            foreach ($res as &$item){
                //записываем имя
                $ar = explode('/', $item['src']);
                if (is_array($ar))
                    $item['name'] = $ar[3];
                
                //записываем теги
                if ($tagID != 0){
                    $query = 'SELECT
                                ti.`tagID`
                              FROM `fb_TagsImages` ti
                              WHERE ti.`imageID` = "'.$item['imageID'].'"
                              ORDER BY ti.`tagID` ASC';

                    $item['tags'] = DB::QueryToArray($query);
                }
                
                //записываем количество пользователей, к-ые скачали фото
                $query = 'SELECT
                            COUNT(*)
                          FROM `fb_Logs` lg
                          WHERE lg.`imageID` = "'.$item['imageID'].'"';

                $item['count'] = DB::QueryOneValue($query);
            }

        return $res;
    }
    
    public function get_all_tags()
    {
        $query = 'SELECT
                    tg.`tagID`,
                    tg.`caption`
                  FROM `fb_Tags` tg';
        
        $res = DB::QueryToArray($query);
        
        if (is_array($res)){
            $ar = array();
            foreach ($res as $item){
                $ar[$item['tagID']] = $item['caption'];
            }
        }
        
        return $ar;
    }
    
    public function get_user_tags($userID)
    {
        $query = 'SELECT
                    tg.`tagID`
                  FROM `fb_UsersTags` ut
                  INNER JOIN `fb_Tags` tg ON tg.`tagID` = ut.`tagID`
                  WHERE ut.`userID` = "'.$userID.'"';

        $res = DB::QueryToArray($query);
        
        //добавляем нулевой тег, если у пользователя все фото из раздела "ФОТО БЕЗ ТЕГОВ" (для checkbox) ***************
        $query = 'SELECT COUNT(*) FROM `'.TablesNames::$IMAGES_TABLE_NAME.'` img
                  WHERE NOT img.`imageID` IN (SELECT ti.`imageID` FROM `fb_TagsImages` ti)';

        $count_rec = DB::QueryOneValue($query);
        
        $query = 'SELECT COUNT(*) FROM `fb_UsersImages` ui
                  WHERE 
                    ui.`userID` = "'.$userID.'" AND
                    NOT ui.`imageID` IN (SELECT ti.`imageID` FROM `fb_TagsImages` ti)';

        $count_rec_user = DB::QueryOneValue($query);
        
        if ($count_rec == $count_rec_user)
            $res[]['tagID'] = 0;
        // *************************************************************************************************************
        
        return $res;
    }
    
    public function get_user_fotos($userID)
    {
        $query = 'SELECT
                    img.`imageID`,
                    IFNULL(ti.`tagID`,0) as `tagID`
                  FROM `fb_UsersImages` ui
                  INNER JOIN `'.TablesNames::$IMAGES_TABLE_NAME.'` img ON img.`imageID` = ui.`imageID`
                  LEFT JOIN `fb_TagsImages` ti ON ti.`imageID` = img.`imageID`
                  WHERE ui.`userID` = "'.$userID.'"';

        return DB::QueryToArray($query);
    }
    
    public function add_user_tags($userID, $ar_tagID)
    {
        if (empty($ar_tagID)) return false;
        else{
            $ar_set = array();
            foreach($ar_tagID as $item){
                if ($item == 0){ 
                    //добавляем все фото раздела "ФОТО БЕЗ ТЕГОВ"
                    $query = 'SELECT img.`imageID` FROM `'.TablesNames::$IMAGES_TABLE_NAME.'` img
                              WHERE NOT img.`imageID` IN (SELECT ti.`imageID` FROM `fb_TagsImages` ti)';
                      
                    $ar_imageID = DB::QueryToArray($query);
                    if (!empty($ar_imageID)){
                        foreach ($ar_imageID as &$item)
                            $item = $item['imageID'];
                    }
                    
                    $this->add_user_fotos($userID, $ar_imageID);

                    if (count($ar_tagID) == 1) return false;
                    else continue;
                }
                
                $ar_set[] = '("'.$userID.'", "'.$item.'")';
            }
            
            $values = implode(',', $ar_set);
        }

        $query = 'REPLACE INTO `fb_UsersTags` (`userID`, `tagID`) VALUES '.$values;
     
        return DB::Query($query);
    }
    
    public function del_user_tags($userID, $ar_tagID)
    {
        if (empty($ar_tagID)) return false;
        else{
            $ar_set = array();
            foreach($ar_tagID as $item)
                $ar_set[] = '"'.$item.'"';
            
            $values = implode(',', $ar_set);
        }
        
        $query = 'DELETE FROM `fb_UsersTags` 
                  WHERE `userID` = "'.$userID.'" AND `tagID` IN ('.$values.')';
        
        return DB::Query($query);
    }
    
    public function add_user_fotos($userID, $ar_imageID)
    {
        if (empty($ar_imageID)) return false;
        else{
            $ar_set = array();
            foreach($ar_imageID as $item)
                $ar_set[] = '("'.$userID.'", "'.$item.'")';
            
            $values = implode(',', $ar_set);
        }
        
        $query = 'REPLACE INTO `fb_UsersImages` (`userID`, `imageID`) VALUES '.$values;
        
        return DB::Query($query);
    }
    
    public function del_user_fotos($userID, $ar_imageID)
    {
        if (empty($ar_imageID)) return false;
        else{
            $ar_set = array();
            foreach($ar_imageID as $item)
                $ar_set[] = '"'.$item.'"';
            
            $values = implode(',', $ar_set);
        }
        
        $query = 'DELETE FROM `fb_UsersImages` 
                  WHERE `userID` = "'.$userID.'" AND `imageID` IN ('.$values.')';
        
        return DB::Query($query);
    }
    
    public function access_user_image_imageID($imageID, $userID)
    {
        $query = 'SELECT COUNT(*) FROM `fb_UsersImages` ui
                  WHERE ui.`userID` = "'.$userID.'" AND
                        ui.`imageID` = "'.$imageID.'"';
        
        return DB::QueryOneValue($query);
    }
    
    public function access_user_image_tagID($imageID, $userID)
    {
        $query = 'SELECT COUNT(*) FROM `fb_UsersTags` ut
                  INNER JOIN `fb_TagsImages` ti ON ut.`tagID` = ti.`tagID`
                  WHERE ut.`userID` = "'.$userID.'" AND
                        ti.`imageID` = "'.$imageID.'"';
        
        return DB::QueryOneValue($query);
    }
    
    public function log_download($imageID, $userID)
    {
        $query = 'REPLACE INTO `fb_Logs` (`date_last`, `userID`, `imageID`)
                  VALUES (NULL, "'.$userID.'", "'.$imageID.'")';
  
        return DB::Query($query);
    }
}

?>
<?php
/**
 * Конроллер для формы запроса
 * 
 * @version 1.0
 *
 */
class FotobankControllerFront extends FrontController
{
    public $fotobank = null;
    
    public function __construct()
    {
        parent::__construct();
        $this -> view = new FotobankViewFront();
        $this -> fotobank = new Fotobank();
    }
    
    public function Index($args = null)
    {
        //получаем ID пользователя 
        $this -> user = new User();
        if ($this-> user -> GetRole() === 'admin'){
            $userID = null;
        }else{
            $userID = $this-> user -> GetUserID();
        }
        
        //защищаем водным знаком фото 
        if ($ar = explode('.', $_SERVER['REQUEST_URI']) and in_array($ar[count($ar)-1], array('png','gif','jpg','jpeg'))){
            $file_path = explode('/', $ar[0]);
            $file_name = $file_path[3];
            $imageID = (int)substr($file_name, 3);
            
            if ($this-> user -> GetRole() === 'admin'){
                $ok_watermark = false;
                
                //записываем в лог
                if(count($file_path) === 4){
                    $this -> fotobank -> log_download($imageID, $this-> user -> GetUserID());
                }
            }else{
                $ok_watermark = !($this -> fotobank -> access_user_image_imageID($imageID, $this-> user -> GetUserID()) or 
                                  $this -> fotobank -> access_user_image_tagID($imageID, $this-> user -> GetUserID()));
                
                //записываем в лог
                if(!$ok_watermark && (count($file_path) === 4)){
                    $this -> fotobank -> log_download($imageID, $this-> user -> GetUserID());
                }
            }
            
            $this -> waterMark($_SERVER['DOCUMENT_ROOT'].$_SERVER['REQUEST_URI'], $_SERVER['DOCUMENT_ROOT'].'/modules/Fotobank/images/watermark.png', 'center=0,middle=0', null, $ok_watermark);
            return new EmptyActionResult();
        }
        
        //получаем фото выбранного тега
        if (isset($args[0]) and is_numeric($args[0])){
            $user_images = $this -> fotobank -> get_fotos_by_tagID($args[0], false, $userID);
            ViewData::Assign('USER_IMAGES', $user_images);
        };
        
        //используем статический метод для получения параметра rootAlias
        $settings = CommonModule::LoadSettings('Fotobank');
        $rootAlias = $settings['rootAlias'];
        
        //получаем страницу из базы
        $this -> page = new Page(array($rootAlias));
        
        //получаем доступные пользователю теги
        $tat = $this -> fotobank -> get_types_and_tags($userID);

        ViewData::Assign('TYPES_AND_TAGS', $tat);
        ViewData::Assign('ROOT_ALIAS', $rootAlias);
        $this -> view -> Index();
        return $this -> view;
    }
    
    public function waterMark($original, $watermark, $placement = 'bottom=5,right=5', $destination = null, $ok_watermark = true) { 
        $original = urldecode($original);
        $info_o = getImageSize($original); 
        if (!$info_o) 
              return false; 
        $info_w = getImageSize($watermark); 
        if (!$info_w) 
              return false; 

        list($vertical, $horizontal) = split(',', $placement,2); 
        list($vertical, $sy) = split('=', trim($vertical),2); 
        list($horizontal, $sx) = split('=', trim($horizontal),2); 

        switch (trim($vertical)) { 
           case 'bottom': 
              $y = $info_o[1] - $info_w[1] - (int)$sy; 
              break; 
           case 'middle': 
              $y = ceil($info_o[1]/2) - ceil($info_w[1]/2) + (int)$sy; 
              break; 
           default: 
              $y = (int)$sy; 
              break; 
           } 

        switch (trim($horizontal)) { 
           case 'right': 
              $x = $info_o[0] - $info_w[0] - (int)$sx; 
              break; 
           case 'center': 
              $x = ceil($info_o[0]/2) - ceil($info_w[0]/2) + (int)$sx; 
              break; 
           default: 
              $x = (int)$sx; 
              break; 
           } 

        header("Content-Type: ".$info_o['mime']); 

        $original = @imageCreateFromString(file_get_contents($original)); 
        $watermark = @imageCreateFromString(file_get_contents($watermark)); 
        $out = imageCreateTrueColor($info_o[0],$info_o[1]); 

        imageCopy($out, $original, 0, 0, 0, 0, $info_o[0], $info_o[1]); 

        //Здесь задаем размер изображения в которые можно добавлять Watermark
        // $info_o[0] > 250 - ширина изображения должна быть больше 250 px
        // $info_o[1] > 250 - высота изображения должна быть больше 250 px
        $x_water = 400;
        $y_water = 80;

        if( ($info_o[0] > 250) && ($info_o[1] > 250) and $ok_watermark )
        {
            for($i=0;$i*$x_water<$info_o[0];$i++){
                for($j=0;$j*$y_water<$info_o[1];$j++){
                        imageCopy($out, $watermark, $x + $i*$x_water, $y + $j*$y_water, 0, 0, $info_w[0], $info_w[1]);
                }
            }
        }

        switch ($info_o[2]) { 
           case 1: 
              imageGIF($out); 
              break; 
           case 2: 
              imageJPEG($out); 
              break; 
           case 3: 
              imagePNG($out); 
              break; 
              } 

        imageDestroy($out); 
        imageDestroy($original); 
        imageDestroy($watermark); 

        return true; 
   } 
}
?>
<?php

/**
 * Модель для работы с модулем Articles во фронт-енде
 *
 * @version 2.0
 */
class ArticlesModelFront
{
    public static $rootAlias = 'articles';

    public function __construct($rootAlias = null)
    {
        if($rootAlias)
        {
            self::$rootAlias = $rootAlias;
        }
    }

    public function GetArticles($sect = null, $start = 0, $limit = 10)
    {
        $sectCond = !empty($sect) ? " AND c.alias=".DB::Quote($sect) : "";
        $cond = '';//empty($section) ? ' AND a.asectionID IS NULL' : ($section == 'all' ? '' :' AND c.alias = '.DB::Quote($section));

        $query = '
			SELECT SQL_CALC_FOUND_ROWS
                            a.*, c.`caption` as cat_caption, c.`alias` as cat_alias,
                            concat(' . DB::Quote("/" . self::$rootAlias) . ', IF (a.asectionID IS NOT NULL, concat("/",c.alias), "" ), "/",a.alias,"/") as fullAlias
			FROM
                            `' . Articles::$articlesTableName . '` a
                        LEFT JOIN `' . Articles::$sectionsTableName . '` c ON c.asectionID=a.asectionID
			WHERE
                            a.`isPublished` = 1 AND
                            a.`isDeleted` = 0
                            '.$cond.$sectCond.'
			ORDER BY a.`publicationDate` DESC, a.articleID DESC
			LIMIT ' . $start . ', ' . $limit . '
		';

        return DB::QueryToArray($query);
    }

    public function GetArticle($alias,$section=null)
    {
        $cond = empty($section) ? ' AND a.asectionID IS NULL' : ' AND c.alias = '.DB::Quote($section);
        $query = '
			SELECT
				a.*, c.`caption` as cat_caption, c.`alias` as cat_alias
			FROM
				`' . Articles::$articlesTableName . '` a
                        LEFT JOIN `' . Articles::$sectionsTableName . '` c ON c.asectionID=a.asectionID
			WHERE
				a.`alias`=' . DB::Quote($alias) . $cond . " AND a.isPublished = 1 AND a.isDeleted = 0";

        $result = DB::QueryOneRecordToArray($query);
        return $result;
    }

    public function GetFoundRows()
    {
        $query = 'SELECT FOUND_ROWS()';
        return DB::QueryOneValue($query);
    }

    public function isSection($alias)
    {
        // trace($alias);
        return DB::QueryOneValue("SELECT count(*) FROM " . Articles::$sectionsTableName . " WHERE alias=" . DB::Quote($alias));
    }

    public static function GetTopNews($count = 3)
    {
        return DB::QueryToArray('SELECT
				nl.`articleID`, nl.`caption`, nl.`summary`, nl.`publicationDate`,nl.imageID, nl.alias,s.asectionID, s.caption as cat_caption, s.alias as cat_alias,
                concat(' . DB::Quote("/" . self::$rootAlias) . ', IF (nl.asectionID IS NOT NULL, concat("/",s.alias), "" ), "/",nl.alias,"/") as fullAlias
			FROM
				`' . Articles::$articlesTableName . '` nl
			LEFT JOIN `' . Articles::$sectionsTableName . '` s ON s.`asectionID` = nl.`asectionID`
			WHERE
                            nl.`isPublished` = 1 AND nl.`isDeleted` = 0
			ORDER BY nl.`publicationDate` DESC
			LIMIT ' . $count);
    }

    public function GetSection($sectionAlias = null)
    {
        if ($sectionAlias)
        {
            return DB::QueryOneRecordToArray("SELECT * FROM `".Articles::$sectionsTableName."` WHERE alias=".DB::Quote($sectionAlias));
        }
        return [];
    }

}

?>
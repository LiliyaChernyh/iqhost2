<?php

/**
 * Интерфейс кеширующего драйвера
 *
 * @version 1.0
 */
interface ICacheDriver
{
    /**
     * Сохраняет данные в кеше
     *
     * @param string $dataID
     * @param mixed $data
     * @param int $lifetime
     * @return bool
     */
    public function Save($dataID, $data, $tags = null, $lifetime = null);
    
    /**
     * Берет данные из кеша, если они там есть
     *
     * @param string $dataID
     * @return mixed
     */
    public function Load($dataID);
    
    /**
     * Возвращает все данные из кеша, помеченные указанными тегами
     * Передавать можно как массив тегов, так и строку с одним тегом
     *
     * @param array|string $tags
     */
    public function Find($tags);
    
    /**
     * Удаляет данные из кеша
     *
     * @param string $dataID
     * @return bool
     */
    public function Delete($dataID);
    
    /**
     * Удаляет данные из кеша, помеченные указанными тегами
     * Передавать можно как массив тегов, так и строку с одним тегом
     *
     * @param array|string $tags
     * @return bool
     */
    public function DeleteByTags($tags);
}

?>